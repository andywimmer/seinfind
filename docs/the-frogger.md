---
layout: default
title: The Frogger
parent: Season 9
nav_order: 18
permalink: /the-frogger
cat: ep
series_ep: 174
pc: 918
season: 9
episode: 18
aired: April 23, 1998
written: Gregg Kavet, Andy Robin, Steve Koren & Dan O'Keefe (story). Teleplay by Gregg Kavet & Andy Robin
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697701
wiki: https://en.wikipedia.org/wiki/The_Frogger
---

# The Frogger

| Season 9 - Episode 18                                                                                        | April 23, 1998            |
|:-------------------------------------------------------------------------------------------------------------|:--------------------------|
| Written by Gregg Kavet, Andy Robin, Steve Koren & Dan O'Keefe (story) - Teleplay by Gregg Kavet & Andy Robin | Directed by Andy Ackerman |
| Series Episode 174                                                                                           | Production Code 918       |

"The Frogger" is the 174th episode of the NBC sitcom Seinfeld. It is the 18th episode for the ninth and final season. It first aired on April 23, 1998.

## Plot

Elaine is confronted with cake from two separate celebrations at her workplace; tired of the forced socializing, she calls in sick the next day. Jerry and George go to their old high-school hangout, Mario's Pizza Parlor, for one last slice of pizza before it closes down. Kramer has visited the police station where he obtained some caution tape used for crime scenes and also heard about a serial killer nicknamed "The Lopper" who is on the loose in the Riverside Park area.

At Mario's, George discovers he still has the high score on the old Frogger video game, with a score of 860,630 points; both he and Jerry remember that they stopped going to Mario's because he had a tendency to insult his customers. Elaine's co-workers give her a cake to celebrate her return to work from being sick, but she refuses to take part in any future celebrations. Jerry dates Elaine's friend Lisi (Julia Campbell) and discovers that she's a sentence-finisher: "It's like dating Mad Libs!" After lamenting that his shrine will be gone, George decides to buy the Frogger machine to preserve his fame, but Jerry asks how he will move it and keep it plugged in to preserve the high score. Kramer discovers that the last victim of the Lopper looked a lot like Jerry.

George works to find a solution to his Frogger problem, and Kramer volunteers the help of a man he knows named "Slippery Pete" (Peter Stormare). Missing the 4:00 sugar-rush she'd become used to from all the celebrations, Elaine raids her boss Peterman's refrigerator, where she finds a piece of cake. Later, Peterman reveals that it's worth $29,000 because of its historical significance: it's from King Edward VIII's wedding to Wallis Simpson.

Jerry wants to break up with Lisi, then discovers she lives in the Riverside Park area. To avoid the Lopper, he takes her back to his place, where she finishes one of his thoughts that takes their relationship to the next level. Elaine tells Jerry and George about the cake, then tells Jerry that Lisi is planning a weekend trip for them to Pennsylvania Dutch country. Jerry fears that Lisi received the wrong message; a trip like that means it's a serious relationship! Elaine tries to even out Peterman's slice of cake, but gets swept up in the moment and finishes it off. George and Kramer meet with Slippery Pete and truck driver Shlomo (Reuven Bar Yotam), to coordinate the movement of the Frogger machine. Elaine later looks for a replacement for Peterman's cake, and Kramer suggests an Entenmann's cake.

Jerry goes to Lisi's apartment, where he tries to break up with her. When Jerry is finally ready to leave, it's 10 hours later and dark, and as he exits the apartment he sees a man whom he fears is the Lopper and pleads that Lisi let him back inside; the man turns out to actually be just "Slippery Pete". Peterman is bewildered when he has his piece of cake appraised at $2.19. George finds "Slippery Pete" playing his Frogger game on battery power until only about 3 minutes of power remain. The only available power source is across the busy street, and Kramer has run out of caution tape. Convinced he does not need any help, George starts moving the machine across the busy street, moving through traffic like the frog from the video game. However, as George reaches the opposite sidewalk he is unable to lift the game onto the curb; an oncoming Freightliner smashes the game cabinet, causing Jerry to quip, "Game over".

Peterman shows Elaine surveillance videotape of her eating and "dancing" with the slice of cake. He is convinced that the effect of such a "vintage" cake on her digestive system will be all the punishment she needs and dismisses her.

## The Frogger arcade game

The sound effects during George's moving of the machine are actual sounds from Frogger, played in time with his movements. The sound that plays shortly after the machine is smashed by a truck is the "squash" sound when the frog is hit by a vehicle during the game. On September 24, 2005, The Twin Galaxies Intergalactic Scoreboard issued a colorful poster that offered a $1,000 cash prize to the first video game player who could break George Costanza's fictional Frogger high score of 860,630 points as portrayed in "The Frogger". No player was able to break this mark before the December 31, 2005 deadline. On December 22, 2009, Pat Laffaye of Connecticut, USA, scored a Frogger world record high score of 896,980 points. His score was beaten again by Michael Smith, who scored 970,440 points on July 15, 2012.

## Production

This episode was originally going to be titled "The Cake Parties".

Jason Alexander performed his own stunt in this episode, actually diving out of the path of an oncoming truck and being showered with the shrapnel of the crushed Frogger machine as the truck ran it over. He later recounted that two large and heavy pieces of paneling from the side of the game landed uncomfortably close to his head during the shooting of this scene.

Most scenes deleted from this episode involved Kramer attracting a new "long distance" girlfriend; this carries over to the next episode "The Maid".

## Cast

#### Regulars

Jerry Seinfeld ...................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus .............. Elaine Benes  
Michael Richards .................. Cosmo Kramer

#### Recurring

John O'Hurley ................ J. Peterman

#### Guests

Julia Campbell ...................... Lisi  
Peter Stormare .................... Slippery Pete  
Reuven Bar ......................... Shlomo  
Sam Shamshak .................... Sal  
Wayne Wilderson ............... Walter  
Mark Daniel Cade ............... Other Walter  
Jack Esformes ..................... Mike  
Melissa Denton ................... Kobe  
Oliver Muirhead ................. Lubeck  
Drake Bell ........................... Kenny

## Script

[INT. J. PETERMAN LUNCHROOM - DAY]

(Elaine and several co-workers stand around a table which has a cake sitting on it.)

ALL (singing): Happy birthday to you.

WALTER: Thanks.

(Everyone claps.)

FEMALE WORKER: Elaine, cake?

ELAINE: Uh, no, thanks.

FEMALE WORKER: It's Walter's special day.

ELAINE: You know, there are 200 people who work in this office. Every day is somebody's special day.

(Elaine takes a piece of cake and makes her way to the door, but is stopped as a male worker carrying a cake enters.)

MALE WORKER: Elaine! Where're you going? It's Walter's last day. We have to celebrate.

ELAINE: It's his birthday and it's his last day?

MALE WORKER: This is other-Walter, from returns.

(Other-Walter enters followed by more co-workers.)

OTHER-WALTER: Hey, what's going on here?

ALL: Surprise!

OTHER-WALTER: Oh guys.

(Elaine tries to leave, but other-Walter stops her.)

OTHER-WALTER: Elaine, it's my last day. Have a piece.

ELAINE: Alright, pile it on.

ALL (singing, competing): For he's a jolly good fellow... happy birthday to you... for he's a jolly good fellow... birthday to you... which nobody can deny...

(Elaine looks on frustrated.)

[INT. JERRY'S APARTMENT - DAY]

(Jerry and Elaine stand in front of his stereo.)

JERRY: What is so bad about having a little piece of cake?

ELAINE: It is the forced socializing. I mean, just because we work in the same office, why do we have to act like we're friends?

JERRY: Why aren't you there now?

ELAINE: I had to take a sick day. I'm so sick of these people. By the way, I talked to Lisi, and tomorrow night's good for her.

(They sit on the sofa.)

JERRY: You know, I shouldn't go out with a friend of yours. I foresee messiness.

ELAINE: Yeah, you're better off sitting around here, reading comic books, and eating spaghetti at two in the morning...

JERRY: Hey, speaking of tomato sauce, you want to come with me and George to Mario's Pizza?

ELAINE: Your old high school hangout? Why?

JERRY: They're closing. We're going for one last slice.

(Kramer barges through the door holding a roll of yellow police tape.)

KRAMER: Hey. Alright. Hi. Check it out, official police caution tape. Look at that.

(Jerry walks towards the counter where Kramer has placed some of the tape.)

KRAMER: Uh-uh-uh. Step back, son, there's nothing to see here.

JERRY: Where did you get this?

KRAMER: Well, I got it from my cop buddy Doug.

JERRY: You sure have a lot of friends. How come I never see any of these people?

KRAMER: They want to know why they never see you.

(Kramer ties a piece of tape around a banana.)

KRAMER: I'm gonna eat that later.

JERRY: So they just gave you this?

KRAMER: Oh no, no no. I had to fish around in the evidence room for it. You know, they're all preoccupied, trying to hunt down this new psycho-serial killer, the Lopper. Alright, I'll see ya.

ELAINE: Wait a minute, wait a minute. Who is the Lopper?

KRAMER: Oh, it's no big deal. It's just some guy who's been running around Riverside park-pffff. You know, cutting people's heads off.

JERRY: How come I haven't read about this?

KRAMER: Well, you know, the police, they've been having some internal dissension about the name.

ELAINE: Really? What're the other titles?

KRAMER: Uh, Headso... uh... The Denogginizer... Son of dad.

JERRY: Son of dad?

KRAMER: Yeah. That was my suggestion. It's sort of a catchall.

[INT. MARIO'S PIZZA - DAY]

GEORGE: Mario's Pizza.

(George and Jerry admire their former hang out. Mario, an elder man, stands behind the counter.)

GEORGE: Just as she was. Hey, Mario! Remember us?

MARIO: No.

JERRY: We used to come in every day.

MARIO: So where ya been? We're tanking here.

GEORGE: We'll have 2 slices and 2 grape sodas.

MARIO: Oh, thanks. That'll save us.

JERRY: Alright, make it the large sodas.

(George and Jerry walk across the room.)

GEORGE: Hey, Jerry, remember Frogger? I used to be so into this game. Gettin' that frog across the street was my entire life.

(They walk over to watch a boy playing Frogger.)

JERRY: Yeah. And then you went on to... Well, it's a good game.

GEORGE: Double jump! Eat the fly! Eat it!

(The boy loses.)

BOY: Thanks a lot.

GEORGE: Ah, beat it, punk.

(The boy exits.)

JERRY: Hey, look at the high score -- &quot;G.L.C.&quot; George Louis Costanza. That's not you, is it?

GEORGE: Yes! 860,000. I can't believe it's still standing. No one has beaten me in like 10 years.

JERRY: I remember that night.

GEORGE: The perfect combination of Mountain Dew and mozzarella... just the right amount of grease on the joystick...

MARIO: Here's your pizza pea brains.

JERRY: I think I remember why we stopped coming here.

GEORGE: Yeah.

[INT. ELAINE'S OFFICE - DAY]

(Elaine is sitting at her desk smelling a pen.)

ELAINE (thinking): This pen smells really bad. So why do I keep smelling it? Is it too late for me to go to law school?

(There's a knock on the door and several co-workers enter with a cake.)

ELAINE: What is this?

MALE WORKER: You were out sick yesterday, so we got you a get-well cake.

FEMALE WORKER: It's carrot. It's good for you.

WORKERS (singing): Get well get well soon, we wish you to get--

ELAINE: Stop it! That's not even a song! I mean, now we're celebrating a sick day?

MALE WORKER: I think it's nice.

ELAINE: What? What is nice? Trying to fill the void in your life with flour and sugar and egg and vanilla? I mean, we are all unhappy. Do we have to be fat, too? Not you Becky, I know you have a slow metabolism. I don't want one more piece of cake in my office!

(Another worker enters late.)

WORKER (singing): Get well, get well soon--

MALE WORKER: It's not happening.

(They all start to leave disappointed.)

BECKY: Can we still it eat?

[INT. MONK'S DINER - NIGHT]

(Jerry and Lisi sit at the usual booth.)

JERRY: I'll tell you Lisi, I never expected that movie to--

LISI: End under water?

JERRY: Be that long. I mean, most action movies are--

LISI: So much more violent.

JERRY: Not as long.

LISI: Well, I should probably--

JERRY: Get going.

LISI: Yeah.

(They both stand.)

JERRY: Well, it was nice meeting you. I'm sure I'll see you--

LISI: Eight tomorrow?

JERRY: Actually, that's--

LISI: What you were thinking.

JERRY: Right.

(Lisi leaves and Jerry goes to pay the cashier. George enters.)

GEORGE: Oh! Here you are. Ha ha... You, uh, you want to--

JERRY: Sure. (points at booth) How about this one?

(They both sit down at their booth.)

GEORGE: Well, I'm doing it, Jerry. I'm buying the Frogger machine. Now the torch will burn forever.

JERRY: Fabulous. See, now you're really do something.

GEORGE: So, you want to come down to Mario's Pizza with me and help me pick up the Frogger?

JERRY: Hey, how you gonna keep the machine plugged in while you move it?

GEORGE: What?

JERRY: Once you unplug the machine, all the scores will be erased.

GEORGE: You're right. Why must there always be a problem? You'd think just once I could get a break. God knows I earned it with that score!

(George gets up and leaves in a huff.)

[INT. JERRY'S APARTMENT - DAY]

KRAMER: Well, more bad news Jerry.

(Kramer and Jerry are talking near the kitchen counter. George is sitting at the table on Jerry's cordless phone. An open phone book is in front of him.)

KRAMER: You know the police, they found another victim of the Lopper in Riverside Park. I saw the photo, and it looked a lot like you.

JERRY: Oh, come on. There's a lot of people walking around the city that look like me.

KRAMER: Not as many as there used to be.

GEORGE: No. I need a guy that can rig a Frogger machine so that I can move it without losing power, 'cause I have the high score. H-hello?

(Kramer peels and eats an orange.)

KRAMER: You know, George, you're not gonna find an electrician like that in the yellow pages. Now, I know just the guy who can do this.

JERRY: Another friend?

KRAMER: Oh, no, no, no. This guy is no friend. In fact, we don't even get along.

GEORGE: Well, is he good, Kramer?

(George gets up and walks towards Kramer.)

KRAMER: Oh, he's the best... and the worst.

GEORGE: Kramer, listen to me. I'm never gonna have a child. If I lose this Frogger high score, that's it for me.

KRAMER: Believe me George, you can count on Slippery Pete.

GEORGE: Slippery Pete?

KRAMER: Yeah, I don't care for the name, either. In fact, that's one of the things that we argue about.

GEORGE: Alright, I'm gonna find a guy with a truck. GLC must live on!

(George grabs his coat and leaves the apartment. The phone rings. Jerry tries to get by Kramer.)

JERRY: Come on.

KRAMER: Dng-ga-gng-ga-wt.

(Jerry picks up the phone.)

JERRY: Hello?

[INT. ELAINE'S OFFICE/JERRY'S APARTMENT]

(Elaine is on the phone and is smelling her tape dispenser.)

ELAINE: So how's it going with my friend?

JERRY: She's a sentence finisher. It's like dating Mad Libs.

(People can be heard singing &quot;Happy Birthday&quot; in the background of Elaine's office.)

JERRY: What is that?

ELAINE: Oh, it's a cake party. It's the third one today. I didn't realize how hooked I got on that 4:00 sugar rush.

JERRY: So join in.

ELAINE: I can't. I denounced them. Maybe I'll go raid Peterman's fridge. He's always got a truffle or something in there.

JERRY: Alright.

(Jerry hangs up and stands up to find some police tape around a broken egg on the floor.)

JERRY: Hey, wh-what--

KRAMER: Yeah. I dropped an egg. Be careful.

(Kramer leaves.)

[INT. J. PETERMAN'S OFFICE - DAY]

(Elaine knocks on the open door.)

ELAINE: Anybody here? Peterboy?

(No one answers. She runs over to Peterman's fridge, opens it, and takes out a box. She opens the box to find a cake.)

ELAINE: Ooh, it's a cake walk.

(She takes a bite of the cake. Outside in the hallway, Mr. Peterman can be heard singing.)

PETERMAN (singing): Get well, get well soon we wish you to get well.

(Elaine quickly puts the box back in the fridge. Mr. Peterman enters his office.)

PETERMAN: Ha ha ha ha... Oh, what a stirring little anthem of wellness.

ELAINE: Mr. Peterman, um--

PETERMAN: We missed you at the get well party. Poor old Walt has a polyp in the duodenum. It's benign, but--ooh--still a bastard. Oh, Elaine, can you keep a secret?

ELAINE: No, sir, I can't.

PETERMAN: Inside that small college boy mini-fridge is my latest acquisition. A slice of cake from the wedding of King Edward VIII to Wallis Simpson, circa 1937, price -- $29,000.

[EXT. CITY SIDEWALK - NIGHT]

(Jerry and Lisi walk along.)

JERRY: Well Lisi, that was another-

LISI: Lovely evening.

JERRY: Really bad meal. I was thinking maybe we should--

LISI: Go for a hansom cab ride?

JERRY: Call it a night. I'll walk you home. Where do you live?

LISI: 84th street, right off Riverside Park.

JERRY: Riverside Park.

(Jerry grabs Lisi and turns around.)

LISI: I thought we were going--

JERRY: Back to my place. That's right.

[EXT. JERRY'S APARTMENT - DAY]

(George and Jerry are sitting on the sofa.)

GEORGE: So you slept with her?

JERRY: She lives right off Riverside Park. I was scared of the Lopper, So I let her stay over.

GEORGE: And you automatically sleep with her?

JERRY: Well, I just wanted to make out a little, but she kind of--

GEORGE: Finished your thought.

(George gets up and walks over to the sink. Elaine enters.)

ELAINE: Guess what I ate.

GEORGE: An ostrich burger.

ELAINE: No. A $29,000 piece of cake. Peterman got it at The Duke Of Windsor auction. It was the most romantic thing I've ever eaten.

JERRY: How'd it taste?

ELAINE: A little stale.

JERRY: Yeah.

GEORGE (nudges Elaine with his elbow): So, uh are you sleeping with Peterman?

ELAINE (nudges George with her elbow): No. He doesn't know I ate it. In fact, he almost caught me. I have to sneak back in and even it out.

GEORGE: You know, they say ostrich has less fat, but you eat more of it.

(Jerry and Elaine start to walk from the counter towards the table.)

ELAINE: Hey, so I talked to Lisi and she has got a big surprise for you. She's planning a weekend trip to Pennsylvania Dutch country.

JERRY: Pennsylvania Dutch country? Oh, that's the serious relationship weekend place.

ELAINE: What is going on with you two?

JERRY: Well, I think by sleeping with her, I may have sent her the wrong message.

GEORGE: What's that?

(Elaine opens up a paper bag and pulls out a cookie.)

ELAINE: 4:00 sugar fix.

JERRY: Well, I'm calling this off right now.

ELAINE: No, no. You are way past the phone call breakup stage.

JERRY: Well, I'm not going over there. That's where the Lopper is.

ELAINE: Oh... it's daylight. It won't take you that long. Just make a clean break.

(Elaine bites the head of her gingerbread man.)

[INT. J. PETERMAN'S OFFICE - DAY]

ELAINE: Just a little off the side...

(Elaine is at Mr. Peterman's desk with the cake box.)

ELAINE: Well, no point in wasting $1,200.

(She eats a slice of the cake as fantasy waltz music starts to play. Elaine dances around the room talking to one of the sculptures in the room.)

ELAINE (thinking): Oh, commander, isn't the wedding marvelous? More cake? Oh, I shouldn't. I mustn't. Ah, what the hell?

(She gets more cake.)

[INT. MONK'S DINER - DAY]

GEORGE: Now, each of you is here because you're the best at what you do.

(George, Kramer, Slippery Pete, and Shlomo sit at a booth.)

GEORGE: Slippery Pete, Kramer tells me you are one hell of a rogue electrician. And Shlomo, you're the best truck driver.

SHLOMO: I don't know If I'm the best.

GEORGE: Oh... you're very good.

SHLOMO: Let's say &quot;good.&quot;

GEORGE: Ok. Good. And Kramer, you're in charge of taping off the loading zone.

KRAMER: Lock and load.

SLIPPERY PETE: You think you can handle that, numb-nuts?

KRAMER: Alright, alright, come on, now.

SLIPPERY PETE: That was my mail-order bride.

KRAMER: Hey, you weren't home, so I signed for her.

SLIPPERY PETE: It doesn't give you the right to make out with her.

KRAMER: You weren't even married yet.

GEORGE: Alright, alright, calm down, calm down. Whatever happened in the past is past.

(George gets a napkin and starts to draw on it.)

GEORGE: Now, this is the basic layout for Mario's Pizza.

SHLOMO: So what kind of jail time are we looking at if we're caught?

GEORGE: What do you mean?

SLIPPERY PETE: We're stealing this thing, right?

GEORGE: No. I--I paid for it.

SLIPPERY PETE: I thought we were stealing it.

KRAMER: Yeah. It feels like we're stealing it.

GEORGE: We're not stealing it.

SHLOMO: I definitely thought we're stealing it.

GEORGE: Alright, let's--let's focus. Can we get back to the plan?

SLIPPERY PETE: Well, I need a battery for this kind of a job. Can I at least steal a battery?

GEORGE: Fine. Steal the battery. Now, alright, here is the Frogger. This is the front door, and this is the outlet.

SLIPPERY PETE: What's that?

GEORGE: The outlet?

SLIPPERY PETE: Mm-hmm.

GEORGE: That's where the electricity comes out.

SLIPPERT PETE: Oh, you mean the holes.

SHLOMO: Which one's the bathroom?

GEORGE: Uh, here.

SHLOMO: They put the Frogger with the toilet? Yecchh.

GEORGE: The Frogger is here.

KRAMER: George, I thought that was the door.

SLIPPERY PETE: Where are all the pizza ovens?

SHLOMO: I thought the bathroom was here.

[INT. MARIO'S PIZZA - DAY]

(George, Shlomo, Slippery Pete, and Kramer are sitting in the same places as they were in the booth at Monk's, but this time they are around a table at Mario's.)

GEORGE: Alright. You understand now? It's not that complicated.

[INT. MONK'S DINER - DAY]

ELAINE: I need to replace an antique piece of cake.

(Elaine is sitting at the counter with a Sotheby's auction book open in front of her.)

ELAINE: Do you have anything that's been... you know, laying around for a while? Something prewar would be just great.

(The waitress leaves and Kramer enters.)

KRAMER: Oh, hey, Elaine. What, you got the munchies?

ELAINE: Oh, Kramer, I am in big, big, big trouble. I need a cake that looks like this.

KRAMER: Oh, yeah--Sotheby's. Yeah. They make good cake.

ELAINE: Do any of these look close?

KRAMER: No, but I know I've seen cake just like that. Oh -- Entenmann's. Yeah.

ELAINE: Entenmann's? From the supermarket?

KRAMER: Well, no. They're not really in the supermarket. Yeah, they got their own case at the end of the aisle.

[INT. LISI'S APARTMENT - DAY]

(There's a knock at the door. Lisi opens it. It's Jerry holding a baseball bat.)

JERRY: Hi, Lisi.

LISI: Hi, honey. Is that a bat?

JERRY: Uh, yeah. I found it on the street. It's gotta be worth something.

LISI: So, what do you want to do, Sweetheart?

JERRY: Well, before we do anything... maybe we should talk.

MONTAGE:

(Lisi is sitting at her couch while Jerry paces behind her.)

JERRY: Then this Pennsylvania Dutch thing comes out of nowhere. I mean, how am I supposed to respond to that?

(Both Jerry and Lisi are on the couch. Jerry has his head in his hands.)

LISI: Then may I say something... without being interrupted?

(Lisi is in another room with the door closed. Jerry stands in the hallway and is talking to her through the door.)

JERRY: Well I'm sorry if I ruined your life. That's exactly what I set out to do.

(They are sitting on the couch again. Jerry nods at everything Lisi mumbles.)

LISI: Uh-huh. Uh-huh. Mm-hmm. Uh-huh...

(Now Jerry is sitting on the couch and Lisi is pacing behind him.)

LISI: Are you afraid to kiss me in public?

JERRY: Have we even been in public?

(Jerry is walking away from Lisi and she follows behind him.)

LISI: So now you're going to tell me what I'm thinking. Well, go ahead, 'cause I'd really like to know.

(Lisi is crying and Jerry stands looking over her shoulder.)

JERRY: You are not dumb. Don't say that...

(They both sit around the coffee table eating beans.)

JERRY: These beans are pretty good.

LISI: 20 minutes.

(Now Jerry is in a room with the door shut. Lisi stands out in the hallway.)

JERRY: Well, I'm sorry I'm not Brad. I'm me!

(Jerry opens the door.)

JERRY: Nice to meet ya!

(Jerry is lying on the couch while Lisi paces behind.)

LISI: Boy, did your mother do a number on you.

(Lisi is on the couch with Kleenex and Jerry stands on the other side of the couch.)

LISI: Fine. So it's over.

JERRY: Oh, thank god. Why is it dark out? What time is it?

LISI: 9:30.

JERRY: We've been breaking up for 10 hours?

LISI: Good-bye, Jerry.

JERRY: Lopper. You know, Lisi, maybe we should give this a little more time. See how it looks in the light of day.

LISI: Out!

[EXT. LISI'S APARTMENT - NIGHT]

(Jerry steps out of Lisi's building. He looks both ways cautiously before walking down the steps. Surprised, he sees a shadowy figure walking towards him holding something resembling a head in one hand and a knife in the other.)

JERRY: Lopper.

(He quickly runs back up to Lisi's building and yells into her intercom system.)

JERRY: Lisi, Lisi. Let me in! We can work this out. I was wrong, you were right. I'll do anything!

(She buzzes him up. Just as he walks into the building, we see the Lopper is actually Slippery Pete carrying a car battery by it's wires)

[EXT. CITY SIDEWALK - DAY]

(Jerry stands outside a boutique store when George arrives carrying rope.)

GEORGE: Jerry, you came for the big moment.

JERRY: No. I'm waiting for...

GEORGE: Ha ha. Everything's timed out to perfection, Jerry. Slippery Pete's got the Frogger running on battery power, the truck will be there any minute, and Kramer's taped out the loading zone.

JERRY: Oh. Sounds great.

GEORGE: Yeah, yeah. You gotta come over tonight. We can play.

JERRY: Oh, I can't. I'm busy. I'm going away on a long weekend.

GEORGE: Where?

(Lisi exits the boutique wearing a Dutch bonnet.)

LISI: Look what I found. I got one for you, too.

(She puts a Dutch hat on Jerry head.)

JERRY: Great. Uh, you know what? Why don't you put it in the car so I don't toss it in that dumpster?

LISI: Ha ha. Ok. I'll meet thee in front of your place, 15 minutes.

(Lisi leaves.)

JERRY: A long, long weekend.

GEORGE: I hear thee.

[INT. J. PETERMAN'S OFFICE - DAY]

(Elaine has finished replacing the cake. She throws the empty Entenmann's box away. She's about to leave, but Mr. Peterman enters with another man.)

PETERMAN: Elaine! Excellent. I'd like you to meet a friend of mine, Irwin Lubeck.

ELAINE: Oh, hello.

LUBECK: Charmed.

(Mr. Peterman gets the cake out of the fridge.)

PETERMAN: Alright, brace yourself, Lubeck. You are about to be launched via pastry back to the wedding of one of the most dashing and romantic Nazi sympathizers of the entire British Royal family.

ELAINE: I guess I'll just--

PETERMAN: Oh, no Elaine, stay. Lubeck here is the world's foremost appraiser of vintage pastry.

(Lubeck inspects the cake.)

PETERMAN: Alright, Lubeck. How much is she worth?

LUBECK: I'd say about 219.

PETERMAN: Ha ha ha ha ha! $219,000! Lubeck, you glorious titwillow. You just made me a profit of $190,000.

LUBECK: No, $2.19. It's an Entenmann's.

PETERMAN: Do they have a castle at Windsor?

LUBECK: No. They have a display case at the end of the aisle.

PETERMAN: Oh, good lord.

LUBECK: You alright, Peterman? You look ill.

ELAINE (singing): Get well, get well soon, we want you to get well. Get well, get well soon we want you to get well.

[EXT. CITY SIDEWALK - DAY]

(George and Jerry see Kramer, Shlomo, and Slippery Pete standing outside Mario's Pizza. Slippery Pete is playing Frogger.)

GEORGE: What are you guys doing?

SHLOMO: Eat the fly. Eat the fly. Got him!

GEORGE: You idiots. You're gonna wear down the battery.

SLIPPERY PETE: The batteries are fine. We've got... oh, god. only 3 minutes left.

GEORGE: Quick. Get this thing back in the pizzeria

KRAMER: George, they closed up.

GEORGE: I need an outlet!

SLIPPERY PETE: A what?

GEORGE: Holes! I need holes!

KRAMER: The pharmacy's still open.

GEORGE: Alright. Kramer, you block off traffic. You to go sweet-talk the pharmacist.

(Shlomo and Slippery Pete go to the pharmacy.)

SLIPPERY PETE (to George): You owe me a quarter.

GEORGE (to Jerry): Slippery Pete. Kramer, hurry up!

(Kramer ties the police tape to a tree and runs into traffic. He runs out of tape before he can get halfway.)

KRAMER: Ahh! I'm out! No tape left!

JERRY: Well, come on George, I'll help you push it across.

GEORGE: Wait a minute. This looks familiar. This reminds me of something. I can do this.

JERRY: By yourself?

GEORGE: Jerry, I've been preparing for this moment my entire life.

(George pushes the machine onto the street. From a view in the sky, we see him dodging cars, hopping back and forth into lanes of traffic. Frogger music and sound effects play.)

SHLOMO: He looks like a Frog.

SLIPPERY PETE: So do you.

(George makes it across just as a huge truck comes barreling down the street. George tries to get the Frogger onto the sidewalk, but can't. He futilely sticks his hand out trying to stop the truck which honks. George jumps out of the truck's way and onto the sidewalk as the Frogger is smashed.)

JERRY: Game over.

[INT. J. PETERMAN'S OFFICE - DAY]

(Mr. Peterman sits in his chair behind his desk. Elaine knocks and enters.)

ELAINE: Mr. Peterman, you wanted to see me, sir?

PETERMAN: Elaine, up until a moment ago, I was convinced that I was on the receiving end of one of the oldest baker's grift in the books -- The Entenmann's shim-sham.

ELAINE: Ohh...

PETERMAN: Until I remembered the videotape surveillance system that I installed to catch other-Walter using my latrine. But it also caught this.

(Mr. Peterman shows Elaine the tape of her waltzing around the room eating the cake.)

EALINE: Mr. Peterman, I, uh...

PETERMAN: Elaine, I have a question for you. Is the item still... with you?

ELAINE: Um... as far as I know.

PETERMAN: Do you know what happens to a butter-based frosting after six decades in a poorly ventilated English basement?

ELAINE: Uh, I guess I hadn't--

PETERMAN: Well, I have a feeling that what you are about to go through is punishment enough. Dismissed.

The End
