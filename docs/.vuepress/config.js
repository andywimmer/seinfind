const { description } = require('../../package')

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: 'Seinfind',
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ['meta', { name: 'theme-color', content: '#8b4513' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    ['link', { rel: "apple-touch-icon", sizes: "152x152", href: "/apple-touch-icon.png"}],
    ['link', { rel: "icon", type: "image/png", sizes: "32x32", href: "/favicon-32x32.png"}],
    ['link', { rel: "icon", type: "image/png", sizes: "16x16", href: "/favicon-16x16.png"}],
    ['link', { rel: "manifest", href: "/site.webmanifest"}],
    ['link', { rel: "mask-icon", href: "/safari-pinned-tab.svg", color: "#3a0839"}],
    ['link', { rel: "shortcut icon", href: "/favicon.ico"}],
    ['meta', { name: "msapplication-TileColor", content: "#8b4513"}],
    ['meta', { name: "msapplication-config", content: "/browserconfig.xml"}]
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    // repo: 'https://gitlab.com/andywimmer/seinfind',
    // repoLabel: false,
    // editLinks: true,
    // docsDir: 'docs',
    // editLinkText: 'Edit',
    // lastUpdated: 'Updated',
    sidebar: {
      '/': [
        {
          title: 'Season 1',
          collapsable: true,
          children: [
            'the-seinfeld-chronicles',
            'the-stake-out',
            'the-robbery',
            'male-unbonding',
            'the-stock-tip'
          ]
        },
        {
          title: 'Season 2',
          collapsable: true,
          children: [
            'the-ex-girlfriend',
            'the-pony-remark',
            'the-jacket',
            'the-phone-message',
            'the-apartment',
            'the-statue',
            'the-revenge',
            'the-heart-attack',
            'the-deal',
            'the-baby-shower',
            'the-chinese-restaurant',
            'the-busboy'
          ]
        },
        {
          title: 'Season 3',
          collapsable: true,
          children: [
            'the-note',
            'the-truth',
            'the-pen',
            'the-dog',
            'the-library',
            'the-parking-garage',
            'the-cafe',
            'the-tape',
            'the-nose-job',
            'the-stranded',
            'the-alternate-side',
            'the-red-dot',
            'the-subway',
            'the-pez-dispenser',
            'the-suicide',
            'the-fix-up',
            'the-boyfriend',
            'the-limo',
            'the-good-samaritan',
            'the-letter',
            'the-parking-space',
            'the-keys'
          ]
        },
        {
          title: 'Season 4',
          collapsable: true,
          children: [
            'the-trip',
            'the-pitch',
            'the-ticket',
            'the-wallet',
            'the-watch',
            'the-bubble-boy',
            'the-cheever-letters',
            'the-opera',
            'the-virgin',
            'the-contest',
            'the-airport',
            'the-pick',
            'the-movie',
            'the-visa',
            'the-shoes',
            'the-outing',
            'the-old-man',
            'the-implant',
            'the-junior-mint',
            'the-smelly-car',
            'the-handicap-spot',
            'the-pilot'
          ]
        },
        {
          title: 'Season 5',
          collapsable: true,
          children: [
            'the-mango',
            'the-puffy-shirt',
            'the-glasses',
            'the-sniffing-accountant',
            'the-bris',
            'the-lip-reader',
            'the-non-fat-yogurt',
            'the-barber',
            'the-masseuse',
            'the-cigar-store-indian',
            'the-conversion',
            'the-stall',
            'the-dinner-party',
            'the-marine-biologist',
            'the-pie',
            'the-stand-in',
            'the-wife',
            'the-raincoats',
            'the-fire',
            'the-hamptons',
            'the-opposite'
          ]
        },
        {
          title: 'Season 6',
          collapsable: true,
          children: [
            'the-chaperone',
            'the-big-salad',
            'the-pledge-drive',
            'the-chinese-woman',
            'the-couch',
            'the-gymnast',
            'the-soup',
            'the-mom-and-pop-store',
            'the-secretary',
            'the-race',
            'the-switch',
            'the-label-maker',
            'the-scofflaw',
            'the-highlights-of-100',
            'the-beard',
            'the-kiss-hello',
            'the-doorman',
            'the-jimmy',
            'the-doodle',
            'the-fusilli-jerry',
            'the-diplomats-club',
            'the-face-painter',
            'the-understudy'
          ]
        },
        {
          title: 'Season 7',
          collapsable: true,
          children: [
            'the-engagement',
            'the-postponement',
            'the-maestro',
            'the-wink',
            'the-hot-tub',
            'the-soup-nazi',
            'the-secret-code',
            'the-pool-guy',
            'the-sponge',
            'the-gum',
            'the-rye',
            'the-caddy',
            'the-seven',
            'the-cadillac',
            'the-shower-head',
            'the-doll',
            'the-friars-club',
            'the-wig-master',
            'the-calzone',
            'the-bottle-deposit',
            'the-wait-out',
            'the-invitations'
          ]
        },
        {
          title: 'Season 8',
          collapsable: true,
          children: [
            'the-foundation',
            'the-soul-mate',
            'the-bizarro-jerry',
            'the-little-kicks',
            'the-package',
            'the-fatigues',
            'the-checks',
            'the-chicken-roaster',
            'the-abstinence',
            'the-andrea-doria',
            'the-little-jerry',
            'the-money',
            'the-comeback',
            'the-van-buren-boys',
            'the-susie',
            'the-pothole',
            'the-english-patient',
            'the-nap',
            'the-yada-yada',
            'the-millennium',
            'the-muffin-tops',
            'the-summer-of-george'
          ]
        },
        {
          title: 'Season 9',
          collapsable: true,
          children: [
            'the-butter-shave',
            'the-voice',
            'the-serenity-now',
            'the-blood',
            'the-junk-mail',
            'the-merv-griffin-show',
            'the-slicer',
            'the-betrayal',
            'the-apology',
            'the-strike',
            'the-dealership',
            'the-reverse-peephole',
            'the-cartoon',
            'the-strongbox',
            'the-wizard',
            'the-burning',
            'the-bookstore',
            'the-frogger',
            'the-maid',
            'the-puerto-rican-day',
            'the-chronicle',
            'the-finale'
          ]
        },
        // {
        //   title: 'What\'s the deal with...',
        //   collapsable: true,
        //   children: [
        //     'dating',
        //   ]
        // }
      ],
    }
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: [
    '@vuepress/plugin-back-to-top',
    '@vuepress/plugin-medium-zoom',
  ]
}
