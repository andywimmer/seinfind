---
layout: default
title: The Pilot
parent: Season 4
nav_order: 23
permalink: /the-pilot
cat: ep
series_ep: 63, 64
pc: 423, 424
season: 4
episode: 23, 24
aired: May 20, 1993
written: Larry David
directed: Tom Cherones
imdb: http://www.imdb.com/title/tt0697754/
wiki: https://en.wikipedia.org/wiki/The_Pilot_(Seinfeld)
---

# The Pilot

| Season 4 - Episode 23  | May 20, 1993             |
|:-----------------------|:-------------------------|
| Written by Larry David | Directed by Tom Cherones |
| Series Episode 63      | Production Code 423      |

"The Pilot" is the two-part season finale episode of the fourth season of Seinfeld. It makes up the 63rd and 64th episodes and first aired on May 20, 1993.

This two-part episode aired at an earlier time, 8:00 PM, to make room for the two-hour finale of Cheers, whose time slot would be claimed by Seinfeld at the start of the new fall season. Approximately 32 million people watched this episode (both parts) when it initially aired, and the show became a major ratings grab for its next five seasons.

## Plot

### Part 1

Jerry and George get the green light to produce Jerry, the pilot for the series based on their "nothing" lives. Russell Dalrymple, the president at NBC behind the pilot, is obsessed with Elaine. George is obsessed with a potentially cancerous white spot on his lip and a box of raisins taken by an actor playing Kramer, played by Larry Hankin (who had actually auditioned for the role of Kramer when Seinfeld began production).

The real Kramer has intestinal problems and, on his way to find a bathroom, he gets delayed by being mugged and "misses his chance" to resolve the problem, resulting in constipation. Jerry has an audition with the new "Elaine" (played by Elena Wohl), a method actress interested in being Elaine in every way, even going as far as dating Jerry (and breaking up with him in Part 2) and being called Elaine. The real Elaine has a problem with Monk's coffee shop, as they appear to be only hiring buxom waitresses, so she tries to get hired; when the owner turns her down, she files a report with the Equal Employment Opportunity office.

### Part 2

Rehearsals for the pilot begin. NBC executive Russell Dalrymple's obsession with Elaine begins to affect his work; she tries to let him down easy by saying she can't be in a relationship with a high-powered man and would prefer to be with someone selfless, such as a member of Greenpeace. Kramer resolves his constipation by administering himself an enema.

George mistakenly thinks that his white spot has been diagnosed as cancer and goes on a tirade at NBC, only to discover that he misunderstood the diagnosis. At the taping of the pilot, "Crazy" Joe Davola leaps out of the audience and onto the set while yelling "Sic semper tyrannis!" He's removed and the taping goes well. The pilot airs and numerous characters from past episodes comment on its accuracy ranging from:

* Susan Ross and her new girlfriend Allison from "The Smelly Car" and "The Outing". Both of them were surprised that Michael had George's behavior.

* Sidney Fields and his housekeeper from "The Old Man." Sidney starts to talk bad about Jerry by quoting "What kind of stupid show is this? Hey! It's that idiot that took all my records!" His housekeeper starts laughing.

* Marla Penny from "The Virgin" who is still in a relationship with John F. Kennedy, Jr. since "The Contest." When John (who is sitting at the edge of their bed) asks Marla if it's the same Seinfeld that she went out with, Marla states that he's "horrible." John then starts to say "Nevertheless..." before their scene cuts to the next scene.

* The Drake and the Drakette from "The Handicap Spot." The Drake states that he loves Jerry while the Drakette states that she hates Jerry while she adjusts the antennae.

* Ping from "The Virgin" and his cousin Cheryl from "The Visa." Ping quoted to Cheryl "I can't believe you were attracted to him" (in a reference to Jerry). Cheryl quoted "I thought he was dark and disturbed" and Ping quotes "Real perceptive."

* Donald Sanger and his parents from "The Bubble Boy." Mel Sanger and his wife work to keep their son from gaining the remote where Donald calls Jerry "a sellout."

* Jerry's parents Morty and Helen Seinfeld are laughing to this. Morty considers the show terrific while Helen asks "How can anyone not like him?"

* Calvin Klein from "The Pick" watching with Tia Van Camp from "The Airport." Calvin quotes "I like his style. He has a sort of casual elegance." Tia quotes "But he picks his nose." Calvin starts to say "Nevertheless..." before their scene was cut to the next scene.

* Sal Bass and Sidra Holland from "The Implant." Sal asks Sidra if that's the same Jerry from their health club. Sidra quotes "Yeah." Sal then quotes "You know that Kim Novak has some big breasts?" causing Sidra to look at him questionably.

* Newman is shown to have fallen asleep watching baseball.

In order to prove himself worthy of Elaine, Russell joins Greenpeace and is lost at sea during a botched assault on a whaling ship. His replacement at NBC named Rita Kearson, who disagreed with Russell on certain issues, dislikes the show and cancels it only two minutes after the episode finished and not knowing, let alone caring, about the ratings or reception. George and Jerry blame Elaine for driving Russell crazy in love and not returning his feelings; she tries to defend herself by saying it's not her fault, but George says it is because she's "very charming." Elaine then demands to know where Russell is, but according to Jerry, nobody knows.

In the end, Jerry, George, Kramer, and Elaine convalesce at Monk's, where it turns out many people have arrived due to the waitresses. Elaine spots the men from the Equal Employment Opportunity office eating there and scolds the owner of the cafe for only hiring large breasted women; the owner explains that they are all his daughters, and everything goes back to normal, with George muttering about getting a job the next day; Kramer recommends he join Greenpeace, to which George declines due to the risks.

The last scene is Russell still lost at sea in a Greenpeace boat with his shipmates (played by Larry Charles and Larry David). The cover for the Jerry Pilot script floats away at sea along with the former NBC president as one of Russell's shipmates vows to one day meet Elaine and tell her about Russell's actions in fighting the whalers.

## Cast

### Part 1

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Recurring

Bob Balaban .................. Russell Dalrymple  
Peter Crombie ............... "Crazy" Joe Davola

#### Guests

Anne Twomey .................... Rita  
Gina Hecht .......................... Dana Foley  
Jeremy Piven ...................... Michael Barth (TV George)  
Larry Hankin ..................... Tom Pepper (TV Kramer)  
Kevin Page ......................... Stu  
Mariska Hargitay ............... Melissa  
Laura Waterbury ............... Casting Director  
Elena Wohl ......................... Sandi Robbins (TV Elaine)  
Bruce Jarchow .................... Doctor  
Al Ruscio ............................. Manager  
Richard Gant ...................... Fred  
Peter Blood ......................... Jay  
Roger Rose ......................... Mark  
Samantha Dorman .............. Waitress  
Erick Avari ......................... Cabbie  
Bob Shaw ........................... Paul  
Stephen Burrows ............... David

### Part 2

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Recurring

Bob Balaban ............... Russell Dalrymple  
Peter Crombie ............ "Crazy" Joe Davola  
Heidi Swedberg ......... Susan Biddle Ross  
Barney Martin ............ Morty Seinfeld  
Liz Sheridan ............... Helen Seinfeld  
Wayne Knight ............ Newman

#### Guests

Anne Twomey ................... Rita  
Jeremy Piven ...................... Michael Barth (TV George)  
Larry Hankin ..................... Tom Pepper (TV Kramer)  
Kevin Page ......................... Stu  
Elena Wohl ......................... Sandi Robbins (TV Elaine)  
Al Ruscio ............................ Manager  
Richard Gant ...................... Fred  
Peter Blood ........................ Jay  
Brian Bradley ..................... Butler  
Bob Shaw ........................... Paul  
Deborah Swisher ............... 1st AD  
Jeff Oetjen .......................... Wilton  
Pat Hazell ........................... Himself  
Kari Coleman ..................... Allison  
Bill Erwin ........................... Sid  
Lanei Chapman ................. Housekeeper  
Jane Leeves ....................... Marla  
Unknown .......................... John-John (Kennedy)  
Rick Overton .................... The Drake  
Elizabeth Dennehy ........... Allison  
Maggie Han ..................... Cheryl  
Ping Wu ........................... Ping  
Brian Doyle-Murray ......... Mel  
Carol Mansell ................... Mother  
Jon Hayman ..................... Voice of Donald (The Bubble Boy)  
Jennifer Campbell ............ Tia  
Nicholas Hormann .......... Calvin Klein  
Teri Hatcher .................... Sidra  
Tony Amendola .............. Rushdie

## Script

[setting: night club]

JERRY: To me, the whole concept of fear of success is proof that we are definitely scraping the bottom of the fear barrel. Are we gonna have to have AA-type meetings for these people? They'll go: &quot;Hi, my name is Bill, and the one thing I'm worried about is to have a stereo and a cream-colored couch.&quot; According to most studies, people's number-one fear is public speaking. Number two is death. *Death* is number two! Now, this means to the average person, if you have to go to a funeral, you're better off in the casket than doing the eulogy.

[setting: Jerry's]

(Jerry is entering his apartment, following by Kramer)

KRAMER: Why can't I play Kramer?

JERRY: Look we've been through this already. You're not an actor!

KRAMER: Neither are you.

JERRY: I know. So why do we need two people in the show that can't act?

KRAMER: Oh come on Jerry. How hard is it to act. You say something, I'll pretend it's funny.

JERRY: My grandmother's in the hospital.

KRAMER: Ha ha ha. Your grandmother's in the hospital!

JERRY: This is real believable.

KRAMER: What you didn't think I was really laughing?

JERRY: It stinks.

KRAMER: Let me see you do it.

JERRY: Say something funny.

KRAMER: Alright. I've never been to Mars but I imagine it's quite lovely.

JERRY: Ah..........

KRAMER: Mine was better than that! Come on look. (starts to laugh again, Jerry too)

(George enters while they're both laughing)

GEORGE: Why are two pretending to be laughing?

JERRY: We're acting. (they stop laughing)

GEORGE: Oh, real good. (George makes a face like: you stink) Any word from NBC?

JERRY: No.

GEORGE: I don't understand. They're supposed to be casting this week. Something's wrong. Maybe they're not doing it.

KRAMER: (to Jerry) Well at least let me audition.

JERRY: (to George) He wants to play Kramer in the Pilot.

KRAMER: (to George) Yeah!

GEORGE: Out of the question.

KRAMER: Oughh!

GEORGE: (to Jerry) How could we not hear anything? What's with this Russel? What's he doing? (Jerry raises his arms and shoulders like: he doesn't know)

(scene ends)

[setting: Peter McManus cafe, an Italian restaurant]

(Elaine and Russell are sitting down at a table)

RUSSELL: I really appreciate you coming.

ELAINE: Oh, that's O.K. I don't have much time though. So...

RUSSELL: All right, first of all, I want to apologize for all the phone calls. It's just--It's just-- (awkward pause) I don't understand, we went out once...

ELAINE: That was two months ago.

RUSSELL: Yes I know. I just-- I can't get you out of my mind. Ever since that-- that day in the restaurant when we met... (we see a flashback from 'The Shoes' of Elaine showing her cleavage and asking Russell for his Ketchup secret)

ELAINE: Russell, you are the president of NBC. You can have any woman you want. (picks up the bowl of munchies on the table)

RUSSELL: But I want you.

ELAINE: God I hate these mixtures. Why don't they just put pretzels on the table. Even peanuts would be good, but I don't know how eats these cheesy things (she does).

RUSSELL: Is it something I said... or did?

ELAINE: Um... Look Russell... You're a very sweet guy. But I got to be honest with you. I don't like television... and that's your world. That's your life. I mean maybe if you were in... I don't know... Greenpeace or something, that would be different, but network television... I mean, come on, Russell, you're part of the problem.

RUSSELL: Oh Elaine, we're doing some really very interesting things right now. We've got some very exciting pilots for next season. We have one with a bright young comedian, Jerry Seinfeld.

ELAINE: Oh yeah, oh yeah. I've heard of him. He's that &quot;Did you ever notice this? Did you ever notice that?&quot; guy.

RUSSELL: Yeah. Anyway it's a ground breaking show.

ELAINE: Really? What is it about?

RUSSELL: (a little more enthusiast) Well, really, it's very unusual. It's about nothing.

ELAINE: (surprised) What do you mean it's about nothing?

RUSSELL: (starts doing George at the first meeting with NBC in 'The Pitch') For example, what did you do today?

ELAINE: Um, I got up. Um, I went to work. Then I came here.

RUSSELL: There's a show. that's a show.

ELAINE: Russell, see, I'm really not interested in this stuff and I do have to go to work (she gets up). So...

RUSSELL: (stops doing George, he's down again) Elaine, When--when--when are we gonna see each other again.

ELAINE: I'm sorry Russell. I'm sorry O.K.? Bye-bye. (Russell, still sitting watches her leaving).

(scene ends)

[setting: Jerry's]

(back to when we left earlier with Jerry, Kramer and George. Jerry is answering the phone while Kramer is about to go back into his apartment)

JERRY: Hello? Yeah he's here. (to Kramer) Hey! It's for you.

GEORGE: He's getting phone calls here now? (he's standing near the counter and eating chips out of a big bag)

(Kramer sits down and starts to talk on the phone)

JERRY: (to George) Again with the sweat pants?

GEORGE: What? I'm comfortable.

JERRY: You know the message you're sending out to the world with these sweat pants? You're telling the world: &quot;I give up. I can't compete in normal society. I'm miserable, so I might as well be comfortable.&quot; (George is baffled)

KRAMER: (to the phone) Hold on a second I got another call. Hello? Yeah, he'll call you back. (Jerry and George look at each other)

JERRY: (to Kramer) Who is it?

KRAMER: That's NBC.

JERRY: NBC!?! Give me the phone!

KRAMER: I'm in the middle of a conversation here.

JERRY: Get off the phone!

KRAMER: (to the phone) Look, I'll call you back. (hangs up)

JERRY: You know I'm waiting to hear from them. Who was it?

KRAMER: Russell Dalrymple's secretary.

JERRY: All right. Now you're doing something to help me. (to the phone) Hello yeah it's Jerry Seinfeld returning the call. Uh-huh.. O.K. great thanks a lot. (hangs up)(to George) Casting tomorrow at NBC. 4:00. We're in business baby, the pilot's on. You're gonna successful. (George looks disappointed)

(scene ends)

[setting: Dana's office]

GEORGE: What if the pilot gets picked up and it becomes a series?

DANA: That'd be wonderful George, you'll be rich and successful.

GEORGE: Yeah, that's exactly what I'm worried about. God would never let me be successful. He'd kill me first. He'd never let me be happy.

DANA: I thought you didn't believe in God?

GEORGE: I do for the bad things.

DANA: Do you hear what you're saying? God isn't out to get you George. What... What is that on your lip?

GEORGE: What?

DANA: It's like a discoloration. It's white.

GEORGE: (gets up and picks a mirror) Yes. Yes, it's white. Why it's white.

DANA: You'd better get that checked out.

GEORGE: Better get that checked out?

DANA: I would.

GEORGE: What kind of a therapist are you? I'm telling I'm scared that something terrible is gonna happen to me, right away you start looking for tumors?

DANA: I'm trying to help you.

GEORGE: What are you like a sadist? No matter how bad somebody feels, you can make 'em feel worse. I bet you're rooting for a tumor. (pointing to her)

DANA: I think you'd better go.

GEORGE: Oh I'm going baby! I'm going! (he leaves)

(scene ends)

[setting: Jerry and George in a cab at a light]

JERRY: Where?

GEORGE: Right here. (showing his lip)

JERRY: Get out of here, it's nothing. (Jerry knows George is hypochondriac. See 'The Heart Attack')

GEORGE: (to the cab driver) Excuse me, do you see anything on my lip here?

CABBIE: Yeah, it's like a discoloration.

GEORGE: Oh, my God.

CABBIE: Yeah, it's all white.

GEORGE: (to Jerry) It's all white Jerry! It's all white!

JERRY: Would you stop?

CABBIE: I would get that checked out if I were you.

GEORGE: Again with the checked out. I'm not going to the doctor. If I don't to the doctor, then nothing will happen to me. If I go he might find something.

JERRY: If you go, maybe they'll catch it in time.

GEORGE: Catch what in time?

JERRY: Whatever it is.

GEORGE: You think it's something?

CABBIE: Ah! I hate these bums with their filthy rags. No no no, I don't want it, get away, get away from my car (he starts his wipers)

JERRY: (to George) You know these squeegee-- Oh my God! It's crazy Joe Davola.

JOE DAVOLA: (through the opened window's cab) Good luck on the pilot Jerry. (the cab pulls away)

(scene ends)

[setting: NBC]

(Stu and Jay are sitting. George is standing in front of them, asking about his lip. The casting Director, a woman, is sitting across from them on a couch. Jerry is in the back, standing and looking through the window. There's a box of raisins on the coffee table.)

STU: (to George) Yeah I think I see it. It's like a white discoloration.

GEORGE: (to Jay) What do you think it is?

JAY: It's like a... white discoloration. (we understand now why a sitcom needs so many producers)

CASTING DIRECTOR: O.K. guys, are we ready to start?

JERRY: Yeah, where is Russell? I thought he was gonna be here.

(George sits down next to Stu and Jay, and grabs a magazine and the box of raisins. Jerry walks over to George and sits next to him.)

STU: Oh you know I don't know. I saw him in the hall this morning, I said hello to him. He walked right past me.

JAY: He must be worried about the fall schedule.

STU: Ah, it's a real bear.

GEORGE: Yeah. So what's going on? We're gonna shoot the pilot and then it's gonna be on TV the following week?

STU: Yeah. Right.

[The casting director enters with an actor]

CASTING DIRECTOR: This is Mark Matts. He'll be auditioning for the role of George. (the guy looks very cool and casual, and has a lot of hair)

MARK: Hey how you doing?

JERRY: (thinking) They've gotta be kidding.

GEORGE: (thinking) This guy's perfect.

CASTING DIRECTOR: O.K. Let's read this. I'll be reading Jerry's part.

MARK: Anyone call for Vandelay Industries? (George is the only one in the room to find Mark funny)

CASTING DIRECTOR: No. Why?

MARK: Listen to me. I told the unemployment office I was close to a job with Vandelay Industries and I gave them your phone number. So, when you answer the phone now, you've got to say: &quot;Vandelay Industries&quot;.

CASTING DIRECTOR: I'm Vandelay Industries?

MARK: Right.

CASTING DIRECTOR: What is that?

MARK: You're in latex.

CASTING DIRECTOR: What do I do with latex?

MARK: I don't know, you manufacture it.

(scene cuts)

[The casting director enters with another actor]

CASTING DIRECTOR: This is Michael Barth. Another George. (he's in sweat pants, bald, with glasses)

ALL: Hi Michael. How you doing?

JERRY: Everything all right?

MICHAEL: I just came from the podiatrist. I have a mole on my foot. I've got a little gangrene, they're probably gonna have to amputate. (everyone laugh except George)

CASTING DIRECTOR: Any questions?

MICHAEL: Yeah. What are we looking at here? Is this guy like a real loser?

GEORGE: No, not a loser!

CASTING DIRECTOR: Let's start with the second scene. You have it here?

MICHAEL: A man gave me a, you know, massage. (everyone laugh except George)

CASTING DIRECTOR: So?

MICHAEL: Well, he-- he had his hands, you know, and uh, he was, huh, ...

CASTING DIRECTOR: He was what?

MICHAEL: He was you know... he was touching and rubbing. (loud laughter)

CASTING DIRECTOR: That's a massage.

MICHAEL: I think it moved.

(scene cuts)

[The casting director enters with a beautiful actress]

CASTING DIRECTOR: This is Melissa Shannon.

MELISSA: Hi.

ALL: Hi. How you doing.

CASTING DIRECTOR: Melissa is reading for Elaine.

MELISSA: It's like a bald convention out there! (she saw George) Sorry. I, uh, made a faux pas.

JERRY: No you didn't. He knows he's bald.

MELISSA: So how about that guy wearing sweat pants? I mean did he do that for the part or does he walk around like that? (Jerry approves with a nod, George drops his notepad on the coffee table)

CASTING DIRECTOR: O.K. Shall we start? (Melissa and the casting director sit down)

JERRY: (getting up) Uh, you know what? I'll read with her.

MELISSA: Oh, great.

[The casting director leaves the chair to Jerry. Jerry sits in front of Melissa and slides the chair very close to her]

JERRY: Alright, want to start?

MELISSA: Yeah.

JERRY: O.K.

MELISSA: Ahem. What was that look?

JERRY: What look?

MELISSA: That look you just gave me?

JERRY: I gave a look?

MELISSA: Yes.

GEORGE: Thank you! Thank you very much. (Jerry and Melissa stop and look at George)

CASTING DIRECTOR: Let's see some more Kramers.

(We see many short scenes with a wide variety of fat, tall, or small actors opening the door like Kramer does. Finally enters Tom Pepper, the guy that will get the role o Kramer)

ALL: Hi. How you doing?

TOM: (to Jerry and very seriously) How you doing?

JERRY: (smiling and surprised at the way Tom is talking) Good.

(scene cuts)

(Tom and the casting director are into a scene)

CASTING DIRECTOR: What is this about?

TOM: (standing) Levels.

CASTING DIRECTOR: Levels?

TOM: Yeah. I'm getting rid of all, all my furniture. All of it! I'm building... levels... with steps... completely carpeted... (making the gesture of carpeting steps) with pillows. (everyone laugh. He sits down) Like Ancient Egypt.

CASTING DIRECTOR: I don't know how you're gonna be comfortable like that?

TOM: Oh! I'll be comfortable. (laughter, applause. He gets up, goes to the coffee table)

GEORGE: Very nice

JERRY: Very good

GEORGE: Very nice Tom, that was terrific.

TOM: May I? (pointing the box of raisins)

GEORGE: Sure. Thank you for coming in. (Tom eats some raisins)

JERRY: (to George) It was a wonderful reading.

GEORGE: Yeah. Really.

TOM: Well, bye.

GEORGE: Take care. Take it easy. (Tom leaves with the casting director)

STU: Now, I thought he was really good, very funny.

JERRY: Yeah, I liked him.

GEORGE: What happened to the raisins?

JAY: Yeah, there was a box of raisins there!

GEORGE: Did he just steal the raisins?

STU: You think he stole them?

CASTING DIRECTOR: (enters with the real Kramer) This is Martin Van Nostrand.

JERRY: (to Kramer) What are you doing here?

CASTING DIRECTOR: You two know each other?

STU: Wait a minute, I know you. You're the guy from the Calvin Klein underwear ads.

KRAMER: That's true.

(Jerry and George look at each other. They're gonna let Kramer have a shot at it. Kramer unfold the script and smile at Jerry and George, very confident.)

KRAMER: (acting very bad) I saw Joe DiMaggio in Dinky Doughnuts again, but this time, I went in. (pause, stops acting) Oh! Uh, where's the bathroom?

STU: I think if you go down the hall, it's on the right at the very end.

KRAMER: Yeah. Be right back. (Kramer leaves)

(We see Kramer, groaning and holding his stomach, running down the hall, and opening the bathroom's door. Someone in there says: &quot;Sorry buddy, full house.&quot; We then see Kramer outside leaving the building and running across the street to a restaurant: &quot;Sorry, customers only&quot; ...running into a movie theater: &quot;Hey you need a ticket!&quot; ...running through the park...)

(scene ends)

[setting: Monk's]

(Jerry and Elaine at a booth)

ELAINE: So who's playing Elaine?

JERRY: Oh, don't worry about it. Very talented, very talented young actress.

ELAINE: Really?

JERRY: Yes.

ELAINE: Who is it?

JERRY: She's an eskimo, actually.

ELAINE: Oh, my God (not in the mood to be kidding)

JERRY: She came down from Juno by sleigh, she was in the Iditarod. Got to the finish line, just kept going. She's got the dogs with her in the hotel room.

ELAINE: Listen, was Russell at the casting?

JERRY: No, he didn't show up.

ELAINE: You know, I'm a little bit worried about him. I don't understand. We had one date two months ago. Am I that charming and beautiful?

JERRY: No. No you're not.

ELAINE: Why do I keep setting you up?

JERRY: I don't know.

ELAINE: (to the waitress) Could we get a little more? (she doesn't listen and walks away) Aghh... You know ever since this new owner took over, the service here is *really* slow.

JERRY: Yeah. Have you noticed anything else that's different since the new management?

ELAINE: Mmm. They're putting a little lemon in the tuna. I love that.

JERRY: Beside that. Look at the waitresses.

ELAINE: Yeah? (we see that all the waitresses have big breasts)

JERRY: What physical characteristic would you say is common to all of them?

ELAINE: Ah...

JERRY: I mean look at this. Every waitress working here has the same proportions. Wouldn't you say?

ELAINE: Yes, I would say.

JERRY: What's going on here. How is that possible?

ELAINE: Do you think it's a coincidence?

JERRY: No. I haven't seen four women like this together outside of a Russ Meyer film.

(the waitress finally came with the coffee)

ELAINE: (to the waitress) Hi. Excuse me. Who does all the hiring waitresses here?

WAITRESS: He does. (pointing to the manager, Mr. Visaki) In fact we're looking for another girl if you know anyone. (she walks away)

ELAINE: You know what? That's discriminatory. That is unfair. Why should these women have all the advantages? It's not enough they get all the attention from men, they have to get all the waitress jobs, too?

JERRY: Hey that's life. Good-looking men have the same advantages. You don't see any handsome homeless.

(scene ends)

[setting: doctor's clinic]

GEORGE: You see, It's right here. It's all white...

DOCTOR: Oh yeah. Yeah. I've never seen this before.

GEORGE: You've never seen this before?

DOCTOR: I'm gonna have to take a biopsy on that. (George grabs the doctor's arm)

GEORGE: (dramatically) A what?

DOCTOR: A biopsy.

GEORGE: A biopsy?

DOCTOR: Yeah.

GEORGE: Cancer? Is it cancer? Do I have cancer?

DOCTOR: Well I don't know what it is.

(scene ends)

[setting: Jerry's]

GEORGE: A biopsy!

JERRY: What did he say?

GEORGE: He said he didn't know what it was.

JERRY: Alright. So?

GEORGE: When I asked him if it was cancer, he didn't give me a &quot;get outta here&quot;. That's what I wanted to hear: &quot;Cancer? Get outta here?&quot;

JERRY: Well, maybe he doesn't have a &quot;get outta here&quot; kind of personality.

GEORGE: How could you be a doctor and not say &quot;get outta here&quot;? It should be part of the training at medical school: &quot;Cancer? Get outta here!&quot; &quot;Go home! What are you crazy? It's a little test. It's nothing. You're a real nut. You know that?&quot; (Jerry gives him half of his sandwich to hopefully shut him up) I told you that God would never let me be successful. I never should've written that pilot. Now the show will be a big hit, we'll make millions of dollars, and I'll be dead. Dead Jerry. Because of this. (showing his lip)

JERRY: Can't you at least die with a little dignity?

GEORGE: No I can't. I can't die with dignity. I have no dignity. I want to be the one person who doesn't die with dignity. I live my whole life in shame. Why should I die with dignity?

(Kramer enters)

JERRY: Hey. What happened to you yesterday?

KRAMER: I got mugged.

GEORGE: You got mugged?

JERRY: Mugged?

KRAMER: Well, I wouldn't have minded it so much but I was running home to go to the bathroom.

JERRY: Why didn't you use the bathroom in the building?

KRAMER: It was full. I tried a few other places, you know, but that didn't work. I mean it was an emergency Jerry. I was really percolating... So I decided to run home through the park and then these two guys they stopped me and...

(door buzzer)

JERRY: Yeah?

ELAINE: It's me.

JERRY: Come on up.

KRAMER: But now I have a big problem, buddy.

JERRY: What is it?

KRAMER: Well, I waited so long I-- I missed my chance.

JERRY: You didn't go?

KRAMER: No. And now I can't get it back.

(George gives back the sandwich to Jerry and goes to the bathroom)

JERRY: The % thing to do is just not think about it.

KRAMER: How could you not think about it?

(Elaine enters)

ELAINE: Hey.

KRAMER: (mumbles and leaves)

ELAINE: What's the matter with him?

JERRY: He's a little backed up.

ELAINE: Oh...

(George gets back from the bathroom and takes back the sandwich from Jerry's hand and sit on the couch.)

GEORGE: Elaine.

ELAINE: So I spoke to some of my sisters about that coffee shop.

JERRY: Oh, the sisters (he sits at the table)

(Elaine goes into Jerry' bedroom)

GEORGE: (to Jerry) Have you seen the waitresses in there lately? I never had so much coffee in my life.

ELAINE: So we decided I should go over there and apply for a job myself.

GEORGE: Apply for a job? What for?

ELAINE: Because, it's discriminatory (she comes back wearing one of Jerry's shirts, untucked)

GEORGE: It's a coincidence.

JERRY: This is what you gonna wear?

ELAINE: Yeah.

JERRY: You're not gonna get the job.

ELAINE: Exactly.

(phone rings, Jerry gets up and answers, Elaine sits on the couch's arm next to George and takes a bite of his sandwich)

JERRY: (to the phone) Hello. Oh, hi. Yeah I guess we could do that. At what time? All right. I'll see you there. O.K., bye. (hangs up)

ELAINE: Who was it?

JERRY: TV Elaine. She wants to get together and talk about the part.

ELAINE: What about the dogs?

JERRY: They're having sex in the hotel room.

(scene ends)

[setting: Peter McManus cafe, same table as earlier]

(Jerry and TV Elaine: Sandi Robbins)

SANDI: So, the Elaine character is based on someone you know.

JERRY: Yes.

SANDI: And she's really your ex-girlfriend?

JERRY: Uh, Huh, yeah.

SANDI: I want to get to know her from the inside. What is she like? Tell me about her.

JERRY: Well, she's fascinated with Greenland. She enjoys teasing animals, banlon, and seeing people running for their lives. She loves throwing garbage out the window, yet she's extremely dainty.

SANDI: How would she eat a hamburger?

JERRY: With her hands.

SANDI: What about pasta?

JERRY: Also with her hands.

SANDI: Seriously... I want to experience everything she's experienced.

JERRY: Everything?

SANDI: Everything.

JERRY: All right she cuts her pasta with a knife.

SANDI: That's good. What's her favorite movie?

JERRY: Shaft.

SANDI: You got to get me a picture. What about sex?

JERRY: She likes talking during sex.

SANDI: Oh... dirty talking?

JERRY: No. Just chitchat, movies, current events, regular stuff. You know Sandi-- (looking at his watch)

SANDI: Elaine.

JERRY: What?

SANDI: Call me Elaine.

JERRY: All right. Elaine.

SANDI: How does Elaine kiss?

JERRY: Well--

SANDI: Does she kiss... like this? (she kisses Jerry)

JERRY: Actually she has a thing where she spirals her tongue around, it's like--

SANDI: Like this? (kisses again but with the spiral)

JERRY: I think you got it.

(scene ends)

[setting: Monk's]

(Kramer and Tom at a the booth behind the cashier)

KRAMER: I like to eat spaghetti with just a fork. Because I can keep the strands long, and I can slurp it out to my mouth. Like this look. (faking to slurp spaghetti) Now sex, I like the bottom. Let them do all the work. You should be writing this stuff down... (waitress comes to take the order) Bran lakes...100%. I got a big problem.

TOM: I'll have a hamburger. That's it.

KRAMER: Yeah, that's good. Oh, now I like to play golf.

TOM: This stuff doesn't matter to me. See, I'm gonna do the character like me, not like you.

KRAMER: You gotta play him like me. I'm Kramer.

TOM: I'm Kramer.

KRAMER: Whoa, I'm Kramer.

(scene cuts to Elaine who enters and walks to the manager)

Mr. VISAKI: (foreign accent) What can I do for you? Would you like a table.

ELAINE: No, I'd like to apply for a waitress job.

Mr. VISAKI: (looks Elaine up and down) Have you ever waited on tables before.

ELAINE: Oh yeah. I've been a professional waitress for the last 10 years. I've worked all over the city. These, uh, are my references. I'm sure you'll find that I'm more than qualified.

Mr. VISAKI: I don't think I need anyone else right now.

ELAINE: You're in big trouble mister. And I mean trouble with a capital 'T'. (she leaves)

Mr. VISAKI: What? What did I do?

(scene ends)

[setting: The Equal Employment Opportunity Commission Office]

ELAINE: Anyway there's at least four of them, and they're all huge. And one is bigger than the next. It's like a Russ Meyer movie.

FRED: Who's Russ Meyer?

ELAINE: Oh, he's this guy who made these terrible movies in the 70's with these kinds of women. He's obsessed. He's obsessed with breasts. That's hard to say.

FRED: Anyway, go on.

ELAINE: Um... Well, there's not really much more to tell. He was looking for waitresses, and I went in to apply for the job. And, he looked me up and down and he rejected me.

FRED: (to a guy in the hall at the water cooler machine) Paul. Come in for a second. I want you to listen to this.

PAUL: (to Elaine) Hi.

ELAINE: Hi.

FRED: Paul, woman here claims there's a restaurant on the West side that's only hiring large-breasted women.

PAUL: (to Elaine) Really?

[setting: NBC, pilot's set]

(Jerry, Tom, and Michael are at the counter, rehearsing. George is standing and watching them next to another guy. Rita, Jay, Stu and Russell are sitting in the crowd's bleachers)

TOM: What do you mean made up?

JERRY: It's made up. Haagen-Dazs is made up. It's not Danish.

TOM: You're crazy.

JERRY: No I'm not. (to Michael) George. Is Haagen-Dazs Danish?

MICHAEL: What do you mean Danish?

GEORGE: (to the guy next to him) This guy stinks. (speaking of Michael)

JERRY: Danish. Is it from Denmark?

MICHAEL: No, they make it in New Jersey. It's just a Danishy name.

TOM: I can't believe that. They fooled *me* Jerry.

RITA: (to Jay) Boy, talk about a show about nothing. (Jay, the integral producer, smiles stupidly)

GEORGE: Uh, excuse me. (stopping them from rehearsing) Excuse me. (he walks to the guy's in charge of yelling: &quot;take #!&quot;) This--This is not right. May I? (the guy looks at George with a bothered face. George then walks up to Tom and takes him away from Jerry and Michael to talk to him in private)(to Tom) You see, you're going: &quot;They fooled *me* Jerry!&quot; (George shakes his head with disapproval) You wanna hit 'fooled' more: &quot;They *fooled* me Jerry!&quot;. You see the difference?

TOM: I'm not gonna say it like that.

GEORGE: Just a suggestion. (chuckles and walks back to the yelling guy)

YELLING GUY: (with the same bothered face and while he's looking at George) All right everybody, take a five.

GEORGE: (very casual and raising his hand in the air) Yep. That's five!

JERRY: George? (walks away to talk privately. George, still casual, taps on Jerry's shoulder) I don't have a lot of experience with this acting stuff. But from what I can gather, they're a little touchy about being told how to say the lines.

GEORGE: Why is that?

JERRY: I don't know, but they don't seem to like it. By the way how am I doing?

GEORGE: Oh, you're fine... you're fine. (looking at Tom in the back and then quieter to Jerry) So you think this guy playing Kramer took the raisins?

JERRY: Why would he steal a box of raisins?

GEORGE: Yeah, it's bizarre. (they both look around them suspiciously)

(scene cuts to the bleachers with the producers)

RITA: (to Jay about Russell) What's with him? (to Russell) Russell? (louder) Russell?

RUSSELL: What?

RITA: You O.K.?

RUSSELL: Yeah. No, uh, I was just thinking of something. I'll be back in a second. (he gets up and leaves)

(scene cuts to Jerry sitting next to Sandi. They're both going through their copy of the script)

SANDI: What's the matter?

JERRY: Nothing.

SANDI: You're acting weird. Is anything wrong?

JERRY: No.

SANDI: Are you breaking up with me?

JERRY: Are we going out?

SANDI: You're breaking up with me, aren't you? (almost crying)

JERRY: Do you want me to break up with you?

SANDI: If that's what you want.

JERRY: I don't even know what you're talking about.

SANDI: Fine. Break up with me.

JERRY: All right. We're broken up.

SANDI: (little pause) Can we still be friends? (Jerry raises his head, staring ahead and wondering what's going on)

(scene cuts to George and Tom standing, backstage)

GEORGE: Remember when you came to audition for us?

TOM: Yeah.

GEORGE: There was a box of raisins on the coffee table. Did you, by any chance, take them with you when left?

TOM: What are you talking about?

GEORGE: Well we were all eating the raisins. And I remember you--you were eating some of the raisins. And then you left, and the raisins were gone. And I was just wondering if, you know (chuckles), maybe you took them with you.

TOM: Are you accusing me of stealing the raisins?

GEORGE: Oh, no, no--

TOM: (angry) Why would I steal a box of raisins!?

GEORGE: No you wouldn't. Nobody would. It's just that... they were missing, and... well I'm just inquiring. (chuckles nervously)

TOM: Let me give you a word of advice. O.K.? I want you to stay away from me. I don't wanna talk to you, and I don't wanna hear anymore of your stupid little notes and suggestions. I don't like you. So if you got any other problems whether it's raisins, prunes, figs, or any other dried fruit, just keep it to yourself and stay out of my way, O.K.?

GEORGE: Mm-hmm. Mm-hmm. All right. I don't think we're gonna have any problem with that. (chuckles nervously) Good talking to you Tom. Really.

(scene cuts to Russell, still on the set, but on the phone with Elaine)

RUSSELL: (nervously, almost desperately) Elaine. Elaine. What do you want? What can I do? Is it my job? Is that what it is? Elaine I can't go on like this. Will you call me? Would you call me? Well, why? All right. May I call you? Elaine? Elaine? (she hung up. An employee walks by, bumps into Russell and spills coffee accidentally on him)

DAVID: Excuse me Mr. Dalrymple. I am so sorry.

RUSSELL: All right. All right. What's your name?

DAVID: David Richardson.

RUSSELL: Get out! You're fired!

DAVID: But Mr. Dalrymple--

RUSSELL: Don't talk back to me. Didn't you hear what I say? Get out! You want me to call the cops? I make and break little worms like you every day. Do you know how much money I make? Do you have any idea! Do you know where I live? I can have any woman in this city that I want. Any one. Now, GET OUT! (David leaves. Everyone on the set is looking at Russell) What are you all looking at? Go back to work! BACK! NOW! (they do, Russell leaves)

(scene ends)

[setting: Jerry's]

GEORGE: The doc called and said the lab's backed up and now I'm not gonna get the results for another two days.

JERRY: Ah! You're fine. There's nothing wrong with you. I'm the one who's dying.

GEORGE: What do you mean?

JERRY: Because I can't act! I stink! I don't what I'm doing!

GEORGE: Come on you're... uh... you're fine.

JERRY: This show's gonna ruin my entire career. I don't know how I got involved in this.

GEORGE: What about me? I was a total failure. Everything was fine. Now this thing's gonna be a success and God's gonna give me a terminal disease.

JERRY: This actress playing Elaine, she's out of her mind.

GEORGE: The guy playing Kramer threatened me.

JERRY: Why?

GEORGE: 'cause I asked him about the raisins.

JERRY: You mentioned the raisins.

GEORGE: Oh yeah.

JERRY: Did he take 'em?

GEORGE: I don't know.

JERRY: Well if he didn't take 'em, what happened to 'em?

GEORGE: That's what I'm trying to find out.

(Kramer enters slowly and carefully)

JERRY: Hey.

KRAMER: Hey.

JERRY: Any luck?

KRAMER: No. No, nothing. I got no... peristalsis.

JERRY: What about bran?

KRAMER: I tried bran-- 40%, 50% 100%. The bran isn't working for me.

JERRY: Well my friend, (Jerry puts his hand on Kramer's shoulder) it may be time to consider the dreaded apparatus.

KRAMER: Pfft! Hold it right there. If you're suggesting what I think you're suggesting, you're wasting your time. I am not Jerry, under any circumstances, doing any inserting in that area.

JERRY: Oh, it's not that bad!

GEORGE: Yes it is.

(Elaine enters)

ELAINE: Well it's all taken care of. I filed a report. An investigation is underway.

JERRY: (to Elaine) So, you going to the taping tomorrow night?

ELAINE: No. I don't think I should go. I really don't wanna bump into Russell. He called me the other day. He won't quit.

JERRY: Oh, come on you gotta go! He's harmless. He's got a little crush on you.

ELAINE: Jerry, this is not a crush. This is a complete fixation. he makes me very uncomfortable.

JERRY: We need you there!

ELAINE: (to Kramer) Hey are you gonna go?

KRAMER: No. No. I'm gonna stay home. I want to be close to my home base in case there's any news from the front. (he leaves)

(scene ends)

[setting: NBC, pilot's set, the taping]

The taping is about to begin.

People are walking into the studio.

Jerry is getting a makeup.

Michael walks around backstage, he seems nervous and agitated.

Rita, Stu and Jay are sitting in the crowd. Russell is missing.

George looks at his lip in a mirror.

Tom is sitting in his dressing room, eating the raisins.

Elaine walks into the studio, wearing and adjusting a blonde short-haired wig, and also wearing bold glasses.

Scene cuts to the drugstore where Kramer is buying the dreaded apparatus.

Sandi, sitting next to Jerry who's still getting a makeup, is having her hair done.

SANDI: (to her hairdresser) No! Pick it up more in the front! It's got to be higher! Higher! Make a wall! A wall!

ASSISTANT DRESSER: Sandi, are you in wardrobe? Sandi?

JERRY: Try Elaine.

ASSISTANT DRESSER: Elaine?

SANDI: Yes?

(scene cuts to the crowd. Elaine is sitting in the front with her disguise, and a guy behind her taps on her shoulder)

WILTON: Elaine? It's me-- Wilton Marshall. Remember? Camp Tioga-- 1978? Remember?

ELAINE: Oh, right.

WILTON: Wow! You know you haven't changed a bit.

(scene cuts to Michael, backstage, still walking around nervously. He sees Jerry and runs to him)

MICHAEL: I can't remember my lines!!!

JERRY: Just relax, you'll be fine.

MICHAEL: I can't relax. I don't know what line! I don't know any of 'em!

JERRY: You're just like George. George'd do the same thing. You're just like him. It's amazing!

MICHAEL: Help me Jerry! Help me!

(scene cuts to the producers in the crowd)

RITA: (to Stu) Where is Russell?

STU: You know I don't know. I thought he was coming. I assumed he wouldn't miss it.

JAY: He hasn't been well.

STU: (to Rita) Can I tell you something in confidence? I think it's a woman.

RITA: How pathetic.

(scene cuts to George on the same phone as Russell earlier)

GEORGE: This is George Costanza, I'm calling for my test results. Negative? Oh, my God. WHY! WHY! WHY? What? What? Negative is good? Oh, yes of course! How stupid of me. Thank you. Thank you very much. (he hangs up)

(scene cuts to the crowd and we see Joe Davola is there)

(scene cuts to Kramer entering his apartment with the dreaded apparatus)

(scene cuts to George, happy, eating and double-dipping chips. Tom is staring at him, George notices him, and aborts a double-dip)

GEORGE: (he walks casually to Tom, and taps his arm) Listen. I know we've had our problems in the past, but we got a show to do tonight. Time to pull together as a team. Life's too short. I say, let's let bygones be bygones. If you took the raisins, if you didn't take the raisins-- They weren't even my raisins. I was just curious because it seems like a strange to do to walk into a room, audition, and to walk out with a box of raisins. Anyway, whatever. If you ever want to tell me about it, the door to my office is always opened. In the event that I get an office. You'll come in, we'll talk about the raisins. We'll have a nice laugh.

TOM: How would you like it if I just pulled your heart out of your chest right now, and shoved it down your throat?

(scene cuts to the presenter, Pat Hazell, talking to the crowd)

PAT HAZELL: Are you ready to meet our cast? (crowd applause) All right.

(scene cuts to Kramer holding and staring at the dreaded apparatus, then closing his bathroom's door.)

(scene cuts to Jerry, holding a microphone and talking to the crowd)

JERRY: Good evening, folks. How you doing? (small reaction from the crowd) Well, you sound like a great crowd. We have a show we're gonna put on for you tonight. It's a new TV show. It's what they call a pilot. And we hope it becomes a series. It's called 'Jerry', and I'm playing Jerry--

JOE DAVOLA: (getting up then shouting) SIC SEMPER TYRANNIS! (he jumps over a balcony and on the stage. The crowd is yelling)

(scene ends)

[setting: Jerry's]

GEORGE: Sic semper tyrannis? What is that, Latin?

JERRY: Yeah, it's what John Wilkes Booth yelled out when he shot Lincoln.

GEORGE: Really? What does it mean?

JERRY: It means: &quot;Death to tyrants&quot;.

GEORGE: I can see that.

(Elaine enters)

ELAINE: See, now this is exciting! This is exciting! Did I miss anything already?

JERRY: No, it starts in five minutes. You were there at the taping, what's the big deal?

ELAINE: Nah, now it's on TV. It's different. I told everybody I know to watch it.

GEORGE: Yeah, me too.

JERRY: Hey, what about Russell? Did you hear from him?

ELAINE: No.

JERRY: Strange. Even not showing up at the taping...

(Kramer enters, singing and dancing)

KRAMER: Hey, pistol-packin mama, you swing that gal around, Allemande left with the old gray hag, around and around you go. Yee-ha!!

JERRY: Well, well, well.

ELAINE: Congratulations.

KRAMER: Well, thank you.

GEORGE: You went for the big &quot;E&quot;.

KRAMER: Wet and wild.

JERRY: All right. Come on sit down. It's about to start.

KRAMER: Oh, yes.

(Elaine finds something under the couch cushions)

ELAINE: Hey, what's this? Look. A wallet.

JERRY: A wallet? Let me see that.

ELAINE: Here.

JERRY: Ah, man! It's my father's wallet! The one he thought they stole at the doctor's office that time.

GEORGE: Shh! This is it!

JERRY: How do you like that?

(The show begins. There are three different settings while the show is on TV. Each line or description will be preceded by the right setting:

[Jerry's] Jerry's apartment with Elaine, George and Kramer watching the pilot.

[TV] The pilot

[Viewers] Characters from the season 4, watching TV in their home and commenting on the pilot.)

===[TV]===

(Jerry's doing his stand-up routine at a comedy club. There's the music theme and we don't hear what he is saying, but the closed captions put that:

&quot;I get into a car accident. The guy that hit me doesn't have any insurance. So the judge sentences him to be my butler. Sounds like a sitcom, doesn't it?&quot;

We see the title 'Jerry', then, sitting at the comedy club, we see:

Micheal, Sandi, and Tom, and finally Jerry, and the four of them make a toast while it's written: &quot;Created by Jerry Seinfeld and George Costanza&quot;.

===[Jerry's]===

(they all applause as the intro ends)

ELAINE: Bravo!

(Kramer taps George on the shoulder)

GEORGE: You hurt me.

===[TV]===

(Jerry's apartment, Michael knocks and enters)

MICHAEL: Hey.

JERRY: Hey George.

MICHAEL: New sneakers?

JERRY: Yeah.

MICHAEL: What do you need new sneakers for?

JERRY: I like sneakers.

MICHAEL: How do you make a decision which one to wear? I'd go crazy if I have to decide which sneakers to wear every day.

JERRY: Nah, you're crazy anyway.

===[Viewers]====

***

(Susan and Allison from 'The Smelly Car'. They're eating popcorn, sitting on a couch)

SUSAN AND ALLSION: (to each other while they recognize one of George's behaviors in Michael) George!

***

(Sid and the housekeeper from 'The Old Man')

SID: What kind of stupid show is this? Hey! It's that idiot that took all my records! (the housekeeper starts laughing)

***

(John-John and Marla the virgin from 'The Contest'. Marla is under the bed sheets while John-John is sitting at the end of the bed, with the TV remote in his hands)

MARLA: John, what are you doing? Come back to bed.

JOHN: (with a Boston accent) This show looks interesting. Isn't he that Seinfeld fellow you went out with?

MARLA: Ooh, he's horrible! Horrible!

JOHN: Nevertheless...

***

(The Drake, who went back with the Drakette, Allison, from 'The Handicap Spot'. They watch the pilot on a tiny mini-TV with a tiny antenna)

THE DRAKE: Ah, that Jerry's a funny guy. Huh? Got to love the Sein!

ALLSION: Hate the Sein! (while she adjusts the tiny antenna)

***

(Ping and Cheryl from 'The Visa'. They talk in Chinese and we see sub-titles in English)

PING: I can't believe you liked him.

CHERYL: I thought he was dark and disturbed.

PING: Real perceptive.

***

(Donald and his parents from 'The Bubble boy'. We see his arm coming off the bubble, between his parents. He doesn't have the TV remote anymore. His parents are laughing)

DONALD: This is a piece of crap!

MOTHER: Donald, you used to like him.

DONALD: What a sellout! Give me that remote!

MEL: No, Donald.

(he grabs the remote from his father's hand and the three of them start to fight)

===[Jerry's]===

(Jerry is getting a soda from the refrigerator)

KRAMER: Come on Jerry, the commercials almost over.

JERRY: All right.

ELAINE: You know Jerry I really like this guy who's playing the butler.

JERRY: Oh yeah. He's good. You know he's John Ritter's cousin.

ELAINE: Really?

JERRY: Yeah.

===[TV]===

(doorbell, Jerry opens the door, it's the butler, Charles)

JERRY: Hello, Charles.

CHARLES: Hello. So, where do you want me to start today?

JERRY: Why don't you start in the bedroom?

CHARLES: (to himself, upset) Start in the bedroom...

(Tom enters)

TOM: Hey.

JERRY: Hey. The butler's here.

TOM: He is? Listen. When he's finished, send him over to my house.

JERRY: I'm not sending him to your house.

TOM: Why not?

JERRY: Because the judge decreed he'd become my butler, not my friend's butler.

TOM: Jerry, he is your butler. You can give him any order you want. That's what butlers do.

JERRY: But I don't want to.

KRAMER: Jerry, my house is a pigsty, come on.

(buzzer, Jerry presses the button)

JERRY: Yeah?

SANDI: (from the buzzer's speaker) It's Elaine.

JERRY: Come on up.

(The butler comes back from the bedroom with a can of Pledge)

CHARLES: I need more Pledge.

JERRY: More Pledge! I just bought two cans last week and I don't even have any wood in the house!

CHARLES: Well, it goes fast.

(Sandi enters)

SANDI: (to Charles, very friendly) Hello.

CHARLES: Hello. (he goes back in the bedroom)

JERRY: What's all this about?

SANDI: We had a date.

JERRY: You had a date? You went out with my butler? Who said you could go out with my butler?

SANDI: Why do I need your permission?

JERRY: Because he's my butler!

===[Viewers]===

***

(Morty and Helen laughing)

MORTY: That's terrific!

HELEN: How could anyone not like him?

***

(Calvin Klein with Tia from 'The Pick')

C.K.: I like his style. He has a sort of casual elegance.

TIA: But he picks his nose.

C.K.: Nevertheless...

***

(Sal Bass and Sidra from 'The Implant')

SAL BASS: He's a member of our health club. Isn't he?

SIDRA: Yeah...

SAL BASS: You know that Kim Novak has some big breasts?

***

(Newman sleeping and snoring in his chair while a baseball game is on TV)

===[TV]===

(final monologue at the comedy club)

JERRY: Ever notice a lot of butlers are named Jeeves?

(quick shot at ===[Jerry's]=== they all watch)

JERRY: You know I think when you name a baby Jeeves, you've pretty much mapped out his future, wouldn't you say? Not much chance is gonna be a hitman I think after that. (with a British accent) &quot;Terribly sorry Sir, but I'm going to have to whack you&quot;.

(end of the pilot and of the three different settings)

[setting: back to Jerry's]

ALL: (applauding and shaking hands) Wooh! Yeah!

ELAINE: Wow! That was great! That show was so funny. It was really funny. I'm not just saying that cause I know you. Honestly.

JERRY: Let's go out and celebrate! (they all get up)

ELAINE: That was so good.

JERRY: Come on let's eat something. (phone rings)

ELAINE: You know what I think this thing is gonna get picked up George. You guys are gonna be rich!

GEORGE: Do you really think so?

ELAINE: Oh yeah.

GEORGE: And God didn't kill me.

JERRY: (to the phone) Hello?

RITA: Hi Jerry, this is Rita Kierson.

JERRY: Oh, hi Rita.

RITA: I'm calling to let you know that Russell Dalrymple is no longer with this network.

JERRY: Oh, my God. Did he get fired?

RITA: To be honest with you. Nobody really knows. He seems to have disappeared.

JERRY: Russell's disappeared?

RITA: In any event, I've been made the new president of NBC. As you may or may not know, Russell and I did not see eye to eye on many, many projects. And as my first order of business, I'm, uh, passing on your show.

JERRY: You're passing already? But the show just ended two minutes ago!

RITA: Well, I just got the job. Goodbye, Jerry.

JERRY: Yeah, see ya. (he hangs up)

(Jerry and George stare at Elaine)

ELAINE: What-- What are you looking at me for?

GEORGE: It was you!

ELAINE: What did I do?

JERRY: Do you realize his obsession with you cost us a TV series?

ELAINE: I didn't know that he'd fall for me and I'd drive him insane. I mean, you know, that's not my fault.

GEORGE: Yes it is! You're very charming!

ELAINE: I can't believe this? What happened to him? Where the hell is he?

JERRY: No one knows.

(scene ends)

[setting: Greenpeace raft on the ocean, following a whaler]

(Russell with two other guys in the Greenpeace boat)

RUSSELL: She works for Pendant Publishing. She's the most beautiful woman I've ever seen. You know, I used to work for NBC, but when I go back to her this time, she'll respect me.

MAN ON RAFT: You'd better get down. They might start firing soon. (harpoon fires)

(scene ends)

&nbsp;

[setting: Monk's]

(The restaurant is full of men, some of them standing and waiting for a table. Jerry, Elaine, George and Kramer enters)

JERRY: Hey look at this. What is going on here?

GEORGE: Well, well, well.

ELAINE: Nothing has changed. How did this happen? (she sees the two guys of the Equal Employment Opportunity Commission at a table) Ah, these are the two guys I talked to at the Equal Employment Opportunity Commission. Hey! What are you two guys doing here? I thought you were gonna do something about this. Now you're eating here?

FRED: Oh no. That's why we're here. We're checking things out.

PAUL: Yeah, we're checking it out.

ELAINE: (to Paul) You're checking it out?

(a man is leaving the restaurant and walks by Fred and Paul's table)

MAN: (to Fred and Paul) See you back at the office, guys.

Mr. VISAKI: Fred, Paul, lunch and dinner? Boy, you guys ought to move in. How about a piece of pie on me? Sophia! Take care of these fellows.

ELAINE: (to the manager) Hey! Come here a second. I want you to know something. You are not gonna get away with this!

Mr. VISAKI: Get away with what?

ELAINE: Ah, &quot;with what?&quot; You know what. With the waitresses. How they're all... alike.

Mr. VISAKI: Of course they're alike. They're my daughters.

(they all show smiles of surprise)

ELAINE: (embarrassed, but smiling) Oh, your daughters.

GEORGE: You must be very proud Mr. Visaki. (shaking his hand) And may I say sir they're lovely girls, absolutely lovely girls. It's nice to see such fine upstanding women in gainful employment, Mr. Visaki.

Mr. VISAKI: Oh, here's a table for you.

GEORGE: A table right here.

Mr. VISAKI: Peggy!

GEORGE: Peggy! (they all sit) His daughter Peggy. Peggy's coming over to serve.

JERRY: What a family!

Mr. VISAKI: My daughter Peggy.

GEORGE: Ah! Peggy. Good to see you.

ELAINE: Hi Peggy.

GEORGE: Thank you very much. (Peggy leaves the menus and walks away) So guess what I got do tomorrow?

JERRY: What?

GEORGE: Start looking for a job.

KRAMER: You know what you ought to do George? You should work for Greenpeace. You those people they attack the whalers out on the open sea.

GEORGE: Are you crazy? You take your life in your hands with those nuts.

(scene ends)

&nbsp;

[setting: Greenpeace raft]

(Russell fell off the boat but still hangs on to a rope. The other man, still in the boat is screaming to him and holds the rope.)

MAN: Keep fighting matey! Get your head above the water! I've got you matey! I've got you! Matey! (he loses the rope) I'll remember her name! Elaine Benes! I'll write to her. I'll tell her all about you and what you did out here! Goodbye, matey! Goodbye!

(we see the script of the pilot 'Jerry' floating on the ocean)

The End
