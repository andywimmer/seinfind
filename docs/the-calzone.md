---
layout: default
title: The Calzone
parent: Season 7
nav_order: 20
permalink: /the-calzone
cat: ep
series_ep: 130
pc: 720
season: 7
episode: 20
aired: April 25, 1996
written: Alec Berg & Jeff Schaffer
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697669/
wiki: https://en.wikipedia.org/wiki/The_Calzone
---

# The Calzone

| Season 7 - Episode 20                | April 25, 1996            |
|:-------------------------------------|:--------------------------|
| Written by Alec Berg & Jeff Schaffer | Directed by Andy Ackerman |
| Series Episode 130                   | Production Code 720       |

"The Calzone" is the 130th episode of the NBC sitcom Seinfeld. This was the 20th episode of the seventh season. It aired on April 25, 1996.

## Plot

Steinbrenner becomes intrigued when he smells George's lunch during a meeting. George explains that it is an eggplant calzone and allows him to taste it. Steinbrenner then has George bring him a calzone for lunch every day. One day at the Paisano restaurant, George puts some money into the tip jar but realizes his gesture was unnoticed by the employee. George tries to fish out the money in order to replace it and get recognition for his tip. However, the Counter Guy (Peter Allas) turns and, under the impression that George is stealing money from the jar, refuses to sell him any more calzones and threatens him. In anxiety, George recommends to Steinbrenner that they try something new for lunch the next day. Steinbrenner, however, insists that they continue to keep their routine by stating once he likes something he sticks with it. George makes a deal to obtain the calzones from Newman, whose mail route brings him past Paisano's every day. Newman accepts, but demands his own calzone, a slice of pepperoni pizza, a large soda and three cannolis a week.

Meanwhile, Kramer is raving about wearing clothes "straight out of the dryer," because the warmth is comfortable; and Elaine's new friend, Todd Gack (John D'Aquino), is dating her without really ever asking her out. This puzzles her, and Jerry explains, "Because if he doesn't ask you out, he doesn't get rejected. He has found a dating loophole."

Jerry takes advantage of his beautiful girlfriend Nicki's (Dylan Tays) ability to get anything she wants, including convincing a cop to not give Jerry a speeding ticket. Todd Gack offers to sell Jerry some Cuban cigars, which Jerry thinks "might be a nice idea for George's wedding," but they turn out to be from Peru. Kramer starts using Jerry's oven to warm his clothing.

To George's frustration, however, Newman calls in sick and does not go to work because it is raining (something he always does, despite the postman's creed, which Newman claims he never cared for). In desperation, George asks Kramer to get him a calzone so that he can stay in Steinbrenner's good graces. Kramer agrees, but also, because he got wet in the rain, puts his clothes in the pizza oven at Paisano's, and they get burned. He tries to pay the calzone seller with pennies and an argument ensues; the Counter Guy kicks Kramer out of the restaurant. Kramer goes to George's office and drops his burnt clothes by a vent.

Jerry and Elaine lose Nicki and Todd to one another, when Nicki sees Todd about the cigars but he winds up wheedling another "non-date date" dinner with her. Jerry and Elaine are smoking the Peruvian cigars, which "smell like a rubber fire" and feels like they are "smoking a chicken bone"; when Elaine asks Jerry if he wound up paying for them, Jerry says, "It's being taken care of right now." The scene cuts to Kramer knocking on Todd's apartment door. When Todd answers, Kramer tosses him a large sack full of pennies with such force that it violently knocks him to the floor.

The calzone smell from Kramer's clothes wafts into Steinbrenner's office, and he runs to George's office, thinking he has calzones. Steinbrenner realizes the smell is from the clothes, and (like Kramer) thinks, "That's not a bad idea."

## Critical response

Linda S. Ghent, Professor in the Department of Economics at Eastern Illinois University, discusses this episode in terms of incentive and explains:

> George puts a dollar in the tip jar at the pizzeria, but the counterman's head was turned and he didn't see it. George laments that it cost him a dollar, but he got no credit for it. His altruism is not pure - he gets utility not from giving, but from getting credit for giving.

## Cast

#### Regulars

Jerry Seinfeld....................... Jerry Seinfeld  
Jason Alexander.................. George Costanza  
Julia Louis-Dreyfus............. Elaine Benes  
Michael Richards................. Cosmo Kramer

#### Recurring

Wayne Knight................. Newman

#### Guests

John D'Aquino..................... Todd  
Peter Allas............................ Counter Guy  
Danette Tays........................ Nicki  
Greg Collins.......................... Policeman  
Jane A. Johnston.................. Todd's Mother

## Script

[George in a meeting at Yankee Stadium]

GEORGE: I believe the doors on the bathroom stalls, here at the stadium, don't offer much by way of privacy. But I was thinking if we extend the doors all the way to the floors...

MR. STEINBRENNER: All the way to the floor! What are you crazy! You'd suffocate in there. Your lucky you have any doors at all. You know when I was in the army... Hey Costanza. What's that your eating over there? It looks pretty tasty.

GEORGE: It's a calzone, sir.

MR. STEINBRENNER: A calzone huh. Pass it down here. Let's have a look at at it. I want a little taste. Come on, come on. Pass it down here. That's a good boy. Okay. What's in this thing?

GEORGE: Uh. Cheese, pepperoni, eggplant.

MR. STEINBRENNER: Eggplant. Yes. That's a hell of a thing. Okay let's get back to business. Okay here you go. Very good, very good. Excellent. excellent calzone you got there Costanza. Okay a little jealous now. Okay lets go. Ok last week... You know that eggplant was very good. Everybody out. I got eggplant on my mind. Costanza get me couple of those calzones right now. Pronto. Move out. Pigstein what's an eggplant calzone. Must have one. Everybody out. Out.

[Jerry and Elaine at apartment]

ELAINE: One of those fabric wholesalers. This guy Todd Gack. I won a bet from him.

JERRY: What bet?

ELAINE: He bet me Dustin Hoffman was in Star Wars.

JERRY: Dustin Hoffman in Star Wars!?! Short Jewish guy against Darth Vader. I don't think so.

ELAINE: That's what I said.

JERRY: So the bet was that the loser has to buy dinner?

ELAINE: Yeah.

JERRY: Huh.

ELAINE: What?

JERRY: No. nothing.

(Kramer walks in)

JERRY: What's with you?

KRAMER: Feel this.

JERRY: Wow. That's hot.

KRAMER: Yeah. It's piquing hot. It's fresh out of the dryer. Hey Elaine you have to feel my pants.

ELAINE: I'll see you later.

KRAMER: Oh. Alright. You don't know what your missing. I'm loving this Jerry. I am never putting on another piece of clothing unless it's straight out of the dryer.

JERRY: So now every time you get dressed, you are going to go down to the basement and use the dryer?

KRAMER: Oh yeah. It's a warm and wonderful feeling, Jerry. So what are you doing later?

JERRY: I got a date with Nikki.

KRAMER: Oh yeah she's a beauty.

JERRY: She's also quite bold.

KRAMER: Oh bold and beautiful.

[George and Mr. Steinbrenner at Yankee Stadium]

MR. STEINBRENNER: I am loving this calzone. The pita pocket prevents it from dripping. The pita pocket. (phone rings) What is it Watson? A lost and found. No. I don't think we need that. If people keel over because they lost something that's there tough luck. You got a drip on your mouth by the way.

GEORGE: You know a lost and found could be a good idea.

MR. STEINBRENNER: Hold on Watson. You like lost and found George?

GEORGE: Definitely.

MR. STEINBRENNER: Alright lost and found. But these got to be a time limit. We're not running a pawn shop here.

[Jerry and Nikki at movies]

JERRY: Hey, Elaine.

ELAINE: Hi Jerry!

JERRY: This is Nikki.

NIKKI: Hi!

ELAINE: Hello. This is Todd Gack.

JERRY: Oh of course. Todd Gack. You did you bet was in Star Wars? Sammy Davis Jr.

ELAINE: So what movie are you guys seeing?

NIKKI: &quot;Means to an End&quot;

ELAINE: Oh. We were going to see that but it was sold out. So were going to see &quot;Blame it on the Rain&quot;.

JERRY: Why don't you see what you can do?

NIKKI: Okay.

ELAINE: What's she going to do? There's no more tickets.

JERRY: We'll see.

TODD: Hey Jerry. Do you like cigars?

JERRY: Yeah. Why?

TODD: I am going to Montreal tomorrow and they sell them dirt cheap.

JERRY: Hey, that might be a nice idea for George's wedding.

TODD: So do you want a box?

JERRY: Sure. If there cheap Why not.

TODD: Alright I buy a box and give to Elaine.

NIKKI: Okay two tickets &quot;Means to an End&quot;

JERRY: Told you.

ELAINE: How did you do that?

NIKKI: I just talked to the manager.

JERRY: Alright. Enjoy &quot;Blame it on the Rain&quot;?

[Jerry and George at the coffee shop]

GEORGE: They're putting in a lost and found because of me. There's a time limit but still.

JERRY: There really building a Utopian society up there huh? And you tribute all this to the calzone.

GEORGE: Yeah. I am like a drug dealer. I got the guy hooked. I am having lunch at his desk everyday this week. He doesn't make a move without me. It's very exciting.

JERRY: With you two guys at the helm. The last piece of the puzzle is in place.

GEORGE: So let me ask you a question about the tip jar. I had a little thing with the calzone guy this week. I go to drop a buck in the tip jar and just as I am about to drop it in he looks the other way. And then when I am leaving he gives me this look think thanks for nothing. I mean if they don't notice it what's the point.

JERRY: So you don't make it a habit of giving to the blind.

GEORGE: Not bills.

JERRY: So George. Remember when I told you Nikki gets whatever she wants. We're at the movies last night. It's sold out. Nikki goes and talks to the manager. Right in.

GEORGE: Beautiful women. You know they could get away with murder. You never she any of them lift anything over three pounds. They get whatever they want whenever they want it. You can't stop them.

JERRY: She's like a beautiful Godzilla.

GEORGE: Without thousands of fleeing Japanese.

[Jerry and Kramer at his apartment]

KRAMER: Hey buddy.

JERRY: What the hell is all this?

KRAMER: I'm looking for quarters for the dryer.

JERRY: Why can't you do this on your table?

KRAMER: Because I don't have a table.

(Elaine walks in)

ELAINE: Hey.

JERRY: Hey. So how was &quot;Blame it on the Rain?&quot;

ELAINE: Huh. Yeah thanks for getting us tickets too.

JERRY: Oh!! Let me ask you a question. Was the movie part of the bet?

(Kramer leaves)

ELAINE: No. We were both in the mood for one.

JERRY: You know Elaine, It is not my way to intrude on the personal lives of close fiends...

ELAINE: Oh is that so?

JERRY: Absolutely. But I feel I must inform you that what happened last night was more than a simple bet.

ELAINE: What are you talking about?

JERRY: Come on. Dustin Hoffman in Star Wars. He made a bet he knew he was going to lose just to take you to dinner

ELAINE: If he wanted to ask me out why didn't he just ask me.

JERRY: Because if he doesn't ask you out he doesn't get rejected. He's found a dating loophole.

ELAINE: I don't buy it.

(Kramer walks in with more change)

JERRY: So what happened after the movie?

ELAINE: Nothing. He walked me home.

JERRY: To the door?

ELAINE: Yeah.

JERRY: That's a date.

ELAINE: No it's not.

JERRY: But I never walk you home.

ELAINE: That's just because your a jackass.

KRAMER: Ah!! I found a quarter. Anybody want there clothes heated up?

JERRY: No, no.

ELAINE: No, no.

JERRY: So how did you leave it with him?

ELAINE: I'm supposed to meet him to pick up your cigars.

JERRY: That's another loophole. That's two dates without asking you out.

ELAINE: Your crazy!

JERRY: Crazy like a man.

[George at Pisano's]

WORKER: Number 49.

GEORGE: You know my last name is Costanza. That's Italian. So you and I are like country men. Pisano's!

WORKER: $6.50 your change.

GEORGE: And I always take care of my Pisano's. So here is a little something. (drop in tip and worker looks the other way, so George decides to take it out and try again only to get caught)

WORKER: Hey! You steal my money!!

GEORGE: No no. That's not what I was trying to do.

WORKER: I know what you try to do. Get out. Don't ever come back ever.

[George and Mr. Steinbrenner at Yankee Stadium]

GEORGE: I got your calzones Mr. Steinbrenner.

MR. STEINBRENNER: Beautiful. I am starving George.

GEORGE: I thought tomorrow maybe we'd try a little corn beef.

MR. STEINBRENNER: Corn beef. I don't think so. It's a little fatty.

GEORGE: How about Chinese?

MR. STEINBRENNER: Uhhhhh. No. Too many containers. Big mess, big mess. Too sloppy. I want to stick with the calzones from Pisano's. That's the ticket.

GEORGE: I just thought it would be nice. A little variety.

MR. STEINBRENNER: No, no, no. George let me tell you something. When I find something I like I stick with it. From 1973 to 1982 I ate the exact same lunch everyday. Turkey chili in a bowl made out of bread. Bread bowl George. First you eat the chili then you eat the bowl. There's nothing more satisfying than looking down after lunch and seeing nothing but a table.

[Todd walking Elaine to her apartment]

ELAINE: Thanks for the dinner.

TODD: Well I had to give these cigars and we were both hungry.

ELAINE: Hey Todd. Let me ask you a question. Um. Was this whole date thing just a way of asking me out?

TODD: What?

ELAINE: I mean Dustin Hoffman in Star Wars?

TODD: Elaine that was a legitimate bet and I lost so I bought you dinner.

ELAINE: Oh alright. Okay well, goodnight.

TODD: Hey, if your not doing anything Saturday do you want to meet somewhere?

ELAINE: See what is that? Is that a date?

TODD: Why can't two people go and do something without it being a date?

ELAINE: Alright. I'm sorry it's not a date.

TODD: No way. So I'll see you Saturday night?

ELAINE: Alright.

TODD: Pick you up at 8:00 p.m.

[Jerry and Nikki getting pulled over by a cop]

POLICE OFFICER: Do you know what the posted speed limit on this road is?

JERRY: It's got to be 55.

POLICE OFFICER: That's right it is. Do you know how fast you were going?

JERRY: A lot faster than that!

POLICE OFFICER: Step out of the car sir.

JERRY: Okey dokey.

POLICE OFFICER: Can I have your license and registration please?

JERRY: Absolutely. Nikki!

NIKKI: Yes.

JERRY: Would you mind bringing the officer the registration?

NIKKI: Not at all.

POLICE OFFICER: I got you on the radar at 93 miles per hour.

JERRY: You must have gotten me when I slowed down to take that curve because for a while there I was doing well over 100.

NIKKI: Officer. Hi. Do you really have to give us a ticket?

JERRY: Alright Nik. That's it.

[Kramer and Jerry at his apartment]

KRAMER: Hey buddy. I'm waiting for my shirt.

JERRY: You got your shirt in my oven!?!

KRAMER: I didn't have any quarters for the dryer. Anyway this is better. And it's more convenient.

JERRY: For both of us.

KRAMER: And I have a lot more control. I have one shirt going for 10 minutes at 325 degrees.

JERRY: What's wrong with your oven?

KRAMER: I am baking a pie!

(Buzz, buzz)

JERRY: Yeah.

GEORGE: Yeah.

JERRY: Come on up.

KRAMER: You got cigars, huh.

JERRY: I got some Cubans for George's wedding. They were more than I wanted to pay for but what the hell!

KRAMER: Oh yeah baby. spit, spit. What are these? &quot;Perducto de Peru&quot; Jerry, if you think these are Cubans you have another thing coming.

JERRY: Peru! I paid $300 bucks for these. I could have bought a house in Peru for $300 bucks!

KRAMER: You got ripped buddy.

JERRY: I got to pay this Todd Gack guy $300 bucks just so he has some excuse to see Elaine again without asking her out.

KRAMER: That's a nice name. Todd Gack. Is that Dutch? (Dingggggg) Oh baby. Here we go. Uh momma.(putting his fresh out of the oven shirt on) Hey George hey.

(Kramer leaves)

GEORGE: Well this is bad. I am really in a bad situation now.

JERRY: so what is Steinbrenner going to do if he doesn't get his calzones?

GEORGE: What's he going to do? That's exactly the point. Nobody knows what this guy is capable of! He fires people like it is a bodily function.

JERRY: Why don't you get someone else from the office to go get Pisano's for you?

GEORGE: Because before you know it he'll be having lunch with him. You know how these interoffice politics work.

JERRY: No. I never had a job.

(Kramer walks in with no pants on)

KRAMER: I decided to go with the brown ones. (pants)

GEORGE: What the hell is this?

JERRY: Kramer's cooking up some corduroy.

GEORGE: There has got to be some way to get back into Pisano's.

KRAMER: Pisano's. That's the place by the stadium right?

GEORGE: Yeah. You've heard of it?

KRAMER: Yeah. Newman raves about it. It's on his mail route. He goes by there everyday.

GEORGE: I'll see you guys later.

JERRY: What kind of pie are you cooking?

KRAMER: Huckleberry.

[George at Newman's apartment]

NEWMAN: You certainly are in a bind.

GEORGE: Yeah. And since you go buy there everyday. I was hoping that we could help each other out.

NEWMAN: Oh well. Let me perfectly blunt. I don't care for you Costanza. You hang out at the west side of the building with Seinfeld all day and just it up wasting your lives.

GEORGE: Are you going to help me or not?

NEWMAN: Alright, alright. I'll help you but I will except something in return.

GEORGE: What?

NEWMAN: Well for starters I want a calzone of my own...

GEORGE: Alright.

NEWMAN: And a slice of pepperoni pizza, and a large soda... and three times a week I will require a cannoli.

GEORGE: That's a little steep don't you think?

NEWMAN: You know I hear Mr. Steinbrenner can be a bit erratic. I would hate to see him when he's hungry.

GEORGE: Alright, alright.

NEWMAN: Do we have a deal?

GEORGE: But I have to have them by one o'clock. He's very regiment about his meals.

NEWMAN: I know exactly how he feels. Pleasure doing business with you. Do come again. Ha, ha, ha, ha.

[Todd and Elaine at a restaurant]

ELAINE: This is nice.

TODD: Gack. Party of four.

ELAINE: Party of four? Who are we meeting?

TODD: Mom! Dad! This is Elaine.

MOM: Hello.

ELAINE: Hellllllooooo.

(End of dinner)

MOM: Nice meeting you.

TODD: Bye mom.

MOM: She's wonderful.

ELAINE: What the hell was that?

TODD: What?

ELAINE: Why did you introduce me to your parents?

TODD: There nice people. I thought you would like them.

ELAINE: Come on Todd. Admit it, this is a date.

TODD: Why is this a date?

ELAINE: Saturday night with your parents. Unless I'm your sister this is a date.

TODD: Elaine. I don't understand why you can't meet someone else's parents without classifying it as a date.

ELAINE: Well if it's not a date then what is it?

TODD: It's a lovely evening together.

ELAINE: I don't believe this.

TODD: Well I am getting a cab want to join me?

ELAINE: No. I'll just walk home.

TODD: Okay goodnight. (goes to kiss her)

ELAINE: Now what was that?

[George at Newman's apartment]

(Knock, knock)

NEWMAN: Hello. What's this?

GEORGE: Well I was dropping of the calzone money for the week... Um shouldn't you be at work by now?

NEWMAN: Work? It's raining.

GEORGE: Soooooo?

NEWMAN: I called in sick. I don't work in the rain.

GEORGE: You don't work in the rain? Your a mailman. &quot;Neither rain nor sleet nor snow...&quot; It's the first one!

NEWMAN: I was never that big on creeds.

GEORGE: You were supposed to deliver my calzones. We had a deal!

NEWMAN: I believe the deal was that I get the calzones on my mail route. Well today I won't be going on my mail route! Will I? Perhaps tomorrow.

GEORGE: But I'm paying you!

NEWMAN: Yes thank you. (slams door)

GEORGE: Newman!!

[Nikki and Jerry at his apartment]

NIKKI: Peru? I thought you wanted cigars from Cuba?

JERRY: I did.

NIKKI: Well if these aren't what you wanted then why did you pay him?

JERRY: Well what could I do? Unless you pay him a visit.

NIKKI: Okay.

[George at Kramer's apartment]

(Knock, knock)

GEORGE: Kramer!

KRAMER: Hey you!

GEORGE: Look I need you to do me a favor. I need you to get me lunch at Pisano's.

KRAMER: What happened to Newman?

GEORGE: He called in sick.

KRAMER: Oh yeah right, it's raining.

GEORGE: Can you do it?

KRAMER: What time do you need it at?

GEORGE: 1:00 p.m. Do you need any money?

KRAMER: No. I got eight tons of change. I'm loaded.

[Kramer on street]

KRAMER: Hey hold that bus!

[Kramer at Pisano's]

KRAMER: Hey. It's really wet out there.

WORKER: What can I get you?

KRAMER: I hear you make a pretty mean calzone.

WORKER: Calzone!

KRAMER: Yeah calzone.

WORKER: The best!

KRAMER: Alright. Lay them on me. I'll take three.

WORKER: Three calzones.

KRAMER: Hey. That's a big oven. Huh. Listen. I was wondering if you could do me a favor.

[Elaine goes to the coffee shop]

ELAINE: Hey Todd.

TODD: Hi. You know Nikki.

ELAINE: Yeah sure.

NIKKI: Wait. Elaine will settle this. What's the &quot;M&quot; stand for in Richard M. Nixon?

ELAINE: Milhouse.

NIKKI: I told you so. He said it was Moe. You owe me a dinner.

[Kramer at Pisano's]

WORKER: Your order is ready. Three calzones and one shirt and jacket.

KRAMER: Oh. This is all burned up. Look at this.

WORKER: What the hell do I know about cooking a shirt? What the hell is this? You're paying in pennies?

KRAMER: That's all I got.

WORKER: No. You have to have bills. Paper money. You can't pay with this.

KRAMER: I told you this is all I got.

WORKER: Then no calzones.

(Worker and Kramer yelling at each other back and forth in Italian)

[George's office at Yankee Stadium]

GEORGE: What happened? Where have you been?

KRAMER: The guy wouldn't give them to me because I wanted to pay in change.

GEORGE: What the hell happened to your shirt?

KRAMER: He overcooked it. It's ruined.

GEORGE: Your clothes smell just like Pisano's. There's another Italian place on Jerome. Maybe I can fool him.

[Mr. Steinbrenner at his office]

MR. STEINBRENNER: (on phone) That's right. Do you want to say it again. I'll say it again. I hadn't had a pimple since I was eighteen and I don't care that you don't believe me or not. And how's this. Your fired. Okay your not. I am just a little hungry. Where's Costanza with my calzone. It's 1:15. He's late. That smell. I have to call you back. Costanza. He's in the building. Costanza is in the building and he's not in this office. Costanza! I'll get you.

[Jerry and Elaine at apartment]

JERRY: Stupid cigars. You know if I didn't send Nikki over to talk to him they wouldn't be together.

ELAINE: These are terrible.

JERRY: It's like trying to smoke a chicken bone.

ELAINE: What kind of a name is Todd Gack anyway.

JERRY: I think it's Dutch. I got to get going.

ELAINE: Where are you going?

JERRY: I... uh... promised Nikki that I'd walk her dog for her.

ELAINE: But she broke up with you.

JERRY: I know, I know. But some how she explained it to me and I couldn't say no.

ELAINE: It smells like a rubber fire.

JERRY: What's that?

ELAINE: I said rubber fire.

JERRY: Oh.

ELAINE: Did you ever pay Todd for these things?

JERRY: Actually it's being taken care of right now.

[Kramer at Todd's apartment]

(Knock, knock)

KRAMER: You Gack?

TODD: Yeah.

KRAMER: Here's your money.

(Kramer throws a bag full of change at him)

[Mr. Steinbrenner in George's office]

MR. STEINBRENNER: George. Why do these clothes smell like Pisano's?

GEORGE: Because they were heated up there.

MR. STEINBRENNER: Heating up your clothes? That's not a bad idea.

The End
