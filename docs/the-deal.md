---
layout: default
title: The Deal
parent: Season 2
nav_order: 9
permalink: /the-deal
cat: ep
series_ep: 14
pc: 213
season: 2
episode: 9
aired: May 2, 1991
written: Larry David
directed: Tom Cherones
imdb: http://www.imdb.com/title/tt0697682/
wiki: https://en.wikipedia.org/wiki/The_Deal_(Seinfeld)
---

# The Deal

| Season 2 - Episode 9   | May 2, 1991              |
|:-----------------------|:-------------------------|
| Written by Larry David | Directed by Tom Cherones |
| Series Episode 14      | Production Code 213      |

"The Deal" is the ninth episode of the second season of NBC's Seinfeld, and the show's 14th episode overall. The episode centers on protagonists Jerry (Jerry Seinfeld) and Elaine Benes (Julia Louis-Dreyfus) who decide to have a purely physical relationship, with a set of ground rules. However, as their "relationship" progresses, they experience difficulties maintaining their original friendship.

Series co-creator Larry David wrote the episode in a response to NBC's continued efforts to get the two characters back together. The main inspiration behind the episode was a similar agreement David once made with a woman. The episode, which introduced the character of Tina, Elaine's roommate, first aired on May 2, 1991 and was watched by approximately 22.6 million viewers. Critics reacted positively to the episode, and David received a Primetime Emmy Award nomination for Outstanding Writing for a Comedy Series.

## Plot

As they are watching TV in Jerry's apartment, Jerry and Elaine flip through the channels, stumbling upon the soft-core pornography channel. Upon the realization that neither of them has had sexual relations in a while, they start toying with the idea of sleeping together. They refer to their friendship as "this" and sexual intercourse as "that". However, as they do not wish to ruin their friendship, they establish a set of ground rules. Happy with their agreement, they make their way to the bedroom. The next day Jerry has lunch with his friend George Costanza (Jason Alexander), and makes him aware of his situation with Elaine. George remains skeptical, even after Jerry explains the rules system to him. He is proven right when Jerry and Elaine get into an argument over the second rule: "Spending the night is optional". Jerry eventually does not spend the night, leaving their agreement on shaky terms.

With Elaine's birthday coming up Jerry has to decide on what to get her. Since they are not in a relationship but they are still having sex he feels that the symbolism of the gift needs to be carefully thought out. He looks for a gift with George but is unable to think of anything, though he vaguely remembers her saying "something about a bench". Elaine is unhappy with the eventual gift: $182 cash. When Jerry's neighbor Kramer (Michael Richards) gives Elaine the bench she was looking for, for which she is much more grateful, she and Jerry talk over their agreement. They decide to start dating. When Kramer sees them again and asks what they are up to, Jerry notes that they now do "this, that and the other," "the other" being their romantic relationship as a couple.

## Production

Series co-creator Larry David wrote the episode, which was directed by Tom Cherones. Since the start of the show, NBC executives, especially Warren Littlefield, had been pressuring the writing staff to get Jerry and Elaine back together. Larry David had been against this idea from the start. However, brainstorming for an episode idea, he remembered he had once made a deal with a woman to have a purely physical relationship, which he thought "would make a really funny show, even if they had never [told us to get Jerry and Elaine back together]". Though Jerry and Elaine are still in a relationship at the end of the episode, they are no longer together by the end of the season. Seinfeld and David decided that they had satisfied the NBC executives and went back to the original format. Seinfeld and David have also noted that "The Deal" is the only Seinfeld episode ever to contain sincere emotions, during the scene in which Jerry and Elaine discuss the ending of their physical relationship.

On February 25, 1991, the table-read of the episode was held, subsequent filming occurred at CBS Studio Center in Studio City, Los Angeles, California three days later. "The Deal" is the first episode in which Elaine's apartment is shown. During rehearsals controversy arose over how Jerry and Elaine would sit during their "this and that" conversation. Several producers believed that, as the scene was intimate, the two should sit close together. David, however, believed the discussion was more of a transaction than an intimate scene and felt that Jerry and Elaine should sit further apart. On audio commentary recorded for the Seinfeld: Volume 1 DVD set, David commented that when he showed his idea of the scene, "I remember everybody saying 'there's no heat, there's no heat', and I said, that's the point, there's not supposed to be any". David and producer Andrew Scheinman got into a big argument over the issue, which David eventually won.

Aside from showing Elaine's apartment for the first time, "The Deal" also marks the first appearance of Elaine's roommate Tina, who had been mentioned in earlier episodes. Siobhan Fallon was cast in the role; she would reprise the character two more times, in season three's "The Truth" and in the season five finale "The Opposite". Norman Brenner, who worked as Richards' stand-in on the show for all its nine seasons, appears as an extra, working in the store George and Jerry visit to look for a gift for Elaine.

## Reception

"The Deal" was first broadcast on May 2, 1991 on NBC and received a Nielsen rating of 15.5 and an audience share of 25, indicating that 15.5 percent of American households watched the episode, and that 25 percent of all televisions in use at the time were tuned into it. With averagely 22.6 million homes watching the episode, the series was the eleventh most-watched show in the week it was broadcast, tied with NBC's The Golden Girls. David received a Primetime Emmy Award nomination for Outstanding Writing in a Comedy Series, but lost the award to Gary Dontzig and Steven Peterman, writers of the Murphy Brown episode "Jingle Hell, Jingle Hell, Jingle All the Way".

Critics reacted positively to the episode. Eric Kohanik of The Hamilton Spectator called "The Deal" a "hilarious episode". Entertainment Weekly critics Mike Flaherty and Mary Kaye Schilling commented "Jerry and Elaine's circuitous verbal dance pondering the relative worth of that [sex] versus this [the friendship] is sublime. The show's ability to be both explicit and vague will become a hallmark."

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Guests

Siobhan Fallon ..................... Tina  
Norman Brenner ................. Clerk

## Script

[Opening Monologue]

I've been watching women in the department stores. They're trying on clothes, and I've noticed that they do it differently from men. Women don't try on the clothes, they get behind the clothes, you see? They take a dress off the rack, and they hold it up against themselves. They can tell something from this. They stick one leg way out 'cause they need to know, if some day I'm one-legged, and at a forty-five degree angle, what am I gonna wear? You never see a man do that. You never see a guy take a suit off the rack, put his head in the neck, and go, &quot;What do you think about this suit? I think I'll get it. Yeah, it looks fine. Put some shoes by the bottom of the pants, I wanna make sure. Yeah, perfect. And what if I'm walking? Move the shoes, move the shoes, move the shoes, move the shoes.

[While Jerry and Elaine are sitting on Jerry's couch watching the tube, Elaine is flipping through the channels constantly.]

JERRY: What are you doing? Alright, alright. What's the matter with that? What about that one?

ELAINE: Robert Vaughn, The Helsinki Formula?

JERRY: He was good in Man From Uncle.

ELAINE: Guess whose birthday's comin' up soon?

JERRY: I know, I'm having my root canal the same week.

ELAINE: Oh, right. I hope you have a good oral surgeon because that can be very serious. (Changes channel) Hey, look at naked people.

JERRY: No, I don't wanna see the naked people.

ELAINE: Been a while?

JERRY: I have a vague recollection of doing something with someone, but it was a long, long time ago.

ELAINE: I think my last time was in Rochester. My hair was a lot shorter.

JERRY: I remember that it's a good thing. Someday, I hope to do it again. (Jerry looks at Elaine)

ELAINE: What?

JERRY: What?

ELAINE: What was that look?

JERRY: What look?

ELAINE: The look you just gave me.

JERRY: I gave a look?

ELAINE: Yes.

JERRY: What kind of look?

ELAINE: I know that look.

JERRY: Then what was it?

ELAINE: Why should I tell you?

JERRY: Well, you're the big look expert. I wanna see how smart you are.

ELAINE: Trust me. I know the look. (Pause) So...

JERRY: What?

ELAINE: What about the look?

JERRY: I don't know.

ELAINE: You got something on your mind?

JERRY: No. Things pop into your head. You?

ELAINE: Things occur to me from time to time.

JERRY: Yeah, me too. Well, you can't expect to just forget the past completely.

ELAINE: No, of course not.

JERRY: I mean, it was something we did. Probably about, what? Twenty-five times?

ELAINE: Thirty-seven.

JERRY: Yeah, we pretty much know what we're doin' in there. (Points to bedroom)

ELAINE: We know the terrain.

JERRY: No big surprises.

ELAINE: Nope.

JERRY: What do you think?

ELAINE: I don't know. What do you think?

JERRY: Well, it's something to consider.

ELAINE: Yeah.

JERRY: I mean, let's say we did.

ELAINE: What if.

JERRY: Is that like the end of the world or something?

ELAINE: Certainly not.

JERRY: Why shouldn't we be able to do that once in a while if we want to?

ELAINE: I know.

JERRY: I mean, really, what is the big deal? We go in there. (Points to the bedroom) We're in there for a while. We come right back out here. It's not complicated.

ELAINE: It's almost stupid if we didn't.

JERRY: It's moronic.

ELAINE: Absurd!

JERRY: Of course, I guess, maybe, some little problems could arise.

ELAINE: We, there are always a few.

JERRY: I mean, if anything happened, and we couldn't be friends the way we are now, that would be really bad.

ELAINE: Devastating.

JERRY: Because this is very good. (Points back and forth between them to indicate friendship)

ELAINE: And that would be good. (Points to bedroom)

JERRY: That would be good too. The idea is combine the this and the that. But this cannot be disturbed.

ELAINE: Yeah, we just wanna take this and add that.

JERRY: But of course, we'd have to figure out a way to avoid the things that cause the little problems. Maybe some rules or something.

ELAINE: Huh.

JERRY: For example, now, I call you whenever I'm inclined and vice versa.

ELAINE: Right.

JERRY: But if we did that, we might feel a certain obligation to call.

ELAINE: Well why should that be? Oh, I have an idea. I have an idea. No call the day after that.

JERRY: Beautiful. Let's make it a rule.

ELAINE: Alright, sir.

JERRY: Now here's another little rule.

ELAINE: Yeah.

JERRY: When we see each other now, we retire to our separate quarters. But sometimes, when people get involved with that, they feel pressure to sleep over. When that is not really sleep. Sleep is separate from that. And I don't see why sleep got all tied up and connected with that.

ELAINE: Okay, okay. Rule Number Two. Spending the night is optional!

JERRY: Well now we're gettin' somewhere.

ELAINE: What about the kiss goodnight?

JERRY: Tough one. You're call.

ELAINE: It's brug-wa (?).

JERRY: Fine. Well.

ELAINE: Well.

JERRY: You ready?

ELAINE: Ready.

JERRY: So think you can handle this?

ELAINE: Definitely. (Runs into bookshelf)

[Jerry's apartment the next day. Kramer enters.]

KRAMER: Hey.

JERRY: Hey.

KRAMER: Got the paper?

JERRY: Not yet.

KRAMER: No paper?

JERRY: I haven't been out yet.

KRAMER: Well, what's taking you so long? (Elaine enters from the bedroom. Kramer is a little shocked) Uh? Oh, well, yeah... (He exits)

[George and Jerry at Monk's.]

GEORGE: What's the deal with Aquaman? Could he go on land, or was he just restricted to water?

JERRY: No, I think I saw him on land a couple times. So how's the job situation goin'?

GEORGE: Still lookin'. It's pretty bad out there. What about you?

JERRY: Nothin' much. I slept with Elaine last night.

GEORGE: Oxygen! I need some oxygen! This is major.

JERRY: I thought you'd like that.

GEORGE: Oh, this is huge!

JERRY: I know.

GEORGE: Alright, okay. Let's go, details.

JERRY: No, I can't do details.

GEORGE: You wha?

JERRY: I can't give details.

GEORGE: No details?

JERRY: I'm not in the mood.

GEORGE: You ask me here to have lunch, tell me you slept with Elaine, and then say you're not in the mood for details. Now you listen to me. I want details and I want them right now. I don't have a job, I have no place to go. You're not in the mood? Well you get in the mood!

JERRY: Alright, okay. We're in the apartment watching TV.

GEORGE: Where are you sitting?

JERRY: On the couch.

GEORGE: Next to each other?

JERRY: No, separated.

GEORGE: Time?

JERRY: Around eleven.

GEORGE: Okay, go ahead.

JERRY: So she's flipping around the TV, and she gets to the naked station.

GEORGE: Oh, see? that's why I don't have cable in my house. Because of that naked station. If I had that in my house, I would never turn it off. I wouldn't sleep, I wouldn't eat. Eventually, firemen would have to break through the door, they'd find me sitting there in my pajamas with drool coming down my face. Alright, alright. So you're watching the naked station.

JERRY: And then, somehow, we started talking about, what if we had sex.

GEORGE: Boy, these are really bad details.

JERRY: It pains me to say this, but I may be getting to mature for details.

GEORGE: Oh I hate to hear this. That kind of growth really irritates me.

JERRY: Well. I'll tell you though. It was really passionate.

GEORGE: Better than before?

JERRY: She must've taken some kind of seminar or something.

GEORGE: This is all too much. So what are you feeling? What's going on? Are you like a couple again now?

JERRY: Not exactly.

GEORGE: Not exactly. What does that mean?

JERRY: Well, we've tried to arrange a situation where we'll be able to do this once in a while and still be friends. (George laughs hysterically and stands out of his seat)

GEORGE: Where are you living? Are you here? Are you on this planet? It's impossible. It can't be done.

(He sits back down) Thousands of years people have been trying to have their cake and eat it too. So all of a sudden the two of you are going to come along and do it. Where do you get the ego? No one can do it. It can't be done.

JERRY: I think we've worked out a system.

GEORGE: Oh, you know what you're like? You're like a pathetic gambler. You're one of those losers in Las Vegas who keeps thinking he's gonna come up with a way to win at blackjack.

JERRY: No, this is very advanced. We've designed at set of rules that we can maintain the friendship by avoiding all of the relationship pitfalls.

GEORGE: Sure, alright. Tell me the rules.

JERRY: Okay. No calls the next day.

GEORGE: (To himself) So you're havin' the sex, next day you don't have to call. That's pretty good. (Back to Jerry) Go ahead.

JERRY: You ready for the second one?

GEORGE: I have tell you, I'm pretty impressed with the first one.

JERRY: Spending the night. Optional.

GEORGE: No, you see? You got greedy.

JERRY: No, that's the rule. It's optional.

GEORGE: I know less about women than anyone in the world. But one thing I do know is they're not happy if you don't spend the night. It could be a hot, sweaty room with no air conditioning and all they have is a little army cot this wide (Displays with French fry) You're not going anywhere.

JERRY: I think you're wrong.

GEORGE: I hope I am.

[At Elaine's apartment, Jerry opens the fridge and pulls out a piece of cake.]

JERRY: Is this yours or the roommate's?

ELAINE: The roommate's.

JERRY: Would she mind?

ELAINE: She keeps track of everything.

JERRY: Well, that's too bad, 'cause I'm takin' it.

ELAINE: Thanks.

JERRY: Well, guess I'll get going.

ELAINE: Oh.

JERRY: Well, I got that root canal tomorrow morning. It'll be easier if I go home.

ELAINE: Fine, go away.

JERRY: I don't understand. Is there a problem? (Elaine is pulling a roll of paper towels about twenty feet long) I'm getting the impression there's a problem.

ELAINE: Just go.

JERRY: I'm having surgery tomorrow.

ELAINE: Oh, surgery. You're going to the dentist.

JERRY: But you said, it can be very serious.

ELAINE: Okay, so fine. Go.

JERRY: What happened to the rules? Remember? Sleeping over was optional.

ELAINE: Yeah, it's my house, it's my option.

JERRY: It has nothing to do with whose house it is.

ELAINE: Oh, of course it does. (Elaine's roommate, Tina, enters)

Tina: Hi.

Elaine + JERRY: Hi.

Tina: Hi, Jerry.

JERRY: Hi.

Tina: Such a great improv class tonight.

ELAINE: Oh really?

Tina: I had this improv where I pretended I was working in one of those booths. You know, in the amusement park, where you have to shoot the water in the clown's mouth and you have to blow up the balloon.

ELAINE: Uh, Tina? Could you excuse us for just one second?

Tina: Oh, yeah. I'll excuse you. (She walks away)

ELAINE: What are you doing?

JERRY: I can't go if you're mad.

ELAINE: I'm not mad.

JERRY: You seemed a little mad.

ELAINE: No, no. Jerry, I'm fine really. It's okay.

JERRY: So you're okay with everything?

ELAINE: Definitely. Are you?

JERRY: Definitely. Well, goodnight.

ELAINE: Goodn-- (He starts to kiss her) What're you doing?

JERRY: What?

ELAINE: Rules.

Tina: Hey, who took my cake? (Jerry exits quickly)

[Jerry and George are at the store.]

GEORGE: What about jewelry? That's very nice gift.

JERRY: No, no. I have to be very careful here. I don't want to send the wrong message. Especially after the other night.

GEORGE: Maybe I'll get her some jewelry.

JERRY: No, no. You can't get her anything better than me. Whatever I spend, you have to spend half.

GEORGE: What am I supposed to get, a bazooka?

JERRY: You don't understand. I'm in a very delicate position. Whatever I give her, she's going to be bringing in experts from all over the country to interpret the meaning behind it.

GEORGE: What does she need? Maybe there's something that she needs.

JERRY: I think I heard her say something about a bench.

GEORGE: A bench? What kind of a bench?

JERRY: I don't know, but she mentioned a bench.

GEORGE: What, like at a bus stop?

JERRY: I don't know.

GEORGE: Like a park bench?

JERRY: I have no idea.

GEORGE: Who puts a bench in their house?

JERRY: Forget the bench.

GEORGE: I got it. You wanna get her something nice? How 'bout a music box?

JERRY: No, too relationshippy. She opens it up, she hears that Laura's theme, I'm dead.

GEORGE: Okay, what about a nice frame? With a picture of another guy in it. Frame says I care for you, but if you wanna get serious, perhaps you'd be interested in someone like this.

JERRY: Nice looking fellow.

GEORGE: What about candle holders?

JERRY: Too romantic.

GEORGE: Lingerie?

JERRY: Too sexual.

GEORGE: Waffle maker.

JERRY: Too domestic.

GEORGE: Bust of Nelson Rockefeller.

JERRY: Too Gubernatorial.

GEORGE: Let's work on the card.

[Jerry and Elaine sitting on his couch. Elaine holding a present.]

JERRY: Maybe you won't like it.

ELAINE: Oh, how could I not like it? Of course I'll like it.

JERRY: You could not like it.

ELAINE: Just the fact that you remembered means everything.

JERRY: Of course I remembered. You reminded me everyday for two months. Oh, the card. (She opens)

ELAINE: Cash?

JERRY: Would do you think?

ELAINE: You got me cash?

JERRY: Well this way I figure you can go out and get yourself whatever you want. No good?

ELAINE: Who are you, my uncle?

JERRY: Well come on. That's $182 right there. I don't think that's anything to sneeze at.

ELAINE: Let me see the card. (Reading) To a wonderful girl, a great pal, and more? (Kramer enters)

KRAMER: Hey. Oh, Elaine. I'm glad you're here. Stay right there. I'm gonna be right back. (He exits)

ELAINE: Pal? You think I'm your pal?

JERRY: I said, &quot;and more.&quot;

ELAINE: I am not your pal.

JERRY: What's wrong with pal? Why is everyone so down on pal? (Kramer enters with present)

ELAINE: Oh, what is this? You got me something?

KRAMER: Yeah. Open it.

ELAINE: Oh Kramer... (She opens it) The bench! You got me the bench that I wanted! (Jerry looks irritated)

KRAMER: That's pretty good, huh?

JERRY: Great.

KRAMER: Remember when we were standing there and she mentioned it? I made a mental note of it.

JERRY: Well goody for you.

KRAMER: Oh yeah, I'm very sensitive about that. I mean, when someone's birthday comes up, I keep my ears open. So what'd you get her?

JERRY: 182 bucks.

KRAMER: Cash? You gotta be kidding. What kind of gift is that? That's like something her uncle would get her.

ELAINE: (Reading card) Think where man's glory most begins and ends and say my glory was I had such a friend.

KRAMER: (To Jerry) Yates.

ELAINE: Oh Kramer. (They embrace)

JERRY: Could you excuse us please?

KRAMER: What?

JERRY: We're talking.

KRAMER: Oh, the relationship. (He leaves)

JERRY: You know, we never had one fight before this deal.

ELAINE: I know.

JERRY: Never.

ELAINE: Ever.

JERRY: We got along beautifully.

ELAINE: Like clams.

JERRY: It was wonderful.

ELAINE: A pleasure.

JERRY: So I think we should just forget the whole deal, and go back to being friends.

ELAINE: I can't do it.

JERRY: You what?

ELAINE: I can't do that.

JERRY: You mean it's... (She nods) No this. No that. No this or that. Oh, boy. Hmmm. What do you want?

ELAINE: This, that, and the other.

JERRY: Oh, sure. Of course, you're entitled. Who doesn't want this, that, and the other?

ELAINE: You.

JERRY: (Starts to correct then realizes) Well...

[Jerry and George at Monk's.]

GEORGE: Those birthdays. I told you. They're relationship killers. If a relationship is having any problems whatsoever, a birthday will always bring it out.

JERRY: I never should have made up those rules.

GEORGE: What is it about sex that just disrupts everything? Is it the touching? Is it the nudity?

JERRY: It can't be the nudity. I never got into these terrible fights and misunderstandings when I was changing before gym class.

GEORGE: You know what this means? I can't see her anymore either.

JERRY: Why?

GEORGE: It's break up by association. Besides, she's mad at me anyway because of my birthday present.

JERRY: What did you end up giving her?

GEORGE: 91 dollars.

JERRY: Sorry about that.

GEORGE: So what're you gonna do?

JERRY: Well, if I call her, there's no joking around anymore. This is pretty much it.

GEORGE: So, maybe this should be it.

JERRY: Could be it.

GEORGE: She seems like an it.

JERRY: She's at it as you get. Imagine bumping into her on the street in five years with a husband. And she tells me he's a sculptor, they live in Vermont...

GEORGE: We'd have to kill him.

JERRY: We'd get caught, I'd get the chair.

GEORGE: I'd go to prison as your accomplice. I'd have to wear that really heavy denim. Go to the cafeteria line with the guy who slops those mashed potatoes onto your plate. Go to the bathroom in front of hundreds of people.

JERRY: Plus, you know what else.

GEORGE: You better call her.

[Jerry's apartment, Kramer enters.]

KRAMER: Hey.

JERRY: Hey.

KRAMER: You got the paper yet?

JERRY: Yeah.

KRAMER: Well where is it? (Elaine enters from bedroom with newspaper) Hey, you done with that?

ELAINE: No.

KRAMER: Well, you're not reading it now.

ELAINE: Alright, you can take it. But I want it back.

KRAMER: Oh yeah. So, ah, what're you guys gonna do today?

ELAINE: Ah, this. And that.

JERRY: And the other.

KRAMER: Boy, I really liked the two of you much better when you weren't a couple. (He exits)

The End
