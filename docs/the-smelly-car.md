---
layout: default
title: The Smelly Car
parent: Season 4
nav_order: 21
permalink: /the-smelly-car
cat: ep
series_ep: 61
pc: 422
season: 4
episode: 21
aired: April 15, 1993
written: Larry David & Peter Mehlman
directed: Tom Cherones
imdb: http://www.imdb.com/title/tt0697778/
wiki: https://en.wikipedia.org/wiki/The_Smelly_Car
---

# The Smelly Car

| Season 4 - Episode 21                  | April 15, 1993           |
|:---------------------------------------|:-------------------------|
| Written by Larry David & Peter Mehlman | Directed by Tom Cherones |
| Series Episode 61                      | Production Code 422      |

"The Smelly Car" is the 61st episode of the sitcom Seinfeld. The episode was the 21st episode of the fourth season. It aired on April 15, 1993.

## Plot

After dinner, Jerry and Elaine discover a strong smell of body odor in Jerry's BMW 5 Series, assumed to have been left by a valet who was tasked with parking it. After they endure an unpleasant drive to the home of Elaine's boyfriend Carl (Nick Bakay), Carl is offended by the smell of Elaine's hair when they embrace. He tells her he has to get up early, so she does not spend the night.

George and Kramer return a video; at the video store, he admires two women who are holding hands. George is astounded when they turn around and one is his ex-girlfriend, Susan, now revealed to be bisexual. Meanwhile, in the background of this conversation, Kramer is practicing his golf swing with a broom in front of Susan's girlfriend, Mona, a golf instructor. She is clearly amused by him. George returns the video (Rochelle, Rochelle), but the clerk tells him that he has to pay a $2 fee because he didn't rewind the tape. George is upset, but Kramer tells him that it would be cheaper to keep the video and return it the next day.

At Jerry's apartment, after Kramer enters, he comments on their body odor. Jerry blames the car's stink on the valet at the restaurant and is determined to demand that the restaurant share the cost of cleaning. Elaine realizes that the odor is the reason Carl didn't spend the night with her. The restaurant maître d' (Michael Des Barres) initially refuses to pay for car cleaning, but eventually agrees after Jerry locks him inside the car, refusing to let him out unless he pays half of the $250 price. Meanwhile, George discovers that someone stole the video out of the car while he and Jerry were inside the restaurant.

Mona seems to be attracted to Kramer, which Jerry, Elaine and George cannot understand; she has never been with a man. This angers Susan, due to her dislike of Kramer, as well as George, who worries that he drove Susan to lesbianism. Feeling even more attracted to her after finding this out, George attempts to woo Susan, and appears to be making progress. However, despite his best efforts, the two are approached in Monk's Café by George's ex, Allison (from the episode "The Outing") - and after introducing them to one another, he can only sigh as the two women exchange amorous glances.

After a complete "de-ionizing" of the car, Jerry discovers the stink is still there. Meanwhile, Elaine decides to go to a hair salon to wash the smell out of her hair, despite having already taken a thorough shower, and tries to restart her relationship with Carl. He, however, tells her that she still smells. Jerry tries to sell the car, but the dealer claims he cannot sell it. Kramer's relationship with Mona ends when she smells the odor on him, even though he was never in the car - but he had borrowed Jerry's jacket, which was the same one he was wearing in the car. When it cannot be sold, Jerry ends up abandoning the car and its keys out on the street just to get rid of it. A young man sees this, seizes the keys, and hops into the car, but is also disgusted by the smell.

## Production

Co-writer Peter Mehlman got the idea for the episode from the real-life experiences of a friend of his. According to Mehlman, the friend would frequently pitch him ideas for episodes, none of which were even close to being good enough to use, but on this occasion the friend was simply complaining about the ordeal, at which point Mehlman immediately decided that it would be perfect for the show.

Amy McWilliams remarks that this is an example of many episodes that are "open-ended": it lacks "a traditional resolution completely, as when the viewer is simply left with a final comic shot of a street hoodlum wrinkling his nose as he tries to steal 'The Smelly Car'."

## Critical response

Linda S. Ghent, Professor in the Department of Economics at Eastern Illinois University, discusses this episode in view of its economic themes, specifically those of externality, the Coase theorem, and moral hazard. Ghent explains,

> The external costs are large: the smell attaches itself to Jerry and Elaine, who have to resort to costly measures to cleanse themselves. Jerry attempts to recoup some of the damage by cleverly bargaining with the restaurant owner to cover the cost of cleaning the car. In the end, the cleaning is not enough, and Jerry leaves the car and keys in plain sight hoping it will be stolen, in which case the insurance company will bear the loss.

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Recurring

Heidi Swedberg ........... Susan Biddle Ross

#### Guests

Michael Des Barres .............. Restaurateur  
Nick Bakay .......................... Carl  
Kari Coleman ...................... Allison  
Taylor Negron .................... Hairdresser  
Courtney Gains .................. Clerk  
Raf Mauro .......................... Car Washer  
Viveka Davis ...................... Mona  
Robert Noble ..................... Salesman  
Patricia Place ...................... Wife  
Walt Beaver ....................... Husband

## Script

[Opening Monologue]

JERRY: ...And it *is* embarrassing, because a doggie bag means either you are out at a restaurant when you aren't hungry, or you've chosen the stupidest possible way to get dog food that there is. How about the doggie bag on a date? That's a good move for a guy, huh? Lemme tell you something: if you're a guy and you ask for the doggie bag on a date, you might as well have them just wrap up your genitals too. You're not going to be needing those for awhile, either.

[We open in front of a restaurant (&quot;Kady's&quot;) with Jerry and Elaine waiting for the valet to come and get the keys for Jerry's car. Jerry is amusing himself by trying to drive Elaine nuts by humming and waving his scarf in front of her face.]

JERRY: Is that bothering you?

ELAINE: No, not at all...

(Amazing resilience, that Elaine has. The valet comes and Jerry gives him the keys. Elaine has some advice.)

ELAINE: Oh, could you please hurry?

JERRY: [mockingly] &quot;Please hurry&quot;. Look at you. Look at what you've become.

ELAINE: What? What have I become? I haven't &quot;become&quot; anything...

JERRY: Oh, *Carl* can't wait a few more minutes?

ELAINE: I don't want to keep him waiting...

JERRY: He'll like you more...

ELAINE: That's impossible...

(I'll buy that... Anyhoots, an elder married couple saunter by (yes, they actually saunter)...

WIFE: Andrew, why do you have to pick your teeth at the table?

HUSBAND: Leave me alone.

JERRY: Yeah, I'm wanting to get married *real* soon...

JERRY: So, where am I dropping you?

ELAINE: His place...

JERRY: This guy's got quite a racket. I take you to dinner and then drop you off at his apartment...

ELAINE: *And* he gets the rest of my chicken...

(No, that's no cleverly hidden euphemism. Elaine has a doggie bag from the restaurant.)

JERRY: So, is tonight &quot;the night&quot;?

ELAINE: You never know...

JERRY: Oooh! Bay-bee *doll*!

(The car arrives. They drive away. Soon, they discover something is definitely amiss)

JERRY: Boy, do you smell something?

ELAINE: Do I smell something? What am I, hard of smelling? Of *course* I smell something.

JERRY: What is it?

ELAINE: I think it's B.O.!

JERRY: What?

ELAINE: It's B.O. The *valet* must have had B.O.

JERRY: It *can't* be. Nobody has B.O. like this.

ELAINE: Jerry. It's *B*.*O*.

JERRY: But the whole car smells.

ELAINE: So?

JERRY: So when somebody has B.O., the &quot;O&quot; usually stays with the &quot;B&quot;. Once the &quot;B&quot; leaves, the &quot;O&quot; goes with it.

(They each do their best dog impression and stick their heads out the window while they're driving. They come back inside. They moan disgustedly and stick their heads out, again.)

(We shift to Carl's rather nifty place.)

ELAINE: I can't believe you ski!

CARL: I'm a great skier.

ELAINE: Yeah? What else?

CARL: Let's see... I ski, I fish, I pillage, I plunder...

ELAINE: [delightedly] Oh! You &quot;pillage and plunder&quot;?

CARL: ...When I travel.

ELAINE: See? Finally, *finally* I get to meet a man who pillages and plunders! I'm so lucky.

(Elaine! I can do those things! I can pillage! I can plunder! I can pillage *and* plunder! I can even divide and conquer in a search algorithm! Oh, never mind... They embrace and kiss, but Carl gets a couple o' whiffs of Elaine's hair, and, judging by the look on Carl's face, he's not smelling Aussie Scrunch Spray)

[We shift to Champagne video where George is returning a movie with Kramer.]

GEORGE: This'll only take a second.

KRAMER: Yeah, I'm going to poke around...

(George goes towards the counter, but he sees a couple of girls holding hands browsing for movies (beside of a cardboard display for &quot;A Few Good Men&quot;-- the irony, the irony...)

GEORGE: [to himself] Hey, whatd'ya know? Look at that! A *lesbian* sighting. Oh-ho! My lucky day. They're *so* fascinating. Why is that? Because they don't want us. You gotta respect that...

(The couple turns around and one of the two is Susan, George's ex-girlfriend!)

GEORGE: [to himself] Oh, my God! It's Susan! What do I do?

(George turns around so as not to be recognized, but I'm guessing his distinctive bald head gives him away)

SUSAN: George?

GEORGE: [to himself] Argh! [to Susan] Susan! Hi! Oh, boy! What are you doing here?!

SUSAN: Renting a video! What do you got there?

GEORGE: Oh, ... some stupid movie...

SUSAN: This is Mona.

(George methodically extends his hand for a shake, bit by bit)

GEORGE: Oh, hi...

MONA: Pleasure to meet you.

GEORGE: Yes. Well...

MONA: Well, I'll let you two, uh... catch up.

SUSAN: You okay?

GEORGE: Yeah. Yes! I just haven't seen you in a long time.

SUSAN: And you didn't expect me to be holding hands with a woman.

GEORGE: Oh, *please*! Me? C'mon! That's *great*! Are you kidding? I think thats fan*tastic*! I've always encouraged experimentation! I'm the first guy in the pool! Who do you think you're talking to?

SUSAN: I *know* who I'm talking to.

GEORGE: Of course you do... It's just, uh, y'know, I-I never *knew*, uh, that, uh...

SUSAN: I liked women?

GEORGE: There you go.

GEORGE: So, uh, how long has this been going on?

SUSAN: Since you and I broke up.

GEORGE: Ssssso, after me, you... went that way?

SUSAN: Yeah.

GEORGE: Oh, I think that's fantastic. Good for you. Nice. That's very nice.

(Meanwhile, Kramer is practicing his golf swing across from an amused Mona. They can be seen talking in the background as Susan inspects George's movie)

SUSAN: So, what have you got there?

GEORGE: Oh, I, uh--

SUSAN: Oh, ``Rochelle, Rochelle''

GEORGE: It's a foreign movie... a *film*, is what it is, actually.

SUSAN: Yeah... A lot of nudity in that, huh?

GEORGE: No, no, no... Just a *tiny* bit... It's not even *frontal* nudity. It's... *sidal* nudity...

CLERK: Next.

GEORGE: Oh, that's me.

SUSAN: Alright, well... Good seeing you, George.

GEORGE: Yes, good to see you, too. And Good luck with, uh... with the whole thing, there.

CLERK: Uh, what are you returning?

GEORGE: [embarrassed pause] ``Rochelle, Rochelle''.

CLERK: Ah, ``Rochelle, Rochelle''... &quot;A young girl's strange, erotic journey from Milan to Minsk&quot;...

CLERK: Uh, that'll be, uh... $3.49.

GEORGE: $3.49? It says $1.49.

CLERK: Well, you didn't rewind it. There's a $2.00 charge for not rewinding.

GEORGE: What! There's no signs here! This is an outrage!

KRAMER: George, don't give him any money for that. It'll cost you less to keep it another day, rewind it and bring it back tomorrow. Don't give him the satisfaction.

GEORGE: I'm not giving you the satisfaction. I'm gonna watch it again...

(Ah yes, memories of Kramer putting thoughts into Newmans mind while dealing with Ron (of Ron's Records). Anyway, we cut to the apartment where Jerry and Elaine are bundling old magazines... )

JERRY: So, this morning I go down to the garage to check the car out. I figure by this time, the odour molecules have had at least twelve hours to de-smellify. I open the car door, like a *punch* in the *face*, the stench hits me-- it's almost as if it had *gained* strength throughout the night...

ELAINE: Y'know I can think of at *least* six known offensive odours that I would *rather* smell than what's livin' in your car.

JERRY: What about skunk?

ELAINE: I don't mind skunk.

JERRY: Horse manure?

ELAINE: I *loooove* horse manure.

JERRY: Well, I've never seen anything like this in my life. In fact, I went to the car wash, they want 250 dollars to detail it, and get the smell out. I'm not payin' for that. That's not my responsibility. In fact, I'm drivin' up to that restaurant now, and *demand* they pay for it.

ELAINE: Absolutely.

ELAINE: Listen, lemme ask you something. When you're with a guy, and he tells you he has to get up early, what does that mean?

JERRY: It means he's lying.

ELAINE: Wow...

JERRY: Why? Is that what he told you?

ELAINE: Yeah, last night. Oh, come on... Men *have* to get up early some time...

JERRY: No. Never.

ELAINE: Jerry! I'm *sure* I've seen men on the street early in the morning.

JERRY: Well, sometimes we do actually have to get up early, but a man will *always* trade sleep for sex.

ELAINE: Is it possible I'm not as attractive as I think I am?

JERRY: Anything's *possible*...

(Kramer Enters)

JERRY: What's the matter with you?

KRAMER: Steinbrenner! He's ruinin' my life...

JERRY: Oh yeah, Steinbrenner...

KRAMER: I don't think I can take another season with him, Jerry. He'll just trade away their best young prospects, just like he did with Beuner, McGee, Drabek... McGriff...

JERRY: I know the list...

KRAMER: What's that smell?

JERRY: What smell?

KRAMER: Ooooh... You stink.

JERRY: Whatd'ya mean I stink?

KRAMER: You *stink*. Why don't you go take a shower?

JERRY: I showered! Oh, wait a second... Since I showered, I've been in the car!

ELAINE: So?

JERRY: Don't you see what's happening here? It's attached itself to me! It's alive!

ELAINE: If it attached itself to you, then... Oh, my God! That's why Carl said he had to get up early! Because I stink! Jerry, he thinks I have B.O.! Me!

(George buzzes)

KRAMER: What happened?

JERRY: What happened? My car *stinks* is what happened. And it's destroying the lives of everyone in it's path.

[In the car]

GEORGE: What is that? B.O.?

JERRY: Yeah.

GEORGE: This is *unbelievable* B.O.

JERRY: I know... I was at the car wash this morning and the guy told me in his 38 years in the business, he's never smelled anything like it.

GEORGE: So, let me ask you. Do you think I could have done this?

JERRY: No, no. It's the valet guy.

GEORGE: No, no, I mean, driving Susan to lesbianism.

JERRY: Oh... No, that's ridiculous.

GEORGE: What if her experience with me *drove* her to it?

JERRY: Suicide, maybe, not lesbianism.

GEORGE: The woman she's &quot;lesbianing&quot; with? Susan told me she's *never* been with a guy.

GEORGE: Oh, this isn't even B.O.! This is *beyond* B.O.! It's *B*.B.O.!

JERRY: There should be a B.O. squad that patrols the city like a &quot;Smell Gestapo&quot;. To sniff 'em out, strip 'em down, and wash them with a big, soapy brush...

GEORGE: Y'know, the funny thing is, somehow I find her more appealing now... It's like if I knew she was a lesbian when we went out, I never would've broken up with her.

JERRY: Lemme see if I understand this... On second thought...

(At the restaurant, Jerry, refuses to have his car parked by the valet.)

JERRY: Here he is... that's the guy! (rolls up window) No, thank you, go back... go back... I'll park it! You go back!

(Inside, he confronts a snooty restaurant type guy.)

RESTAURATEUR: What do you mean-- &quot;stunk up&quot;?

JERRY: I mean the car *stinks*! George, does the car stink?

GEORGE: Stinks.

JERRY: Stinks!

RESTAURATEUR: Well, perhaps *you're* the one who has the odour...

JERRY: Hey, I've never smelled in my *life*, buddy!

RESTAURATEUR: Really? Well, I smell you now.

JERRY: That's from the car!

RESTAURATEUR: Well, maybe *you're* the one who stunk up the car, rather than the car stinking up you!

GEORGE: Oh, it's the chicken and the egg...

JERRY: Thank you very much... Well, then go out and smell the car; see which smells worse.

RESTAURATEUR: I don't have time to smell cars.

GEORGE: Forget about smelling the car. Smell the valet. Go to the source...

JERRY: You've gotta smell the car

RESTAURATEUR: I'm a busy man

JERRY: C'mon! One whiff!

RESTAURATEUR: Alright, one whiff...

(Inside the car, the restaurateur realizes there is some reality in Jerry's story)

RESTAURATEUR: Alright! I give up! I admit it! It stinks! Now will you let me out!

JERRY: Alright, will you pay for the cleaning?

RESTAURATEUR: Yes! 50 dollars! I'll give you 50 dollars!

(Jerry keeps him inside until he agrees to pay half ($125). To his horror, George discovers that some sick individual has stolen ``Rochelle, Rochelle'' from the dash of the car while they were inside. He asks the restaurateur to pay for that, too)

RESTAURATEUR: I'm not paying for *that*. They've already got my seven dollars... [sarcastically] &quot;...erotic journey from Milan to Minsk&quot;...

[Elaine and Carl at Elaine's apartment]

CARL: The valet had such bad B.O.?

ELAINE: Oh, man, just *rampant*, **mutant** B.O. The &quot;O&quot; went from the valet's &quot;B&quot;, to the car, to me. It clings to everything. Jerry thinks it's an entity. But I showered and I shampooed, so...

CARL: That's a relief...

(Carl recoils from an embrace from Elaine)

ELAINE: What?

CARL: It's still there...

ELAINE: No, no, no! It *can't* be! I shampooed! I rinsed! I repeated!

(Elaine corners the poor guy)

[We cut to the video store where George tells the clerk about the video. Susan shows up and George asks her]

GEORGE: Listen, I gotta ask you: I was a little concerned that perhaps I was responsible in some way for your, uh... metamorphosis.

CLERK: That'll be $98.00.

GEORGE: What $98.00?

CLERK: That's what I said. $98.00.

GEORGE: How could that piece of *crap* cost $98.00!?

(He borrows $35 from Susan to pay for the movie)

GEORGE: So, was it me?

SUSAN: Oh, don't be ridiculous! Is that what you wanted to talk to me about? [Gives him the $35] Here.

GEORGE: Oh, thanks. Thanks a lot. I'll pay you back.

SUSAN: Yeah, *sure*... I gotta go.

GEORGE: Listen. Let me ask you something. If you and Mona were ever to... dance, how do you decide who leads? I mean... do you take turns? Do you discuss it beforehand? How does that work?

SUSAN: You're an idiot.

GEORGE: Why? That's a *legitimate* sociological question.

SUSAN: I'll see ya. And George, by the way... You stink... Real bad.

GEORGE: It's not me! It's the car!

[Cut to Kramer waiting on a street corner]

MONA: I didn't think I'd come.

KRAMER: I knew you would.

MONA: Oh, Kramer!

(They embrace.)

[Car-wash]

CAR WASHER: We spray everything with Ozium-D, let it de-ionise, vacuum the spray out with a de-ionising machine. Hit it with high-pressure compressed air, and wet-dry vac it to extract the remaining liquids. We top it off with one of our seven air-fresheners, in your case, I would recommend the Jasmine, or the Potpourri.

JERRY: Let's do it.

[Cut to the hair salon where Elaine hears a similar tale from the hairdresser.]

HAIRDRESSER: The first thing we're gonna do is flush the follicles with the five essential oils. Then, we put you under a vapour machine, and then a heated cap. Then, we shampoo and shampoo and condition and condition. Then, we saturate the hair in diluted vinegar-- two parts vinegar, 10 parts water. Now, if that doesn't work, we have one last resort. Tomato sauce.

ELAINE: Tomato Sauce?

[Jerry doles out his $250 and gets in the April-fresh car...]

JERRY: Wait a minute! It still smells! It still smells!

[Elaine visits Carl to present her olfactory-wise magnificent hair.]

CARL: It still smells.

[Jerry explains things to George back at the apartment.]

JERRY: It still smells!

GEORGE: How could it still smell after all that?

JERRY: I don't know!

GEORGE: Well, what are you gonna do?

JERRY: I'll tell you what I'm gonna do, I'm selling that car!

GEORGE: You're *selling* the car!?

JERRY: You don't understand what I'm up against. This is a force more powerful than anything you can imagine. Even *Superman* would be helpless against this kind of stench. And I'll take anything I can get for it.

GEORGE: Maybe I'll buy it.

JERRY: Are you crazy? Don't you understand what I'm saying to you? This is not just an odour-- you need a *priest* to get rid of this thing!

ELAINE: I still smell!

JERRY: You see! You see what I'm saying to you? It's a presence! It's the beast!

[Outside, a woman's voice is heard screaming and pounding on Kramer's door.]

SUSAN: Kramer! Kramer! Kramer, open up, I know you're in there!

JERRY: Susan!

SUSAN: Kramer!

JERRY: What is going on?

SUSAN: You know what's going on? First, he vomits on me. Then, he burns down my father's cabin. And now, he's taken Mona away from me.

GEORGE: He stole your girlfriend?

SUSAN: Yes. She's in *love* with him.

GEORGE: Amazing. I drive them to lesbianism, he brings 'em back.

(Kramer waits until George and Susan have left, then calls Jerry on the phone, and begins to explain the situation to Jerry (while coming into his apartment, still on the phone). He realizes his silliness and puts the phone away, but continues the tale, explaining how Mona is a golf instructor, and how he's already taken six strokes off his game.)

JERRY: That's the *least* of what you've accomplished...

(Kramer explains further and borrows Jerry's jacket for his date with Mona that evening. As he's leaving, Elaine asks the inevitable)

ELAINE: Wait a minute, wait a minute. Kramer, Kramer... Hold on a second. I don't get this. This woman has *never* been with a man her *entire* life--

KRAMER: I'm Kramer.

[George and Susan at Monk's]

GEORGE: I know what you're going through. Women. Who knows what they want?

SUSAN: I just don't know what she sees in *Kramer*.

GEORGE: Listen. You're beautiful. You're intelligent. You'll meet other girls...

SUSAN: You think so?

GEORGE: Yes, I know so. You happen to be a very eligible lesbian.

SUSAN: You're very sweet...

GEORGE: Hey, I know what I'm talking about. I gotta be honest with you, I gotta tell ya... Ever since I saw you holding hands with that woman, I can't get you out of my mind.

SUSAN: Really?

GEORGE: Yeah, you're just so... hip.

(A woman exits the bathroom and approached the table)

GEORGE: Oh, my God...

SUSAN: What?

GEORGE: It's Allison. I dated her right after you. She's obsessed with me.

(Despite his hiding, Allison sees George.)

ALLISON: George?

GEORGE: Allison! Hi! Oh, my God! How are you?

ALLISON: Good. You know, you owe me $50...

GEORGE: Right. I don't have it on me. Allison, this is Susan. Susan, Allison.

ALLISON: Nice to meet you...

SUSAN: Nice to meet you...

ALLISON: That's a beautiful vest...

SUSAN: Thank you...

[Kramer and Jerry at the car lot trying to sell J's car.]

KRAMER: I don't understand it. I was with her last night in my apartment; it was very romantic. Y'know with that fake wood wallpaper, the atmosphere is *fabulous* in there, now. It's like a ski lodge.

SALESMAN: What year did you say this was?

JERRY: '90.

KRAMER: Anyway, we were on the couch, I move to hug her, next thing she tells me she's leaving; she's got to get up early.

JERRY: That's strange...

SALESMAN: How many miles you got on this thing?

JERRY: 23 000.

KRAMER: And I was looking good, too. I had a nice, new shirt on, I'm wearing *your* jacket...

JERRY: Wait a second... My jacket! I wore that in the car! The Beast!

[Speak of the Devil, the car salesman exits the car with an &quot;Augh!&quot;... ]

SALESMAN: I can't sell this car.

JERRY: This... **thing**... has got to be stopped!

[At the salon, Elaine rationally decides her fate to the hairdresser]

HAIRDRESSER: So, what do you want to do?

ELAINE: Sauce me.

[In a rough-looking part of town]

(Jerry parks in front of a punk, tosses his keys in the car, and leaves, making it *very* obvious to the guy that the car has got a high steal quotient. He stops short of screaming &quot;TAKE IT!&quot;. The punk gets in, and... well, let's just say he doesn't smell potpourri...)

[Opening Monologue]

Why do we need B.O.? What is the function of it? Everything in nature has a reason, has a purpose, except B.O. Doesn't make any sense-- do something good, hard work, exercise, smell very bad. This is the way the human being is designed. You move, you stink. Why can't our bodies help us? Why can't sweat smell good? It'd be a different world, wouldn't it? Instead of putting your laundry in the hamper, you'd put it in a vase. You'd go down to the drugstore and pick up some deodorant and perspirant. You'd probably have a dirty sweat sock hanging from the rear-view mirror of your car. And then on a really special night, maybe a little underwear comin' out of your breast pocket. Just to let her know she's important.

The End
