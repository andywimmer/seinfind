---
layout: default
title: The Summer of George
parent: Season 8
nav_order: 22
permalink: /the-summer-of-george
cat: ep
series_ep: 156
pc: 822
season: 8
episode: 22
aired: May 15, 1997
written: Alec Berg & Jeff Schaffer
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697794/
wiki: https://en.wikipedia.org/wiki/The_Summer_of_George
---

# The Summer of George

| Season 8 - Episode 22                | May 15, 1997              |
|:-------------------------------------|:--------------------------|
| Written by Alec Berg & Jeff Schaffer | Directed by Andy Ackerman |
| Series Episode 156                   | Production Code 822       |

"The Summer of George" is the 156th episode of the sitcom Seinfeld. It was also the 22nd and final episode of the eighth season. It originally aired on May 15, 1997.

## Plot

George discovers he has a severance package from the New York Yankees that should last him about 3 months, so he decides that he's going to take full advantage of 3 months off and become very active. Jerry and Kramer are going to the Tony Awards: Jerry as an invited guest, Kramer as a seat filler. Elaine mocks Sam, a coworker who walks without moving her arms (as if "she's carrying invisible suitcases", as Elaine puts it).

Jerry picks up his date to the Tonys, a waitress named Lanette, only to find out to his dismay that she lives with a man named Lyle (a "dude") with whom she has an ambiguous relationship. While filling a seat for a nominee who's stepped away, excited Tony winners moving through Kramer's row accidentally whisk him to the stage. As a result, he receives a Tony Award for the fictional musical Scarsdale Surprise (based on the killing of Dr. Herman Tarnower), in which Raquel Welch is the star.

Meanwhile, instead of living a very active lifestyle as he'd planned, George becomes extremely lazy. He never changes out of his pajamas, and feels too weak to even come to Jerry's apartment, asking Jerry, Elaine and Kramer to instead visit him or talking to Jerry on the phone to know what's going on over at his apartment.

Elaine's coworker Sam talks to Elaine about how Sam isn't fitting in at work, to which Elaine mentions her arms never move and inadvertently mocks her by comparing her to a caveman. In a rage, Sam later trashes Elaine's office and leaves her threatening phone messages, leading the men in Elaine's life to excitedly say that she's now involved in a "catfight", and refuse to help.

Kramer uses his Tony as a ticket into Sardi's, where the producers of Scarsdale Surprise have a proposition for him - he can only keep his Tony award if he fires Raquel Welch, who like Sam, also doesn't swing her arms when she moves; the reason the producers ask Kramer to fire Raquel is that they're terrified of her ("I heard from someone that when they cut one of her lines, she climbed up the rope on side of the stage and started dropping lights on people's heads," as Kramer quotes). Kramer fires her and she responds by attacking him and destroying his Tony as well. While walking down the street afterwards, Raquel sees Elaine describing Sam's walk to the police; thinking that Elaine is mocking her, Raquel attacks her, too.

Meanwhile, Lanette begins to wear Jerry out with her busy lifestyle after leaving Lyle, and George suggests that perhaps they team up, with George acting as Jerry's dating assistant. When Lanette needs invitations to a party, George picks them up, but on the way back to his apartment he stops to play "frolf" (frisbee golf) with some people. (The man who invites him to join the game is David Mandel, a Seinfeld writer.) At his apartment, an invitation falls out of the box and lands on the stairs. When George leaves again to deliver the invitations, he slips on the invitation and falls down the stairs, sending him to the hospital.

The final scene pays homage to the ending of "The Invitations". In the wake of George's accident, the gang meets up at the hospital where the same doctor who informed them of Susan's death informs them that George may never walk again due to being unhealthy. The others respond with the same callous reaction as they did the year before. The end credits show George learning to walk again through physiotherapy, along with Sam, who's being taught how to swing her arms.

## Cast

#### Regulars

Jerry Seinfeld ...................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus .............. Elaine Benes  
Michael Richards .................. Cosmo Kramer

#### Recurring

John O'Hurley .................. J. Peterman

#### Guests

Raquel Welch ........................ Herself  
Molly Shannon ...................... Sam  
Amanda Peet ........................ Lanette  
Joe Urla ................................ Dugan  
Victor Raider-Wexler ............ Doctor  
Peter Dennis ......................... Lew  
Tucker Smallwood ............... Malcolm  
Wayne Wilderson ................ Walter  
Blake Gibbons ...................... Lyle  
Adrian Sparks ..................... Man  
Jane A. Johnston .................. Woman  
Lauren Bowles ..................... Waitress  
Sue Goodman ....................... Clerk  
Neil Flynn ............................. Cop #1  
Tom Michael Bailey ............... Cop #2  
Denise Bessette .................... Therapist  
Dave Mandel ........................ Himself

## Script

[George and Jerry at a caf&eacute; terrace]

JERRY: Nah, you had a good run. Took them to the World Series.

GEORGE: I got to give the players most of the credit for that.

JERRY: Don't sell yourself short. You made all the flight arrangements, hotels, busses.

GEORGE: No, I don't know who was doing that.

JERRY: So, when you actually did work, what it was that you did?

GEORGE: I tell ya they had a pastry cart you wouldn't believe.

(Waitress (Lanette) comes with drinks)

LANETTE: Here you go. Your latt&eacute;, your cappuccino.

(Waitress leaves)

JERRY: Maybe I should ask her out?

GEORGE: She is a good waitress.

JERRY: That's true. Maybe I take her to the Tony's.

GEORGE: You're going to the Tony's?

JERRY: Yeah, I wrote some jokes to the show and they gave me two tickets.

GEORGE: Why didn't you ask me? I know a million theater jokes. 'What's the deal with those guys down in the pit?'

JERRY: They're musicians. That's not a joke.

GEORGE: It's a funny observation.

(George reads a letter)

GEORGE: Severance package... The Yankees are giving me three months full pay for doing nothing.

JERRY: They did it for three years. What's another few months.

GEORGE: I'm really going to do something with these three months.

JERRY: Like what?

GEORGE: I'm gonna read a book. From beginning to end. In that order.

JERRY: I've always wanted to do that...

GEORGE: I'm gonna play frolf.

JERRY: You mean golf?

GEORGE: Frolf, frisbee golf Jerry. Golf with a frisbee. This is gonna be my time. Time to taste the fruits and let the juices drip down my chin. I proclaim this: The Summer of George!

(A bee comes and George has to runaway to inside)

[Elaine's office. She and two co-workers are chatting in a corridor.]

ELAINE: ...and then Peterman ate it. I never told him.

(A woman comes walking by, with her hands hanging still besides her.)

ELAINE: Who's that?

DUGAN: That's Sam, the new girl in the counting.

WALTER: What's with her arms? They just hang like salamis.

DUGAN: She walks like orangutan.

ELAINE: Better call the zoo.

DUGAN: Reer...

ELAINE: What?

WALTER: ssssss...

DUGAN: Cat-ty...

[Jerry's apartment. Jerry and Elaine. George is watching TV.]

ELAINE: It's like she's carrying invisible suitcases.

JERRY: Like this? (imitates the walk)

ELAINE: Yes, exactly.

JERRY: That's so strange.

ELAINE: Right. So why I'm the one who gets 'reer'. You know I mean they were being as catty as I was. It's a double standard.

JERRY: Oh, what about 'ladies night'? Women admitted free before 10?

ELAINE: That is so stupid.

JERRY: Reer.

GEORGE: Hey, 'The White Shadow' is on...

JERRY: Boy, you're really packing it all in.

GEORGE: Jerry, my vacation just started. I need a day or two to decompress. Besides, I did plenty today.

JERRY: Like what?

GEORGE: I bought a new recliner with a fridge build right in to it.

(Kramer enters wearing a bathrobe)

KRAMER: Hey Jerry, you got any Tums?

JERRY: Stomach ache?

KRAMER: I drank too much water in the shower.

JERRY: Aah, top of the fridge. Hey George, I'm taking that waitress to the Tony's.

GEORGE: Shadow!

KRAMER: Oh, the Tony's. I'll see you there buddy.

ELAINE: You're going to the Tony's too?

KRAMER: Roger that.

JERRY: Where are you sitting?

KRAMER: Well, all over the place. Yeah, I'm a seat filler. They don't like to see empty seats on TV so when somebody gets up I just park my caboose on their spot until they get back.

ELAINE: How did you get that job?

KRAMER: Mickey, Mickey he hooked me up. He's a member of the academy.

JERRY: What academy?

KRAMER: Well, he didn't say (leaves).

[Jerry comes to pick up Lanette]

LANETTE: Hi!

JERRY: Hi!

LANETTE: Nice tuxedo.

JERRY: Thanks, it's a breakaway.

LANETTE: Should we go?

JERRY: Absolutely.

LANETTE: Lyle, were going! Jerry, this is Lyle.

LYLE: Hey, how you doing?

JERRY: Ok...

(Lyle and Lanette kiss)

LANETTE: Bye.

LYLE: Have a good time.

JERRY: Thanks, Lyle...

[At the Tony's. Kramer is looking a place to fill. One man stands up.]

KRAMER: Are you leaving? Cause I got you covered.

(man tries to button his jacket and Kramer squeezes to his seat)

KRAMER: I'll just squeeze in...

MAN: What are you doing?

KRAMER: My job. What are you doing?

(Kramer looks at the woman next to him)

KRAMER: You know, if they catch two of us on TV, you got some explaining to do.

(Same time Jerry and the waitress are sitting elsewhere.)

JERRY: So, you and Lyle are roommates?

LANETTE: No.

JERRY: Gay?

LANETTE: What?

JERRY: Is he gay?

LANETTE: No.

JERRY: Are you sure?

LANETTE: I think I would know.

JERRY: (to himself) This is a new one.

KRAMER: (to woman next to him) Pemmican turkey? Come on take a bite... well more for me.

(Kramer takes a bite and hits the man in front of him in the head. Then points the blame to lady next to him.)

ANNOUNCER: ...and the best musical award goes to: Scarsdale surprise!

(People around Kramer stand up and roll on to the stage taking Kramer with them.)

(George is at home watching the show on TV and takes a soda out of his new recliner.)

GEORGE: Kramer?

(TV shows the producers and Kramer accepting the award on stage.)

MR. GRAHAM: (on TV) Thank you and God bless you all. This has truly been a Scarsdale surprise!

[At Monk's. Elaine and Sam.]

SAM: Elaine, am I crazy? I just get the feeling that Dugan and the others are making fun of me all the time.

ELAINE: Well, You might wanna think about... maybe, eh... moving your arms a little when you walk.

SAM: My arms?

ELAINE: You know, sort of swing them, so your not lurching around like a caveman.

SAM: I a caveman?

ELAINE: No no no no, it's just...

SAM: Everybody told what a catty shrude you are. Your horrible!

[5A. Jerry and George.]

GEORGE: She had a dude?

JERRY: Yeah, when I went to pick her up there was this dude.

GEORGE: How do you know it was her dude?

JERRY: What do you think it could've been just some dude?

GEORGE: Sure, dudes in this town are dime a dozen.

JERRY: I reckon.

GEORGE: Or maybe, she just wanted to go to the Tony's. I tell you what; you ask her out again. No Tony, just Jerry. That way you know it he was her dude or just some dude.

JERRY: Dude!

(They make a high five.)

GEORGE: Alright, that's enough. I gotta go home and take a nap.

JERRY: It's 10:30 in the morning?

GEORGE: I tell you; I'm wiped.

JERRY: So, has the summer of George already started or are you still decomposing?

GEORGE: Decompressing.

(Kramer enters wearing a tuxedo and carrying a Tony statue.)

KRAMER: Whooa, good morning gentleman and Tony says hello to you.

JERRY: You didn't give that thing back?

KRAMER: Jerry, it was a whirlwind. They whisked us backstage, the media is swarming, champagne is flowing...whooo! I can't describe how great it is to win.

JERRY: That's because you didn't win.

GEORGE: 'Scarsdale surprise'. That's the musical about that Scarsdale diet doctor murder.

KRAMER: Featuring the mind-blowing performance of Ms. Raquel Welch!

JERRY: You haven't even seen it.

KRAMER: Aah, Jerry I'm not gonna let you bring me down from this high. I've been partying all night. I saw the sunrise at Liza's!

GEORGE: What, Minelli's!?!

KRAMER: No.

[Elaine's office. She is at her desk talking to Sam.]

ELAINE: Sam, listen I'm so sorry about the other day.

SAM: No, don't apologize Elaine. I was thinking that maybe I should swing my arms a little bit more.

ELAINE: See, yeah, that's all I was saying.

SAM: How's this (Sam hits a pen case out of the table), or this (swings a paper holder of the table and starts to clear the table left and right).

ELAINE: Well, you seem to be getting a hang of it...

[Jerry is picking up Lanette again. She opens the door wearing a towel.]

JERRY: Hi!

LANETTE: Sorry, I'm running late. I just lost track of time.

JERRY: No rush.

(Lyle walks by wearing a towel.)

LYLE: Hey, Jerry! What's up?

JERRY: (to himself) I have absolutely no idea.

[5A. Jerry is on the phone with George who is at home.]

(missing the beginning)

JERRY: ..except that the dude plays the showroom and I'm stuck doing food and beverage!

(Kramer comes in with the Tony.)

GEORGE: Who's that?

JERRY: It's Kramer.

GEORGE: Hey Kramer.

JERRY: George says hi.

KRAMER: (shouts to the telephone) Hi George!

GEORGE: How's that Tony?

JERRY: Why don't you just come over?

GEORGE: Why can't we do this on the phone? What's Kramer doing now?

JERRY: He's looking in the refrigerator.

GEORGE: Kramer. Anything good in there? Any popsicles?

JERRY: I can't do this.

KRAMER: So, what's George doing?

JERRY: He's not doing anything. Goodbye!

KRAMER: So listen, I'm going to crab a bite to eat at Sardi's. You wanna go?

JERRY: Are you taking the Tony to Sardi's?

KRAMER: The Tony is taking me to Sardi's.

(Knock on the door. Jerry opens and it's Lanette.)

JERRY: Oh, hello.

KRAMER: I'm going.

LANETTE: Congratulations!

KRAMER: Oh, thank you, thank you so much. I have so many people I want to thank and don't want to forget anyone...

JERRY: Alright, alright.

(Phone rings and Jerry answers.)

JERRY: I said no! (hangs up the phone.)

LANETTE: Jerry, I just want to let you know; Lyle and I are completely over. I'd rather be with you.

JERRY: Just me? No dudes or fellas?

LANETTE: What do you think?

JERRY: I can start right away.

(Phone rings again.)

JERRY: But not here. (They leave.)

JERRY'S ANSWERING MACHINE: 'I'm not here, leave a message.'

GEORGE: (on the answering machine) Jerry, what's happening? Come on, pick up the phone.

[At Sardi's. Kramer is sitting with a group of people.]

KRAMER: ...so I said to him: Arthur, Artie come on, why does the salesman have to die? Change the title; The life of a salesman. That's what people want to see.

MR. GRAHAM: Mr. Kramer, my name is Lewis Maxton Graham. I'm one of the producers of 'Scarsdale surprise'.

KRAMER: Hey, eh, Lew!

MR. GRAHAM: We need to talk.

[Elaine is talking to Peterman at her office.]

PETERMAN: Elaine, what did you want to talk me about?

ELAINE: This. My office. Sam trashed my office.

PETERMAN: Well, I see what's going on in here. I am smack dab in the middle of a good old fashioned cat fight.

ELAINE: Mr. Peterman, this is not a cat fight. This is violent psychotic behavior directed at me all because are told her to swing her arms.

PETERMAN: Woof!

ELAINE: Do you mean &quot;reer?&quot;

PETERMAN: Yes, that's the one! Good day Elaine. (Leaves.)

ELAINE: Oh, no please Mr. Peterman, she's crazy! (Sam walks by and Elaine starts to sing) Crazy for feelings...

[At Monk's. Jerry and Lanette.]

JERRY: I can't believe how much we did this afternoon. I have friends who this would've be their whole life.

LANETTE: Now, what time are you picking me up tonight?

JERRY: Eh, what?

LANETTE: You got our reservation from Sfuzi's, didn't you?

JERRY: Oh yeah, Sfuzi? I- I've gotta do that.

LANETTE: Should I ware the outfit I bought today?

JERRY: Sure.

LANETTE: Which one?

JERRY: The one with the... (mumbles.)

LANETTE: If I wanna get my hair cut I've gotta go now. Call me when you get home. I wont be there, but leave a message so I know you called.

JERRY: Ok, ok...

LANETTE: Do you mind?

JERRY: No, I'll grab it.

(Lanette leaves and Jerry bangs his head to the table. George walks in.)

GEORGE: Hey, I've done that today.

JERRY: What, did you lose your remote?

GEORGE: Nah, the cable's out. What's with you? You look dead.

JERRY: It's Lanette! I need an assistant or intern or something.

GEORGE: (Laughs) Relationship intern... hey, what if two of us teamed up?

JERRY: Not.

GEORGE: No, no...

JERRY: No, because that's...

GEORGE: No, listen; we are always sitting here, I am always helping you with your girl problems and you are helping me with my girl problems. Where do we end up?

JERRY: Here.

GEORGE: Exactly! Because neither one of us can handle a woman all by ourselves.

JERRY: I'm trying.

GEORGE: I've tried. We don't have it. But maybe the two of us, working together at full capacity, could do the job of one normal man.

JERRY: Then each of us would only have be like a half man. That sounds about right!

[Kramer at Sardi's, talking with two producers.]

MR. GRAHAM: I'm sure how excited you are to have this very very prestigious award. But you didn't have anything to do with the actual production.

KRAMER: No.

MR. GRAHAM: I don't think there's no way how we can allow you to keep this Tony. Unless...

KRAMER: Anything...

MALCOLM: Are you familiar with our star, Raquel Welch?

KRAMER: Oh yeah, she's fantastic...

MR. GRAHAM: She's a train wreck.

MALCOLM: There's a big tap dance number just before Jean Harris leaves the (?) school to confront Dr Tarnover.

MR. GRAHAM: It is a gut wrenching scene.

MALCOLM: But, Raquel Welch doesn't move her arms when she tap dances. It's very distracting.

MR. GRAHAM: There's lot of this (swings his arms) in tap dancing.

KRAMER: So, you'd like me to teach how to dance?

MALCOLM: No, we want you to fire her.

[5A. Jerry and Kramer.]

JERRY: Why they want you to fire Raquel Welch?

KRAMER: Because they're terrified of her. I heard from someone that when they cut one of her lines, she climbed up the rope on side of the stage and started dropping lights on peoples heads. Story like that has got to be true.

JERRY: She seems very nice.

KRAMER: Jerry, you're not in show business. You don't know what these people are like.

JERRY: I'm in show business.

KRAMER: Oh, come on! What am I gonna do? She's going to eat me alive.

JERRY: I've got a tape of 'Fantastic voyage' if you think that would help.

KRAMER: Yayaya...

(Elaine enters.)

ELAINE: Jerry, that crazy straight-armed woman down at Peterman's trashed my office. And then listen to this; this is message she left me.

(Takes a tape recorder and plays the tape.)

SAM'S VOICE: Elaine... I am going to find you. If not in your office then in the xerox room or the little conference room near the kitchen...

ELAINE: She must've got a blueprint of the building or something.

JERRY: Did you tell Peterman about this?

ELAINE: Well, I tried, but he thought it was some sort of cat fight.

KRAMER: Cat fight?

ELAINE: Ok, why? Why do guys do this? What is so appealing to men about a cat fight?

KRAMER: Yeye cat fight!

JERRY: Because men think if women are grabbing and clawing at each other there's a chance they might somehow kiss.

KRAMER: T-t-t-t...

[Jerry and George are walking down the street.]

JERRY: You got the tickets?

GEORGE: Yeah, two for the 7:15 of Novaj pravas (?). What you're wearing the green sweater?

JERRY: I like it.

GEORGE: She doesn't like. Here's your blue one, it's her favorite. (Takes sweater out of his bag.)

JERRY: What?

GEORGE: Just put it on! Alright now, remember she had her nails done today so remark how you like the color. And if you need me you beep me, alright? Here you go, hey, hey, hey, hey... (sprays Binaca into Jerry's mouth.) Go get'em you're a tiger!

JERRY: Hey George, one second, she's having a party friday night and she wants me to take care of the invitations.

GEORGE: A little notice would've helped! How many people?

JERRY: 35, and George, on the invitations...

GEORGE: I know, I know... don't skimp. Go go go go...

(Jerry leaves and Lanette joins him.)

LANETTE: Right on time, I like that.

JERRY: I like your nails, that is a great color.

LANETTE: Love the sweater.

JERRY: This old thing?

[George is getting the invitations, Melody Stationers.]

GEORGE: Hi, I need some party invitations.

CLERK: Okay, have you been in here before?

GEORGE: About a year ago. Wedding invitations.

CLERK: Right, how did that all work out?

GEORGE: No complaints.

CLERK: Well, they are arranged according the price. And as I recall... (she flips the sample book all the way to the end.)

GEORGE: Actually, (George flips the book back to the beginning) I'll take these nice glossy ones.

[Majestic theater, Raquel Welch's dressing room.]

RAQUEL: &quot;You are a fraud Dr. Tarnover. You haven't even been to Scarsdale.&quot;

(Kramer knocks on the door and enters.)

KRAMER: Ms. Welch.

RAQUEL: Who are you?

KRAMER: Well, I'm Cosmo Kramer, I'm one of the producers.

(Phone rings and Raquel answers.)

RAQUEL: Hello, Sidney! No, no I told you I don't want to do that! If you bring it up again I will feed your genitals to a wolf! (hangs up) Kids! You're still here.

KRAMER: Well, I- I Ms. Welch I do need to talk to you about a little problem regarding, eh, your performance.

(Raquel kicks a chair out of her way.)

RAQUEL: What kind of problem?

KRAMER: Well, it seems that due to the vagaries of the production parameters of this fragmenting of the audience to the cable television, carnivals, water parks...

RAQUEL: Out with it!

KRAMER: Well, you're fired because you don't move your arms when you tap dance, you're like a gorilla out there I've gotta go...

(Kramer runs out, but Raquel grabs him from the back.)

[George walks at the park with the invitations. A frisbee flies to his feet.]

GUY: Little help?

GEORGE: Hey, frolf?

GUY: Yeah, you know we need a fourth for the back nine. You want in?

(George looks up and sees Jerry on the other side and frisbee on the other. Jerry says: &quot;What's the deal with airplane peanuts?&quot;)

GEORGE: Yeah, sure.

GUY: Ok, come on.

[Jerry is at Lanette's apartment. They are both wearing a towel.]

JERRY: Ok, let's towel it up.

LANETTE: Jerry, where are those invitations you were supposed to get? If they don't go out today they're useless.

JERRY: But we're in towels.

LANETTE: Jerry.

JERRY: Alright. One second.

(Picks up the phone.)

(George runs up the stairs)

GEORGE: He frolfs, he scores... (he drops one invitation on the stairs.)

(George comes to his apartment and picks up the phone.)

GEORGE: Hello.

JERRY: George, where are those invitations? You were supposed to leave them with her doorman!

LANETTE: Did you shave your chest hair?

JERRY: No. (Lanette leaves.) Did you at least pick them up?

GEORGE: Yeah, the super glossy. The best they had.

JERRY: Ok, get them over here pronto. We're in towels here George.

GEORGE: Alright, Alright, keep your towel on.

JERRY: ...what?

GEORGE: It's a joke.

JERRY: Alright, that's not bad. Now get over here!

(George leaves from his apartment and comes down the stairs. He slips on an invitation and falls on his back.)

[Elaine is on the street and plays a tape to two police officers.]

SAM'S VOICE: (on the tape) ...if not in your apartment then in the laundry room or the ATM in the building across the street or the watch shop!

ELAINE: Can't you do anything about this? I mean this woman is a psycho!

COP #1: 'Reer.'

ELAINE: Look, just because I'm a woman...

COP #2: 'Mauau.'

COP #1: 'Meeow.'

(Raquel Welch walks down the street.)

RAQUEL: I don't move my arms when I dance. That's my signature!

ELAINE: Would you just keep an eye out for this woman. She's about ye high and eh, she doesn't swing her arms when she walks.

COP #1: What do you mean?

ELAINE: Like this... (imitates the walk with her arms hanging.)

(Raquel Welch comes towards her.)

RAQUEL: What the hell is that? Are you making fun of my dancing?

ELAINE: Aren't you Raquel Welch?

RAQUEL: You know who I am. Now, what are you doing?

ELAINE: Nothing, I wasn't just moving my arms...

RAQUEL: That's it, you are going down.

COP #1: Ooh, cat fight.

[The New York Hospital. Jerry and Kramer.]

KRAMER: So how's George?

JERRY: I don't know. They don't tell me anything. What's that? (Kramer holds a broken Tony)

KRAMER: Tony.

JERRY: What happened to you?

KRAMER: Raquel Welch!

JERRY: Yikes.

(Elaine comes. She has a scars on her face.)

JERRY: What happened to you?

ELAINE: Raquel Welch!

KRAMER: The woman is a menace.

ELAINE: Yeah, I bumped in to her on the street. It got pretty ugly.

JERRY: Cat fight with Raquel Welch.

KRAMER: Yey eye ca-catfight.

(George is wheeled in on a hospital bed.)

ELAINE: My god, George!

GEORGE: I slipped on the invitations... how's the towels?

JERRY: Back on the rack.

GEORGE: With the two of us?

JERRY: I think we're still a man short.

(Doctor comes.)

DOCTOR: Mr. Costanza... your legs have sustained extensive trauma. Apparently your body was in the state of advanced atrophy, due to a period of extreme inactivity. But with a lot of hard work and a little bit of luck, I think there's a good chance you may, one day, walk again.

(Doctor leaves.)

KRAMER: Well, that's good news.

ELAINE: Wow, invitations again...

KRAMER: Yeah, that's weird.

ELAINE: Alright, well... you want to grab some coffee?

JERRY: Yeah...

KRAMER: I'd like to get some coffee.

(Jerry, Kramer and Elaine leave.)

GEORGE: This was supposed to be 'The summer of George'! The summer of George.

[George is at the hospital physical therapy, trying to walk, leaning on parallel bars. Next to him, Sam tries to walk with swinging her arms.]

THERAPIST: Now, swing them... swing... swing them, just swing them.

SAM: I can't do it. It's hard!

(George is at end of the bars and falls down. He tries to get up.)

GEORGE: Still a little summer left.

The End
