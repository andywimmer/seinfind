---
layout: default
title: The Muffin Tops
parent: Season 8
nav_order: 21
permalink: /the-muffin-tops
cat: ep
series_ep: 155
pc: 821
season: 8
episode: 21
aired: May 8, 1997
written: Spike Feresten
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697736/
wiki: https://en.wikipedia.org/wiki/The_Muffin_Tops
---

# The Muffin Tops

| Season 8 - Episode 21     | May 8, 1997               |
|:--------------------------|:--------------------------|
| Written by Spike Feresten | Directed by Andy Ackerman |
| Series Episode 155        | Production Code 821       |

"The Muffin Tops" is the 155th episode of the sitcom Seinfeld. This was the 21st episode of the eighth season. It aired on May 8, 1997.

## Plot

### The Tourist

While George and Jerry are walking down the street, a man (Barry Kramer) asks George to watch a bag of luggage for a minute. When the stranger doesn't return, George starts wearing some of the clothing from the bag, using the rationale that "I'm still watching them." While he walks down the street in the tourist's clothing, looking at a map, a woman who works for the New York Visitor's Center mistakes George for a tourist and introduces herself as Mary Anne (Rena Sofer). George pretends that he's visiting from Little Rock, Arkansas, where he works as a hen supervisor for the Tyler Chicken company.

They go to Monk's Café, where George continues to lie to Mary Anne about himself. George finds her attractive, and she's interested in him, but she doesn't want to get involved, believing he'll be leaving soon. To prolong the relationship, he tells her he's thinking of 'moving' to New York; she replies that the city would "eat him alive."

George is insulted by this and sets out to prove he can get by in New York by showing her his 'new' apartment and his office where he works for the New York Yankees. Then Steinbrenner enters George's office and Mary Anne tells him of George's alleged job with Tyler Chicken. Steinbrenner then tells George that his mind is blown about his moonlighting. Then he phones the CEO of Tyler Chicken named Don Tyler (who, like Steinbrenner, is seen from behind) to announce that he doesn't want to share George while stating that George has been dividing his time between the New York Yankees and Tyler Chicken. Tyler states that he doesn't know who he is and that if he's that important, he's not giving him up that easily. Steinbrenner then comments that Tyler is playing hardball. Tyler then says "How about this: you give me Costanza, I convert your concessions to all chicken, no charge. Instead of hot dogs, chicken dogs. Instead of pretzels, chicken twists. Instead of beer, alcoholic chicken." When Steinbrenner asks how alcoholic chicken is made, Tyler replies "Let if ferment, just like everything else." Impressed with the alcoholic chicken, Steinbrenner agrees to trade George to Tyler by stating that he will have him on the next bus to Arkansas.

At a bar, George tells the bartender how Steinbrenner traded him to Tyler Chicken. Eventually, the tourist returns for his clothing and is upset that George is wearing them in a bar. He takes his clothing off George, leaving him in his boxers.

### Jerry

Jerry notices his chest hair is uneven and tries to straighten it out, but ends up shaving it all off. He worries what his girlfriend Alex (Melinda Clarke) will think, until he discovers she's fond of hairless dogs. He continues to shave his chest. Kramer warns Jerry that shaving will only accelerate the hair's growth. After Jerry was invited on the Peterman Reality Tour, Alex notices that Jerry's beard has grown. While Kramer is at the garbage dump as the full moon comes out, Jerry's chest begins itching from hair growth as Kramer searches for a dump that'll take the muffin stumps. Jerry's itching makes him run off the bus, past Kramer, and into the forest to scratch his chest. When the moon comes out, he howls like a werewolf while sighing at the relief that he can scratch his chest out in the woods.

### Top of the Muffin to You!

While eating a muffin at J. Peterman's book signing, Elaine mentions to Mr. Lippman (Richard Fancy), her ex-boss who was also attending the book signing, that she only eats the tops and that a store selling just the tops would be a million dollar idea. Lippman decides to start a business called "Top of the Muffin to You!," based on Elaine's idea. Elaine is outraged that he stole her idea. When the business starts to fail, Lippman asks her for advice and bribes her with 30% of the profits. She tells him that he must make the whole muffin and then pop the top from the stump; she also demands that he remove the exclamation point from his sign ("It's not top of the muffin TO YOU!" she says. Lippman replies, "No, no, it is." in a recall to "The Sniffing Accountant.") This gives the business a boost but leaves them with the problem of disposing of the muffin stumps. They initially give the stumps to the homeless shelter. But after complaints about the missing muffin tops by charity worker Rebecca DeMornay, they have to get rid of them somewhere else.

### The Peterman Reality Tour

Kramer learns from Laney that the stories he'd sold to J. Peterman in a previous episode were put into Peterman's biography. Kramer goes to the book signing, claiming he's the "real" Peterman until he's kicked out. He then starts conducting "The Peterman Reality Tour" on a school bus for $37.50 apiece. Kramer asks Jerry and his girlfriend to take the tour. While Elaine enters, she asks Kramer to get rid of the muffin stumps on his tour in return. Kramer has a hard time finding a dump that will take the bag of muffin stumps. At the latest dump, Jerry runs out of the bus and by him as Kramer asks "What's the matter?"

### Credits

When Mary Ann comes into the bar, the bartender asks if she is looking for George. When she says yes, the bartender tells her that George is still in the bathroom. George is seen in the bathroom using its phone to call Jerry asking him to get him some clothes while mentioning that Steinbrenner traded him to Tyler Chicken. Mary Ann pops her head into the bathroom and comments that she told him that New York would "eat him alive."

Elaine eventually hires a "cleaner" for Mr. Lippman to make the muffin stump problem go away until the store gets private trash removal. The "cleaner" is Newman in a Pulp Fiction spoof as he asks for an 8 oz. glass from Mr. Lippman that would help him in removing the muffin stumps. Newman then uses four different bottles of milk when consuming the muffin stumps.

## Cultural references

David Sims of The A.V. Club describes this as "a spoof-heavy episode".

Newman's role as "The Cleaner" is a reference to Harvey Keitel's role as "The Wolf" in Pulp Fiction. He is seen driving an Acura NSX when he arrives at the bakery, which is the same kind of car that "The Wolf" drives. There is a character known as "The Cleaner" played by Harvey Keitel in Point of No Return. Newman uses a crate of milk bottles to help dispose of the muffin stumps, which is a reference to Jean Reno's role as "Victor the Cleaner" in the film Nikita (upon which Point of No Return is based), who uses a crate of acid bottles to dispose of murder victims. This also refers to 'The Cleaner' in Leon (a.k.a. The Professional) where he is a cleaner and he "takes care of problems" and is only ever seen consuming milk. Finally, the use of milk is a reference to a series of advertisements then running for milk, in which various characters can eat cake only when they have milk to drink on hand.

The joke was spoofed again in the 2001 B-movie Deadly Scavenger, in which Tim Sullivan plays "The Doctor," who was hired because his job is "to clean things up."

There is also a reference to the film Wolf, with Jerry Seinfeld playing the role played by Jack Nicholson.

Kramer's "Peterman Reality Tour" is a direct spoof of "Kramer's Reality Tour," conducted by the real-life Kenny Kramer, the former neighbor of Larry David and inspiration for the character Cosmo Kramer. Lippman gripes that "every half-wit and sitcom star has his own book out now," a reference to Seinfeld's own book Seinlanguage. The title of J. Peterman's autobiography, No Placket Required is a parody of the Phil Collins album No Jacket Required.

Originally, George was going to claim to be an employee of the real Tyson Chicken. However, when Seinfeld inquired about the right to use their name, executives at Tyson Chicken objected to the joke about "alcoholic chicken" and the fictional "Tyler Chicken" was used in its place. One Tyson Foods Inc. spokesman, Archie Schaffer, told business news reporter Bill Bowden, "We told them the only problem we had with the whole concept was the alcoholic chickens concept didn't make a whole lot of sense to us, and it wasn't funny. Is that something you drink or a drunk chicken?" Schaffer suggested using "chicken brewskis" instead; another spokesman, Ed Nicholson, remarked, "There were some inaccuracies. George was supposed to have worked on the hen line. Of course, there is no hen line."

The actor Reuven Bar-Yotam, who is seen on Kramer's "Peterman Reality Tours" bus, also appears as Shlomo in season nine's "The Frogger". The "Jiffy Dump" employee is the same man who was in charge of "Jiffy Park" in the 7th season episode "The Wig Master." The conversation even references that episode as George is told to "take it up with consumer affairs" in "The Wig Master" and Kramer can be heard yelling, "Maybe I will take it up with consumer affairs!" after being turned away.

Elaine's exchange with Lippman regarding the use of exclamation points is a reference to an earlier episode in which Elaine and Lippman had a related dispute while working at Pendant Publishing. In the earlier dispute, Lippman was against the use of exclamation points and Elaine was for it.

Lippman's declaration that the idea for selling muffin tops was "all in the air" is a reference to the cult horror film Body Parts, wherein a deranged painter (played by Brad Dourif) vehemently insists that the inspiration for his gruesome artwork is "in the air," and not derived from the amputated arm of a death row serial killer that had been transplanted onto him.

On the tour bus, Kramer mentions that his window is "the one that's covered with chicken wire." Later on, a scene shows the CEO's office at Tyler Chicken. There is the shadow of a window covered with chicken wire. The chicken wire is later referenced in the episode "The Voice."

## Cast

#### Regulars

Jerry Seinfeld ...................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus .............. Elaine Benes  
Michael Richards .................. Cosmo Kramer

#### Recurring

Wayne Knight ................. Newman  
John O'Hurley ................. J. Peterman  
Richard Fancy .................. Mr. Lippman

#### Guests

Rena Sofer ........................... Mary Anne  
Melinda Clarke..................... Alex  
Chaim Jeraffi ........................ Jiffy Dump Guy  
Sonya Eddy ......................... Rebecca  
Barry Kramer ...................... Bag Guy  
Elayn Taylor ........................ Book Fan  
Paige Moss ........................... Girl  
Viola Kates Simpson ............. Old Woman  
Jack Riley ............................. Rider  
Bunny Summers ................... Rider  
Earl Carroll .......................... Rider  
Norman Brenner .................. Rider  
Reuven Bar .......................... Foreign Guy  
Vince Donvito ...................... Passerby  
Chris Burmester ................... Passerby  
Deck McKenzie .................... Man  
Victoria Fischer .................... Bartender  
Keith Sellon-Wright .............. Guy

## Script

[Jerry and George walking down the street.]

JERRY: Hang on just let me pick up a paper.

MAN: Excuse me. Would you mind watching my bag for a minute?

GEORGE: Yeah. No problem.

JERRY: Let's go.

GEORGE: Woah, I gotta watch this guy's bag.

JERRY: For how long?

GEORGE: I'm sure he'll be back in a second.

JERRY: Come on.

GEORGE: Excuse me sir. Would you mind watching my bag for a minute?

MAN 2: Why? So I can stand here like an idiot not knowing if you'll ever come back?

(Jerry starts to leave.)

GEORGE: Where are you going?

JERRY: I'm going to be this guy's friend.

[Jerry and George at Monk's]

JERRY: New clothes?

GEORGE: Yeah. I did some shopping. Some new clothes shopping. (turns to a man) Can I borrow your menu?

JERRY: Strange. For new pants, there's noticeable wear on the buttocks of those chinos. Wait those are the clothes from the bag!

GEORGE: The guy never came back.

JERRY: He asked you to watch them not wear them.

GEORGE: I'm still watching them.

JERRY: You look like a tourist.

GEORGE: Alright, let me ask you something: When do you start to worry about ear hair?

JERRY: When you hear like a soft rustling.

GEORGE: It's like puberty that never stops. Ear puberty, nose puberty, knuckle puberty, you gotta be vigilant. Let me ask you this: Do you know where Walker Street is downtown? I've got a league meeting there.

JERRY: Oh right, the new job, how is it?

GEORGE: I love it. New office, new salary. I'm the new Wilhelm.

JERRY: So who's the new you?

GEORGE: They got an intern from Francis Louis High. His name is Keith. He comes in Mondays after school.

JERRY: Oh hi Alex.

ALEX: I'm sorry I'm late. Have you ordered yet?

JERRY: No.

ALEX: I'll be right back.

GEORGE: Where are you meeting these women? When they get off the bus at the port authority?

JERRY: Right here, George. In here. (pointing to his chest) Try opening this up. You'll find the biggest dating scene in the world.

GEORGE: Thanks. Thanks a lot.

[Kramer in Jerry's apartment. Kramer searches in Jerry's couch for something and picks it up. Elaine enters. Kramer hurriedly puts the cushions back on the couch.]

KRAMER: Hey.

ELAINE: Hey.

KRAMER: Hi.

ELAINE: Where's Jerry?

KRAMER: Well he's in the shower. You want me to get him?

ELAINE: No. No no. Actually I kind of need to speak to you.

KRAMER: Well let's sit down.

ELAINE: Kramer, ahem, remember that whole deal with you selling Peterman your stories for his book and then he gave them back to you?

KRAMER: Vaguely.

ELAINE: Well I was kind of, heh, short on material and I, um, I put them in the book anyway.

KRAMER: You put my life's stories in his autobiography?

ELAINE: Kramer listen, it is such a stupid book. It doesn't matter.

KRAMER: Oh no. Sure. It matters. Wow. I've broken through, huh. I'm part of popular culture now. Listen I've got to thank Mr. Peterman.

ELAINE: He's doing a book signing at Waldenbooks this afternoon.

KRAMER: Waldenbooks? That's a major chain huh.

(Kramer enters the bathroom.)

KRAMER: He Jerry, I'm going to Waldenbooks.

JERRY: (yelling) Get out! Get out! I don't want to live like this.

KRAMER: Alright, let's go.

[At Waldenbooks.]

ELAINE: Mr. Lippman, how are you?

MR. LIPPMAN: Well I'm not bad. Not bad.

ELAINE: What are you doing here?

MR. LIPPMAN: I work for Pendant Publishing. This is our book.

ELAINE: Oh.

MR. LIPPMAN: If you can call it that. Why is it every half-wit and sitcom star has his own book out now?

KRAMER: Hey buddy. Remember me?

MR. PETERMAN: You're that gangly fellow we bought the stories from.

KRAMER: Yeah, I'm just here to do my part. What's your name darling?

WOMAN:: Who are you?

KRAMER: I'm the real Peterman.

MR. PETERMAN: Alright playtime's over.

KRAMER: Relax man. There's enough juice here to keep us all fat and giggley.

WOMAN: I can't believe somebody pulled the top off of this muffin.

ELAINE: That was me. I'm sorry. I don't like the stumps.

MR. LIPPMAN: So you just eat the tops.

ELAINE: Oh yeah. It's the best part. It's crunchy, it's explosive, it's where the muffin breaks free of the pan and sort of (makes hand motions) does it's own thing. I'll tell you. That's a million dollar idea right there. Just sell the tops.

(Two men forcefully pick Kramer up and push him out of the store.)

KRAMER: I have a right to be here. These are my fans. Hey you're hurting my elbow.

(George is walking down the street looking down at his map. He is bumping into people.)

MAN 1: Try looking up hayseed.

MAN 2: You wanna sightsee? Get on a bus.

MARY ANNE: Please don't think all New Yorkers are so rude.

GEORGE: Well actually I'm...

MARY ANNE: I'm Mary Anne. I work for the New York Visitor's Center. Where are you visiting from?

GEORGE: Little Rock, Arkansas.

MARY ANNE: Ooh.

[Jerry is in his bathroom shaving. He thinks. He tilts the mirror down.]

JERRY: Hmm. That looks new.

(He thinks some more. He picks up his razor.)

KRAMER: So get this. Peterman has his henchmen forcefully eject me from the book signing like I'm some kind of a maniac.

JERRY: (uncomfortably) Yeah that's too bad.

KRAMER: What's the matter with you?

JERRY: (uncomfortably) Nothing.

KRAMER: No, no, no. Don't give me that. I know you. Something's wrong. What is it.

JERRY: I did something stupid.

KRAMER: What did you do?

JERRY: Well I was shaving. And I noticed an asymmetry in my chest hair and I was trying to even it out. Next thing I knew, (high pitched voice) Gone.

KRAMER: Don't you know you're not supposed to poke around down there.

JERRY: Well women do it.

KRAMER: (high pitched voice) &quot;Well women do it.&quot; I'll tell you what. I'll pick you up a sundress and a parasol and you can just (high pitched voice) sashay your pretty little self around the town square.

JERRY: Well what am I going to tell Alex?

KRAMER: Listen to me. You don't tell anybody about this. No one. You hear me?

JERRY: Mmhmm.

(George enters)

KRAMER: Hey, Jerry shaved his chest.

JERRY: Hey!

KRAMER: I forgot. Wait. Never mind.

(Jerry and Alex walking.)

ALEX: How about the beach this weekend?

JERRY: You couldn't pay me enough to go to the beach on a weekend. I mean it's hard enough...

ALEX: Alright. Alright. Wow is that a Mexican Hairless? Oh, I love those. Ooh, Hairless. This is where it's at. It's so much smoother and cleaner.

JERRY: Really?

[Elaine walks into a muffin shop]

ELAINE: &quot;Top of the Muffin to you!&quot;?

MR. LIPPMAN: Top of the muffin to you. Elaine!

ELAINE: Mr Lippman?

[George and Jerry at Jerry's apartment.]

JERRY: So you're pretending to be a tourist?

GEORGE: It's beautiful. She makes all the plans. I'm not from around here so it's okay if I'm stupid, and she knows I'm only in town visiting so there's no messy breakup

JERRY: How do you explain your apartment?

GEORGE: I got a hotel room.

JERRY: You moved into a hotel?

GEORGE: Well I don't know anyone here Jerry. Where else am I going to stay?

JERRY: So get this: we're in the park today, Alex goes wild for this hairless dog.

GEORGE: So?

JERRY: So. I figure since she likes one hairless animal why not another.

GEORGE: Oh really. You tell her you shaved it?

JERRY: Are you nuts? I don't want her to think I'm one of those low-rise briefs guys who shaves his chest.

(Kramer is in a school bus. He honks his horn. Camera shot down on the bus.)

KRAMER: (yelling up at Jerry) Hey Jerry.

(Jerry pulls up the blinds on his upstairs window and looks down.)

KRAMER: (yelling) I'm starting a Peterman Reality Bus Tour. Check it out. Hahaha.

GEORGE: Reality tour?

JERRY: The last thing this guy's qualified to give a tour of is reality.

[Elaine at the muffin shop]

ELAINE: This was my idea you stole my idea.

MR. LIPPMAN: Elaine these ideas are all in the air. They're in the air.

ELAINE: Well if that air is coming out of this face then it is my air and my idea.

MR. LIPPMAN: You want a muffin or not?

ELAINE: Peach.

[George and Mary Anne at Monks]

MARY ANNE: So I notice you don't have much of an accent.

GEORGE: Yeah my parents have it. Sometimes it skips a generation.

MARY ANNE: Look George, I'm really enjoying spending time with you but I'm not sure this is going to work out. At some point you're going back to your job at Tyler Chicken and your three-legged dog Willie.

GEORGE: Willie. Yeah.

MARY ANNE: And I'm still going to be here.

GEORGE: Well what if I told you I'm thinking of moving here?

MARY ANNE: (laughs) George, no offense. But this city would eat you alive.

JERRY: You're moving to New York? That's fantastic. I can see you all the time now.

GEORGE: Eat me alive, huh? We'll see who can make it in *this* town.

JERRY: What is it she think you can't do?

GEORGE: Find a job. Get an apartment.

JERRY: How did you do those things?

GEORGE: Never mind. They're done. All I have to do now is redo them. You know if you take everything I've ever done in my entire life and condense it down into one day, it looks decent.

JERRY: Hey, what were you doing with that bus yesterday?

KRAMER: Here you go, here you go, check it out.

JERRY: &quot;The Real Peterman Reality Bus Tour&quot;. I'm confused.

KRAMER: Peterman's book is big business. People want to know the stories behind the stories.

JERRY: Nobody wants to go on a three hour bus tour of a totally unknown person's life.

KRAMER: I'm only charging $37.50, plus you get a pizza bagel and desert.

GEORGE: What's desert?

KRAMER: Bite-size Three Musketeers. Just like the real Peterman eats.

GEORGE: He eats those?

KRAMER: No. I eat those. I'm the real Peterman.

GEORGE: I think I understand this. Jay Peterman is real. His biography is not. Now, you Kramer are real.

KRAMER: Talk to me.

GEORGE: But your life is Peterman's. Now the bus tour, which is real, takes you to places that, while they are real, they are not real in sense that they did not *really* happen to the *real* Peterman which is you.

KRAMER: Understand?

JERRY: Yeah. $37.50 for a Three Musketeers.

[Elaine and Mr. Lippman at Monk's]

MR. LIPPMAN: Elaine. I'm in over my head. Nobody likes my muffin tops.

ELAINE: So? What do you want me to do about it?

MR. LIPPMAN: You're the muffin top expert, tell me what I'm doing wrong.

ELAINE: Mr. Lippman, when I worked for you at Pendent Publishing, I believed in you, you know as a man of integrity. But, I saw you in that paper hat and that apron...

MR. LIPPMAN: What if I cut you in for 30% of the profits?

ELAINE: Deal. Here's your problem. You're making just the muffin tops. You've gotta make the *whole* muffin. Then you... Pop the top, toss the stump. Taste.

MR. LIPPMAN: Ah. (takes a bite of the top.) Mmmmm. Ah hah?

ELAINE: Yeah.

MR. LIPPMAN: So what do we with the bottoms?

ELAINE: I don't know, give em to a soup kitchen.

MR. LIPPMAN: That's a good idea.

ELAINE: And one more thing, you really think we need the exclamation point? Because, it's not &quot;Top of the Muffin *TO YOU!!!*&quot;

MR. LIPPMAN: No. No. It is.

[At Jerry's apartment]

KRAMER: Hey Jerry. What is this? Lady Gillette? What's going on?

JERRY: What? Can't I get a moment's peace?

KRAMER: What are you doing to yourself?

(Jerry walks into camera view with his chest covered with shaving cream.)

JERRY: I can't stop. Alex thinks I'm naturally hairless.

KRAMER: You can't keep this up. Don't you know what's going to happen? Every time you shave it, it's going to come in thicker and fuller and darker.

JERRY: Oh that's an old wives tale.

KRAMER: Is it? Look at this.

(Kramer walks off-screen and opens his shirt. On-screen, Jerry reels from the sight.)

KRAMER: (high pitched voice) Look at it! Look at it! And it's all me. I shaved there when I was a lifeguard.

JERRY: Oh come on. That's genetics. That's not going to happen to me.

KRAMER: Won't it? Or is it already starting to happen?

[Elaine at the muffin shop]

ELAINE: Wow. Look at this. We're cleaning up.

LIPPMAN: Oh, Rubin, get me another tray of low-fat cranberry.

REBECCA: Excuse me, I'm Rebecca Demore from the homeless shelter.

ELAINE: Oh, hi.

REBECCA: Are you the ones leaving the muffing pieces behind our shelter?

ELAINE: You been enjoying them?

REBECCA: They're just stumps.

ELAINE: Well they're perfectly edible.

REBECCA: Oh, so you just assume that the homeless will eat them, they'll eat anything?

MR. LIPPMAN: No no, we just thought...

REBECCA: I know what you thought. They don't have homes, they don't have jobs, what do they need the top of a muffin for? They're lucky to get the stumps.

ELAINE: If the homeless don't like them the homeless don't have to eat them.

REBECCA: The homeless don't like them.

ELAINE: Fine.

REBECCA: We've never gotten so many complaints. Every two minutes, &quot;Where is the top of this muffin? Who ate the rest of this?&quot;

ELAINE: We were just trying to help.

REBECCA: Why don't you just drop off some chicken skins and lobster shells.

ELAINE: I think I might.

[Mary Anne and George at George's &quot;new&quot; apartment.]

MARY ANNE: I can't believe you found something so quickly. How much you pay?

GEORGE: $2300.

MARY ANNE: Ouch. A month?

GEORGE: Yeah.

MARY ANNE: Well, guess that's alright for now, but if you say here for more than a few months, you're a real sucker.

GEORGE: Yeah, well I uh got lots of other stuff to show you too. Wait till you see the plum job that I landed.

MARY ANNE: Yeah. We should let this place air out anyway. It smells like the last tenant had monkeys or something.

(Mary Anne exits. George sniffs his armpit.)

[On Kramer's bus.]

KRAMER: Coming up on the right, if you glance up you can just make out my bedroom window. It's the one that's covered in chicken wire.

WOMAN: Hey if you're the real Peterman, how come you're wearing those ratty clothes? They're not very romantic.

KRAMER: (over the speaker) Well that's your opinion.

MAN 1: Can I have another Three Musketeers? They're rather small.

KRAMER: Forget it. Okay Newman's postal route is around here somewhere.

MAN 2: Who's Newman?

MAN 3: Who cares.

MAN 4: Hey fake Peterman, let me off. I'm nauseous.

MAN 1: Can I have his candy bar?

KRAMER: Ahh. Everyone just settle down. We have three hours left on this thing, and I can't drive and argue with you rubes all at the same time. Okay. Lomez's place of worship is right on the right here.

[At Jerry's apartment.]

JERRY: Why do I have to go on the tour?

KRAMER: Jerry you're a minor celebrity. If you go on this thing, it could create a minor stir. Bring that girlfriend of your and I'll only charge to 60 bucks.

(Elaine enters)

JERRY: Hey, how's business?

ELAINE: Ooh, I've got stump troubles. The Sanitation Department won't get rid of them all, I can't get a truck to haul this stuff until next week. Meanwhile, I'm sitting on a mountain of stumps.

KRAMER: Alright, I've got to hose the puke off the floor of the bus.

ELAINE: Bus? Wait a minute, wait a minute, bus? You've got a bus?

KRAMER: Yeah.

ELAINE: You got any room on that thing?

KRAMER: Yeah there are a few seats still available.

ELAINE: Do you think you could transport some stumps for me? I'll make it worth your while.

KRAMER: Well, if they don't mind sitting in the back.

ELAINE: No they don't.

KRAMER: Are they war veterans?

(Elaine looks at him confused.)

[In George's office.]

MARY ANNE: Wow this is your office.

MR. STEINBRENNER: Woah. Hello. Sorry George, didn't know you got a girl in here. Give me a signal on the doorknob like a necktie or a sock or something. Come on George, help me out.

MARY ANNE: Mr. Steinbrenner, I would like to thank you for taking a chance on a hen supervisor at Tyler Chicken like our boy George here.

MR. STEINBRENNER: Hen supervisor from Tyler Chicken?

GEORGE: Yes. Very nice to have had her to mention... (starting to leave)

MR. STEINBRENNER: Wait a minute George.

GEORGE: Be right with you. Look Mr. Steinbrenner.

MR. STEINBRENNER: Moonlighting for Tyler Chicken. Pretty impressive George. Days with the New York Yankees and nights in Arkansas with a top flight bird outlet. And a hen supervisor to boot. I am blown. Bloooown away. Blown George. (vibration in the &quot;o&quot;'s) Bloooooooooooooooooooown.

[On Kramer's bus.]

ALEX: You know when you make a pizza bagel, you really shouldn't use cinnamon raisin.

JERRY: You also shouldn't use a donut.

(Kramer gets on the bus. He starts the tape player playing banjo music.)

KRAMER: Alright ladies and gentlemen. Welcome to the Peterman Reality Tour...

TAPE PLAYER: Turn music off.

JERRY: Can we just go?

KRAMER: And go we will.

MAN: What is this? A piece of pound cake?

KRAMER: We have a bonus reality stop today. We will be hauling muffin stumps to the local repository.

MAN 2: We're going to a garbage dump?

KRAMER: And we're off.

JERRY: You know I never though he would be able to recreate the experience of actually knowing him, but this is pretty close.

[Mr. Steinbrenner is sitting at his desk on the phone with the manager at Tyler chicken who is also sitting at his desk.]

MR. STEINBRENNER: (the back of his head to the camera) John Tyler? George Steinbrenner here. I want to talk about George Costanza. I understand he's been dividing his time between us and you. I cannot have that.

JOHN TYLER: (the back of his head also to the camera) Well I don't know who he is but if you want him that bad I'm not giving him up that easily.

MR. STEINBRENNER: Oh is that so. Playing a little hardball huh Jonnyboy?

JOHN TYLER: How about this. You give me Costanza, I convert your concessions to all chicken no charge. Instead of hot dogs, chicken dogs. Instead of pretzels, chicken twists. Instead of beer, alcoholic chicken.

MR. STEINBRENNER: How do you make that alcoholic chicken?

JOHN TYLER: Let if ferment, just like everything else.

MR. STEINBRENNER: That stuff sounds great. Alright. I'll have Costanza on the next bus.

[Kramer at a garbage dump carrying a garbage bag.]

MAN: Hey hey hey hey hey. Where do you think you're going?

KRAMER: I was going to dump this.

MAN: It doesn't look like garbage.

KRAMER: Well it's muffin stumps

MAN: Where are the muffin tops?

KRAMER: This is a garbage dump. Just let me dump it.

MAN: Can't do it.

KRAMER: Is this a joke?

MAN: That's what I'd like to know about it.

ALEX: You have a pretty heavy beard, don't you?

JERRY: What's that?

ALEX: Well look it's almost time for you to shave again.

JERRY: Oh. Yeah.

KRAMER: (gets back on the bus, yelling) Well maybe I will take it up with Consumer Affairs. Ladies and Gentlemen you're in for an additional treat. We're going to extend the tour at no extra charge.

MAN: Where are we going?

KRAMER: (looking at a map) I don't know. (over the speaker) Uh, no more questions.

(Banjo music plays as they look for garbage dumps.)

[Next scene. Kramer argues with someone at a dump.]

[Next scene. A man vomits on the floor.]

[Next scene. Kramer is driving. He is sleepy. His head nods down onto the horn. The horn blows. Startled, Kramer sits back up. Banjo music finishes.]

WAITRESS: So, the New York Yankees traded you for a bunch of Tyler chicken.

GEORGE: Dogs, twists, a kind of fermented chicken drink.

MAN: Hey, aren't you the guy I asked to watch my clothes?

GEORGE: What clothes?

MAN: These clothes. The ones you're wearing.

[On Kramer's bus]

JERRY: (in low voice to next to Kramer) Kramer how much longer? My chest hair is coming back and it's itching me like crazy. I can't let her see me scratch it.

KRAMER: Don't worry. I've got a good feeling about this dump.

JERRY: I'm telling you man, I'm losing it.

(Kramer gets off the bus, carrying a garbage bag.)

(Eerie music is playing. Jerry looks out the bus window at a full moon. A dog starts barking.)

JERRY: I can't sit on this bus anymore. I think I'll go play with that dog.

KRAMER: I don't know where the tops are.

(Jerry runs past Kramer and another person. Eerie music still playing.)

KRAMER: Jerry what's the matter?

(In slow motion Jerry runs into the woods. At normal speed he runs behind a tree. Camera shot down on him as he starts scratching his chest.)

JERRY: (for the first half of the howl, a dog howls along with him.) Awoooooo-oooooooo, that feels good.

BARTENDER: Hey, you looking for George?

MARY ANNE: Yeah.

BARTENDER: He's been in the bathroom awhile. You might want to check on him.

GEORGE: (talking on the phone) Jerry you gotta bring me some clothes down here. I lost my job with the Yankees. I'm standing in the men's room on 43rd street in my underpants.

MARY ANNE: I told you this city would eat you alive.

[At the muffin shop.]

MR. LIPPMAN: What is this guy again?

ELAINE: They call him a Cleaner. He makes problems go away.

(Newman enters. )

NEWMAN: Hello Elaine. Where are they?

ELAINE: In the back.

NEWMAN: Alright, I'm going to need a clean 8 ounce glass.

MR. LIPPMAN: What is going on here?

NEWMAN: If I'm curt, then I apologize. But as I understand it, we have a situation here and time is of the essence.

(Newman goes to the back room with the muffin stumps and sets down a cooler and an empty glass. From the cooler he takes out 4 bottles of milk and sets them down. He bites into a stump, then takes a drink of milk from the glass. (continuity error: he never actually poured the glass of milk.) He swishes the muffin and the milk together and swallows. He takes another stump.)

The End
