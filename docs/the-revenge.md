---
layout: default
title: The Revenge
parent: Season 2
nav_order: 7
permalink: /the-revenge
cat: ep
series_ep: 12
pc: 212
season: 2
episode: 7
aired: April 18, 1991
written: Larry David
directed: Tom Cherones
imdb: http://www.imdb.com/title/tt0697766/
wiki: https://en.wikipedia.org/wiki/The_Revenge_(Seinfeld)
---

# The Revenge

| Season 2 - Episode 7   | April 18, 1991           |
|:-----------------------|:-------------------------|
| Written by Larry David | Directed by Tom Cherones |
| Series Episode 12      | Production Code 212      |

"The Revenge" is the seventh episode of the second season of the NBC sitcom Seinfeld, and the show's 12th episode overall. The story revolves around George Costanza's (Jason Alexander) plot to exact revenge on his boss, with his friend Elaine Benes' (Julia Louis-Dreyfus) help, after he quits his job at Rick Barr Properties and is refused re-employment. Meanwhile, Jerry (Jerry Seinfeld) and his neighbor Kramer (Michael Richards) get even with a laundromat owner—who they believe has stolen money from Jerry—by pouring cement into one of his washing machines.

Written by series co-creator Larry David and directed by Tom Cherones, the episode premiered in the United States on NBC on April 18, 1991. Largely based on David's own experiences, "The Revenge" was the first episode he wrote without Seinfeld's collaboration. The episode also contained the first mention of Newman, as a suicidal man who lives in Jerry and Kramer's apartment building, who would later become a popular recurring character. As the episode is the first in which Kramer does physical comedy, some cast and crew members consider it a turning point for the show. When first broadcast in the United States, the episode gained a Nielsen rating of 14.4/24 and was met with positive response from critics.

## Plot

The episode relates three parallel plots, in intertwining scenes. The first plot concerns George being banned from the executive toilet. Because of this he quits his job, but immediately regrets the decision. He discusses job opportunities with Jerry, but is unable to think of an occupation that would suit him. Jerry suggests that George could try to go back to work and pretend he never quit. George takes this advice, but his former boss, Rick Levitan (Fred Applegate), refuses to let him stay and insults him. As revenge, George decides to slip a Mickey Finn into his drink during an office party, and enlists Elaine Benes to help him. At the party, Elaine distracts Levitan while George puts the mickey in his drink. When Levitan notices George, however, he decides he was unreasonable and tells George he can have his job back. George attempts to intercept the drink, but after Levitan welcomes him back with a toast sprinkled with insults at George's expense, he changes his mind. In the following scene we see George once again brainstorming job opportunity ideas, the subtext being that his boss discovered the spiking of his drink, connected it to George, and has fired him once again.

The second plot of the episode revolves around Jerry; when he prepares to go to the laundromat, Kramer asks him to take his laundry with him. Jerry agrees after some reluctance, insisting that their clothes remain segregated. After retrieving the laundry the following day and returning Kramer's portion, Jerry remembers that he had hidden a large sum of money in his laundry bag, but is unable to find it. The owner of the laundromat (John Capodice), tells him that he did not see the money, but also points out that he is not responsible for valuables. Kramer and Jerry both assume Vic stole the money and Kramer comes up with a plan to put cement mix in one of Vic's washing machines as revenge. Once they have acted out the plan, Kramer discovers that he had the money all along; and it turns out to be just enough to cover the damage to the washing machine.

In a subplot, Kramer tells Jerry about his suicidal friend Newman who repeatedly threatens to kill himself by jumping off the apartment building. When he does jump, he jumps from the second floor and survives, much to Kramer's amusement. At the end of the episode, Newman threatens to jump again, Kramer asks Newman if he wants to go shoot some pool with him, but Newman declines, stating that he has plans to go to the movies.

## Production

"The Revenge" was written by series co-creator Larry David and directed by Tom Cherones. All prior Seinfeld episodes were co-written by Seinfeld and David. The Revenge is the first episode written by David alone, though Seinfeld did proofread the script and would continue to do so for all scripts up to the eighth season. George's storyline in the episode is based on David's own experiences while a writer at Saturday Night Live. David had quit SNL halfway through the 1984-1985 television season, but felt he had made a mistake once he reached his home. His neighbor Kenny Kramer, who later served as the main inspiration for Kramer, suggested David return to work the following day and act as if nothing had happened. Unlike George Costanza, the ploy succeeded for David, who remained with SNL's writing staff until the end of that season. George's reason for quitting was inspired by Seinfeld writer Larry Charles' use of the private restroom in Seinfeld and David's office instead of the public one. The Newman sub plot was inspired by one of David's neighbors, who once jumped from the second floor of the apartment building in which they both lived. The unseen character Mr. Papanickolas, who is mentioned by Kramer, was named after Pete Papanickolas, a member of the production crew.

"The Revenge" was first read by the cast of the show on February 13, 1991 and was filmed in front of a live audience on February 20, 1991. Filming of the episode had been delayed two days due to President's Day. Both Alexander and Louis-Dreyfus praised the scene in which Jerry and George discuss the types of employment George could apply for after he quit his job. Louis-Dreyfus stated she was jealous that she was not in the scene. A number of scenes in the episode were removed prior to broadcast, such as one in which George and Kramer meet in the hallway and Kramer informs George that Jerry has gone to the laundromat. The writers decided that George could just say Kramer told him Jerry was at the laundromat and, upon that addition, the scene was cut. Initially, during Jerry and George's conversation about jobs, George mentions Regis Philbin, when they discuss George being a talk show host. Additional dialogue between George and Jerry at the laundromat was also removed. Because the episode "The Stranded" did not air until mid season three, the few references "The Revenge" contained to the episode, were cut. The Newman subplot was significantly reduced; the character initially appeared in one scene, but it was never filmed. In that scene, he would have explained to Jerry and Kramer that he jumped from the roof, but an awning broke his fall, though Jerry and Kramer would remain skeptical. The episode also involved the second appearance of Harold the building superintendent, who had previously appeared in "The Apartment". Harold would inform the main characters that Newman made up the story about the awning breaking his fall, though, with the reduction of the Newman subplot, the scene was removed.

The cast considered the episode a turning point for the show. As a method actor, Richards insisted on dumping a real bag of cement into the washing machine used on set, so that the proper physical reactions to such a heavy object would be present. Richards stated that at that point, "rather than talking funny, I wanted to do funny". During the first take of the scene, Richards fell through a door, and it had to be filmed again. "The Revenge" is also the first episode in which the George and Elaine characters collaborate. Louis-Dreyfus later stated that she and Alexander immediately had "some sort of shorthand with one another comedically, and [she] really relished that."

Although Newman's appearance was ultimately cut from the episode, auditions were held for the role; Tim Russ, who would go on to star in Star Trek: Voyager, auditioned, as did William Thomas, Jr., known for his appearance on The Cosby Show, who was cast in the part. Newman does share a brief dialogue with Kramer at the end of the episode, David recorded the lines, though he was not credited. The show's writing staff did not intend to have the character return in any later seasons, but because the idea of having actor Wayne Knight as a neighbor appealed to them, they re-cast Knight in the role of Newman for the season 3 episode "The Suicide". Afterwards, Knight re-recorded Newman's lines for the syndicated version of this episode to establish better continuity. Both Knight's and David's dialogue were included on the Seinfeld: Volume 1 DVD boxset. Additionally, Fred Applegate guest-starred as George's boss and John Capodice portrayed Vic, the laundromat owner. Deck McKenzie, who worked as Seinfeld's stand-in, portrayed George's colleague Bill. Teri Austin portrayed Ava, a co-worker of George's; she would appear again later in "The Stranded", which was filmed as part of season two, but aired as part of season three. Patrika Darbo, who played George's co-worker Glenda, would reappear later in the season five episode "The Sniffing Accountant" as a woman Newman flirts with.

## Reception

First broadcast in the United States on NBC on April 18, 1991, Nielsen Media Research estimated that the episode gained a Nielsen rating of 14.4 and an audience share of 24. This means that 14.4% of American households watched the episode, and that 24% of all televisions in use at the time were tuned into it. Seinfeld was the 15th most-watched program of the week it was broadcast in, and the sixth most-watched program broadcast on NBC. Entertainment Weekly reviewers Mike Flaherty and Mary Kaye Schilling gave the episode a mixed review and graded it with a C, stating "Although neat for its parallel plotting and George's hilariously clueless career chats with Jerry, 'The Revenge' is not so sweet". IGN critic Andy Patrizio considered "The Revenge" one of his personal favorites of season two. The scene in which Kramer struggles to put cement in one of the washing machines has gained positive responses from critics. Margery Eagan of The Boston Globe cited the scene as a perfect example of Kramer's personality. Neal Justin of the Minneapolis Star-Tribune also considers the scene to be one of the show's "classic moments". Daily News of Los Angeles critic Jody Leader also praised Seinfeld for how he distracted Vic in the scene.

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Guests

Fred Applegate ................. Levitan  
John Capodice .................. Vic  
Teri Austin ........................ Ava  
Patrika Darbo ................... Glenda  
Marcus Smythe ................ Dan  
John Hillner....................... Greeny  
Deck McKenzie.................. Bill  
Larry David.........................Voice of Newman (uncredited)

## Script

[Opening Monologue]

JERRY: Whenever I see the news and they're hauling in some kind of terrorist, psycho, maniac, mass murderer guy. You notice he's always covering up his face with the newspaper, with the jacket, with the hat. What is he worried about? I mean what is this man's reputation? That he has to worry about this kind of exposure damaging his good name? I mean, what is he up for a big job promotion down at the office or something? Afraid the boss is gonna catch this on TV and go &quot;isn't that Johnson from sales? He's up in that clock tower picking people off one by one. I don't know if that's that kind of man we want heading up that new branch office. He should be in bill collection. I think he's got aptitude.&quot;

[Office complex; Levitan's office]

LEVITAN: (on the phone) Ha ha, she was great. You don't want to know. Hey Breaky, remind me to tell you what we did in Lake George. (laughing) Get this... I got it all on video. (laughing)

(George bursts into the office)

GEORGE: That's it. This is it. I'm done. Through. It's over. I'm gone. Finished. Over. I will never work for you again. Look at you. (laughing) You think you're an important man? Is that what you think? You are a laughingstock. You are a joke. These people are laughing at you. You're nothing! You have no brains, no ability, nothing! (knocking object over on desk) I quit!

[Jerry's Apartment]

(Jerry exits his bedroom with a bag of laundry; Kramer enters)

KRAMER: Hey.

JERRY: Hey.

KRAMER: Boy, I have really had it with Newman. He wakes me up again last night at three o'clock in the morning to tell me he's going up onto the roof to kill himself.

JERRY: Well, what'd you say?

KRAMER: I said &quot; Jump.&quot; Well, he's been threatening to do this for years. I said &quot; Look, if you're gonna kill yourself do it already and stop bothering me.&quot; At least I'd respect the guy for accomplishing something.

JERRY: What's his problem?

KRAMER: No job. No women.

JERRY: He called the right guy.

KRAMER: Well, what am I supposed to tell him? How much there is for him to live for? Why should I lie to him?

JERRY: Alright, I'm leaving. I going to the laundry.

KRAMER: Why don't you use the machines down in the basement?

JERRY: Fluff and Fold. The only way to live. (snapping fingers in tune with words) I drop it off. I pick it up. It's a delight.

KRAMER: How 'bout if I put a few things --

JERRY: Wait a sec. I don't wanna do --

KRAMER: Well, you're going over there.

JERRY: I don't wanna mix in everything! My guys don't know your guys. You can't just lock 'em all in the same machine together. They'll start a riot.

KRAMER: Have you ever met my guys?

JERRY: No. I can't say as I have.

KRAMER: Well!

JERRY: Alright. Put 'em on top.

KRAMER: Ah!

JERRY: Oh, beautiful.

[Laundromat]

JERRY: This stuff on top is my friends. Could I get it done in a separate machine?

VIC: I'll have to charge you for another machine.

JERRY: Whatever it costs. In fact, I would prefer it if the machines are not even touching each other. Because something could, you know, jump across.

(George shows up)

GEORGE: Guess what.

JERRY: How did you know I was here?

GEORGE: Kramer. Guess what.

JERRY: I don't know.

GEORGE: I quit my job.

JERRY: Get outta here.

GEORGE: I couldn't take it anymore.

VIC: You can have this on Monday. (hands Jerry a ticket)

JERRY: What happened? Levitan?

GEORGE: I go in to use his private bathroom, everybody uses it, and then I get a memo - a memo - telling me to use the men's room in the hall. Well, (laughing) I mean we share it with Pace Electronics. It's disgusting!

JERRY: You and your toilets.

GEORGE: I snapped! It was the last straw. (sighs)

JERRY: So, what are you gonna do now? Are you gonna look for something else in real estate?

GEORGE: Nobody's hiring now. The market's terrible.

JERRY: So what are you gonna do?

(George realizes he's made a mistake)

[Jerry's Apartment]

GEORGE: I like sports. I could do something in sports.

JERRY: Uh-huh. Uh-huh. In what capacity?

GEORGE: You know, like the general manager of a baseball team or something.

JERRY: Yeah. Well, that - that could be tough to get.

GEORGE: Well, it doesn't even have to be the general manager. Maybe I could be like, an announcer. Like a color man. You know how I always make those interesting comments during the game.

JERRY: Yeah. Yeah. You make good comments.

GEORGE: What about that?

JERRY: Well, they tend to give those jobs to ex-ballplayers and people that are, you know, in broadcasting.

GEORGE: Well, that's really not fair.

JERRY: I know. Well, okay. Okay. What else do ya like?

GEORGE: Movies. I like to watch movies.

JERRY: Yeah. Yeah.

GEORGE: Do they pay people to watch movies?

JERRY: Projectionists.

GEORGE: That's true.

JERRY: But you gotta know how to work the projector.

GEORGE: Right.

JERRY: And it's probably a union thing.

GEORGE: (scoffs) Those unions. (sighs) Okay. Sports... movies. What about a talk show host?

JERRY: Talk show host. That's good.

GEORGE: I think I'd be good at that. I talk to people all the time. Someone even told me once they thought I'd be a good talk show host.

JERRY: Really?

GEORGE: Yeah. A couple of people. I don't get that, though. Where do you start?

JERRY: Well, that's where it gets tricky.

GEORGE: You can't just walk into a building and say &quot;I wanna be a talk show host.&quot;

JERRY: I wouldn't think so.

GEORGE: It's all politics.

JERRY: Alright. Okay. Sports, movies, talk show host. What else?

GEORGE: This could have been a huge mistake.

JERRY: Well, it doesn't sound like you completely thought this through.

GEORGE: (sighs) Guess not. What should I do?

JERRY: Maybe you can just go back.

GEORGE: Go back?

JERRY: Yeah. Pretend like it never happened.

GEORGE: You mean just walk into the staff meeting on Monday morning like it never happened?

JERRY: Sure. You're an emotional person. People don't take you seriously.

GEORGE: Just..go back. Pretend the whole thing never happened.

JERRY: Never happened.

GEORGE: I was just blowin' off a little steam. So what?

JERRY: So what? You're entitled.

GEORGE: I'm emotional.

JERRY: That's right. You're emotional.

GEORGE: Never happened.

JERRY: Never happened.

[Middle Monologue]

JERRY: To me the most annoying thing about the couple of times that I did work in an office. Is that when you go in, in the morning you say Hi to everyone and for some reason throughout the day you have to continue to greet these people all day every time you see them. I mean you walk in &quot;morning Bill, morning Bob, how you doing? fine&quot; Ten minutes later you see him in the hall, &quot;How ya doin'?&quot; Every time you pass you gotta come up with another little greeting. You know you start racking your brains you know you do the little eyebrow &quot;Hey&quot; You start coming up with nicknames for them. &quot;Jimbo.&quot;

[Office complex; Staff Meeting]

(George enters)

GEORGE: How ya doin'?

GLENDA: What are you doing here?

GEORGE: What? I work here.

GLENDA: I thought you quit.

GEORGE: What quit? (laughing) Who quit?

DAN: Bill, how was your weekend?

BILL: Oh, excellent weekend. What about your weekend?

DAN: Fine weekend.

GEORGE: Yeah. Good weekend.

DAN: Went up to the Cape. Took the kids sailing. (laughing) Lisa was a little scared at first, but that kids' gonna be a good sailor someday.

GEORGE: Aw, she's gonna be a fine sailor.

(Levitan enters; George covers his face with a folder)

LEVITAN: Ava, what happened to you Friday afternoon?

AVA: Oh, I got a little tied up.

LEVITAN: I'll bet you did.

(laughter breaks out in boardroom)

LEVITAN: I wanna remind everyone that the tenth anniversary party for Rick Barr Properties is gonna be Wednesday afternoon at four o'clock in Lasky's Bar, on Madison 48th. I want all of you to be there. This really means a lot to me. Is that Costanza over there? What are you doing here?

GEORGE: What?

LEVITAN: Am I crazy, or didn't you quit?

GEORGE: When?

LEVITAN: Friday.

GEORGE: Oh, what? What? That? Are you kidding? I didn't quit. What? You took that seriously?

LEVITAN: You mean, laughingstock? All that stuff?

GEORGE: Come on. Will you stop it.

LEVITAN: No brains? No ability?

GEORGE: Teasing.

LEVITAN: Okay. I want you outta here.

GEORGE: I don't know where you're getting this from. I... you're serious aren't you? Oh, (laughing) you see? You see, you just don't know my sense of humor. Dan, don't I joke around all the time?

DAN: I wouldn't say all the time.

LEVITAN: You can't win. You can't beat me. That's why I'm here and you're there. Because I'm a winner. I'll always be a winner and you'll always be a loser.

[Laundromat]

GEORGE: &quot; I'll always be a winner and you'll always be a loser.&quot; This is what he said to me.

JERRY: Well, so that's that.

GEORGE: No. That's not that.

JERRY: That's not that?

GEORGE: No.

JERRY: Well, if that's not that, what is that?

GEORGE: I've got some plans. I got plans.

JERRY: What kind of plans?

GEORGE: What's the difference?

JERRY: You don't wanna tell me?

GEORGE: I'm gonna slip him a mickey.

JERRY: What? In his drink? Are you outta your mind? What are you Peter Lorre?

GEORGE: You don't understand. He's got this big party coming up. He's been looking forward to this for months. This is gonna destroy the whole thing.

JERRY: What if you destroy him?

GEORGE: No. No. No. No. No. Don't worry. It's perfectly safe. I researched it. He'll get a little woozy. He might keel over.

JERRY: Well, wha - what does that do? Big deal.

GEORGE: This is what they would do in the movies! It's a beautiful thing! It's like a movie! I'm gonna slip him a mickey!

JERRY: You've really gone mental.

GEORGE: Nah.

JERRY: Where are you gonna get this mickey? I can't believe I'm saying &quot;mickey&quot;!

GEORGE: I got a source.

JERRY: You got a mickey source?

GEORGE: And Elaine is gonna keep him busy.

JERRY: Elaine? How did you rope her into this?

GEORGE: I told her what a sexist he is. How he cheats on his wife.

JERRY: She knew that.

GEORGE: But she didn't know he doesn't recycle.

JERRY: What is the point of all this?

GEORGE: Revenge.

JERRY: Oh, the best revenge is living well.

GEORGE: There's no chance of that.

[Jerry's apartment]

(Kramer enters somewhat depressed)

JERRY: Did you get your laundry?

KRAMER: Yeah.

JERRY: What's with you?

KRAMER: He jumped.

JERRY: What?

KRAMER: Yeah. Newman jumped.

JERRY: Did he call you last night?

KRAMER: Oh, yeah. Yeah. Yeah. Yeah. Yeah. Yeah.

JERRY: What did you say?

KRAMER: I said &quot; Wave to me when you pass my window.&quot;

JERRY: Whew. Did he wave?

KRAMER: No! He jumped from the second floor. Mr. Papanickolous saw him from across the street. He's lying out there faking. See, he's trying to get back at me.

JERRY: (realizing something) Oh, my god!

KRAMER: What's the matter?

JERRY: (tearing through his laundry bag) Well, on Thursday when I came home I had $1500 on me. For some reason I decided to hide it in my laundry bag and then I completely forgot about it... and then I took the laundry in on Friday! Oh, come on, let's go.

KRAMER: Where? Where?

JERRY: To the Laundromat.

[Laundromat]

VIC: I never saw it.

KRAMER: Okay. Come on. Give the guy his money. What -- what are you doing?

VIC: Hey, you see that sign right there? (Points to a sign saying &quot;Not Responsible for Valuables&quot;

JERRY: Oh, I see. So, you put up a sign so you can do whatever you want? You're not a part of society.

VIC: Yea that's right, 'cuz this place is my country and I'm the president, and that's my constitution. I'm not responsible.

JERRY: So, anybody leaves anything here, you can just take it? You have a license to steal? You are like the James Bond of laundry?

VIC: You ever hear of a bank?

JERRY: Come on. Let's go.

KRAMER: No. You can't let him get away with this.

[Office party at the Bar]

(Elaine and George staking out the bar)

ELAINE: Which one is he?

GEORGE: That's him over there. The one that looks like a blowfish.

ELAINE: Oh, yeah. I see him.

GEORGE: Yeah. Hey, thanks for doing this.

ELAINE: Why pass up the opportunity to go to prison?

GEORGE: This is by far the most exciting thing I've ever done.

ELAINE: Yeah. It is kind of cool.

GEORGE: First time in my life I've ever gotten back at someone.

ELAINE: I can't believe we're doing this. This is the kind of thing they do in the movies.

GEORGE: That's exactly what I told Jerry!

ELAINE: Really?

GEORGE: Yes! (both laugh) God, I've never felt so alive!

[Laundromat]

(Jerry is carrying a large bag of clothes)

JERRY: Maybe we should call this off.

KRAMER: Come on. What's the big deal? Just gonna put a little concrete in the washing machine.

JERRY: And what's gonna happen?

KRAMER: Well, it'll gonna mix up with the water, and then by the end of the cycle it'll be a solid block!

JERRY: If only you could put your mind to something worthwhile. You're like Lex Luthor.

KRAMER: You keep him busy.

(Kramer, holding heavy bag of concrete in arms, stumbles to machine, knocking chairs around)

(Kramer plops concrete down onto washing machine)

KRAMER: Whoa!

(lifting bag into the air, Kramer, thrown off balance, stumbles backwards and slams into the dryer)

[Office party at the Bar]

(George and Elaine are in the corner talking)

GEORGE: You go over there -

ELAINE: Yeah.

GEORGE: You start flirting with him and I'll come by and, while you're keeping him busy, I'll slip it in his drink.

ELAINE: Wouldn't it be easier just to punch him in the mouth?

(George motions for her to do it. Elaine walks over to where Levitan is sitting)

LEVITAN: Come on! They're terrible. They got no infield.

ELAINE: Oops! (bumps into Levitan) 'Scuse me.

LEVITAN: Yeah.

GREENY: I'm gonna get some food. You want some?

LEVITAN: Nah.

ELAINE: Hi.

LEVITAN: Hi.

ELAINE: (sneezes)

LEVITAN: God bless you.

ELAINE: Oh! Thank you. Thank you very much. (blowing nose) Really. I mean that. I am not one of those people who give insincere thank you's. No sir. No sir. When I thank someone I really thank them. So, thank... yoooou!

LEVITAN: (confused) You're welcome.

ELAINE: People don't say &quot; God bless you &quot; as much as they used to. Have you noticed that?

LEVITAN: No.

ELAINE: (having trouble getting him to pay attention) So, I'm going to a nudist colony next week.

LEVITAN: (interested) Nudist colony?

ELAINE: Oh, yeah. Yeah. I love nudist colonies. They help me..unwind. Aah!

LEVITAN: (laughing) I'd never been to a nudist colony.

ELAINE: Oh, really? Oh, you should go. They're great. They're great. Of course, when it's over, it's - it's hard to get used to all this clothing, you know. So, a lot of times, I'll just lock the door to my office and I'll just sit there naked.

LEVITAN: Seriously?

ELAINE: Oh, yeah. I usually work naked a... couple hours a day.

(George makes his move; Glenda is occupying the seat next to Levitan)

GEORGE: (whispering) Glenda, can I ask you a favor? Can I have this seat?

GLENDA: (loud) What do you have to sit here for? There are plenty of other seats.

GEORGE: (whispering) I can't explain. It's very important that I sit here.

GLENDA: (loud) What are you doing here anyway? I thought you were fired.

GEORGE: (whispering angrily) Okay. Okay. Fine.

ELAINE: I cook naked, I clean... I clean naked, I drive naked. Naked. Naked. Naked.

LEVITAN: Who are you?

ELAINE: Oh, you don't wanna know, mistah. I'm trouble. Big trouble.

[Laundromat]

(Back to Kramer after hitting the dryer; Picks up the concrete makes his way back to the washer; opens the lid on the machine, it slams shut forcing Kramer to pour the concrete all over the machine)

JERRY: (trying to divert Vic's attention) What about the gentle cycle? You ever use that?

(opening lid, Kramer begins to contort &amp; flail arms about as a cloud of concrete covers his face)

JERRY: Do you think it's effeminate for a man to put clothes in a gentle cycle?

(Kramer emptying bag into machine)

JERRY: What about fine fabrics? How do you deal with that kind of temperament?

(finished with machine, Kramer walks to corner of room with half-full bag &amp; drops it on the floor)

(Kramer giving &quot;OK&quot; sign)

(Kramer starting machine &amp; trying to brush the mounds of concrete off of the machine)

JERRY: What about stone washing? You ever witness one of those? That must be something. What? Do they just pummel the jeans with rocks?

(Kramer walks over to where Jerry is standing, his entire body covered with dry concrete)

KRAMER: I didn't realize it was a full box.

[Office party; Bar]

GEORGE: (trying again with Glenda) I'm gonna count to three. If you don't give up the chair, the wig is coming off.

GLENDA: I don't' wear a wig.

GEORGE: One... (Glenda seeing George is serious; gets up and leaves)

ELAINE: No. No. No. No. No. I don't really have a phone. In fact, I - I really don't have an apartment. I kinda sleep around.

(Levitan &amp; Elaine laughing)

ELAINE: I just like to have and few drinks and just let the guy do whatever he wants. Would you close your eyes for a second? I wanna tell you a secret about my bra.

(George empties contents of tiny bottle into Levitan's drink)

(Elaine &amp; Levitan laughing)

GEORGE: Hello, Rick.

LEVITAN: Heh heh heh hey! Look who's here!

GEORGE: That's right, Ricky Boy, it's me!

LEVITAN: You know something, Costanza? I'm a very lucky man.

GEORGE: Oh!

LEVITAN: I've always been lucky. Things just seem to fall right in my lap.

GEORGE: Boom!

(all laughing)

LEVITAN: You wouldn't believe it if I told you. In fact, uh, I'm glad you're here. You know, maybe I've been a little rough on ya, huh?

GEORGE: Oh.

LEVITAN: Why should we let petty, personal differences get in the way of business? I, uh, I want you to come back. (George is shocked) You can use my bathroom anytime you want.

GEORGE: You want me to come back? Uh...

LEVITAN: Hey! How about a toast, huh? Everybody, a toast!

GEORGE: Rick.

LEVITAN: Everyone, I wanna propose a toast to ten great years at Rick Barr Properties.

GEORGE: Uh, Rick..

LEVITAN: And all the people in this room, (clears throat) that made that possible..

GEORGE: Rick.

LEVITAN: I'd also like to welcome back into the fold our..our little shrimpy friend, George Costanza who, although he didn't really have a very good year -- how you blew that McConnell deal, I'll never know. But, hey, what the hell, huh? We've always enjoyed his antics around the office. Heh heh. Anything you wanna add to this?

GEORGE: Drink up. (Levitan takes a drink)

[Jerry's Apartment]

(George is sitting at the coffee table; Jerry and Elaine on the couch)

GEORGE: I like history. Civil War. Maybe I could be a professor, or something.

ELAINE: Well, to teach something you really have to know a lot about it. I think you need a degree.

(Kramer enters)

JERRY: Yeah. That's true.

KRAMER: (seeing Jerry is with people) Oh.

JERRY: What? (Kramer hands Jerry an envelope) My God, the money! The 1500! Where'd you find it?

KRAMER: It was in my laundry.

JERRY: In your laundry the whole time? I told you not to mix in our guys. What did we figure the damage on that machine would be?

KRAMER: It was about 1200 bucks.

NEWMAN: Kramer!

KRAMER: Oh! That's Newman. (goes over to the window)

NEWMAN: I'm on the roof!

KRAMER: (yelling up) Well, what are you waiting for?

JERRY: Elaine, come on, take a walk with me down to the Laundromat. I gotta pay this guy the money..

(Elaine and Jerry leave)

GEORGE: (talking to nobody) I like horses. Maybe I could be a stable boy.

KRAMER: You wanna shoot some pool tonight?

NEWMAN: I can't. I'm goin' to a movie.

GEORGE: (talking to nobody) Nah. It's probably a union thing.

[Closing Monologue]

JERRY: People like the idea of revenge. Have you ever heard the expression 'The best revenge is living well' I've said this, in other words it means supposedly the best way to get back at someone is just by being happy and successful in your own life. Sounds nice, doesn't really work on that Charles Bronson. kinda level. You know what I mean, those movies where his whole family gets wiped out by some street scum. You think you could go up to him, 'Charlie forgot about the 357 what you need is a custom-made suit and a convertible. New carpeting, french doors, a divan. That'll show those punks.'

The End
