---
layout: default
title: The Pony Remark
parent: Season 2
nav_order: 2
permalink: /the-pony-remark
cat: ep
series_ep: 7
pc: 202
season: 2
episode: 2
aired: January 30, 1991
written: Larry David &amp; Jerry Seinfeld
directed: Tom Cherones
imdb: http://www.imdb.com/title/tt0697757/
wiki: https://en.wikipedia.org/wiki/The_Pony_Remark
---

# The Pony Remark

| Season 2 - Episode 2                    | January 30, 1991         |
|:----------------------------------------|:-------------------------|
| Written by Jerry Seinfeld & Larry David | Directed by Tom Cherones |
| Series Episode 7                        | Production Code 202      |

"The Pony Remark" is the second episode of the second season of the NBC sitcom Seinfeld, and the seventh episode overall. The episode was written by series co-creators Jerry Seinfeld and Larry David, based on a remark David made once.

In this episode, Jerry, at a relative's 50th-anniversary dinner, makes a tactless remark about hating anybody who had a pony when they were a child. His remark upsets the female guest-of-honor, causing her to storm out in anger. When the woman dies shortly after the dinner, Jerry and his friends wonder if the pony remark had something to do with her death.

The episode featured the first appearance of Jerry's uncle Leo (Len Lesser), who became a recurring character on the show. The episode also featured the first appearance of Barney Martin as Morty Seinfeld, replacing actor Phil Bruns, who had portrayed Morty in the season 1 episode "The Stake Out". "The Pony Remark" aired on January 30, 1991, and gained a Nielsen rating of 10.7/16. It gained positive responses from critics and The New York Times considers the episode a turning point for the show.

## Plot

Jerry's parents, Helen (Liz Sheridan) and Morty Seinfeld, are staying at Jerry's apartment in New York City, making themselves at home. He bursts in, wearing baseball clothes, carrying a bat and glove, and proudly tells them that during his softball game, "I made an incredible play in the field! There was a tag-up at third base and I threw the guy out from left field on a fly! We'll be in the championship game Wednesday because of me. It was the single greatest moment in my life."

Morty shares the reflected glory by remembering his own greatest moment; he is sympathetic, but Helen reminds them that they are all going to the 50th-anniversary dinner of Helen's second cousin Manya (Rozsika Halmos) and her husband Isaac (David Fresco). Though Jerry does not want to go—he has made plans, he doesn't know the people—his mother coerces him: "At least come and say hello, have a cup of coffee, then you'll leave." Jerry knows it won't be that simple, so he persuades Elaine to attend, too. (To their unspoken dismay, she is seated at the kiddie table, lower than the grown-ups.)

During the dinner, he makes the tactless comment to which the title refers:

> HELEN: I hear the fella owns a couple of racehorses. You know, trotters, like at Yonkers.
>
> JERRY: Horses? They're like big riding dogs.
>
> ELAINE: What about ponies? What kind of abnormal animal is that? And those kids who had their own ponies...
>
> JERRY: I know, I hated those kids. In fact, I hate anyone that ever had a pony when they were growing up.
>
> MANYA: (angry) I had a pony.
>
> (The room goes dead quiet.)
>
> JERRY: Well, I didn't, uh, really mean a pony, per se...
>
> MANYA: When I was a little girl in Poland, we all had ponies. My sister had pony, my cousin had pony... So, what's wrong with that?

Jerry tries to apologize, even going as far as to compare ponies with compact cars, but Manya gets even more angry and leaves the table. Just after she leaves, Jerry tries to reason with the others that he did not know she had a pony and wonders why immigrants with ponies would leave Europe to come to America without ponies ("Who leaves a country packed with ponies to come to a non-pony country?") After the dinner, when Jerry's parents are leaving, his father soothes: "Hey, I agree with him. Nobody likes a kid with a pony." But Jerry receives a phone call from Uncle Leo, who informs him that Manya has died. Jerry is very upset about it, but he's also upset to learn that the funeral will be held on the same day of his softball championship.

At Monk's Café, Jerry discusses the situation with George and Elaine. They speculate whether his comment may have been a factor, though both are self-centered: Elaine wonders about her own death, and George urges Jerry to play in the softball game, because he thinks Jerry needs "to play left field", as Jerry's replacement fielder "stinks," and, George adds, "I just don't see what purpose is it gonna serve your going? I mean, you think dead people care who's at their funeral? They don't even know they're having a funeral."

Feeling guilty, Jerry ends up going to the funeral, where he, again, apologizes for his remark. Isaac informs him that Manya had forgotten Jerry made the pony remark: "Oh, no no no. She forgot all about that. She was much more upset about the potato salad." Elaine asks Isaac multiple times about what is going to happen with their apartment. Isaac eventually tells her that Jerry's cousin Jeffrey is going to live in it. When it starts to rain, Jerry realizes that the game will be postponed. The following day, after the game, Jerry, George and Elaine meet at Monk's Café, where they discuss the lousy way Jerry played softball. (Jerry recalls a certain play, about which he admits, "It was the single worst moment of my life.") Elaine wonders if Manya's spirit put a spell on him.

In a subplot, Jerry and Kramer bet whether or not Kramer will rebuild his apartment so that it has multiple flat wooden levels instead of needing furniture. Kramer changes his mind and decides not to build levels, but refuses to pay Jerry, arguing that since he did not attempt it, the bet was invalid.

## Production

This episode was written by series creators Larry David and Jerry Seinfeld, in their second episode for this season, and directed by Tom Cherones, also his second episode this season, during the course of the second production season. This episode was based on a remark David once made during a conversation. Cherones deliberately made Elaine sit at a smaller table while directing the dinner scene. "The Pony Remark" was the first episode in which Kramer wants to gamble, it is later established that he has a gambling addiction. The idea of Elaine asking Isaac what is going to happen with his old apartment was added during rehearsals. The first table reading of the episode was held on October 24, 1990, and a run through was held two days later. "The Pony Remark" was filmed in front of a live audience on October 30, 1990, while Seinfeld's stand-up routine was filmed one day earlier, along with the performances used in "The Ex-Girlfriend" and "The Busboy"; Seinfeld would change wardrobe between takes.

"The Pony Remark" featured the second appearance of Helen and Morty Seinfeld, who had previously appeared in the season 1 episode "The Stake Out". In "The Stake Out", Morty was portrayed by Phil Bruns; however, David and Seinfeld decided they wanted the character to be harsher, and re-cast him with Barney Martin, who auditioned for the part on October 15, 1990 at 12.45 PM. Martin was unaware that another actor had already established the part. Helen was portrayed by Liz Sheridan; in an early draft of the episode, her name was Adele, though this did not match her name from "The Stake Out". It was later changed back to Helen. The episode also introduced Jerry's uncle Leo, portrayed by Len Lesser, who was known for his acting in gangster films, and also The Outlaw Josey Wales and Kelly's Heroes. When Lesser auditioned for the part on October 22, 1990, he got a lot of laughs from David, Seinfeld and casting director Marc Herschfield, but did not understand why, because he did not think his lines were funny. Herschfield stated that Lesser was the right actor for the part when Lesser had auditioned. David Fresco guest starred in the episode as Isaac. Fresco had some difficulty with his lines in the episode, and would sometimes burst into laughter during filming. Other actors who guest-starred in the episode were Rozsika Halmos, who portrayed Manya, and Milt Oberman, who played the funeral director.

## Reception

On January 30, 1991, "The Pony Remark" was first broadcast on American television. It gained a Nielsen rating of 10.7 and an audience share of 16. This means that 10.7% of American households watched the episode, and that 16% of all televisions in use at the time were tuned into it. The episode gained two Primetime Emmy Award nominations; Seinfeld and David were nominated for Outstanding Writing in a Comedy Series and Cherones was nominated for Outstanding Directing in a Comedy Series. Though the episode did not win either of its Emmy nominations, Seinfeld was praised for co-hosting the Emmy telecast.

Dave Kehr of The New York Times felt that "The Pony Remark" was a turning point for the show, stating that, after the first few episodes, the show "turn[ed] into something sharp and distinctive [...] Here, suddenly, is the tight knot of guilt and denial, of hypersensitivity and sarcastic contempt that Seinfeld would explore for the next eight years." Holly Ordway of DVD Talk considered the episode the best episode of Seinfeld's second season. "The Pony Remark" is considered one of Seinfeld's "classic episodes". Writing for Entertainment Weekly, critics Mike Flaherty and Mary Kaye Schilling called the episode "Seinfeld at its mordant best" and graded it with an A-.

In the book Something Ain't Kosher Here: The Rise of the "Jewish" Sitcom, Vincent Brook analysed the episode, saying, "Jerry is made to feel guilty for his 'lethal' pony remark, whence the episode's macabre humor; yet the moral in terms of ethno-spatial identity is clear. In its violent rejection of Manya, Seinfeld has driven descent-based ethnicities (and their legacy of privation and self-sacrifice) off the face of the earth, and literally off the air. There is no place for traditional Jewishness in the hedonistic Seinfeld world, "The Pony Remark" vociferously proclaims."

David Sims of The A.V. Club gave the episode an A, calling it a "classic" and writing that it "is so damn clever in how it bonds Jerry's fears about social niceties with larger fears about mortality"; he also praised Louis-Dreyfus's acting, saying that Elaine "has an amusingly stark little bit of dialogue about death midway through the episode: "You know, funerals always make me think about my own mortality and how I'm actually going to die someday. Me, dead. Imagine that!" I think it's probably Louis-Dreyfus' best moment of the show so far, because she's really starting to nail Elaine's declarative, vaguely imperious, self-centered tone." He also admired "the estimable Barney Martin in his first appearance as Jerry's irascible dad."

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Recurring

Liz Sheridan ................... Helen Seinfeld  
Morty Seinfeld ............... Barney Martin  
Len Lesser ....................... Uncle Leo

#### Guests

Rozsika Halmos ................. Manya  
David Fresco ....................... Isaac  
Scott N. Stevens ................. Intern  
Earl Boen ............................. Eulogist

## Script

INT. COMEDY CLUB - NIGHT

(Jerry is on stage, performing.)

JERRY: My parents live in Florida now. They moved there last year. They didn't want to move to Florida, but they're in their sixties, and that's the law. You know how it works. They got the leisure police. They pull up in front of the old people's house with a golf cart, jump out, "Let's go Pop, white belt, white pants, white shoes, get in the back. Drop the snow shovel right there. Drop it!" I am not much for the family gathering. You know, you sit there, and the conversation's so boring. It's so dull. And you start to fantasize. You know, you think, "What if I just got up and jumped out that window? What would it be like? Just crashed right through the glass." You know. Come back in, there's broken glass, everybody's all upset. "No, I'm alright. I was just a little bored there. And uh no, I'm fine. I came back. I wanted to hear a little more about that Hummel collection, Aunt Rose. Let's pick it up right there."

INT. JERRY'S APARTMENT - DAY

(Jerry's father, Morty, is watching television. His mother, Helen, is ironing his father's jacket.)

HELEN: You have so many nice jackets. I don't know why you had to bring this jacket. Who wears a jacket like this? What's wrong with that nice gray one? You have beautiful clothes. They sit in your closet. Morty, you can't wear this!

(Jerry's phone rings. Helen goes into the bathroom)

MORTY: Are you getting that?

HELEN: I thought you were getting it.

MORTY: Should I pick up?

HELEN: You want me to get that?

MORTY: I'll get it!

HELEN: I'll get it!

(They both go for the phone; Helen gets to it first.)

HELEN: Hello?... Hello?

(She hangs up. Jerry enters, wearing baseball clothes and carrying a bat and glove.)

JERRY: Hi.

HELEN: Hi.

JERRY: (to Morty) Would you make this thing lower! I can hear it on the street!

(Jerry picks up the remote and turns off the TV.)

MORTY: So, how'd you do?

JERRY: We won. I made an incredible play in the field! There was a tag-up at third base and I threw the guy out from left field on a fly! We'll be in the championship game Wednesday because of me. It was the single greatest moment in my life.

HELEN: This is your greatest moment? A game?

JERRY: Well, no. Sharon Besser, of course.

MORTY: You know what my greatest moment was, don't you? Nineteen-forty-six. I went to work for Harry Flemming and I came up with the idea for the beltless trenchcoat.

HELEN: Jerry, look at this sport jacket. Is this a jacket to wear to an anniversary party?

JERRY: Well, the man's an individualist - he worked for Harry Flemming. He knows what he's doing.

HELEN: But it's their 50th anniversary.

MORTY: Your mother doesn't like my taste in clothing.

HELEN: You know, I spoke to Manya and Isaac on the phone today. They invited you again. I think you should go.

JERRY: First of all, I made plans with Elaine.

HELEN: So bring her.

JERRY: I don't even know them. What is she, your second cousin? I mean, I've met them three times in my life.

MORTY: I don't know her either. (gesturing to Helen) She makes me fly all the way from Florida for this, and then she criticizes my jacket.

HELEN: At least come and say hello, have a cup of coffee, then you'll leave.

MORTY: How come he gets to leave?

JERRY: If I wind up sitting next to Uncle Leo, I am leaving. He's always grabbing my arm when he talks to me. I guess it's because so many people have left in the middle of his conversation.

MORTY: And it's always about Jeffrey, right?

JERRY: Yeah. He talks about him like he split the atom. The kid works for the Parks Department.

(Enter Kramer.)

KRAMER: Morty, are you coming in?

MORTY: Oh, yeah. I forgot all about it.

KRAMER: (to Jerry) Hey, how'd you do?

JERRY: We won. We're in the finals on Wednesday.

KRAMER: Yeah!

JERRY: (to Kramer and Morty) What is this about?

KRAMER: I'm completely changing the configuration of the apartment. You're not gonna believe it when you see it. A whole new lifestyle.

JERRY: What are you doing?

KRAMER: Levels.

JERRY: Levels?

KRAMER: Yeah, I'm getting rid of all my furniture. All of it. And I'm going to build these different levels, with steps, and it'll all be carpeted with a lot of pillows. You know, like ancient Egypt.

JERRY: You drew up plans for this?

KRAMER: No no. It's all in my head.

MORTY: I don't know how you're going to be comfortable like that.

KRAMER: Oh, I'll be comfortable.

JERRY: When do you intend to do this?

KRAMER: Ohh... should be done by the end of the month.

JERRY: You're doing this yourself?

KRAMER: It's a simple job. Why, you don't think I can?

JERRY: Oh, no. It's not that I don't think you can. I know that you can't, and I'm positive that you won't.

KRAMER: Well, I got the tools. I got the pillows. All I need is the lumber.

MORTY: Hey, that's some big job.

JERRY: I, don't see it happening.

KRAMER: Well, this time, this time you're wrong. C'mon. I'll even bet you.

JERRY: Seriously?

HELEN: I don't want you betting. Morty, don't let him bet.

KRAMER: A big dinner with dessert. But I've got till the end of the month.

JERRY: I'll give you a year.

KRAMER: No no no. End of the month.

JERRY: It's a bet.

(They "pinkie swear" to lock the deal.)

INT. MANYA &amp; ISAAC'S APARTMENT - EVENING

(Jerry sits between Uncle Leo and Elaine. Elaine is sitting at the kiddie table, lower than everyone else. Helen and Morty sit across from Jerry. Manya and Isaac sit at the head of the table. Other guests are present as well.)

JERRY: (to Elaine) Seriously, do you wanna switch chairs?

ELAINE: No, no. I'm fine.

(Uncle Leo grabs Jerry's arm.)

UNCLE LEO: Jerry, are you listening to this?

JERRY: Yeah, Uncle Leo.

UNCLE LEO: So, so, now the parks commissioner is recommending Jeffrey for a citation.

JERRY: Right. For the reducing of the pond scum?

UNCLE LEO: No, for the walking tours.

JERRY: Oh, yeah. Where the people eat the plant life. The edible foliage tour.

UNCLE LEO: That's exactly right. He knows the whole history of the park. For two hours he's talking and answering questions. But you want to know something? Whenever he has a problem with one of these high-powered big shots in the Parks Department, you know who he calls?

JERRY: Mickey Mantle?

(Uncle Leo is confused by this comment.)

ELAINE: (saving Jerry from Leo) Jerry, Jerry. Did you taste these peas? (to Manya) These peas are great!

JERRY: (eating a forkful) These peas are bursting with country fresh flavor.

ELAINE: Mmm... phenomenal peas.

MORTY: Are you ready for dessert?

JERRY: Well, actually, we do have to kind of get going.

MANYA: (surprised) You're going?

ELAINE: Oh uh, I don't really eat dessert. I'm dieting.

JERRY: Yeah, I can't eat dessert either. The sugar makes my ankles swell up, and I can't dance.

MANYA : Can't dance?

HELEN: He's kidding, Manya.

MANYA: Is that a joke?

HELEN: So, did you hear Claire's getting married?

MANYA: Yeah, yeah..

HELEN: I hear the fella owns a couple of racehorses. You know, trotters, like at Yonkers.

JERRY: Horses? They're like big riding dogs.

ELAINE: What about ponies? What kind of abnormal animal is that? And those kids who had their own ponies...

JERRY: I know, I hated those kids. In fact, I hate anyone that ever had a pony when they were growing up.

MANYA: (angry) I had a pony.

(The room goes dead quiet.)

JERRY: Well, I didn't uh really mean a pony, per se...

MANYA: When I was a little girl in Poland, we all had ponies. My sister had pony, my cousin had pony... So, what's wrong with that?

JERRY: Nothing. Nothing at all. I was just merely expressing...

HELEN: Should we have coffee? Who's having coffee?

MANYA: He was a beautiful pony! And I loved him.

JERRY: Well, I'm sure you did. Who wouldn't love a pony? Who wouldn't love a person that had a pony?

MANYA: You! You said so!

JERRY: No, see, we didn't have ponies. I'm sure at that time in Poland, they were very common. They were probably like compact cars..

MANYA: That's it! I had enough!

(Manya gets up from the table and exits.)

ISAAC: Have your coffee, everybody. She's a little upset. It's been an emotional day.

(Isaac exits. Everyone looks at Jerry.)

JERRY: I didn't know she had a pony. How was I to know she had a pony? Who figures an immigrant's going to have a pony? Do you know what the odds are on that? I mean, in all the pictures I saw of immigrants on boats coming into New York harbor, I never saw one of them sittin' on a pony. Why would anybody come here if they had a pony? Who leaves a country packed with ponies to come to a non-pony country? It doesn't make sense. Am I wrong?

INT. JERRY'S APARTMENT - DAY

(Morty and Helen are collecting their luggage.)

JERRY: I'll drive you to the airport.

HELEN: No, we're taking a cab.

JERRY: I just hope that whole pony incident didn't put a damper on the trip.

HELEN: Don't be ridiculous. It was a misunderstanding.

MORTY: Hey, I agree with him. Nobody likes a kid with a pony.

JERRY: Well, if you ever talk to her, tell her I'm sorry. Elaine too. She feels terrible.

HELEN: You know, you should give Manya a call.

JERRY: Maybe I will.

(Jerry opens his door. Kramer is standing in the hallway.)

KRAMER: Oh, hi. I uh just came to say goodbye.

(He points to the bags.)

KRAMER: Need any help with those?

MORTY: It's nothing. I got it. So, how are your levels coming along?

KRAMER: Oh, well... I decided I'm not gonna do it.

JERRY: (laughing) Really? What a shock.

HELEN: Goodbye, Jerry.

JERRY: Take care.

HELEN: We'll call you.

(Helen exits.)

MORTY: Bye, Jer.

JERRY: Bye, Dad. Take it easy.

MORTY: Bye, Mr. Kramer.

KRAMER: Yeah. So long, Morty.

(Morty leaves. Jerry closes the door. He and Kramer enter the kitchen. )

JERRY: So, when do I get my dinner?

KRAMER: There's no dinner. The bet's off. I'm not gonna do it.

JERRY: Yes, I know you're not gonna do it. That's why I bet.

KRAMER: Ya well, there's no bet if I'm not doing it.

JERRY: That's the bet! That you're not doing it!

KRAMER: Yeah, well, I could do it. I don't wanna do it.

JERRY: We didn't bet on if you wanted to. We bet on if it would be done.

KRAMER: And it could be done.

JERRY: Well, of course it could be done! Anything could be done! But it only is done if it's done! Show me the levels! The bet is the levels!

KRAMER: I don't want the levels!

JERRY: That's the bet! (The phone rings; Jerry answers it.) Hello?... No- oh, hi... (Kramer leaves) no, they just left... Oh, my God... hang on a second. Maybe I can still catch 'em. (Jerry goes over to the window and opens it; calling out the window) Ma! Ma! Up here! Don't get in the cab! Manya died! Manya died!!

INT. JERRY'S APARTMENT - LATER

(Helen and Morty are back with Jerry.)

HELEN: Who did you talk to?

JERRY: Uncle Leo.

HELEN: And when's the funeral?

JERRY: I don't know. He said he'd call back.

MORTY: You know what this means, don't you? We lost the Supersaver. Those tickets are non-refundable.

HELEN: She just had a check-up. The doctor said she was fine. Unless...

JERRY: What?

HELEN: What? Nothing.

JERRY: You don't think... What? The pony remark?

HELEN: Oh, don't be ridiculous. She was an old woman.

JERRY: You don't think that I killed her?

MORTY: You know what the flight back'll cost us?

JERRY: It was just an innocent comment! I didn't know she had a pony!

MORTY: Maybe we can get an army transport flight. They got a base in Sarasota, I think.

JERRY: The whole thing was taking out of context. It was a joke. (The phone rings.) That's probably Uncle Leo.

(Helen picks up.)

HELEN: Hello?... Yes, I know... Well, it's just one of those things... Sure, sure, we'll see you then.

(She hangs up.)

HELEN: The funeral's Wednesday.

JERRY: Wednesday? What, what Wednesday?

HELEN: Two o' clock, Wednesday.

JERRY: Ah

(Helen looks at Jerry.)

HELEN: What?

JERRY: I've got the softball game on Wednesday. It's the championship.

HELEN: So? You're not obligated. Go play in your game.

JERRY: I didn't even know the woman.

HELEN: So don't go.

JERRY: I mean I met her three times. I don't even know her last name.

HELEN: Jerry, no one's forcing you.

JERRY: I mean, who has a funeral on a Wednesday? That's what I want to know. I mean, it's the championship, I'm hitting everything.

HELEN: I don't have a dress to wear. (to Morty) And you. You don't have anything.

MORTY: I got my sport jacket.

HELEN: You're not wearing that to a funeral.

MORTY: What's wrong with it?

HELEN: It looks ridiculous.

MORTY: What? I'm gonna buy a new sport jacket now?

JERRY: I don't know what to do.

MORTY: (depressed) You know what this funeral's gonna wind up costing me? Oh boy!

INT. COMEDY CLUB - NIGHT

(Jerry is on stage, performing.)

JERRY: We don't understand death. And the proof of this is that we give dead people a pillow. And, uh, I mean, hey, you know. I think if you can't stretch out and get some solid rest at that point, I don't see how bedding accessories really make the difference. I mean, they got the guy in a suit with a pillow. Now, is he going to a meeting, or is he catching forty winks? I mean, let's make up our mind where we think they're going.

INT. MONK'S DINER - DAY

(Elaine and Jerry sit across from George at a booth.)

ELAINE: I actually like ponies. I was just trying to make conversation. What time's your game?

JERRY: Two Forty-Five.

ELAINE: And what time's the funeral?

JERRY: Two o' clock.

ELAINE: How long does a funeral take?

JERRY: Depends on how nice the person was. But you gotta figure, even Oswald took forty-five minutes.

ELAINE: So you can't do both?

JERRY: You know, if the situation were reversed and Manya had some mah-jongg championship or something, I wouldn't expect her to go to my funeral. I would understand.

ELAINE: How can you even consider not going?

GEORGE: You know, I've been thinking. I cannot envision any circumstances in which I'll ever have the opportunity to have sex again. How's it gonna happen? I just don't see how it could occur.

ELAINE: You know, funerals always make me think about my own mortality and how I'm actually gonna die someday. Me, dead. Imagine that.

GEORGE: They always make me take stock of my life. And how I've pretty much wasted all of it, and how I plan to continue wasting it.

JERRY: I know, and then you say to yourself, "From this moment on, I'm not gonna waste any more of it." But then you go, "How? What can I do that's not wasting it?"

ELAINE: Is this a waste of time? What should we be doing? Can't you have coffee with people?

GEORGE: You know, I can't believe you're even considering not playing. We need you. You're hitting everything.

ELAINE: He has to go. He may have killed her.

JERRY: Me? What about you? You brought up the pony.

ELAINE: Oh, yeah, but I didn't say I hated anyone who had one.

GEORGE: (to Jerry) Who's going to play left field?

JERRY: Bender.

GEORGE: Bender? He can't play left. He stinks. I just don't see what purpose is it gonna serve your going? I mean, you think dead people care who's at their funeral? They don't even know they're having a funeral. It's not like she's hanging out in the back going, "I can't believe Jerry didn't show up."

ELAINE: Maybe she's there in spirit. How about that?

GEORGE: If you're a spirit, and you can travel to other dimensions and galaxies, and find out the mysteries of the universe, you think she's gonna wanna hang around Drexler's funeral home on Ocean Parkway?

ELAINE: George, I met this woman. She is not traveling to any other dimensions.

GEORGE: You know how easy it is for dead people to travel? It's not like getting on a bus. One second. (snaps his fingers) It's all mental.

JERRY: Fifty years they were married. Now he's moving to Phoenix.

ELAINE: Phoenix? What's happening with his apartment?

JERRY: I don't know. They've been in there since, like, World War II. The rent's three hundred a month.

ELAINE: Three hundred a month? Oh, my God.

INT. FUNERAL HOME - DAY

(The eulogy is in progress. Jerry is in attendance.)

EULOGIST: Although this may seem like a sad event, it should not be a day of mourning. For Manya had a rich, fulfilling life. She grew up in a different world - a simpler world - with loving parents, a beautiful home in the country, and from what I understand, she eve had a pony. (Jerry throws up his hands.) Oh, how she loved that pony. Even in her declining years, whenever she would speak of it, her eyes would light up. It's lustrous coat, it's flowing mane. It was the pride of Krakow.

(Jerry sinks in his seat.)

INT. FUNERAL HOME - LATER

(Jerry and Elaine are talking to Helen. Jerry checks his watch.)

JERRY: Well, the game's starting just about now.

HELEN: It was good that the two of you came. It was a nice gesture.

(Morty is talking to his NEPHEW on the other side of the room.)

NEPHEW: I'm not a doctor yet, Uncle Morty. I'm just an intern. I can't write a note to an airline.

MORTY: You've got your degree. They don't care. They just want to see something.

(Jerry is talking to Isaac.)

JERRY: I just wanted to say how sorry I was...

(Uncle Leo interrupts.)

UNCLE LEO: Jerry, you wanna hear something? Your cousin, Jeffrey, is switching parks. They're transferring him to Riverside - so he'll completely revamp that operation, you understand?

JERRY: Yeah.

UNCLE LEO: He'll do in Riverside what he did in Central Park. More money. So, that's your cousin.

(Morty is still talking to his nephew.)

MORTY: You don't understand, I've never paid a full fare.

(Jerry and Elaine talk to Isaac.)

JERRY: Once again, I just want to say how sorry I am about the other night.

ELAINE: Oh, me too.

ISAAC: Oh, no no no. She forgot all about that. She was much more upset about the potato salad.

ELAINE: So, I understand you're moving to Phoenix?

(Jerry shakes his head and walks away.)

ISAAC: Yeah, my brother lives there. I think Manya would've liked Phoenix.

ELAINE: Mmm, gorgeous, exquisite town. So, what's happening with your apartment?

ISAAC: Of course it's very hot there. I'll have to get uh air conditioner.

ELAINE: Oh, you can have mine. I'll ship it out to you. (Isaac isn't listening to Elaine) But what about that big apartment on West End Avenue?

ISAAC: Although they say it's a dry heat.

ELAINE: Dry, wet... (trying to get through to him) what's happening with your apartment?

ISAAC: I don't even know if I should take my winter clothing.

ELAINE: I have an idea. Leave the winter clothing in the apartment, and I'll watch it for you and I'll live there and I'll make sure that nothing happens to it.

(Jerry returns.)

ISAAC: Oh, the apartment. Jeffrey's taking the apartment.

ELAINE: Oh, Jeffrey.

JERRY: You know Jeffery?

ELAINE : Yeah, from what I understand, he works for the Parks Department.

(Helen approaches Jerry.)

HELEN: It's raining.

(Jerry goes to the window.)

JERRY: It's raining? It's raining. The game will be postponed. We'll play tomorrow.

(Back to Morty and his nephew.)

MORTY: Believe me, I wouldn't bother you if the army hadn't closed that base in Sarasota. Here, scribble a little something here.

NEPHEW: I can't. I'll get in trouble.

MORTY: Oh, for God's sake!

INT. MONK'S DINER - DAY

(George, Jerry and Elaine are sitting at a table. Jerry and George are wearing baseball uniforms.)

GEORGE: Who gets picked off in softball? It's unheard of.

JERRY: It's never happened to me before.

ELAINE: I remember saying to myself, "Why is Jerry so far off the base?"

JERRY: I'll have to live with this shame for the rest of my life.

(George consults his stat sheet of the game.)

GEORGE: And then in the fifth inning, why did you take off on the pop fly?

JERRY: I thought there were two outs.

ELAINE: I couldn't believe it when I saw you running. (laughing) I thought maybe they had changed the rules or something.

JERRY : It was the single worst moment of my life.

GEORGE: (smiling) What about Sharon Besser?

JERRY: Oh, well, of course. Nineteen-seventy-three.

ELAINE: Makes you wonder, though, doesn't it?

JERRY: Wonder about what?

ELAINE: You know... (looking up) the spirit world.

JERRY: You think Manya showed up during the game and put a hex on me?

ELAINE: I never saw anyone play like that.

JERRY: But I went to the funeral.

ELAINE: Yeah, but that doesn't make up for killin' her.

GEORGE: Maybe Manya missed the funeral because she was off visiting another galaxy that day.

JERRY: Don't you think she woulda heard I was there?

GEORGE: Not necessarily.

(Pause.)

JERRY: Who figures and immigrant's gonna have a pony?

(Elaine laughs)

INT. COMEDY CLUB - NIGHT

(Jerry is on stage, performing.)

JERRY: What is the pony? What is the point of the pony? Why do we have these animals, these ponies? What do we do with them? Besides the pony ride. Why ponies? What are we doing with them? I mean, police don't use them for, you know, crowd control. (Jerry crouches down, and makes like he's riding a pony.) "Hey, uh, you wanna get back behind the barricades. Hey! Hey, little boy. Yeah, I'm talking to you. Behind the barricades!" So somebody, I assume, genetically engineered these ponies. Do you think they could make them any size? I mean, could they make them like the size of a quarter, if they wanted? That would be fun for Monopoly, though, wouldn't it? Just have a little pony and you put him on the... "Baltic, that's two down, go ahead. Hold it. Right there. Baltic. Yeah, that's it. Fine. Right there, hold it right there."

The End
