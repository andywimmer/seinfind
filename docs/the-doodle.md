---
layout: default
title: The Doodle
parent: Season 6
nav_order: 20
permalink: /the-doodle
cat: ep
series_ep: 106
pc: 618
season: 6
episode: 20
aired: April 6, 1995
written: Alec Berg & Jeff Schaffer
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697688
wiki: https://en.wikipedia.org/wiki/The_Doodle
---

# The Doodle

| Season 6 - Episode 20                | April 6, 1995             |
|:-------------------------------------|:--------------------------|
| Written by Alec Berg & Jeff Schaffer | Directed by Andy Ackerman |
| Series Episode 106                   | Production Code 618       |

"The Doodle" is the 106th episode of the NBC sitcom Seinfeld. This was the 20th episode for the sixth season. It aired on April 6, 1995.

## Plot

Jerry and George are having dinner with their new girlfriends, Shelly (Dana Wheeler-Nicholson) and Paula (Christa Miller). George met Paula at Elaine's drawing class at The New School. George whispers to Jerry that he is eating pecans that were in Shelly's mouth. Jerry spits them out and exclaims his disgust, which angers Shelly. When leaving the restaurant, George finds a "doodle" that Paula drew of him.

The next day at Monk's, George complains to Jerry about the doodle Paula drew, finding it an ugly caricature. Elaine enters and sees the doodle, thinking it is Mr. Magoo. Elaine reports that her friend, Judy, recommended her for a job at Viking Press. In order to stay at the company's suite at the Plaza Hotel she let Viking Press think that she is coming from out of town, and gave Jerry's parents' address in Florida as hers.

Jerry says his parents are coming to town at the same time. George implores Elaine to find out if Paula really likes him.

At his apartment, Jerry is scratching desperately. Kramer enters eating a "Mackinaw peach" from Oregon which is ripe for only two weeks a year. Jerry then realizes he has flea bites due to his unbearable itching just when his parents arrive.

At the drawing class, Paula confesses to Elaine that she likes George and says that "looks aren't that important to me." The exterminator at Jerry's apartment confirms the flea infestation, and has to close down the apartment for 48 hours to fumigate. Jerry convinces Elaine to give the hotel suite to his parents so they have a decent place to stay. George enters, asking about Paula's interest in him. Elaine says Paula really likes him but, without thinking, repeats Paula's comments about not caring about physical appearances. George becomes upset, declaring that Paula must think he is ugly, and states that he would prefer to be hated and handsome rather than liked and ugly.

The Seinfelds arrive at the Plaza and marvel at the suite. After meeting with Judy, Elaine realizes she has to go back to Jerry's apartment to retrieve a manuscript sent from Viking Press for her to read. Outside the apartment, Jerry warns her that it's impossible to enter because it has already been fumigated; an instant later Kramer walks out, having been unaware of the gas (he thought the sign on the door was to keep Jerry's parents out when he (Jerry) had a girl over). Once informed, Kramer worriedly says he spent an hour and a half in the apartment engrossed with a manuscript. Holding her breath, Elaine frantically searches inside, even flipping over the couch cushions but only finds "Chunky" candy bar wrappers. Seeing the wrappers, Jerry realizes that it was Newman who gave him fleas and goes to confront him. Newman first attempts to deny it, suggesting that Jerry must keep his apartment in a "state of disrepair" and "squalor", but soon succumbs to Jerry's torturous description of how badly flea bites itch, breaks down, and admits to being "rife with fleas". (In a deleted scene, Newman frantically confesses that he was ambushed with an adorable English bulldog named Beauford and caves in horror as Jerry smiles in delight and tosses the Chunky wrappers at the tortured postman.)

At the Plaza, Morty, Helen, Uncle Leo and Nana lavishly use room service, watch four pay-per-view movies at the same time, order $100 massages and food at great expense. Meanwhile, George confronts Paula on the street about her indifference towards looks; she confirms her feelings and states that George can drape himself in velvet for all she cares. George perks up at this notion (in "The Label Maker", he had expressed the desire to do just this). At Monk's, Elaine gets Kramer to summarize the manuscript, which he does in a typically eccentric fashion, leaving Elaine to despair. Suddenly, Kramer notices that he is unable to taste food due to the fumigation exposure.

Jerry is staying at Shelly's apartment but discovers that he has forgotten his own toothbrush; Shelly insists that he use hers. On the street, Kramer discovers with dismay that he cannot even taste a Mackinaw peach, to which Newman heartlessly responds that Kramer should then give them all to him. Elaine has her interview at Viking Press and repeats Kramer's manuscript interpretation, of which the publisher surprisingly approves. Back at the Plaza, the Seinfelds lay nearly comatose, satiated from excessive food and movies (although Morty is eager to watch Under Siege again).

George arrives at Monk's wearing a velvet sweatsuit, now very happy with his relationship with Paula. Jerry reports that he has been thrown out of his girlfriend's apartment for refusing to use her toothbrush and is now stuck staying with the "Velvet Fog" ("The Jimmy"). Back at her job interview, Elaine is questioned about the astronomical room charges at the Plaza, including adult films and room damages; Elaine desperately tries telling her publisher the reason why she sent Jerry's parents to the room due to the fleas, fumigation, and even Kramer unable to taste food, but the publisher brushes her off, thinking she "read one too many Billy Mumphrey stories." At Monk's, George enters finishing off a Mackinaw peach, discarding the pit on the table. When Paula casually pops the discarded pit into her mouth to suck out the remaining flavor, he gags with revulsion. Kramer's tastebuds return just in time for the last peaches; however, Newman has beaten him to it, finishing off the last one. Kramer exacts revenge by siccing a bulldog on him.

In the final scene, Elaine visits the messed up hotel suite; Uncle Leo sees her and, confused, states that he was expecting an Asian woman (perhaps as a reference to the earlier episode "The Chinese Woman")

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Cosmo Kramer

#### Recurring

Wayne Knight ................. Newman  
Barney Martin ................. Morty Seinfeld  
Liz Sheridan .................... Helen Seinfeld  
Len Lesser ...................... Uncle Leo

#### Guests

Billye Ree Wallace .................. Nana  
Christa Miller ......................... Paula  
Dana Wheeler-Nicholson ...... Shelly  
Guy Siner ............................. Mandel  
Ellis E. Williams ..................... Karl  
Coby Turner ........................ Judy  
Wayne C. Dvorak ................ Teacher  
Norman Brenner .................. Passerby

## Script

[Opening Monologue]

They call themselves exterminators but they can't really do it. The best they can do is get the bugs to somebody else's house. They just relocate them, y'know what I mean, they're bug realtors is what they are. Ahmmmm.. I'll think you'll be happy here, there's a lotta crumbs not much light, ahmmm they usually sleep through the night, so you'll really have a (?) of the place. Nobody really cares about killing insects, even the animal rights people don't care. You could probably walk into an animal rights organization meeting and hear a speech like; The only way to stop the cruelty of the scientific testing on animals is (slaps his face)...Ha! Got him, is to boycott these companies.

[Restaurant]

GEORGE: Jerry it's funny, Paula and I actually met because of Elaine.

PAULA: Elaine is in my drawing class at the new school

GEORGE: ..And I went down there one time to see...

JERRY: (cuts in) A nude model.

GEORGE: If Elaine wanted to get some coffee.

JERRY: You know I went out once with a nude model. Never let me see her naked. Hundreds of people see her naked every week, except me. Needless to say it was quite vexing.

GEORGE: Are you through?

JERRY: Yeah!

GEORGE: So Anyway, I started to compliment Elaine on her sketches and it turns out, they're Paula's.

PAULA: George, I just like to doodle.

(Jerry picks up a pecan that his girlfriend had just masticated.)

GEORGE: Oh! Dropped a napkin...(Whispers) Jerry!

JERRY: What?

GEORGE: What are you doing?...She had those nuts in her mouth, she just spit them out.

JERRY: (spits the nut) OooH!!! You. you ate these? You sucked on these and put them on the plate?

SHELLY: Well I didn't know you were gonna eat them?

JERRY: Soo...

SHELLY: I'm sorry you find me so repulsive?

JERRY: No, no I don't, I mean, Don't be silly..

SHELLY: Yeah!

JERRY: It's just...

SHELLY: Well, hmm, if you'll excuse me I think I'll just go to the ladies room.

PAULA: I'll join you.

JERRY: Oh, man did you see that? I ate discarded food.

GEORGE: Well I've done that.

JERRY: Yeah, but with you it's intentional.

GEORGE: Haven't you kissed her?

JERRY: Yeah, but this is different, this is like, you know, semi digested foodstuff. You know the next stop is the stomach and you can take it from there.

(George stops a passing busboy)

GEORGE: Excuse me just for a second. (fixes his hair looking at his reflection in a coffee pot.)

JERRY: AH. Yes that's gonna make a big difference.

GEORGE: This is dating, you can't leave anything to chance.

JERRY: Hey, you think that Shelly's upset that I made such a big deal about the pecan.

GEORGE: Hehummm, Yeah!

JERRY: Thanks.

GEORGE: No problem.

(The girls return from the ladies room.)

SHELLY: Well Jerry, I guess we should get going.

JERRY: Ah! boy.

GEORGE: Well, it was very nice meeting you Shelly and Jerry be careful, there's a lot of nuts out there. (to Paula) Alright you have everything?

PAULA: Can you grab my purse.

GEORGE: Yeah. (reaches for the purse and finds a piece of paper. he looks annoyed.)

[Monks, next day. George showing Jerry the piece of paper he picked up]

JERRY: Yeah, so?

GEORGE: Don't you see what this is?

JERRY: Yeah! It's a doodle.

GEORGE: Yeah! A doodle of me... Look at the size of the nose, the ears, all my features are distorted.

JERRY: Oh, it's an affectionate caricature.

GEORGE: I'm grotesque. I look like a troll.

JERRY: It's just a drawing.

GEORGE: Don't you see what this says? How can you possibly like somebody if you think they look like this?

(Elaine walks in)

GEORGE: (gets up to leave the table) HELLO!!! (angrily)

ELAINE: What is with him?

JERRY: The usual.

ELAINE: So, you know what? My friend Judy recommended me for a job at Viking Press.

JERRY: Good for you.

ELAINE: Yeah! But get this. Viking has a deal with the Plaza Hotel, they got a two bedroom suite, there, for out-of-town clients... So guess what I did?

JERRY: Oh come on, you told them you're from out-of-town just so you could stay in a hotel room.

ELAINE: I know, I know Jerry.. but it's The Plazaaa... I've never stayed there. It'll be like a little vacation.

JERRY: Well be sure to catch a Broadway show while you're in town.

ELAINE: (laughs) Listen, I've used your parents address in Florida.

JERRY: Oh, they're coming to town tomorrow by the way.

ELAINE: Hey, what's this?

JERRY: Don't ask.

ELAINE: What is it... A drawing of Mr. Magoo?

JERRY: No, it's George. (Elaine laughs heartily)

(George comes back)

ELAINE: It is...

GEORGE: You enjoying yourself? (More laughs from Elaine)

ELAINE: Sorry.

GEORGE: You see. You see! Listen when is your next drawing class?

ELAINE: Tomorrow.

GEORGE: Alright, I want you to do me a favor.

ELAINE: What?

GEORGE: I want you to find out if she likes me.

ELAINE: Find out if she likes you? What, are you in High School? George come on can't you just talk to her yourself?

GEORGE: But then she's gonna know that I like her more than she likes me.

JERRY: You know my parents are coming in and I got some cleaning up to do, so if you and Potsie are done scheming...

[Jerry's apartment]

KRAMER: Well, they're in...

JERRY: What's in?

KRAMER: The MACKINAW PEACHES, Jerry. The MACKINAW PEACHES!!!

JERRY: Ah ..right. The ones from Oregon that are only ripe for two weeks a year..

KRAMER: Yeah yeah I split a case with Newman... I waited all year for this. Oooh this is fantastic.. Makes your taste buds come alive... It's like having a circus in your mouth... Take a taste

JERRY: Nah, I don't wanna.

KRAMER: Come on, just take a taste.

JERRY: I don't want it..

KRAMER: Come on JUST TASTE!!!

JERRY: I DON'T WANT IT.

KRAMER: HE..YA. AYA. AYYYYYAAAAA!!!!

JERRY: I am not gonna taste your peach. I ate someone's pecan last night, I'm not gonna eat your peach.

KRAMER: Jerry, this is a miracle of nature that exists for a brief period. It's like the Aurora Borealis.

(Jerry scratches his ankle furiously)

JERRY: What is this?

KRAMER: What?

JERRY: Yeah! I think I got flea bites.

KRAMER: Flea bites?

JERRY: Look at this, my ankle's all bitten up.

KRAMER: You got a dog?

JERRY: No.

KRAMER: Well, that is strange.

JERRY: How could I have fleas?

KRAMER: Don't sweat it buddy... I used to have fleas.

JERRY: What did you do about them?

KRAMER: What do you mean?

[Jerry's parents come through the door.]

MORTY: Hey guys. Jerry, Kramer.

HELEN: Hi Jerry... What's wrong?

JERRY: Nothing...

HELEN: Jerry, I'm your mother, now what is it?

JERRY: Mom, Dad... I have fleas.

[Elaine's drawing class]

ELAINE: Hey Paula! I hear you been going out with George Costanza?

PAULA: How did you know?

ELAINE: Everybody knows. Y'know George told me he thinks you're totally cute and everything.

PAULA: He said that?

ELAINE: Mmhmm... Do you like George?

PAULA: Yeaaah! He's cool.

ELAINE: No I mean... Do you like him or do you like him like him?

PAULA: Like like.. Looks aren't important to me, you know?

TEACHER: Miss Benes, Are you chewing gum?

ELAINE: (nods) Mmhmmm...

(Elaine spits the gum in the waste basket then as the teacher turns around she makes a throwing up motion at Paula.)

[Later at Jerry's]

KARL: Yep, in your bedroom too Mr. Seinfeld. You've got a full outbreak of fleas on your hands.

JERRY: I don't get this. How did this happen? I don't have a dog.

KARL: I don't explain 'em Mr. Seinfeld. I just exterminate them.

JERRY: I don't understand this...

KARL: I'm gonna have to seal the place up for 48 hours and fog it. That's the only way to get rid of them.

JERRY: Nobody can be in here for 48 hours? I got my parents in town.

KARL: Well, unless you want to kill them. They can't stay in here. This stuff is pretty toxic. I'll go get my stuff, it's in the truck.

JERRY: Okay...

(Elaine comes in)

ELAINE: Hi...?

JERRY: Bug guy.

ELAINE: Why do you have a bug guy?

JERRY: I have fleas.

ELAINE: Ugh, fleas (strikes the purse she just deposited on the couch) How did you get fleas?

JERRY: I don't know... But everyone's got to clear out of the apartment for two days. I don't know what I'm gonna do with my parents. They'll never let me pay for a hotel and if they go to someplace on their own I'm sure it's gonna be some awful dump. Wait a second... Have you checked in The Plaza yet?

ELAINE: No... Oh No...

JERRY: Come on, c'mon.

ELAINE: No, No...

JERRY: c'mon, c'mon...

ELAINE: No, No...

JERRY: c'mon, c'mon....

ELAINE: No, No... Yesss!!!

JERRY: Yes!!!

(Door buzzer)

JERRY: Yes!!!

GEORGE: Yeah!

ELAINE: Well what about you? Where you gonna stay?

JERRY: I dunno, I'm gonna ask Shelley, but she still might be upset from the masticated pecan incident.

ELAINE: Hey!! I found out from Paula; She likes George. I'll bet he'll be relieved.

JERRY: Yeah.. When he's dead he'll be relieved... Oh by the way Viking Press sent a FedEx for you to my parents. They brought it with 'em.

ELAINE: Yeah that's just some stuff about the company.

GEORGE: Hmm.. (to Elaine) Hey! Did you talk to Paula?

ELAINE: Yeah.

GEORGE: So what did she say?

ELAINE: She... likes you..

GEORGE: She said she liked me. No kiddin' she said that?

ELAINE: Ya!

GEORGE: Those were her exact words, &quot;I like George&quot;.

ELAINE: Yep!

GEORGE: Ha Haaaaaaa... Jerry how do you like that? You see I get myself in a tizzy, I'm all worked up and for what?

ELAINE: For nothing..

GEORGE: Ha Ha..

ELAINE: In fact she said that looks aren't even that important to her...

GEORGE: You see... WHAT!?

ELAINE: Uh Ohh...

GEORGE: She said looks aren't important to her?

ELAINE: Well.. Umm... let me rephrase that. She said...

GEORGE: She thinks I'm ugly. I knew it.

JERRY: You see the thing of it is, there's a lot of ugly people out there walking around, but they don't know they're ugly, because nobody actually tells them.

GEORGE: ...So what's your point?

JERRY: I dunno...

ELAINE: Okay.. The point, George, is she likes you.

GEORGE: Oh, so what? I'd rather she hate me and thought I was good looking... At least I can get somebody else. (scratching his chest) What is this? Why am I itching?

JERRY: That'd be the fleas.

[At the Plaza Hotel]

JERRY: Hey!!!

MORTY: Hey! How do you like this? Huh, huh!

HELEN: Oh my god, Morty lets go, this is too nice.

MORTY: Hey! This is the kind of room Sinatra stays in. Hey look, Macadamia nuts.

HELEN: Macadamia nuts?

MORTY: Hey, you know what these cost? They're like 80 cents a nut.

HELEN: Jerry, are you sure this alright?

JERRY: Yeah, it's all taken care of.

MORTY: (from the other room) Hey!!! They got a phone in the john here.

[Outside Elaine's building]

ELAINE: Judy.

JUDY: Hey!

ELAINE: Hi. Thank you so much for recommending me to Viking Press.

JUDY: It is my pleasure, just make sure you give that manuscript a good read.

ELAINE: Manuscript?

JUDY: Yeah. I'm sure they FedEx'd you a manuscript. They want to see that you can read an unpublished work and give insightful criticism.

ELAINE: Oooh!!

JUDY: Read it twice if you have to. This is a big step in your career.

ELAINE: Yeah! hmmm.. I gotta go..

JUDY: Hey! What about lunch?

ELAINE: (she leaves hurriedly) I gotta gooo...

[Catches up with Jerry on the street.]

ELAINE: Thank god I found you.

JERRY: Oh! Hey!

ELAINE: You still got that FedEx?

JERRY: Yeah, I got it. It's in the apartment, but we can't go in there it's being fumigated.

ELAINE: No I'll take my chances. Come on... (Grabs him by the coat and head back to his place)

[Jerry's place. There is a sign on the door that says &quot;Fumigation Do not enter&quot;]

JERRY: You see?

ELAINE: Jerry, I need that FedEx right now..

JERRY: I told you to take it.

ELAINE: Well, I didn't know that it was a manuscript that I had to read...

JERRY: Well, you can't go in there it's like a gas chamber in there.

(Kramer comes out of the apartment.)

KRAMER: I left a Mackinaw peach in your refrigerator.

JERRY: Kramer, they're fumigating. There's toxic gas in there.

KRAMER: Toxic gas!!!

JERRY: Aw, you'll be fine, you were in there for what, a couple of minutes?

KRAMER: An hour and a half!!! I was reading a manuscript, I just couldn't put it down.

ELAINE: My manuscript?

JERRY: How do you feel?

KRAMER: Now that you mention it, a little woozy.

ELAINE: Kramer you gotta go back in there and grab my manuscript.

KRAMER: I'm not going back in there!!

ELAINE: Alright then, where is it?

KRAMER: I left it on the coffee table or somethin'.

JERRY: Well wh.. what are you doing?

ELAINE: I'm going in.

JERRY: Didn't you see the sign on the door?

KRAMER: Well I thought it was so your parents wouldn't walk in while you're with a girl.

(Elaine breathes heavily does the sign of the cross and enters. She goes to the coffee table then looks underneath the couch and finishes by his desk then the dinner table. She runs out of air so she has to rush out.)

ELAINE: (Heavy Panting) It's not on any table, Kramer. Where is it?

KRAMER: Well I don't know. I was in the bathroom, the kitchen...

ELAINE: Okay... bathroom... kitchen.

JERRY: Can you get me a soda?

(Elaine goes back in)

KRAMER: Jerry, I had some milk, I made a sandwich. I got to get out of the building.

(Elaine searches the kitchen, then the couch cushions and gives up.)

ELAINE: (again pants) I couldn't find it anywhere. How did you get fleas anyway?

JERRY: I don't know. Who could've been in my apartment.

ELAINE: I've looked everywhere, even under the couch but all I could find were the stupid Chunky wrappers. I couldn't...

JERRY: Wait a minute. Did you say Chunky wrappers?

ELAINE: Yeah!

JERRY: Let me see those. (smells them) Oh, I know the chunky that left these Chunkies... NEWMAN!!! I've got him.

[Newman's door. Jerry knocks]

JERRY: Newman. Open the door, Newman, I know you're in there.

NEWMAN: Hello Jerry. What a pleasant surprise.

JERRY: There's nothing pleasant about it, so lets just cut the crap.. You gave me fleas. I know it and you know it..

NEWMAN: Fleas? Bwa ha ha ha ha. That's preposterous. How can *I* give you fleas. Now if you don't mind...

JERRY: Oh, but I do. There's probably fleas crawling all over your little snack bar. (as he says this Newman is wildly scratching behind his back. He suddenly stops when Jerry turns around)

NEWMAN: So, you have fleas. Maybe you keep your house in a state of disrepair. Maybe you live in squalor.

JERRY: You know Newman, the thing about fleas is that they irritate the skin and they start to... itch. Oh, maybe you can hold out five seconds or ten, maybe fifteen or twenty but after a while, no matter how much will power a person may have. It won't matter, because they're crawling, crawling on your skin. Up your legs, up your spine, up your back...

NEWMAN: (Cannot take this torture anymore) Baaaaaaaaarrhhhhhhhhhhh... I'm rife with fleas (scratches furiously)

[Plaza Hotel, Morty is getting a massage.]

MORTY: Oh! Oh! That feels good. Hey! This guy charges a hundred bucks an hour but I'm telling ya he's worth every penny OOOOHH!

HELEN: I'm next.

MORTY: Hey! Leo, get this, four movies at once; Pay Per View.

LEO: I love these nuts.

NANA: This Champagne's gone flat. (throws her glass over her shoulder)

HELEN: Nana!!!

NANA: He Ha Ha Ha... Let the chambermaid clean it up!

[On the street in front of a flower shop]

GEORGE: Hello..

PAULA: What's the matter?

GEORGE: Well I spoke to Elaine...

PAULA: Hey look, no shave.

GEORGE: No... Why should that make any difference to you?

PAULA: It doesn't..

GEORGE: Of course not. You don't care what I look like.

PAULA: That's right I don't.

GEORGE: I suppose I could just pull this out (his tucked shirt) and walk around like this and you wouldn't care?

PAULA: Not a wit.

GEORGE: MmHmm? I suppose we could go to Lincoln Center and I'd be wearing sneakers and jeans and that would be fine too.

PAULA: You can wear sweatpants.

GEORGE: I could..

PAULA: (seductively) You could drape yourself in velvet, for all I care.

GEORGE: Velvet...

[Monks]

ELAINE: Did you read the whole thing?

KRAMER: Oh yeah.

ELAINE: Huh. So what's it about?

KRAMER: Well it's a story about love, deception, greed, lust and... unbridled enthusiasm.

ELAINE: Unbridled enthusiasm...?

KRAMER: Well, that's what led to Billy Mumphrey's downfall.

ELAINE: Oh boy.

KRAMER: You see Elaine, Billy was a simple country boy. You might say a cockeyed optimist, who got himself mixed up in the high stakes game of world diplomacy and international intrigue.

ELAINE: Oh my God.

KRAMER: Ah! Here we go.

ELAINE: (to the waitress) Can I have a scotch on the rocks?

KRAMER: May I.. (pointing to her food)

ELAINE: (feeling sick) Yeah! Go ahead.

KRAMER: What is this?

ELAINE: What? What are you doin'? (Kramer salting her food)

KRAMER: I can't taste this.

ELAINE: What are you talking about?

KRAMER: This food, it has no taste... nothin' I'm gettin' nothin'... (realizes) It must be the toxic gas from the fumigation... (he leaves paranoid and confused)

[At Shelly's]

JERRY: Hey, thanks a lot for lettin' me stay here.

SHELLY: Well, I don't keep pecans in the house so I didn't think it'd be a problem.

JERRY: (embarrassed laugh) ...Oh! Damn..

SHELLY: What's the matter?

JERRY: I forgot my toothbrush.

SHELLY: Oh, no problem... You can use mine.

JERRY: Yours?... You know what I'll think I'll brush later.

SHELLY: Brush now.

JERRY: (long pause) ...Sure. (hums a song then stares at the toothbrush)

[Kramer and Newman who is coming out of the market eating a peach.]

KRAMER: Newman, let me have a bite of your Mackinaw.

NEWMAN: What for, you got your own.

KRAMER: Come on, c'mon I need to taste it. (takes his peach) ...Nothin', can't even taste a Mackinaw.

NEWMAN: (resumes eating) Well that's a shame.

KRAMER: Waited all year and I can't even taste it..

NEWMAN: You can't taste 'em, why waste 'em? Why not give them all to me?

[At Viking Press.]

ELAINE: It's a story about love, deception, greed, lust and... unbridled enthusiasm.

MANDEL: Unbridled enthusiasm.

ELAINE: Yeah! ..tha..that's right. That that's what led to... (throat clearing) Billy Mumphrey's downfall.

MANDEL: Hmmm... interesting take. So you believe had he not been so enthusiastic he could have adverted disaster.

ELAINE: Yes...Ye..Yes..That's right... You see, Billy Mumphrey was a simple country boy. Some might say a cockeyed optimist, who got caught up in the dirty game of world diplomacy and International intrigue.

MANDEL: So.. It was more a question of attitude than politics..

ELAINE: Yes, yes Mr. Mandel.

[Plaza Hotel]

MORTY: Hey, Under Siege is on again. Whose up for it?

LEO: No more nuts. Awrghh...

[Monks. George walks in wearing velvet.]

JERRY: Oh my god. What the hell is this? ...Don't tell me... Velvet?

GEORGE: It's the real deal.

JERRY: She's seen you in this thing?

GEORGE: That's right... We just had sex... You know Jerry I've been searching for someone a long time. Well the search is over.

JERRY: And now the search for the right psychiatrist begins.

GEORGE: He he... So uh, what's with the suitcase?

JERRY: Ahh! She threw me out.

GEORGE: Why?

JERRY: I wouldn't use her toothbrush.

GEORGE: So where are you staying?

JERRY: Well I guess I'm stuck with the Velvet Fog.

[At Viking again.]

MANDEL: Three hours of massage time, twelve in-room movies including several adult features, five shoe shines and four hundred dollars worth of snacks. Not to mention the damage to the room.

ELAINE: Mr Mandel, you don't understand ...my my friend had fleas. I ran into the gas, it could have killed me, and my, my other friend couldn't taste his peaches, they're only good for two weeks.

MANDEL: I think, you've read one too many Billy Mumphrey stories. Good day Miss Benes.

ELAINE: Okay... Good day..

[Monks again. George comes in eating a Mackinaw peach.]

PAULA: Hi George.

GEORGE: (mouth full) Hi, this is fantastic (puts the pit in the a plate) You ever had a Mackinaw peach?

PAULA: Oh yeah I love those.

GEORGE: Too bad, it's all done.

(Paula takes the pit and puts it in her mouth. George stares in disbelief, then disgust.)

[Kramer is about to lick an envelope he is about to send.]

KRAMER: Ya... Yes! Yes! It's back I can taste again. (to a passerby) Hey! What's the date today?

PASSERBY: The fifteenth.

KRAMER: Fifteenth. Yes, last day for the Mackinaws. I can still make it. Wait.. Newman...

NEWMAN: Sorry, last one. Would you want to suck the pit?

KRAMER: (fake laugh) Look Hubert. It's the mailman. You remember the mailman don't you?

(unleashes a dog; Dog proceeds to chase after Newman)

[Finally at The Plaza Hotel]

ELAINE: (knocks) Hello is anybody here?

LEO: They said they were sending an Asian woman.

ELAINE: Oh my God.

The End
