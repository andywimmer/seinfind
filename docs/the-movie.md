---
layout: default
title: The Movie
parent: Season 4
nav_order: 14
permalink: /the-movie
cat: ep
series_ep: 54
pc: 415
season: 4
episode: 14
aired: January 6, 1993
written: Steve Skrovan & Bill Masters & Jon Hayman
directed: Tom Cherones
imdb: http://www.imdb.com/title/tt0697735/
wiki: https://en.wikipedia.org/wiki/The_Movie
---

# The Movie

| Season 4 - Episode 14                                | January 6, 1993          |
|:-----------------------------------------------------|:-------------------------|
| Written by Steve Skrovan & Bill Masters & Jon Hayman | Directed by Tom Cherones |
| Series Episode 54                                    | Production Code 415      |

"The Movie" is the 54th episode of the sitcom Seinfeld. It is the 14th episode of the fourth season, and first aired on January 6, 1993.

## Plot

The episode begins with Jerry ranting in a stand-up comedy monologue about age gaps in employment:

> What's with the age gap hiring policy at most movie theaters? Didja ever notice, they never hire anyone between the ages of fifteen... and eighty, you know what I mean? Like, the girl that sells you the ticket, she's ten. Then there's the guy who rips the ticket, he's a hundred and two. So, what happened in the middle, there? You couldn't find anybody? It's like they want to show you how life comes full circle.

A night filled with miscommunication begins when Jerry has two stand-up acts scheduled for the same night. At first, the Improv manager, Kernis (played by actor Tom La Grua), tells Jerry that the show has been delayed. Jerry is upset that he must lose his on-stage slot; he is more upset when, after telling Kernis, "You don't understand. I got this all timed out. I got another spot across town at 9:50, I'm not gonna be able to make it!" - he then learns, when he shows up at the other comedy stage later, that his spot was for 9:15, not 9:50.

Adding to Jerry's distress is the fact that a hopeful comedian, Buckles (Barry Diamond), "hangs around hoping that somebody drops out." Buckles always bores and pesters Jerry.

Jerry tells Kernis that he'll agree to lose his moment at the microphone, saying, "I'm supposed to meet my friends to see this [fictional] movie, CheckMate, at 10:30."

George has been chosen to buy the movie tickets. At the Paragon Theater, George joins the end of a queue. He taps the shoulder of the man in front of him and asks, "Excuse me, do you have a ticket?" When the answer is no, George is certain that he is in the line of ticket purchasers; in fact, he is in the wrong line, the line of ticket holders. This will cause frustration and loss of important time.

Jerry decides to head for the movie theater to meet his friends. He is grabbed by Buckles, who insists on sharing a taxicab. Buckles irritates Jerry by trying out a new comic routine:

"Hey, do you think this is funny? 'Why do they call it athlete's foot? You don't have to be an athlete to get it. I mean, my father gets it all the time, and believe me, he's no athlete!'"

Elaine joins George in line, and they squabble a bit about films they've seen. Elaine says she hated a movie about Ponce de León: "That Fountain of Youth scene at the end, where they're all splashin' around, and then they go running over to the mirror to see if it really worked? I mean, come on!" Interestingly, George protests that he liked that film: "When Ponce looked in that mirror and saw that he hadn't changed, and that tear started to roll down his cheek? ... I lost it." George, identifying with Ponce, found it moving, whereas Elaine found no emotional or cognitive gratification in the film at all.

Kramer joins George and Elaine, but he missed dinner and wants to cross the street to get a Papaya King hot dog. When Elaine urges him to get movie concession food, he protests, "I don't wanna get a movie hot dog!" (Much later, in the episode The Gum, Kramer does eat one, but it is ancient and he vomits it up.)

The crucial destruction of the friends' plans happens in a few moments:

Elaine: Kramer, Jerry is going to be here any second, and then this line is going to start moving, and we're going to end up in the front row.

Kramer: Well, just save me a seat.

Elaine: No! I don't want to save seats. Don't put me through that! I once had the fleece just ripped out of my winter coat in a seat-saving incident!

George goes to buy tickets for all of them, but Elaine and Kramer want to change plans. It is now 10:20, and they can go instead to see the 10:45 showing at the theater around the corner. Elaine refuses, because "a miniplex multi-theater" has screens that are too small: "It's like a room where they bring in POWs to show them propaganda films."

Elaine finally agrees that she and George will go purchase tickets at the Multiplex, and Kramer will wait to tell Jerry of the change in plans. However, just before Jerry arrives, Kramer runs over to buy a hot dog at the Papaya.

Jerry, meanwhile, is suffering an apparently unending taxi ride with Buckles, and when he finally escapes the cab, Buckles reveals his real intention: "Can you get me on The Tonight Show?"

At the new movie theater, George is interested in another film there, Rochelle, Rochelle. Meanwhile, Elaine struggles to save seats for everyone, and George runs afoul of an usher. Jerry then misses his second show after being delayed by his taxi driver.

Through a comedy of errors, everyone (but Kramer) misses the movie they were originally going to see and end up in Rochelle, Rochelle.

## Continuity

The fictional film Rochelle, Rochelle ("a young girl's strange, erotic journey from Milan to Minsk") makes its first appearance in this episode. George later rents it from the video store in Season 4's "The Smelly Car", and it is turned into a Broadway musical starring Bette Midler in Season 6's "The Understudy".

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Guests

Barry Diamond .................. Buckles  
Perry Anzilotti ................... Usher  
Tom La Grua ..................... Kernis  
Eric Poppick ....................... Maurice  
Cathy Lind Hayes ............. Woman Behind Elaine  
Allan Kolman ..................... Cab Driver  
Molly Cleator .................... Cashier  
Christie Mellor .................. Concessionaire  
Jeff Norman ...................... Man in Line  
Paul Eisenhauer ................. Man in Theatre  
Montrose Hagins .............. Woman in Theatre

## Script

[Opening monologue]

What is with the age-gap hiring policy at most movie theaters? They never hire anyone between the ages of 15 and 80. Know what I mean? The girl that sells you the tickets, she's 10. Then there's the guy that rips it, he's 102. What happened in the middle? Couldn't find anybody? It's like they want to show you how life comes full circle. You're 15. You're selling tickets. Then you leave. You go out, you have a family, kids, marriage, career, grandchildren. Eighty years later, you're back at the same theater... 3 feet away, ripping tickets. Took you 80 years to move 3 feet.

[Nightclub]

JERRY: The show's delayed? Why?

KERNIS: I don't know. Diane told me to hold it for 15 minutes.

JERRY: But I got this all timed out. I got another spot across town at 9:50. I'm not gonna make it.

KERNIS: I hear you, guy.

JERRY: And I'm doing Letterman Monday. You know, I gotta work out the material.

KERNIS: Let me see what I can do.

JERRY: Oh, no, Buckles? You're not putting him on tonight, are you?

KERNIS: No. He hangs around in case somebody drops out. Look, why don't you come back and do the 11:00 spot.

JERRY: I was supposed to meet my friends to see this movie Checkmate at 10:30. Alright, forget the movie. I'll do the 11:00 spot. I'll be back.

BUCKLES: Hey, Jer.

JERRY: Heeeeyyyyy.

[George approaches a line at the Paragon theater]

GEORGE: Excuse me. Do you have a ticket?

MAN: No.

GEORGE: Okay. Good.

[Nightclub]

MAURICE: You missed your spot. It was 9:15.

JERRY: But, Maurice, I'm positive you told me 9:50.

MAURICE: In any event, I had no alternative but to reassign your spot to one of your peers who possesses a clearer understanding of our scheduling policies.

BUCKLES: Jerry!

JERRY: What are *you* doing here?

BUCKLES: Hey, do you think this is funny? Why do they call it "athlete's foot"? You don't have to be an athlete to get it. I mean, my father gets it all the time and, believe me, he's no athlete.

JERRY: I gotta go.

BUCKLES: Wait. Where?

JERRY: The movies.

BUCKLES: I thought you told Kernis you were going back to do the 11:00.

JERRY: I'm just gonna stop by and tell my friends I can't make it.

BUCKLES: Cool. I'll split a cab with you.

JERRY: I could be a while over there.

BUCKLES: I got time. It'll give us a chance to get to know each other a little better.

[In line at the theater]

ELAINE: Hey.

GEORGE: Hey.

ELAINE: Where is everybody?

GEORGE: Everybody's late because they know I'll get in line early.

ELAINE: I've been *dying* to see Checkmate.

GEORGE: Well, if it's as good as Ponce de Leon, I'll be happy.

ELAINE: Ponce de Leon? Are you kidding me? I hated that movie.

GEORGE: Ponce de Leon? But that was great.

ELAINE: Oh, come on. That fountain of youth scene at the end where they're splashing around and they go running over to the mirror to see if it really worked. I mean, come on. (laughing) It's so stupid.

GEORGE: Let me tell you something; When Ponce looked in that mirror and saw that he hadn't changed and that tear started to roll down his cheek, I lost it.

ELAINE: Oh, there you are. Where have you been?

KRAMER: Around the corner at the Paradise Twin.

ELAINE: Well, I told you the Paragon.

KRAMER: No, you said Paradise. It's playing there too, you know.

ELAINE: No. I would not have said Paradise. That's a twin theater. I want to see this movie on a big screen.

KRAMER: I'm gonna get a hot dog at Papaya King.

GEORGE: You'll never make it back in time.

KRAMER: I haven't had any dinner.

ELAINE: You can get a hot dog at the theater.

KRAMER: I don't want a movie dog. I want a Papaya King hot dog.

ELAINE: Kramer, Jerry is gonna be here any second. Then this line's gonna start moving, and we'll end up in the front row.

KRAMER: Just save me a seat.

ELAINE: No, I don't wanna save seats. Don't put me through that. I once had the fleece ripped out of my winter coat in a seat-saving incident.

KRAMER: Give me my ticket, then.

GEORGE: Well, I don't have them yet.

ELAINE: You don't have them yet?

GEORGE: Well, I'm in line to buy.

ELAINE: No, George, this is a ticket-*holders* line.

GEORGE: No, it's not. It's the ticket-*buyers* line.

ELAINE: Then how come we're not moving?

KRAMER: Good question.

GEORGE: Is this the ticket-holders line or the buyers?

MAN IN LINE: Holders.

GEORGE: When I asked you before if you had a ticket, you said no.

MAN IN LINE: I didn't. My friend was getting it.

GEORGE: That's good. It's good to be accurate. Stay here. I'll go get tickets.

ELAINE: Can you believe him?

KRAMER: He spaced out.

ELAINE: And how long would *you* have stood in the ticket-holders line?

(Kramer thinks for a moment)

ELAINE: Yeah, exactly.

GEORGE: It's sold out.

ELAINE: Oh, real good, George. Real good.

GEORGE: Hey, I asked the guy if he had a ticket.

KRAMER: Alright, what time is it?

GEORGE: Twenty after 10.

KRAMER: They got one at the Paradise at 10:45.

ELAINE: No, I don't wanna go to a miniplex multi-theater.

GEORGE: It's the same movie. What's the difference?

ELAINE: No, it's not a theater. It's a room where they bring in POWs to show them propaganda films.

GEORGE: Alright, I'm going.

KRAMER: It's in Dolby.

ELAINE: Well, what about Jerry?

KRAMER: You go get the tickets. I'll wait for him.

GEORGE: Terrific. Alright, come on.

ELAINE: Alright.

[Jerry & Buckles split a cab]

BUCKLES: So then, when I was 12, the whole family moved from Bensonhurst to Sheepshead Bay. We were right on the water. The whole atmosphere stunk from fish. You know that fishy smell I'm talking about?

JERRY: Yeah, yeah. The fishy smell.

BUCKLES: To this day I won't eat fish. Do you eat fish?

JERRY: Yes! Yes, I eat fish!

BUCKLES: I don't know how you eat that crap.

JERRY: Take the Park.

BUCKLES: No, no, take 55th. No, there's beautiful homes there. There's lovely, talented, attractive people. You'll thank me. Jerry, I want you to do me a favor: No more fish.

JERRY: Okay. I get your point.

BUCKLES: I have a point?

[Paradise Movie theater]

GEORGE: Got them.

ELAINE: Beautiful.

GEORGE: Hey, you know what else is playing here? Rochelle Rochelle.

(Elaine groans)

GEORGE: I wouldn't mind seeing that.

ELAINE: Yeah. Men can sit through the most pointless, boring movie if there's the slightest possibility that a woman will take her top off.

GEORGE: So, what's your point? By the way, you owe me $7.50.

ELAINE: Alright. Can you break a 20?

GEORGE: No. I don't have any change.

ELAINE: Well, then I'll pay you later.

GEORGE: Or I could take the 20, and I could pay *you* later.

ELAINE: Yeah, you *could*...

GEORGE: Might be easier.

ELAINE: Well, how's that easier? I mean, then you would owe me $12.50 instead of me owing you $7.50.

GEORGE: Either way.

ELAINE: Yeah, it's the same thing.

GEORGE: So can I have it?

ELAINE: I tell you what, I'll get the popcorn and the soda.

GEORGE: What do you mean, you'll *get* the popcorn and the soda?

ELAINE: I will buy your popcorn and soda, and we'll call it even.

GEORGE: Tell you what. Give me the 20, I'll buy *you* a popcorn and soda and I'll throw in a bonbons.

ELAINE: (exasperated) George, you're sapping my strength.

GEORGE: Come on. Go.

ELAINE: What about Kramer and Jerry?

GEORGE: I'll wait here for them. You go save seats.

ELAINE: Me? But that's three seats. I can't save three seats. I told you about that guy who tore up my coat.

GEORGE: Save them, go.

ELAINE: No, but George...

[Taxicab]

JERRY: Fifty-Fifth Street. Great idea. Saturday night, theater traffic. Good move.

BUCKLES: I wonder how Ike Turner would react in traffic. "You better move that car, girl, before I bust you up." And what about Jose Feliciano? "It's not bad enough that I can't see, I have to sit here for two hours. I don't think so." It can be anybody. People down through history reacting in traffic. Franklin Roosevelt: "Driver..."

JERRY: Alright, alright. I got the bit.

BUCKLES: Jerry, I want you to have this piece of material.

JERRY: That's nice of you. I can't do the voices.

BUCKLES: Jerry, don't start up with me.

JERRY: I gotta get out of this cab.

BUCKLES: But, Jerry, we're riffing.

JERRY: No, I'm not riffing. I'm ignoring. Do you understand the difference?

BUCKLES: Can you help me get on The Tonight Show?

[Elaine is saving seats in the "Checkmate" theater]

ELAINE: No, these are saved.

MAN: All of them?

ELAINE: Yeah.

MAN: You can't take four seats.

ELAINE: What, is that a rule?

WOMAN: Are these your things?

ELAINE: Yeah.

GEORGE: Elaine, I can't wait anymore. I'm gonna miss the beginning.

WOMAN: Well, we'll take these seats. Would you please remove your stuff?

ELAINE: No, no, they'll be here.

WOMAN: But I am here. They are not.

ELAINE: They're coming. George, listen, run over to the Paragon and give Kramer the two tickets so they can come in. These are taken. Taken. If you hurry back, you won't miss the beginning.

GEORGE: You go, and I could save the seats. You don't like saving anyway.

ELAINE: No, taken! Taken, taken. (to George) I'm getting the hang of it.

GEORGE: Why don't you give me the 20, I'll stop and get change and then you and I can, you know, settle up.

ELAINE: Can we do this later, George?

GEORGE: What's the point of discussing it anymore. You'll give me the money when you have it. I trust you.

[At the box office]

KRAMER: Will you do me a favor? You see a guy who's about 5 foot 11 he's got a big head and flared nostrils tell him his friend's gonna be right back, okay?

[Elaine tries in vain to save seats]

ELAINE: No, I'm sorry. These are taken. They're in the lobby buying popcorn. What are you doing? These are taken!

MAN: Which one?

ELAINE: These two and this one. No, don't come over here! These are taken. Go! Go! These are taken. They're taken! They're taken! Oh, take them.

[At the box office]

GEORGE: Excuse me, have you seen a guy with, like a horse face, big teeth and a pointed nose?

CASHIER: Flared nostrils?

GEORGE: Yeah.

CASHIER: Nope. Haven't seen him.

[Taxicab]

BUCKLES: Jerry, could you do me a personal favor? And if I'm out of line, please let me know. Could I keep my trench coat in your closet for a few months?

JERRY: Your trench coat in my closet?

BUCKLES: Jerry, my closet is packed to the gills. I'm afraid to open the door. Just for a few months. It'll make all the difference in the world.

JERRY: I can't keep your coat in my closet.

BUCKLES: I'm sorry you feel that way.

JERRY: Well, that's how I feel.

BUCKLES: Hey, your friends aren't here.

JERRY: They must be inside. I'm gonna run in and tell them I can't make the movie.

BUCKLES: We should go see Rochelle Rochelle. I hear it's really hot.

JERRY: No, thanks. Maybe some other time.

BUCKLES: Really? Do you mean that?

JERRY: No, I don't.

BUCKLES: But you like the athlete's-foot bit, right?

JERRY: No. No. I was kidding. It was terrible. Here you go. I'll see you later.

BUCKLES: Do you want me to wait for you?

JERRY: No. Don't wait.

BUCKLES: Jerry, I'll wait just to make sure you get in, that's all.

JERRY: I got friends inside I need to get a message to. You mind if I walk through real quick?

USHER: Go ahead.

JERRY: Bye-bye.

[Box office]

KRAMER: Hey, did that guy show up?

CASHIER: The guy with the horse face and the big teeth?

KRAMER: No, the guy with the big head and the flared nostrils.

CASHIER: Haven't seen him. There was a short guy with glasses looked like Humpty-Dumpty with a melon head. But he left.

[In the "Checkmate" theater]

WOMAN: So I got home, and he was vacuuming. I mean, he's 12 years old. Who else but my Alan would do something like that? And then last night, he put on my high heels. He put on such a show for us. He was dancing around, lip-synching to A Chorus Line. I mean, you can see he's got talent.

ELAINE: Excuse me. Excuse me.

WOMAN: What's the problem?

ELAINE: You're talking.

WOMAN: It's the coming attractions. So anyway, he sings, he dances. And you know what he's gotten into now? He is cooking. He does a great...

USHER: Ticket, sir?

GEORGE: I just went out. I went to look for my friend.

USHER: Do you have your stub?

GEORGE: My stub? Who keeps the stub? No one holds on to the stub. (searching through pockets) I'm going to the movies for 25 years, nobody ever asked me for the stub. You don't remember me?

USHER: It's a big city, sir.

GEORGE: I went in with a pretty woman. You know, short, big wall of hair, face like a frying pan.

USHER: Nope.

GEORGE: I can't find it.

USHER: I can't let you in without your stub.

GEORGE: Alright. Here. It's my friend's ticket.

GEORGE: (Whispering in the dark theater) Elaine? Elaine?

MAN: Sit down!

GEORGE: (Louder) Elaine?

MAN: Hey, shut up!

GEORGE: I'm looking for my friend.

MAN: Check the bathroom. You're bothering us.

GEORGE: I know my friend's here. What do you want me to do?

MAN: You can shut up!

GEORGE: Elaine?

MAN: Shut up!

GEORGE: Elaine? Elaine?

NARRATOR: The Village Voice calls it a masterpiece. A young woman's strange, erotic journey from Milan to Minsk. It's a story about life and love and becoming a woman. Rochelle Rochelle. Now playing at the Paradise Two.

[Elaine at the concessions stand]

ELAINE: Can I have a medium Diet Coke?

CONCESSIONAIRE: You want the medium or middle size?

ELAINE: What's the difference?

CONCESSIONAIRE: Well, we have three sizes: medium, large and jumbo.

ELAINE: What happened to the small?

CONCESSIONAIRE: There is no small. Small's medium.

ELAINE: So, what's medium?

CONCESSIONAIRE: Medium's large, and large is jumbo.

ELAINE: Okay, give me the large.

CONCESSIONAIRE: That's medium.

ELAINE: Right. Yeah. Can I have a small popcorn?

CONCESSIONAIRE: There is no small. Child-size is small.

ELAINE: What's medium?

CONCESSIONAIRE: Adult.

ELAINE: Do adults ever order the child-size?

CONCESSIONAIRE: Not usually.

ELAINE: Okay, give me the adult.

CONCESSIONAIRE: Do you want butter?

ELAINE: Is it real butter?

CONCESSIONAIRE: It's butter flavoring.

ELAINE: Yeah, well, what is it made of?

CONCESSIONAIRE: It's yellow.

[Taxicab]

JERRY: Thanks. Thank you. You don't know how long I've been waiting out there.

CAB DRIVER: Very busy tonight. Very busy. Where you go?

JERRY: Forty-Fourth and 9th.

CAB DRIVER: Have you got a cigarette?

JERRY: No.

USHER: Ticket, sir?

GEORGE: We've just been through this. You don't remember? We just had this exact conversation a minute ago.

USHER: I need to see your stub.

GEORGE: I got the stub. I got the stub. I put it right in my pocket. I got the... I'm telling you, I got the stub. I just... I don't know where it is.

USHER: I can't let you in without your stub.

GEORGE: You just let me in. We just did this a minute ago! Alright. Okay. There. There you go, okay? That's my other friend's ticket. Happy now? You got two tickets. Two of my friends.

USHER: Ticket, sir?

KRAMER: No, see, my friend bought me a ticket. See, I'm late. She's already inside.

USHER: Go ahead. Enjoy the movie.

KRAMER: Thanks.

MAN: What is it, Your Majesty?

KING: Anyone asks...

KRAMER: Is that seat taken?

WOMAN: It's all yours.

KING: When I tell you something, you damn better well listen. I'm getting tired of these questions.

MAN: Yes, Your Majesty.

KING: I'm sorry, Grendel, I... I've been very taxed lately.

[Taxicab]

CAB DRIVER: Yeah, I'm very sorry. You give me a few minutes. I have to stop for gasoline.

JERRY: Gasoline? Can't you get it after you drop me off?

CAB DRIVER: No. Impossible. It is on empty. See.

JERRY: Yeah, yeah. Okay.

["Checkmate" theater]

ELAINE: What happened to my seat? Oh, my God, where was I?

MAN 1: Hey, sit down. I can't see.

MAN 2: Get out of the way.

ELAINE: I can't find my seat.

MAN 1: Just move!

ELAINE: No, you move!

MAN 2: Get the hell out of here!

[George sits in a the "Rochelle Rochelle" theater]

MAN: Come, come, you're soaking wet. Come in.

ROCHELLE: My name is Rochelle. I'm from Milan. I'm supposed to visit my relatives in Minsk.

MAN: Yes. Come by the fire. Take off those wet clothes. You'll catch cold.

ROCHELLE: My hands are so cold, I can barely get these buttons open. That's much better. Much, much better.

[Elaine can't find her seat]

ELAINE: Listen, I just went to go get popcorn and God. I just went to go get popcorn, okay and somebody took my seat, and my coat is in there.

USHER: There's a seat in the front row.

ELAINE: No, no, I can't sit in the front row.

USHER: You're gonna have to wait then.

ELAINE: I can't stand around here for *two hours*.

USHER: I can let you see Rochelle Rochelle.

ELAINE: Thanks. Hey, listen, by the way have you seen a tall, lanky doofus with a bird face and hair like the Bride of Frankenstein?

USHER: Haven't seen him.

ELAINE: Okay.

[Taxicab]

JERRY: Let's go. I'm gonna miss my spot.

CAB DRIVER: Yes, yes, we go. We go. One minute. I run across the street for cigarettes.

JERRY: I don't have time. I'm gonna miss my spot.

CAB DRIVER: No, no. We go very soon.

["Checkmate" theater]

MAN: I didn't know you enjoyed chess, Your Majesty.

KING: Why wouldn't I?

MAN: Well, because the king is always in jeopardy.

KING: Yes, but it's only a game.

MAN: Yes, of course. Only a game.

[Jerry's other gig]

JERRY: Hey, did I make it?

KERNIS: Sorry.

JERRY: Great. That's great. What a night.

ANNOUNCER: Pat Buckles, ladies and gentlemen. Another round of applause for Pat Buckles.

JERRY: You got my spot?

BUCKLES: That athlete's-foot bit killed.

JERRY: Really?

BUCKLES: You think I need to lose some weight?

JERRY: Weight? No. You just need some height.

BUCKLES: Jerry, don't start up with me. What are you doing now?

JERRY: What am I doing? My whole night's ruined. I didn't do any sets. I didn't go to the movies.

BUCKLES: Come on. We can still catch most of Rochelle Rochelle.

JERRY: Rochelle Rochelle?

BUCKLES: A young girl's strange, erotic journey from Milan to Minsk.

JERRY: Minsk?

["Rochelle Rochelle" theater]

ROCHELLE: My father was a shoemaker. He worked hard, and we didn't have much money.

MAN: Rochelle, Rochelle, what are we going to do with you? I'm going away tomorrow.

ROCHELLE: Where? Where are you going? I want to go with you. Take me with you.

MAN: Well, don't be a silly goose.

ROCHELLE: But I've never seen Minsk. I hear there's so much hustle and bustle.

MAN: You're being absurd. Leave me alone. Rochelle, Rochelle, do you know..?

ELAINE: Give me a break!

JERRY: Elaine?

ELAINE: Jerry.

JERRY: Elaine!

MAN: (whisper) Shut up.

GEORGE: Jerry?

JERRY: George?

GEORGE: Elaine?

ELAINE: George.

JERRY: But where's Kramer?

MAN: (whisper) Will you shut up?

ELAINE: I don't know. Does this movie stink or what?

JERRY: Let's get out of here. (to Buckles) I'll see you.

BUCKLES: You're leaving?

JERRY: Yeah.

BUCKLES: (holding out his coat) Jerry, take the coat. Please. One month.

JERRY: I don't want the coat.

BUCKLES: Jerry, call me when you get home, so I know you're okay.

ELAINE: What happened to you?

JERRY: I missed you at the other theater. Then I missed my set, and I had nothing to do.

GEORGE: Man. Look at this. I sat in gum.

JERRY: What happened to Checkmate?

ELAINE: Oh, I went out to get popcorn, and some creep took my seat.

GEORGE: Hey, by the way, you owe me $7.50.

JERRY: But I didn't even use the ticket.

GEORGE: I still paid for it.

JERRY: I only have a 20.

KRAMER: Hi.

JERRY: Hey!

GEORGE: What happened to you?

ELAINE: That's my coat. Give me that. Where did you get that?

KRAMER: It was on the seat.

ELAINE: *You* took my seat?

GEORGE: You owe me $7.50.

KRAMER: Yeah. Right.

ELAINE: What is this stain?

KRAMER: It's yellow mustard. (to George) Can you break a 20?

[Closing monologue]

But I always get confused in the movie theater by the plot. It's embarrassing to have to admit, but I'm the one you see in the parking lot, after the movie, talking with his friends, going: "You mean, that was the same guy from the beginning?" Nobody will explain it to you. In the theater, you can't find out. Why did they kill that guy? Why did they kill him? Who was that guy? Who was that guy? I thought he was with them. Wasn't he with them? Why would they kill him if he was with them? Oh, he wasn't really with them. I thought he was with them. It's a good thing they killed him.

The End
