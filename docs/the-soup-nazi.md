---
layout: default
title: The Soup Nazi
parent: Season 7
nav_order: 6
permalink: /the-soup-nazi
cat: ep
series_ep: 116
pc: 706
season: 7
episode: 6
aired: November 2, 1995
written: Spike Feresten
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697782/
wiki: https://en.wikipedia.org/wiki/The_Soup_Nazi
---

# The Soup Nazi

| Season 7 - Episode 6      | November 2, 1995          |
|:--------------------------|:--------------------------|
| Written by Spike Feresten | Directed by Andy Ackerman |
| Series Episode 116        | Production Code 706       |

"The Soup Nazi" is the 116th episode of the NBC sitcom Seinfeld, which was the sixth episode of the seventh season. It first aired in the United States on November 2, 1995.

The Soup Nazi is also the nickname of the eponymous character, Yev Kassem, played by Larry Thomas. The term "Nazi" is used as an exaggeration of the excessively strict regimentation he constantly demands of his patrons.

## Plot

Jerry, George and Elaine patronize a new soup stand Kramer has been praising. Jerry explains that the owner, Yev Kassem, is known as the "Soup Nazi" due to his temperament and insistence on a strict manner of behavior while placing an order.

At the soup stand, George tells the cashier that he didn't receive any bread with his order. Kassem says bread is two dollars. George complains that other customers received free bread, to which Kassem first responds by saying the bread is now three dollars, then decrees "No soup for you!" George's order is quickly taken away, and his money returned.

En route to the soup stand with Jerry and George, Elaine notices a guy on the sidewalk with an giant armoire for sale. She decides to forego the soup in favor of buying the armoire. However, her building superintendent informs her that furniture move-ins are not allowed on Sundays, so she asks Kramer to watch the armoire and promises to get soup from Kassem for him in return.

On a subsequent visit, George successfully manages to buy soup (bread came with it this time), but Elaine, having ignored Jerry's advice on how to order, quickly draws Kassem's ire and she is banned for a year. During this time, two thieves (Bob and Cedric) intimidate Kramer and steal the armoire.

Later, Jerry and his current girlfriend Sheila (Alexandra Wentworth) visit the soup stand. Kassem is repulsed by their public displays of affection, so Jerry disavows knowing Sheila to stay on Kassem's good side. Jerry talks about the situation with George, who has expressed disgust at Jerry's "baby talk". Jerry admits he behaved facetiously with Sheila at the soup stand and vows to redeem himself. George considers this and begins to behave similarly with Susan to express his disgust, but Susan instead takes this as a sign of George expressing their love in public.

Some time later, Kramer, who has befriended Kassem, tells him about the armoire theft. Kassem offers him an antique armoire he has in storage as a replacement. Elaine is elated, and goes to Kassem to thank him. When Kassem learns the armoire was for Elaine, he says he would have rather destroyed it than give it to her. Upset, Elaine returns to her apartment with Jerry, where they discover the armoire is filled with Kassem's soup recipes. Elaine returns to the soup stand and confronts Kassem with the recipes, intent on ruining his business in revenge for mistreating her.

Jerry encounters Newman, who is running to get a pot from his apartment. Newman tells him that because of what Elaine said to Kassem, he is closing down his stand and returning to Argentina and giving away whatever soup he has left. Jerry quickly runs home to follow Newman's lead.

## Production

"The Soup Nazi" was Spike Feresten's first credited Seinfeld episode as a writer. The idea for the episode arose when Feresten told Jerry Seinfeld and Larry David about New York soup vendor Ali "Al Yeganeh", who was nicknamed "The Soup Nazi." Seinfeld and David laughed and said, "That's a show. Do that as your first show." Feresten's inspiration for the armoire subplot was a New York apartment building in which he had lived, which forbade moving furniture on certain days. The armoire thieves were written as homosexual because Larry David decided that "only gay guys would steal an armoire."

The first cast table reading for "The Soup Nazi" was held on September 28, 1995, and it was filmed before a studio audience on October 3. In the episode, Elaine (Julia Louis-Dreyfus) imitates Al Pacino in Scent of a Woman. This was done at Jerry Seinfeld's suggestion, even though Louis-Dreyfus had never seen the film.

## The character

The Soup Nazi was portrayed by Larry Thomas, who was nominated for a 1996 Emmy for the role. Thomas, who did not realize that the character was based on a real person, received the inspiration for his portrayal from watching Lawrence of Arabia and studying Omar Sharif's accent.

A stone-faced immigrant chef with a thick Stalin-esque moustache, he is renowned throughout Manhattan for his soups. He demands that all customers in his restaurant meticulously follow his strict queuing, ordering, and payment policies. Failure to adhere to his demands brings the stern admonition ("No soup for you!"), whereupon the customer is refunded and denied his or her order. He will then yell at the top of his lungs to the next person in line, "Next!" Elaine parodies this when she reveals that she has his recipes. She says to him, "You're through, Soup Nazi. Pack it up. No more soup for you. NEXT!"

The Soup Nazi has a cameo in the Seinfeld series finale, in which his true name is revealed, but which he refuses to spell when asked by District Attorney Hoyt. He tells Hoyt about how he banned Elaine from his shop, only for her to return and ruin his business, forcing him to move to Argentina. Elaine angers him by claiming his soup was no good. During the recess, he is seen serving soup to Robin, Mr. Lipman, Poppie and Babu Bhatt. Poppie appears to ask for salt with his soup, whereupon Kassem takes his soup away. He is present and obviously satisfied when Jerry, Elaine, George and Kramer are found guilty and sent to prison.

### Inspiration

The character was inspired by Al Yeganeh, a soup vendor who ran Soup Kitchen International in New York City. Yeganeh has stated on numerous occasions that he is very offended by the "Soup Nazi" moniker.

According to writer Spike Feresten, Jerry Seinfeld and several members of the production team went to Soup Kitchen International for lunch weeks after "The Soup Nazi" aired. Upon recognizing Seinfeld, Yeganeh went into a profanity-filled rant about how the show had "ruined" his business and demanded an apology. Seinfeld allegedly gave what Feresten describes as "the most insincere, sarcastic apology ever given". Obviously having seen the episode, Yeganeh then bellowed, "No soup for you!" and ejected them from the restaurant.

According to Nora Ephron's DVD commentary, the first pop culture reference to Yeganeh (though not by name) seems to have come years before the Seinfeld episode, in the 1993 movie Sleepless in Seattle. In the film, a character playing a writer pitches a story for the lifestyle section of the publication to their editor: "This man sells the greatest soup you have ever eaten, and he is the meanest man in America. I feel very strongly about this, Becky; it's not just about the soup."

## Legacy

### Advertising

Like Jackie Chiles, the Soup Nazi character (played by Thomas) has appeared in commercials after the end of the series.

* In an advertisement by the corporate lobbying group Center for Consumer Freedom, he denies food to people he considers to be too fat.

* Thomas appeared, in character, along with Jerry Seinfeld in a television commercial for Acura that aired during the 2012 Super Bowl. In the advertisement, Seinfeld is trying to bribe an ordinary guy to get an Acura, offering him soup from The Soup Nazi, who happily offers "Soup for you!". After Jay Leno beat Jerry Seinfeld in bribing the ordinary guy, the Soup Nazi was seen with Jerry, an alien, and a "Munchkin" at a restaurant where they are angered at Jay Leno's actions.

* In 2013, Serbu Firearms refused to sell their model BFG-50A semi-automatic .50 rifles to the New York City Police Department after the passage of the NY SAFE Act that classified their weapon as an assault rifle. Following their refusal to sell the rifles, Serbu had T-shirts printed with an image of the Soup Nazi character with the words "No Serbu For You". Thomas, a gun control advocate, contacted Facebook and the T-shirt printers to have the shirts removed. Serbu has since removed the image of Thomas and replaced it with one of their founder Mark Serbu.

* The restaurant chain Eat'n Park put up signs on their reader boards advertising for soup in January 2016. As a joke, the sign says "No soup for you!" before saying "Just kidding" and advertising the price for a take-out quart of soup.

### In popular culture

* Larry Thomas appeared as himself in the Scrubs episode "My Self-Examination." He denies he is the Soup Nazi when asked by J.D. (Zach Braff), who then tricks him into saying the catchphrase "No soup for you!" by asking him "What is [the catchphrase] again? It's like, 'You're out of luck in the soup department...'"

* Seinfeld: A XXX Parody, a 2009 porn parody of Seinfeld, is a spoof of "The Soup Nazi". In it, the Nazi character is named "The Porn Nazi" and is played by Evan Stone.

* Rapper Wale used lines from the episode as an introduction and outro to his song "The Soup" on his 2010 mixtape More About Nothing. He acts as the Soup Nazi and uses the soup chef's catchphrase "No soup for you!"

* In the Ben 10: Alien Force episode "The Gauntlet", Ben brings juice for Gwen and Kevin and says "No juice for you!" to Kevin.

* In the 2010 reboot of the video game NBA Jam for Xbox 360, PlayStation 3 and Wii, when an alley-oop maneuver is blocked by an opponent, you can hear the announcer say, "No hoop for you!"

* In the sitcom Arrested Development, the crooked housing entrepreneur George Bluth Sr. is charged with signing a development deal with Saddam Hussein, despite the embargo against Iraq. Bluth claims that he acted in good faith, mistakenly believing that Hussein was Larry Thomas because of his resemblance to the Soup Nazi. This get referenced in a later episode, where Thomas appears in the role as a political decoy for Saddam Hussein who has lost his job because of the American invasion of Iraq.

* In 2011's NBA Jam: On-Fire Edition, the tagline "No soup for you!" can be purchased as a reward for the player's card.

### In-person promotions

* Larry Thomas has used the character to promote soup kitchens for the homeless.

* In July 2012, the "Seinfeld Food Truck" embarked on an eight-stop United States tour. The truck, driven by Larry Thomas, handed out free soup along with other Seinfeld-related food items: Snapple, Twix, Junior Mints, black and white cookies and muffin tops.

* Thomas was hired by Yeganeh's company in July 2015 to portray the Yev Kassem character as promotion for Soupman products.

### Other

* The episode inspired an actual soup chain, "Soup Nutsy", which opened in 1996 in New York City. Though it had no official connection to, or endorsement from, Seinfeld or its creators, it included specific Seinfeld references such as describing two of its soups as "Jerry's Favorite" and "Kramer's Favorite", respectively. In 1997 it was bought by Franchise Concepts. A few of its locations remain in Toronto, Ontario in Canada.

* In August 2009, Albert Gonzalez was convicted for robbery, being the most prolific hacker of credit cards (130 million). He operated on the Internet using the handle "Soupnazi".

## Critical response

Linda S. Ghent, Professor in the Department of Economics at Eastern Illinois University, discusses this episode in terms of its dramatization of the economic issue of market power. The Soup Nazi has monopoly power because he has the power to alter the market price of the goods and services he sells, such as charging George $2 for soup, and then $3, for bread. The soup seller is free to practice price discrimination against George and can banish Elaine from his restaurant because he doesn't like her attitude. Because the Soup Nazi's soup is so good, his reign over New York's soup is powerful to the point that his customers prefer his market, and even his abuse, rather than seek soup elsewhere. Elaine breaks his monopoly when she finds his recipes.

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Cosmo Kramer

#### Recurring

Wayne Knight ............... Newman  
Heidi Swedberg ............ Susan Biddle Ross  
Steve Hytner ................ Kenny Bania

#### Guests

Alexandra Wentworth ........ Sheila  
Larry Thomas ..................... Soup Nazi  
John Paragon ...................... Ray  
Yul Vazquez ........................ Bob  
Thom Barry ......................... Super  
Vince Melocchi .................... Furniture Guy  
Ana Gasteyer ..................... Woman  
Cedric Duplechain .............. Customer  
Mike Michaud .................... Customer

## Script

GEORGE: Alright. So, what theatre you wanna go to tonight? We got 61st and 3rd or 84th and Broadway.

JERRY: Which one you wanna go to shmoopy?

SHEILA: You called me shmoppy. You're a shmoopy.

JERRY: You're a shmoopy!

SHEILA: You're a shmoopy!

JERRY: You're a shmoopy!

GEORGE: Alright, shmoopies... what's it gonna be? Pick a theater.

JERRY: Uh... we'll go to 3rd Avenue. So, can you come with us for lunch to the soup place?

SHEILA: No. You have a good lunch. But I'll meet you back here for the movie.

GEORGE: Hey.

ELAINE: Hey.

SHEILA: Hi Elaine.

ELAINE: Hi Sheila.

JERRY: Alright, then. I'll see you later.

SHEILA: Bye shmoopy.

JERRY: Bye shmoopy.

ELAINE: Okay. We ready to go?

GEORGE: Yes. Please. Please, let's go.

ELAINE: Boy, I'm in the mood for a cheeseburger.

JERRY: No. We gotta go to the soup place.

ELAINE: What soup place?

GEORGE: Oh, there's a soup stand, Kramer's been going there.

JERRY: He's always raving. I finally got a chance to go there the other day, and I tell you this, you will be stunned.

ELAINE: Stunned by soup?

JERRY: You can't eat this soup standing up, your knees buckle.

ELAINE: Huh. Alright. Come on.

JERRY: There's only one caveat -- the guy who runs the place is a little temperamental, especially about the ordering procedure. He's secretly referred to as the Soup Nazi.

ELAINE: Why? What happens if you don't order right?

JERRY: He yells and you don't get your soup.

ELAINE: What?

JERRY: Just follow the ordering procedure and you will be fine.

GEORGE: Alright. Alright. Let's - let's go over that again.

JERRY: Alright. As you walk in the place move immediately to your right.

ELAINE: What?

JERRY: The main thing is to keep the line moving.

GEORGE: Alright. So, you hold out your money, speak your soup in a loud, clear voice, step to the left and receive.

JERRY: Right. It's very important not to embellish on your order. No extraneous comments. No questions. No compliments.

ELAINE: Oh, boy, I'm really scared!

JERRY: Elaine.

ELAINE: Alright. Jerry, that's enough now about the Soup Nazi. Whoa! Wow! Look at this. You know what this is? This is an antique armoire. Wow! It's French. Armoire.

JERRY: Ar-moire.

ELAINE: How much is this?

FURNITURE GUY: I was asking 250, but you got a nice face. 2 even.

ELAINE: Huh? Ha. 200. You know, I've always wanted one of these things.

JERRY: He gave you the nice face discount.

ELAINE: Yeah. Alright. You guys go ahead.

JERRY: What about the soup?

ELAINE: I'm getting an armoire, Jerry.

JERRY: [in French accent] Pardon.

GEORGE: This line is huge.

JERRY: It's like this all the time.

GEORGE: Isn't that that Bania guy?

JERRY: Oh, no. It is. Just be still.

GEORGE: Whoop! Too late. I think he picked up the scent.

BANIA: Hey, Jerry! I didn't know you liked soup.

JERRY: Hard to believe.

BANIA: This guy makes the best soup in the city, Jerry. The best. You know what they call him? Soup Nazi.

JERRY: Shhhhh! Alright, Bania, I - I'm not letting you cut in line.

BANIA: Why not?

JERRY: Because if he catches us, we'll never be able to get soup again.

BANIA: Okay. Okay.

GEORGE: Medium turkey chili.

JERRY: Medium crab bisque.

GEORGE: I didn't get any bread.

JERRY: Just forget it. Let it go.

GEORGE: Um, excuse me, I - I think you forgot my bread.

SOUP NAZI: Bread -- $2 extra.

GEORGE: $2? But everyone in front of me got free bread.

SOUP NAZI: You want bread?

GEORGE: Yes, please.

SOUP NAZI: $3!

GEORGE: What?

SOUP NAZI: No soup for you! [snaps fingers]

(cashier takes George's soup and gives him back his money)

ELAINE: What do you mean I can't bring in here? I live here.

SUPER: It's Sunday, Elaine. There's no moving on Sunday. That's the rule.

ELAINE: But I didn't know, Tom. I g -- can't you just make an exception? Please. I've got a nice face.

SUPER: Tomorrow, okay? You can move it in tomorrow. I'll even give you a hand, all right?

ELAINE: Ohh! Well, you're just gonna have to hold this for me.

FURNITURE GUY: I'm a guy on the sidewalk. I don't have layaway.

ELAINE: Oh, no... please don't go. Please - please don't walk away.

JERRY: Oh, man. Ohh! This is fantastic. How does he do it?

GEORGE: You know, I don't see how you can sit there eating that and not even offer me any?

JERRY: I gave you a taste. What do you want?

GEORGE: Why can't we share?

JERRY: I told you not to say anything. You can't go in there, brazenly flaunt the rules and then think I'm gonna share with you!

GEORGE: Do you hear yourself?

JERRY: I'm sorry. This is what comes from living under a Nazi regime.

GEORGE: Well, I gotta go back there and try again. Hi Sheila.

SHEILA: Hi. Hi shmoopy.

JERRY: Hi shmoopy.

SHEILA: No, you're a shmoopy!

JERRY: You're a shmoopy!

GEORGE: I'm going.

JERRY: Hey, listen, so we'll meet you and Susan at the movie tonight?

GEORGE: You know what? I changed my mind. I, uh, I don't think so.

JERRY: Why?

GEORGE: I just don't feel like it anymore.

JERRY: Just like that?

GEORGE: Just like that.

SHEILA: Boy, he's a weird guy, isn't he?

KRAMER: Hey.

JERRY: Hey.

KRAMER: [taking Jerry's couch cushion] Yeah.

JERRY: Hey. Hey. Hey. Hey. Hey. Hey. Hey. Wha -- what are you doing?

KRAMER: Yeah. Elaine, she has to leave her armoire on the street all night... I'm gonna guard it for her. I need something to sit on.

JERRY: Well, sit on one of your couch cushions.

KRAMER: Yeah, but this is so nice and thick. Ahoy there!

ELAINE: Oh, Kramer! Thank God. I really appreciate you doing this.

KRAMER: Yeah. Well, you ask for it, you got it.

ELAINE: Do you need anything?

KRAMER: Well, a bowl of mulligatawny would hit the spot.

ELAINE: Mulligatawny?

KRAMER: Yeah. It's an Indian soup. It's simmered to perfection by one of the great soup artisans in the modern era.

ELAINE: Oh! Who? The Soup Nazi?

KRAMER: He's not a Nazi. He just happens to be a little eccentric. Most geniuses are.

ELAINE: Alright. I'll be back.

KRAMER: Wait a second. You don't even know how to order.

ELAINE: Oh, no. No. No. No. I got it.

KRAMER: No. No, Elaine!

ELAINE: Hey, I got it. Hey. Didn't you already get soup?

GEORGE: No. I didn't get it.

ELAINE: Why? What happened?

GEORGE: I made a mistake.

ELAINE: [laughing]

GEORGE: Alright. Well, we'll see what happens to you.

ELAINE: Yeah. No. Listen, George, I am quite certain I'm walking out of there with a bowl of soup.

GEORGE: Yeah. Hey, let ask you something. Is it just me, or - or do you find it unbearable to be around Jerry and that girl?

ELAINE: Oh, I know! It is awful!

GEORGE: Why do they have to do that in front of people?

ELAINE: I don't know.

GEORGE: What is that with the shmoopy?

ELAINE: Ohh!

GEORGE: The shmoopy, shmoopy, shmoopy, shmmopy, shmoopy!

ELAINE: Ohh! Stop it! I know.

GEORGE: I had to listen to a five minute discussion on which one is actually called shmoopy.

ELAINE: Ugh!

GEORGE: And I cancelled plans to go to the movies with them tonight.

ELAINE: You know, we should say something.

GEORGE: You know, we absolutely should.

ELAINE: I mean, why does he do that? Doesn't he know what a huge turnoff that is?

GEORGE: I don't know. He can be so weird sometimes.

ELAINE: Yeah.

GEORGE: I still haven't figured him out.

ELAINE: No. Me neither.

GEORGE: Alright. Shh! I gotta focus. I'm shifting into soup mode.

ELAINE: Oh, God!

GEORGE: Good afternoon. One large crab bisque to go. Bread. Beautiful.

SOUP NAZI: You're pushing your luck little man.

GEORGE: Sorry. Thank you.

ELAINE: Hi there. Um, uh -- [drumming on countertop] Oh! Oh! Oh! One mulligatawny and, um.... what is that right there? Is that lima bean?

SOUP NAZI: Yes.

ELAINE: Never been a big fan. [coughing] Um.. you know what? Has anyone ever told you you look exactly like Al Pacino? You know, &quot; Scent Of A Woman.&quot; Who-ah! Who-ah!

SOUP NAZI: Very good. Very good.

ELAINE: Well, I --

SOUP NAZI: You know something?

ELAINE: Hmmm?

SOUP NAZI: No soup for you!

ELAINE: What?

SOUP NAZI: Come back one year! Next!

RAY: Look at this.

BOB: It's an antique.

RAY: It's all hand made and I love the in-lay.

BOB: Yes. Yes. me, too. Ay, it's gorgeous. Completely. Pick it up. No. No. Pick it up from the bottom over there.

KRAMER: Wait. Wait. Wait. Wait. What are you doing?

BOB: What does it look like we're doing? We're taking this.

KRAMER: You can't take this. This belongs to a friend of mine.

BOB: Look, you wanna get hurt?

KRAMER: Huh?

BOB: I don't think you wanna get hurt. Because if you wanna get hurt I can hurt you. Now, just back off.

RAY: Bob.

BOB: Just pick it up.

KRAMER: What is this, huh?

BOB: You have some kind of problem here? What is it you not understanding? We taking the armoire and that's all there is to it. Okay?

ELAINE: I mean, is he allowed to do this? It's discrimination! I'm gonna call the states' attorney office. I really am.

GEORGE: Oh, this is fabulous. My God Elaine, you have to taste this.

ELAINE: Alright. Alright. Give me a taste. Mmm! Oh God, I gotta sit down. What happened? Where's my armoire?

KRAMER: Well, b -- it was stolen.

ELAINE: Wha--?

KRAMER: These street toughs, they robbed me.

ELAINE: Street toughs took my armoire?

KRAMER: Yeah. It was very frightening. My life was in danger. You should've seen the way they talked to me.

ELAINE: I can't believe this!

KRAMER: Well, where's the soup?

ELAINE: Wha -- the Soup Nazi threw me out.

KRAMER: Oh... yeah!

JERRY: What are you gonna get?

SHEILA: I'll decide at the last minute.

JERRY: You better decide, sister. You're on deck. Sheila!

(Soup Nazi pounding on countertop)

JERRY: Uh-oh.

SOUP NAZI: Hey, what is this? You're kissing in my line? Nobody kisses in my line!

SHEILA: I can kiss anywhere I want to.

SOUP NAZI: You just cost yourself a soup!

SHEILA: How dare you? Come on, Jerry, we're leaving. Jerry?

JERRY: Do I know you?

ELAINE: So, essentially, you chose soup over a woman?

JERRY: It was a bisque.

ELAINE: Yeah. You know what I just realized? Suddenly, George has become much more normal than you.

JERRY: Really?

ELAINE: Yeah. Come on. I mean, think about it. He's engaged to be married. Your top priority is soup.

JERRY: Have you tasted the soup?

ELAINE: Yeah. Alright. You made the right decision.

JERRY: See, the way I figure it, it's much easier to patch things up with Sheila than with the Soup Nazi.

JERRY: Hey.

KRAMER: Yeah.

ELAINE: Hey.

KRAMER: Yeah.

JERRY: Oh, thanks.

ELAINE: There he is.

KRAMER: Elaine, I'm really sorry about the armoire.

ELAINE: Yeah. I know. Me, too.

JERRY: So, did these thieves want any money?

KRAMER: No.

JERRY: They just wanted the armoire?

KRAMER: Yeah. They were.. quite taken with it.

(intercom buzzes)

JERRY: Yeah?

GEORGE: Hup! Hup!

JERRY: Hey, have you noticed George is acting a little strange lately?

ELAINE: No. In what way?

JERRY: I don't know. A lot of attitude, like he's better than me, or something.

ELAINE: I don't think George has ever thought he's better than anybody.

GEORGE: Hello.

JERRY: Hello.

KRAMER: Hey.

GEORGE: Hello.

ELAINE: Hello.

GEORGE: Were you just talking about me? What's going on?

JERRY: Absolutely not.

GEORGE: Something's going on here.

KRAMER: Alright, [claps hands] I'm gonna go get some soup.

ELAINE: One of these days that guy is gonna get his.

GEORGE: So, how was the movie?

JERRY: Aw, we didn't go. Sheila and I are kind of on the outs.

GEORGE: Oh, yeah?

JERRY: Yeah. Wha - wha - what are you, happy?

GEORGE: Happy? Why should I be happy?

JERRY: I don't know, but you look like you're happy.

GEORGE: Why should I care?

JERRY: You can't fool me. Don't insult me, George because I know when you're happy.

GEORGE: Alright. I am happy, and I'll tell ya why -- because the two of you were making me and every one of your friends sick! Right, Elaine?

(Elaine sneaks out of Jerry's apartment)

JERRY: Is that so?

GEORGE: Yeah. Yeah. With all that kissing and the shmoopy, shmoopy, shmoopy, shmoopy, shmoopy out in public like that. It's disgusting!

JERRY: Disgusting?

GEORGE: People who do that should be arrested.

JERRY: Well, I guess I have all the more reason to get back with her.

GEORGE: Ye - yeah. And we had a pact, you know.

JERRY: What?

GEORGE: You shook my hand in that coffee shop.

JERRY: You're still with the pact?

GEORGE: Mmm-hmm. You reneged.

JERRY: All I did was shake your hand.

GEORGE: Ah-ha!

KRAMER: And then they just ran off with the armoire, just like that.

SOUP NAZI: Ohh! This city.

NEWMAN: One large jambalaya, please.

SOUP NAZI: So, continue.

KRAMER: Well, my friend is awful disappointed is all. You know, she's very emotional.

NEWMAN: Thank you. [inhaling deeply] Jambalaya!

SOUP NAZI: Alright, now listen to me. You have been a good friend. I have an armoire in my basement. If you want to pick it up, you're welcome to it. So, take it, it's yours.

KRAMER: How can I possibly thank you?

SOUP NAZI: You are the only one who understands me.

KRAMER: You suffer for your soup.

SOUP NAZI: Yes. That is right.

KRAMER: You demand perfection from yourself, from your soup.

SOUP NAZI: How can I tolerate any less from my customer?

CUSTOMER: Uh, gazpacho, por favor.

SOUP NAZI: Por favor?

CUSTOMER: Um, I'm part Spanish.

SOUP NAZI: Adios muchacho!

KRAMER: Git.

JERRY: It was stupid of me.

SHEILA: Well, it was very insulting.

JERRY: No. I know. I - I was really sort of half-kidding.

SHEILA: Well, behind every joke there's some truth.

JERRY: What about that Bavarian cream pie joke I told you? There's no truth to that. Nobody with a terminal illness goes from the United States to Europe for a piece of Bavarian cream pie and then when they get there and they don't have it he says &quot; Aw, I'll just have some coffee.&quot; There's no truth to that.

SHEILA: Well, I guess you're right.

JERRY: So, am I forgiven, shmoopy?

SHEILA: Yes, shmoopy.

JERRY: Aw!

SUSAN: Hey, Jerry!

JERRY: Oh, hi Susan, George. You remember Sheila.

GEORGE: Oh, yes. Hello.

SHEILA: Hello. Won't you join us?

GEORGE: No, thanks.

SUSAN: Of course.

GEORGE: Yes. Well -- So, uh, sit on the same side at a booth, huh?

JERRY: Yeah. That's right. You got a problem?

GEORGE: I, uh, just think it's a little unusual. Two people to sit on one side... and leave the other side empty.

JERRY: Well, we're changing the rules.

GEORGE: Ahh. Good for you.

SUSAN: Aw, what are you getting George?

GEORGE: I don't know, honey. What do you want to get? [in babying voice] I want you to get anything you want...'cause I love you so much. I want you to be happy. Okay, sweetie?

SUSAN: Oh, George, you're so sweet.

GEORGE: Well, I could be a little sweetie tweetie weetie weetie.

SUSAN: Aww!

JERRY: What about you, shmoopy? How 'bout a little tuna? You want a little tuna fishy?

SHEILA: Yeah.

JERRY: Yum yum little tuna fishy?

GEORGE: Come here.

[George &amp; Susan begin making out; Jerry &amp; Sheila begin making out in order to keep up]

KRAMER: And.. voila!

ELAINE: [gasps]

KRAMER: Yeah.

ELAINE: Oh! Oh, I love it! I absolutely love it!

KRAMER: Yeah. Did the K Man do it or did the K Man do it?

ELAINE: The K Man did it!

KRAMER: Yeah!

ELAINE: [laughing] How much did you pay for this thing?

KRAMER: How 'bout zero?

ELAINE: What?

KRAMER: Yeah.

ELAINE: What? Who's was it? Where'd you get it?

KRAMER: I'll tell ya where I got it. I got it from the guy you so callously refer to as the Soup Nazi.

ELAINE: Get out!

[Elaine pushes on Kramer's chest, causing in to fall backwards through her swinging door]

ELAINE: The Soup Nazi gave it to you?

KRAMER: Yeah.

ELAINE: Why?

KRAMER: Well, I told him the whole story and he just let me have it. Wha -- Yeah. He's a wonderful man.

ELAINE: [gasps]

KRAMER: Yeah. Well, a little bit misunderstood but, uh...

ELAINE: Well, I'm just gonna go down there and personally thank him. I mean, I had this guy all wrong. This is wonderful!

KRAMER: Yeah. Well, he's a dear.

GEORGE: How much tip do you leave on $8.15?

SUSAN: You know sweetie, I just want you to know that I was so proud of you today expressing your feelings so freely in front of Jerry and all. Just knowing that you're not afraid of those things is such a great step forward in our relationship.

GEORGE: Huh?

SUSAN: [in babying voice] Because you love your little kiki don't you?

CUSTOMER: How is he today?

BANIA: I think he's in a good mood.

ELAINE: Hi. You know, Kramer gave me the armoire and it is so beautiful. I'm mean, I just can't tell you how much I appreciate it.

SOUP NAZI: You? If I knew it was for you, I never would have given it to him in the first place! I would have taken a hatchet and smashed it to pieces! Now, who wants soup? Next! Speak up!

JERRY: I'm heading over to Elaine's.

KRAMER: Oh. Jerry, those are the guys that mugged me for the armoire.

JERRY: Those two?

KRAMER: Yeah.

JERRY: Are you sure?

KRAMER: Yeah. That's them.

JERRY: Well, let's confront 'em.

KRAMER: No. No. No. No. Let's get a cop.

JERRY: There's no cops around. They're gonna leave. Come on.

KRAMER: No!

JERRY: Let's go.

BOB: Oh, wow look, that one is gorgeous. I would just kill for that one.

RAY: Oh, not in blue. Blue does not go with all.

BOB: Oh, please. Do you know what you're talking about? Because I don't think you know what you're talking about. Take a look at that.

KRAMER: Excuse me.

RAY: Are you talking to me?

KRAMER: Uh, well, uh, we --

RAY: I said, are you talking to me?

BOB: Well, maybe, he was talking to me. Was you talking to him? Because you was obviously talking to one of us. So what is it? Who?! Who was you talking to?!

KRAMER: Well, wha -- I, uh -- uh, we were kind of, uh, talking to each other, weren't we?

[Jerry &amp; Kramer turn around and run away]

ELAINE: I mean, you know, I've never been so insulted in my entire life. There's something really wrong with this man. He is a Soup Nazi. What? What is that?

JERRY: I don't know. &quot; 5 cups chopped Porcine mushrooms, half a cup of olive oil, 3 pounds of celery, chopped parsley...&quot;

ELAINE: Let me see this. [gasps] You know what this is? This is a recipe for soup, and look at this. There are like thirty different recipes. These are his recipes!

JERRY: So?

ELAINE: So? So, his secret's out. Don't you see? I could give these to every restaurant in town. I could have 'em published! I could - I could drop fliers from a plane above the city.

JERRY: Wait a second, Elaine. Where do you think you're going?

ELAINE: What do you care?

JERRY: Elaine, I don't want you causing any trouble down at that soup stand. I happen to love that soup.

ELAINE: Get out of my way, Jerry.

JERRY: Elaine, let the man make his soup!

ELAINE: Don't make me hurt you, Jerry.

SUSAN: Look, they have it in blue... for my baby bluey. Are you my baby bluey?

GEORGE: Oh, yes. I - I'm your baby bluey.

JERRY: Well. Well.

SUSAN: Hi, Jerry.

JERRY: Hey, Susan, George.

SUSAN: You know, I really like Sheila a lot.

JERRY: Oh, really?

SUSAN: Mmm-hmm.

JERRY: Because we're kind of not seeing each other anymore.

SUSAN: Oh, no! That's too bad.

JERRY: Yeah. Well, she was very affectionate - which I love. You know I love that - but mentally, we couldn't quite make the connection.

GEORGE: Really?

JERRY: Yeah. Too bad, 'cause you gotta have the affection - which you obviously have. I think it's great that you're so open with your affections in public. See, we had that.

SUSAN: Mmm-hmm.

GEORGE: You did?

JERRY: Oh, yeah. But the mental thing. But anyway. I'll see ya.

GEORGE: Yeah. See ya.

SOUP NAZI: Go on! Leave! Get out!

WOMAN: But I didn't do anything.

SOUP NAZI: Next!

ELAINE: Hello.

SOUP NAZI: You. You think you can get soup? Please. You're wasting everyone's time.

ELAINE: I don't want soup. I can make my own soup. &quot;5 cups chopped Porcine mushrooms, half a cup of olive oil, 3 pounds celery.&quot;

SOUP NAZI: That is my recipe for wild mushroom.

ELAINE: Yeah, that's right. I got 'em all. Cold cucumber, corn and crab chowder, mulligatawny.

SOUP NAZI: Mulliga... tawny?

ELAINE: You're through Soup Nazi. Pack it up. No more soup for you. Next!

NEWMAN: [panting] Jerry! Jerry! Jerry!

JERRY: What is it?

NEWMAN: Something's happened with the Soup Nazi!

JERRY: Wha - wha - what's the matter?

NEWMAN: Elaine's down there causing all kinds of commotion. Somehow she got a hold of his recipes and she says she's gonna drive him out of business! The Soup Nazi said that now that his recipes are out, he's not gonna make anymore soup! He's moving out of the country, moving to Argentina! No more soup, Jerry! No more for of us!

JERRY: Well, where are you going?

NEWMAN: He's giving away what's left! I gotta go home and get a big pot!

The End
