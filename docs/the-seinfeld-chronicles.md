---
layout: default
title: The Seinfeld Chronicles
parent: Season 1
nav_order: 1
permalink: /the-seinfeld-chronicles
cat: ep
series_ep: 1
pc: 101
season: 1
episode: 1 (Pilot)
aired: July 5, 1989
written: Larry David & Jerry Seinfeld
directed: Art Wolff
imdb: http://www.imdb.com/title/tt0098286/
wiki: https://en.wikipedia.org/wiki/The_Seinfeld_Chronicles
---

# The Seinfeld Chronicles

| Season 1 - Episode 1 (Pilot)            | July 5, 1989          |
|:----------------------------------------|:----------------------|
| Written by Larry David & Jerry Seinfeld | Directed by Art Wolff |
| Series Episode 1                        | Production Code 101   |

"The Seinfeld Chronicles" (also known as "Good News, Bad News" or "Pilot") is the pilot episode of the American sitcom Seinfeld, which first aired on NBC on July 5, 1989.

The first of the 180 Seinfeld episodes, the pilot was written by show creators Larry David and Jerry Seinfeld, and directed by Art Wolff. The episode revolves around a fictionalized version of Seinfeld, who is unsure about the romantic intentions of a woman he met, and frets about the meaning of her signals with his friend George (Jason Alexander) and neighbor Kramer (Michael Richards).

Though they had been asked to put together a 90-minute TV special, Seinfeld and David wrote a TV pilot as they felt their "show about nothing" concept would fit better in a shorter format. The storyline, and the main characters, were inspired by real-life events and people. Though the NBC executives were unsure about the show, they, as Warren Littlefield would later state, "all said, ah what the hell, let's try a pilot on this thing and see what happens". The test audiences, however, reacted extremely negatively. Although NBC would still broadcast the episode to see how audiences and TV critics would react, the network had already decided not to pick up the show, as a result of the test-results.

When The Seinfeld Chronicles aired, it was watched by nearly 11% of American households, and received generally favorable reviews from critics, who reacted with disappointment that NBC did not order a first season. Convinced that the show had potential, and supported by the positive reviews, NBC executive Rick Ludwin managed to convince his superiors to order a four-episode first season (the smallest order in TV history), by offering a part of his personal budget in return. The show, renamed Seinfeld, would go on to become one of the most successful sitcoms in television history.

## Plot

The series opens with Jerry Seinfeld (Jerry Seinfeld) and George Costanza (Jason Alexander) seated at Pete's Luncheonette, debating the placement of one of George's shirt buttons. Jerry tells George about a woman he met in Lansing, Michigan, Laura (Pamela Brull), who is coming to New York, and the two discuss whether or not she has romantic intentions. The next evening, Jerry tells his neighbor Kessler (Michael Richards) that he thinks he misunderstood the situation with Laura. However, he then receives a telephone call from Laura, who asks if she can stay overnight at his apartment. Though Jerry agrees, he is still unsure whether or not her visit is intended to be romantic. George and Jerry continue to debate the issue, with Jerry determined to find the true nature of her visit.

While waiting at the airport for Laura to arrive, Jerry and George try to identify the possible signals Laura might give upon her arrival, with George explaining the meaning of various greetings. However, when Laura arrives, her greeting is ambiguous. Upon arriving at Jerry's apartment Laura removes her shoes and some excess clothing to get comfortable, asks for wine, and turns down the light and asks if she can stay over a second night. As Jerry removes his own shoes and begins to grow confident, the phone rings for Laura. When Laura gets off the phone she tells Jerry: "Never get engaged." Jerry then realizes that he has no chance with Laura, but has already committed himself – and his one-bedroom apartment – to an entire weekend with her, including a five-hour sightseeing boat ride around Manhattan.

## Production

### Concept and writing

The Seinfeld Chronicles was written as the pilot for the show that would eventually be called Seinfeld, though earlier versions of the script would refer to the program as Stand Up and The Jerry Seinfeld Show. The idea for the show started on November 2, 1988, after NBC executives had approached comedian Jerry Seinfeld to do a project with the network, upon a suggestion by George Shapiro, Seinfeld's manager at the time. Seinfeld enlisted fellow comedian Larry David to help him develop it, and they wrote a concept for a special about where comedians get their material. However, upon further discussion, Seinfeld felt that the concept could not be sustained for 90 minutes, upon which the two decided that the project was to become a pilot for a TV-series, rather than a special. Developed by NBC executive Rick Ludwin, and produced by Castle Rock Entertainment, it was a mix of Seinfeld's stand-up comedy routines and idiosyncratic, conversational scenes focusing on mundane aspects of everyday life.

Conceived as a "show about nothing," in which the main characters would "just make fun of stuff", Seinfeld said that the idea of the pilot episode was to explore the "gaps in society where there are no rules." The storyline, as well as most of the main characters, were inspired by the personal lives of its creators. Jerry was a fictionalized version of Seinfeld, George a fictionalized version of Larry David and Kessler was based on David's neighbor Kenny Kramer. Though Seinfeld was initially concerned the "wacky neighbor" would be too much of a cliché, David convinced him to put the character in the script. However, anticipating that the actual Kramer would exploit the benefits of having a TV character based on him, David hesitated to call the character Kramer. Thus, in the pilot, the character's name was "Kessler". However, intrigued by the name, Seinfeld was convinced that the character's name should be Kramer, prompting Kenny Kramer to call NBC's legal department with various financial and legal demands, most of which he received. The name inconsistency would eventually be corrected in the season 9 episode "The Betrayal" in which Kramer explains that Kessler is the name on his apartment buzzer.

David and Seinfeld re-wrote the script several times before submitting it to the network, dropping and adding various elements. Originally George, who was called Bennett in early drafts, was a comedian as well, and the first scene of the episode focussed on Jerry and George discussing their stand-up material. The character of Kramer, was not included in the first draft of the script, and in another draft he was called "Hoffman". Another element that was added was Kessler's dog, since it was originally planned that Jerry's stand-up routines would match the events of each episode. Though the stand-up routine about dogs was eventually dropped, the scene in which Kessler enters with his dog remained in the episode. When David and Seinfeld eventually submitted the script, the network executives were unsure whether or not to produce the pilot, but as NBC executive Warren Littlefield would later state "we all said, ah what the hell, let's try a pilot on this thing and see what happens".

Directed by Art Wolff, the pilot was filmed in front of a live studio audience on April 27, 1989, at Stage 9 of Ren-Mar Studios, the same studio where The Dick Van Dyke Show was filmed, which was seen by the crew as a good omen. The exterior of Pete's Luncheonette, the restaurant in which the episode opens, was a leftover set piece from The Muppets Take Manhattan (1984). Seinfeld's stand-up routine was recorded at Ren-Mar Studios in Hollywood, in front of an audience of paid extras, though not all of the recorded material was included in the broadcast version. Additionally, a scene was recorded featuring Jerry and George driving to the airport talking about changing lanes on the road and giving "Thank you waves", but was cut before broadcasting. The music used in the episode was composed by Jep Epstein; however, when the show was picked up, Epstein's tune was replaced by the trademark slap bass music by Jonathan Wolff.

### Casting

The Seinfeld Chronicles featured four characters that were intended to be series regulars if the show was to be picked-up for a first season: Jerry, George, Kramer and Claire the waitress. Though it was already settled that Seinfeld would play a fictionalized version of himself, auditions were held for the other three characters. Though George was based on Larry David, David was keen on writing, and did not have the desire to portray the character himself. Prior to the casting progress, Seinfeld pleaded with his friend Jake Johannsen to play the part, but he rejected it. When the casting process started, as casting director Marc Hirschfeld stated, the casting crew "saw every actor \[they\] could possibly see in Los Angeles". Among these actors were Larry Miller, Brad Hall, David Alan Grier, and Nathan Lane yet none of them seemed fit for the part. Jason Alexander auditioned for the part via a video tape, though he had very little hope for being cast, as he felt he was doing a Woody Allen impression. However, upon watching the tape, David and Seinfeld were immediately convinced Alexander would be the right actor to cast. However, traditionally casting sessions work with rounds, so Alexander and a few other actors considered for the role were flown to Los Angeles for a second audition. One of the other actors who made it to this round was Larry Miller. As Alexander knew Miller was a close friend of Seinfeld, he was convinced he would not get the part, but eventually did.

Kenny Kramer initially demanded that he would play the part of Kessler, as he served as the inspiration for the character. However, David did not want this and it was decided upon that casting sessions would be held. Among those who auditioned for the part of Kramer were Steve Vinovich, Tony Shalhoub and Larry Hankin. Although he was not cast for the part Hankin would later portray an in-show fictional version of Kramer in the season four episode "The Pilot". Seinfeld and David were both familiar with Michael Richards, and David had worked with him on Fridays. Richards did his final audition at the Century Plaza Hotel on April 18, 1989, reputedly finishing with a handstand. David was not sure about casting Richards, as he was trying to cast an actor that resembled the original Kramer. However, impressed by Richards' audition, Seinfeld convinced David that Richards would be the right actor for the part.

Lee Garlington was cast as Claire the Waitress, who in an earlier draft of the episode was called "Meg". Though initially cast as a series regular, the character was replaced with Elaine Benes when the series was picked up for a first season. Accounts differ on the reason why the character was replaced. Warren Littlefield has said that it was because the character's occupation: "I thought that as a waitress she'd never be one of the gang. She'd be relegated to pouring coffee, catching up. So I insisted they create a female character they wanted to spend time with". Dennis Bjorklund of Seinfeld Reference has suggested that the character was dropped in favor of a female character with more sex appeal. However, Alexander said that Garlington was written out of the series because she had re-written her scene and given it to David, who was not happy with this. Seinfeld has, however, stated that this was not the reason the character was removed from the show, but rather that the producers were looking for "someone who was more involved". Julia Louis-Dreyfus, who would go on to replace Garlington, has stated that she was not aware of the pilot before becoming a regular on the show, and she will never watch it out of superstition.

## Reception

The pilot was first screened by a group of two dozen NBC executives in Burbank, California in early 1989. Although the pilot did not yield the explosion of laughter garnered by the pilots for the decade's previous NBC successes like The Cosby Show and The Golden Girls, it drew mostly positive responses from the assembled executives. One exception was Brandon Tartikoff, who was concerned that the show was "Too New York, too Jewish". Before the episode's TV premiere it was shown to a test audience of 400 households, and met with extremely negative responses. Littlefield would later recall "In the history of pilot reports, Seinfeld has got to be one of the worst of all time". The memo that summarized the test audience's reaction, contained feedback such as "No segment of the audience was eager to watch the show again" and "None of the \[supporting characters\] were particularly liked". Despite the low rating the show received from its test audience, the first public broadcast of "The Seinfeld Chronicles" took place on the fifth of July 1989, to see how viewers would react, even though the executives had already decided the show would not be picked-up for a full season. "The Seinfeld Chronicles" finished second in its time slot, behind the CBS police drama Jake and the Fatman, receiving a Nielsen rating of 10.9/19, meaning that the pilot was watched by 10.9% of American households, and that 19% of all televisions in use at the time were tuned into it. With these ratings "The Seinfeld Chronicles" finished in the 21st place of the week it was broadcast, tied with Fox's Totally Hidden Video.

Unlike the test audience, television critics generally reacted positively to the pilot, viewing it as original and innovative, USA Today critic Tom Green summarized the show as a "crisply funny blend of stand-up routines interwoven with more traditional sitcom stuff". Eric Mink of St. Louis Post-Dispatch wrote he thought the show was unusual and intriguing, yet "quite funny". Joe Stein of the San Diego Evening Tribune commented "Not all standup comedians fit into a sitcom format, but Seinfeld does". A more negative response came from a The Fresno Bee critic, that stated "I liked the concept, but Seinfeld 's jokes were so dull that you hoped the standup stuff would fly by so you could get back to the story". Though the critic praised Alexander's acting he commented that his performance was not enough to keep the show "from being just another piece of summer drivel offered up by a major commercial network".

Various critics compared the pilot to It's Garry Shandling's Show. In his review of the episode The Philadelphia Inquirer critic Ken Tucker commented "Seinfeld's brisk funniness prevents Chronicles from being a rip-off", while Jerry Krupnick of The Star-Ledger felt that The Seinfeld Chronicles differentiated itself from It's Garry Shandlings Show by its supporting cast, which he praised. By contrast, John Voorhees of The Seattle Times commented that, though he thought the show was amusing, he considered It's Garry Shandlings Show to be better, and the Houston Chronicle's Mike McDaniel referred to the pilot as "a not-as-good Garry Shandling-like show".

Most critics reacted with disappointment to the fact that NBC had not picked up the show. Bob Niedt of the Syracuse Herald-Journal commented "What gives? Comedy this good, and NBC is keeping -- excuse me -- A Different World on the schedule?". Ken Tucker stated "NBC is making a mistake if it doesn't pick up The Seinfeld Chronicles as a midseason replacement; it's bound to be superior to most of what the network has planned for the fall". Additionally casting directors Hirschfeld and Meg Liberman were nominated for a Casting Society of America Artios Award for 'Best Casting for TV, Pilot', but lost to the casting directors of Northern Exposure.

Though the network executives had decided not to pick up The Seinfeld Chronicles for a first season, some of them were reluctant to give up on it, as they felt the series had potential. Rick Ludwin, one of the show's greatest supporters, eventually made a deal with Tartikoff, giving up some of his own development money, cancelling a Bob Hope special, so that the entertainment division could order four more episodes of The Seinfeld Chronicles, which formed the rest of the show's first season. Although this was a very low order number for a new series (the smallest sitcom order in television history), Castle Rock failed to find any other buyers when it tried to sell the show to other networks, and accepted the order. About a year later, the first season would premiere, the show was renamed Seinfeld, to avoid confusion with ABC's The Marshall Chronicles. To lead in the first official season of Seinfeld, the pilot episode was repeated on June 28, 1990; it received a Nielsen rating of 13.9/26.

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Michael Richards ................. Kessler

#### Guests

Lee Garlington ................ Claire  
Pamela Brull ..................... Laura

## Script

INT. COMEDY CLUB - NIGHT

(Jerry is on stage, performing.)

JERRY: Do you know what this is all about? Do you know, why we're here? To be out, this is out... and out is one of the single most enjoyable experiences of life. People... did you ever hear people talking about "We should go out"? This is what they're talking about... this whole thing, we're all out now, no one is home. Not one person here is home, we're all out! There are people tryin' to find us, they don't know where we are. (on an imaginary phone) "Did you ring?, I can't find him." "Where did he go?" "He didn't tell me where he was going". He must have gone out. You wanna go out: you get ready, you pick out the clothes, right? You take the shower, you get all ready, get the cash, get your friends, the car, the spot, the reservation... Then you're standing around, whatta you do? You go: "We gotta be getting back". Once you're out, you wanna get back! You wanna go to sleep, you wanna get up, you wanna go out again tomorrow, right? Where ever you are in life, it's my feeling, you've gotta go.

INT. PETE'S LUNCHEONETTE - DAY

(Jerry and George sit at a table, having coffee.)

JERRY: (pointing at George's shirt) See, to me, that button is in the worst possible spot. The second button literally makes or breaks the shirt, look at it. It's too high! It's in no-man's-land. You look like you live with your mother.

GEORGE: Are you through?

JERRY: You do of course try on, when you buy?

GEORGE: Yes, it was purple, I liked it, I don't actually recall considering the buttons.

JERRY: Oh, you don't recall?

GEORGE: (on an imaginary microphone) Uh, no, not at this time.

JERRY: Well, senator, I'd just like to know, what you knew and when you knew it.

(A waitress, Claire, approaches the table. She pours refills Jerry's coffee.)

CLAIRE: Mr. Seinfeld. Mr. Costanza.

(Claire tries to refill George's coffee, but George blocks her.)

GEORGE: Are, are you sure this is decaf? Where's the orange indicator?

CLAIRE: It's missing, I have to do it in my head: decaf left, regular right, decaf left, regular right it's very challenging work.

JERRY: Can you relax, it's a cup of coffee. Claire is a professional waitress.

CLAIRE: Trust me George. No one has any interest in seeing you on caffeine.

(Claire exits.)

GEORGE: How come you're not doin' the second show tomorrow?

JERRY: Well, there's this uh, woman might be comin' in.

GEORGE: Wait a second, wait a second, what coming in, what woman is coming in?

JERRY: I told you about Laura, the girl I met in Michigan?

GEORGE: No, you didn't!

JERRY: I thought I told you about it, yes, she teaches political science? I met her the night I did the show in Lansing...

GEORGE: Ha.

JERRY: (looks in the creamer) There's no milk in here, what...

GEORGE: Wait wait wait, what is she... (takes the milk can from Jerry and puts it on the table) What is she like?

JERRY: Oh, she's really great. I mean, she's got like a real warmth about her and she's really bright and really pretty and uh... the conversation though, I mean, it was... talking with her is like talking with you, but, you know, obviously much better.

GEORGE: (smiling) So, you know, what, what happened?

JERRY: Oh, nothing happened, you know, but is was great.

GEORGE: Oh, nothing happened, but it was...

JERRY: Yeah.

GEORGE: This is great!

JERRY: Yeah.

GEORGE: So, you know, she calls and says she wants to go out with you tomorrow night? God bless! Devil you!

JERRY: Yeah, well... not exactly. I mean, she said, you know, she called this morning and said she had to come in for a seminar and maybe we'll get together.

GEORGE: (whistles disapprovingly) Ho ho ho, 'Had to'? 'Had to' come in?

JERRY: Yeah, but...

GEORGE: 'Had to come in' and 'maybe we'll get together'? 'Had to' and 'maybe'?

JERRY: Yeah!

GEORGE: No... no... no, I hate to tell you this. You're not gonna see this woman.

JERRY: What, are you serious... why, why did she call?

GEORGE: How do I know, maybe, you know, maybe she wanted to be polite.

JERRY: To be polite? You are insane!

GEORGE: Alright, alright, I didn't want to tell you this. You wanna know why she called you?

JERRY: Yes!

GEORGE: You're a back-up, you're a second-line, a just-in-case, a B-plan, a contingency!

JERRY: Oh, I get it, this is about the button.

(Claire passes the table; George stops her and writes something on his notepad.)

GEORGE: Claire, Claire, you're a woman, right?

CLAIRE: What gave it away, George?

GEORGE: Uhm... I'd like to ask you... ask you to analyze a hypothetical phone call, you know, from a female point of view.

JERRY (to George) Oh, come on now, what are you asking her? Now, how is she gonna know?

GEORGE: (to Claire) Now, a woman calls me, alright?

CLAIRE: Uh huh.

GEORGE: She says she 'has to' come to New York on business...

JERRY: Oh you are beautiful!

GEORGE: ...and, and 'maybe' she'll see me when she gets there, does this woman intend to spend time with me?

CLAIRE: I'd have to say, uuhh, no.

(George shows his note-block to Jerry; it says very largely: NO.)

GEORGE (to Claire) So why did she call?

CLAIRE: To be polite.

GEORGE: To be polite. I rest my case.

JERRY: Good. Did you have fun? You have no idea, what you're talking about, now, come on, come with me. (stands up) I gotta go get my stuff out of the dryer anyway.

GEORGE: I'm not gonna watch you do laundry.

JERRY: Oh, come on, be a come-with guy.

GEORGE: Come on, I'm tired.

CLAIRE: (to Jerry) Don't worry, I gave him a little caffeine. He'll perk up.

GEORGE: (panicking) Right, I knew I felt something!

(Claire exits, smiling.)

INT. LAUNDROMAT - DAY

(Jerry and George are there; George is staring at one of the dryers.)

GEORGE: Jerry? I have to tell you something. This is the dullest moment I've ever experienced.

(George walks away from the dryer. A man passes by.)

JERRY: Well, look at this guy. Look, he's got everything, he's got detergents, sprays, fabric softeners.  This is not his first load.

GEORGE: I need a break, Jerry, you know. I gotta get out of the city. I feel so cramped...

JERRY: And you didn't even hear how she sounded.

GEORGE: What?!

JERRY: Laura.

GEORGE: I can't believe- (falls on his knees) WE ALREADY DISCUSSED THIS!

JERRY: Yeah, but how could you be so sure?

GEORGE: 'Cause it's signals, Jerry, it's signals! (snapping his fingers) Don't you- alright. Did she even ask you, what you were doin' tomorrow night, if you were busy?

JERRY: No.

GEORGE: She calls you today and she doesn't make a plan for tomorrow? What is that? It's Saturday night!

JERRY: Yeah.

GEORGE: What is that? It's ridiculous! You don't even know what hotel she's staying at, you can't call her. That's a signal, Jerry, that's a signal! (snaps his fingers) Signal!

JERRY: Maybe you're right.

GEORGE: Maybe I'm right? Of course I'm right.

JERRY: This is insane. You know, I don't even know where she's staying! She, she's not gonna call me, this is unbelievable.

(George puts his arm around Jerry.)

GEORGE: I know, I know. Listen, your stuff has to be done by now, why don't you just see if it's dry?

JERRY: No no no, don't interrupt the cycle. The machine is working, it, it knows what it's doing. Just let it finish.

GEORGE: You're gonna overdry it.

JERRY: You, you can't overdry.

GEORGE: Why not?

JERRY: Same as you can't overwet. You see, once something is wet, it's wet. Same thing with death. Like once you die you're dead, right? Let's say you drop dead and I shoot you. You're not gonna die again, you're already dead. You can't overdie, you can't overdry.

GEORGE: (to the other laundry patrons) Any questions?

JERRY: How could she not tell me where she was staying?

(George stands by the dryer again and secretly opens it. The dryer stops working and he closes the lid.)

GEORGE: Look at that. They're done!

INT. COMEDY CLUB - NIGHT

(Jerry is on stage, performing.)

JERRY: Laundry day is the only exciting day in the life of clothes. It is... y'know, think about it. The washing machine is the nightclub of clothes. You know, it's dark, there's bubbles happening, they're all kinda dancing around in there- shirt grabs the underwear, "C'mon babe, let's go". You come by, you open up the lid and they'll- (stiffens up, as the clothes) Socks are the most amazing article of clothing. They hate their lives, they're in the shoes with stinky feet, the boring drawers. The dryer is their only chance to escape and they all know it. They knew a escape from the dryer. They plan it in the hamper the night before, "Tomorrow, the dryer, I'm goin'. You wait here!" The dryer door swings open and the sock is waiting up against the side wall. He hopes you don't see him and then he goes down the road. They get buttons sewn on their faces, join a puppet show. So they're showing me on television the detergent for getting out bloodstains. Is this a violent image to anybody? Bloodstains? I mean, come on, you got a T-shirt with bloodstains all over it, maybe laundry isn't your biggest problem right now. You gotta get the harpoon out your chest first.

INT. JERRY'S APARTMENT

(Jerry is on the couch, watching TV. The phone rings. He picks up the receiver.)

JERRY: (answering, quickly) If you know what happened in the Met game, don't say anything, I taped it, hello. Yeah, no, I'm sorry, you have the wrong number. Yeah, no.

(There's a knock at the door.)

JERRY: (to the door) Yeah?

(Kessler enters.)

KESSLER: Are you up?

JERRY: (to Kessler) Yeah. (to the phone) Yeah, people do move. Have you ever seen the big trucks out on the street? Yeah, no problem.

(Jerry hangs up.)

KESSLER: Boy, the Mets blew it tonight, huh?

JERRY: (upset) Ohhhh, what are you doing? Kessler, it's a tape! I taped the game, it's one o'clock in the morning! I avoided human contact all night to watch this.

KESSLER: Hey, I'm sorry, I- you know, I, I thought you knew. (takes two loaves of bread out of his pockets, and holds them out to Jerry.) You got any meat?

JERRY: Meat? I don't, I don't know, go... hunt! (Kessler opens the refrigerator and sticks his head in.) Well what happened in the game anyway?

KESSLER: (from the refrigerator) What happened? Well, they STUNK, that's what happened!

(He takes some meat from the refrigerator and closes it.)

KESSLER: You know, I almost wound up going to that game.

JERRY: (cynical) Yeah you almost went to the game. You haven't been out of the building in ten years!

KESSLER: Yeah. (Jerry sits down on the couch. Kessler walks over with his sandwich and looks at Jerry and uses expressions to ask Jerry to move the newspapers on the other side of the couch so he could site down. Kessler sits down next to him and starts turning over the pages of a magazine. Suddenly he spots an article he likes and tears it out. Jerry gives him a look as if to say, "Do you mind?") Are you done with this?

JERRY: No.

(Kessler glues the article back with his own saliva and puts the magazine back on the table.)

KESSLER: When you're done, let me know.

JERRY: Yeah, yeah... you can have it tomorrow.

KESSLER: I thought I wasn't allowed to be in here this weekend.

JERRY: No, it's okay now, that, that girl is not comin'. Uh, I misread the whole thing.

KESSLER: You want me to talk to her?

JERRY: I don't think so.

KESSLER: Oh, I can be very persuasive. Do you know that I was almost... a lawyer.

(Kessler shows with his fingers how close he was.)

JERRY: That close, huh?

KESSLER: You better believe it.

(The phone rings. Jerry picks it up.)

JERRY: Hello... Oh, hi, Laura.

KESSLER: Oh, give me it... let me talk to her.

JERRY: (to the phone) No believe me, I'm always up at this hour. How are you?... Great... Sure... What time does the plane get in?... I got my friend George to take me...

KESSLER: (to the TV) SLIDE! Wow!

JERRY: No, it's, it's just my neighbor... Um... Yeah, I got it. (Jerry takes a pencil and a cereal box to write on.) Ten-fifteen... No, don't be silly, go ahead and ask... Yeah, sure... Okay, great, no no, it's no trouble at all... I'll see you tomorrow... Great, bye. (He hangs up the phone; to Kessler) I don't believe it. That was her. She wants to stay here!

INT. JERRY'S APARTMENT - NIGHT

(Jerry and George enter, carrying a heavy mattress.)

JERRY: If my father was moving this he'd had to have a cigarette in his mouth the whole way. (as his father) `Have you got your end?... Your end's got to come down first, easy now, drop it down... drop it down, your end's got to come down.'

GEORGE: You know, I can't believe you're bringin' in an extra bed for woman that wants to sleep with you. Why don't you bring in an extra guy too?

(George takes a seat. Jerry hands him a beer.)

JERRY: Look, it's a very awkward situation. I don't wanna be presumptuous.

GEORGE: Alright, alright, one more time, one more time! What was the exact phrasing of the request?

JERRY: Alright, she said she couldn't find a decent hotel room...

GEORGE: A decent hotel-room...

JERRY: Yeah, a decent hotel-room, would it be terribly inconvenient if she stayed at my place.

GEORGE: You can't be serious. This is New York city. There must be eleven million decent hotel rooms! Whatta you need? A flag? (waving his handkerchief) This is the signal, Jerry, this is the signal!

JERRY: (cynical) This is the signal? Thank you, Mr. Signal. Where were you yesterday?

GEORGE: I think I was affected by the caffeine.

(Suddenly a dog enters the apartment and jumps George on the couch.)

GEORGE: Ho, ho, ho, good dog, good dog...

(Kessler enters and closes the door.)

KESSLER: Hey, he really likes you, George.

GEORGE: Well, that's flattering.

(The dog heads for the bathroom and starts drinking from the toilet.)

KESSLER: Oh, he's gettin' a drink of water. (pointing to the mattress) Is this for that girl?

JERRY: Yeah.

KESSLER: Why even give her an option?

JERRY: This is a person I like, it's not "How to score on spring break".

GEORGE: Right, can we go? 'Cause I'm double-parked, I'm gonna get a ticket.

JERRY: Yeah, okay. Oh, wait a second. Oh, I forgot to clean the bathroom.

GEORGE: So what? That's good.

JERRY: Now, how could that be good?

GEORGE: Because filth is good. Whaddaya think, rock stars have sponges and ammonia lyin' around the bathroom? They, have a woman comin' over, "I've gotta tidy up?" Yeah right, in these matters you never do what your instincts tell you. Always, ALWAYS do the opposite.

JERRY: This is how you operate?

GEORGE: Yeah, I wish.

JERRY: Let me just wipe the sink.

KESSLER: Why even give her an option for?

(Jerry walks to the bathroom and closes the door.)

KESSLER: (to George, pointing at the mattress) It's unbelievable.

GEORGE: Yeah.

KESSLER: How's the real estate-business?

GEORGE: (feeling awkward) It's uh, not bad, it's comin' along. Why? Did you need something.

KRAMER: Do you handle any of that commercial... real estate?

GEORGE: Well, I might be getting into that.

KESSLER: (slaps George on the arm) You keep me posted!

GEORGE: I'm aware of you. (to Jerry) Alright, let's go (opens the bathroom door) Let's go!

(Jerry and the dog come out.)

INT. COMEDY CLUB - NIGHT

(Jerry on stage; performing)

JERRY: The dating world is not a fun world... it's a pressure world, it's a world of tension, it's a world of pain... and you know, if a woman comes over to my house, I gotta get that bathroom ready, 'cause she needs things. Women need equipment. I don't know what they need. I know I don't have it, I know that- You know what they need, women seem to need a lot of cotton-balls. This is the one I'm- always has been one of the amazing things to me... I have no cotton-balls, we're all human beings, what is the story? I've never had one... I never bought one, I never needed one, I've never been in a situation, when I thought to myself: "I could use a cotton-ball right now." I can certainly get out of this mess. Women need them and they don't need one or two, they need thousands of them, they need bags, they're like peat moss bags, have you ever seen these giant bags? They're huge and two days later, they're out, they're gone, the, the bag is empty, where are the cotton-balls, ladies? What are you doin' with them? The only time I ever see 'em is in the bottom of your little waste basket, there's two or three, that look like they've been through some horrible experience... tortured, interrogated, I don't know what happened to them. I once went out with a girl who's left a little zip-lock-baggy of cotton-balls over at my house. I don't know what to do with them, I took them out, I put them on my kitchen floor like little tumbleweeds. I thought maybe the cockroaches would see it, figure this is a dead town. "Let's move on." The dating world is a world of pressure. Let's face it: a date is a job interview that lasts all night. The only difference between a date and a job interview is not many job interviews is there a chance you'll end up naked at the end of it. You know? "Well, Bill, the boss thinks you're the man for the position, why don't you strip down and meet some of the people you'll be workin' with?"

INT. AIRPORT - NIGHT

(Jerry and George are waiting for Laura.)

JERRY: Wouldn't it be great if you could ask a woman what she's thinking?

GEORGE: What a world that would be, if you just could ask a woman what she's thinkin'.

JERRY: You know, instead, I'm like a detective. I gotta pick up clues, the whole thing is a murder investigation.

GEORGE: Listen, listen, don't get worked up, 'cause you're gonna know the whole story the minute she steps off the plane.

JERRY: Really? How?

GEORGE: 'Cause it's all in the greeting.

JERRY: Uh-huh.

GEORGE: Alright, if she puts the bags down before she greets you, that's a good sign.

JERRY: Right.

GEORGE: You know, anything in the, in the lip area is good.

JERRY: Lip area.

GEORGE: You know a hug: definitely good.

JERRY: Hug is definitely good.

GEORGE: Sure.

JERRY: Although what if it's one of those hugs where the shoulders are touching, the hips are eight feet apart?

GEORGE: That's so brutal, I hate that.

JERRY: You know how they do that?

GEORGE: That's why, you know, a shake is bad.

JERRY: Shake is bad, but what if it's the two-hander? The hand on the bottom, the hand on the top, the warm look in the eyes?

GEORGE: Hand sandwich.

JERRY: Right.

GEORGE: I see, well, that's open to interpretation. Because so much depends on the layering and the quality of the wetness in the eyes.

(A woman approaches Jerry from behind and puts her hands over Jerry's eyes.)

LAURA: Guess who?

JERRY: Hey, hey.

LAURA & JERRY: Heeeey!

(They take each others hands and shake them around. George is looking puzzled.)

JERRY: It's good to see you.

LAURA: Hi.

JERRY: This is my friend George.

LAURA: Hi, how nice to meet you.

GEORGE: Hi, how are you?

JERRY: This is Laura.

GEORGE: Laura, sure.

JERRY: (to Laura) I can't believe you're here.

GEORGE & JERRY: Ooh yeah, the bags, sure.

(They pick up the bags.)

LAURA: Oh, thank you.

JERRY: (aside, to George) Now that was an interesting greeting, did you notice that, George?

GEORGE: Yes, the surprise blindfold greeting. That wasn't in the manual, I don't know.

INT. JERRY'S APARTMENT - NIGHT

(Jerry is in the midst of showing Laura the apartment.)

JERRY: So uh, what do you think?

LAURA: Ooohhh, wow! This place isn't so bad.

JERRY: Yeah, it kind a motivates me to work on the road. So uh, make yourself at home. (Laura sits down on the couch, takes off her shoes and opens some buttons of her shirt.) So uh, can I get you anything? Uuhhh, bread, water... salad-dressing?

LAURA: (laughs) Actually, um, do you have any wine?

JERRY: Uh, yeah, I think I do.

LAURA: (referring to a lamp) Oh, do you mind if I turn this down?

JERRY: Uh, no, yeah, go right ahead.

(She turns down the lamp.)

LAURA: Uh, Jerry, uh, I was wandering, would it be possible - and if it's not, fine - for me to stay here tomorrow night too?

(Jerry takes off his shoes to make himself comfortable)

JERRY: Uh, yeah, yeah, sure, why don't you stay? Yeah, uhm... What is your, what is your schedule for tomorrow? Are you, are you doin' anything?

LAURA: No, I'd love to do something. Uh, I have my seminar in the morning, then after that I'm right open.

JERRY: Really? What would you like to do?

LAURA: Well... now I know this sounds touristy, but I'd just love to go on one of those five-hour boat rides around Manhattan.

JERRY: (unenthusiastic) Yeah, we could do that... why not, why not. (pouring the wine) I'm just, I'm really glad you're here.

(The phone rings; he picks it up.)

JERRY: (answering) Yeah, hello... yes... yes, she is, hold on. (to Laura) Um, it's for you.

LAURA: (to the phone) Hello?... Hi!... No no it was great, right on time... No, I, I'm gonna stay here tomorrow... yes, yes it's fine... No, we're goin' on a boat ride... Don't be silly... I'm not gonna have this conversation... Look, I'll call you tomorrow... Okay, bye. (She hangs up the phone.) Never get engaged.

JERRY: (not excited) You're engaged?

LAURA: You, you really have no idea what it's like until you actually do it. And I'm on this emotional roller coaster.

JERRY: You're engaged?

LAURA: You know, I can't believe it myself sometimes. You have to start thinking in terms of "we". Ugh, it's a very stressful situation.

JERRY: (matter-of-factly) You're engaged.

LAURA: Yeah, yeah, he's a great guy...

JERRY: Yeah.

LAURA: You'd really like him, you know, I can't wait to get on that boat.

JERRY: Me too!

INT. COMEDY CLUB - NIGHT

(Jerry is on stage, performing.)

JERRY: I swear, I have absolutely no idea what women are thinking. I don't get it, okay? I, I, I admit, I, I'm not getting the signals. I am not getting it! Women, they're so subtle, their little... everything they do is subtle. Men are not subtle, we are obvious. Women know what men want, men know what men want, what do we want? We want women, that's it! It's the only thing we know for sure, it really is. We want women. How do we get them? Oh, we don't know 'bout that, we don't know. The next step after that we have no idea. This is why you see men honking car-horns, yelling from construction sites. These are the best ideas we've had so far. The car-horn honk, is that a beauty? Have you seen men doing this? What is this? The man is in the car, the woman walks by the front of the car, he honks. E-eeehh, eehhh, eehhh! This man is out of ideas. How does it...? E-e-e-eeeehhhh! "I don't think she likes me." The amazing thing is, that we still get women, don't we? Men, I mean, men are with women. You see men with women. How are men getting women, many people wonder. Let me tell you a little bit about our organization. Wherever women are, we have a man working on the situation right now. Now, he may not be our best man, okay, we have a lot of areas to cover, but someone from our staff is on the scene. That's why, I think, men get frustrated, when we see women reading articles, like: "Where to meet men?" We're here, we are everywhere. We're honking our horns to serve you better.

The End
