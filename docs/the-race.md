---
layout: default
title: The Race
parent: Season 6
nav_order: 10
permalink: /the-race
cat: ep
series_ep: 96
pc: 612
season: 6
episode: 10
aired: December 15, 1994
written: Tom Gammill & Max Pross and Larry David & Sam Kass (story). Teleplay by Tom Gammill & Max Pross and Larry David
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697763
wiki: https://en.wikipedia.org/wiki/The_Race_(Seinfeld)
---

# The Race

| Season 6 - Episode 10                                                                                                       | December 15, 1994         |
|:----------------------------------------------------------------------------------------------------------------------------|:--------------------------|
| Written by Tom Gammill & Max Pross and Larry David & Sam Kass (story) - Teleplay by Tom Gammill & Max Pross and Larry David | Directed by Andy Ackerman |
| Series Episode 96                                                                                                           | Production Code 612       |

"The Race" is the 96th episode of the American sitcom Seinfeld, the tenth episode of the sixth season. The episode first aired on December 15, 1994. The story follows Jerry as he meets an old rival, who suspects that he cheated in a high school race and wishes to re-run it. Elaine is put on a "blacklist" and finds out her boyfriend is a communist, which sees George respond to a personal ad in the Daily Worker and Kramer, who is working as a department store Santa Claus, is eventually convinced to become a communist by Elaine's boyfriend.

## Plot

Jerry is excited to finally be dating a woman named Lois (Renee Props). However, Jerry is stunned when he finds out that Lois works for Duncan Meyer (Don McManus), his old rival from high school. Elaine's complaints about her Chinese food delivery cause her to be blacklisted from Hop Sing's. George notes to Elaine that Ned (Todd Kimsey), her new boyfriend, has a copy of the Daily Worker, which prompts suspicion of Ned being a communist. George is intrigued by one of the personal ads, which remarks, "Appearance not important." Jerry recounts his rivalry with Duncan: in a track race in ninth grade, Jerry had gotten an inadvertent head start that nobody noticed and won. Though he was praised for his seemingly amazing speed, only Duncan remained skeptical.

Lois quizzes Jerry about cheating in the race; Jerry defends his win, and Lois believes him. Ned admits to Elaine he is a communist. George announces he contacted a girl from The Daily Worker. Kramer gets ready for his new job as Santa Claus at Coleman's department store, with Mickey as his elf. Lois arranges lunch at Monk's with her, Jerry, and Duncan, and Jerry knows that the subject of the race will come up. George agrees to turn up at the coffee shop, pretend he has not seen Jerry since high school, and back up his winning story.

At Yankee Stadium, George receives a call from Natalie, his personal ad girl from The Daily Worker. George's secretary, Ada, overhears the conversation and suspects George of having communist sympathies. At Coleman's, Ned gets Kramer interested in communist practices. At Monk's, while Duncan is protesting the race, George turns up, pretending he has not seen Jerry in years, and backs Jerry's story (while also lying about the accomplishments of his own life). Duncan is still unconvinced, and Lois suggests that the two of them just race each other again, but Jerry refuses ("I choose not to run"). Nonetheless, Duncan starts to call up everyone from high school to come out for the race, and Jerry gets worried the legend will die.

Kramer is taken by Ned's communist literature, but Mickey thinks it's a bad idea. Elaine is reluctant to order dinner from Hop Sing's after her fight with the delivery man, but Ned insists, as his father spent much of his time at the restaurant after being blacklisted. At Coleman's, Kramer (as Santa) is accused by a kid of spreading communist propaganda; Kramer and Mickey are subsequently fired. Elaine places her Chinese food order under Ned's name, but the delivery man uncovers her ruse and blacklists Ned from the restaurant, too. Jerry refuses to participate in the race, until he learns that Duncan will fire Lois unless he agrees to run. The rumor that George is a communist spreads to Steinbrenner, who is delighted: with a communist working for the Yankees, they will be able to scout Cuban baseball players for the team.

On the street, Jerry and Duncan are lined up to race again. Duncan smugly asides to Lois that if he loses, he'll give her a two-week Hawaiian vacation. As the race is about to begin, Kramer's car backfires; Jerry mistakes that for the starting pistol, giving him another head start. To the strains of the Superman theme, Jerry wins the race.

A week later in Cuba, George meets with Fidel Castro (who like Steinbrenner is seen from behind) who lets him recruit any players and invites him to a luxurious dinner due to his supposed communist leanings. However, Castro (very much like Steinbrenner) begins to ramble on about trivialities and George is forced to listen to him while quietly exiting.

## Themes

A recurring theme throughout Seinfeld is the references to Superman; the theme features prominently in The Race. When Jerry says to Lois, "Faster than a speeding bullet, Lois," it was a reference to the Superman series, Adventures of Superman starring George Reeves. He also at one time says, "Why, I'd have to be Superman to do that, Lois." At the end of the episode, Jerry breaks the fourth wall and winks to the camera after he says, "Maybe I will, Lois. Maybe I will." This was the first and only instance of breaking the fourth wall in the series, excluding the retrospective The Highlights of 100. The wink towards the camera is a reference to the older Superman television series. When Jerry says "I choose not to run" in reference to the proposed (re-)race, it is possibly a reference to Calvin Coolidge saying "I do not choose to run for president in 1928," as his reason for not running, though it may instead be a reference to Bartleby, the Scrivener. Jerry's stature and language is also reminiscent of Superman throughout the episode. In addition, Cold War paranoia is lampooned through a young boy making "commie" accusations against Kramer, calling him a traitor to "our country", while Mickey tries to keep him quiet.

## Deleted scenes

The phone conversation between George and Natalie is shortened considerably in the final cut. In addition, there is a deleted scene which shows George explaining to Jerry about how he is being sent to Cuba; and they subsequently talk about him not having a visa. As Kramer walks back into his apartment, George asks him if he still knows people down at the Cuban Embassy. Kramer says he plays golf with them, and the pair hurry down there before it closes. However, by the time they arrive, the building is closed, but Kramer claims to know of a secret passage that was built during the Cuban Missile Crisis. Though he has not used it before, Kramer takes George there. The next scene shows Kramer crashing down the chimney to the surprise of the Cubans - as Kramer is still in his Santa Claus outfit.

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Recurring

Danny Woodburn .................... Mickey Abbott

#### Guests

Todd Kimsey ................................ Ned  
Renee Props .................................. Lois  
Don R. McManus .......................... Duncan  
Vicki Lewis .................................. Ada  
Claude Earl Jones ......................... Mr. Bevilaqua  
Mark Christopher Lawrence ........ Boss  
Michael Sorich ............................. Castro  
Denise Poirier .............................. Arlene  
Spencer Klein ................................ Kid  
Martin Chow ............................... Lew  
Eva Svensson ............................... Woman

## Script

[Opening Monologue]

The Christmas tree certainly seems to inspire a love/hate relationship. All that time is spent selecting it and decorating it, and then a week after it's just thrown somewhere, you see it by the side of the road, it looks like a mob hit. A car slows down, a door opens and this tree just rolls out. People snap out of that Christmas spirit like it was a drunken stupor, they just wake up one morning and go, &quot;Oh my god, there's a tree inside the house! Just throw it anywhere!&quot;

[Lois's office]

JERRY: Ready to go Lois?

LOIS: You really like to say my name? Don't you?

JERRY: Excuse me Lois. Stand back Lois. Jimmy's in trouble Lois.

LOIS: Oh, Mr. Meyers this is my friend, Jerry.

DUNCAN: Jerry Seinfeld!

JERRY: Duncan Meyers!

LOIS: You two know each other?

DUNCAN: Yeah! We uh, went to High School together. Didn't we Jerry? Gee I hope you're not leaving now. We still have a lot of work left to do.

LOIS: Would you be able to come all the way downtown again in rush hour to pick me up?

JERRY: Well, I'd have to be Superman to do that Lois.

[Elaine's Apartment]

ELAINE: No, no This is all wrong. Where's the Chicken Cashew?

LEW: You no order Chicken Cashew.

ELAINE: I didn't order any of this. I'm not paying for this.

LEW: Fine Benes. We are putting you on our list.

ELAINE: What list?

LEW: The &quot;do not deliver&quot; list.

ELAINE: MERRY CHRISTMAS TO YOU! Well, I guess we'll just go out.

GEORGE: Yeah. What are you doing with the Daily Worker?

ELAINE: Ned must have left it here.

GEORGE: Your boyfriend reads the Daily Worker? What is he? A communist?

ELAINE: HE reads everything, you know, Ned's very well read.

GEORGE: Maybe he's just &quot;very well RED&quot;?

ELAINE: Communist? Don't you think he probably would have told me?

GEORGE: Well, does he wear bland, drab, olive colored clothing?

ELAINE: Yes, . . . yes he does dress a little drab.

GEORGE: Huh, he's a communist. . . . Look at this. &quot;Exciting uninhibited woman seeks forward thinking comrade and appearance not important.&quot; . . . Appearance Not Important! This is unbelievable. Finally this is an ideology I can embrace.

(Jerry enters)

JERRY: Hi oh.

ELAINE &amp; GEORGE: Hey.

ELAINE: Where's Lois?

JERRY: She couldn't make it.

GEORGE: I can't believe you're really going out with a woman named Lois.

JERRY: I know, finally. But George, guess who her boss is. Duncan Meyers.

GEORGE: Duncan Meyers?

ELAINE: Who's he?

JERRY: Elaine, only one other person in the world knows what I am about to tell you and that's George. When we were in the ninth grade they had us all line up at one end of the school yard for this big race to see who was going to represent the school in this track meet.

ELAINE: Uh uh

JERRY: I was the last one on the end. George was next to me. And Mr. Bevilaqua, the gym . . .

ELAINE: What's that?

JERRY: Mr. Bevilaqua, the gym teacher.

ELAINE: Oh, of course.

JERRY: He was down at the other end. So he yells out, &quot;Ready, On your mark, Get set, &quot; and I was so keyed up I just took off. By the time he said go I was ten yards ahead of everybody.

ELAINE: No.

GEORGE: I looked up. I couldn't believe it.

JERRY: By the time the race was over I had won. I was shocked nobody had noticed the head start.

ELAINE: Really?

JERRY: And I had won by so much a myth began to grow about my speed. Only Duncan suspected something was a miss. He's hated me ever since. Now he's back.

ELAINE: Well what happened when you raced him again?

JERRY: I never did. In four years of high school I would never race anyone again. Not even to the end of the block to catch a bus. And so the legend grew. Everyone wanted me to race. They begged me. The track coach called my parents. Pleading. Telling them it was a sin to waste my god given talent. But I answered him in the same way I answered everyone. I chose not to run.

ELAINE: So now Duncan is back?

JERRY: He's back. And I knew he would be someday. (drinks) Man that's some tart cider!

[In Jerry's Car - Lois gets in]

LOIS:Hi.

JERRY: Hi.

LOIS: Sorry I missed the Chinese food.

JERRY: Oh, so am I . Uh, how's Duncan?

LOIS: He's okay.

JERRY: He say anything?

LOIS: About what?

JERRY: Oh, nothing in particular.

LOIS: . . . Why did you cheat in that race?

JERRY: I did not cheat.

LOIS: He said that you got a head start.

JERRY: Oh, he's just jealous because he came in second.

LOIS: Really?

JERRY: Yes

LOIS: So you WERE the fastest kid in school.

JERRY: Faster than a speeding bullet Lois.

[Elaine's Apartment]

ELAINE: So how was work? Another day, another dollar?

NED: I guess.

ELAINE: Oh well nothing wrong with that. Gotta make those big bucks. . . . money money money money money money money . . . ha ha ha ha ah . . . are you a communist?

NED: Yes, as a matter of fact I am.

ELAINE: OH, AH! OH! WOW! WHOA! A COMMIE! Wow, gee, man it must be a bummer for you guys what with the fall of the soviet empire and everything .

NED: Yeah, well, we still got China, and Cuba,

ELAINE: Yeah, but come on . . .

NED: I know it's not the same.

ELAINE: Well, you had a good run, what was it 75, 80 years? Wreaking havoc, making everybody nervous.

NED: Yeah, we had a good run.

ELAINE: Well, so enjoy yourself. (clink glasses) ha ha uh ha

[Jerry's apartment]

GEORGE: So you lied to her?

JERRY: I couldn't tell her the truth. I don't know what's going to happen between us. What if we have a bad breakup. She'll go straight to Duncan. And I want him to go to his grave never being certain I got that

head start.

(Elaine enters)

ELAINE: Well, I'm dating a communist.

JERRY: Wow, a communist. That's something.

ELAINE: Yeah, that's pretty cool isn't it?

GEORGE: Hey, did I tell you I called one of those girls from the personal ads in The Daily Worker?

JERRY: The Daily Worker has personal ads?

GEORGE: And they say appearance is not important.

JERRY: Yours or hers?

(Kramer enters dressed as Santa)

KRAMER: Ho Ho Ho Ho Ho Merry Christmas everyone. Merry Christmas.

JERRY: Wow, look at you. So you got the job.

KRAMER: Yeah, you're looking at the new Santa at Coleman's Department store.

ELAINE: Oh, congratulations

(Mickey enters)

MICKEY: Come on get your bead on. We're going to be late.

KRAMER: On Prancer on Dasher, on Donna.

MICKEY: Not Donna, it's Donner.

KRAMER: Donna!

MICKEY: Yeah, right!. On Prancer, on Dancer, on Donna, on Ethyl, on Harriet.

JERRY: Hello, Oh hi Lois, you want to get together, what for? I don't know about that, I'll have to think about it. I'll let you know. Okay, bye.

GEORGE: What's up?

JERRY: Duncan wants to get together with her and me for lunch tomorrow. He obviously wants me to admit I got a head start. And I don't think she believes me.

GEORGE: He wants to meet you? I'll tell you what. I'll show up. He doesn't know we're friends. I'll pretend I haven't seen you since High School. I'll back up the story.

JERRY: That's not bad.

GEORGE: Not bad? It's gorgeous!

[Coleman's Department Store]

KRAMER: Ho ho ho Well come on little Princess, tell Santa what you want. Don't be shy.

MOM: She doesn't speak English (with a Swedish accent).

KRAMER: Santa speaks the language of all children. A notchie watchie dotchie do.

(Child cries and reaches for her mom)

KRAMER: A dotchie cotchie dochie,

KRAMER: Hey, Mickey when do we get a break? My lap is killing me.

MICKEY: There is no break.

KRAMER: A sweat shop.

(Elaine and commie enter)

KRAMER: Hey, hey, hey.

(Kid sits on Kramer's lap and they both slide to the floor)

[Yankee Stadium]

ADA: Natalie on line 2.

GEORGE: Natalie?

ADA: From the Daily Worker.

GEORGE: Thank you.

(ADA leaves but listens at the door)

GEORGE: Hello, it's Natalie? Yeah, this is a business office but I'm not a business man per se. I'm here working for the people. Yes, I'm causing dissent. Stirring the pot. Getting people to question the whole rotten system.

[Coleman's Department Store]

ILENE: Elaine.

ELAINE: Ilene.

ILENE: Hi.

ELAINE: Hello.

ILENE: Doing a little Christmas shopping?

ELAINE: Yeah, yeah. Oh, this is Ned. He's a communist.

ILENE: Oh, really?

ELAINE: Yep . . . a big communist, a big big communist.

ILENE: Oh, well, it's awfully nice to see you. See you later.

ELAINE: Bye bye

ELAINE: Hey, listen while we're here why don't we do a little shirt shopping?

NED: Out of the question.

ELAINE: Um. Kramer!

KRAMER: Hi

ELAINE: Hi, oh hi Mickey, this is Ned

KRAMER: Oh, hey, hi buddy.

ELAINE: You guys stay here, I'll be right back.

KRAMER: Eight hours of jingle belling and ho-ho-hoing. Boy, I am ho'd out.

NED: Anyone who works here is a sap.

(Mickey attacks Ned)

MICKEY: Watch it!

KRAMER: Woah, woah, come on.

NED: You understand the Santa's at Bloomfields are making double what you are?

KRAMER: Double?

NED: I bet the beard itches doesn't it?

KRAMER: You got that straight.

NED: So when you get a rash all over your face in January do you think Coleman's will be there with a medical plan?

MICKEY: Look, you take that commie crap out into the street.

NED: Kramer, I've got some literature in my car that will change your whole way of thinking.

KRAMER: Talk to me baby.

MICKEY: Don't listen to him Kramer, you've got a good job here.

[Monks]

DUNCAN: But there's no way you could have beaten me by that much. I already beaten you in Junior High School three times.

JERRY: I didn't hit puberty til the 9th grade. That's what gave me my speed. Besides, if I got a head start why didn't Mr. Bevilaqua stop the race?

(George enters)

DUNCAN: That's what I've always wondered about.

JERRY: Well, I . . . [sees George]

GEORGE: Oh, my God, No, oh my God, . . . Jerry!

JERRY: I'm sorry, uh,

GEORGE: George, George Costanza!

JERRY: Oh, George Costanza , Kennedy High.

GEORGE: Yes yes yes This is unbelievable.

DUNCAN: Hi, George

GEORGE: Oh, wait a minute, wait a minute, don't tell me, don't tell me. It starts with a . . . Duncan Meyers. Oh, wow, this is something. I haven't seen you guys in what, twenty years?

JERRY: This is Lois.

LOIS: Hello.

GEORGE: So what have you been doing with yourself?

JERRY: I'm I'm a comedian.

GEORGE: Ah ha, well, I really wouldn't know about that. I don't watch much TV. I like to read. So what do you do, a lot of that &quot;did you ever notice?&quot; this kind of stuff.

JERRY: Yeah, yeah

GEORGE: It strikes me a lot of guys are doing that kind of humor now.

JERRY: Yeah, yeah, Well, you really got bald there, didn't you?

GEORGE: Yeah, yeah.

JERRY: You really used to have a think full head of hair.

GEORGE: Yeah, yeah. Well, I guess I started losing it when I was about twenty-eight right around the time I made my first million. You know what they say. The first million is the hardest one.

JERRY: yeah, yeah.

LOIS: What do you do?

GEORGE: I'm an architect.

LOIS: Have you designed any buildings in New York?

GEORGE: Have you seen the new addition to the Guggenheim?

LOIS: You did that?

GEORGE: Yep. And it didn't take very long either.

JERRY: Well you've really built yourself up into something.

GEORGE: Well, well, I had a dream, Jerry.

JERRY: Well, one cannot help[ but wonder what brings you into a crummy little coffee shop like this.

GEORGE: Well, I like to stay in touch with the people.

JERRY: Ah, you know you have a hole in your sneaker there. What is that canvas?

GEORGE: You know my driver's waiting, I really should get running. Good to see you guys again.

JERRY: George, George, hang on. I haven't seen you in so long.

GEORGE: Ha, uh,

JERRY: I thought we might reminisce a little more. You know Duncan and I were just taking about the big race.

GEORGE: Oh, the big race.

JERRY: Yeah.

GEORGE: Yes, yes,.

LOIS: You were there?

GEORGE: Yes, sure, surely was. Yeah, I'll remember that day. Well I'll never forget it because that was the day that I uh, lost my virginity to Miss. Stafford, the uh, voluptuous home room teacher.

DUNCAN: Miss Stafford?

GEORGE: Yes, yes, you know I was in detention and she came up behind me while I was erasing the blackboard . . .

JERRY: George!

GEORGE: But I digress. Let me see, now. You were standing at one end of the line and I was right next to you. And I remember we were even for like, the first five yards and then , BOOM,...You were gone.

JERRY: Did I get a head start?

GEORGE: Head start, oh no absolutely not.

JERRY: You satisfied? So you see?

DUNCAN: No, I'm still not convinced and I never will be.

LOIS:Why don't the two of you just race again?

DUNCAN: That's a good idea.

JERRY: No, no, no, another race - out of the question.

DUNCAN: I know, you've been saying that for twenty years because you know you can't beat me. You couldn't beat me then and you can't beat me now.

LOIS: Race him Jerry. Race him.

JERRY: All right! I'll do it. The race is on.

[Jerry's apartment]

ELAINE: . . . shut up! (?)

JERRY: And he's calling all these people from High School to come and watch. I knew this day would come. I can't do it. I can't go through with it. I'm calling it off. I can't let the legend die. It's like a kid finding out there's no Santa Claus

[Street]

KRAMER: Each according to his ability, to each according to his needs.

MICKEY: What does that mean?

KRAMER: Well, if you've got needs and abilities that's a pretty good combination.

MICKEY: So what if I want to open up a delicatessen?

KRAMER: There are no delicatessens under Communism.

MICKEY: Why not?

KRAMER: Well, because the meats are divided into a class system. You got Pastrami and Corned Beef in one class and Salami and Bologna in another. That's not right.

MICKEY: So you can't get Corned Beef?

KRAMER: Well, you know, if you're in the Politburo, maybe.

[Jerry's Apartment]

GEORGE: (on phone) . It's George Costanza. . are there any messages for me? Why does Mr. Steinbrenner want to see me in his office? . . . Communist? I'm not a Communist . . . . All right, all right. All right, I'll be there. - ( hangs up ) My secretary Ada, told Mr Steinbrenner I'm a Communist Now he wants to see me in his office.

JERRY: So you'll just explain to him you're not a Communist. You just called the woman for a date.

(Phone rings)

JERRY: Hello, oh hi Duncan, 4:00 o'clock tomorrow? That is not going to work. . . . Why? I'll tell you why. Because I chose not to run!

[Elaine's Apartment]

NED: I'm sorry Elaine. The shirt's too fancy.

ELAINE: Just because you're a communist, does that mean you can't wear anything nice? You look like Trotsky. It's gorgeous. Fine, you want to be a Communist, be a Communist. Can't you at least look like a successful Communist?

NED: All right, I'll try it on.

ELAINE: I'm going to order Chinese Food.

NED: You're ordering from Hop Sing's, right?

ELAINE: Ugh, does it have to be Hop Sing's. I kind of had a fight with him.

NED: Elaine, when my father was black listed he couldn't work for years. He and his friends used to sit at Hop Sing's every day figuring out how to survive.

ELAINE: You're father was blacklisted?

NED: Yes he was, and you know why? Because he was betrayed by people he trusted. They &quot;named names&quot;.

ELAINE: Okay, okay. (phones) Um, yeah, hi, I'd like delivery please to 16 West 75th St. apartment 2G.

LEW: I know that address. You're Benes, right. You're on our list. No more delivery.

ELAINE: No. no, she doesn't live here anymore. This is someone else.

LEW: Oh, yeah. What's the name?

ELAINE: Why do you need the name? You already have the address.

LEW: We need a name. Give us a name.

ELAINE: Okay, okay, Ned Isakoff.

[Coleman's Department Store]

KID: I want a racing car set.

KRAMER: Ho ho ho ho A racing car set! Those are assembled in Tai Wan by kids like you. And these Coleman pigs, they sell it at triple the cost.

KID: But I want a racing car set.

KRAMER: You see kid, you're being bamboozled. These capitalist fat cats are inflating the profit margin and reducing your total number of toys.

KID: Hey, this guy's a COMMIE!

MICKEY: Hey, kid, quiet. Were did a nice little boy like you learn such a bad word like that? Huh?

KID: Commie, Commie, Commie . . . (unknown) .

MICKEY: Santa is not a Commie. He just forgot how his good friend stuck his neck out for him to get him a good job like this. Didn't he Santa!

STORE MANAGER: Is there a problem here?

KRAMER: ho ho ho ho.

KID: This guy's a Commie. He's spreading propaganda.

STORE MANAGER: Oh yeah? Well that's enough pinko! You're through. The both of ya'

MICKEY: I got two kids in college.

KRAMER: You can't fire me, I'm Santa Claus.

STORE MANAGER: Not anymore. Get your skinny ass out of here.

[Jerry's Car]

JERRY: Hi how are you?

LOIS: . . . Fine.

JERRY: What's the matter?

LOIS: I just spoke to Duncan. He said if you don't race, he's going to fire me.

JERRY: What? He can't do that.

LOIS: Yes he can. He controls the means of production. What are you going to do Jerry?

JERRY: Don't worry Lois. I'll think of something.

[Elaine's Apartment]

LEW: Ah, I knew it was you! You tried to trick Hop Sing! You are onour list; Elaine Benes! And now you are on our list; Ned Isakoff.

NED: You got me blacklisted from Hop Sing's?

LEW: She named name!

&nbsp;

[Steinbrenner's Office]

GEORGE: You, uh, wanted to see me, Mr. Steinbrenner?

STEINBRENNER: Yes George, I did. Come in, come in. George, the word around the office is that you're a Communist.

GEORGE: C-Communist? I am a Yankee, sir, first and foremost.

STEINBRENNER: You know George, it struck me today me that a Communist pipeline into the vast reservoir of Cuban baseball talent could be the greatest thing ever to happen to this organization.

GEORGE: Sir?

STEINBRENNER: You could be invaluable to this franchise. George, there's a southpaw down there nobody's been able to get a look at; something Rodriguez, I don't really know his name. You get yourself down to Havana right away.

GEORGE: Yes, sir. Yes sir, do my best.

STEINBRENNER: Good, Merry Christmas George. And bring me back some of those cigars in the cedar boxes, you know the ones with the fancy rings? I love those fancy rings. They kind of distract you while you're smoking. The red and yellow are nice. It looks good against the brown of the cigar. The Maduro, I like the Maduro wrapper. The darker the better, that's what I say. Of course, the Claro's good too. That's more of a pale brown, almost like a milky coffee. (George exits) I find the ring size very confusing. They have it in centimeters which I don't really understand that well...

[On the Street]

MICKEY: That was quick! Nice job, Santa!

KRAMER: Yeah,

MICKEY: I knew that Commie stuff was going to get us in trouble.

KRAMER: Yeah, well I didn't realize that was such a sensitive issue.

MICKEY: Communism, You didn't realize Communism was a sensitive issue? What do you think has been going on in the world for the past 60 years? Wake up and smell the coffee.

KRAMER: I guess I screwed up!

MICKEY: You sure did. Big time.

[Street - race begins]

ELAINE: How do you feel?

JERRY: I need a miracle.

DUNCAN: Now you're going to see what kind of liar you're mixed up with.

LOIS: If he beats you I want a big raise.

DUNCAN: If he beats me, I'll not only give you a raise, I'll send you to Hawaii for two weeks.

KRAMER: I parked in front of that restaurant . As soon as this race is over I got to go to the airport.

GEORGE: Okay, all right, all right.

MR. BEVILAQUA: You ready boys?

JERRY: Yes, Mr. Bevilaqua

MR. BEVILAQUA: Okay, this is how it works. You take your marks, I say, READY - ON YOUR MARK - GET SET - and then fire. You got it?

DUNCAN &amp; JERRY: Yes Mr. Bevilaqua.

(Kramer enters his car)

(Mr. Bevilaqua raises gun)

MR. BEVILAQUA: READY - ON YOUR MARK

(Kramer's car backfires)

(Jerry is off early and wins the race - the crowd goes crazy - )

LOIS:So will you come to Hawaii with me Jerry?

JERRY: Maybe I will , Lois. Maybe I will.

(Jerry winks at camera like in the Superman movie)

[Havana]

(George enters Castro's office (like Steinbrenner))

GEORGE: You wanted to see me, El Presidente?

CASTRO: Si, si. (a Spanish word I can't figure out) Come here. I understand you are very interested in one of our players, eh?

GEORGE: Si, si.

CASTRO: Ordinarily I would not grant such a request but I've heard you are, uh, how you say, Communista simpatico, eh?

GEORGE: Muy simpatico. Muy muy muy.

CASTRO: Well good, then you can have your pick.

GEORGE: Oh, oh!

CASTRO: They will play for your Yankees.

GEORGE: Oh well, gracias El Commandante, gracias. Muy muy.

CASTRO: And I would be honored if you would be my guest for dinner tonight at the Presidential palace. There will be girls there and, I hear, some pretty good food. Of course the problem with parties is you invariably have to eat standing up which I don't care for but on the other hand I don't like to balance a plate on my lap either. Once when I was at a party, I put my plate on someone's piano. I assure you, if I had not been a dictator, I would not have been able to get away with that one.

The End
