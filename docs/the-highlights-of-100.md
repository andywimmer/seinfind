---
layout: default
title: The Highlights of 100
parent: Season 6
nav_order: 14
permalink: /the-highlights-of-100
cat: ep
series_ep: 100, 101
pc: 623
season: 6
episode: 14, 15
aired: February 2, 1995
written: Peter Mehlman
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697644
wiki: https://en.wikipedia.org/wiki/The_Highlights_of_100
---

# The Highlights of 100

| Season 6 - Episode 14 & 15 | February 2, 1995          |
|:---------------------------|:--------------------------|
| Written by Peter Mehlman   | Directed by Andy Ackerman |
| Series Episode 100 & 101   | Production Code 623       |

"The Highlights of 100" is an hour-long, two-part episode of the NBC sitcom Seinfeld. This was the 14th episode for the sixth season, along with it being the 100th overall episode. It aired on February 2, 1995. It is a retrospective about the past 99 episodes. In syndication, it airs as two separate episodes of 30 minutes each.

### Part 1

* 403/404 "The Pitch/The Ticket" - Jerry and George discussing the merits of a "show about nothing"
* 311 "The Red Dot" - Kramer noticing the dot on Elaine's sweater
* 306 "The Parking Garage" - The gang trying to remember where their car is parked
* 411 "The Contest" - Kramer declaring "I'm out"
* 314 "The Pez Dispenser" - Elaine laughing at the Pez dispenser during the piano recital
* 206 "The Chinese Restaurant" - Jerry, Elaine, and George placing their reservation
* 422 "The Smelly Car" - Jerry and Elaine discovering the lingering odor in Jerry's car
* 311 "The Red Dot" - Mr. Lippman firing George from Pendant Publishing
* 209 "The Stranded" - Elaine: "Maybe the dingo ate your baby."
* 518 "The Fire" - Kramer telling the harrowing tale of his mission to rescue his girlfriend's severed toe
* 405 "The Wallet" - Morty Seinfeld yelling about his stolen wallet
* 517 "The Wife" - Uncle Leo meeting Jerry at the dry cleaners
* 519/520 "The Raincoats" - Morty and Helen confronting Jerry about making out during Schindler's List
* 504 "The Sniffing Accountant" - Frank Costanza teaching George about bra sizes
* 411 "The Contest" - George visiting Estelle in the hospital
* 418 "The Old Man" - Newman explaining why the job of a postal worker is so stressful
* 102 "Male Unbonding" - Elaine and Jerry deciding where to go out
* 305 "The Pen" - Jerry reluctantly taking Jack Klompus' astronaut pen
* 503 "The Puffy Shirt" - Kramer revealing the puffy shirt to Jerry
* 310 "The Alternate Side" - Jerry explaining the difference between taking and holding a reservation
* 403/404 "The Pitch/The Ticket" - Jerry and George pitching the "Jerry" pilot to NBC

## "Relationships"

* 318 "The Limo"
* 522 "The Hamptons"
* 302 "The Truth"
* 516 "The Stand-In"
* 422 "The Smelly Car"
* 609 "The Secretary"
* 611 "The Label Maker"
* 509 "The Masseuse"
* 317 "The Fix-Up" - Jerry setting George up with Cynthia/Elaine setting Cynthia up with George
* 303 "The Dog"/203 "The Busboy" - Kramer breaking up with his girlfriend/Elaine hurriedly packing for the guy she hates (these two segments are interweaved)
* 506 "The Lip Reader" - George claiming creation of "It's not you, it's me"
* 315/316 "The Boyfriend" - Elaine on a date with Keith Hernandez
* 201 "The Ex-Girlfriend" - Marlene breaking up with Jerry
* 421 "The Junior Mint" - Jerry trying to guess "Mulva's" name
* 408 "The Cheever Letters" - Sandra leaving Jerry's apartment after the panty remark
* 419 "The Implant" - Sidra leaving Jerry's apartment ("By the way, they're real...and they're spectacular.")
* 213 "The Deal" - Jerry and Elaine laying down "the rules"/Jerry giving George the details

*

## "Ambition"

* 315/316 "The Boyfriend" - George telling Jerry to answer the phone as "Vandelay Industries"
* 102 "Male Unbonding" - Kramer's idea for a pizza place where you make your own pie
* 513 "The Marine Biologist" - Elaine sharing the fake anecdote about Tolstoy's War and Peace
* 103 "The Stake Out" - George: "I'm an architect."
* 601 "The Chaperone" - Elaine's ill-fated job interview at Doubleday Publishing
* 513 "The Marine Biologist" - George: "You know that I always wanted to pretend I was an architect!"
* 612 "The Race" - George faking his life story about architecture
* 521 "The Opposite" - Kramer on Live with Regis and Kathie Lee promoting his coffee table book
* 315/316 "The Boyfriend" - Kramer unwittingly ruining George's "Vandelay Industries" scam

### Part 2

## "Off The Subject"

* 105 "The Stock Tip" - Jerry and George discussing Superman's sense of humor
* 504 "The Sniffing Accountant" - Kramer and Newman discussing how days of the week feel
* 419 "The Implant" - George accused of double-dipping a chip
* 406 "The Watch" - Kramer's affinity for decaf cappuccinos
* 315/316 "The Boyfriend" - Jerry and George discussing their favorite explorers
* 410 "The Virgin" - Elaine's diaphragm story
* 315/316 "The Boyfriend" - Kramer and Newman's JFK-esque Keith Hernandez story
* 403/404 "The Pitch/The Ticket" - George praising the value of characters in entertainment
* 418 "The Old Man" - Sid Fields
* 307 "The Cafe" - Babu Bhatt
* 501 "The Mango" - Joe the fruit stand manager
* 605 "The Couch" - Poppie
* 409 "The Opera" - Crazy Joe Davola
* 608 "The Soup" - Kenny Bania
* 516 "The Stand-In" - Mickey
* 304 "The Library" - Detective Bookman
* 205 "The Jacket" - Alton Benes
* 606 "The Gymnast" - Mr. Pitt

## "Self-Images"

* 208 "The Apartment" - Jerry and George arguing over which one of them is the bigger idiot
* 420 "The Handicap Spot" - Kramer asking Jerry and George if he is a "hipster doofus"
* 422 "The Smelly Car" - Elaine wondering if she's not as attractive as she thinks
* 416 "The Outing" - Jerry and George insisting that they aren't gay ("Not that there's anything wrong with that")
* 501 "The Mango" - Elaine admitting that she faked her orgasms with Jerry
* 414 "The Visa" - George: "I'm disturbed, I'm depressed, I'm inadequate - I got it all!"
* 521 "The Opposite" - Elaine worries that she's becoming George
* 521 "The Opposite" - George enacting his new policy of doing the opposite of what his instincts tell him

## "Catch Phrases"

* 310 "The Alternate Side" - "These pretzels are makin' me thirsty!"
* 420 "The Handicap Spot" - "Boy, I'm really startin' to dislike the Drake!"
* 319 "The Good Samaritan" - "You are so good-lookin'."
* 503 "The Puffy Shirt" - "She's one of those low talkers."
* 603 "The Pledge Drive" - "You know, because he's a high talker."
* 519/520 "The Raincoats" - "He's nice, bit of a close talker."
* 512 "The Stall" - "I don't have a square to spare."
* 314 "The Pez Dispenser" - "I need hand! I have no hand!"
* 519/520 "The Raincoats" - "Newman!"
* 507 "The Barber" - "Newman!"
* 602 "The Big Salad" - "Newman!"
* 611 "The Label Maker" - "Hello, Newman."
* 411 "The Contest" - "Master of my domain"/"King of the county"/"Lord of the manor"/"Queen of the castle"
* 522 "The Hamptons" - "You mean shrinkage?"
* 417 "The Shoes" - Russel Dalrymple with an upset stomach discussing the "Jerry" script with Jerry and George
* 313 "The Subway" - Kramer stumbling to get a seat on the subway
* 504 "The Sniffing Accountant" - Kramer smoking and drinking at the same time
* 409 "The Opera" - Kramer singing opera
* 515 "The Pie" - Olive scratching Kramer's itch
* 405 "The Wallet" - Kramer screaming with his hair on fire
* 502 "The Glasses" - Kramer diving after Jerry's plummeting air conditioner
* 513 "The Marine Biologist" - Kramer playing golf at the beach
* 411 "The Contest" - The instigation of the contest

## "Sensitivity"

* 518 "The Fire" - George fleeing like a coward from the fire
* 407 "The Bubble Boy" - Jerry's apathetic reaction to the plight of the bubble boy
* 309 "The Nose Job" - Kramer: "You're as pretty as any of them; you just need a nose job."
* 310 "The Alternate Side" - Elaine feeding her 60-year-old stroke-stricken boyfriend
* 202 "The Pony Remark" - Jerry's pony remark
* 407 "The Bubble Boy" - George asking Susan for recompense for the tolls as the cabin burns down
* 521 "The Opposite" - Jake Jarmel learning that Elaine stopped off for Jujyfruits
* 202 "The Pony Remark" - Jerry: "Who figures an immigrant's gonna have a pony?"
* 421 "The Junior Mint" - Kramer dropping the Junior Mint into the operating area
* 421 "The Junior Mint" - "It's a Junior Mint!"
* 312 "The Suicide" - "Drake's Coffee Cake?"
* 410 "The Virgin" - "Snapple?" "No thanks."
* 314 "The Pez Dispenser" - "Pez?"
* 521 "The Opposite" - "Box of Jujyfruits?"
* 514 "The Dinner Party" - "Clark Bar."
* 407 "The Bubble Boy" - "I love Yoo Hoo."
* 602 "The Big Salad" - "Ooh, Chunkies."
* 514 "The Dinner Party" - "Chocolate babka."
* 514 "The Dinner Party" - "It's Gore-Tex."
* 517 "The Wife" - "Cashmere?" "No, Gore-Tex."
* 421 "The Junior Mint" - "Who's gonna turn down a Junior Mint? It's chocolate, it's peppermint - it's delicious!"
* 513 "The Marine Biologist" - George recounts the incredible story of how he rescued the beached whale
* 403/404 "The Pitch/The Ticket" - Jerry finally subscribes to the idea of a "show about nothing"
* 306 "The Parking Garage" - Kramer's car refusing to start
* 206 "The Chinese Restaurant" - "Seinfeld, four!"
