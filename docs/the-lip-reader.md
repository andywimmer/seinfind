---
layout: default
title: The Lip Reader
parent: Season 5
nav_order: 6
permalink: /the-lip-reader
cat: ep
series_ep: 70
pc: 506
season: 5
episode: 6
aired: October 28, 1993
written: Carol Leifer
directed: Tom Cherones
imdb: http://www.imdb.com/title/tt0697723
wiki: https://en.wikipedia.org/wiki/The_Lip_Reader
---

# The Lip Reader

| Season 5 - Episode 6    | October 28, 1993         |
|:------------------------|:-------------------------|
| Written by Carol Leifer | Directed by Tom Cherones |
| Series Episode 70       | Production Code 506      |

"The Lip Reader" is the 70th episode of the sitcom Seinfeld. It is the sixth episode of the fifth season, and first aired on October 28, 1993. The episode brought in a 20.4 rating and 31 share.

## Plot

Jerry and George are at the U.S. Open where George mentions that he had invited his girlfriend Gwen (Linda Kash) to a party. Jerry says this is a bad idea, as he cannot socialize with others when he is with his girlfriend. George buys an ice cream sundae at the match and eats it, getting the chocolate all over his face, which is caught by the cameras and broadcast on television. Jerry becomes smitten with the lineswoman, and when he tries to get her attention it appears as if she is ignoring him. Only after she turns around, Jerry finds out that she is actually deaf.

Elaine, who is using the company car service to travel, is tired of all the chatty drivers – so to avoid talking to one she pretends to be deaf. However, her plan fails when she "hears" the radio message to the driver to pick up Tom Hanks. To make it up to the driver, who was disgusted with her feigned deafness, she gets him tickets to a rock concert.

Jerry tells Kramer about the deaf lineswoman, Laura (Marlee Matlin). Kramer claims to know sign language because a deaf cousin lived with him for a year. He then gets an idea to become a "ball man" at the tennis center and asks Jerry to find out from Laura how to get the job.

Gwen breaks up with George, saying "it's not you; it's me"; George is offended, as he considers this to be his signature break-up line. Kramer tells George about seeing him on TV with the ice cream on his face, which George believes to be the reason for Gwen breaking up with him. Jerry takes George as the third wheel on a date with Laura, and they find out that Laura can eavesdrop on people by lip reading. George then gets the idea to take Laura to a party to read his girlfriend's lips to see what she is saying about him. After the date Jerry asks if he can pick up Laura at six for the party ("How about six?"), which she misinterprets as 'sex' ("How about sex?") and leaves him in a huff.

Kramer goes for the ball boy tryouts and aces it, getting the job. Jerry tells George about the 'six/sex' misunderstanding and that he straightened it out with Laura. Newman enters and asks to 'borrow' Laura to spy on his supervisor, and, not surprisingly, Jerry refuses. Later, in the limo, they find out that the driver has gone slightly deaf from sitting near the speakers at a Metallica concert | the same concert for which Elaine had given him tickets. When he recognizes Elaine, the driver throws everyone out of his cab.

They arrive late to the party and meet Laura who has a brief and nonsensical sign language conversation with Kramer. When Gwen arrives, Laura watches her conversation with Todd and signs to Kramer who voices it out loud. The conversation is surprisingly mundane—about peas, pea soup, carrots and carrot soup (a subtle joke; "silent" extras often mouth the phrase "peas and carrots" to give the illusion of conversation). Todd then asks Gwen to assist him in the post-party cleanup, asking if she wants to "sweep" with him. Laura signs the exchange correctly; however, Kramer mistakenly voices it as Todd asking Gwen to 'sleep' with him. George becomes hysterical. He runs up to them and rants about the disloyalty. When Gwen corrects him, he starts shouting at Laura and Kramer. When Laura and Kramer start arguing and signing furiously, she accidentally hits George in the eye. He topples over a table and ruins the party.

Later, the group are seen at a tennis match where Kramer is the "ball man". Kramer rushes at the ball and accidentally knocks out Monica Seles, thus ending the great ball man experiment. After the match, Laura gets into the limo with the same chatty driver. When he starts talking to her, she explains to him, "I'm deaf." He turns back with a slightly agitated and dubious expression before the credits roll.

## Critical reception

David Sims of The A.V. Club gave the episode an A- / B+, calling it "perfectly good but just a little more forgettable than the stone cold classics season five opened with," including "some very memorable moments." Sims notes that much of the episode "makes George look like a fool."

Nick DeNitto remarked that "Laura's lip-reading isn't perfect, which leads to some funny moments of miscommunication."

Commentator Paul Arras praises the episode for its "jokes that take on stereotypes, political correctness, and other assumptions about etiquette."

> While Elaine becomes the foil, Matlin plays the character with such self-confidence and empowerment, it's easy to forget that social etiquette implies her deafness should be cause for pity. In a hilarious scene, Jerry and George out to dinner with Laura, go to great lengths to guard their mouths in natural ways, raising a glass to their face or rubbing their eyes so Laura doesn't know they are talking about her. George, of course, lacks the shame he should be expected to feel about using Laura's skill for his own selfish interests. Jerry is more hesitant, saying, 'She's not a novelty act, George, where you hire her out for weddings and bar mitzvahs.' Eventually Jerry relents to ask, but before he can, Laura blurts out, 'Sure. I'll do it.' She's turned the tables and demonstrated her own empowerment. Their attempts to disguise their conversation from her failed. She is too good a lip reader to be outwitted.

Marlee Matlin told interviewer Heather Hogan about her experiences working on Seinfeld:

> Q | I think Seinfeld's "Lip Reader" episode is iconic. Did you catch any flak for taking on that role?
>
> A | The only thing I got for taking on that role was an Emmy nomination and a lot of approving looks, from people in airports, on the freeway, in public restrooms while I was freshening up! People across the board loved the episode and why wouldn't they? It was SEINFELD for God's sake, probably one of the top one or two funniest sitcoms in history. I am so honored to have been a small part of its legacy. And I am so jazzed to be working and developing a half-hour comedy with the creator of "Lip Reader," Carol Leifer. Funny lady!

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Recurring

Wayne Knight ............... Newman

#### Guests

Christopher Darga .............. Driver  
Linda Kash .......................... Gwen  
Jerry Sroka ......................... Todd  
Marlee Matlin ...................... Laura  
Veralyn Jones ...................... Renee  
Dylan Haggerty .................. Young Man  
Bret Anthony ..................... Teen

## Script

[Opening Monologue]

JERRY: Professional tennis. To me I don't understand all the shushing. Why are they always shushing. Shh, shh. Don't the players know that we're there? Should we duck down behind the seats so they don't see us watching them? To me tennis is basically just ping-pong and the players are standing on the table. That's all it is. And that goofy scoring, you win one point and all the sudden you're up by 15. Two points, 30-love. 30-love. Sounds like an English call girl. &quot;That'll be 30, love... And could you be a little quieter next time, please, shh.&quot;

[In the bleachers at the tennis court. Two players are hitting the ball back and forth.]

JERRY: Are these seats unbelievable or what?

GEORGE: Where's the sunblock?

JERRY: Here.

GEORGE: 25? You don't have anything higher?

JERRY: What, are you on Mercury?

GEORGE: I need higher. This has paba in it, I need paba-free.

JERRY: You got a problem with paba?

GEORGE: Yes, I have a problem with paba.

JERRY: You don't even know what paba is.

GEORGE: I know enough to stay away from it.

ANNOUNCER: 30-Love

GEORGE: So are you going to Todd's party this weekend?

JERRY: I'll go if someone else drives. You going?

GEORGE: Gwen really wants to go.

JERRY: You're bringing a date to a party?

GEORGE: No good?

JERRY: A party is a bad date situation. It doesn't matter who you're with. You could be with J. Edgar Hoover. You don't want to sit and talk with Hoover all night. You want to circulate. (Makes hand motions) Ho, ho, ho.

GEORGE: Why'd you pick Hoover? Was he that interesting to talk to?

JERRY: Well I would think, with the law enforcement and the cross dressing. Seems like an interesting guy.

GEORGE: Yeah I guess. What can I do? I gotta take her with me. Todd introduced us, I'm obligated.

JERRY: That woman is absolutely stunning.

GEORGE: Who, the Croat? [the tennis player]

JERRY: Not the Croat, the lineswoman. That is the most beautiful lineswoman I've ever seen.

GEORGE: Yeah, she's a B.L.

JERRY: B.L.?

GEORGE: Beautiful lineswoman. Alright listen uh I'm going to go to the concession stand and get some real sunblock. You want anything? Jerry? (Jerry is staring at the lineswoman) Jerry?

[At Elaine's office.]

COWORKER: You know, I just heard the Lexington line is out.

ELAINE: (annoyed) Uh, you are kidding me. How am I supposed to get to this meeting?

COWORKER: Take a car service. We have an account.

ELAINE: Oh forget it, I hate those. Everytime I take one, the driver will *not* stop talking to me. No matter how disinterested I seem he just keeps yakking away. Blah, blah, blah, blah, blah. Why does everything always have to have a social component? Now a stage coach, that would have been a good situation for me. Cause I'm in the coach, and the driver is way up there on the stage.

COWORKER: Well you're not going to get a cab now. Four thirty in the afternoon? Read a magazine, keep your head down.

ELAINE: Yea, I guess that could work.

[At the tennis court concession stand. George is eating a sundae. ]

[At Jerry's apartment. Kramer is watching TV.]

ANNOUNCER: And that is it. The match to Ms. Natalia Valdoni. Coming up next, mens single, but for now let's stop a minute and take a look at our beautiful tennis center backdrop.

KRAMER: Hey, hey, it's George.

(George is on TV eating a sundae. His face is covered in ice cream and fudge. He is taking a bite of ice cream and dripping it around his mouth.)

ANNOUNCER: Holy cow it's a scorcher. Boy I bet you that guy can cover a lot of court.

ANNOUNCER #2: Hey buddy, they got a new invention. It's called a napkin. We'll take a station break and continue with more action...

[Elaine in the Service Car]

DRIVER: Dag Gavershole Plaza huh? (Elaine ignores him) Pendant Publishing, that's books right? (Elaine is annoyed and still ignoring him) Miss?

ELAINE: Pardon me?

DRIVER: Books, that's what you do?

ELAINE: Yeah.

DRIVER: Yeah, I don't read much myself, (Elaine is annoyed) well you know besides the paper. Yeah a lot of people read to relax, not me. You know what I do?

ELAINE: You know I-I'm having a lot of trouble, um, hearing you back here. So...

DRIVER: (Yelling) I said you know what I do (Elaine is very annoyed) when I want to relax? The Jumble. Hey uh do you make a book of Jumbles?

ELAINE: I'm going to have to be honest with you. I'm going deaf.

DRIVER: Going deaf?

ELAINE: What?

DRIVER: Oh I-I-I'm sorry.

ELAINE: It can be very frustrating.

DRIVER: Hey what about a hearing aid?

ELAINE: Am I fearing AIDS? Oh, yeah sure, who isn't. But you know you gotta live your life.

DRIVER: No, no I said. Ehhh, forget it. (Elaine looks pleased)

[At the tennis court.]

(George returns to his seat next to Jerry)

JERRY: I can't take my eyes off that lineswoman. The woman is absolutely mesmerizing.

GEORGE: Boy you are really smitten.

JERRY: I gotta talk to her. What do you think?

GEORGE: Cold? How are you going to do that? You're not one of those guys.

JERRY: I'm going to psyche myself into it like those people that just walk across the hot coals.

GEORGE: They're not mocked and humiliated when they get to the other side.

JERRY: I have to. I won't be able to live with myself.

GEORGE: Wait a minute Jerry, there's a bigger issue here. If you go through that wall and become one of those guys I'll be left here on this side. Take me with you.

JERRY: I can't.

GEORGE: What are you going to say?

JERRY: I don't know, &quot;Hi&quot;.

GEORGE: (laughing) You think you're going to the other side with &quot;Hi&quot;? You're not going to make it.

[Elaine in the car service car.]

RADIO: Base to 92 come in

DRIVER: Yes this is 92

RADIO: After this go back to city for a 6:00 pickup

DRIVER: Righteo

RADIO: 794 Bleeker the party's Hanks. Tom Hanks.

ELAINE: Tom Hanks? After me you're picking up Tom Hanks? I love him.

DRIVER: So I guess your hearing goes in and out huh?

ELAINE: (realizing he caught her) Yeah. Yes it does...

DRIVER: Yeah. You know what I think? I think you made that whole thing up.

ELAINE: No no, no no.

DRIVER: Yeah yeah, I know your type. You're too good to make conversation with someone like me. Oh god forbid you could discuss the Jumbles. But to go so far as to pretend you're almost deaf, I mean that is truly disgusting. And Mr. Tom Hanks, may I say he too would be disgusted by your behavior.

[At the tennis court. Jerry is standing behind the lineswoman]

JERRY: Excuse me. (Woman ignores him) Excuse me? (Still ignores him) Oh that's nice. That's right ignore me. That's real polite. Yea nobody's even talking to you. All you big lineswoman. Oh you've got some kind of a cool job. I know your type thinking your too good for everyone, but it's women like you (woman turns around and notices him) oh well, what are you deaf?

LAURA: Bingo.

[At Jerry's apartment.]

KRAMER: And you're saying she's deaf.

JERRY: I'm not *saying* she's deaf, she's deaf.

KRAMER: Can't hear a thing.

JERRY: Can't hear a thing.

KRAMER: And you're going to go out with her.

JERRY: Yeah, isn't that something?

(Elaine enters.)

ELAINE: Hey.

JERRY &amp; KRAMER: Hey.

KRAMER: Hey I know how to sign.

JERRY: Really?

KRAMER: Yeah when I was 8, I had a deaf cousin who lived with us for about a year. (signing as he speaks) So I haven't been able to do it in a while.

ELAINE: What is this about?

JERRY: I met this deaf lineswoman at the tennis match.

ELAINE: You are kidding. That is amazing. (She pushes Jerry, Jerry falls back into Kramer.) I just took a car service from work and to get the driver to not talk to me, I pretended I was going deaf.

JERRY: Wow good plan.

ELAINE: Oh didn't work. He caught me hearing. Alright it's terrible, but I'm not a terrible person.

JERRY &amp; KRAMER: No.

ELAINE: No. When I shoo squirrels away, I always say &quot;get out of here&quot;. I never ever throw things at them and try to injure them like other people.

JERRY: That's nice.

ELAINE: Yeah, and when I see freaks in the street I never, ever stare at them. Yet, I'm careful not to look away, see, because I want to make the freaks feel comfortable.

JERRY: (turning to Kramer) That's nice for the freaks.

(Kramer gives Elaine a thumbs up)

ELAINE: Yeah, and I don't poof up my hair when I got to a movie so people behind me can see. I've got to make it up to this guy or I won't be able to live with myself. What can I do?

JERRY: Why don't you get him some tickets or something, how about that friend of yours that works at the ticket agency.

KRAMER: Yeah yeah Pete, he can get you great tickets to something.

ELAINE: Really?

KRAMER: Like a rock concert. Whatever you like.

ELAINE: Oh great, thanks Kramer.

KRAMER: You got it. Hey Jerry, do me a favor. The next time you see that lineswoman ask her how those ball boys get those jobs. I would love to be able to do that.

JERRY: Kramer, I think perhaps you've overlooked one of the key aspects of this activity. It's ball *boys*, not ball men. There are no ball men.

ELAINE: Yeah I think he's right. I've never seen a ball man.

KRAMER: Well there ought to be ball men.

JERRY: All right I'll talk to her. If you want to be a ball man go ahead, break the ball barrier. (Elaine drinks straight out of the orange juice container) Hey.

ELAINE: Hey you know a friend of mine from work said that she saw George at the tennis match on TV yesterday.

KRAMER: Yeah, yeah me too. Yeah he was at the snack bar eating a hot fudge sundae. He had it all over his face. He was wearing that chocolate on his face like a beard and they got in there real nice and tight. And he's... (Imitates scooping up ice cream. Elaine and Jerry laugh)

[At Monk's.]

GWEN: I'm sorry George.

GEORGE: I don't understand things were going so great. What happened? Something must have happened.

GWEN: It's not you, it's me.

GEORGE: You're giving me the &quot;it's not you, it's me&quot; routine? I invented &quot;it's not you, it's me&quot;. Nobody tells me it's them not me, if it's anybody it's me.

GWEN: All right, George, it's you.

GEORGE: You're *damn* right it's me.

GWEN: I was just trying to...

GEORGE: I know what you were trying to do. Nobody does it better than me.

GWEN: I'm sure you do it very well.

GEORGE: Yes well unfortunately you'll never get the chance to find out.

(George then looks confused like what he said just didn't sound right.)

[At Jerry's apartment.]

JERRY: But I thought things were going great.

GEORGE: Yeah so did I.

JERRY: Did she say why?

GEORGE: No. She tried to give me the &quot;it's not you, it's me&quot; routine.

JERRY: But that's your routine.

GEORGE: Yeah. Well apparently word's out.

(Kramer enters.)

KRAMER: Hey, Georgie, I saw you on TV yesterday.

GEORGE: Really? At the tennis match?

KRAMER: Yeah you were at the snack bar eating a hot fudge sundae.

GEORGE: Get out of here. I didn't see any cameras there.

KRAMER: Oh, the cameras was, vrooom, there. The announcers, they made a couple of cracks about you.

GEORGE: Cracks? What were they saying?

KRAMER: That you had ice cream all over your face. They were talking about how funny you looked.

GEORGE: Oh my god, maybe Gwen saw it. Maybe that's what did it.

KRAMER: Well I'll tell you it wasn't a pretty sight.

GEORGE: She must have seen me eating it on TV.

JERRY: So she sees you with hot fudge on your face and she ends it? You really think she would be that superficial?

GEORGE: Why not. I would be.

(The phone rings.)

JERRY: Hello... Oh hi dad... You saw him?... Really with the ice cream?... All right I'll talk to you later, bye.

GEORGE: You're parents saw me on TV?

JERRY: Yeah.

GEORGE: This is nightmare. Kramer how long was I on?

KRAMER: It felt like 8 seconds.

GEORGE: One-one-thousand, two-one-thousand, three-one-thousand.

(Elaine enters.)

ELAINE: I heard you *really* inhaled that thing. Did anyone tape it?

GEORGE: Can we move on?

JERRY: He thinks Gwen broke up with him because she saw him eating the ice cream on TV.

ELAINE: Oh come on. If she's that superficial you don't want her.

GEORGE: Yes I do.

ELAINE: So I guess you're not going to Todd's party on Friday.

GEORGE: Well I can't now, Gwen's going to be there.

KRAMER: Well she should be the one that shouldn't go.

JERRY: Well if a couple breaks up and have plans to go to a neutral place, who withdraws? What's the etiquette?

KRAMER: Excellent question.

JERRY: I think she should withdraw. She's the breaker, he's the breakee. He needs to get on with his life.

ELAINE: I beg to differ.

JERRY: Really.

ELAINE: He's the *loser*. She's the victor. To the victor belong the spoils.

JERRY: Well I don't care, I don't want to go anyway. I don't want to fight that traffic on Friday night.

ELAINE: Well we can take the car service from my office.

JERRY: Really?

ELAINE: Yeah, they don't know.

KRAMER: All right, I'll see you later.

JERRY: Okay.

(Elaine heads to the bathroom)

KRAMER: (while eating a banana) Hey Georgie.

(Kramer exits)

GEORGE: &quot;To the victor goes the spoils.&quot; What are you going to do tonight?

JERRY: Oh I got a date with Laura the lineswoman.

GEORGE: Oh. (he stands there)

JERRY: Why? (George fiddles with the lock on the door.) Well what are you doing?

GEORGE: Well I was just going to wander the streets. Don't wanna tag along with you or anything.

JERRY: Oh, uh, do you want to come with us?

GEORGE: Jerry please, that's very nice, but, uh, (closes the door) where would we be going?

[At a Chinese restaurant.]

GEORGE: So, I've got ice cream all over my face. There were no napkins there. Whoever it was that's responsible for stocking that concession stand cost me a relationship.

LAURA: They never have napkins there.

JERRY: Let's get the check. (Waves in the air) Is this uh considered signing? Do you do this when you want the check?

LAURA: (does the same thing Jerry is doing) Yea.

JERRY: Really. I know a sign, that's my first sign.

LAURA: Uh, oh. That couple is breaking up.

GEORGE: They're breaking up? How do you know?

JERRY: She reads lips.

GEORGE: What are they saying now?

LAURA: &quot;It's not you, it's me.&quot;

GEORGE: (Holding his drink up to his mouth) Oh my gosh, I just had a great idea. She could come to the party tomorrow and read Gwen's lips for me.

JERRY: (Puts his hand over his mouth) What?

GEORGE: (Puts nuts into his mouth, and in the process covers his mouth) We bring her to the party, and she can tell me what Gwen is saying about me.

JERRY: (Holds his drink up to his mouth) She's not a novelty act, George. Where you hire her out for weddings and bar mitzvahs.

GEORGE: (Puts his hands on his face, rubbing his eyes) Look. It's a skill, just like juggling. She probably enjoys showing it off.

JERRY: (Puts his napkin over his mouth) I don't know George. I'm not sure about this.

GEORGE: (Puts his arms in the air, stretching, and covers his mouth with an arm) Could you ask her, just ask her. If she says no, case closed.

JERRY: (Puts his hand on his chin over his mouth) All right.

JERRY: Uh Laura, George was wondering if...

LAURA: Sure. I'll do it.

[After the restaurant. Jerry and Laura in Jerry's car outside Laura's place.]

JERRY: So I really had a good time.

LAURA: Yeah, me too.

JERRY: So you want to go to the party on Friday night?

LAURA: Yeah.

JERRY: All right, we're taking a car service. So we'll swing by and pick you up. How about six? (Laura looks offended). Six is good. (Laura looks offended and angry). You got a problem with six? (Laura opens the door and gets out). What? What?

[At the tennis court.]

MAN: Okay listen up people. There are plenty of you here, but we've only got two spots to fill. Good luck.

BOY: (to Kramer) Hey pops, isn't there a better way to spend your twilight years?

KRAMER: I may be old, but I'm spry.

BOY: The tryout lasts three and a half to four hours. Are you up for it?

KRAMER: Oh I'll be up for it punk.

(Kramer fetches some balls, and he is doing some pretty fancy footwork.)

[At Jerry's apartment.]

JERRY: See I was saying &quot;six&quot; but she thought I was saying &quot;sex&quot;. We straightened the whole thing out though.

GEORGE: She confused &quot;six&quot; with &quot;sex&quot;?

JERRY: Yeah.

GEORGE: Well if she can't tell &quot;six&quot; from &quot;sex&quot; then how is she going to lip read from across the room?

JERRY: Well &quot;six&quot; and &quot;sex&quot; are close.

GEORGE: It's two completely different sounds. &quot;ih&quot; and &quot;eh&quot;.

JERRY: Eh.

GEORGE: It seems like a problem.

JERRY: Well I'm not dating any other deaf women.

(Kramer enters.)

KRAMER: Hey guess who's going to be the new ball man for the finals.

JERRY: You're kidding.

KRAMER: Yeah. They said they haven't seen anybody go after balls with such gusto.

GEORGE: Oh, when is that car service coming?

JERRY: In five minutes. He's then going to pick us up, then we're going to pick up Elaine, and Laura is going to meet us there.

GEORGE: If this lip reading thing works tonight do you know how incredible this is going to be? It's like having Superman for your friend.

JERRY: I know. It's like X-ray vision.

GEORGE: If we could just harness this power and use it for our own personal gain, there'd be no stopping us.

(Newman enters.)

NEWMAN: Hey, hey, hey. (to Jerry) I hear you've got some lip reader working for you. You gotta let me use her for one day. Just one day.

JERRY: Can't do it Newman.

NEWMAN: But Jerry, we've got this new supervisor down at the post office. He's working behind this glass. I know they're talking about me. They're going to transfer me, I know it. Two hours, give me two hours.

JERRY: It's not going to happen.

NEWMAN: (Sinister) All right, all right. All right you go ahead. You go ahead and keep it secret. But you remember this. When you control the mail, you control... information.

[In the car service car.]

JERRY: Uh just pull over right there by the stop sign.

DRIVER: (The same driver as before) Pardon me sir?

JERRY: I said pull over by the stop sign.

DRIVER: I'm so sorry, you'll have to forgive me. I can't hear a damn thing. I went to that rock concert last night at the garden. My seats were right up against the speaker. It's a heavy metal group. Metalli-something.

KRAMER: -ca.

DRIVER: Huh?

GEORGE: What?

JERRY: ca.

GEORGE: ah.

DRIVER: My ears are still ringing. Some woman's idea of a joke.

(Elaine gets in. The driver looks up and notices her. He gets angry. A moment later, the group gets out of the car.)

DRIVER: Get out. Get out. Go on. Hey. Shut the door.

(Kramer shuts the door and the car takes off, squealing the tires.)

[At the party.]

JERRY: You know the whole idea of taking the car service was so I wouldn't have to fight the traffic on Friday night.

(Laura sees the group and gets up. She points to her watch.)

JERRY: I know. I'm late. Hey now I know two signs, (puts his hand in the air) check, and (points to his watch) late. Hey this is the guy you helped become the first ball man.

LAURA: Congratulations.

(Kramer and Laura start signing to each other. We see confusion from them as they are signing. George puts his forehead on his hand.)

KRAMER: She doesn't know what she's talking about.

TODD: Guys you made it.

GEORGE: Hey hey.

TODD: (to George) Hey buddy.

JERRY: Hey Todd.

TODD: Sorry to hear about Gwen.

GEORGE: Why? Did she say something to you about why she broke up with me?

TODD: Oh no. Tonight will be the first chance I've had to talk to her.

GEORGE: Really?

TODD: Look George, I'm friends with both of you. But I can't betray her confidence by telling you anything.

GEORGE: I wouldn't hear of it, Todd. It's none of my business. But you should try to find out everything you possibly can. In fact, I'll even stay all the way on the other side of the room just so there's no chance of me overhearing anything.

TODD: You are so centered.

GEORGE: Hey, grown-up. (they both chuckle, George notices Gwen enter the room) Oh my god, there she is. Go ahead, go ahead. (to the others) Let's go, let's go. All right what are they saying?

(Gwen and Todd talk. Laura makes hand signs and Kramer translates.)

KRAMER: &quot;Hi Gwen, hi tide.&quot;

JERRY: Hi tide?

KRAMER: Hi Todd.

KRAMER: &quot;You've got something between your teeth&quot;

GEORGE: Where?

KRAMER: No that's what he said. &quot;That's interesting. I love carrots, but I hate carrot soup. And I hate peas, but I love pea soup.&quot; So do I.

ELAINE: She's so wild. Can I borrow her for a few hours tomorrow afternoon?

JERRY: No. If I lend her to you I'll have to lend her to everybody.

GWEN: I don't envy you Todd. The place is going to be a mess.

TODD: Well maybe you can stick around after everybody leaves and we can sweep together.

KRAMER: &quot;Why don't you stick around and we can sleep together.&quot;

GEORGE: What?

KRAMER: &quot;You want me to sleep with you?&quot;

TODD: I don't want to sweep alone.

KRAMER: He says &quot;I don't want to sleep alone.&quot; She says, oh boy, &quot;love to.&quot;

GEORGE: Alright that's it. (George walks across the room over to them.) So you get rid of me and now the two of you are going to sleep together?

GWEN: What? You're crazy.

KRAMER: &quot;What? You're crazy.&quot;

GEORGE: I heard your whole conversation.

GWEN: How?

KRAMER: &quot;How?&quot;

GEORGE: (looks back to the group) I can read lips. You said let's sleep together.

GWEN: No I didn't. I said &quot;sweep&quot;. Let's sweep together, you know with a broom. Cleaning up.

KRAMER: &quot;... with a broom, cleaning up.&quot;

GEORGE: Sweep?

GWEN: Yes sweep.

KRAMER: &quot;Yes sweep.&quot;

GEORGE: Cut it.

KRAMER: George says &quot;Cut it.&quot;

GEORGE: Cut it.

KRAMER: George is saying &quot;Cut it.&quot;

GEORGE: Cut it. (goes back to the group) (Yelling) Would you stop signing?

KRAMER: What?

GEORGE: She said &quot;sweep together&quot; you idiots, not &quot;sleep together.&quot;

(Laura's mouth is wide open. She looks at Kramer and points to George. She is mad at Kramer. Apparently she did say sweep and Kramer mis-translated. She signs to Kramer.)

KRAMER: I know how to sign.

(Kramer and Laura are arguing back and forth with sign language. They are gesturing signs fiercely. One of Laura's signs causes her hand to swing backwards and hit George in the face.)

GEORGE: Ow. My eye, my eye.

[At the tennis court.]

ELAINE: It's so amazing getting to see Monica Seles playing in the finals.

JERRY: I know and on the first tournament of her comeback.

(Kramer is sitting poised on the sideline. He waves back to the group. George and Elaine gives him a thumbs up. The two players hit the ball back and forth. The ball lands in the net. Kramer springs into action running toward the ball and runs into Monica Sellas. Monica falls to the ground in pain.)

JERRY: Thus ends the great ball man experiment.

[Laura gets into the car service car.]

DRIVER: (The same driver as before) You with the tennis center?

LAURA: Yep.

DRIVER: Hey how about that ball man injuring Monica Seles. Wasn't that something.

LAURA: I'm deaf.

DRIVER: Oh. (Very suspicious look on his face.)

[Closing Monologue]

JERRY: I've always been a big fan of the little check move. You know (does the motion for the check) Check, Check. Unless the waiter isn't too shape then you gotta total it up. Sometimes they come over, &quot;Do you want the check?&quot; No I wanna be pen pals, can't you see what I'm doing here? I'm trying to be cool and impress people.

The End
