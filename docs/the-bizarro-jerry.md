---
layout: default
title: The Bizarro Jerry
parent: Season 8
nav_order: 3
permalink: /the-bizarro-jerry
cat: ep
series_ep: 137
pc: 803
season: 8
episode: 3
aired: October 3, 1996
written: David Mandel
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697657/
wiki: https://en.wikipedia.org/wiki/The_Bizarro_Jerry
---

# The Bizarro Jerry

| Season 8 - Episode 3    | October 3, 1996           |
|:------------------------|:--------------------------|
| Written by David Mandel | Directed by Andy Ackerman |
| Series Episode 137      | Production Code 803       |

"The Bizarro Jerry" is the 137th episode of the American television sitcom Seinfeld. This was the third episode for the eighth season. It was originally broadcast on the NBC network on October 3, 1996. The title and plot extensively reference the Bizarro (the polar opposite of Superman) and Bizarro-Earth concepts that originally appeared in various comic books published by DC Comics. This episode is famous for introducing the phrase "man-hands".

David Mandel wrote the episode in response to his then girlfriend Rebecca ending their long-distance relationship. Rebecca, now wife of Mandel, was self-conscious about what she calls her "farm-hands". According to Mandel, writing a Seinfeld episode (and this one in particular) "it’s the modern equivalent of a Shakespeare sonnet”.

## Plot

Elaine breaks up with her boyfriend Kevin (from "The Soul Mate"), but they decide to "just be friends." Much to Elaine's surprise, Kevin is thrilled at the idea, and starts becoming a much more reliable friend than Jerry. Jerry suggests to Elaine that Kevin is "Bizarro Jerry", and explains the comic book concept of Bizarro World. Meanwhile, Kramer accidentally gets a job at a company called Brandt/Leland when he aids an employee in the hall and starts going to meetings. He soon finds out he fits right in and starts working there for no pay, stating his reason as doing it "just for me." When Jerry asks Kramer what he does, Kramer responds, "TCB - you know, taking care of business!" A montage of his office work includes scenes of Kramer eating crackers at lunch and shining his shoes at the water cooler.

Jerry starts dating Gillian (Kristin Bauer), an attractive woman whose only flaw is that she has "man-hands.", i.e. her hands are large and coarse like a man's. George uses a picture of Gillian to get into the "forbidden city", a club of attractive women and models, by saying that Gillian is his late fiancee Susan. Unfortunately, his luck ends when he accidentally burns the picture with a hair dryer. Jerry becomes bored at home, now that Kramer is "working", Elaine is always hanging out with Kevin and his friends Gene and Feldman (Bizarro versions of George and Kramer, respectively), and George only comes to him when he wants something.

By the end of the episode, Kramer gets fired by Leland (despite the fact that Kramer doesn't really work at Leland) because of his shoddy work ("It's almost as if you have no business training at all"). Jerry wants to be "just friends" with Gillian, who does not take too well to the idea. While trying to get another picture of her from her purse for George, she grabs Jerry's wrist (which Jerry later describes as almost ripping his arm right out of the socket). George tries to use a picture of a model from a magazine to get back into the club, but his plan is foiled when he accidentally confronts exactly the same model from the magazine picture and gets kicked out. Jerry, George, and Kramer go to meet Elaine at the same time that Kevin, Gene, and Feldman were to meet up with her - both groups of men react rather awkwardly at seeing their Bizarro-counterparts. Elaine decides to stay with her "Bizarro friends", but is explicitly asked to leave by them when they do not take to some of the normal things she usually does with Jerry, such as eating olives directly out of the jar from Kevin's refrigerator and pushing Kevin, with her trademark outburst of "get out!", so hard that he falls and is hurt.

Later, George takes Jerry to the location of the club, but all they find is a meat packing plant. George is dismayed, while Jerry doesn't believe there ever was a club there. As they leave, they miss seeing the photo George had taken from a magazine, lying amidst the sawdust on the ground.

## Cultural references

When George starts dating models, he makes a reference to Jerry by saying "Flame on!". This is a reference to The Fantastic Four's team member, the Human Torch.

### Bizarro counterparts

* Elaine describes Kevin as Jerry's opposite since Kevin is reliable and kind, contrasted to Jerry's forgetfulness and indifference.

* Gene is shown to be quiet, courteous, charitable and well-dressed as opposed to George being loud, obnoxious, cheap and slobbish.

* Feldman acts generously to his friends, regularly buying them lunch and bringing Kevin groceries. He also always knocks on Kevin's door and waits for him to unlock it. This is opposite to Kramer, who constantly takes Jerry's groceries and bursts through his door without warning. As opposed to Kramer's zany schemes which often are seen through to the end, Feldman has good ideas which he rejects as silly.

* Vargus, a FedEx worker, is good friends with Kevin, opposed to Newman, a postal worker and mutual enemies with Jerry.

* Elaine hangs out with these counterpart friends at Reggie's, which Jerry describes as the "Bizarro Coffee Shop" (contrast to Monk's, where they normally go to). Reggie's had previous visits by George in "The Soup" and "The Pool Guy". Jerry and Elaine also went to Reggie's (in "The Soup"), but were dissatisfied with the menu and service.

* Kevin's apartment, where he and his friends spend time reading, is a mirror image to Jerry's. Also seen in the background of Kevin's apartment is a unicycle, which also is a reflection on Jerry's bicycle hanging in his apartment, and a PC - the opposite of Jerry's Mac. Kevin also has a statue of Bizarro as opposed to Jerry's statue of Superman. The locks on Kevin's apartment door are on the opposite side and, in this case, are actually used, in sharp contrast to the locks on Jerry's apartment door.

* The signature Seinfeld theme song is played backwards in the tag scene of the episode.

The episode also features a quintessential conversation in the Seinfeld repertoire when Elaine compares Kevin (a.k.a. Bizarro Jerry) to Jerry:

> ELAINE: ...They read.
>
> JERRY: I read.
>
> ELAINE: Books, Jerry.
>
> JERRY: Oh... big deal!

### Superman reference

The concept of a Bizarro universe is directly taken from the Superman universe, in addition to verbal references to Superman:

> JERRY: Yeah, like Bizarro Superman—Superman's exact opposite, who lives in the backwards Bizarro world. Up is down; down is up. He says "Hello" when he leaves, "Goodbye" when he arrives.
>
> ELAINE: Shouldn't he say "bad bye"? Isn't that the opposite of goodbye?
>
> JERRY: No, it's still goodbye.
>
> ELAINE: Does he live underwater?
>
> JERRY: No.
>
> ELAINE: Is he black?
>
> JERRY: Look, just forget the whole thing. Alright?

At the very end of the show, a scene takes place in Kevin's apartment (which has a Bizarro statue by the door, similar to how Jerry's apartment has a Superman statue by the door) in which Kevin, Gene, and Feldman all join in a group hug, and the following line is spoken in the same way that the Bizarro from the Superman universe speaks:

> KEVIN: Oh... me so happy. Me want to cry.

## Critical reception

In a review of two adjacent episodes, David Sims of The A.V. Club writes, "The Bizarro Jerry and The Little Kicks are probably two of the better-known season 8 episodes and for good reason – they're a lot of fun." Sims speculates that "The Bizarro Jerry just reeks of a concept that Seinfeld wanted to do forever, given his obsession with Superman, and finally got the chance to once he was fully in charge of the show... Elaine finds that Kevin...and his friends are like a weird mirror group to her friends. But it's very effectively staged that it works, even once the joke has become totally familiar – the idea of characters having strange doubles is now one of the oldest sitcom tropes in the book... Elaine, of course, quickly realizes that the bizarro universe is not for her... the whole time she's more of an interested party than anything, examining the bizarro gang like a scientist." Of the "man-hands" segments, Sims says, "Like many a good Seinfeld episode, there's a B-plot nestled in here that feels like the dominant A-plot of another episode, considering what a major meme it became."

John J. O'Connor of The New York Times also explained why he found the episode fun: "Bizarro Jerry has found Elaine (Julia Louis-Dreyfus) entering a world of virtual reality with a new boyfriend who eerily resembled Jerry except that he was reliable and considerate. Moreover, his friends were physical clones of George and Kramer (Michael Richards). 'It's like Superman's opposite,' observed Jerry, pinpointing the bizarro of the title. Meanwhile, Jerry was dating a beautiful young woman whose only flaw (flaws are inevitable on Seinfeld) was having man's hands: meaty paws, whined Jerry, 'like a creature out of Greek mythology.' Kramer drifted incomprehensibly into a corporate job in which he 'finally found structure' and was able to strut about with a briefcase full of Ritz crackers."

## Trivia

The hands of actor James Rekart are the "famous Man-Hands" in the episode of "The Bizarro Jerry". James Rekart was personally cast by Jerry Seinfeld. Seinfeld remembered Rekart from acting classes that they both attended.

## Cast

#### Regulars

Jerry Seinfeld ........................ Jerry Seinfeld  
Jason Alexander .................... George Costanza  
Julia Louis-Dreyfus ................ Elaine Benes  
Michael Richards .................... Cosmo Kramer

#### Guests

Tim DeKay .............................. Kevin  
Kristin Bauer ........................... Gillian  
Pat Kilbane ............................. Bizarro Kramer (Feldman)  
Kyle T. Hefner ....................... Bizarro George (Gene)  
Justina Vail ............................. Amanda  
J. Patrick McCormack ............. Leland  
Harry Murphy ....................... Office Manager  
Dana Patrick .......................... Model #1  
Shireen Crutchfield ................ Model #2  
Robin Nance .......................... Model #3  
Mark S. Larson ...................... Bizarro Newman (Vargas)  
James Lesure ......................... Office Worker  
Jason Beck ............................. Bouncer

## Script

[JERRY and GEORGE are eating at an outdoor table, possibly in a zoo.]

JERRY: All right. How 'bout this one: let's say you're abducted by aliens.

GEORGE: Fine.

JERRY: They haul you aboard the mother ship, take you back to their planet as a curiosity. Now: would you rather be in their zoo, or their circus?

GEORGE: I gotta go zoo. I feel like I could set more of my own schedule.

JERRY: But in the circus you get to ride around in the train, see the whole planet!

GEORGE: I'm wearin' a little hat, I'm jumpin' through fire.. They're puttin' their little alien heads in my mouth..

JERRY: (resigned) At least it's show business..

GEORGE: But in the zoo, you know, they might, put a woman in there with me to uh.. you know, get me to mate.

JERRY: What if she's got no interest in you?

GEORGE: W--then I'm pretty much where I am now. At least I got to take a ride on a spaceship.

[Exterior long shot: looking up at an office building; Interior of office reception area of &quot;Brandt/Leland&quot; as George, Jerry, and Kramer come in. They calmly keep it quiet.]

KRAMER: George, why couldn't I use the bathroom in that store?

GEORGE: Kramer, trust me, this is the best bathroom in midtown!

KRAMER: (frustrated) Wha??

JERRY: (dry) He knows.

GEORGE: (Anyway,) on the left--exquisite marble! High ceilings. An' a flush, like a jet engine! (imitates sound) Ha ha!

KRAMER: (impressed) Now, listen, uh. You better not wait. I'll catch you later.

GEORGE: You sure?

JERRY: (dry) He knows.

(KRAMER goes on his mission and they're waiting for the elevator as the Receptionist calmly arrives, not noticing them. She could be a model.)

GEORGE: Wow. Nice.

JERRY: Why don't you try your engagement story?

GEORGE: (considers, but got into elevator) Won't work.

JERRY: Are you sure?

GEORGE: (wry) He knows.

[Reggie's diner. Elaine and Kevin, a geekish nice guy in a red jacket, are eating at a table. ELAINE is a little bored, dreading.]

ELAINE: Look. Kevin. I really like you, heh, heh, uh. But, um, maybe we'd be, better off just being.. friends. (takes a bite of her sandwich)

KEVIN: Friends?

ELAINE: Well. I mean. (distracted by the food) Oh, god. This tuna tastes like an old sponge.

KEVIN: Friends. Yeah! Why not friends? I might like to try that! Like you an' Jerry!

[Men's room door. Flush sound and &quot;Wooo!&quot;.]

(Down the hall, a Man in a suit is trying to use the copier as Kramer comes out of the men's room affecting nonchalance.)

MAN: (frustrated) Damn thing is jammed again..

KRAMER: You know what happens with these? The rollers, they get flat spots on 'em. (hits button several times and whacks it)

MAN #2 (rushing by, tense) Hey Leland wants everyone in the conference room right now.

MAN: Come on, let's go.

KRAMER: (absently wants to be part of it) Oh, yeah--yeah.. (follows them)

[Monk's. George, Jerry, and Elaine at a booth.]

ELAINE: (to Jerry) Remember I was telling you about Gillian, my friend who writes for the L.L. Bean catalogue? I really think you should give her a call.

JERRY: (doubtful) I don't know, do you have a--

(Elaine has anticipated the question, gives him her picture.)

JERRY: Not bad. Wuh--what does she--

ELAINE: (dry) I put her stats on the back.

JERRY: Pretty impressive -- &quot;Serious boyfriend '92 to '95.&quot; Owns her own car.. &quot;Favorite president: James Polk!&quot; (Elaine had echoed: &quot;Uh-huh. Yup.&quot; during it)

GEORGE: Hnn! Let me see that.

JERRY: (hands it to him as he asks Elaine) So how'd it go with Kevin? Did you steel-toe his ass back to Kentucky?

ELAINE: (laughs in appreciation) You are not gonna believe this! I told him that I just wanted to be friends. He's fine with it. He really wants to be friends.

JERRY: Why would anybody want a friend?

ELAINE: (dread) Uh. It's really not that bad, actually. He said he'd even go with me to the Museum of Miniatures. This is something you would never ever do.

JERRY: I mean all that stuff is so small.. (Elaine is wryly assessing him) stupid..

GEORGE: (still looking at that picture) You know if I told my engagement story to that receptionist, but told her this, was my fianc&eacute;e..

JERRY: What?

GEORGE: Don't you see. Women like that are like, members of a secret tribe living in a forbidden city. People like me have not been inside in thousands of years.. But with this, it's like I've already been with one of her own! My hands been stamped! I come and go as I please!

ELAINE: (wry, not impressed) Well you cracked it! I warned the Queen you were gettin' close an', now it looks like we're gonna have to move the whole damn forbidden city. (chuckles with Jerry)

GEORGE: (getting up) Can I keep this?

ELAINE: No, I need it.

GEORGE: (absently leaving with it) Thanks.

[The Brandt/Leland office reception area. George comes in off elevator to the Receptionist (Amanda).]

GEORGE: Hi. I'm ah, I'm here to see a Mr. Art Vandelay..

AMANDA: I'm sorry sir, there is no Mr. Vandelay here..

GEORGE: (taking out wallet) Well, let me. Heh. Let me just eh.. check an', make sure I have the right man--ha! Heh, heh, heh! Seems--oh! I, oh! (has conveniently dropped the photo into her view)

AMANDA: Oh! She's beautiful! Who is she?

GEORGE: (humble) Well, if you must know, she, was my fianc&eacute;e, Susan. May she rest in peace.

AMANDA: Oh, sorry.. She was lovely. I'm Amanda.

GEORGE: (shaking hands) I'm George.

(As they continue talking, Man #3 and Kramer go to the elevator.)

MAN #3: Good work today, K-man!

KRAMER Well, you know what they say, you don't sell the steak, you sell the sizzle.

MAN #3: Heh, heh!

KRAMER: You want a drink? I'm buyin'.

MAN #3: In that case, make mine a double!

(They yuck it up, getting on the elevator.)

[Night at a restaurant. Jerry and Gillian meet.]

GILLIAN: Jerry?

JERRY: Gillian. Hiii..

GILLIAN: Very nice to meet you.

JERRY: It's nice to meet you!

(They're shaking hands and he notices hers are like a man's--nice enough, but big, beefy.)

[Day, at Monk's. Jerry and Elaine are in a booth.]

JERRY: She had man-hands.

ELAINE: (pause) Man, Hands?

JERRY: The hands of a man. It's like a creature out of Greek Mythology, I mean, she was like part woman, part horrible beast.

ELAINE: (weary) (Look,) would you, prefer it, if she had, no hands at all?

JERRY: Would she have hooks?

ELAINE: Do uh, do hooks make it more attractive, Jerry?

JERRY: Kinda cool lookin'..

ELAINE: (getting up to go) Uh.. Listen, you're picking me up from my (place tomorrow)--

JERRY: (leaving too) Yeah. Yeah.

ELAINE: Okay, I've got five, huge boxes of (buttons).

JERRY: Right. Well if you need an extra set of hands, I know who you can call--

ELAINE: (weary) Jerry!

[Early morning in Jerry's &quot;house,&quot; a tea kettle is whistling. Kramer's cooking breakfast as Jerry comes out of the bedroom, tired but surprised. He's in his pajamas. Kramer's wearing a suit and oven mitts.]

JERRY: Kramer?!

KRAMER: Hey buddy! Hey!

JERRY: It--eight o'clock in the morning! What the hell is goin' on?!

KRAMER: Breakfast. I gotta be in at Brandt/Leland by nine.

JERRY: Why??

KRAMER: Because I'm workin' there, that's why.

JERRY: (disoriented) How long have I been asleep? What--what year is this?

KRAMER: Jerry. I don't know if you've noticed, but lately, I've been drifting, aimlessly?

JERRY: (snaps fingers) Now that you mention it.

KRAMER: But I finally realized what's missing, in my life. Structure. An' at Brandt/Leland, I'm gettin' things done. An' I love the people I'm workin' with.

JERRY: How much are they payin' you?

KRAMER: Oh, no, no, no-no--I don't want any pay. I'm doin' this just for me.

JERRY: Really. So uh, what do you do down there all day?

KRAMER: T.C.B. You know, takin' care o' business. Aa--I gotta go.

JERRY: All right.

KRAMER: (leaving) I'll see you tonight, huh? (turning back, grabs his briefcase) Forget my briefcase.

JERRY: W-w-wha' you got in there?

KRAMER: (as he leaves with it) Crackers.

(MUSIC (Sheena Easton's &quot;Morning Train (Nine to Five)&quot;) accompanies assorted shots of working-man Kramer: Getting on the subway (everyone else is going the opposite direction). Washing his shoes at the water cooler. Eating rolls of crackers out of his briefcase. Laughing it up after hours with co-workers at a TGIF-type restaurant.)

[Jerry at home on the phone. George is on the other end.]

JERRY: So the picture worked. Amazing!

GEORGE: Hey, she wants me to dress uh &quot;smart casual.&quot; What uh, what is that?

JERRY: I don't know, but you don't have it.

GEORGE: Right. Bye.

(Elaine comes in, appalled, shrugging at Jerry. He doesn't know why so shrugs back.)

ELAINE: Where were you today?

JERRY: What?

ELAINE: Pick. Up.

JERRY: (whispers) Damn.

ELAINE: So? Where were you?!

JERRY: Uh, here I guess, an' uh, uh I went out and picked up a paper.

ELAINE: (irritated, throwing down her bag) I had to ask Kevin, to leave his office an' come an' pick me up!

JERRY: So? What are friends for?

ELAINE: Yeah! An' he is a friend, Jerry. He is reliable. He is considerate. He's like your, exact opposite.

JERRY: So he's Bizarro Jerry!

ELAINE: (pause) Bizarro Jerry?

JERRY: Yeah. Like Bizarro Superman. Superman's exact opposite, who lives in the backwards bizarro world. Up is Down. Down is Up. He says &quot;Hello&quot; when he leaves, &quot;Good bye&quot; when he arrives.

ELAINE: (pause) Shouldn't he say &quot;Bad bye&quot;? Isn't that the, opposite of &quot;Good bye&quot;?

JERRY: No. It's still a goodbye.

ELAINE: Uh. Does he live underwater?

JERRY: No.

ELAINE: Is he black..

JERRY: Look. Just, forget it, (already). All right?

(Kramer enters, exhausted, frazzled.)

KRAMER: Wow. Man. What a day. Could I use a drink. (starts getting a drink and ice)

JERRY: Tough day at the office?

KRAMER: Just comin' in, an' that phone just wouldn't stop.

JERRY: Well, we better get goin' if we're gonna go to that uh, seven o'clock cold fusion.

KRAMER: Yeah. Well, count me out. I'm swimmin'. Old man Leland is bustin' my hump over these reports. If I don't get 'em done by nine, I'm toast.. (takes a swig and reacts)

[Exterior of cheap-looking bar with neon &quot;V&quot;s on the outside. Then interior, it's a black-walled dance bar full of gorgeous women and George and AmandaA.]

GEORGE: This is a fantastic place! I always thought it was a meat packing plant!

(Three female models come up to them.)

MODEL #1: Hey! Amanda!

AMANDA: These are my friends. Anabelle, Justina, and Nikki. We used to model together.

GEORGE: Oh! Modelling! What's that like? Fun? Ha ha. (to self in head) Stupid! Stupid! Stupid!

AMANDA: So Nikki, uh, how was Paris this time?

MODEL #2: (petulant) A bore.

GEORGE: You know, I used to love Paris. My uh, dead Fianc&eacute;e, Susan.. (opening wallet) In fact I. think I, I may have a picture of her..

(shows them the picture)

MODEL #3: (awed) Wow.. She was beautiful.. Do you wanna dance?

[Exterior of a restaurant.]

(Close-up of Gillian's man-hands tearing restaurant bread in half.)

(Gillian and Jerry at a table in a restaurant. Jerry's hiding his horror of her hands.)

GILLIAN: Would you like some bread, Jerry?

JERRY: No.. No thanks, I'm, just not hungry.

GILLIAN: Well, then at least drink your beer.. (she's opening the bottle--brief closeup on hands)

JERRY: (to self) Oh. Twist off..

GILLIAN: You have a little something on your face.

JERRY: I can get it. (feeling his face)

GILLIAN: Eh, no-no, No-no.. You're missing it, it's higher. (reaches over)

(A close-up of his face while a man-hand is getting the very small thing. He's uneasy.)

GILLIAN: (friendly) It's an eyelash. Make a wish.

(Closeup of his face with her outstretched finger in front of him.)

JERRY: I don't want to.

GILLIAN: Make a wish.

JERRY: Okay. (closes eyes, blows on her finger, opens eyes, says to self) Didn't come true.

GILLIAN: (smiles at him) Don't you just love lobster?

(Closeup of her man-hands tearing open a lobster.)

[Night, Elaine and Kevin walking into Reggie's diner.]

KEVIN: That Museum of Miniatures was amazing.

ELAINE: I know, he's so tiny!

(Two guys are sitting in a booth and Kevin sees them, motioning Elaine to come along.)

KEVIN: Yeah! Hey--hey guys! Elaine, sit down. These are a couple o' my friends. Uh, this is Gene. And this guy, we.. just call &quot;Feldman!&quot;

(Gene is short and bald with glasses. Feldman is tall and a little goofy-looking. Elaine sits with them.)

ELAINE: (to self in head) Bizarro world..

[Day, Jerry and George at Monk's.]

GEORGE: (delighted) Jerry? It was incredible! Models! As far as the eye could see!

JERRY: Then it does exist.

GEORGE: Yes. The legends are true.

JERRY: So when are you goin' out with this girl again?

GEORGE: I'm not! I'm inside the walls!

JERRY: So you're gonna burn that bridge.

GEORGE: Flame on!

[Reggie's, Feldman, Gene, and Kevin are at a booth when Elaine comes and sits with them.]

GENE: (about the check) I got it.

KEVIN: (grabs it) No. You got it last time.

GENE: (calmly takes it) Don't worry about it.

ELAINE: (to self in head) This is unbelievable..

FELDMAN: Hey Elaine, what do you think of an alarm clock, that automatically tells you the weather when you wake up?

ELAINE: Well, I gotta say that I think that that is a fantastic idea, Feldman!

FELDMAN: Nah, it's not--it's just not practical.

KEVIN: (getting up) Well. See ya later, Elaine. Feldman an' I 'a' gotta get down to the library. (the three guys are leaving)

ELAINE: What are you gonna do down there?

KEVIN: Read!

ELAINE: (pause, then vaguely waves while watching them leave) Hello?

[At Jerry's, Jerry and Kramer eating breakfast. Kramers's tired, preoccupied with the paper, Jerry's in a blue robe eating cereal and orange juice.]

JERRY: So, uh. Gillian's comin' over later. I think I'm gonna end it.

KRAMER: Uh-huh.

JERRY: Those meaty paws, I feel like I'm dating George the Animal, (steer).

KRAMER: Yeah..

JERRY: Maybe I'll chain her to the refrigerator an' sell tickets.

KRAMER: That's nice..

JERRY: Kramer, put the paper down! You never listen to me anymore! We hardly even talk!

KRAMER: Well, we're, talkin' now, aren't we?--

JERRY: I sit here for twenty lousy minutes in the morning--

KRAMER: Oh here we go--

JERRY: An' then when you come home at night, you're always exhausted--we never do anything anymore!

KRAMER: What are you starting with me for? You know this is my crazy time o' year?!

JERRY: (pause) It's your third day..

KRAMER: (grabs briefcase to leave) I gotta go to work. We'll talk about this later. (leaves)

JERRY: Well. (calling down the hall) Call if you're gonna be late!

(Elaine arrives.)

ELAINE: What? What is goin' on with you two?

JERRY: Oh, I don't wanna talk about it..

ELAINE: All right, listen. Have you seen my addre-- (sees address book on counter) --ah! There it is. Okay. I got it. I'll see you later. (leaving)

JERRY: Hey! Wait! Wait! Wait a second! Where you goin'? I-I hardly ever see you anymore.

ELAINE: (stops, pause) Well, I. (a little ashamed) I guess I been at Reggie's..

JERRY: The Bizarro coffees hop?

ELAINE: Kevin and his friends are nice people! They do good things. They read..

JERRY: I read.

ELAINE: Books, Jerry.

JERRY: (pause) Oh. Big deal..

ELAINE: Well! I can't spend the rest of my life coming into this stinking apartment every ten minutes to pore over the, excruciating minutia, of every, single, daily event..

JERRY: (What's goin' on,) like yesterday, I go to the bank to make a deposit, an' the teller gives me this look, like--

ELAINE: I'll see you later man. I gotta go.

JERRY: (frustrated, to self) The whole system is breakin' down!

[George in his bathroom, drying his hair, looking at Gillian's picture. In the background, on a door, is a poster of a closeup of Dennis Franz in black and white. The phone rings and GEORGE puts the picture and blow-drier together on the side of the sink--the dryer's still running.]

GEORGE: Hello? Amanda. Hi, yes. Listen. You know, I'm thinkin', we might just be better of bein' friends. Yeah. Yeah, you know what, I can't even really talk about it right now. Bye-bye. (hangs up) (happy with himself, but then sees the burned up photo) No! No!

[At Jerry's, Jerry and Gillian.]

GILLIAN: Friends. Just friends.

JERRY: Yeah.

GILLIAN: Yeah. All right. Well, do you still want to see a movie later?

JERRY: I wish I could, but, we're friends..

GILLIAN: I'm just gonna go, wash my hands.

JERRY: Good idea. (phone's ringing, goes to it) (muttering) There's a beach towel on the rack.. (to phone) Yeah.

GEORGE: (frantic) Jerry! Jerry, muh--muh--my hair-dryer ruined the picture! An' I need another one or I can't get back into the forbidden city!

JERRY: (to drive him crazy) Who is this..

GEORGE: Jerry! I need you to get another picture of man-hands. I'm beggin' you!

JERRY: (pause) If I get it for you, will you take me to that club an' show me a good time?

GEORGE: Yes! Yes, alright--anything!

(Jerry looks toward the bathroom--Gillian's taking forever--she's got quite a job there. He opens her purse and peeks around in it. Close-up of his hand getting the picture.)

JERRY: Got it. (but now a man-hand grabs his arm) Uh!

[Jerry's house, it's dark, Jerry is sitting at the table waiting up as Kramer comes in and turns on the light.]

KRAMER: Jerry. Hey Jerry?

JERRY: I'm right here. (note: he has an athletic bandage on his right hand) You're late.

KRAMER: Yeah, well, I got held up, you know. What happened to your hand?

JERRY: Like you care.

KRAMER: The work piled up, I lost track of time--

JERRY: (calmly getting up with plate of chicken) Oh! Sure! Sure! You an' your work! Elaine's off in the Bizarro World, George only calls when he wants something, an' I'm left sitting here like this plate of cold chicken, which, by the way, (drops chicken into sink) was for two.

KRAMER: You cooked?

JERRY: (calm) I ordered in. It's still effort.

KRAMER: (in pain) Ow! Jeez!

JERRY: What's wrong?

KRAMER: (Ow!) It's my stomach.

JERRY: You're probably gettin' an ulcer. This job is killing you! It's killing Us.

KRAMER: (putting briefcase down) You know what? You're right. These reports, they can wait a couple of hours. Whadda say we go out tonight? Any place you want.

JERRY: The coffee shop?

KRAMER: You got it, buddy.

JERRY: (pleased) I'll call George!

[Night exterior. George, Jerry, and Kramer walking down the street. Down the sidewalk is Elaine coming out of a shop by a liquor store.]

JERRY: Hey. Isn't that Elaine?

GEORGE: (quiet, desperate) Maybe she can get me another picture of man-hands. (calling) Elaine!

JERRY: Elaine!

KRAMER: Elaine!

ALL THREE: Elaine! Elaine!

(Elaine finally notices them, but suddenly from the other direction, three other guys are coming: Gene, Kevin, and Feldman.)

KEVIN: Hey! Elaine! Hi-i! Over here!

(Eerie music as Elaine looks one way, then the other. All the men continue walking toward her. They all meet by her. To the left of Elaine are KEvin, Gene, and Feldman. To the right of her are Kramer, George, and Jerry. It's like a bizarre mirroring.)

ELAINE: Jerry.. George, Kramer.. This is Kevin, Gene.. and Feldman.

(The men murmur &quot;How ya doin'..&quot;, &quot;Good to meet you..&quot;)

JERRY: (quietly crept out) This is really weird..

(KRAMER's drinking--a bottle of Pepto Bismol.)

ELAINE: (diplomatically to Kevin, Gene, and Feldman) Could you guys excuse us, just for a moment.

KEVIN: Sure. (strolls away with his friends)

ELAINE: (pause) Thanks. (to Jerry, George, and Kramer) What.. what do you guys want..

GEORGE: Elaine, I got to have another picture of Gillian.

JERRY: I tried to get him one but Man-Hands almost ripped my arm out of the socket!

(While the three argue briefly, Elaine looks over to see where Kevin and friends are. They are friendly giving a beggar money.)

KEVIN: (Here ya are.)

ELAINE: (pause, turns back to Jerry et al.) Guys. I gotta go. Take it easy.

(GEORGE tentatively follows her a couple steps.)

GEORGE: Elaine? (she turns, sighing) Can I come?

ELAINE: I'm, I'm sorry.. We've already got a George..

(Kevin, Gene, and Feldman cheerily/nicely turn to Elaine as she joins them. They all go on companionably. Jerry, George, and Kramer look on awkwardly.)

[Day, interior of Brandt/Leland, Kramer's in Leland's office. Leland's a greying man.]

KRAMER: What did you want to see me about, Mr. Leland?

LELAND: Kramer, I've.. been reviewing your work.. Quite frankly, it stinks.

KRAMER: Well, I ah.. been havin' trouble at home and uh.. I mean, ah, you know, I'll work harder, nights, weekends, whatever it takes..

LELAND: No, no, I don't think that's going to, do it, uh. These reports you handed in. It's almost as if you have no business training at all.. I don't know what this is supposed to be!

KRAMER: Well, I'm uh, just--tryin' to get ahead..

LELAND: Well, I'm sorry. There's just no way that we could keep you on.

KRAMER: I don't even really work here!

LELAND: That's what makes this so difficult.

[Night, George at the forbidden city.]

GEORGE: Hi. George.

MODEL #4: Are you sure you're supposed to be here?

GEORGE: Oh, yeah. Yeah. I used to come here all the time with my fianc&eacute;e, back when it was a meat-packing plant. Ha. Here's her picture. (hands her a magazine page)

MODEL #4: What'd you do? Cut this out of a magazine or something?

GEORGE: Huh?

MODEL #4: That's me? It's from a Clinique ad I did..

GEORGE: Ha! Heh.

(Muscled bouncer comes up.)

BOUNCER: Let's go. Private party. (escorts him out)

[Night, Kevin's apartment. The doorbell rings. Kevin gets up to get it. Gene is reading on the couch. The apartment layout and furniture is the reverse of Jerry's--what's to the left in Jerry's is to the right at Kevin's. Also, Kevin's furnishings have more earth tones.]

KEVIN: Who is it?

ELAINE: (off-camera) It's Lainey!

KEVIN: (unlocks, opens door) Hi Elaine! (warmly hugs her)

ELAINE: Hi (?). Oh! Oh-oh-oh-oh!

KEVIN: Come on in!

ELAINE: Okay. Hi Gene! (comfortably tosses her bag to the left, it falls on the floor) Uh-ha! (smiling, picks up her bag, puts it on a chair that's to the right)

(As Kevin gets back to reading, Elaine looks around, noting how everything is the reverse. There's a unicycle on the wall. There's a bookcase near the couch. She goes to the kitchen. He has whole foods, not cereal boxes.)

ELAINE: What's up?

KEVIN: (friendly) Just reading..

(Elaine decides to make herself at home, opens the refrigerator and starts eating olives out of a jar, with her fingers. In the living room area, Kevin is looking at her. Note: a statue of Bizarro Superman on a stereo speaker.)

KEVIN: Hey.. What're you doing?

ELAINE: Eatin' olives.

KEVIN: Have you ever heard of asking? (door bell rings) Who is it?

FELDMAN: (off-screen) Feldman.. From across the hall.

KEVIN: (drops his suspicion and smiles, goes to door) Hold on. (unlocks and opens door) Hey.

FELDMAN: He-ey, Kevin!

KEVIN: Hi.

FELDMAN: Look who I ran into.

(Vargus, a heavy Fed-Ex guy comes into view in the hall.)

VARGUS: (testy) Hello, Kevin..

KEVIN: (testy) Hello, Vargus..

(They both break into grins, chuckling--it was a joke.)

KEVIN: Ya wanna catch a ballgame this weekend?

VARGUS: Great! I'll see ya later! (leaves)

KEVIN: Okay. (to self, smiling) Vargus.. (to Feldman) So?

FELDMAN: I got 'em..

KEVIN: All right! Hey, Elaine, Feldman was able to get us all tickets to the Bolshoi!

ELAINE: Oh! (comes to him)

KEVIN: (enthused) Fourth row, center.

ELAINE: Get out! (pushes him back, but he falls back on the floor!)

(Gene and Feldman rush to Kevin, who's hurt!)

GENE: (to Elaine) What is the matter with you?

ELAINE: Oh, Kevin! I'm so sorry. Is there anything I can do?

GENE: Haven't you done enough already?

(Embarrassed, she grabs her bag and tries to leave. But the locks are set and the rape chain's in place. She tries to undo the locks.)

ELAINE: (turning to them, awkward) It's locked..

(The meat-packing plant. George and Jerry are walking around. The walls are still black, but there's no furniture and there are sides of beef hanging on hooks, sawdust on the floor. They are the only people there. George is dismayed, and bumps into a side of beef in his distraction.)

JERRY: So this is it, huh?

GEORGE: But it--eh.. It was here, I'm tellin' you, an' w--w--it was really here! The, there was, a, bar, and a, an' a dance floor..

JERRY: (dry) I guess the DJ booth was over there behind the bone saw? (really disappointed) Let's get out of here George.

(Shot of their feet in the sawdust as they walk out. There is the magazine photo George had brought there the previous night.)

[Day, exterior of an apartment building. The familiar Seinfeld interlude music, but done via corny &quot;Brady Bunch&quot;-esque sound effects.; Interior of Kevin's apartment. Gene and Kevin are talking]

GENE: At work today, I discovered there's a payphone in the lobby that has free long distance.

KEVIN: Oh, so what did you do?

GENE: I called the phone company an' immediately reported the error.

KEVIN: Nice. (doorbell rings) Who is it?

FELDMAN: (off-screen) Feldman.. From across the hall.

KEVIN: (smiles, relieved) Hold on.

(Gene's arranging fruit as Kevin goes to unlock and open the door. Feldman's carrying two paper bags of groceries.)

FELDMAN: Kevin! Brought some groceries.

KEVIN: Again?! Feldman, you didn't have to do that!

FELDMAN: Hey, what are friends for?

KEVIN: You know, I may not say this enough but you two are about the best friends a guy could have.

(They have a long smarmy group hug.)

KEVIN: (eyes closed in hug) Oh. Me so happy. Me want to cry.

The End
