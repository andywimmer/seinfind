---
layout: default
title: The Bookstore
parent: Season 9
nav_order: 17
permalink: /the-bookstore
cat: ep
series_ep: 173
pc: 917
season: 9
episode: 17
aired: April 9, 1998
written: Spike Feresten and Darin Henry & Marc Jaffe (story). Teleplay by Spike Feresten
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697659
wiki: https://en.wikipedia.org/wiki/The_Bookstore
---

# The Bookstore

| Season 9 - Episode 17                                                                       | April 9, 1998             |
|:--------------------------------------------------------------------------------------------|:--------------------------|
| Written by Spike Feresten and Darin Henry & Marc Jaffe (story) - Teleplay by Spike Feresten | Directed by Andy Ackerman |
| Series Episode 173                                                                          | Production Code 917       |

"The Bookstore" is the 173rd episode of the NBC sitcom Seinfeld. This was the 17th episode for the ninth and final season. It aired on April 16, 1998.

## Plot

The episode opens with a montage of what Kramer does at Jerry's place when he's not home. This includes making and spilling a smoothie (using Jerry's couch cushion to clean it up), riding Jerry's bike around his apartment, yelling down at people on the street, doing a Jerry stand-up impersonation, redecorating the apartment, and hosting parties. Kramer manages to revert the place to normal by the time Jerry comes home, except for a drink that is not on a coaster.

Jerry and George are at a bookstore, Brentano's, where George hopes to meet women and Jerry spots Uncle Leo shoplifting. George takes a large book into the bathroom with him, then the bookstore makes George buy the book. Elaine is at the annual Peterman party, where everyone is anxious to know if she is going to dance again. Elaine doesn't dance at the party; instead, she and a man named Zach get drunk and make out at their table. George suggests that Elaine tell everyone that she and Zach are dating, so that she won't be known as the "office skank". Kramer and Newman plan to implement Kramer's idea for running a rickshaw service in the city. They are getting a rickshaw from Hong Kong and need to find someone to pull it. Jerry confronts Uncle Leo about the stolen book. Uncle Leo claims it is a right as a senior citizen. Elaine catches Zach with another woman. Kramer and Newman attempt to interview potential rickshaw pullers from a collection of homeless men; however, one of the candidates steals the rickshaw. George tries to return his book, but is told the book has been "flagged" as having been in the bathroom.

Jerry rats out Uncle Leo to the security guard at the bookstore, and later talks with his parents about Uncle Leo's theft. He is told about Uncle Leo's prior arrest, a "crime of passion" of which his mother will not tell him the details. His parents also inform him of the senior approach; it is not stealing if you need it. Elaine plans to use the cheating angle to protect her reputation. Jerry tries to talk with Uncle Leo, but the only thing Uncle Leo tells him is that he never forgets when he's been betrayed. George discovers his book has been "flagged" in all the databases as a bathroom book. Elaine's plan goes awry when J. Peterman demands that she help Zach get off the "yam yam" by helping him to quit cold turkey. Jerry has a nightmare in which a heavily tattooed Uncle Leo does pull-ups in jail and vows to have revenge (a parody of Cape Fear).

Newman and Kramer discover where the rickshaw is and Kramer loses the contest to determine who will pull the other. George tries to donate his book to charity, but even they won't take the marked book. When Kramer gets tired pulling Newman in the rickshaw up a hill and lets it go, the results are disastrous as the rickshaw runs over Elaine's "boyfriend" Zach. George plans to steal a good copy of the book, so he can return it to get his money back. Just as Jerry finds out from the manager that the manager has been told that the store needs to make a good example out of a shoplifter, any shoplifter, as long as they catch him in the act. Jerry then points out that George is shoplifting, and he gets caught.

## Controversy

In this episode, J. Peterman referred to opium as "the Chinaman's nightcap". The episode prompted many Asian American viewers, including author Maxine Hong Kingston, to send letters of protest. In her letter, Kingston wrote that the term is "equivalent to niggers for blacks and kikes for Jews". Media watchdog Media Action Network for Asian Americans (MANAA) called on NBC to issue a public apology. NBC did not issue an apology, but it removed the offending term from the episode in the episode's rerun in May 1998. NBC's executive vice president for broadcast standards and content policy sent MANAA a letter stating that the network never intended to offend. MANAA was pleased with the studio's response despite the lack of an apology, and Kingston, while disappointed there was no apology, was pleased that the term was removed from the episode.

## Cast

#### Regulars

Jerry Seinfeld ...................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus .............. Elaine Benes  
Michael Richards .................. Cosmo Kramer

#### Recurring

Wayne Knight ............... Newman  
Barney Martin ............... Morty Seinfeld  
Liz Sheridan .................. Helen Seinfeld  
John O'Hurley ............... J. Peterman  
Len Lesser .................... Uncle Leo

#### Guests

Jonathan Penner ................. Zach  
Ted Rooney ........................ Crichton  
John Gries ........................... Rusty  
Sonya Eddy ........................ Rebecca DeMornay  
Merrin Dungey ................... Cashier  
Kevin Ruf ........................... Security Guard  
Heather Morgan ................ Server  
Lauren Bowles ................... Waitress  
Mark Daniel Cade .............. Walter  
Sloppy Joe .......................... Hobo Joe

## Script

[Setting: Bookstore]

(George and Jerry both enter a bookstore)

GEORGE: I read somewhere that this Brentano's is the place to meet girls in New York.

JERRY: First it was the health club, then the supermarket, now the bookstore. They could put it anywhere they want, no one's meetin' anybody.

(Kramer walks up to Jerry and George while leafing through a book)

KRAMER: Jerry, look at all these pagodas, huh? I gotta get over to Hong Kong before it all goes back to China..

JERRY: (Sarcastic) You better hurry.

GEORGE: I'm gonna hit the head.

KRAMER: Oh, boy, look at this. Hong Kong's outlawed the rickshaw. See, I always thought those would be perfect for New York.

JERRY: (Sarcastic) Yes. The city needs more slow-moving wicker vehicles.

(George is about to go into the bathroom. He grabs a book on his way in)

KRAMER: Hmm, Elaine's been to Hong Kong. I should give her a call.

JERRY: She's at that annual Peterman party tonight. You know the one she danced at last year?

KRAMER: (Remembering) No, that wasn't dancing.

JERRY: (Pointing) Hey, there's Leo.

KRAMER: Oh? Who's Leo?

JERRY: Uncle Leo.

KRAMER: Oh, yeah. Right. Uncle, Leo. Forgot his first name...

(Kramer and Jerry both watch Leo looking at the books on a shelf. Then, Leo takes a book and puts it under his coat)

JERRY: Did I just see that?!

KRAMER: (To Jerry) Well, that ought'a keep you busy for a few days, huh?

[Setting: The Annual Peterman Party]

(Elaine is sitting alone at a table. Walter, a co-worker, comes up to her)

WALTER: (Joking around with her) So, Elaine... are you going to dance this year?

ELAINE: Maybe... All over your face!

(A waiter serving food approaches Elaine)

WAITER: If you do dance, the cooks want to know - so they can be brought out of the kitchen. They missed it last year.

(Elaine is looking both angry and embarrassed. Scene cuts to Peterman giving a toast)

PETERMAN: My friends, a toast. As the woolly-haired Melanasians of Papua, New Guinea once said, (Makes a series of clicking and popping sounds. The music starts up) Alright! Who's dancing? (No one makes a gesture that they intend to dance) No one? Alright, I'll just have to get things started. (Grabs a female employee, and starts dancing with her. The crowd is impressed)

(Scene cuts back to Elaine's table. A fellow employee sits down at Elaine's table)

ZACH: Hi, I'm Zach.

ELAINE: Hi, I'm miserable. (They both laugh)

[Setting: Bookstore]

(George casually puts the large book he took into the bathroom with him on the shelf. A manager notices, and approaches him)

MANAGER: Excuse me, Sir. What are you doing?

GEORGE: (Acting innocent) I'm all set.

MANAGER: (Pointing) Did you take that book with you into the bathroom?

GEORGE: (Not sure what the answer should be) What do you want to hear?

[Setting: The Coffee shop]

(George and Jerry are at Monk's Coffee shop)

GEORGE: They made me buy it... A hundred bucks this thing cost me. (Gesturing to the book) How dare they?! I got news for you, if it wasn't for the toilet, there would be no books.

JERRY: (Sarcastic) Yeah. I understand Gutenberg used to spend a lot of time in there.

GEORGE: They're selling coffee, bran muffins... you're surrounded by reading material. It's entrapment!

JERRY: (Reading the cover of the book George was forced to buy) 'French Impressionist Paintings'?

GEORGE: I find the soothing pastoral images very conduc-

JERRY: (Cutting him off) Thank you very much.

GEORGE: Well, I'm gonna go back there later and return it when there's different people working... You want to catch a movie?

JERRY: I can't. I'm meeting Uncle Leo. I saw him shoplifting at the bookstore.

GEORGE: (Praising Leo's stealing) Alright, Leo! Stickin' it to the man!

(Elaine enters and sits down)

JERRY: Sleeping in the car again?

ELAINE: Cocktail flu.

JERRY: (Remembering) Oh, right. The big party...

GEORGE: You, uh, didn't dance again, did you?

ELAINE: (Angered) No, I found a better way to humiliate myself. There was this guy, and we had a few too many...

GEORGE: You went home with him?

ELAINE: Worse. We made out at the table like our plane was going down!

JERRY: (Rubbing it in) Ah, the drunken make-out. An office classic. Did you end up xeroxing anything?

ELAINE: (Gives Jerry a look) Do you know how embarrassing this is to someone in my position?

JERRY: (Confused) What's your position?

ELAINE: I am an associate.

GEORGE: Hey, me too.

(A waitress, passing their table, speaks up)

WAITRESS: Yeah, me too.

ELAINE: Oh God. Why did I do this? Now I'm the office skank.

GEORGE: Well, unless you tell everybody you're dating.

ELAINE: (Liking the idea) Ohh... right. Cause if we're dating, what everyone saw was just a beautiful moment between two lovers.

JERRY: (Jokingly rubbing it in) As opposed to a spirited bout of Skanko-Roman wrestling.

ELAINE: (Sarcastic) Oh, bravo.

[Setting: Jerry's apartment]

(Jerry enters his apartment to find Newman and Kramer having a conference at his table)

JERRY: (Sarcastic) Oh, hey. Can I fix you fellas some drinks and sandwiches?

KRAMER: (Taking his offer seriously) No, we've already eaten.

NEWMAN: (Gesturing to dishes and silverware on the table) But you can clear some of this stuff out of the way.

KRAMER: Jerry, check this out. (Pointing at some papers on the table) Remember my idea about rickshaws in New York? Well, we're gonna make it happen!

JERRY: (Jokingly trying to be skeptical) No, you're not.

KRAMER: Newman, he knows a guy in the Hong Kong post office...

JERRY: (Still skeptical) No, he doesn't.

NEWMAN: He's shipping us a rickshaw. It can't miss!

JERRY: Yes, it can.

KRAMER: We'll start out with one, and they when it catches on, we're gonna have a whole fleet!

NEWMAN: It's the romance of the Handsome Cab without the guilt or dander of the equine.

JERRY: So, who's gonna pull this thing?

(Kramer and Newman both look at each other)

KRAMER: (To Newman) Well, I just assumed you would.

NEWMAN: Yeah, but I though-

KRAMER: (Stopping his thought) Da-da-da-da no.

JERRY: (Extremely happy about Kramer and Newman's dilemma) My, isn't this an awkward moment?

KRAMER: (Brainstorming) What about the homeless?

NEWMAN: Can't we worry about them later?

KRAMER: (Explaining) To pull the rickshaw.

NEWMAN: (Pondering Kramer's plan out loud) They do have an intimate knowledge of the street..

KRAMER: They're always walkin' around the city. Why not just strap something to them?!

JERRY: (Sarcastic) Now, that's the first sensible idea I've heard all day.

[Setting: The Coffee shop]

(Uncle Leo enters, and joins Jerry at his booth)

LEO: Jerry, hello! (Sits down)

JERRY: So, Leo, how's everything? You doin' Okay?

LEO: I still have the ringing in the ears. Sounds like the phone.

JERRY: (Shrugging his problems off) Yeah, yeah. But what about money? Are you strapped? Do you need a little?

LEO: What, are you kidding? I should you loaning you money! (Quickly amending what he just said) But I'm not.

JERRY: (Being frank) Leo, I saw you in Brentano's yesterday.

LEO: Why didn't ya say hello?

JERRY: Because you were too busy stealing a book.

LEO: (Giving a courtesy lesson) You still say hello.

JERRY: (Showing that it's a problem) Leo, I saw you steal.

LEO: Oh, they don't care. We all do it.

JERRY: Who, criminals?

LEO: Senior citizens. No big deal.

JERRY: You could get arrested.

LEO: Arrested? Come on! (Goes into a routine explanation for his stealing) I'm an old man. I'm confused! I thought I paid for it. What's my name? Will you take me home?

JERRY: (Pleading) Leo...

LEO: Alright, alright. Mr. Goody Two-Shoes. You made your point.

JERRY: (Thinking he's stopped Leo's thefts) Thank you.

LEO: (Yelling out to every one in the coffee shop) Will somebody answer that damn phone?!

[Setting: Peterman office building hallway]

ELAINE: (Talking to a co-worker) Of course Zach and I have been dating. What'd you think, I was the office skank?

WALTER: Well...

ELAINE: &quot;Well&quot;? We've been dating for three months. Between you and me, and... anyone else you want to tell.

(Elaine enters into the break room. There, she sees Zach making out with another co-worker)

ELAINE: (Exits, closing the door behind her) Oh man. Ugh...

WALTER: (Pointing at the closed door) Isn't that Zach?

ELAINE: Yeah.

WALTER: Aren't you upset?

ELAINE: (Starts to fake cry) Yes. Oh, man! Oh!

[Setting: NYC street]

(Kramer and Newman are standing there with a rickshaw. Close by are three homeless guys in a line)

KRAMER: Alright, listen up. Now, you three have been hand-picked out of possibly dozens that applied. Now, what we're looking for are motivated, hard-working, homeless gentlemen like yourselves to pull rickshaws. (One of the homeless men starts to wander off, walking away) Now, I don't care where you're from, or how you got here, or what happened to your homes. But you will have to be physically fit.

(One of the 2 remaining homeless men drops a bottle)

HOMELESS MAN: The government!

KRAMER: (Continuing as if nothing happened) Because to pull rickshaws means more than just strong legs. You're also going to need a well-toned upper body. (looks at the two confused guys) Or a shirt...

NEWMAN: Alright, who's first?

(The homeless man with a shirt raises his hand. Kramer pats him on the shoulder - dust flies into the air)

KRMAER: Hey.

NEWMAN: Name, please.

HOMELESS MAN: Rusty.

NEWMAN: (Writing on a clipboard) Rusty.

KRAMER: (To Rusty) You know, I once knew a horse named Rusty. No offense.

NEWMAN: (To Rusty) Alright, uh, take it down to the end of the block. Make a controlled turn, and bring her back. Let's see what you've got! Ok? Ready, and go!

(Rusty takes off down the street with the rickshaw)

KRAMER: (Watching Rusty's pulling of the rickshaw) Giddy up! Good form.

NEWMAN: (Yelling out to Rusty) Alright, pace yourself, 'Cause you're gonna have to do this all day for very little money.

(They both notice that Rusty is not stopping)

KRAMER: Hey, what's he doin'?

NEWMAN: I think he stealing our rickshaw!

KRAMER: Well, then, he's out!

(Newman stops his timer. The other homeless man is still standing in line - alone)

HOMELESS MAN 2: (Salutes Kramer) I'll take the job. Potato salad!

[Setting: Bookstore]

(Jerry and George are at Brentano's. George is trying to return the book)

GEORGE: Yes, I, uh, I need to return this book.

CASHIER: (Puts the book's code into the computer) I'm sorry, we can't take this book back.

GEORGE: Why not?

CASHIER: It's been flagged.

GEORGE: (Confused) Flagged?

CASHIER: It's been in the bathroom.

GEORGE: It says that on the computer?

CASHIER: Please take it home. We don't want it near the other books.

GEORGE: (Outraged. Leaving) Well, you just lost a lot of business! Because I love to read!

(George storms out. Jerry is about to follow until he sees Leo stealing another book)

JERRY: (To himself) I don't believe this! (Walks over to a security guard) Excuse me, I wonder if you could do me a favor? My uncle's having a little problem with shoplifting...

GUARD: Mm-hmm. Where's your uncle?

JERRY: (Pointing) He's over there in the overcoat. If you could just kind of put a scare into him.. You know, set him straight...

GAURD: (Into his walkie-talkie) We have a 51-50 in paperbacks. All units respond.

JERRY: '51-50'? That - that's just a scare, right?

GUARD: Sir, I'm gonna have to ask you to stand out of the way and let us handle this. (The Guard rushes tward Uncle Leo) Swarm! Swarm!

(Suddenly, Leo is surrounded by guards)

LEO: What?! I'm an old man! I'm confused!

GUARD: You're under arrest.

JERRY: (To Guard) I just wanted you to scare him.

LEO: Jerry, you ratted me out?!

JERRY: (Unsure of what to say - he remembers Leo's courtesy tip) Hello?

LEO: Hello.

[Setting: Jerry's apartment]

(Jerry is on the phone with his parents)

JERRY: Mom, I didn't rat out Uncle Leo. I just wanted the guard to scare him straight.

HELEN: Jerry, he won't last a day in prison.

JERRY: (Scoffing) Prison. I'm sure it's just a fine.

MORTY: She's got priors.

JERRY: (Not believing it) Prior convictions? Leo?

HELEN: It was a crime of passion. Leave it alone.

MORTY: Besides, it's not stealing if it's something you need.

JERRY: (Confused) What does that mean?

HELEN: Nobody pays for everything.

JERRY: (Shocked at his parents) You're stealing too?!

MORTY: Nothing. Batteries. (Jerry scoffs) Well, they wear out so quick.

JERRY: Mom, you too?

HELEN: Sometimes your father forgets, so I have to steal them.

(Kramer and Newman both enter)

JERRY: Alright, I'll talk to you later.

KRAMER: (While washing his hands in Jerry's kitchen sink) Well, the rickshaw's gone. We strapped it to a homeless guy and he (Makes a noise), he bolted.

JERRY: (Joking around) Well, you know, eighty-five percent of all homeless rickshaw businesses fail within the first three months.

KRAMER: (To Newman) See, we should've gotten some collateral from him.. Like his bag of cans, or... his other bag of cans.

NEWMAN: We gotta find that rickshaw. You check the sewers and dumpsters. I'll hit the soup kitchens, bakeries, and smorgasbords.

(Newman and Kramer both go to leave)

JERRY: To the Idiotmobile!

[Setting: The Coffee shop]

(Jerry and Elaine are sitting at a booth)

JERRY: So, even though you're not really going out with this guy, he's cheating on you?

ELAINE: That is correct. But here's the beauty part - now I stand up for myself by telling everybody I'm dumping his sorry ass, and I'm the office-

JERRY: (Butting in) Tina Turner?

ELAINE: (Accepting) Alright.

(George enters with his large art book)

GEORGE: Well, I've to every Brentano's. This thing's flagged in every database in town!

JERRY: Is it so horrible to have to keep a book?

GEORGE: I don't understand what the big deal is. They let you try on pants.

JERRY: (Stern on George) Not underpants.

ELAINE: Hey, that's your Uncle Leo.

(Uncle Leo is at the front desk in Monks. He's paying for his check)

JERRY: (Getting up) Uncle Leo. Hello!

LEO: (Bitter) Jerry.

JERRY: (Trying to explain) Uncle Leo, I'm sorry. I didn't know about your... past.

LEO: (Exiting) You mean my crime of passion? If anyone betrays me, I never forget!

JERRY: (Following Leo out the door) Uncle Leo, wait! Hello?!

(Elaine has picked up George's book, and is now thumbing through it)

ELAINE: French impressionism. Oh, I love this. (Looking up at George) Now, what is the problem with this book?

GEORGE: Nothing.

ELAINE: How much do you want for it?

GEORGE: You know, I could let it go for... say... a hundred and twenty-five.

(Jerry has given up on Leo, and now rejoins Elaine and George at the booth)

JERRY: Leo's furious. (He stops in his tracks when he sees Elaine looking at George's book) What is that doing on the table?

GEORGE: (Wanting Elaine to take it off his hands, he tries to silence Jerry) Jerry, simmer down.

JERRY: (Pointing) I'm not eating anything in the vicinity of that book.

ELAINE: (Confused) What is wrong with this book?

GEORGE: Simmer!

JERRY: That book has been on a wild ride. George took it into the bathroom with him and-

ELAINE: (Cutting Jerry off, she stands up, trying to get away from the book. Yelling out) Alright! Everyone clear! Bio-hazard coming through! Clear! Clear!! (Runs to the bathroom to wash her hands)

GEORGE: (The damage has been done. He is slightly angered at Jerry) May I ask, what do you read in the bathroom?

JERRY: I don't read in the bathroom.

GEORGE: Well, aren't you something?

[Setting: Elaine's office]

(Peterman walks in)

PETERMAN: Elaine, do you have a moment? It's about your lover.

ELAINE: (Faking a broken heart) Oh yes. I know all about his little performance in the break room.

PETERMAN: Elaine, who among us hasn't snuck into the break room to nibble on a love newton?

ELAINE: (Confused) Love newton?

PETERMAN: I'm afraid the problem with Zach is more serious. He's back on the horse, Elaine. Smack. White palace. The Chinaman's nightcap.

ELAINE: An addict? (Sarcastic) Well, it just keeps getting better!

PETERMAN: And, in a tiny way, I almost feel responsible. I'm the one who sent him to Thailand - in search of low-cost whistles. Filled his head with pseudoerotic tales of my own Opium excursions. Plus, I have him some phone numbers of places he could score near the hotel.

ELAINE: Look, uh, Mr. Peterman, the fact is that I was planning on breaking up with Zach anyway. He was cheating on me!

PETERMAN: Damn it, Elaine. That wasn't Zach. That was the yam-yam. Now, he is going cold turkey. (Ordering) And you will be at his side.

ELAINE: Oh. Well, you know, I had planned to uh-

PETERMAN: (Cutting her off) No buts, Elaine. Or I will strip you of your 'associate' status. (Goes to leave) Uh, P.S., the first twenty-four hours are the worst. Better bring a poncho.

[Setting: Jerry's bedroom]

(Jerry is tossing and turning in his bed. Voices are going through his head while he's trying to sleep)

HELEN: It was a crime of passion.

LEO: If anyone betrays me, I never forget.

HELEN: He won't last a day in prison.

(Now, Jerry has slipped into a dream. He visualizes Leo in prison. The following scene of Leo in jail is a parody of the movie &quot;Cape Fear&quot;)

LEO: (Leo has &quot;Jerry&quot; written on the fingers his right hand, and &quot;Hello&quot; written on his left. He's doing pull-ups) Jerry. Hello. Jerry. Hello. Jerry. (Turns to the right, yelling out) Answer that damn phone!

(Scene cuts to Jerry, who is just now waking up to the phone's ringing. He answers it)

JERRY: Hello?

ELAINE: Hey, it's me.

JERRY: Uncle Leo?

ELAINE: (Sarcastic) Oh, that's nice. What are you up to?

JERRY: Nightmares. You?

ELAINE: My fake boyfriend is going through real withdrawals.

ZACH: (Yelling out from off-camera) I'm burning up! Elaine!

ELAINE: Eat your soup!

JERRY: You're not feeding him, are you?

ELAINE: Why? (Zach vomits. Elaine yells out to him) I told you, away from the curtains. Away. (Pointing) Use your bucket. (He vomits again - this time into the bucket) There you go, that's it. (To Jerry) You know what? I gotta go.

(Jerry hangs up, then tries to go back to sleep. Kramer walks in)

KRAMER: Hey, buddy.

JERRY: (Scared) Ah! Kramer!

KRAMER: I thought I heard you.

JERRY: Get out of here!

NEWMAN: (From outside the room) Kramer? Kramer? (Enters Jerry's bedroom) There you are.

JERRY: Will everybody please leave?!

NEWMAN: I just heard that a postman spotted a rickshaw down in Battery Park.

KRAMER: Our rickshaw?

NEWMAN: It's entirely possible.

JERRY: I want everyone out!

KRAMER: (Exiting the bedroom with Newman) Let's talk in Jerry's kitchen. I'll make some cocoa.

NEWMAN: (To Jerry) Good night.

JERRY: (Bitter) Good night, Newman.

[Setting: Park]

(Newman and Kramer both find the rickshaw and Rusty in the park)

NEWMAN: There it is!

KRAMER: Rusty!

RUSTY: Oh, there you are. Oh, do I get the job?

KRAMER: (Sarcastic) Yeah, yeah. We'll get back to you. (Pulling the rickshaw with Newman) Let's get this baby home.

NEWMAN: Uh...

KRAMER: What?

NEWMAN: You know, when you think about it, it's kind of silly for us both to pull this thing all the way back uptown. I mean, after all, it is a conveyance.

KRAMER: Yes, that's true.

NEWMAN: So, which one of us is gonna pull?

KRAMER: Well, there's only one way to settle this. (Starts pointing back and forth between Newman and him with each word of the rhyme) One spot, two spot, zig, zag, tear, pop-die, pennygot, tennyum, tear, harum, scare 'em, rip 'em, tear 'em, tay, taw, toe...

NEWMAN: (Realizing he won) Yeah.

KRAMER: Best two out of three? (Starts the pointing and the rhyme again) One spot, two spot..

(Scene cuts to Newman riding in the rickshaw - Kramer pulling)

NEMWAN: Hey, boy. Smooth it out up there. Too much jostling!

[Setting: A homeless charity center]

(George is trying to give the book to the homeless. He comes across Rebecca DeMornay - the same woman who confronted Elaine about her muffin stumps in episode &quot;The Muffin Tops&quot;)

REBECCA: (Gesturing toward the book) So, you want to donate this to charity?

GEORGE: Well, I assume there's some sort of write-off.

REBECCA: What's the value of the book?

GEORGE: Uh, about two hundred dollars, Miss DeMooney.

REBECCA: (Correcting. Stern) It's DeMornay. Rebecca DeMornay.

GEORGE: Oh.

REBECCA: (Opens the cover of the book) Oh, wait a second. (Certain) This book has been in the bathroom.

GEORGE: (Nervous) Wh-what are you talking about? That - that's ridiculous.

REBECCA: It's been flagged. I know. I used to work in a Brentano's. Mister, we're trying to help the homeless here - it's bad enough that we have some nut out there trying to strap 'em to a rickshaw!

GEORGE: (Desperate to get rid of the book) Alright, I, I'll just take fifty. Do - do we have a deal?

REBECCA: Yeah, and here it is: You get your toilet book out of here, and I won't jump over this counter and punch you in the brain!

GEORGE: I could take it in merchandise...

REBECCA: (Threatening to hit him) Here I come...

(George grabs his book and runs for his life)

[Setting: Bookstore]

(Elaine and Jerry are both at the bookstore. Elaine is talking with the cashier while checking out a book)

ELAINE: So, this book'll tell me how to get puke out of cashmere?

CASHIER: Yeah.

ELAINE: Great.

JERRY: (To Elaine) So, the worst is over?

ELAINE: Yeah. Now I can break up with him. He's clean, and I'm the office hero.

JERRY: Seems like you're better at fake relationships than real ones.

ELAINE: Yeah, huh. I even got an idea out of it; the Detox Poncho.

(She's through buying the book, and is ready to go)

ELAINE: (Leaving Jerry) See ya.

JERRY: (To cashier) I'd like to speak with the manager, please.

[Setting: NYC Street]

(Newman is being pulled up a steep hill on the rickshaw by Kramer)

NEWMAN: Mind your pace, Boy. Chop, chop!

KRAMER: (Tired) Oh, I can't go on. I gotta take a break. (Sets the rickshaw down - taking a rest)

NEWMAN: Well, don't tarry. I'm behind schedule as it is.

KRAMER: (Stretching) Oh...

(The rickshaw starts to roll back down the hill)

NEWMAN: (Scared) Boy... Boy. Kramer!

(The rickshaw starts to rapidly go down the street. Kramer makes an attempt to chase it)

KRAMER: Woah! Wait!

NEWMAN: Ahhh! Yaaaahhh!

(Scene cuts to Zach. he's walking out into the street)

ZACH: (Optimistic) Well, this is the first day of the rest of my life!

(Scene cuts back to Newman - still rolling down the hill)

NEWMAN: Waaaaahhhh!

(Zach and the rickshaw collide. From back up on top of the street, Kramer tries to separate himself from the accident. He starts walking, then running away from the crushed rickshaw)

[Setting: Bookstore]

(Jerry is in Brentano's - waiting for the manager. He sees George enter)

JERRY: George? What are you doin' here?

GEORGE: I can't sell the book. It's been marked.

JERRY: (Sarcastically joking) It certainly has.

GEORGE: So, I'm gonna steal another one, and then return it. That way, everything is even.

JERRY: (Trying to straighten things out) You defile one book, steal another, ask for your money back - and to you that's even?

GEORGE: I'm goin' in!

(George walks off to steal one of the books. The manager walks up to Jerry)

MANAGER: Did you want to speak with the manager?

JERRY: Yes. My Uncle Leo was caught shoplifting here the other day...

MANAGER: Yes, Uncle Leo. I remember him. I'm sorry, our policy is we prosecute all shoplifters.

JERRY: (Pleading) Oh, come on. He's just a lonely old man. All old people steal.

MANAGER: That's right. That's why we stopped carrying batteries. Look, I'll be honest with you, we've had a lot of trouble with theft lately - and my boss says I have to make an example to someone.

JERRY: So it could be anyone?

MANAGER: I... guess. As long as we catch him in the act.

(Jerry turns to George. George has a huge bundle under his overcoat - and is trying to act innocent)

JERRY: That guy! (Pointing at George) Swarm! Swarm!

(George is instantly surrounded by guards)

GEORGE: No! Jerry!

The End
