---
layout: default
title: Male Unbonding
parent: Season 1
nav_order: 4
permalink: /male-unbonding
cat: ep
series_ep: 4
pc: 102
season: 1
episode: 3
aired: June 14, 1990
written: Jerry Seinfeld & Larry David
directed: Tom Cherones
imdb: http://www.imdb.com/title/tt0697645/
wiki: https://en.wikipedia.org/wiki/Male_Unbonding
---

# Male Unbonding

| Season 1 - Episode 3                    | June 14, 1990            |
|:----------------------------------------|:-------------------------|
| Written by Jerry Seinfeld & Larry David | Directed by Tom Cherones |
| Series Episode 4                        | Production Code 102      |

"Male Unbonding" is the fourth episode of the NBC sitcom Seinfeld to be produced, and aired on June 14, 1990, as the fourth episode of the first season.

In it, Jerry Seinfeld tries to avoid meeting an old childhood friend, Joel Horneck (Kevin Dunn). Jerry's neighbor, Kramer, conceptualizes "a pizza place where you make your own pie". The episode was written by Larry David and Jerry Seinfeld, and was the first taped episode directed by Tom Cherones. This was the first episode produced with Elaine Benes (Julia Louis-Dreyfus) as a character. It also was the first episode to use Jonathan Wolff's title music. This is the only Seinfeld episode whose title does not begin with the definite article "The".

## Plot

George tells Jerry that he was out with a girlfriend. They went to see a play, during which he put his hand in his pocket to get some money and accidentally got some dental floss stuck to his hand. George worries that his girlfriend is going to leave him because of it.

Jerry has problems with a childhood friend, Joel Horneck, who persists in keeping in touch with him. He does not like Horneck, who does not pay attention to anything that Jerry says. Jerry says that he feels uncomfortable "breaking up with" Horneck, so George suggests that he should pretend that Horneck is a woman and break up normally. Jerry therefore attempts to break up with Horneck at Monk's Café, but when he does so, Horneck bursts into tears. Deeply uncomfortable, Jerry assures Horneck he didn't mean it, and agrees to take him to see the New York Knicks, although he was supposed to take George.

As George tells Jerry that his girlfriend no longer wants to see him, Jerry tells George that he gave away his ticket to Horneck. George does not go to the game because he does not know Horneck, so Jerry decides to give Horneck both tickets, claiming that he cannot make the game because he is tutoring his nephew. Later, when talking to his ex-girlfriend Elaine, she tries to provide Jerry with a list of excuses to help him avoid Horneck. He later discovers that Horneck took Kramer to the game and that Horneck is in the building. When Horneck meets Jerry and Elaine, Horneck invites them out to another Knicks game. They come up with more unusual excuses in an attempt to avoid going out. However, Horneck then gets out a newspaper and tries to organize a time when they can all meet, weeks in advance. Jerry realizes that no matter what excuses he comes up with, he cannot avoid Horneck.

Kramer, working under the name "Kramerica Industries", conceptualizes building "a pizza place where you make your own pizza pie". Jerry and George try to persuade Kramer to forget the idea, but Kramer is determined to go on with it. Kramer's pizza parlor idea reappears in later episodes such as "The Puffy Shirt" in season 5, and "The Couch" in season 6.

## Production

This is the first episode that was made after the original pilot, "The Seinfeld Chronicles". The title of the series was shortened to Seinfeld to avoid confusion with another sitcom called The Marshall Chronicles.

This is the only episode that does not have "the" in the title. A decision was made to name all the episodes in this way so that the writers would not waste time trying to think of funny titles and instead make the content of the episode funny. However, this decision was made after the script for "Male Unbonding" was completed. Jerry Seinfeld tried to have the title of the episode changed to "The Male Unbonding" some time later, but was unsuccessful.

This is the first episode written which stars the character of Elaine. The first version of the script does not include Elaine, despite the fact that one of the conditions given when Seinfeld was given a series was that a female character was included. Originally, the character's name was Eileen. Louis-Dreyfus claims that she was unhappy with only being given one scene in the first episode in which she appeared, but said that she performed well in the episode. Similarly, other early versions of the script refer to the character of Kramer as "Breckman". Kevin Dunn, who plays Joel in the episode, auditioned for the role of George Costanza in the original pilot. The episode also stars Anita Wise, who plays a waitress. Wise appeared again in another episode from the first season of Seinfeld titled "The Robbery". Frank Piazza, a customer at the bank appears in the season 2 episode "The Stranded".

This episode features different title music from the pilot; this music, composed by Jonathan Wolff, is used throughout the rest of the series. The standup interstitials for this episode were recorded twice. Originally, the set for the interstitials was brightly lit and was designed to look like that of a church basement, but then it was remade to look like a nightclub and the material was performed again. The scene that was set in the bank was originally set in a dry cleaner's. However, this was moved and some of the material was moved to a later episode called "The Stock Tip".

The episode had an alternative ending, in which Joel borrows a k.d. lang tape from Jerry. Jerry then finally manages to "break up" with Joel. Joel leaves, but then comes back again to tell Jerry that he will bring back the tape. Jerry then refers to Joel as Jason from the Friday the 13th films. "Male Unbonding" was filmed on February 13,1990.

## Reception

When first broadcast on June 14, 1990, the episode attracted a Nielsen rating of 13.6/24, meaning that 13.6% of American households watched the episode, and that 24% of all televisions in use at the time were tuned into it. Several reviews at the time compared Seinfeld to It's Garry Shandling's Show, in which Garry Shandling, like Seinfeld, plays a fictionalized version of himself.

Jonathan Boudreaux writes that of the four season one episodes produced after the pilot, "'Male Unbonding' is the strongest. This episode centers on the classic Seinfeld theme of the gang complaining about an outsider's self-centeredness while conveniently ignoring their own selfish, antisocial behavior. The characters slowly begin to fall into place as George takes great strides toward being the neurotic moron we love, and Kramer becomes more spastic and idiosyncratic."

Colin Jacobson for DVD Movie Guide was also positive, saying, "'Unbonding' marks a demonstrable improvement over the pilot. No one will mistake the episode for one of the series' greats, but at least the characters start to resemble the ones we'd come to know later. In addition, it tosses out just enough humor to make it enjoyable." David Sims of The A.V. Club gave the episode a B+, saying, "It's a pretty funny episode – my main criticism is just that at this point, Kramer isn't integrated at all into the stories, rather he just comes by and dispenses weird dialog for a couple minutes."

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Guests

Kevin Dunn ......................... Joel  
Anita Wise ............................ Waitress  
Frank Piazza ........................ Customer  
Kimberley LaMarque ......... Teller

## Script

INT. COMEDY CLUB - NIGHT

(Jerry is on stage, performing.)

JERRY: Most men like working on things. Tools, objects, fixing things. This is what men enjoy doing. Have you ever noticed a guy's out in his driveway working on something with tools, how all the other men in the neighborhood are magnetically drawn to this activity. They just come wandering out of the house like zombies. Men, it's true, men hear a drill, it's like a dog whistle. Just... (his head perks up) you know, they go running up to that living room curtain, "Honey, I think Jim's working on something over there." So they run over to the guy. Now they don't actually help the guy. No, they just want to hang around the area where work is being done. That's what men want to do. We want to watch the guy, we want to talk to him, we want to ask him dumb questions. You know, "What are you using, a Phillips-head?" You know, we feel involved. That's why when they have construction sites, they have to have those wood panel fences around it, that's just to keep the men out. They cut those little holes for us so we can see what the hell is going on. But if they don't cut those holes, we are climbing those fences. Right over there. "What are you using the steel girders down there? Yeah, that'll hold."

INT. JERRY'S LOBBY - DAY

(Jerry and George are waiting for the elevator.)

GEORGE: I had to say something. (chuckles) I had to say something. Everything was going so well. I had to say something.

JERRY: I don't think you did anything wrong.

GEORGE: I told her I liked her. Why? Why did I tell her I like her? I have this sick compulsion to tell women how I feel. I like you I don't tell you.

JERRY: We can only thank God for that.

(Elevator opens. They get on.)

GEORGE: I'm outta the picture. I am outta the picture. (laughs) It's only a matter of time now.

JERRY: You're imagining this. Really.

GEORGE: Oh, no. No no no no.

(Elevator doors close.)

INT. ELEVATOR - CONTINUING

GEORGE: I'll tell you when it happened. When that floss came flying out of my pocket.

JERRY: What floss? When?

GEORGE: We were in the lobby during the intermission of the play. I was buying her one of those containers of orange drink, for five dollars. I reached into my pocket to pay for it, I looked down; there's this piece of green floss hanging from my fingers.

JERRY: Ah, mint.

GEORGE: Of course. So, I'm looking at it. I look up, I see she's looking at it. Our eyes lock. It was a horrible moment. I just..

(Elevator doors open. They get off.)

JERRY: So let me get this straight. She saw the floss, you panicked and you told her you liked her.

GEORGE: If I didn't put that floss in my pocket, I'd be crawling around her bedroom right now looking for my glasses.

(They approach Jerry's apartment.)

JERRY: And you're sure the floss was the catalyst?

GEORGE: Yes, I am.

JERRY: You don't think it might've had anything to do with that?

(Jerry points to George's fanny pack.)

GEORGE: What? You don't like this?

JERRY: It looks like your belt is digesting a small animal.

(They enter Jerry's apartment)

INT. JERRY'S APARTMENT - CONTINUING

(Kramer is on the couch, talking on the phone.)

KRAMER: (to the phone) Oh, they've got a cure for cancer. See, it's all big business. Oh hey, Jerry just walked in. Hi, George. (to the phone again) Yeah yeah yeah yeah take my number. 555-8643. Okay, here he is.

(Kramer hands the phone to Jerry.)

JERRY: (to Kramer) Who is it?

KRAMER: Take it.

JERRY: Who is it?

KRAMER: It's for you.

JERRY: (to the phone) Hello? (disappointed) Oh, hi Joel. (Jerry hits Kramer with a magazine.) No. Uh, I was out of town. I just got back. Kramer doesn't know anything. He's just my next-door neighbor. Uh, nothing much... Tuesday? Uh, Tuesday, no. I'm meeting somebody... Uh, Wednesday? Wednesday's okay... Alright. Uh, I'm a little busy right now. Can we talk Wednesday morning?... Okay... Yeah... Right... Thanks... Bye. (Jerry hangs up; to Kramer) Why did you put me on the phone with him? I hate just being handed a phone.

KRAMER: Well, it's your phone. He wanted to talk to you.

JERRY: Maybe I didn't want to talk to him.

KRAMER: Well, why not?

JERRY: He bothers me. I don't even answer the phone anymore because of him. He's turned me into a screener. Now I gotta go see him on Wednesday.

GEORGE: What do you mean Wednesday? I though we had tickets to the Knick game Wednesday. We got seats behind the bench! What happened? We're not going?

JERRY: We're going. That's next Wednesday.

GEORGE: Oh. Who is this guy?

JERRY: His name is Joel Horneck. He lived like three houses down from me when I grew up. He had a Ping Pong table. We were friends. Should I suffer the rest of my life because I like to play Ping Pong? I was ten. I would've been friends with Stalin if he had a Ping Pong table. He's so self-involved.

(Kramer's cordless phone rings. Kramer pulls it out of his pocket.)

KRAMER: That's for me. (to the phone) Kramerica Industries. Oh, hi, Mark. No no no. Forget that. I got a better idea. A pizza place where you make your own pie...

JERRY: Can you conduct your business elsewhere?

KRAMER: (ignoring Jerry) No no no. I'm talking about a whole chain of 'em. Yeah.

(Kramer leaves Jerry's apartment while still on the phone.)

GEORGE: I don't know why you even bother with this ping pong guy, I'll tell you that.

JERRY: I don't bother with him. He's been calling me for seven years. I've never called him once! He's got the attention span of a five-year-old. Sometimes I sit there and I make up things just to see if he's paying attention.

GEORGE: I don't understand why you spend time with this guy.

JERRY: What can I do? Break up with him? Tell him, "I don't think we're right for each other." He's a guy! At least with a woman, there's a precedent. You know, the relationship goes sour, you end it.

GEORGE: No no no no, you have to approach this as if he was a woman.

JERRY: Just break up with him?

GEORGE: Absolutely. You just tell him the truth.

JERRY: The Truth.

(They both careen)

INT. COMEDY CLUB - NIGHT

(Jerry is on stage, performing)

JERRY: As a guy I don't know how I can break up with another guy. You know what I mean? I don't know how to say, "Bill, I feel I need to see other men." Do you know what I mean? There's nothing I can do. I have to wait for someone to die. I think that's the only way out of this relationship. It could be a long time. See, the great thing about guys is that we can become friends based on almost nothing. Just two guys will just become friends just because they're two guys. That's almost all we need to have in common. 'Cause sports - sports and women - is really all we talk about. If there was no sports and no women the only thing guys would ever say is, "So, what's in the refrigerator?"

INT. MONK'S DINER - DAY

(Jerry and Joel are sitting at a table.)

JOEL: ...so my shrink wants me to bring my mother in for a session. This guy is a brilliant man. Lenny Bruce used to go to him. And I think, uh, Geraldo.

JERRY: You know, I read the Lenny Bruce biography, I thought it was really... interesting. He would-

JOEL: (to the counter of the restaurant) Hey hey hey hey, we're starving here! We've been waiting here for ten minutes already!

JERRY: (testing Joel) So, I'm thinking about going to Iran this summer.

JOEL: I have to eat! I mean, I'm hypoglycemic.

JERRY: Anyway, the Hezbollah has invited me to perform. (Joel shakes his head agreeing; Jerry smiles) You know, it's their annual terrorist luncheon.

JOEL: Yeah.

JERRY: (cont'd) I'm gonna do it in Farsi.

JOEL: Do you think I need a haircut?

(A waitress approaches their table.)

CLAIRE: Are you ready?

JERRY: Yeah, I'll have the egg salad on whole wheat.

JOEL: (to the waitress) Let me ask you a question. This, uh, this turkey sandwich here, is that real turkey, or is it a turkey roll? I don't want that processed turkey. (to Jerry) I hate it.

WAITRESS: I think it's real turkey.

JOEL: Is there a real bird in the back?

WAITRESS: No, there's not bird but-

JOEL: Well, how do you know for sure? Look, why don't you do me a favor. Why don't you go in the back and find out, okay? (The waitress leaves.) Unbelievable.

JERRY: How can you talk to someone like that?

JOEL: What are you saying? What, you like turkey roll?

JERRY: Listen, Joel. There's something I have to tell you...

JOEL: (laughing) Wait, you'll never guess who I ran into.

(Joel pushes out his ears as a hint to Jerry.)

JERRY AND JOEL: Howard Metro.

JOEL: He asked me if I still saw you. I said, "Sure, I see him all the time. We're still great friends." Anyway, Howard says hello. (laughs)

JERRY: Listen, Joel, I don't think we should see each other anymore.

JOEL: What?

JERRY: This friendship: It's not working.

JOEL: Not working? What are you talking about?

JERRY: We're just not suited to be friends.

JOEL: How can you say that?

JERRY: Look, you're a nice guy, it's just that... we don't have anything in common.

JOEL: (starting to cry) Wai-Wait. What did I do? Tell me. I want to know what I did.

JERRY: Y-You didn't do anything. It's not you, it's me. It's- this is very difficult.

JOEL: Look, I know I call you too much, right? I mean, I know you're a very busy guy.

JERRY: No, it's not that.

JOEL: (crying) You're one of the few people I can talk to.

JERRY: Oh, come on. That's not true.

JOEL: I always tell everybody about you. Tell everybody to (to the rest of the coffee shop) GO SEE HIS SHOW! (to Jerry) I mean, I'm your biggest fan!

JERRY: I know, I know.

JOEL: I mean, you're my best friend.

JERRY: Best friend- I've never been to your apartment.

JOEL: I cannot believe that this is happening. I can't believe it.

JERRY: Okay, okay. Forget it. It's okay. I didn't mean it.

JOEL: Didn't mean what?

JERRY: What I said. I've been under a lot of stress.

JOEL: Oh, you've been under a lot of stress.

JERRY: Just, can we just forget the whole thing ever happened? I'm sorry. I didn't mean it. I took it out on you. We're still friends. We're still friends. Still friends. Okay? Look, I'll tell you what. I've got Knick tickets this Wednesday. Great seats behind the bench. You want to come with me? Come on.

(Joel's tears are clearing up.)

JOEL: Tonight?

JERRY: No, next Wednesday. If it was tonight, I would've said tonight.

JOEL: Do you really want me to go?

JERRY: (lying) Yes.

JOEL: Okay. (Jerry gives him some napkins to clean himself up) Yeah, okay. Great! That would be, that'd be great. So, next Wednesday.

JERRY: Next Wednesday.

JOEL: Where is that waitress? (to the counter) Hey!

INT. BANK - DAY

(Jerry is at the counter, filling out a slip. George is carrying a jug full of change.)

GEORGE: ...she calls me up at my office

JERRY: Yeah.

GEORGE: (cont'd) she says, "We have to talk."

JERRY: Ugh, the four worst words in the English language.

GEORGE: That, or "Who's bra is this?"

JERRY: That is worse.

GEORGE: So we order lunch, and we're talking. Finally, she blurts out how it's 'not working'.

JERRY: Really.

GEORGE: So, I'm thinking, as she's saying this, I'm thinking: great, the relationship's over. But the egg salad's on the way. So now I have a decision: Do I walk or do I eat?

JERRY: Hm. You ate.

GEORGE: We sat there for twenty minutes, chewing, staring at each other in a defunct relationship.

JERRY: Someone says, "Get out of my life," and that doesn't affect your appetite?

GEORGE: Have you ever had their egg salad?

JERRY: It is unbelievable.

GEORGE: It's unbelievable. You know what else is unbelievable? I picked up the check. She didn't even offer. She ended it. The least she could do is send me off with a sandwich.

(Jerry looks at George's jug of change.)

JERRY: How much could you possibly have in there?

GEORGE: It's my money. What should I do? Throw it out the window? I know a guy who took his vacation on his change.

JERRY: Yeah? Where'd he go, to an arcade?

GEORGE: (sarcastically) That's funny. You're a funny guy.

JERRY: C'mon, move up.

(George moves up in the bank line. There's a customer in front of him.)

CUSTOMER: Oh great, Ewing's hurt.

GEORGE: Ewing's hurt? How long is he going to be out?

CUSTOMER: A couple of days at the most but...

GEORGE: Geez.

JERRY: Oh, God.

GEORGE: I got scared there for a second. The Knicks without Ewing.

JERRY: Listen, George, little problem with the game.

GEORGE: What about it?

JERRY: The thing is, yesterday, I kind of.. uh..

GEORGE: What?

JERRY: I gave your ticket to Horneck.

GEORGE: (not believing him) You what?!

JERRY: Yeah, I'm sorry. I had to give it to Horneck.

GEORGE: No! My ticket?! You gave my ticket to Horneck?

JERRY: C'mon, c'mon, go ahead, move up.

GEORGE: Why did you give him my ticket for?

JERRY: You didn't see him. It was horrible.

GEORGE: Oh, c'mon, Jerry. I can't believe this.

JERRY: I had to do it.

(George is up to the teller. Jerry goes to another one.)

GEORGE: Oh, please. (to the teller) Can you change this into bills?

TELLER: I'm sorry, sir. We can't do that.

JERRY: Do you want to go with him? You go. I don't mind.

GEORGE: I'm not going with him. I don't even know the guy. (to the teller) Look, they did this for me before.

TELLER: Look, I can give you these and you can roll them yourself.

GEORGE: You want me to roll six thousand of these?! What, should I quit my job?!

INT. COMEDY CLUB

(Jerry is on stage, performing.)

JERRY: No, I do not like the bank. I've heard the expression "Laughing all the way to the bank." I have never seen anyone actually doing it. And those bank lines. I hate it when there's nobody on the line at all, you know that part, you go to the bank, it's empty and you still have to go through the little maze. (walking on the stage like he is going through a maze) "Can you get a little piece of cheese for me? I'm almost at the front. I'd like a reward for this please."

INT. JERRY'S APARTMENT - DAY

(George is stuffing pennies into rolls.)

GEORGE: ...thirty-two, thirty-three-

JERRY: George.

(George puts up his hand.)

GEORGE: Not now.

(George counts to himself.)

JERRY: Could you stop the counting?

(George loses count.)

GEORGE: Nnnnnnngaaa!

(He dumps out roll.)

GEORGE: What?!

JERRY: Can I make it up to you? I'll give you fifty bucks for the jug.

GEORGE: Oh, yeah, sure. Keep your money.

JERRY: Well, then I'm not going to the game either. Okay? I'll give him both tickets.

GEORGE: Oh geeeee. (George pantomimes sticking a knife in his heart, and twists it.) Go, go!

JERRY: I- no, I don't want to go.

GEORGE: He was really crying?

JERRY: I had to give him a tissue. In fact, let me call his machine now and I'll just make up some excuse why I can't go to the game either.

GEORGE: Wait a minute. Wait a minute. As long as you're going to lie to the guy, why don't you tell him that you lost both of the tickets, then we can go?

JERRY: George, the man wept.

(Kramer enters.)

KRAMER: Oh, hey guys. Man, I'm telling you, this pizza idea, is really going to happen.

GEORGE: This is the thing where you go and you have to make your own pizza?

KRAMER: Yeah, we give you the dough, you smash it, you pound it, you fling it in the air. And then you get to put your sauce and you get to sprinkle your cheese, and then- you slide it into the oven.

GEORGE: You know, you have to know how to do that. You can't have people shoving their arms into a six-hundred degree oven!

KRAMER: It's all supervised.

GEORGE: Oh, well...

KRAMER: All of it. You want to invest?

GEORGE: My money's all tied up in change right now.

KRAMER: No, I'm tellin' you, people, they really want to make their own pizza pie.

JERRY: I-I have to say something. With all due respect, I just never- I can't imagine anyone in any walk of life, under any circumstance, wanting to make their own pizza pie... but that's me. Alright

KRAMER: That's you.

JERRY: I'm just saying..Alright.

KRAMER: Okay, okay. I just wanted to check with you guys.

JERRY: Okay.

KRAMER: You know, this business is going to be big. I just wanted- okay.

(Kramer exits quickly, then sticks his head back through the door.)

KRAMER: One day, you'll beg me to make your own pie.

(Kramer exits for good. Jerry dials Joel's number.)

JERRY: (to the phone) Hi, Joel. This is Jerry. I hope you get this before you- Oh, Hi. Joel... Oh, you just came in... Listen, I can't make it to the game tonight... I, uh, have to tutor my nephew... Yeah, he's got an exam tomorrow... Geometry... You know, trapezoids, rhombus... Anyway, listen, you take the tickets. They're at the Will-Call window... And I'm really sorry... Have a good time... We'll talk next week... Okay... Yeah, I don't... Fine.. Fine... Bye.

(He hangs up.)

GEORGE: Trapezoid?

JERRY: I know. I'm really running out of excuses with this guy. I need some kind of excuse rolodex.

INT. JERRY'S APARTMENT - NIGHT

(Elaine and Jerry are there.)

ELAINE: Come on, let's go do something. I don't wanna just sit around here.

JERRY: Okay.

ELAINE: Want to go get something to eat?

JERRY: Where do you want to go?

ELAINE: I don't care, I'm not hungry.

JERRY: We could go to one of those uh cappuccino places. They let you just sit there.

ELAINE: What are we gonna do there? Talk?

JERRY: We could talk.

ELAINE: I'll go if I don't have to talk.

JERRY: Then we'll just sit there.

ELAINE: Okay. I'm gonna check my machine first. (Elaine sees a pad of paper by the phone; reading) Picking someone up at the airport, jury duty, waiting for cable guy...

JERRY: Okay, just hand that over, please.

ELAINE: Oh, what is this?

JERRY: It's a list of excuses, it's for that guy, Horneck, who's at the game tonight with my tickets. I have that list now so in case he calls, I just consult it and I don't have to see him. (Elaine laughs.) I need it. (Elaine starts writing on the list.) What are you doing?

ELAINE: I got some for you.

JERRY: I don't need any more.

ELAINE: No no no no no, these are good. Listen, listen: You ran out of underwear, you can't leave the house.

JERRY: (not amused) Very funny.

ELAINE: How about: You've been diagnosed as a multiple personality. You're not even you. You're Dan.

JERRY: I'm Dan. Can I have my list back, please?

(Elaine gives Jerry the list.)

ELAINE: Here, here. Jerry Seinfeld, I cannot believe you're doing this. This is absolutely infantile.

JERRY: What can I do?

ELAINE: Deal with it. Be a man!

JERRY: Oh no. That's impossible. I'd rather lie to him for the rest of my life that go through that again. He was crying. Tears. Accompanied by mucus.

ELAINE: You made a man cry? I've never made a man cry. I even kicked a guy in the groin once and he didn't cry. I got the cab.

JERRY: Couple of tough monkeys.

(Elaine laughs. Kramer enters.)

KRAMER: Oh, hi Elaine, hey. (to Jerry) Hey, you missed a great game tonight, buddy!

JERRY: Game?

KRAMER: Knick game. Horneck took me. We were sitting two rows behind the bench. We're getting hit by sweat!

JERRY: Wait. How does Horneck know you?

KRAMER: Last week. When I, you know, gave you the phone. He's really into my pizza place idea!

JERRY: This is too much.

ELAINE: Wait, what pizza place idea?

JERRY: Oh, no.

KRAMER: You get to make your own pie!

ELAINE: Oh, that sounds like a great idea. It would be fun.

KRAMER: Yea.

JOEL: (from the hallway) Kramer.

KRAMER: Yeah.

JERRY: Perfect.

(Joel enters.)

JOEL: Hey.

KRAMER: Okay, who wants meatloaf?

JERRY & ELAINE: No thanks.

KRAMER: (to Joel) It's gonna be hot in a minute.

(Kramer exits.)

JOEL: So, I though you were tutoring your nephew?

JERRY: Oh, we finished early.

JOEL: M-hm, I'll bet. So, are you going to introduce me to your 'nephew'?

(Elaine laughs)

JERRY: Elaine Benes, this is Joel Horneck.

ELAINE: Hi.

JOEL: Whoa, Nelson! This is Elaine? I though you guys split?

JERRY: We're still friends.

JOEL: So, thanks again for those tickets. But next week, I'm going to take you. How about next Tuesday night? (to Elaine) And why don't you come along?

ELAINE: Oh, no no. Tuesday's uh no good because we've got choir practice.

JERRY: Right. I forgot about choir.

ELAINE: We-We're doing that evening of Eastern European National Anthems.

JERRY: Right. You know, the Wall being down and everything.

JOEL: What about Thursday night? I mean they're playing the Sonics.

(Jerry shakes his head.)

ELAINE: Huh... Thursday is no good because we've got to get to the hospital to see if we qualify as those organ donors.

JOEL: You know, I should really try something like that.

JERRY: You really should.

JOEL: Well, let's just take a look here.

(Horneck looks at his schedule.)

JOEL: Forty-one home games. Let's see saturday night we've got the Mavericks. If you don't like the Mavericks, next Tuesday: Lakers. I mean, you gotta like Magic, right? Let's see, on the road, on the road, on the road, on the road, back on the fourteenth. They play the Bulls. You can't miss Air Jordan...

INT. COMEDY CLUB - NIGHT

(Jerry is on stage, performing.)

JERRY: You know, I really... I've come to the conclusion that there are certain friends in your life that they're just always your friends, and you have to accept it. You see them, you don't really wanna see them. You don't call them, they call you. You don't call back, they call again. The only way to get through talking with people that you don't really have anything in common with is to pretend you're hosting your own little talk show. This is what I do. You pretend there's a little desk around you. The only problem with this is there's no way you can say, "Hey, it's been great having you on the show. We're out of time."

The End
