---
layout: default
title: The Sponge
parent: Season 7
nav_order: 9
permalink: /the-sponge
cat: ep
series_ep: 119
pc: 709
season: 7
episode: 9
aired: December 7, 1995
written: Peter Mehlman
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697783/
wiki: https://en.wikipedia.org/wiki/The_Sponge
---

# The Sponge

| Season 7 - Episode 9     | December 7, 1995          |
|:-------------------------|:--------------------------|
| Written by Peter Mehlman | Directed by Andy Ackerman |
| Series Episode 119       | Production Code 709       |

"The Sponge" is the 119th episode of the NBC sitcom Seinfeld. This was the ninth episode for the seventh season. It aired on December 7, 1995.

## Plot

Jerry, Elaine, and Kramer are at Monk's Café and they mention that Jerry still wears a size 31 pair of jeans. Kramer also mentions the female contraceptive sponge is being taken off the market. Kramer passes both of them an AIDS Walk sheet for them to sign. While reading the list of signatures Jerry sees the name of a girl (Lena, played by Jennifer Guthrie) whom he once met, but does not know her unlisted number. Jerry takes down the number and calls the girl. George and his fiancée Susan get into a fight about sharing their secrets; in order to get back into her good graces, George tells her that Jerry does not actually wear size 31 jeans.

Meanwhile, Elaine goes on a 25-block radius hunt to find the contraceptive sponges. When she finally arrives at a pharmacy which still carries them, she purchases a full case at Pasteur Pharmacy from a pharmacist named Roger Hoffman (David Byrd), who acts a little suspicious since Elaine wants a whole case. Her limited supply requires that she restrict her usage. She puts her current boyfriend, Billy (Scott Patterson), through a rigorous examination to make sure he is "sponge-worthy". Jerry tells George that he found his new girlfriend on the AIDS Walk list. George then tells Susan against Jerry's wishes. Susan then tells her friend, who tells a friend until the phone tree reaches Lena. When George comes to Jerry's apartment, Jerry tells him that he is "out of the loop" because he told Susan about Jerry. When Jerry later learns from Lena that she doesn't mind him taking her number from the AIDS Walk list, he gets turned off from her being "too good" and that he doesn't want to be with someone who is "giving and caring and genuine concerned about the welfare of others."

When Kramer takes part in the AIDS Walk, he refuses to wear an AIDS ribbon in opposition to "ribbon bullies", led by Bob and Cedric from "The Soup Nazi" episode. (The storyline appeared to be based on the real-life controversy of former Days of Our Lives actress Deidre Hall, who (in 1993) publicly refused to wear AIDS ribbons at public events, such as the Daytime Emmys. Hall claimed that the volunteers who passed out the ribbons bullied celebrities into wearing the ribbons.) When they and several walkers confront Kramer about the ribbon, he states he doesn't want to wear it since he lives in America, culminating in the group to attack Kramer; he attempts to flee up a fire escape, but is dragged down and attacked.

At Lena's, Jerry finds out that she has a lifetime supply of contraceptive sponges (assuming the boxes shown are all she has, there are approximately 720 sponges) and realizes that "she is depraved." While trying to disguise that he has seen the sponges, he tells her his secret about his jeans and she dumps him - she figures he is not "sponge-worthy". Later at the AIDS Walk, Jerry and George see Kramer (disheveled after getting attacked by the "ribbon bullies") stumble across the finishing line; Jerry, however, assumes this was because Kramer had exhausted himself (from running up several flights of stairs and having a poker game the night before the Walk). George then asks Kramer where his ribbon is, causing Kramer to look up at him in despair.

At Elaine's, she presumably ends her relationship with Billy since she still plans on conserving her sponges.

## Original story ideas

When Peter Mehlman originally wrote this episode, it included several storylines which never made it into the final episode. These included a story in which Kramer and Newman bought stock in a company that sells over-the-counter contraceptive devices because Kramer had heard that the sponge was going out of business, and that this other company would capitalize on this. Then, in order to improve on their investment, Kramer and Newman go to Wall Street in order to start a buyout rumor on their company by whispering loudly about it at brokerage houses, so that their stock would go five or 10 points before they sell. It was at this point in the show's original story that Kramer shared his scheme with Elaine and told her about the sponge; she immediately panics and rushes off to buy as many sponges as possible. However their plan backfires when they have a fight with an obnoxious guy in an elevator at Wall Street and a negative rumor about the company they've invested in is immediately generated.

George's story was originally different too, and was later modified to fit in with season seven's story arc of his engagement to Susan. In its original conception George meets a girl who argues that she and George are so like each other, that if they dated, they would have great sex for about ten days and then hate each other and split up. George, seizing the opportunity, wonders out loud if there would be any problem if they stayed with each other for a week and then, by mutual consent, they would call it quits. That way they could have all the pleasure without any of the pain. George is on the edge of victory when he learns that the girl likes a certain type of contraceptive and the entire West Side has been cleared of the product by Elaine.

Jerry's story was much the same as it is in the final episode, except when his girlfriend asks him if he donates to AIDS charities, he says he does and blurts out the name of Kramer and Newman's company. Jerry's girlfriend, believing in his ramblings about this terrific company and its AIDS research capacity, buys 5,000 shares, just in time for the negative rumors to take hold, and Kramer and Newman's stock's price falls through the floor.

## Option pricing

A paper by Avinash Dixit used this episode to explore an option value problem in determining the "spongeworthiness" of potential partners.

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Cosmo Kramer

#### Recurring

Heidi Swedberg ............ Susan Biddle Ross

#### Guests

Jennifer Guthrie .................. Lena  
Scott Patterson .................... Billy  
John Paragon ...................... Cedric  
Yul Vazquez ........................ Bob  
David Byrd ......................... Roger  
Ileen Getz ........................... Organizer  
Steven Hack ....................... Walker #1  
Wren T. Brown .................. Walker #2  
P.B. Hutton ........................ Walker #3  
Susan Moore ...................... Monica

## Script

[Opening Monologue]

I have a friend, wears eyeglasses, no prescription in the glasses because he thinks it makes him look more intelligent, now why? Why do we think that glasses makes us look more intelligent? Is it from the endless hours of reading and studying and researching that this person supposedly blew out their eyeballs, and that's why they need the glasses? It's just a corrective device. If you see someone with a hearing aid, you don't think, 'Oh, they must have been listening real good... yeah, to a lot of important stuff...' No, they are deaf. They can't hear.

[Jerry and Elaine in a booth at Monk's. Kramer comes over from the counter with a clipboard in his hand.]

KRAMER: Hey, boys and girls. I need you both to sponsor me in the AIDS walk.

ELAINE: Is that tomorrow?

KRAMER: Yeah, yeah, so... git-git... (gestures to Elaine to sign the form.)

ELAINE (signing): Well, I admire you for joining the fight against AIDS.

KRAMER: Well, if I didn't do something I wouldn't be able to live with myself.

JERRY (signing): It's hard enough living next door.

KRAMER: I tell ya, there's some people, they just wear a ribbon and they think they're doin' something? Not me. I talk the talk, and I walk the walk, baby. (gets up) I'll be right back.

(Jerry stands up and gets a bottle of ketchup from the next table.)

ELAINE: New jeans?

JERRY: Yeah.

ELAINE: Still a 31 waist?

JERRY: Yep. Since college. (Looks at Kramer's AIDS walk list.) Hey, Lena Small's on this list.

ELAINE: Lena Small?

JERRY: Yeah, that girl I was gonna call for a date, she was unlisted... and now here's her number.

ELAINE: Oh, you're not gonna cop a girl's phone number off an AIDS charity list!

JERRY (copying down the number): Elaine, you should admire me... I'm aspiring to date a giving person.

ELAINE: You're a taking person.

JERRY: That's why I should date a giving person. If I date a taking person, everyone's taking, taking, taking, no one's giving - it's bedlam.

ELAINE (warns): She's gonna ask how you got her number.

JERRY: Oh, I'll tell her I met some guy who knew her and he gave it to me.

ELAINE: What's he look like?

JERRY: I really didn't pay much attention, I'd just come from buying a speedboat.

ELAINE: You're buying a speedboat?

JERRY: See, we're already off the subject of how I got her number. (Elaine laughs.) All I gotta do is get past the first phone call and I'm home free.

ELAINE: I don't know about that.

JERRY: So if Billy had gotten your number off the AIDS walk list, you wouldn't have gone out with him?

ELAINE: Well...

JERRY: Yeah. So you really like this guy.

ELAINE: Very much.

JERRY: How's the... sexual chemistry?

ELAINE: Haven't been in the lab yet. But I am birth control shopping today. (Kramer overhears as he returns to the booth.)

KRAMER: Are you still on the pill?

ELAINE: Uh, Kramer...

KRAMER: I'll tell ya, I think birth control should be discussed in an open forum.

ELAINE: The sponge, o.k.? The Today sponge.

KRAMER: But wasn't that taken off the market?

ELAINE: Off the market? The sponge? No, no... no way. Everybody loves the sponge.

KRAMER: I read it in Wall Street Week ...Louis, uh, Rukeyser.

(Elaine laughs.)

[Jerry on the phone in his apartment.]

JERRY: Hello, Lena? Hi, it's Jerry Seinfeld. How did I get your number? I met a guy that knows you, he gave it to me... I don't remember his name. Think it began with a W, maybe a Q. I wasn't paying that much attention, I'd just come from shopping for a speedboat...

[George and Susan in George's car.]

SUSAN: You know, I really like those new jeans Jerry was wearing. He's really thin.

GEORGE: Not as thin as you think.

SUSAN: Why? He's a 31. I saw the tag on the back.

GEORGE: The tag, huh?

SUSAN: Mmm-hmm.

GEORGE: Let me tell you something about that tag. It's no 31, and uh... let's just leave it at that.

SUSAN: What are you talking about?

GEORGE: He scratches off a 32 and he puts in 31.

SUSAN: Oh, how could he be so vain?

GEORGE: Well, this is the Jerry Seinfeld that only I know. I can't believe I just told you that.

SUSAN (laughing): Why not?

GEORGE: Well, Jerry doesn't want anyone to know.

SUSAN: Well, it's alright, I'm your fiance. Everyone assumes you'll tell me everything.

GEORGE: Where did you get that from?

SUSAN: Well, we're a couple. It's understood.

GEORGE: I never heard of that.

SUSAN: Well, you've never been a couple.

GEORGE: I've coupled! I've coupled!

SUSAN: Keeping secrets! This is just like your secret bank code.

GEORGE: This is totally different! That was my secret, this is Jerry's secret! There's...there's attorney-client privileges here! If I play it by your rule, no one'll ever confide in me again, I'll be cut out of the loop!

[George arrives at Jerry's apartment.]

GEORGE: Hey.

JERRY: Hey. What's the matter?

GEORGE: I had a fight with Susan.

JERRY: What about?

GEORGE: Oh... (is about to tell Jerry, but reconsiders) ...clothing, something, I dunno. So, uh, what are you doing today?

JERRY: I got a date with that girl, Lena.

GEORGE: Lena, how'd you meet her?

JERRY: I actually met her a few weeks ago, but... (Jerry stops, and mentally visualizes George telling Susan about how Jerry got Lena's number from the AIDS list... then Susan passing the information along to Monica on the phone at work.)

GEORGE: You met her a few weeks ago, but...?

JERRY (slowly): I didn't call her till today.

GEORGE: So, uh... wanna double?

JERRY: What?

GEORGE: Well, I just had a fight - I need a group dynamic.

JERRY: I dunno. (Elaine enters.) Hey.

ELAINE: Well, Kramer was right. My friend Kim told me the sponge is off the market.

JERRY: So what are you gonna do?

ELAINE: I'll tell you what I'm gonna do - I'm gonna do a hard-target search. Of every drug store, general store, health store and grocery store in a 25-block radius.

GEORGE: Just for these sponges?

ELAINE: Hey man, women are really loyal to their birth control methods. What does Susan use?

GEORGE: I dunno.

ELAINE: You don't know?

GEORGE: I, uh... figure it's something. (Kramer enters.)

JERRY: What are you all out of breath from?

KRAMER (panting): The elevator just broke. I had to walk up five flights.

JERRY: And you got the AIDS walk tomorrow. You're never gonna make it, you're in horrible shape.

KRAMER: Hey, I'm in tip-top shape. Better than you!

JERRY: I got a 31 waist, mister!

KRAMER: Yeah, well I'm walking for charity, what are you doing?

JERRY (proudly): What am I doing? I'm... dating a woman who happens to be sponsoring one of these walkers.

[A musical montage of Elaine's &quot;hard-target search&quot; - visiting stores and pharmacies all over town and not finding a sponge anywhere. She ends up at the Pasteur Pharmacy.]

PHARMACIST: Can I help you?

ELAINE (with little hope): Yeah, do you have any Today sponges? I know they're off the market, but...

PHARMACIST: Actually, we have a case left.

ELAINE (excited): A case! A case of sponges? I mean, uh... a case. Huh. Uh... how many come in a case?

PHARMACIST: Sixty.

ELAINE: Sixty?! Uh... well, I'll take three.

PHARMACIST: Three.

ELAINE: Make it ten.

PHARMACIST: Ten?

ELAINE: Twenty sponges should be plenty.

PHARMACIST: Did you say twenty?

ELAINE: Yeah, twenty-five sponges is just fine.

PHARMACIST: Right. So, you're set with twenty-five.

ELAINE: Yeah. Just give me the whole case and I'll be on my way.

[Jerry and Lena having dinner in a restaurant with George and Susan.]

JERRY: Hey, I have found the best-smelling detergent. Lena, smell my shirt.

LENA (smells Jerry's arm): Mmm! Very nice.

JERRY: It's All-Tempa-Cheer.

LENA: I use Planet. It's bio-degradable and doesn't pollute the oceans.

GEORGE: Yeah, the oceans really are getting very sudsy.

LENA (to waiter): Can you wrap up all the left-overs on the table, please? I always take the left-overs. I work in a soup kitchen every morning at 6 a.m.

JERRY: They serve soup at 6 a.m.?

LENA: Yeah. That's all they have.

JERRY: Do the bums ever complain? &quot;Soup again?&quot;

GEORGE: I'd get tired of it.

JERRY: How could you not?

LENA: Guess who volunteered last week?

GEORGE: Mick Jagger.

LENA: No. Maya Angelou.

SUSAN: Oh, the poet!

JERRY (to Lena): So, let me ask you something - these people eat soup three times a day?

LENA: I don't know.

SUSAN (to Lena): So, did you get to talk to her?

LENA: Talk to who?

JERRY: Is it a lot of cream soups?

SUSAN: Maya Angelou, the poet.

LENA: No, I didn't get the chance.

GEORGE: Oh, well, I'm sure you can reach her... she's a poet. What does a poet need an unlisted number for?

(Jerry gives George a surprised look. George looks back, puzzled.)

SUSAN: I'm going to the ladies room.

LENA: I'll go with you. (They leave.)

GEORGE: What are you looking at me like that for?

JERRY: Why'd you have to mention &quot;unlisted number&quot;?

GEORGE: What are you talking about?

JERRY: Alright, I gotta tell you something, but you cannot tell Susan.

(George's interest is piqued.)

[George and Susan in the car on the way home.]

SUSAN: Jerry got her phone number off of an AIDS walk list? Oh, that's awful!

GEORGE: I know, but don't say anything to anyone. He told me not to tell you.

SUSAN: But you told me anyway?

GEORGE: Well, you know, I was thinking about what you said before, and... you're right, I've never really been a couple, so... if that's the rule, then I'm gonna go by the rule.

SUSAN: Thank you, honey.

GEORGE: So, you wanna go home and... make up, officially?

SUSAN: Can we stop by a drug store first?

GEORGE: What for?

SUSAN: I'm out of birth control stuff.

GEORGE: Oh, o.k., yeah. Where am I gonna park here...? (Pulls over.)

SUSAN: Oh, don't park. I'll just sit in the car, you can run in.

GEORGE: Me run in? Why don't you run in?

SUSAN: You don't know what I use for birth control, do you?

GEORGE: Of course I do.

SUSAN: You do? What?

GEORGE: You know. You use the, uh... (mutters something unintelligible under his breath.)

SUSAN: The what?

GEORGE: You know, the uh... (mutters it again.)

SUSAN: Just get me some sponges, please.

GEORGE: Wait, wait a minute... they don't have them anymore. I just found out, they just took them off the market.

SUSAN: Off the market? The sponge?

GEORGE: Yeah, so you gotta use something else.

SUSAN: I can't! I love the sponge! I need the sponge!

GEORGE: O.k... (thinks) I think I know where we can get one.

[Jerry approaching his apartment door. He hears the sound of a loud group of people from inside Kramer's place. He knocks on Kramer's door. Kramer answers.]

JERRY: Kramer, what the hell is going on in there?

KRAMER: It's a poker game... (yells to the crowd) And I'm kickin' some serious butt!

JERRY: Are you out of your mind? You got the AIDS walk tomorrow!

VOICE FROM POKER GAME: Hey, Kramer - are you in?

KRAMER: Oh, you gotta be kiddin'! You see those two ladies I got showin'? Do they look scared?!

JERRY: You're never gonna make it!

(Kramer giggles and returns to the game.)

[Elaine and Billy in Elaine's apartment, kissing passionately on the sofa.]

BILLY: You, uh... you wanna go in the bedroom?

ELAINE: O.k. Hold on just a second. (Gets up and heads to the bathroom. George knocks at the door.)

GEORGE: Elaine? It's me, George. (Elaine opens the door.) Hey, sorry to bother you so late. (To Billy) Hey! How ya doin.' (To Elaine) Uh, did you get any of those sponges?

ELAINE: Yeah. Cleaned out the whole west side. Why?

GEORGE: Well... Susan.

ELAINE: Ah, Susan uses the sponge.

GEORGE: Susan loves the sponge.

ELAINE: Yeah, I'm sorry, George. I can't help you out.

GEORGE: What?

ELAINE: I can't do it. No way, there's no how. (Tries to push George out the door. George resists.)

GEORGE: Elaine... let me just explain something to you. See, this is not just a weekend routine... I'm on the verge of make-up sex here. You know about make-up sex?

ELAINE: Oh yeah, I know all about make-up sex, and I'm really sorry. (Shoves George into the hallway and closes the door. George blocks the door with his foot.)

GEORGE: Elaine, can I just explain something to you very privately here? Susan and I have been together many, many times now, and just between you and me, there's really no big surprises here, so... make-up sex is all that I have left.

ELAINE: I'm sure you'll have another fight, George. (Stamps on George's foot and closes the door.) (To Billy) Hold that thought!

(Elaine goes into the bathroom for a sponge, but then stops and reconsiders.)

[Susan talking on the phone with Monica.]

SUSAN: So, listen to this. But don't tell anyone - Jerry Seinfeld? He got a woman's number off an AIDS walk list.

(Cut to Monica at home talking on the phone with Susan.)

MONICA: He got her number off an AIDS walk list?

(Cut to Lena at home talking on the phone with Monica.)

LENA: He what?

[Jerry and Lena in Jerry's apartment.]

JERRY: How'd you find out?

LENA: A friend of a friend of a friend of Susan's.

JERRY: George!

LENA: Pardon?

JERRY: Nothing. Listen, I'm sorry, I just -

LENA: It's o.k.! There's nothing to be sorry about. I don't mind.

JERRY: You don't mind that I got your number off the AIDS walk list?

LENA: No, not at all. No problem. (Jerry looks at Lena suspiciously. Lena leaves with all of Kramer's poker buddies, who are filing out of Kramer's apartment.)

KRAMER: Ah, you're lucky you're walkin' out of here with a pair of pants on!

JERRY: You went all night?

KRAMER (shows Jerry his winnings): Jerry, ah? Breakfast on me, huh?

JERRY: Kramer, are you out of your mind? You got the AIDS walk in like, three hours! You're never gonna make it!

KRAMER: AIDS walk! That's a cake walk. (George enters.) Hey!

JERRY: So, George, guess what? Lena found out how I got her number.

GEORGE: Really? How'd she do that?

JERRY: A friend of a friend of Susan's.

GEORGE: My Susan?

JERRY: Why'd you tell her?!

GEORGE: Because, Jerry, it's a couple rule! We have to tell each other everything!

JERRY: Well you know what this means, don't you?

GEORGE: What?

JERRY: You're cut off, you're out of the loop!

GEORGE: You're cutting me off? No, no, no Jerry, don't cut me off!

JERRY: You leave me no choice! You're the media now as far as I'm concerned!

GEORGE: C'mon Jerry, please! It won't happen again.

JERRY: If you were in the mafia, would you tell her every time you killed someone?

GEORGE: Hey, a &quot;hit&quot; is a totally different story.

JERRY: I don't know, George.

GEORGE: So, Lena was upset, huh?

JERRY: You know what? That was the amazing thing.

GEORGE: What, it didn't bother her?

JERRY: No, she said it was fine. There's something very strange about this girl.

GEORGE: What?

JERRY: She's too good.

GEORGE: Too good...

JERRY: I mean, she's giving and caring and genuinely concerned about the welfare of others - I can't be with someone like that!

GEORGE: I see what you mean.

JERRY: I mean, I admire the hell out of her. You can't have sex with someone you admire.

GEORGE: Where's the depravity?

JERRY: No depravity! I mean, I look at her, I can't imagine she even has sex.

(Elaine enters.)

JERRY (using Elaine as an example): On the other hand...

ELAINE: What?

GEORGE (to Elaine): Thanks again for last night!

ELAINE: Hey, I didn't even use one.

JERRY: I thought you said it was imminent.

ELAINE: Yeah, it was, but then I just couldn't decide if he was really sponge-worthy.

JERRY: Sponge-worthy?

ELAINE: Yeah, Jerry, I have to conserve these sponges.

JERRY: But you like this guy, isn't that what the sponges are for?

ELAINE: Yes, yes - before they went off the market. I mean, now I've got to re-evaluate my whole screening process. I can't afford to waste any of 'em.

GEORGE: You know, you're nuts with these sponges. George is gettin' frustrated!

[Kramer signing in at the AIDS walk.]

KRAMER (to organizer at desk): Uh, Cosmo Kramer?

ORGANIZER: Uh... o.k., you're checked in. Here's your AIDS ribbon.

KRAMER: Uh, no thanks.

ORGANIZER: You don't want to wear an AIDS ribbon?

KRAMER: No.

ORGANIZER: But you have to wear an AIDS ribbon.

KRAMER: I have to?

ORGANIZER: Yes.

KRAMER: See, that's why I don't want to.

ORGANIZER: But everyone wears the ribbon. You must wear the ribbon!

KRAMER: You know what you are? You're a ribbon bully. (Walks away.)

ORGANIZER: Hey you! Come back here! Come back here and put this on!

[George and Susan in a booth at the coffee shop.]

GEORGE: Elaine and her sponges... she's got like, a war chest full of them.

SUSAN: Well, I don't see why you just can't use condoms.

GEORGE: Oh, no, no... condoms are for single men. The day that we got engaged, I said goodbye to the condom forever.

SUSAN: Just once... for the make-up sex.

GEORGE: Make-up sex? You have to have that right after the fight, we're way past that.

SUSAN: Come on, just once?

GEORGE: No, no... I hate the condom.

SUSAN: Why?

GEORGE: I can never get the package open in time.

SUSAN: Well, you just tear it open.

GEORGE: It's not that easy. It's like &quot;Beat The Clock,&quot; there's a lot of pressure there.

[Kramer in the AIDS walk.]

WALKER #1: Hey, where's your ribbon?

KRAMER: Oh, I don't wear the ribbon.

WALKER #2: Oh, you don't wear the ribbon? Aren't you against AIDS?

KRAMER: Yeah, I'm against AIDS. I mean, I'm walking, aren't I? I just don't wear the ribbon.

WALKER #3: Who do you think you are?

WALKER #1: Put the ribbon on!

WALKER #2: Hey, Cedric! Bob! This guy won't wear a ribbon! (Cedric and Bob turn around and glare at Kramer.)

BOB: Who? Who does not want to wear the ribbon? (Kramer is frightened.)

[Elaine and Billy in her apartment.]

ELAINE: So, you think you're sponge-worthy?

BILLY: Yes, I think I'm sponge-worthy. I think I'm very sponge-worthy.

ELAINE: Run down your case for me again...?

BILLY: Well, we've gone out several times, we obviously have a good rapport. I own a very profitable electronics distributing firm. I eat well. I exercise. Blood tests - immaculate. And if I can speak frankly, I'm actually quite good at it.

ELAINE: You going to do something about your sideburns?

BILLY: Yeah, I told you... I'm going to trim my sideburns.

ELAINE: And the bathroom in your apartment?

BILLY: Cleaned it this morning.

ELAINE: The sink, the tub, everything got cleaned?

BILLY: Everything, yeah. It's spotless.

ELAINE: Alright, let's go. (They head for the bedroom.)

[Jerry arrives at Lena's apartment.]

JERRY: Hi.

LENA: Hi! Hey, look at this - I just got a citation in the mail for my work with shut-ins.

JERRY: Oh, the shut-ins, that's nice. You know, they're a very eccentric group. Because they're shut in. Of course, they're not locked in, they're free to go at anytime.

LENA: Oh, by the way, I checked at the soup kitchen - they do have cream soups.

JERRY: Hey, that's dynamite. You know, Lena, I wanted to talk to you about something... you know, because you're such a good person -

LENA: Oh, hang onto that thought - I'm rinsing a sweater, I left the water running. (Goes into the bathroom.) Hey, Jerry, can you get me a towel out of my bedroom closet?

JERRY: Oh, o.k. (Goes to the closet for the towel and finds dozens of boxes of Today sponges.)

JERRY'S brain: Oh my god! Look what's goin' on here! She is depraved! (Grabs a towel and brings it to Lena.) There you are.

LENA: Thanks. So, you were saying...?

JERRY: What? Nothing.

LENA: No, you said I was a good person...

JERRY: Oh...

LENA: You seem like you want to tell me something.

JERRY: Tell you something... I do.

LENA: What is it, Jerry? You can tell me anything.

JERRY: Oh, uh... you see these jeans I'm wearing?

LENA: Yeah.

JERRY: I change the 32 waist on the label to a 31 on all my jeans. So, you know. That's it. (Lena is puzzled.)

[George and Susan in bed. We see George's hands struggling to open a condom wrapper.]

SUSAN: Come on, George, just tear it open.

GEORGE: I'm trying, dammit.

SUSAN: Tear it.

GEORGE: I tried to tear it from the side, you can't get a good grip here. You gotta do it like a bag of chips.

SUSAN: Here give it to me.

GEORGE: Would you wait a second? Just wait? (They fight over it.)

SUSAN: Give it to me. (She rips it open.) Come on. Come on!

GEORGE (tosses the condom aside): It's too late.

[Kramer surrounded by Cedric, Bob, and the other walkers.]

BOB: So! What's it going to be? Are you going to wear the ribbon?

KRAMER (nervously): No! Never.

BOB: But I am wearing the ribbon. He is wearing the ribbon. We are all wearing the ribbon! So why aren't you going to wear the ribbon!?

KRAMER: This is America! I don't have to wear anything I don't want to wear!

CEDRIC: What are we gonna do with him?

BOB: I guess we are just going to have to teach him to wear the ribbon!

(Kramer tries to climb up a fire escape, but the mob grabs him and pulls him back down. Kramer screams.)

[Jerry and George waiting for Kramer at the finish line.]

JERRY: It completely turned her off.

GEORGE: Well, I can see that. What do you have to do that for? Who cares about your pants size?

JERRY: I don't wanna be a 32.

GEORGE: I'd kill to be a 32.

JERRY: She said I wasn't sponge-worthy. Wouldn't waste a sponge on me!

GEORGE: That condom killed me. Why do they have to make the wrappers on those things so hard to open?

JERRY: It's probably so the woman has one last chance to change her mind.

GEORGE: You never run out, do you? (Jerry smiles.) Where's Kramer? Everything's finished here.

JERRY: Oh, I told him he'd never make it. He was up all night! Oh my god... Kramer?

(They see Kramer staggering towards them with cuts and bruises, clothes torn, one shoe off. He collapses and crawls across the finish line.)

JERRY: Look at you. I told you. Up all night playing poker. Come on. (Jerry and George are about to leave. George turn's back and looks at Kramer.)

GEORGE: Hey, where's you AIDS ribbon?

(Kramer looks at George unbelievingly.)

[Elaine and Billy in bed the next morning.]

ELAINE (smiling): Good morning.

BILLY: How'd you sleep?

ELAINE (stretches): Great. You?

BILLY: Fine, fine. Everything o.k.?

ELAINE: Yep.

BILLY: No regrets?

ELAINE: Nope. (Billy leans in to kiss her.) What are you doing?

BILLY: What do you mean?

ELAINE: Oh... I don't think so.

BILLY: Why not? I thought you said everything was fine.

ELAINE: I wish I could help you, but I can't afford two of 'em. (Pats Billy on the shoulder and gets out of bed.)

The End
