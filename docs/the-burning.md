---
layout: default
title: The Burning
parent: Season 9
nav_order: 16
permalink: /the-burning
cat: ep
series_ep: 172
pc: 916
season: 9
episode: 16
aired: March 19, 1998
written: Jennifer Crittenden
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697663
wiki: https://en.wikipedia.org/wiki/The_Burning_(Seinfeld)
---

# The Burning

| Season 9 - Episode 16          | March 19, 1998            |
|:-------------------------------|:--------------------------|
| Written by Jennifer Crittenden | Directed by Andy Ackerman |
| Series Episode 172             | Production Code 916       |

"The Burning" is the 172nd episode of the NBC sitcom Seinfeld. This was the 16th episode for the ninth and final season. It aired on March 19, 1998.

The title of this episode is the same as the 1981 slasher film The Burning, which was Jason Alexander's film debut.

This episode is dedicated to the memory of Lloyd Bridges, who died on March 10, 1998. Bridges played Izzy Mandelbaum in "The English Patient" and "The Blood".

## Plot

Elaine thinks that boyfriend David Puddy may be religious after finding Christian rock stations set on his car radio. At the coffee shop, George laments to Jerry about losing respect at a project meeting led by Mr. Kruger after following a good suggestion with a bad joke. Jerry suggests that George use the Vegas showmanship trick of leaving the room after a comedic high note. Elaine tells George and Jerry about her suspicions with Puddy. George suggests altering his radio presets as a test. Kramer and Mickey Abbott get an acting gig playing sick for some medical students. Jerry's girlfriend Sophie (Cindy Ambuehl) calls him with the "it's me" greeting, but he does not recognize her voice. At the next Kruger meeting, George takes Jerry's suggestion and actually leaves the room after a well-received joke and goes to a movie theater to see Titanic. For their acting job, Mickey and Kramer are assigned bacterial meningitis and gonorrhea, respectively. Elaine confirms that Puddy is religious. Kramer picks up on the showmanship idea and gives an impressive theatrical performance of gonorrhea for the med students. When Sophie uses the unwelcome "it's me" greeting on Jerry's answering machine, George suggests he does an "it's me" when he calls back to see if she recognizes Jerry's voice. Sophie does not, and assumes it's a friend named "Rafe". She reveals that she has not told Jerry about an incident she calls the "tractor story". Puddy confirms that he is religious and doesn't care that Elaine is not, because he is "not the one going to Hell".

George and Jerry speculate on what the tractor story may be, George thinks she may have lost her thumbs in an accident and that her big toe is grafted on in their place. This causes Jerry to yell that she does not have "toe-thumbs". Elaine is frustrated that Puddy does not seem concerned about her, when he thinks she is going to Hell. George's showmanship backfires when Kruger throws everyone else off the large project because they are boring in comparison. Kramer is concerned about being typecast when the hospital wants him to perform gonorrhea again the next week, due to his stellar performance. Jerry sees a scar on Sophie's leg and assumes it was from a tractor accident. George finds that he has to do all the actual work on the project as Kruger constantly makes excuses and goofs off. Puddy asks Elaine to steal a newspaper. He would do it himself, but he reasons that he is bound by the Ten Commandments and she is going to Hell anyway. Kramer is attacked by Mickey after trying to take over Mickey's assigned role of cirrhosis of the liver. Elaine and Puddy seek the advice of a priest about their relationship. The priest informs them that they're both going to Hell for premarital sex, much to Elaine's delight and Puddy's resentment. Sophie tries to tell Jerry the tractor story, but he tells her that he already knows about it (believing it to be about the scar). Kramer and Mickey enter, still arguing about being given (the role of) gonorrhea, and Sophie tells them her tractor story. She says she got gonorrhea from riding a tractor in her bathing suit. Kramer tells her that that's impossible and she says that's what her boyfriend told her (with the implication that her boyfriend gave her gonorrhea, and that she was rather gullible and believed him). After hearing that, Jerry leaves the relationship on a comedic high note.

George tries to get Mr. Kruger to work and instead he makes silly comments and walks off on a high-note just like George previously did, leaving George with a mountain of paperwork.

## Cast

#### Regulars

Jerry Seinfeld ...................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus .............. Elaine Benes  
Michael Richards .................. Cosmo Kramer

#### Recurring

Patrick Warburton ......... David Puddy  
Danny Woodburn ........... Mickey Abbott

#### Guests

Daniel Von Bargen ............... Kruger  
Cindy Ambuehl .................... Sophie  
Henry Woronicz .................. Father Curtis  
Ursaline Bryant .................... Dr. Wexler  
Daniel Dae Kim .................... Student #1  
Alex Craig Mann .................. Student #2  
Brian Posehn ....................... Artie  
Alec Holland ........................ Co-Worker #1  
Suli McCullough ................... Co-Worker #2

## Script

[Elaine is getting ready to drive Puddy's car, he's giving her last minute instructions.]

PUDDY: Alright, be careful with the car, babe.

ELAINE: Yeah, yeah.

PUDDY: And don't move the seat, I got it right where I like it.

ELAINE: Goodbye?

PUDDY: Two and ten, babe.

ELAINE: Okay.

PUDDY: Don't peel out.

ELAINE: I won't.

(Elaine peels out and turns on the car stereo. She hears: &quot;Jesus is one, Jesus is all, Jesus picks me up when I fall...&quot; Elaine changes the stations but all of the presets are set to religious radio stations; &quot;And he said unto Abraham...&quot;, &quot;Amen! Amen!&quot;, &quot;So we pray...&quot;, &quot;Saved!&quot;, &quot;Jey-sus!&quot; She turns off the radio.)

ELAINE: Jesus?

[Meeting at George's office. His Boss, Mr. Kruger, is speaking.]

KRUGER: According to our latest quarterly thing, Kruger Industrial Smoothing is heading into the red. Or the black, or whatever the bad one is. Any thoughts?

GEORGE: Well, I know when I'm a little strapped, I sometimes drop off my rent check having forgotten to sign it. That could buy us some time.

KRUGER: Works for me. Good thinking, George.

CO-WORKER #1: Alright, George.

CO-WORKER #2: Way to go man.

GEORGE: Or we don't even send the check and then when they call, we pretend we're the cleaning service. Heh heh. &quot;Hello? I sorry, no here Kruger.&quot;

KRUGER: Are you done? Silly voices, c'mon people, let's get real.

CO-WORKER #1: Good one.

CO-WORKER #2: That was bad.

[George and Jerry are at the coffee shop.]

GEORGE: I had 'em, Jerry. They loved me.

JERRY: And then?

GEORGE: I lost them. I can usually come up with one good comment during a meeting but by the end it's buried under a pile of gaffes and bad puns.

JERRY: Showmanship, George. When you hit that high note, you say goodnight and walk off.

GEORGE: I can't just leave.

JERRY: That's the way they do it in Vegas.

GEORGE: You never played Vegas.

JERRY: I hear things.

(Elaine enters and has a seat.)

ELAINE: Here's one. I borrowed Puddy's car and all the presets on his radio were Christian rock stations.

GEORGE: I like Christian rock. It's very positive. It's not like those real musicians who think they're so cool and hip.

ELAINE: So, you think that Puddy actually believes in something?

JERRY: It's a used car, he probably never changed the presets.

ELAINE: Yes, he is lazy.

JERRY: Plus he probably doesn't even know how to program the buttons.

ELAINE: Yes, he is dumb.

JERRY: So you prefer dumb and lazy to religious?

ELAINE: Dumb and lazy, I understand.

GEORGE: Tell you how you could check.

ELAINE: How?

GEORGE: Reprogram all the buttons, see if he changes them back. You know? The old switcheroo.

JERRY: No, no, the old switcheroo is you poison your drink then you switch it with the other person's.

GEORGE: No, it's doing the same thing to someone that they did to you.

JERRY: Yeah, Elaine's gonna do the same thing to Puddy's radio that the radio did to her.

GEORGE: Well that's the gist of it!

ELAINE: Quiet! So where is this Sophie?

JERRY: Oh, she's picking me up in a few minutes.

ELAINE: How long have you two been together?

JERRY: I dunno. Since the last one. Oh, here she is. You wanna meet her?

ELAINE &amp; GEORGE: Nah.

(Jerry leaves to go meet Sophie by the register.)

GEORGE: By the way, how did Puddy get back in the picture?

ELAINE: I needed to move a bureau.

[Kramer and Mickey enter Jerry's apartment.]

KRAMER: Hey Jerry, you got any pepper?

MICKEY: Hey Jerry.

JERRY: Hey Mickey. Check the pepper shaker.

KRAMER: Yeah. (inhales some pepper then sneezes violently) See? It should sound like that, something like that.

MICKEY: Aah-choo.

KRAMER: A little wetter. See, I didn't believe it.

JERRY: What's with the fake sneezing?

KRAMER: Yeah, we're going down to Mt. Sinai Hospital, See they hire actors to help the students practice diagnosing.

MICKEY: They assign you a specific disease and you act out the symptoms. It's an easy gig.

JERRY: Do medical schools actually do this?

KRAMER: Well the better ones. Alright, let's practice retching.

KRAMER &amp; MICKEY: HUAAHHH!!

JERRY: I think the phone is ringing.

KRAMER &amp; MICKEY: HUAAHHH!!

JERRY: Would you hold it a second?! Thank you, will you get out of here with that stuff?

KRAMER: Mickey, DTs.

(Kramer and Mickey exit, shaking, while Jerry answers the phone.)

JERRY: Hello?

SOPHIE: Hey. It's me.

JERRY: Elaine?

SOPHIE: No, it's me.

JERRY: George??

SOPHIE: Jerry, it's Sophie. I can't believe you don't recognize my voice.

JERRY: Oh, I knew it was you, I was joking. I'm a comedian.

(Kramer enters.)

KRAMER: You got any Ipecac?

JERRY: Ipecac? Kramer, I really think you guys are going too far with this.

KRAMER: No, Mickey, he swallowed twelve aspirin.

JERRY: Did he overdose?

KRAMER: No, it's just too much.

[Office meeting at Kruger Industrial Smoothing.]

KRUGER: ...And it gets worse. The team working on the statue in Lafayette Square kind of over-smoothed it. They ground the head down to about the size of a softball, and that spells trouble.

GEORGE: Alright, well why don't we smooth the head down to nothing, stick a pumpkin under its arm and change the nameplate to Ichabod Crane?

(Everyone at the meeting breaks out in laughter.)

GEORGE: (getting up and leaving) Alright! That's it for me. Goodnight everybody.

[Mt. Sanai Hospital, a woman in a lab coat is handing out envelopes to a group of people, Mickey and Kramer included.]

DR. WEXLER: In your packet you will find the disease you have been assigned and the symptoms you will need to exhibit.

MICKEY: Bacterial Meningitis. Jackpot!

KRAMER: Gonorrhea? You wanna trade?

MICKEY: Sorry buddy, this is the &quot;Hamlet&quot; of diseases. Severe pain, nausea, delusions, it's got everything.

KRAMER (to the man beside him) How 'bout you, do you wanna trade?

MAN: Sure.

KRAMER: Okay, what do you got?

MAN: The surgeon left a sponge inside me.

KRAMER: Good luck with that.

[George and Jerry are at Jerry's apartment.]

GEORGE: I knew I had hit my high note so I thanked the crowd and I was gone.

JERRY: What did you do the rest of the day?

GEORGE: I saw &quot;Titanic&quot;. So that old woman, she's just a liar, right?

JERRY: And a bit of a tramp if you ask me.

(Elaine enters.)

ELAINE: Hello boys.

GEORGE: Hey, so, did you give that radio the old switcheroo?

ELAINE: I did.

GEORGE: And the Christian rock?

ELAINE: Resurrected! And look what I pried off of his bumper, a Jesus fish!

GEORGE: Jerry, do you have any fishsticks?

JERRY: No. So you're disappointed he's a spiritual person?

ELAINE: Well yeah, I got him because he seemed so one-dimensional, I feel misled.

GEORGE: I think it's neat. You don't hear that much about god anymore.

JERRY: I hear things. Hey, so Sophie gave me the &quot;It's me&quot; on the phone today.

ELAINE: &quot;It's me?&quot; Isn't it a little premature?

JERRY: I thought so.

ELAINE: Hah. She's not a &quot;me&quot;. I'm a &quot;me&quot;.

GEORGE: I'm against all &quot;it's me&quot;s. So self-absorbed and egotistical, it's like those hip musicians with their complicated shoes!

(Kramer enters.)

KRAMER: Well, I got gonorrhea.

ELAINE: That seems about right.

KRAMER: That's what they gave me.

GEORGE: They? The Government?

JERRY: No, no. He's pretending he's got gonorrhea so med students can diagnose it.

KRAMER: And it's a waste of my talent. It's just a little burning. Mickey, he got bacterial meningitis.

GEORGE: I guess there are no small diseases, only small actors.

(The other three start laughing.)

GEORGE: (leaving) Alright that's it for me. Good night everybody.

ELAINE: What was that?

JERRY: Showmanship, George is trying to get out on a high note.

KRAMER: See, showmanship. Maybe that's what my gonorrhea is missing.

JERRY: Yes! Step into that spotlight and belt that gonorrhea out to the back row.

KRAMER: Yes, yes I will! I'm gonna make people feel my gonorrhea, and feel the gonorrhea themselves.

[Mt. Sanai Hospital. Kramer is on the table surrounded by med students.]

STUDENT #1: And are you experiencing any discomfort?

KRAMER: Just a little burning during urination.

STUDENT #1: Okay, any other pain?

KRAMER: The haunting memories of lost love. May I? (signals to Mickey) Lights? (Mickey turns down the lights and Kramer lights a cigar) Our eyes met across the crowded hat store. I, a customer, and she a coquettish haberdasher. Oh, I pursued and she withdrew, then she pursued and I withdrew, and so we danced. I burned for her, much like the burning during urination that I would experience soon afterwards.

STUDENT #1: Gonorrhea?!

KRAMER: Gonorrhea!

(The lab breaks out in spontaneous applause as Mickey turns up the lights and Kramer takes a bow.)

[Jerry and George are back at Jerry's apartment. Jerry is checking his phone messages.]

JERRY: One message. Hope it's not from you.

ANSWERING MACHINE: &quot;Hey Jerry, it's me. Call me back.&quot;

JERRY: Sophie.

GEORGE: She's still doing that?

JERRY: Yep.

GEORGE: Alright, I'll tell you what you do. You call her back and give her the &quot;it's me&quot;, heh? Pull the old switcheroo.

JERRY: I think that's a &quot;what's good for the goose is good for the gander&quot;.

GEORGE: What the hell is a gander, anyway?

JERRY: (picking up the phone and dialing) It's a goose that's had the old switcheroo pulled on it. Hi Sophie, it's me.

SOPHIE: Hey Raef.

JERRY: (to George) She thinks it's someone named Raef.

GEORGE: Good, let her think it.

JERRY: (into the phone, with a disguised voice) So, what's going on?

SOPHIE: Not a lot.

GEORGE: Ask about you, ask about you.

JERRY: So, uh, how are things with Jerry?

SOPHIE: Oh, I really like him but, well, I still haven't told him the tractor story.

JERRY: Right, right, the tractor story.

SOPHIE: Are you sick, Raef? You sound kinda funny.

JERRY: I sound funny?

GEORGE: Abort! Abort!

JERRY: Yeah I better get to a doctor, bye. (Hangs up) That was close! What drives me to take chances like that?

GEORGE: That was very real.

JERRY: She said there's some tractor story that she hasn't told me about.

GEORGE: Woah, back it up, back it up. Beep, beep, beep. Tractor story?

JERRY: Beep, beep, beep? What are you doing?

[Elaine and Puddy are at Puddy's apartment.]

ELAINE: So where do you wanna eat?

PUDDY: Feels like an Arby's night.

ELAINE: Arby's. Beef and cheese and do you believe in god?

PUDDY: Yes.

ELAINE: Oh. So, you're pretty religious?

PUDDY: That's right.

ELAINE: So is it a problem that I'm not really religious?

PUDDY: Not for me.

ELAINE: Why not?

PUDDY: I'm not the one going to hell.

[Jerry and George are at the coffee shop.]

GEORGE: You know what I think? I bet she stole a tractor.

JERRY: No one's stealing a tractor, it's a five-mile-an-hour getaway. We're dancing around the obvious, it's gotta be disfigurement.

GEORGE: Does she walk around holding a pen she never seems to need?

JERRY: No, she looks completely normal.

GEORGE: Oh. Okay, here it is, I got it. She lost her thumbs in a tractor accident and they grafted her big toes on. They do it every day.

JERRY: You think she's got toes for thumbs?

GEORGE: How's her handshake? A little firm, isn't it? Maybe a little too firm?

JERRY: I don't know.

GEORGE: Hands a little smelly?

JERRY: Why do I seek your counsel?

(Elaine walks in.)

ELAINE: Well I'm going to hell.

JERRY: That seems about right.

ELAINE: According to Puddy.

JERRY: Hey, have you heard the one about the guy in hell with the coffee and the doughnuts and--

ELAINE: I'm not in the mood.

GEORGE: (To a passing waitress) I'll have some coffee and a doughnut.

JERRY: What do you care? You don't believe in hell.

ELAINE: I know, but he does.

JERRY: So it's more of a relationship problem than the final destination of your soul.

ELAINE: Well, relationships are very important to me.

JERRY: Maybe you can strike one up with the prince of darkness as you burn for all eternity.

GEORGE: (to the waitress bringing his doughnut) And a slice of devil's food cake.

[Kruger's office. George enters, seeing nobody but Mr. Kruger.]

GEORGE: Hey. Where is everyone?

KRUGER: They're all off the project. They were boring. George, you are my main man.

GEORGE: I am?

KRUGER: I don't know what it is, I can't put my finger on it, but lately you have just seemed 'on'. And you always leave me wanting more.

GEORGE: This is a huge project involving lots of numbers and papers and folders.

KRUGER: Ah, I'm not too worried about it. Let's get started.

GEORGE: Okay.

KRUGER: George? Check it out. (He begins to spin around in his chair) Three times around, no feet.

GEORGE: And?

KRUGER: All me.

[Kramer and Mickey are back at Mt. Sinai.]

DR. WEXLER: Alright, and here are you ailments for this week. By the way, Mr. Kramer, you were excellent.

KRAMER: Oh, thank you.

MICKEY: Cirrhosis of the liver with jaundice! Alright I get to wear make-up! What did you get?

KRAMER: Gonorrhea? Excuse me, I think there's been a mistake, see, I had gonorrhea last week.

DR. WEXLER: Oh, it's no mistake. We loved what you did with it.

KRAMER: I don't believe this, I'm being typecast.

[Jerry and Sophie are at Jerry's playing chess.]

SOPHIE: I move my knight... here. Check.

JERRY: They should update these pieces, nobody rides horses anymore. Maybe they should change it to a tractor.

SOPHIE: Jerry, are you embarrassed that you're losing?

JERRY: Losing? You know, yesterday I lost control of my car, almost bought the farm.

SOPHIE: Bought the farm?

JERRY: Tractor!

SOPHIE: This is an odd side of you, Jerry. I feel uncomfortable.

JERRY: Wait, don't go. Let's thumb wrestle.

(Sophie drops her purse and when she bends down to pick it up, Jerry nods knowingly.)

[Jerry and George are at the coffee shop.]

GEORGE: A scar?

JERRY: A big long scar where her leg would dangle when she's riding a...?

GEORGE: A tractor.

JERRY: I'm sure she's a little self-conscious and doesn't like to talk about it.

GEORGE: I don't see why's she more self-conscious about that than her toe thumbs.

JERRY: She doesn't have toe thumbs.

GEORGE: Well, if she keeps horsing around with that tractor--

JERRY: Alright. So how's the two-man operation at Kruger?

GEORGE: Two-man? It's all me. Kruger doesn't do anything; Disappears for hours at a time, gives me fake excuses. This afternoon I found him with sleep creases on his face. The only reason I got out to get a bite today was that he finally promised to buckle down and do some actual work. (turning around, George sees Mr. Kruger at a booth eating a piece of cake) Oh, I don't believe this. This is what I have to put up with, Jerry. (He walks over) Mr. Kruger? Who said he was going to do some actual work today? Who?

KRUGER: I'm not too worried about it.

GEORGE: Well I am. Couldn't you try to go through some of that stuff I put in your shoebox?

KRUGER: Alright, alright I'm going.

GEORGE: (to Jerry) Huh-ho! Have you ever seen anything like this?

JERRY: Never.

[Elaine's hallway. The door opens, Puddy steps out in his bathrobe. There's a newspaper in front of the door across from Elaine's.]

PUDDY: Elaine, they forgot to deliver your paper today. Why don't you just grab that one.

ELAINE: 'Cause that belongs to Mr. Potato Guy, that's his.

PUDDY: C'mon, get it.

ELAINE: Well if you want it, you get it.

PUDDY: Sorry, thou shalt not steal.

ELAINE: Oh, but it's ok for me?

PUDDY: What do you care, you know where you're going.

ELAINE: Alright, that is it! I can't live like this.

PUDDY: Nah.

ELAINE: C'mon.

PUDDY: Alright, what did I do?

ELAINE: David, I'm going to hell! The worst place in the world! With devils and those caves and the ragged clothing! And the heat! My god, the heat! I mean, what do you think about all that?

PUDDY: Gonna be rough.

ELAINE: Uh, you should be trying to save me!

PUDDY: Don't boss me! This is why you're going to hell.

ELAINE: I am not going to hell and if you think I'm going to hell, you should care that I'm going to hell even though I am not.

PUDDY: You stole my Jesus fish, didn't you?

ELAINE: Yeah, that's right!

(Elaine places her hands beside her head, index fingers raised as 'horns' and she emits a gutteral growling sound.)

[Mt Sanai Hospital. The actors are gathered. Mickey is practicing his part.]

MICKEY: Oh, my liver! Why did I drink all those years? Why did I look for love in a bottle?

DR. WEXLER: Mr. Kramer? You're up.

(Kramer walks in, his face is noticeably yellow.)

MICKEY: Wait a minute. You are doing gonorrhea, aren't you?

KRAMER: Well, we'll see.

STUDENT #2: So, what seems to be bothering you today, Mr. Kramer?

KRAMER: (pulling a liquor bottle from his jacket pocket) Well, I guess it started about twenty years ago when I got back from Vietnam, and this was the only friend I had left.

MICKEY: Hey! That's my cirrhosis! He's stealing my cirrhosis! (he jumps Kramer) You wanna be sick? I'll make you sick.

(They fall to the floor, wrestling.)

STUDENT #2: Cirrhosis of the liver and PCP addiction?

[Elaine and Puddy have gone to see a priest, Father Curtis.]

FATHER CURTIS: Let me see if I understand this. You're concerned that he isn't concerned that you're going to hell. And you feel that she's too bossy.

ELAINE &amp; PUDDY: Yeah, that's right.

FATHER CURTIS: Well, oftentimes in cases of inter-faith marriages, couples have difficulty--

ELAINE (Interrupting) Woah, woah, woah! No one's getting married here.

FATHER CURTIS: You aren't?

PUDDY: No.

ELAINE: We're just, you know, having a good time.

FATHER CURTIS: Oh, well then it's simple. You're both going to hell.

PUDDY: No way, this is bogus, man!

ELAINE: Well, thank you father.

FATHER CURTIS: Oh, did you hear the one about the new guy in hell who's talking to the devil by the coffee machine?

PUDDY: I'm really not in the mood, I'm going to hell.

ELAINE: Oh, lighten up. It'll only feel like an eternity.

(Elaine makes the same 'fingers up' devil gesture as she did in her apartment and Father Curtis joins in.)

[Jerry and Sophie ar at Jerry's apartment.]

SOPHIE: You know, Jerry, there's this thing that I haven't told you about. See, there was this tractor and, oh boy, this is really difficult.

JERRY: Sophie, it's me. I know about the tractor story and I'm fine with it.

SOPHIE: How could you know?

JERRY: (putting his finger to Sophie's lips, then to his own, then back to Sophie's) Shh. Shh. Shh. It's not important. What's important is I'm not gonna let a little thing like that ruin what could be a very long-term and meaningful relationship.

(Kramer and Mickey barge in, they're in the middle of an argument.)

KRAMER: ...I didn't say that, no.

MICKEY: You gave me gonorrhea, you didn't even tell me!

KRAMER: Well, I'm sorry. I gave you gonorrhea because I thought you'd have fun with it.

JERRY: Hey, hey! I'm with someone.

KRAMER: Oh. Hello.

SOPHIE: No, I understand. This could be a tough thing to deal with. The important thing is that you have a partner who's supportive.

KRAMER: (to Mickey) You know? She's right.

SOPHIE: Unfortunately, I didn't have a partner. I got gonorrhea from a tractor.

JERRY: You got gonorrhea from a tractor?? And you call *that* your tractor story??

KRAMER: You can't get it from that.

SOPHIE: But I did. My boyfriend said I got gonorrhea from riding the tractor in my bathing suit.

JERRY: (walking out) Alright, that's it for me. You've been great. Goodnight everybody.

[Mr. Kruger and George are burning the midnight oil. George is working, Mr. Kruger is bouncing a ball against the wall and catching it. George is percolating.]

GEORGE: Would you mind helping me out with some of this stuff?!?

KRUGER: You seem like you've got a pretty good handle on it.

GEORGE: No! I don't! Don't you even care? This is your company! It's your name on the outside of the building! Speaking of which, the 'R' fell off and all it says now is K-uger!

KRUGER: K-uger, that sounds like one of those old-time car horns, huh? K-uger! K-uger!

GEORGE: Huh-ho! Oh! You are too much, Mr. Kruger! Too much!

KRUGER: (getting up to leave) Thank you George, you've been great. That's it for me.

GEORGE: Oh no, you're not going out on a high note with me Mr. Kruger!

KRUGER: It's K-uger!

GEORGE: No! No!

KRUGER: Goodnight everybody!

The End
