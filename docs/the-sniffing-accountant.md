---
layout: default
title: The Sniffing Accountant
parent: Season 5
nav_order: 4
permalink: /the-sniffing-accountant
cat: ep
series_ep: 68
pc: 504
season: 5
episode: 4
aired: October 7, 1993
written: Larry David & Jerry Seinfeld
directed: Tom Cherones
imdb: http://www.imdb.com/title/tt0697779
wiki: https://en.wikipedia.org/wiki/The_Sniffing_Accountant
---

# The Sniffing Accountant

| Season 5 - Episode 4                    | October 7, 1993          |
|:----------------------------------------|:-------------------------|
| Written by Larry David & Jerry Seinfeld | Directed by Tom Cherones |
| Series Episode 68                       | Production Code 504      |

"The Sniffing Accountant" is the 68th episode of the sitcom Seinfeld, being the fourth episode of the series' fifth season. It aired on NBC on Thursday, October 7, 1993.

In the episode, George's father gets him an interview as a brassiere salesman. Evidence points to Jerry's accountant being a cocaine user. Jerry, Kramer and Newman set up a sting to find out the truth. Elaine's new boyfriend is perfect except for his unwillingness to use exclamation points.

The episode was written by creators of Seinfeld, Larry David and Jerry Seinfeld, and directed by Tom Cherones. To research for one of this episode's recurring jokes where the characters feel others' shirt sleeves between their thumb and forefinger, David did this himself, assessing the different kinds of fabric and the owners' reactions. The episode received positive reviews from critics and received a 19.1/21 Nielsen rating.

## Plot

The episode opens with one of Jerry Seinfeld's stand-up comedy bits, centering on the government and the IRS.

The episode then jumps to Monk's Café, where Elaine discusses her new boyfriend, Jake Jarmel (played by Marty Rackham), who she met in her office. She explains how he approached her, and felt her gabardine jacket between his thumb and forefinger rather seductively. The conversation then jumps to Jerry’s new sweater, which he found in the back of his closet. At that point, Elaine looks out the window of the café and sees Barry Prophet (John Kapelos), Jerry's accountant. They invite him in, but are stunned to find him repeatedly sniffing during their conversation. The group discusses the possibilities that he could be on drugs. Jerry is panic-stricken, considering that Barry could write checks out of his account for illegal narcotics.

George goes home to his parents' house, and his father explains how he got him an interview with Sid Farkus (Patrick Cronin), the manager of a company that sells women's underwear, in an attempt to get George a job as a bra salesman. Meanwhile, Jerry tells Kramer about the "Barry on drugs" situation, and Kramer is convinced he is a drug addict after hearing he went to the bathroom during their confrontation. At this point, Jerry gives Kramer his sweater because it was too itchy. Meanwhile, Elaine comes to her apartment, where Jake is preparing dinner. Initially, they laugh and flirt with each other, but an argument ensues when Elaine gets surprised after finding out Jake did not put an exclamation point after an important phone message he wrote down. Jake takes extreme exception to Elaine's trivial criticism and breaks up with her, putting an exclamation point after his parting words | "I’M LEAVING!"

To find out once and for all as to whether he is on drugs or not, Kramer, Newman, and Jerry organize a sting. They wait inside a car in front of Barry's workplace, and when they see him going into a bar, Kramer (wearing Jerry's sweater) goes after him. He finds Barry sniffing in the bar, and manages to get a picture of him in a bathroom stall.

George carried through his interview with Sid Farkus and made a wonderful impression, resulting in him getting hired for the job. He becomes so consumed with confidence from his perfectly executed interview that he feels a random woman's shirt between his thumb and forefinger on his way out. The woman (Christa Miller), who turns out to be Farkus' boss, is enraged by the act and demands that George leave the company. Farkus obeys her order and fires George, which makes Frank mad (at George). Jerry writes a letter to Barry, stating that their relationship is officially terminated, and gives the letter to Newman to mail it. Kramer slips the picture he took of Barry in the bathroom into the letter as well, but in an affair involving a pizza delivery man, Jerry and Kramer conclude that it was actually Jerry's mohair sweater that caused Barry to sniff involuntarily. Jerry rushes out to stop Newman from mailing the letter.

Newman, who was remarkably confident at the time, felt a random woman's coat between his thumb and forefinger on his way to mail the letter. The woman freaked out and called her boyfriends to get Newman. Newman runs away in a mad panic, dropping the letter while doing so. The last scene shows Jerry announcing that Barry filed for bankruptcy, and if he had terminated his relationship with him prior to the filing, he could have gotten his money back.

### Aftermath

It is implied but never actually revealed that Barry spent all of Jerry's money on drugs, thus instigating him to file for bankruptcy.

## Production

This episode was written by series co-creators Larry David and Jerry Seinfeld, and was directed by Tom Cherones. The cast first read the script for this episode on September 8, 1993, at 11:00 a.m. Filming took place on September 14, 1993, with eighteen members of the Vandelay Industries Mailing Listing (a Seinfeld fan club) among the audience.

In real life, Seinfeld has claimed, without proof, that his accountant stole money (about US$50,000) from him to buy illegal drugs, with his suspicions thus inspiring the main plot line for this episode.

David actually worked as a bra salesman during his years as a struggling comedian. That had been many years prior to this episode though, so he had to do research in order to write dialogue pertaining to the configuration of modern bras. This was pre-Internet, so the writer's assistants called bra companies to ask questions.

Kramer's display of simultaneous drinking and smoking in this episode was unscripted, and required two takes to get right. After the first attempt, Michael Richards let out a loud belch (with smoke) that necessitated a second try at the scene. This scene helped Richards win an Emmy Award for his portrayal of the character. The first take was seen in Seinfeld's one-hour retrospective The Chronicle, which took place prior to the original airing of "The Finale." It is now included in the 2005 Season Five DVD set's blooper reel. Julia Louis-Dreyfus said that she was "in awe" when seeing him pull that off.

The line "barring some unforeseen incident" was first uttered in this episode by the character Sid Farkus, and the line eventually became a catchphrase around the show for years after this episode was filmed. Julia Louis-Dreyfus commented on how much she loved that line on the "Inside Look" commentary of the season 5 DVD set. She said that it was like a line from Foghorn Leghorn, and worked as a "precursor to chaos."

A big element to this episode was the element of private investigations. In actuality, Wayne Knight had experience with this line of work. He said, on the "Inside Look" commentary of the season 5 DVD set, that "before coming to Seinfeld, I’ve worked for five years in New York City as a private investigator. It was after having done Broadway, I crapped out, and needed a [...] job, didn’t want to work as a waiter, and had a friend who got a job as a PI, because they liked hiring actors."

During this episode, Kramer mentions how he wants to become a professional private detective. Coincidentally, after Seinfeld had ended, Richards later played a Los Angeles private detective on The Michael Richards Show.

## Series continuity

* Although Elaine and her boyfriend, Jake Jarmel, break up during this episode when he does not put an exclamation point on a note, they briefly get back together in the season finale, "The Opposite." In the season 6 episode "The Scofflaw", Elaine and Jake presumably broke up again and Elaine plans revenge on him.

* The exclamation point is mentioned again in "The Muffin Tops".

* Christa Miller has a brief role as Sid Farkus' boss, in which George touched her clothes briefly resulting in his firing. She would later play a different role as George's girlfriend in season 6's "The Doodle."

* The mohair sweater Kramer wears in the bar is the same sweater worn by Mrs. Sokol's daughter in Season 3's "The Boyfriend (Part 2)" on her second date with George.

* Patrick Cronin reprises his role as Sid Farkus again in "The Doorman", where he is considering doing business with Frank Costanza and Kramer after they create a male bra. The line "barring some unforeseen incident" is uttered once again in that episode by Farkus.

* The lady that had the coat on that Newman rubs in his fingers at the mailbox is Glenda (Patrika Darbo), a former co-worker of George's at his old real estate job in Season 2's "The Revenge".

* George is reading an issue of Glamour magazine at the Costanza's house, the same magazine he was reading when he was "caught" during Season 4's "The Contest".

## Cultural references

This episode makes a number of cultural references. Jerry makes references to Leave it to Beaver in his stand-up comedy bit that opens the show. He jokes about how the government is like parents for adults; the IRS being Ward and June Cleaver, and adults being Wally and The Beaver. He also says that your accountant is like Eddie Haskell, showing you "all these neat tricks to get away with stuff." He then says when you're sent to prison for tax fraud you would hope not to meet Clarence "Lumpy" Rutherford and "Whitey" Whitney.

A reference to Abscam is made when Kramer, Jerry, and Newman consider organizing a sting. Jerry and Newman argue over whether Glide Floss or dental tape is the better floss in this episode as well. Glide Floss was actually a big trend in the Seinfeld production office during the early part of season five.

## Reception

This episode gained a 19.1 Nielsen Rating and a 29 audience share, meaning that 19.1% of American households watched the episode, and 29% of all televisions in use at the time were tuned into it. It reran on March 24, 1994, and earned exactly the same numbers, which was a good sign that the show was becoming a hit. Michael Richards considers this episode to be one of his favorites.

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Recurring

Wayne Knight ............... Newman  
Estelle Harris ................. Estelle Costanza  
Jerry Stiller ..................... Frank Costanza  
Richard Fancy ................ Mr. Lippman

#### Guests

John Kapelos ....................... Barry  
Marty Rackham .................. Jake Jarmel  
Patrick Cronin .................... Farkus  
Christa Miller ...................... Ellen  
Ralph Harris Jr. ................... Ralph  
Maria Stanton ..................... Woman in Diner  
Deck McKenzie ................... Mitch

## Script

[Opening Monologue]

To me government is basically parents or adults... especially the IRS, the IRS is like Ward and June Cleaver, and we're all Wally and the Beaver and your accountant is Eddy Hascal showing you all these neat tricks to get away with stuff. Which is fine unless you get audited then you don't want some wise guy in a suit just standing there going 'You have a very lovely office here sir.' Because jail is the government's way of sending you to your room and when you meet Whitey and Lumpy in the joint, there's really gonna be something wrong with the Beaver.

[Monk's]

(Jerry, George and Elaine at he booth behind the cash register)

JERRY: So, does he like you?

ELAINE: What do you think?

JERRY: You like him?

ELAINE: Yeah, yeah like him, definitely like him. I like him a lot.

GEORGE: So, what's wrong with him?

ELAINE: Nothing, and I've looked.

GEORGE: Well, I'm sure you'll find something.

JERRY: So, how did you meet him?

ELAINE: In the office.

JERRY: So, he's a writer.

ELAINE: Yeah.

JERRY: Yeah, big surprise.

ELAINE: So, I was sitting at the reception desk, I was looking pretty hot. I was wearing my sling back pumps.

GEORGE: What are those?

ELAINE: Ask your mother, (making fun of George's living situation) you live with her now, don't you? Anyway, so then this guy comes up to me and starts feeling my jacket through his thumb and his forefinger like this.

(Elaine feels George's material)

JERRY: So, what did you do?

ELAINE: I said: &quot;So, what do you think?&quot;. And he said, &quot;Gabardine?&quot;. And I said, &quot;Yeah.&quot; That was it.

GEORGE: Wow, just felt your material?

ELAINE: Yeah...Jake Jarmel.

GEORGE: Sounds like a cool guy.

JERRY: Sounds like a jerk. Felt your material, come on.

GEORGE: Jerry, where did you get that sweater?

JERRY: Oh, what do you think? I found it at the back of my closet.

GEORGE: I think that's what the back of closets are for.

(Elaine looks out the window)

ELAINE: Hey, that's Barry. Look it's Barry. (taps on the window)

JERRY: (taps on the window) Hey...

ELAINE: Hi.

GEORGE: Who's that?

JERRY: That's Barry Prophet, he's our accountant.

GEORGE: I don't know how you can let this guy handle all your money.

ELAINE: Oh, he doesn't handle my money, he handles Jerry's money. He just does my taxes.

(Barry comes in)

JERRY: Hey Barry, how you doing?

BARRY: Hey, how are you?

(Jerry and Barry shake hands)

JERRY: This is my friend George.

BARRY: Hi George. (shakes George's hand)

ELAINE: Hi, what are you doing on this neighborhood?

BARRY: Nothing really. (sniffs; looks around) You, eh, you eat here?

JERRY: Yeah, so how's my money?

BARRY: Well it's still green. (sniffs twice)

JERRY: What, you got a cold?

BARRY: No, no.

ELAINE: Wow, look at that ring.

BARRY: (showing off the ring to Elaine) Oh, uh you like that? (Elaine not amused gives Barry a polite laugh; Barry sniffs) Say uh, where's the bathroom?

JERRY: Bathroom uh, bathroom's uh right over there.

BARRY: (turns looking in the direction Jerry mentioned) Oh, great.

(Barry goes to bathroom)

JERRY: Did you see that?

ELAINE: See what?

GEORGE: Yes, I saw that.

ELAINE: What?

JERRY: What was all that sniffing?

ELAINE: I don't know.

JERRY: You don't think...?

ELAINE: Oh, no! Come on Jerry.

GEORGE: He was definitely sniffing.

JERRY: I mean what if, what if, this this guy has got all my money. Plus he has got some Kramer's money with him. This guy could write checks to himself right out of my account.

ELAINE: No, I have known this guy since college. He doesn't do drugs.

JERRY: Then, what was all that sniffing?

ELAINE: Maybe it's the cold weather.

JERRY: Today's not cold.

GEORGE: All right, I gotta get going. My parents are expecting me.

ELAINE: (making fun of George as he leaves) Don't forget to wash your hands before supper.

[Costanza's. George, Estelle and Frank are eating dinner. George is pounding a ketchup bottle.]

FRANK: Why do you need all that ketchup for?

GEORGE: This is my ketchup. I bought this ketchup just so I could have as much as I want.

FRANK: So I, I talked to Phil Casacof today.

ESTELLE: Phil Casacof?

FRANK: Yeah, you know my friend, the bra salesman. He says they are looking maybe to put somebody on so I got you an interview next Friday with his boss.

GEORGE: Next Friday, what time?

FRANK: 2 o'clock.

GEORGE: That's my whole afternoon! I was going to look for sneakers.

FRANK: You can look for sneakers the next day!

ESTELLE: He doesn't know anything about bras.

GEORGE: I know a little. Besides, what do you have to know?

FRANK: Well, it wouldn't hurt to go in the and be able to discuss it intelligently. Maybe you should take a look at a few bras? (to Estelle) Where is you bra? Give him a bra to look.

ESTELLE: I am not giving him a bra.

FRANK: Why not?

ESTELLE: Because I don't need him looking at my bra.

FRANK: Why, so he'll go to the interview and he wouldn't know what he's talking about!?!

GEORGE: Do we have to...?

FRANK: You don't even know what they're made from.

GEORGE: They are made from lycra-spandex.

FRANK: Get out of here! Lycra-spandex?

ESTELLE: I think they are made from lycra-spandex.

FRANK: Wanna bet? How much you wanna bet?

ESTELLE: I'm not betting!

FRANK: Take a look.

ESTELLE: All right, I'll get a bra.

(Estelle leaves)

FRANK: (Yelling to Estelle as she leaves)I don't know what the big problem is getting a bra?!

GEORGE: She doesn't want to get a bra.

FRANK: I'm not saying go to the library and read the whole history, but it wouldn't kill you to know a little bit about it.

GEORGE: All right, it wouldn't kill me.

FRANK: How long it takes to find a bra? What's going on in there? You ask me to get a pair of underwear, I'm back in two seconds...you know about the uh cup sizes and all? They have different cups.

GEORGE: Yea I-I know about the cups.

FRANK: You got the A, the B, the C and the D. That's the biggest.

GEORGE: I know the D is the biggest. I've based my whole life on knowing that the D is the biggest.

ESTELLE: Here, here's the bra.

FRANK: Let me see it.

ESTELLE: (reading the bra) 100% lycra-spandex.

FRANK: Let me see it.

ESTELLE: I told you. Here, think you know everything?

FRANK: Hmm, that's surprising. All right, what else? You got the cups in the front, two loops in the back. All right, a guess that's about it.

GEORGE: I got it. Cups in the front, loops in the back. (puts the bra on the table)

ESTELLE: You got ketchup on it!

(George takes his plate and the ketchup and leaves.)

[5A. Jerry and Kramer.]

KRAMER: Sniffing, what do you mean sniffing?

JERRY: Sniffing, with his nose.

KRAMER: Jerry, he probably had a cold.

JERRY: No, I asked him.

KRAMER: So, what are you saying?

JERRY: I don't know, you know, what if...?

KRAMER: Drugs? You think he's on drugs?

JERRY: I don't know.

KRAMER: Jerry

JERRY: All I know he was sniffing.

KRAMER: Listen, we went in on a CD together.

JERRY: I know.

KRAMER: And Newman gave you money too. So, I didn't even meet this guy. You know, we trusted you.

JERRY: Look, it doesn't necessarily mean anything yet, it just means he was sniffing.

KRAMER: Well, what else? Was he nervous? Did he use a lot of slang? Did he use the word 'man'?

JERRY: No, he didn't use 'man'.

KRAMER: I mean when he was leaving did he say &quot;I'm splittin'&quot;?

JERRY: No, but in one point he did use the bathroom.

KRAMER: Whoh!

JERRY: Do you think that's a bad sign?

KRAMER: Yes!! Yes, that's what they do! They live in the bathroom! All right, what are we going to do? We are going to get our money back, right?

JERRY: I don't know. (takes off his sweater) This sweater really itches me. You want it?

KRAMER: (grabbing the sweater) Yeah.

[Elaine's apartment. Jake is there and Elaine comes in.]

ELAINE: Hello.... hello, oh...

JAKE: Well, you notice anything?

ELAINE: You have cleaned out the whole apartment and you're making dinner. You're perfect, you're a perfect man.

(Jake feels Elaine's coat material.)

JAKE: Ooh...

ELAINE: Did anyone call?

JAKE: You got a few messages, I wrote them down.

ELAINE: Where are they?

JAKE: Lets see, they are (looking for the paper; finds it; hands it to Elaine) here they are.

ELAINE: Thank you. (looking at the messages) Oh ya, heh, I'll call you back. Ooh, Myra had the baby! Oh, my God that's wonderful! Who called?

JAKE: She did.

ELAINE: She did? Oh, that's so great!

JAKE: Where do you keep the corkscrew?

ELAINE: In the drawer on the right. Hmm...

JAKE: What?

ELAINE: Nah it's nothing.

JAKE: What is it?

ELAINE: It's nothing.

JAKE: Tell me.

ELAINE: Well, I was just curious why you didn't use an exclamation point?

JAKE: What are you talking about?

ELAINE: See, right here you wrote &quot;Myra had the baby&quot;, but you didn't use an exclamation point.

JAKE: So?

ELAINE: So, it's ya nothing. Forget it, forget it, I just find it curious.

JAKE: What's so curious about it?

ELAINE: Well, I mean if one of your close friends had a baby and I left you a message about it, I would use an exclamation point.

JAKE: Well, maybe I don't use my exclamation points as haphazardly as you do.

ELAINE: You don't think that someone having a baby warrants an exclamation point.

JAKE: Hey look, I just chalked down the message. I didn't know I was required to capture the mood of each caller.

ELAINE: I just thought you would be a little more excited about a friend of mine having a baby.

JAKE: Ok, I'm excited. I just don't happen to like exclamation points.

ELAINE: Well, you know Jake, you should learn to use them. Like the way I'm talking right now, I would put an exclamation points at the end of all these sentences! On this one! And on that one!

JAKE: Well, you can put one on this one: I'm leaving!

(Elaine laughs as Jake leaves)

[5A. Jerry and Elaine]

JERRY: You're out of your mind you know that.

ELAINE: Why?

JERRY: It's an exclamation point! It's a line with a dot under it.

ELAINE: Well, I felt a call for one.

JERRY: A call for one, you know I thought I've heard everything. I've never heard a relationship being affected by a punctuation.

ELAINE: I found it very troubling that he didn't use one.

JERRY: George was right. Didn't take you long.

(Kramer enters)

KRAMER: Anything new with that guy on drugs?

ELAINE: Oh, he's not on drugs.

KRAMER: Then why the sniffing? Who walks around (sniff, sniff) sniffing?

ELAINE: All right, here, you call him right now. See if he's sniffing right now.

JERRY: Good idea. (Jerry calls Barry's office)

VOICE ON THE PHONE: Prophet and Goldstein.

JERRY: Yes, I'd like to speak to Barry Prophet, please.

VOICE: I'm sorry he's out of town this week.

JERRY: Out of town?

VOICE: Yes, he went to South America.

JERRY: South America?

KRAMER: South America?

JERRY: I'll call back, thank you. (hangs up the phone) He went to South America!

KRAMER: Yyyeeaaah!!

ELAINE: So what?

JERRY: Who goes to South America?

ELAINE: People go to South America.

JERRY: Yeah, and they come back with things taped to they're large intestine.

ELAINE: So, because of a few bad apples you're gonna impugn an entire continent?

JERRY: Yes, I'm impugning a continent.

KRAMER: Well, I say we're going to take our money right now!

(Newman enters)

NEWMAN: Hey, hey...

JERRY: Hello Newman.

NEWMAN: Hello Jerry. (to Kramer) So, any news?

KRAMER: Yeah, he skipped out and *ptruut* went to South America!

NEWMAN: South America?! (to Jerry) What kind of snow blower you get us mixed up with?

ELAINE: Look it, gentlemen. The fact remains you still have no proof. This is all speculation and hearsay.

KRAMER: Wait, there is one way to find out. We set up a sting. You know like Abscam. Like Abscam Jerry.

ELAINE: Ohh, what are you gonna do? You gonna put on phony beards and dress-up like Arab sheiks and sit around in some hotel room. I mean come on...

JERRY: Wait a second. Maybe there is someway we can tempt him and find out...

NEWMAN: If we put our three heads together we should come up with something.

(Jerry, Kramer and Newman begin thinking)

[Jerry's Car]

(Jerry, Kramer and Newman)

KRAMER: What's today?

NEWMAN: It's Thursday.

KRAMER: Really? Feels like Tuesday.

NEWMAN: Tuesday has no feel. Monday has a feel, Friday has a feel, Sunday has a feel....

KRAMER: I feel Tuesday and Wednesday...

JERRY: All right, shut up the both of you! You're making me nervous. Where is he already? He should've been out of work by now.

NEWMAN: Hey, you know this is kind of fun.

KRAMER: Yeah, maybe we oughta become private detectives...

JERRY: Yeah maybe you should.

KRAMER: Maybe I will.

NEWMAN: Yeah, me too.

JERRY: All right, what are you gonna say to him?

KRAMER: Just gonna find out if he's interested.

NEWMAN: Hey, hey maybe I should go with him?

JERRY: No, you stay here in the car.

NEWMAN: Who made you the leader?

JERRY: All right Newman, one more peep outta you, you're out of the whole operation!

NEWMAN: Alright.

JERRY: There he is. He's going into that bar.

KRAMER: All right, I'm going in.

JERRY: Be careful Kramer.

(Kramer leaves)

NEWMAN: I shoulda gone in with him.

JERRY: No, you stay here in the car. I may need you.

NEWMAN: What you need me in the car for?

JERRY: I might need you to get me a soda.

[Bar]

(Barry is drinking by the counter. Kramer enters)

KRAMER: I'll have a brewski, Charlie.

BARTENDER: The name's Mitch.

KRAMER: Oh, there's nothing like a cold one after a long day, eh?

BARRY: Yeah.

KRAMER: Oh yeah, I've been known to drink a beer or two. But then again, I've been known to do a lot of things. (Waiter opens the counter which hits Kramer on the head) Cigarette?

BARRY: (sniffs) No, I never touch them.

KRAMER: I suck 'em down like Coca-Cola. Well here's to feeling good all the time. (Kramer drinks the beer and smokes the cigarette at the same time. Barry sniffs) Looks like you've got yourself a little cold there, eh fella?

BARRY: I don't think so.

KRAMER: Me neither.

(Kramer puts the cigarette back in his mouth the wrong way)

[Jerry's Car]

(Newman is now in the passenger seat)

JERRY: You should try this new dental floss Glide, it's fantastic.

NEWMAN: I use dental tape.

JERRY: You should try this.

NEWMAN: I don't wanna.

JERRY: Not even once?

NEWMAN: No.

JERRY: You're an idiot.

NEWMAN: Why, because I use dental tape?

JERRY: Right, anyone who uses dental tape is an idiot.

[Bar]

KRAMER: South America?

BARRY: Yeah, yeah.

KRAMER: Well that's a burgeoning continent.

BARRY: Well, they are expanding their economic base.

KRAMER: Tell me about it.

BARRY: (sniffs) Excuse me, I gotta goto the bathroom.

KRAMER: I'm hip.

BARRY: Hip to the what?

KRAMER: To the whole scene. (sniff)

BARRY: What scene?

KRAMER: The bathroom scene. (moves his knows as you would if you sniffed)

BARRY: Listen, don't take this personally, but when I'm coming back I'm sitting over there.

KRAMER: What ever turns you on.

[Jerry's Car]

(Newman takes a dental floss out of his mouth.)

NEWMAN: No, no I don't like it.

JERRY: What do you mean you don't like it? How could you not like it?

NEWMAN: I like the thick tape.

(Newman puts dental floss on the dashboard. Jerry looks disgusted.)

[Bar]

(Kramer enters the bathroom, kicks the door open and takes a photograph)

BARRY: Heeyy!! What kind of a nut are you?

[George's job interview.]

FARKUS: So, basically George the job here is quite simple. Selling bras.

GEORGE: Well, that interests me very much Mr. Farkus.Very much indeed, sir.

FARKUS: Have you ever sold a woman's line before?

GEORGE: No, but um I have very good report with women, very good, comfortable. And from the first time I laid eyes on a brassieres, I was enthralled.

FARKUS: Hum. tell me about it.

GEORGE: Well, I was 14 years old. I was in my friends bathroom. His mother's brassieres were hanging on the shower rod and I picked it up, studied it. I thought, I like this. I didn't know what way or what level, but I knew: I wanted to be around brassieres.

FARKUS: That's an incredible story. You have a remarkable passion for brassieres.

GEORGE: Well, they're more than an underwear to me Mr. Farkus. Two cups in the front, two loops in the back. How do they do it?

FARKUS: Well, I think I can say, barring some unforeseen incident, that you will have a very bright future here at E.D. Granmont.

GEORGE: Thank you Mr.Farkus, thank you very much indeed sir.

FARKUS: See you monday 9 o'clock.

GEORGE: If you don't mind, sir. I'll be here at 8.

FARKUS: Excellent.

GEORGE: So long, Mr. Farkus.

(George leaves the office and goes to the elevator. A women there is waiting for the elevator. George feels her blouse material.)

MS. DE GRANMONT: What you're think you're doing?

GEORGE: Oh, nothing...

MS. DE GRANMONT: Farkus, get out here!

FARKUS: Yes, Ms. De Granmont?

MS. DE GRANMONT: Farkus, who is this pervert little weasel?

FARKUS: Uh, this is Costanza, he's our new bra salesman. (George's offers his hand to shake) He's supposed to start on monday.

MS. DE GRANMONT: If he's here on monday, you're not. Take a pick.

FARKUS: (to George): Get out! (to Ms. De Granmont) I'm terribly sorry Ms. De Granmont...

[Pendant publishing. Elaine is at Lippman's office.]

ELAINE: You wanted to see me, Mr. Lippman?

LIPPMAN: I was just uh going over the Jake Jarmel book and I understand you worked with him very closely on this.

ELAINE: Yes (clears her throat) yes I did.

LIPPMAN: And uh, anyway I was just reading your final edit and um, there seems to be an inordinate number of exclamation points.

ELAINE: Uh well um, I felt that the writing lacked certain emotion and intensity.

LIPPMAN: Ah, (reads an excerpt) &quot;It was damp and chilly afternoon, so I decided to put on my sweatshirt!&quot;

ELAINE: Right, well...

LIPPMAN: You put exclamation point after sweatshirt?

ELAINE: That's that's correct, I-I felt that the character doesn't like to be ch-ch-chilly...

LIPPMAN: I see, (reads another excerpt) &quot;I pulled the lever on the machine, but the Clark bar didn't come out!&quot; Exclamation point?

ELAINE: Well, yeah, you know how frustrating that can be when you keep putting quarters and quarters in to machine and then (prrt) nothing comes out...

LIPPMAN: Get rid of the exclamation points...

ELAINE: Ok, ok ok ...

LIPPMAN: I hate exclamation points...

ELAINE: ...ok I'll just....

[5A. Jerry, Kramer, Newman writing a letter. Elaine reads on the couch.]

JERRY: 'Dear Barry. Consider this letter to be official termination of our relationship effective immediately.'

KRAMER: Exclamation point.

ELAINE: You still have no proof.

KRAMER: Elaine, he was sniffing like crazy around me.

JERRY: 'I will expect all funds in form of cashier checks no later than the 18th'.

KRAMER: Double exclamation point!

NEWMAN: Will that take care of ours too?

JERRY: Yeah, I'll give you yours as soon as I get my money back.

NEWMAN: Hey, you want me to mail it? I'm on my way out anyway.

JERRY: Yeah, thanks.

NEWMAN: It'll be my pleasure.

(Kramer puts the photograph in the envelope. Newman and Kramer laughs and Newman leaves.)

NEWMAN: See ya later.

JERRY: You know this...

(Knock on the door. It's pizza guy)

KRAMER: Hey, Ralph.

JERRY: Hi Ralph.

RALPH: What's up fellas? That'll be 14.30.

JERRY: All right.

KRAMER: Mushrooms, you got mushrooms Jerry?

JERRY: Yeah.

(Ralph sniffs and rubs his eyes.)

KRAMER: What's the matter? You've got a cold?

RALPH: No man (sniffs again) Kramer, what is this?

KRAMER: It's a sweater.

RALPH: What is it made out of?

KRAMER: I don't know, Jerry gave it to me.

JERRY: Mohair, I think.

RALPH: Mohair, that figures, I'm allergic to mohair.

JERRY: You mean you just started sniffing?

RALPH: Yeah, mohair does it to me every time.

(Ralph leaves)

JERRY: I was wearing that sweater in the coffee shop when Barry came in.

KRAMER: Jerry, I was wearing it in the bar.

ELAINE: The sweater! The sweater made him sniff! See, I told you he wasn't a drug addict.

JERRY: Oh no! The letter, Newman, it's got exclamation points all over it!

KRAMER: Not to mention the picture of him on the toilet.

(Jerry leaves the door and comes back second later)

JERRY: The what??

(Kramer looks very confused, and smiles at Jerry)

[Newman is taking the letter to the mailbox. There's a woman at same time at the letterbox.]

NEWMAN: After you.

WOMAN: Thank you.

(Newman feels the woman's coat material.)

WOMAN: Get your hands off of me! Johnny!!! Johnny!

(Newman rushes away dropping the letter on the ground. Johnny chases after him)

[Costanza's]

FRANK: What do you mean you felt the material? What, with your fingers like this?

GEORGE: So what, what is so bad about that?

ESTELLE: Who goes around feeling people's material? What can be gained by feeling a person's material? It's insanity!

FRANK: What ever happened to &quot;Why, that's a lovely dress you have on. May I have this dance?&quot;!!

[Monk's]

(Kramer and Elaine at the booth behind the cash register)

ELAINE: You are really lucky Newman never mailed that letter.

(Jerry enters and tosses a letter on the table)

JERRY: Sorry I'm late, I just came from a meeting with my lawyer.

ELAINE: What is this?

JERRY: It's a letter from your friend Barry Prophet's lawyer.

ELAINE: He is filing a chapter eleven. Why, what's going on, why is he filing a chapter eleven?

JERRY: Bankruptcy, bankruptcy...as in I've taken your money and spent it on drugs!

ELAINE: What do you mean, I thought it was the sweater.

KRAMER: All right, What about the money?

JERRY: What about the money? Apparently if I had dissolved my relationship with him prior to his filing chapter eleven, I've could've got the money back. Which I would've done, if a certain imbecile had been able to get to a mailbox and mail a letter!!

(Newman enters and goes to the counter.)

NEWMAN: Pair of bear claws, please.

(Jerry approaches Newman, but some women comes between and feels Jerry's shirt.)

WOMAN: Nice.

JERRY: Think so?

WOMAN: Yeah, what is it?

JERRY: Half silk, half cotton, half linen. How can you go wrong?

[Closing Monologue]

My accountant actually did take a big chunk of money from me and use it to buy drugs and the thing that was hardest for me to comprehend about this is the life choice of drug abuse and accounting. But actually it makes sense I mean why would an athlete or a musician take drugs? They have an interesting job but an accountant if ever a job required some hallucinogenic support this is the job. That should be the legal defense, you're in court &quot;You're charged with possession of illegal narcotics&quot; &quot;But you're honor, I'm an accountant.&quot; &quot;Bang, case closed. Bailiff give this man back his peyote buttons and tequila back for the drive home. Sorry to bother you sir, terribly sorry.&quot;

The End
