---
layout: default
title: The Understudy
parent: Season 6
nav_order: 24
permalink: /the-understudy
cat: ep
series_ep: 110
pc: 621
season: 6
episode: 24
aired: May 18, 1995
written: Marjorie Gross & Carol Leifer
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697802
wiki: https://en.wikipedia.org/wiki/The_Understudy_(Seinfeld)
---

# The Understudy

| Season 6 - Episode 24                    | May 18, 1995              |
|:-----------------------------------------|:--------------------------|
| Written by Marjorie Gross & Carol Leifer | Directed by Andy Ackerman |
| Series Episode 110                       | Production Code 621       |

"The Understudy" is the 110th episode of the NBC sitcom Seinfeld. This was the 24th and final episode for the sixth season. It aired on May 18, 1995. This is the first episode in the series not to open with a stand-up routine.

## Plot

Jerry is dating Gennice, the understudy of stage performer Bette Midler, who bursts into tears for foolish (for instance, when she drops her hot dog at the park) but not expected reasons (like when her grandmother dies). In the opening moments of the episode, Jerry and Gennice are in his apartment watching the film Beaches (starring Midler), and she's sobbing. He can't decide whether to move from his chair to the couch to console her, but isn't inclined to.

During a softball game held in Central Park, in a parody of the 1994 Tonya Harding-Nancy Kerrigan scandal, George injures Midler, who's playing catcher, while he charges for home plate. While Midler goes to hospital, the understudy takes Midler's part in the musical Rochelle Rochelle. Gennice believes George did it all for her, but Kramer (a fan of Midler) is outraged at George, Gennice and Jerry:

> "So, my dear, you think you can get to Broadway. Well, let me tell you something. Broadway has no room for people like you. Not the Broadway I know. My Broadway takes people like you and eats them up and spits them out. My Broadway is the Broadway of Merman, and Martin, and Fontaine, and if you think you can build yourself up by knocking other people down... GOOD LUCK!"

Enraged New Yorkers turn against George, Jerry and Gennice, while Kramer nurses Midler back to health, fetching every food and drink she desires.

Meanwhile, Elaine brings Frank Costanza to her favorite beauty shop to translate the jokes being made at her expense by her Korean manicurists. Within moments, Frank realizes they're insulting him in Korean, and angrily confronts them. It happens that an old flame, Kim, is also working there, but Elaine is kicked out of the shop and banned for "spying." Despondent, she wanders the streets of New York on a rainy night, where she meets J. Peterman, and when they find themselves compatible in discussing clothing, she wins a new job.

Frank takes Kim out and discuss their future in his car. When he uses his "special move" on her, "stopping short" (see also "The Fusilli Jerry"), she gets angry and never wants to see him again.

At the premiere of the musical, Elaine (as a form of apology) brings along the Korean manicurists; however, when an announcement tells the audience that Midler will not perform (Gennice will perform instead), the manicurists get angry with Elaine and leave, leaving her once again despondent. When Gennice finally takes the stage, she has a problem with the laces on her boot and, in an act reminiscent of Harding's bootlace incident, tearfully asks that she be allowed to start over.

In a scene after the credits, Jerry is seen unlocking his front door; he overhears Kramer and Bette singing in his apartment.

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Cosmo Kramer

#### Recurring

Jerry Stiller ......................... Frank Costanza  
John O'Hurley .................... J. Peterman

#### Guests

Bette Midler ............................. Herself  
June Kyoko Lu ........................ Ruby  
Amy Hill .................................. Kim  
Adelaide Miller ........................ Gennice  
Bok Yun Chon ......................... Lotus  
Vonnie C. Rhee ........................ Sunny  
Craig Thomas .......................... Player #1  
Michael James McDonald ........ Player #2  
Lou DiMaggio ......................... Stagehand  
Jason Beck ............................... Umpire  
Bob Shaw ................................ Cabbie  
Johnny Silver ........................... Vendor #1  
William Bastiani ....................... Vendor #2

## Script

[Music from the movie &quot;Beaches&quot; playing on TV in the background]

GENNICE: (Crying) (sob sob)

JERRY: (To himself) Now what am I supposed to do here? Shall I go over there? It's not like somebody died. It's &quot;Beaches&quot; for god's sake. If she was sitting next to me I'd put my arm around her. I can't be making a big move like going all the way over there. I can't. I won't.

[Next day, Jerry and George enter Jerry's apartment]

JERRY: She calls me this morning and tells me she's upset I didn't console her. I mean it was &quot;Beaches&quot; for god's sake. What, what do you do in a situation like that?

GEORGE: Where were you?

JERRY: I was sitting on the chair. She was over here on the couch.

GEORGE: Well you know, if you were sitting right next to her you'd have to console her no matter what.

JERRY: Of course.

GEORGE: When you're talking about a movie like &quot;Beaches&quot;, moving from the chair to the couch , . . . that's quite a voyage.

JERRY: yeah,

(Kramer enters)

KRAMER: Hey.

GEORGE: Hey. I gotta go.

KRAMER: Where you going?

JERRY: The Improv is playing &quot;Rochelle Rochelle&quot; The musical.

KRAMER: Really? What is Bette Midler playing? Is she going to be there?

JERRY: She might be. She's the star of the show.

KRAMER: Bette Midler is going to be in the park today? Yeeee. Jerry, don't tease me.

GEORGE: I didn't know you were such a Bette Midler fan.

KRAMER: So maybe I'll go down there and watch, uh? She'll be there, ,maybe.

GEORGE: Gennice playing today?

JERRY: Yeah, maybe.

KRAMER: Who's Gennice?

JERRY: That's the understudy. I'm dating her.

KRAMER: Oh, uh, is this uh, Bette Midler's understudy?

JERRY: Yeah.

KRAMER: Oh, understudies are a very shifty bunch. The substitute teachers of the theater world.

JERRY: I'm glad she's an understudy. I don't have to avoid going back stage and having to think of something to say.

GEORGE: Going backstage is the worst. Especially when they stink. You know that's a real problem.

JERRY: Just once I would like to tell someone they stink. You know what? I didn't like the show. I didn't like you. It just really stunk. The whole thing...real bad. Stinkaroo. Thanks for the tickets though.

[Ruby's Nail Parlor - Korean language in quotes]

RUBY: You late.

ELAINE: I know I know. I didn't have change for the bus and they don't give change in this city. So they threw me off the bus

RUBY: &quot;That's a shame&quot;. You'll have to wait for Lotus now.

ELAINE: How long do you think this will take. I have a millllioooon things to do.

RUBY: &quot;Mustn't keep the princess waiting. Princess in a big hurry.&quot; &quot;No change for bus&quot; &quot;Poor princess.&quot;

ELAINE: What?, uh?

RUBY: Nothing, won't be long.

LOTUS: &quot;Princess wants a manicure.&quot;

SUNNY: &quot;Oh lucky me.&quot;

LOTUS: &quot;Oh, you got the princess.&quot;

ELAINE: What is so funny?

RUBY: ...tell knock-knock joke.

[Baseball diamond in the park]

ELAINE: The Korean women were talking about me. I think they were calling me a dog.

JERRY: How would you know? You don't speak Korean.

ELAINE: Because this woman came in with a dog and Ruby called the dog the same word they used when they were pointing at me...ge ge ge

KRAMER: You know, maybe in Korean &quot;dog&quot; isn't an insult. Could be like the word &quot;fox&quot; to us. Oh, she's a DOG!

JERRY: Why don't you go to another nail shop?

ELAINE: Because they're the best Jerry, the best. Look. Maybe I'm just being paranoid.

JERRY: What you need is a translator to go in the shop with you and tell you what they're saying.

ELAINE: Yeah, who speaks Korean?

JERRY: You know who speaks Korean?

ELAINE: No, who?

JERRY: George's father.

ELAINE: You gotta' be kidding me. How does he speak Korean?

JERRY: He used to go there a lot on business.

ELAINE: What did he do?

JERRY: He sold religious articles the statues of Jesus, the virgin Mary, that were manufactured in Korea.

ELAINE: Uh,

JERRY: George, does your father speak Korean?

GEORGE: Yeah, he once bumped into reverend Yung Sun Moon.

JERRY: Oh, hi Gennice.

GENNICE: Hi Jerry.

JERRY: This is George, this is Kramer.

GENNICE: Nice to meet you.

JERRY: Playing today?

GENNICE: No. I'm on the bench today.

JERRY: They really stick to that understudy rule.

KRAMER: So she's coming?

GENNICE: Oh, yeah, she'll be here. . . .(DROPS HOT DOG) Oh no, my frankfurter, my frankfurter fell (sob sob sob sob) . It was really good. I can't believe that I dropped it. (sob sob sob sob)

JERRY: It's okay...it's just a hot dog, (still sobbing) everything is going to be okay.

GENNICE: No it (sob) was really good.

(Crowd yelling - Bette arrives)

KRAMER: Look it's Bette It's Bette! Ah, Ah, AH, . . . Bette, psst, hi.

BETTE: Hi.

KRAMER: Uh, I just want to say I think you're wonderful.

BETTE: Uh, thank you.

KRAMER: Yeah, I've seen you in everything you've done.

BETTE: Really?

KRAMER: Anything I can get you? Water? They got ice over here.

BETTE: What flavours do they have?

KRAMER: Chocolate, Lemon, and uh, Cherry.

BETTE: How about Pineapple?

KRAMER: Pineapple, sure, alright, I'll be right back.

[First Italian ice cream seller]

ICE CREAM VENDER #1: No pineapple. Just Cherry, Lemon and Tutti-Frutti

KRAMER: Oh, uh, uh. (Leaves)

[Monk's]

ELAINE: Anyway, Mr. Costanza, what I want you to do is to come to the shop with me and tell me what they are saying. You do speak Korean?

FRANK: I once talked to the reverend Yung Son Moon. He bought two Jesus statues from me. He's a hell of a nice guy.

ELAINE: uh, ha.

FRANK: Ever see that face on him? Like a Biiig apple pie.

ELAINE: Yeah, yeah. Uh, uh listen Mr. Costanza, if uh, if you do this for me I'll get you a manicure, I'll pay for it. Or you can get a pedicure if you want.

FRANK: No one is touching my feet. Between you and me, Elaine, I think I've got a foot odour problem.

(Baseball diamond- Bette at bat)

GEORGE: I watched &quot;Beaches&quot; on cable last night. ...(wings?) ... Give me a break.

BETTE: Get some talent then you can mouth off.

UMPIRE: Strike three.

BETTE: What? Are you blind?

UMPIRE: What?

BETTE: Nothing, nothing.

[Monk's]

FRANK: I had an affair with a Korean woman.

ELAINE: Uh, Mr. Costanza, I ...

FRANK: No, I feel I need to unburden myself. I loved her very deeply. But the clash of cultures was too much. Her family would not accept me.

ELAINE: Mr. Costanza, I, ...

FRANK: Maybe it was because I refused to take off my shoes. Again, the foot odour problem. Her father would look at me and say, &quot; eno enoa juang &quot;. Which means, &quot;this guy - this is not my kind of guy&quot;.

[On Street]

ICE CREAM VENDER #2: Sure I got Pineapple.

(Kramer runs back with it)

[Baseball diamond - George at bat]

BETTE: Move it in. Move in everybody. Get your shrimp here. Shrimp on special today!

JERRY: Come on George, just loosen up.

(George hits one )

JERRY: Come on George.

KRAMER: I GOT THE PINEAPPLE. : I GOT THE PINEAPPLE.

JERRY: (to George) Keep going.

GEORGE: Aaaaaah

(George runs into Bette (trivia- but misses home plate))

UMPIRE: SAFE!

(Mob gets angry and chases George and Jerry)

GEORGE: come on it's just a game.

KRAMER: (holding bette) Don't worry Kramer is going to take care of everything. See,, I got you Pineapple. I saw Beaches last night for the fourth time (sings) &quot;You are the wind...&quot;

[Jerry's Apartment]

TV: the show will go on but not Bette Midler. While playing softball in the park Ms. Midler was injured when another player thoughtlessly rammed her at home plate. All captured on amateur video tape. She will be out for two weeks from her Broadway show; Rochelle Rochelle - The Musical.

(Genice enters)

GENNICE: . . . Thank you. (hugs Jerry and George)

JERRY: Well we...

GENNICE: No, please. This is the first time in my life (sobs) that anyone has ever done anything like this for me. I've always had to struggle so hard for everything I ever got. (sobbing) and I know this is going to be my big break. (sobbing)

JERRY: (with no emotion) It's okay. Everything is going to be all right.

(Knocking at door)

KRAMER: Come on Jerry, open up. I KNOW YOU'RE IN THERE!

JERRY: Come back another time.

(Shot of each lock being opened)

KRAMER: SO...you're all in here together. How convenient. I hope you're all proud of yourselves.

(Turning to Genice)

KRAMER: So my dear you think you can get to Broadway. Well, let me tell you something. Broadway has no room for people like you. Not the Broadway I know. My Broadway takes people like you and eats them up and spits them out. My Broadway is the Broadway of Merman, and Martin, and Fontaine, and if you think you can build yourself up by knocking other people down... ...GOOD LUCK... (exits)

[Nail Parlour]

ELAINE: Hi everyone. Um, this is my friend, Frank.

RUBY: What would you like today? Manicure, Pedicure?

FRANK: I'll take a manicure. I don't take my shoes off for anyone.

RUBY: &quot;That's the least of his problems.&quot;

FRANK: What was that?

ELAINE: What'd they say? What'd they say?

FRANK: They made a derogatory comment about me.

RUBY: &quot;She's with a man twice her age.&quot; &quot;He doesn't look like he's got much money either.&quot;

LOTUS: &quot;Check out that sweater&quot;. (all laughing)

RUBY: &quot;I think I saw a moth fly out of a pocket.&quot;

LOTUS: &quot;What happened to his tail?&quot;

FRANK: Okay, THAT'S IT! &quot;oki on awa&quot; Where's my tail? I heard every word you said. You got some nerve.

(Back room)

KIM: That voice? It sounds so familiar. It reminds me of when I was a young girl in Korea and I met an American businessman. He was a very unusual man. Quick tempered with a strange halting way of speaking. We fell in love but when I brought him home to meet my father? He refused to take his shoes off. And there was a terrible fight.

LOTUS: That man also refuses to take his shoes off.

FRANK: (from other room) I never seen people treated like this!

RUBY: You brought in a spy! . . . Get Out!

KIM: Frank?

FRANK: Kim?

[Hospital room]

KRAMER: (on phone) A turkey sandwich. A side of slaw, ... you want whit e meat or dark?

BETTE: White meat.

KRAMER: Yeah, white meat. And if I see one piece of dark meat on there. It's your ass buster.

BETTE: Get me one of those Black and White cookies.

KRAMER: yeah, all right, yeah.... (hangs up) They don't have any. But don't worry I'm going to get you one somewhere.

BETTE: Good. Because if I don't get a Black and White cookie I'm not going to be very pleasant to be around.

KRAMER: Now that's impossible.

[On rainy street at night]

ELAINE: (sob sob) (bumps into man with an umbrella) ... I don't even know where I'm going.

PETERMAN: That's the best way to get someplace you've never been.

ELAINE: yes, (sob) I suppose, ...

PETERMAN: Have you been crying?

ELAINE: Yes, (sob) you see this (sob) woman, this manicurist,...

PETERMAN: Oh no, that doesn't matter now. That's a very nice jacket.

ELAINE: Uh, (sob) thanks.

PETERMAN: Very soft, huge button flaps, cargo pockets, draw string waist, deep biswing vents in the back perfect for jumping into a gondola.

ELAINE: How do you know all that?

PETERMAN: That's my coat.

ELAINE: You mean..?

PETERMAN: Yes, I'm J. Peterman.

ELAINE: Oh!

[In Taxi]

JERRY: I don't know why I have to go to the hospital. I didn't do anything to Bette Midler. Driver can you stop over here, we're picking somebody up.

(Crowd yelling)

GENNICE: Hey, I didn't do anything. I was never informed. ... YOU CAN ALL GO STRAIGHT TO HELL.... You see that? You see what I am going through?

GEORGE: So what? Somebody dropped an egg on my head as I went into my building last night.

JERRY: Hey, I'm being heckled on stage. People are yelling out Galloogy.

GENNICE: I'm having a little trouble with all this. I mean all I ever wanted to do is sing. Now I'm the focus of this big media frenzy. (sob) Nobody in the show will even talk to me.

JERRY: Stop your crying will ya?

GENNICE: What?

GEORGE: You heard him.

GENNICE: Oh, don't you you're the reason this whole thing happened.

GEORGE: Oh, yeah. I read what you said to the papers yesterday. You weren't in on the planing. What planning? YOU THINK WE PLANED THIS? Uh?

CABBIE: Wait. Wait. I know you. You knocked Bette Midler out of Rochelle Rochelle the Musical. I want you creeps out of my cab.

JERRY: Hey, I had nothing to do with it.

CABBIE: Get out of my cab. You should go to prison. You should be in prison for the rest of your life. Get out, each of you. Each and every one of you get out of my cab.

[Restaurant]

PETERMAN: Then in the distance I heard the bulls. I began running as fast as I could. Fortunately I was wearing my Italian Captoe Oxfords. Sophisticated yet different; nothing to make a huge fuss about. Rich dark brown calfskin leather. Matching leather vent. Men's whole and half sizes 7 through 13. Price $135.00.

ELAINE: Oh, that's not too expensive.

PETERMAN: That shirt. Where did you get it?

ELAINE: Oh, this innocent looking shirt has something which isn't innocent at all. Touchability! Heavy, silky Italian cotton, with a fine almost terrycloth like feeling. Five button placket, relaxed fit, innocence and mayhem at once.

PETERMAN: That's NOT bad!

[Frank's car]

KIM: Oh, Frank. So many years. If only you had taken your shoes off.

FRANK: I couldn't because I had a potential foot problem.

KIM: I thought maybe you had a hole in your socks.

FRANK: I wiped them for two minutes on the mat. I don't know why your father had to make a federal case out of it.

KIM: Anyway that is all in the past. We have our whole future ahead of us.

FRANK: Between you and me I think your country is placing a lot of importance on shoe removal.

(Screeching of brakes)

KIM: You short stop me? We don't do that in Korea! Take me home. I never want to see you again.

[Bette's hospital room - Kramer enters]

KRAMER: Hi, here. I made this for your. (gives her a pasta statue)

BETTE: ... What is it?

KRAMER: It's macaroni Midler.

BETTE: Macaroni Midler?

KRAMER: Yeah, see how you're singing?

BETTE: ... yeah ...ha ha. Well you made a long journey from Milan to Minsk.

KRAMER: oh, what's that from?

BETTE: Oh, that's one of the songs from my show. Bette sings:

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &quot;Well you made a long journey from Milan to Minsk.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rochelle Rochelle.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; You never stopped hoping. now you're in the pink

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rochelle Rochelle

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; When the nay sayers nay you pick up your pace

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; So nothing's going to stop me so get out of my face.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; I'm having adventures all over the place.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Rochelle ROCHELLE!&quot;

KRAMER: Oh You are so freaking talented.

(knock at door - Kramer goes out into hall)

KRAMER: Oh, so look who's here. What do you want.

JERRY: We just want to talk to her. We want to apologize and tell her the whole thing was an accident.

KRAMER: No, no. I'm sorry it's out of the question.

JERRY: What?

KRAMER: Bette is recuperating right now and I'm not going to allow anything to disturb her.

GEORGE: Who are you to decide?

KRAMER: I'm calling the shots around here so there won't be anymore ACCIDENTS!

GEORGE: Hey, look, Kramer,...

KRAMER: AH! I don't want her disturbed.

[Outside Broadway Theater]

RUBY: hello, Elaine.

ELAINE: Hello

LOTUS: We're so excited.

ELAINE: You're welcome.

RUBY: We'll see you inside.

JERRY: What happened?

ELAINE: Well I felt bad about the spying, so you know, ,I got them tickets to the show.

JERRY: Oh,, that's nice. Alright, I'll see you later.

ELAINE: Wait, wait. I didn't get to tell you about my new job.

JERRY: Where?

ELAINE: Writing for the J. Peterman catalogue.

JERRY: (pushing her) How did you get that?

ELAINE: I met him.

JERRY: You met J. Peterman?

ELAINE: yeah.

JERRY: What is he like?

ELAINE: He wore a classic courtman's duster. beige corduroy collar, 100% cotton canvas, high waist, nine pockets, six on the outside, great for running along side a train, (Jerry walking away) waving last goodbyes, posing on a veranda, Men's sizes ...

JERRY: (walking away) Yeah, I'll see ya'.

ELAINE: . . . Small, Medium, Large, XL, Double...

[Backstage]

JERRY: Well, break a leg tonight.

GENNICE: I'm really nervous.

STAGEHAND: Here you got a telegram. Well, look who's here.

JERRY: Listen buddy,

STAGEHAND: What are you going to do? Break my legs? You don't scare me. You or your goons.

GENNICE: How do you like this?

JERRY: What is it?

GENNICE: My grandmother died.

JERRY: Oh, I'm so sorry.

GENNICE: Oh, it's okay,

JERRY: So you don't cry when your grandmother dies? But a hotdog makes you lose control?

VOICE: (Off camera) Places everyone.

GENNICE: I gotta go.

JERRY: Good luck.

[Audience]

ANNOUNCER: ladies and gentlemen for this evening's performance the part of Rochelle will be played by Gennice Grant.

LOTUS: Gennice Glant?

RUBY: What happened to Bette Midler?

ELAINE: Oh, she got hurt.

RUBY: No Bette Midler? (they talk Korean and all three leave)

ELAINE: I, uh, wait,...

[On Stage]

GENNICE: (singing and dancing) &quot;It's a long journey from Milan to Minsk...&quot; (Shoe lace comes undone (even LOOKS like figure skates) wait wait. Hold it stop, (sob) I'm sorry, I have to start it over, my shoelace. (sob) I can't do it like this. Please let me start over. (sob) Please. (sob) Please. . . .

The End
