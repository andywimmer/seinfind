---
layout: default
title: The Chronicle
parent: Season 9
nav_order: 21
permalink: /the-chronicle
cat: ep
series_ep: 177, 178
pc: 921, 922
season: 9
episode: 21, 22
aired: May 14, 1998
written: Darin Henry
directed: Andy Ackerman
imdb: https://www.imdb.com/title/tt0893212/
wiki: https://en.wikipedia.org/wiki/The_Chronicle_(Seinfeld)
---

# The Chronicle

| Season 9 - Episode 21 & 22 | May 14, 1998              |
|:---------------------------|:--------------------------|
| Written by Darin Henry     | Directed by Andy Ackerman |
| Series Episode 177 & 178   | Production Code 921 & 922 |

"The Chronicle" (also known as "The Clip Show") is an hour-long, two-part episode of the NBC sitcom Seinfeld. These were the 177th and 178th episodes of Seinfeld from the ninth and final season. It aired on May 14, 1998. Both parts of "The Chronicle" were seen by 58.53 million viewers. To accommodate the long running time of "The Finale," "The Chronicle" ran for 45 minutes on its initial airing. It was expanded to a full hour when rerun. While originally called "The Clip Show," its official title is "The Chronicle," as mentioned in the "Notes about Nothing" feature of Seinfeld, Volume 8, Season 9, Disc 4.

## Plot

### Part 1

Jerry, Elaine, George, and Kramer plan to go to the movies, but Jerry takes out a little time to look at nine years of memories. During the opening scene, Jerry breaks the fourth wall by talking directly to the audience, while Kramer and George, down the hall on their way to the movies, are still entirely in character and keep interrupting Jerry by yelling back at him, worried that they'll miss the previews.

The first montage of clips is set to John Williams' Superman score. Superman is Jerry Seinfeld's favorite superhero and is often referenced in the show. It plays out short clips of great moments of the series.

The swing music shows short clips of cast wearing different costumes and hairstyles throughout the series. That includes "The Puffy Shirt", "The Barber", "The Wig Master", "The Subway", and "The Reverse Peephole".

### Part 2

Jerry continues to reflect the nine years that have passed until Kramer (in character) comes up to get Jerry. He then breaks the fourth wall by noticing the camera and smiling.

More clips are seen and finally the closing minutes feature a series of bloopers, behind the scenes production, and a musical montage that features the song "Good Riddance (Time of Your Life)" by the band Green Day, from their 1997 album Nimrod.

Although not mentioned in "Notes About Nothing", during the musical montage of the beginning of this half of the show, "Don't Stop 'Til You Get Enough" by Michael Jackson plays during clips of the cast dancing. This montage also contains a spoiler of the final episode. At one point, Jerry, George and Elaine can be seen dancing down the hallway of a prison (a prison guard is standing by the door in the background).
