---
layout: default
title: The Engagement
parent: Season 7
nav_order: 1
permalink: /the-engagement
cat: ep
series_ep: 111
pc: 701
season: 77
episode: 1
aired: September 21, 1995
written: Larry David
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697690/
wiki: https://en.wikipedia.org/wiki/The_Engagement_(Seinfeld)
---

# The Engagement

| Season 7 - Episode 1   | September 21, 1995        |
|:-----------------------|:--------------------------|
| Written by Larry David | Directed by Andy Ackerman |
| Series Episode 111     | Production Code 701       |

"The Engagement" is the seventh-season opener (along with the 111th overall episode) of the NBC sitcom Seinfeld. It aired on September 21, 1995.

## Plot

George breaks up with a woman after she beats him in a game of chess. When he tells Jerry about it at Monk's, they both realize that they have done nothing with their lives and decide they are going to make some changes. However, Kramer warns Jerry against marriage. Jerry decides to remain with his singles lifestyle, unbeknownst to George.

Elaine comes in and tells Jerry and Kramer that a barking dog is keeping her from getting a good night's sleep. Kramer says he knows someone who can "fix her problem". They go to see Newman. After seeing Newman's clear dislike of dogs, Elaine becomes unsure, saying she can't allow a dog to be hurt. Kramer assures her they will merely kidnap it and relocate it to the countryside, where it will be happier. Later that night, Elaine, Kramer and Newman rent a van and prepare to steal the dog.

Contemplating his life, George visits the pier and watches various couples who are happy together, kissing, playing with their babies. George shows up at the door of his old girlfriend Susan Ross and asks her to marry him. The next day, George visits Jerry to tell him the news that he and Susan are engaged.

George eagerly inquires how things are with Melanie, but Jerry confesses they broke up; it irritates Jerry that she eats peas one at a time. George is angry as he and Jerry had a pact to change their lives.

Jerry goes to George's apartment and asks if he is ready to go see a movie (Firestorm with Harrison Ford), but George tells him Susan wants to see a different movie (The Muted Heart with Glenn Close and Sally Field). George is already showing signs of regret about his engagement.

At the theater, after the movies, Susan is weeping about the ending of The Muted Heart, while George looks as though he didn't enjoy it. George sees Jerry and his friend Mario Joyner passing them and enthusiastically talking about Firestorm.

Newman returns to the van with the dog (a Yorkshire Terrier) and Elaine is surprised that it is so small. They try to get it to bark to verify if it is the right dog, but with all the attention, the dog doesn't want to bark. Kramer drives them far away from the city, before finally stopping near Monticello. He gets out and leaves the dog at a random doorstep, but it rips off a piece of his shirt (with a tag from Rudy's vintage shop) before he can let it go.

Back home after the movie, Jerry calls George to inform him that a New York Yankees game is being rerun if he wanted to watch it, but Susan wants him to come to bed and watch an episode of Mad About You that she taped. George looks dismal at the prospect.

The dog finds its way back to its home and owner, carrying the piece of Kramer's shirt, and Elaine is again woken up during the night by its barking. The next day, Jerry tells her the news of George's engagement. With the shirt scrap as evidence, police officers visit Kramer and place him under arrest for dognapping. When they knock at Newman's door, he says with aplomb, "What took you so long?" The subplot ends with Kramer, Newman and Elaine sitting in the back of a police car. Elaine decides to make some changes with her life.

The episode ends with George and Susan watching TV. Susan looks happy whereas George looks clearly upset about the engagement.

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Cosmo Kramer

#### Recurring

Wayne Knight ................ Newman  
Jerry Stiller ..................... Frank Costanza  
Estelle Harris ................. Estelle Costanza  
Heidi Swedberg ............. Susan Biddle Ross

#### Guests

Mario Joyner ....................... Himself  
Janni Brenn ......................... Woman #2  
Mailon Rivera ...................... Cop #1  
Athena Massey ................... Melanie  
Ron Byron ......................... Man  
Renee Phillips ...................... Alice  
Cindy Cheung .................... Woman #1

## Script

[George's Apartment]

GEORGE: Well, you got no place to go. I'll tell you what your problem is: You brought your queen out too fast. What do you think? She's one of these feminists looking to get out of the house? No, the queen is old fashioned.. Likes to stay home. Cook. Take care of her man. Make sure he feels good.

LIZ: Checkmate.

GEORGE: I don't think we should see each other any more.

[At Elaine's. She's trying to get some rest.]

(dogs barking)

ELAINE: Shut up! Shut up! You stupid mutt.

[Monk's]

JERRY: And you broke up with her 'cuz she beat you at chess? That's pretty sick.

GEORGE: I don't see how I could perform sexually in a situation after something like that. I was completely emasculated. Anyway, it's not the only reason.

JERRY: Yeah, what else?

GEORGE: All right. You wanna know what one of her favorite expressions is? Happy, Pappy?

JERRY: Happy, Pappy? What does that mean?

GEORGE: Like if she wants to know if I'm pleased with something, she'll say, &quot;Happy, Pappy?&quot;

JERRY: Oh, you're &quot;Pappy&quot;.

GEORGE: I'm &quot;Pappy&quot;.

JERRY: Oh, I get it. Why don't you just say it?

GEORGE: Oh, come on. What, are you kidding? I'm much more comfortable criticizing people behind there backs. Anyway, look who's talking. You just broke up with with Melanie last week because she &quot;shushed you&quot; while you were watching TV.

JERRY: Hey, I got a real thing about &quot;shushing&quot;!

GEORGE: What is this? Did you ever get the feeling like you've had a haircut but you didn't have one? I'm all itchy back here.

JERRY: Ahh.

GEORGE: What?

JERRY: What is this? What are we doing? What in god's name are we doing?

GEORGE: What?

JERRY: OUR LIVES! What kind of lives are these? We're like children. We're not men.

GEORGE: No, we're not. We're not men.

JERRY: We come up with all these stupid reasons to break up with these women.

GEORGE: I know. I know. That's what I do. That's what I do.

JERRY: Are we going to be sitting here when we're sixty like two idiots?

GEORGE: We should be having dinner with our sons when we're sixty.

JERRY: We're pathetic... you know that?

GEORGE: Yeah, like I don't know that I'm pathetic.

JERRY: Why can't I be normal?

GEORGE: Yes. Me, too. I wanna be normal. Normal.

JERRY: It would be nice to care about someone.

GEORGE: Yes. Yes. Care. You know who I think about a lot? Remember Susan? The one that used to work for NBC?

JERRY: Hmm. I thought she became a lesbian.

GEORGE: No. It didn't take.

JERRY: Oh.

GEORGE: Did I tell you I ran into her last week? Ho-ho, she looked great.

JERRY: Hmm.

GEORGE: You thought she was good looking, right?

JERRY: See, there you go again. What is the difference what I think?

GEORGE: I was just curious.

JERRY: Well, this is it. I'm really gonna do something about my life, you know? You know, I think I'm gonna call Melanie again. So what if she shushed me. George, I am really gonna make some changes.

GEORGE: Yes. Changes.

JERRY: I'm serious about it.

GEORGE: Think I'M not?

JERRY: I'm not kidding.

GEORGE: Me, too.

[At JERRY:'s. He's on the phone]

JERRY: Melanie, you can shush me at every opportunity. 5? 3? Oh, it was just an expression. All right, well, that's very sweet of you. Okay, I'll call you later. All right, bye. Hey!

KRAMER: What?

JERRY: I had a very interesting lunch with George Costanza today.

KRAMER: Really?

JERRY: We were talking about our lives and we both kind of realized we're kids. We're not men.

KRAMER: So, then you asked yourselves, &quot;Isn't there something more to life?&quot;

JERRY: Yes. We did.

KRAMER: Yeah, well, let me clue you in on something. There isn't.

JERRY: There isn't?

KRAMER: Absolutely not. I mean, what are you thinking about, Jerry? Marriage? Family?

JERRY: Well...

KRAMER: They're prisons. Man made prisons. You're doing time. You get up in the morning. She's there. You go to sleep at night. She's there. It's like you gotta ask permission to use the bathroom. Is it all right if I use the bathroom now?

JERRY: Really?

KRAMER: Yeah, and you can forget about watching TV while you're eating.

JERRY: I can?

KRAMER: Oh, yeah. You know why? Because it's dinner time. And you know what you do at dinner?

JERRY: What?

KRAMER: You talk about your day. How was your day today? Did you have a good day today or a bad day today? Well, what kind of day was it? Well, I don't know. How about you? How was your day?

JERRY: Boy.

KRAMER: It's sad, Jerry. It's a sad state of affairs.

JERRY: I'm glad we had this talk.

KRAMER: Oh, you have no idea.

[Epiphany scene - on the pier]

GEORGE: La-la-la...

[Jerry's. Elaine pops in, she looks like something the cat just dragged in]

KRAMER: Hey.

JERRY: Hey.

ELAINE: Three hours of sleep again last night.. Three hours of sleep because of that dog.

KRAMER: What dog?

KRAMER: Yeah. What dog?

ELAINE: This dog in the courtyard across from my bedroom window that never never stops barking..

KRAMER: Don't...

ELAINE: I lost my voice screaming at this thing. I can't sleep. I can't work. I mean, I just moved. I can't move again. What am I gonna do? What? What am I gonna do?

KRAMER: Well, there is something you can do.

ELAINE: What? Kramer, I'll do anything.

KRAMER: Well, what if there should be an unfortunate accident?

JERRY: You're going to rub out the dog?

KRAMER: No, no. Not me. I just happen to know someone who specializes in exactly these kinds of sticky situations.

ELAINE: Uh-huh.

JERRY: What, you're considering this?

KRAMER: Look, just meet with him.

[Newman's - dark and dangerous]

ELAINE: I don't really know why I'm here. Kramer talked me into coming up here. But, obviously, I could never really do anything.

NEWMAN: Of course. Obviously.

ELAINE: Uh, so, anyway, I'm sorry for wasting your time.

NEWMAN: What kind of dog did you say it was?

ELAINE: Um, I don't know. I've never really seen it.

NEWMAN: I see many dogs on my mail route. I'll bet there's not one type of mutt or mongrel I haven't run across.

NEWMAN: If you ask me, they have no business living amongst us. Vile, USELESS BEASTS . . .

KRAMER: Newman! Stop it!!

NEWMAN: Anyway.

ELAINE: Yeah. Well um, I was just curious if I were interested in availing myself of your services, um, what exactly would you do?

NEWMAN: Well, Elaine, there's any number of things I could do. But, I can promise you this, though, this vicious beast will never bother you again. So, what's it going to be?

ELAINE: No, I'm sorry. I can't hurt a dog. I can't hurt a dog. I can't.

KRAMER: I got it. We'll kidnap him and we'll drop him off Upstate and this way he won't bother you anymore and he won't get hurt.

NEWMAN: Yeah, I suppose.

KRAMER: Huh?

ELAINE: I'd have to think about it. I doubt it, though. I doubt it. I'll let you know.

NEWMAN: Of course. Take your time. I be here.

[GEORGE: is still at the pier. His thoughts shift somewhat.]

GEORGE: La-la-la-la-la-la-la! [flashbacks to Susan]

[Elaine's - makes phone call]

ELAINE: All right. Let's do it.

NEWMAN: Excellent. Excellent.

[Restaurant]

JERRY: How come you're eating your peas one at a time?

MELANIE: What's the hurry?

[Susan's]

SUSAN: Who is it?

GEORGE: It's George.

SUSAN: George? George, what is it?

GEORGE: Will you marry me?

[George is on the phone.]

GEORGE: Ma, guess what!

MRS. COSTANZA: Oh, my god!

GEORGE: No, it's nothing bad. I'm getting married.

MRS. COSTANZA: You're what?

GEORGE: I'm getting married?

MRS. COSTANZA: Oh, my god! You're getting married?

GEORGE: Yes!

MRS. COSTANZA: Oh, I can't believe it. Frank, come here.

FRANK: You come here.

MRS. COSTANZA: Georgie's getting married.

FRANK: What?

MRS. COSTANZA: Georgie's getting married.

FRANK: Get the hell out of here. He's getting married?

MRS. COSTANZA: Yes.

FRANK: To a woman?

MRS. COSTANZA: Of course to a woman. What's she look like?

FRANK: I'm sure she's pretty gorgeous.

GEORGE: What difference does it make what she looks like?

MRS. COSTANZA: Is she pretty?

GEORGE: Yes, she's pretty. What difference does it make?

MRS. COSTANZA: Oh, I'm just curious.

FRANK: She's not pretty?

MRS. COSTANZA: Let me talk to her.

GEORGE: She wants to talk to you.

SUSAN: Uh, hello?

MRS. COSTANZA: Congratulations!

SUSAN: I just want you to know that I love your son very much.

MRS. COSTANZA: You do?

SUSAN: Yes.

MRS. COSTANZA: Really?

SUSAN: Yes.

MRS. COSTANZA: May I ask why?

FRANK: Okay...

MRS. COSTANZA: Will you stop. I'm on the telephone.

FRANK: Can I talk to her, please?

[Outside Jer's and Cosmo's apt.]

JERRY: Hey!

KRAMER: Hey!

JERRY: What are you up to?

KRAMER: Nothing.

JERRY: What's the rope for?

KRAMER: Oh! Well, How do you like that. I got rope. Um, I gotta go.

JERRY: The dog. You're getting the dog.

GEORGE: Hey, Kramer, where are you going?

KRAMER: Out.

GEORGE: Don't go. Kramer! Come back. I got great news. Well, I did it.

JERRY: Did what?

GEORGE: I got engaged. I'm getting married. I asked Susan to marry me. We're getting married this Christmas.

JERRY: You're getting married?

GEORGE: Yes!

JERRY: Oh, my god!

GEORGE: I'm a man. Jerry, I'm a man. And do you know why? It's because of that talk we had. You were my inspiration. Do you believe it? You. That lunch was the defining moment of my life.

JERRY: I'm blown away.

GEORGE: You're blown?

JERRY: Wow!

GEORGE: You like that?

JERRY: And she said &quot;Yes&quot;?

GEORGE: It took a couple of hours of convincing. I was persistent. I was just like those guys in the movies. And it worked! She said &quot;Yes&quot;! I can't believe my luck that she was still available. A beautiful woman like that. You think she's good looking, right?

JERRY: You're gonna have gorgeous kids.

GEORGE: Yes. She's got great skin - a rosy glow..

JERRY: Pinkish hue?

GEORGE: Oh, she's got the hue. So, what's going on with you and Melanie? I mean, I know you're not getting married, but uh, things are happening?

JERRY: Well... actually, we kind of broke up.

GEORGE: You what?

JERRY: Well, you know, we were having dinner the other night, and she's got this strangest habit. She eats her peas one at a time. You've never seen anything like it. It takes her an hour to finish them. I mean, we've had dinner other times. I've seen her eat Corn Niblets. But she scooped them.

GEORGE: ...she scooped her niblets?

JERRY: Yes. That's what was so vexing.

GEORGE: Uh-huh. Uh-huh. What about the pact?

JERRY: What?

GEORGE: What happened to the pact? We were both gonna change. We shook hands on a pact. Did you not shake my hand on it?

JERRY: You stuck your hand out, so I shook it. I don't know about a pact. Anyway, you should be happy you're engaged. You're getting married.

GEORGE: Well, it's not that. I just, you know, I thought that we were both uh...

JERRY: You thought I was gonna get married?

GEORGE: Well, maybe not married, but...

JERRY: I mean, you love Susan, right?

GEORGE: Yeah.

JERRY: You want to spend the rest of your life with her.

GEORGE: ...Yeah.

JERRY: So?

GEORGE: Yeah. It doesn't make any difference.

JERRY: No. So, we're still on to see uh, Firestorm tonight?

GEORGE: Yeah.

JERRY: I'll pick you up at your apartment. We'll go to the Eight.

GEORGE: Yeah.

JERRY: Hey, wait a second. Wait a second! Celebrate! How about some champagne?

GEORGE: Champagne?

JERRY: Yes, come on! How often do you get engaged? Come on!

GEORGE: Okay! Alright!

[Jerry searches through his cupboards]

JERRY: You know what? No champagne. Anyway... I'll see you later.

GEORGE: Yeah.

[Night outside]

ELAINE: That's it. That's it. Stop right here.

NEWMAN: All right, give me the rope.

ELAINE: What? What do you need a rope for?

NEWMAN: Look, I don't have time to explain every little thing to you.

ELAINE: I don't know. Now, I'm thinking maybe we shouldn't do this.

NEWMAN: I knew you'd back out.

ELAINE: Kramer, are we doing a bad thing?

KRAMER: Well, Look at it this way. We drop the dog off in front of somebody's house in the country. They find it and adopt it. Now the dog is prancing in the fields. Dancing and prancing. Fresh air.. Dandelions. We're doing this dog a huge favor.

ELAINE: Yeah. That's him.

NEWMAN: All right. I'm going in. Keep the motor running.

[George's.]

JERRY: Ready?

GEORGE: Um, I don't uh, really think I can go.

JERRY: Oh, how come?

GEORGE: Well, I didn't really tell Susan about it, and she doesn't really have anything else to do.

JERRY: Well, she could come.

GEORGE: Well, she doesn't really want to see Firestorm.

JERRY: Oh.

GEORGE: She um, she wants to see The Muted Heart.

JERRY: Oh, The Muted Heart. Glen Close. Sally Field. Well, that should be good.

GEORGE: Yeah. See you later.

JERRY: Hey, wait a second. You know, we could share a cab. They're playing at the same cineplex.

SUSAN: George, better get ready.

GEORGE: I am ready.

SUSAN: You wearing that shirt?

GEORGE: Okay, I guess I'll see you down there.

JERRY: Yeah.

GEORGE: Okay.

[In the van.]

ELAINE: What time you got?

KRAMER: Oh, no. I don't wear a watch.

ELAINE: What do you do?

KRAMER: Well, I tell time by the sun.

ELAINE: How close do you get?

KRAMER: Well, I can guess within an hour.

ELAINE: Tsk. Well, I can guess within the hour, and I don't even have to look at the sun.

KRAMER: Yeah.

ELAINE: Well, what about at night? What do you do then?

KRAMER: Well, night's tougher but it's only a couple of hours.

NEWMAN: Let's go. Let's go. Move!

KRAMER: You got it?

NEWMAN: What do you think? Drive. Drive.

ELAINE: Where is it? Where is it? This? This is the dog?

NEWMAN: Yep.

ELAINE: But he's so small.

NEWMAN: Yeah, but he's a fighter.

ELAINE: That can't be the dog. Are you sure you got the right one?

NEWMAN: Look, you said the one in the second courtyard. He was in the second courtyard.

ELAINE: How could that be the dog?

KRAMER: Well, get him to bark.

ELAINE: Yeah. Yeah. I'll know if he barks.

NEWMAN: All right. Bark.

ELAINE: Bark.

NEWMAN: Bark.

[At the theater]

SUSAN: [crying] Did you like it?

GEORGE: Yes, it was very, very good.

SUSAN: Oh, do you think he'll ever find her again?

GEORGE: Oh, I sure hope so.

JERRY: How about when Harrison Ford jumped out of that plane, and he was shooting back at them as he was falling?

FRIEND: What about that underwater escape?

JERRY: Oh, man!

[In the van]

KRAMER: Let's turn the radio on. Maybe there's a news report about it.

NEWMAN: ???????

ELAINE: You think we're far enough.

NEWMAN: We're practically to Monticello.

KRAMER: Yeah. This looks right. All right. Give me the dog. Okay, boy. This is it. This is your new home. Let go of my shirt. Come on. Let go of my shirt. This shirt is from Rudy's.

[Jerry's on the phone with his newly imprisoned buddy, Georgie.]

GEORGE: Hello?

JERRY: Hey, ??? is rerunning the Yankee game. You watching this?

GEORGE: They are?

SUSAN: George, you coming to bed? I'm taping Mad About You.

GEORGE: Uh, yeah, I'll be there in a minute.

JERRY: What was that?

GEORGE: Uh, nothing. I got to go.

JERRY: Oh, Mattingly just singled .

GEORGE: You know, it was really wrong of you to back out on that deal.

JERRY: I didn't make a deal. I just shook your hand.

GEORGE: Yeah, well that's a deal where I come from.

JERRY: We come from the same place.

SUSAN: George, I'm starting it.

GEORGE: I got to go.

[At the pooch's pad]

LADY: Roxy! Where have you been? We've been worried sick about you. What's this? Hmm. Rudy's.

[Elaine's bedroom - dog barking]

ELAINE: No! No! It's impossible. It's impossible.

[At Jerry's]

ELAINE: I don't know. I don't know how it happened. We were practically in Monticello. I mean, how could that thing have found its way back? There's no way.

JERRY: Very strange.

ELAINE: I know.

JERRY: So, tell me anyway. Who was the BIG mastermind?

ELAINE: Oh, I can't Jerry. I'm sworn to secrecy.

JERRY: All right. But then I can't tell you the BIG news.

ELAINE: News? What news?

JERRY: Sorry!

ELAINE: What? What?

JERRY: All right, Elaine but this is beyond news. This is like Pearl Harbor. Or the Kennedy assassination. It's like not even news. It's total shock.

ELAINE: Oh, come on, Jerry. Please, please, please, please, please!

JERRY: George Costanza...

ELAINE: Yeah?

JERRY: Is getting married!

ELAINE: Get out!

[Outside Kramer's apartment]

KRAMER: Hi.

COP: You Cosmo Kramer?

KRAMER: Uh, yes. Yeah.

COP: You recognize this piece of fabric?

KRAMER: Oh, yeah, that's...

COP: What?

KRAMER: What?

COP: You're under arrest.

KRAMER: Arrest?

COP: I have a receipt for a rental car with your signature. Including a report with some damage to the rear seat. It seems the spring was so compressed it completely collapsed the right side..

JERRY: Newman.

[Cop at Newman's door]

NEWMAN: What took you so long?

[back of a Cop car]

KRAMER: Hey, what do you think they'll do to us?

NEWMAN: Ah, don't worry about a thing. In twenty minutes that place'll be swarming with mailmen. We'll be back on the street by lunch.

ELAINE: I gotta make some changes. I'm not a woman. I'm a child. What kind of life is this?

(George and Susan in bed - Mad About You music)

The End
