---
layout: default
title: The Puerto Rican Day
parent: Season 9
nav_order: 20
permalink: /the-puerto-rican-day
cat: ep
series_ep: 176
pc: 920
season: 9
episode: 20
aired: May 7, 1998
written: Alec Berg, Jennifer Crittenden, Spike Feresten, Bruce Eric Kaplan, Gregg Kavet, Steve Koren, David Mandel, Dan O'Keefe, Andy Robin & Jeff Schaffer
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697761
wiki: https://en.wikipedia.org/wiki/The_Puerto_Rican_Day
---

# The Puerto Rican Day

| Season 9 - Episode 19                                                                                                                                         | April 23, 1998            |
|:--------------------------------------------------------------------------------------------------------------------------------------------------------------|:--------------------------|
| Written by Alec Berg, Jennifer Crittenden, Spike Feresten, Bruce Eric Kaplan, Gregg Kavet, Steve Koren, David Mandel, Dan O'Keefe, Andy Robin & Jeff Schaffer | Directed by Andy Ackerman |
| Series Episode 176                                                                                                                                            | Production Code 920       |

"The Puerto Rican Day" is the 176th episode of the NBC sitcom Seinfeld. It aired on May 7, 1998, and was the 20th episode of the ninth and final season. It was the show's second-highest-rated episode of all time, with 38.8 million viewers, only behind the series finale. The episode aired one week before the two-part clip show and the two-part series finale aired. Because of controversy surrounding a scene in which Cosmo Kramer accidentally burns and then stomps on the Puerto Rican flag, NBC was forced to apologize and had it banned from airing on the network again. Also, it was not initially part of the syndicated package. In the summer of 2002, the episode started to appear with the flag-burning sequence intact.

This episode of Seinfeld has more writer credits (ten) than any other episode. As co-creator Larry David was returning to write the finale, this was the final episode for the active "after Larry David" writing staff and thus was a group effort.

"The Puerto Rican Day" was a rare late-series return to a "plot about nothing" style, filmed in real-time, more commonly seen in early seasons (such as "The Chinese Restaurant").

## Plot

The gang are heading back to Manhattan after leaving a Mets game early in order to beat the traffic, but run into trouble with a driver in a maroon Volkswagen Golf. In the car, George boasts about the clever comment ("That's gotta hurt!") he recently made during a pivotal moment in a new film about the Hindenburg disaster titled Blimp.

As they approach Fifth Avenue, traffic is blocked by the annual Puerto Rican Day Parade. They almost find a way out, headed the wrong way down a one-way side street, but are blocked by their nemesis in the maroon Golf, whose driver refuses to let them cross over.

Elaine gets out of the car, worried about not being able to arrive home for her Sunday night "weekend wind-down" routine, which includes watching 60 Minutes. She starts walking only to find the traffic move again. She gets into a taxi (where she is oddly irritated by the sight of a dog with its ear flipped inside out), only to find the traffic immediately stop. Then, she leaves the taxi, only to find the traffic immediately moving again. She jumps back into the taxi.

George also leaves the car when he sees that the film Blimp is playing in a nearby theater, and he wants to repeat his funny comment for a new audience. But his attempt to be funny is undermined by a man with a laser pointer. When George makes the comment, nobody laughs because they are instead laughing at the laser. George berates the man, who then promptly shines the laser upon him.

Jerry and Kramer are finally allowed to take their shortcut when Jerry is forced to make an apologetic wave to the maroon Golf driver. As they pass by the Golf, Jerry calls the driver a "jackass", only to find himself blocked by oncoming traffic, including the taxi Elaine is in. The maroon Golf driver laughs and won't let him back up again.

A frustrated George then returns to the car, only to find the red dot of the laser pointer appearing all over parts of his body. A panicked George can't see the man holding the laser and worries he will go blind if it touches his eye.

Seeking an alternative way home, Elaine tries to find a way out by walking underneath the viewing stands. In a send up of The Poseidon Adventure, Elaine becomes the leader of a group of similarly distressed people trying to find their way out. She ultimately, however, leads them to a dead end where they have to scream for help to the people above.

Kramer, meanwhile, becomes desperate for a restroom and spots an apartment for sale across the street. To gain access to its restroom, he poses as H.E. Pennypacker, a wealthy industrialist, philanthropist, and bicyclist interested in the property. While there he sees the Mets game (they had left early) on the apartment's television. He excitedly tells Jerry, who also leaves the car and enters the apartment to watch, posing under his alias Kel Varnsen.

George thinks he spots the laser guy and plans a sneak attack, grabbing and breaking what he thinks is the laser pointer only to discover it is a pen. Back outside, Kramer accidentally sets the Puerto Rican flag on fire with a sparkler ("Dios mio!") and a mob of people, led by Bob and Cedric (in their third and final appearance), attacks him. He runs back into the apartment.

George also enters the apartment, as Art Vandelay, to wash the ink from his hands. Jerry then realizes that all three of them are in the apartment and nobody is watching the car. They look out the window to find it getting attacked by the angry mob. George then tells Jerry that the Mets lost, with Jerry sarcastically quipping "I love a parade!"

Later that night, when the parade's finally over, Jerry, George and Kramer see the car having been, somehow, stuck into a stairwell. Elaine arrives, her clothes and hair filthy with food. Then the maroon Golf driver passes by and calls Jerry a "jackass". Defeated, the gang starts walking home and Jerry says "remember where we parked", making reference to the season three episode "The Parking Garage". While they're shown walking from behind, it is seen that George (unbeknownst to him) still has the laser dot on his lower backside.

## Controversy

"The Puerto Rican Day" was criticized not only for the flag-burning incident, but also for the negative portrayal of Puerto Ricans, such as the scene where an angry mob of parade-goers damages Jerry's car, and Kramer later utters, "It's like this every day in Puerto Rico!" The episode sparked angry letters, protests outside NBC's Rockefeller Center in New York, and complaints from Puerto Rican activists. NBC formally apologized for the episode, and later pulled it from summer repeats.

## Cast

#### Regulars

Jerry Seinfeld ...................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus .............. Elaine Benes  
Michael Richards .................. Cosmo Kramer

#### Guests

Mario Joyner ......................... Lamar  
Dayton Callie ........................ Cabbie  
James Karen .......................... Mr. Canterman  
Helen Carey .......................... Mrs. Christine Nyhart  
Yul Vasquez .......................... Bob  
John Paragon ........................ Cedric  
Jenica Bergere ....................... Leslie  
Monica Allison ...................... Gail  
Marcelo Tubert ..................... Father  
Armando Molina .................. Amigo  
Tom Agna ............................. Gary  
Tom Dahlgren ...................... Priest  
Bert Rosario ......................... Man  
Raoul N. Rizik ...................... Parade Goer  
Scott Conte .......................... Sketch Guy  
Mimi Cozzens ....................... Mrs. Canterman  
Alison Martin ....................... Lucy  
Marc Hirschfeld ................... Ellis  
Chip Heller ........................... Policeman

## Script

[INT. JERRY'S CAR - DAY]

(The gang drives along in Jerry's car with the top down. Elaine and Kramer sit in the back, George is shotgun, and Jerry behind the wheel.)

GEORGE: Man, I'm starving.

ELAINE: How can you be hungry after what you ate at that Mets game?

GEORGE: Because ballpark food doesn't count as real food.

JERRY: Right. It's just an activity. It's like that paddle with the ball and the rubber band.

KRAMER: You know, my friend Bob Sacamano made a fortune off of those. See he came up with the idea for the rubber band. Before that, people would just hit the ball, and it would fly away.

JERRY: I can't believe you all made me leave before the end of the game.

ELAINE: Oh, come on, Jerry. It was 9 to nothing. We were getting shellacked.

GEORGE: Those nachos are killing me.

ELAINE: I thought you were hungry.

GEORGE: It's complicated.

KRAMER: Come on, Jerry, you're going to miss the exit.

JERRY: Keep your shirt on. I got it.

ELAINE: Watch out for that maroon Golf.

KRAMER: Oh, boy.

JERRY: Look at this guy. He's trying to box me out.

KRAMER: I'll tell you when you can go. Wait, wait, wait, Wait-- now, now, now. No, no, no. Go, go! No, no. Wait-- now, now! Now! Jerry! Go--ahh...

(Jerry swerves into another lane. Lamar, the driver of the maroon Golf honks the horn.)

JERRY: Oh, calm down, maroon Golf. He thinks I cut him off. He accelerated.

KRAMER: You want me to moon him? Ooh, let's moon him. Roll up your window. Let's do a pressed ham under glass.

ELAINE: Oh, no, I couldn't do that.

KRAMER: Look at this, look at this. He's giving us the finger.

ELAINE: Oh, alright.

KRAMER: Yeah.

(Elaine and Kramer get up to moon Lamar.)

[INT. JERRY'S CAR - DAY]

GEORGE: So I saw that new movie about the Hindenburg.

ELAINE: Oh, yeah. What's that called?

GEORGE: &quot;Blimp: The Hindenburg Story.&quot;

JERRY: How was it?

GEORGE: I found it morose. Why dwell on these negative themes?

JERRY: Yeah. They should make a movie about all the Hindenburg flights that made it.

GEORGE: Anyway, right in the middle, the ship blows up -- burning debris, bodies falling -- and then just as this eerie silence settles over the airfield, I yelled out, &quot;That's gotta hurt!&quot;

JERRY: Heh.

GEORGE: The place went nuts.

JERRY: Imagine the laugh you could have gotten if you'd yelled that out at the actual disaster.

GEORGE: Yeah.

KRAMER: Why are we slowing down?

(Music plays in the background.)

JERRY: What is that music?

GEORGE: What's with all these flags?

JERRY: Oh, no.

ELAINE &amp; JERRY: It's the Puerto Rican Day parade!

ELAINE: Ohh! Oh, the city shuts down Fifth avenue. They never let anyone through. We're never getting home.

(Kramer stands up on the back seat.)

KRAMER: Alright. I'm gonna check it out. Aiee. Mucho trafico.

[STOCK FOOTAGE: Puerto Rican Day parade.]

[EXT. CITY STREET - DAY]

(Kramer runs along the sidewalk and hops into the back seat of a black Saab.)

KRAMER: Yeah... uhh... well, the streets are all blocked. I think every Puerto Rican in the world is out here.

(There is a Puerto Rican family in the car.)

PUERTO RICAN MAN: Well, it is our day.

KRAMER: Whoo. Wrong car. Sorry.

(Kramer hops out.)

[INT. JERRY'S CAR - DAY]

(Still sitting stopped in traffic are Elaine, Jerry, and George. They are listening to the radio.)

RADIO: And the Mets score two in the eighth inning.

JERRY: See? If we had stayed, we could have seen those runs.

GEORGE: I could have had some ice cream. I think that might have calmed down the nachos.

ELAINE: I'm going to miss 60 Minutes. You know, I hate to miss 60 Minutes. It's part of my Sunday weekend wind-down.

JERRY: I don't know how you can unwind with that clock ticking. It makes me anxious.

KRAMER: Alright, gentlemen, I scouted it out. I think we can get out over there.

JERRY: But that's a one-way street coming this way. Besides, how am I gonna get all the way over there?

GEORGE: Just inch over. You worm your way.

ELAINE: Just do it, Jerry. Uhh. This exhaust. I'm gonna throw up.

KRAMER: You know, you should make yourself throw up.

ELAINE: Huh?

KRAMER: You know you're going to.

JERRY: Alright, I'm worming.

KRAMER: Hey, Jerry. You know who the grand marshal is of this thing? None other than Miss Chita Rivera.

JERRY: They're not letting me in.

GEORGE: My hand is out.

JERRY: Well, I think we're gonna need more than a hand. They have to see a human face.

ELAINE: You sure you want his face?

KRAMER: No, no, no. It was Mar&iacute;a Conchita Alonso.

GEORGE: This guy's giving me the stare-ahead.

JERRY: The stare-ahead. I hate that. I use it all the time.

GEORGE: Look at me! I am man! I am you!

(The man in the other car looks over at George. Elaine, sitting behind George, aids in the cause by pleading to the man by mouthing the words, &quot;Hi, can we go in?&quot;)

GEORGE: Alright, he's letting you in. Thank you! Creep.

KRAMER: Oh! I know who it is. Stacy Keach.

JERRY: One more lane to go.

GEORGE: Alright! We're here!

(Just as Jerry is about to enter the one-way street, Lamar and his maroon Golf cut him off.)

LAMAR: Oh, look who's here. My old buddy, black Saab.

JERRY: Maroon Golf.

LAMAR: Where you goin', black Saab? You seem to be a tad askew.

JERRY: Could you move your car back a little?

LAMAR: Oh. Sorry. I seem to have cut you off.

ELAINE: Alright, I think I know where this is going, and I am going somewhere else.

(Elaine pushes George's seat forward slamming his head onto the dashboard. She gets out.)

JERRY: You can't do that. You can't just leave the group.

ELAINE: I've been trying to leave this group for 10 years. Vaya con dios.

KRAMER: Con dios? Well, that's rude.

JERRY: Can you believe her?

GEORGE: Yeah. I'll see you later.

JERRY: Where are you going?

GEORGE: The movies. &quot;Blimp&quot; is playing right there.

JERRY: You're going to that again? Why? Just to do that stupid line?

GEORGE: It's a performance, Jerry. Like what you do.

JERRY: That's not what I do.

GEORGE: Isn't it?

JERRY: Maybe a little. Ah, hell, I guess it is.

(George leaves.)

KRAMER: You know, actually, Jerry, you haven't worked a room that big in a while.

[STOCK FOOTAGE: Taxis stuck in traffic].

[INT. TAXI CAB - DAY]

(Elaine sits in the back.)

ELAINE: Look at that guy's dog. I hate it when their ears get flipped inside out like that. Why doesn't he fix it?

(She moves so she can yell out the driver's open window.

ELAINE (yelling): Hey! Fold your dog's ear back!

(She leans back.)

ELAINE: Ooh! This isn't moving! I could walk faster than this.

CAB DRIVER: No, you can't.

ELAINE: Yes, I can. Here. I'm outta here.

(She pays her fare and gets out.)

[EXT. CITY SIDEWALK - CONTINUOUS]

(She starts to walk. The cab starts to move.)

ELAINE: Oh, now it's moving. Oh, yeah. I knew it. Hey! Hey!

(The cab stops and she gets back in.)

[INT. TAXI CAB - CONTINUOUS]

CAB DRIVER: Where to?

ELAINE: That's cute. That's really cute. Oh! Come on! Alright. Bye again.

(She pays and gets out again.)

[EXT. CITY SIDEWALK - CONTINUOUS]

(Again, the cab starts moving when she gets out.)

ELAINE: Hey. Taxi! Taxi!

(She starts to run after the taxi which starts to move faster.)

[INT. MOVIE THEATER - DAY]

(George sits down next to two attractive women.)

GEORGE: Ladies. I, uh, I haven't seen this before.

LADY 1: What is that dot?

LADY 2: Oh, I think someone has one of those funny laser pointers.

(A red laser moves around the movie screen. Everyone laughs as the dot rests on the breasts of the movie actress.)

[INT. MOVIE LOBBY - DAY]

(The laser pointer guy uses his laser to point at a box of Skittles and RC Cola.)

LASER GUY: Gimme a box of those and one of those.

GEORGE: Excuse me, are you the guy with that funny laser?

LASER GUY: The laser's not funny. I'm funny.

GEORGE: Yeah. The thing is, I, uh... I had this little zinger of my own I wanted to try.

LASER GUY: Uh-huh.

GEORGE: It's right in the explosion scene. So if you could just... leave me a little window. You know, my, uh, my aunt had a thing removed with a laser. Alright, I don't want to interrupt your meal, so...

[INT. JERRY'S CAR - DAY]

(Kramer and Jerry sit listening to the radio.)

RADIO What a comeback for the New York Mets-- 6 runs in the bottom of the ninth.

JERRY: I've gotta see this game. If it wasn't for this guy, we could get out of here.

LAMAR: This traffic's a killer, ain't it?

KRAMER: You want to get outta here? Here's what we do. We leave the car here, we take the plates off, we scratch the serial number off the engine block, and we walk away.

JERRY: Walk away?

KRAMER: You've got insurance. You tell them that the car was stolen, and then you get another one free.

JERRY: Isn't there a deductible?

KRAMER: Alright, what is your deductible?

JERRY: I don't know.

KRAMER: Yes, because they've already deducted it.

JERRY: From what?

KRAMER: The car, which we're leaving. So the net is zero. See you pocket the money, if there is any, and you get a new car.

JERRY: We're not leaving the car!

KRAMER: Alright. If you refuse to grow up and scam your insurance company, you'll have to work this out with maroon Golf.

JERRY: Absolutely not. He sped up.

RADIO: Swung on, line hard toward left center field. That's in the gap, that's a base hit.

JERRY: I'm ready to talk.

[INT. MOVIE THEATER - DAY]

(George and the ladies are sitting in their seats.)

LADY 1: Hey! There's that laser guy again.

LADY 2: He's funny. I never meet anyone funny.

LADY 1: I know. A sense of humor is so much more important to me than looks or hair.

LADY 2: Mmm, yeah.

(There's an explosion on the screen. The laser moves around the screen and everyone laughs.)

GEORGE: That's gotta hurt!

(Everyone stops laughing.)

GEORGE: It's... gotta hurt! Hurt! Because... Aaarrrrrgh!

(George stands up.)

GEORGE: Damn you, laser guy! You had to grab it all with your lowbrow laser shtick! You're just a prop comic! Where's the craft?!

(The laser guy points the laser at George's head.)

LADY 1: Look! It's on the bald guy.

LADY 2: I am so glad we came to this showing.

[EXT. CITY STREET - DAY]

(Kramer is talking to Lamar while Jerry waits in his car. Kramer walks to Jerry.)

KRAMER: Ok, here's the deal. He wants you to acknowledge that you cut him off with an &quot;I am sorry&quot; wave.

JERRY: What's that?

KRAMER: You raise the hand, lower the head -- &quot;I'm sorry, I'm sorry. The buttons are really big on the car. I don't understand it. I haven't read the manual. Ooh!&quot; You get my drift.

JERRY: Ok!

(Jerry puts both hands up over his head and does the &quot;I'm Sorry&quot; wave.)

LAMAR: Hallelujah. Praise the lord. But I'll take it.

KRAMER: Yes! Alright, Lamar, back it up a little bit so we can get out now.

(Kramer gets in the car. George shows up and climbs into the back seat.)

GEORGE: Alright. At last, we're finally gettin' out of here.

JERRY: What's that on your forehead?

GEORGE: It's probably chocolate.

JERRY: Hey, is that one of those laser pointers?

KRAMER: Hey, Jerry, crank up the Floyd. It's a George laserium!

GEORGE: Alright, stop it! Stay away from my breasts! Chest!

JERRY: See ya around maroon Golf. And, by the way, that was an &quot;I'm not sorry&quot; wave.

LAMAR: What was that?

JERRY: I'm glad I cut you off, because black Saab rules! So long, jackass!

(As Jerry pulls into the one way street, a taxi comes down the street and blocks him in. In the taxi is Elaine.)

JERRY: Elaine?!

ELAINE: Jerry?!

LAMAR: Jackass? So I'm a jackass now?

[INT. JERRY'S APARTMENT - DAY]

(The apartment is empty.)

[EXT. CITY STREET - DAY]

(Jerry stands in front of the one way street yelling at the cars.)

JERRY: So if everyone would just put their cars in reverse at the same time, we can do this. Alright, on the count of three. Can everyone hear me? Hey, amigo, are you paying attention?

PUERTO RICAN MAN: Buenos dias, my friend.

JERRY: Not you! The guy in the Amigo.

[INT. TAXI CAB - DAY]

(Elaine sits frustrated in the cab.)

ELAINE: Uh, well, uh, here--here is good.

TAXI DRIVER: Oh, yeah, sure, and now I'm gonna be stuck here. But you knew the way to go! You went to college!

ELAINE: Hey, I went to Tufts! That was my safety school! So don't talk to me about hardship.

(She pays the driver and gets out.)

[EXT. CITY STREET - DAY]

(Elaine walks up to Jerry.)

ELAINE: Boy, eh, can you believe this mess?

JERRY: Elaine, why did you have the cab come down the street?! We were almost out!

LAMAR: So that was your girlfriend that blocked you in. That's real good.

ELAINE: I'm not his girlfriend. Well, actually, we used to date, but not anymore.

JERRY: Elaine, he doesn't need-

LAMAR: Used to date? So I guess you found out he's a jackass.

JERRY: 'Cause that's what's gonna happen.

(Kramer and George walk along eating churros.)

KRAMER: Wow. He's givin' you a mustache. Where is this guy?

GEORGE: Don't look around. Don't look around. That's what he wants.

ELAINE: Alright. Well, I'll see ya. Hey, George, I think there's a sniper lookin' to pop ya.

(Elaine leaves.)

GEORGE: This thing can't hurt me, can it? I mean, it is a laser. What if it hits my eye?

JERRY: I don't know.

GEORGE: I can't be blind, Jerry The blind are courageous.

KRAMER: You'll be fine as long as it doesn't hit you right in the pupil, 'Cause then the whole ball will go up like the Death Star. Tchoo! I gotta go find a bathroom.

(Kramer leaves.)

JERRY: Hold it, George. Don't move. It's right between your eyes.

GEORGE: Oh, my god.

JERRY: Hey, there's the soda guy.

LAMAR: Hey, jackass! Get me a diet Dr. Pepper!

JERRY (exasperated): Alright!

(George stands petrified.)

[EXT. BARRICADE - DAY]

(A police barricade set up on the parade route prevents a crowd of people from crossing. Elaine pushes her way through the crowd.)

OLDER MAN: Hey, hey, hey!

OLDER WOMAN: Wha--ow!

ELAINE: Oh, this is nuts! I can't get across anywhere!

OLDER MAN: Well, none of us can! We're trapped!

OLDER WOMAN: Ow!

ELAINE: Hey! Hey, everyone. This way. I think we can get out through here.

(Elaine picks up a nylon flap that covers the sides of some bleachers.)

OLDER MAN: Oh, I don't know if that's such a good idea.

ELAINE: Look! No one knows how long this parade is gonna last! They are a very festive people. All I know is that it's Sunday night, and I have got to unwind! Now who's with me?!

OLDER WOMAN: Father?

PRIEST: None of us saw the nylon flap. That might mean something.

PREGNANT WOMAN: Oh, alright, alright!

(People start going under the bleachers.)

ELAINE: Alright! Come on. Come on. Let's go. Let's go.

BUSINESS MAN: But it's dark!

ELAINE: Get in there!

[EXT. CITY SIDEWALK - DAY]

(Kramer looks for a bathroom. He sees a sign that reads, &quot;Restrooms are for patrons only.&quot; There's another sign that reads apartment for sale.)

[INT. APARTMENT - DAY]

(There's a knock at the door and the sales woman answers it. It's Kramer.)

KRAMER: Yes, uh, I'm interested in the apartment.

SALES WOMAN: Yes! Come in, come in.

KRAMER: Ok.

SALES WOMAN: I'm Christine Nyhart.

KRAMER: Oh. Delicious to meet you.

SALES WOMAN: Did the broker send you over?

KRAMER: Uh, yes, most likely, yes. I'm, uh, H.E. Pennypacker. I'm a wealthy industrialist and philanthropist and, uh, a bicyclist. And, um, yes, I'm looking for a place where I can settle down with my, uh, peculiar habits, and, uh, the women that I frequent with. (sniffing wall) Mmm. Mombassa, hmm?

SALES WOMAN: The asking price is $1.5 million.

KRAMER: Oh, I spend that much on aftershave. Yes, I buy and sell men like myself every day. Now, I assume that there's a waterfall grotto?

SALES WOMAN: No.

KRAMER: How about a bathroom?

SALES WOMAN: It has 4.

KRAMER: Yes, and where would the absolute nearest one be?

SALES WOMAN: Just down the hall.

KRAMER: Oh, thank you.

(He saunters to the bathroom.)

[INT. BLEACHERS - DAY]

(Elaine leads everyone under the bleachers. Food falls from the bleachers landing on them.)

ELAINE: Oh, don't worry. We'll get you home to your husband real soon.

PREGNANT WOMAN: I'm not married.

ELAINE: Well, I, for one, really respect that.

PREGNANT WOMAN: Oh, thank you.

ELAINE (whispering): Hey! Guess who's not married.

OLDER MAN: Is the boyfriend still in the picture?

ELAINE: Come on, father, you can make it.

PRIEST: No, I can't. I've got a bad hip. Go on without me.

ELAINE: No! I won't!

PRIEST: Leave me! you must.

ELAINE: Alright. Take it easy.

ELAINE: Alright, we can move faster without father o'gimpy.

PRIEST: I heard that!

[EXT. CITY STREET - DAY]

(Jerry stands next to his car with a drink.)

LAMAR: You know, I don't think I've ever seen a man driving a Saab convertible. Still haven't.

JERRY (sarcastically): Ho ho!

(George returns wearing mirrored sunglasses.)

JERRY: What seems to be the problem, officer?

GEORGE: They're for protection, Jerry. Can you tell where I'm lookin'?

JERRY: At me?

GEORGE: No.

JERRY: Oh. It's back.

GEORGE: Bring it on, baby

JERRY: What if it gets in the side?

GEORGE: The side?

JERRY: Yeah. Wouldn't it just bounce back and forth between your cornea and the mirror, faster and faster, getting more and more intense, until finally-

(George rips off his glasses.)

GEORGE: Alright!

JERRY: Oh. It's in your eye now.

(George runs off screaming. Kramer runs up to Jerry with a Puerto Rican flag draped around him.)

KRAMER: Hola, Jerry! I'm into this Puerto Rican day! The sights! The sounds! The hot, spicy flavor of it all! It's caliente, Jerry!

JERRY: Kramer, the Mets have got men on base!

KRAMER: Yeah, I know! I was watchin' the game.

JERRY: You were watchin'? Where?

[INT. APARTMENT - DAY]

(Jerry sits in the apartment watching TV.)

JERRY: Oh, that was a strike! Did you see that?!

SALES WOMAN: Would you like to see the rest of the apartment, Mister, um--

JERRY: Eh... Varnsen. Kel Varnsen. Actually, this room intrigues me. Why is it called the TV room?

SALES WOMAN: Well, it's--

JERRY: Balk?! How was that a balk?! You have any snacks?

SALES WOMAN: Mr. Varnsen, if you like the apartment, I should tell you I've also had some interest from a wealthy industrialist.

JERRY: Not Pennypacker!

SALES WOMAN: You know him?

JERRY: I wish I didn't. Brace yourself, madam, for an all-out bidding war. But this time, advantage Varnsen!

[INT. JERRY'S CAR - DAY]

(George and Kramer sit in the car. George looks in the rear view mirror.)

GEORGE: Wait a second. I think I see where that laser guy is. No! Don't look! Don't look. Oh, yeah, that's him. Ok. I'm gonna sneak up on him. Now the hunted becomes the hunter.

(George exits the car.)

[INT. BLEACHERS - DAY]

(Elaine and the group reach the end of the bleachers.)

ELAINE: We should be able to get across right through here!

(She lifts the nylon flap to find a brick wall.)

OLDER WOMAN: It's a dead end!

ELAINE: Oh, no! I thought--

BUSINESS MAN: You thought?! We're gonna die in the dark! I knew it! I knew it! We're gonna die!

ELAINE: Get a hold of yourself!

(Elaine slaps the man. He shoves her, she shoves back. They grab each other and kiss.)

PREGNANT WOMAN: Oh, come on!

OLDER WOMAN Oooh!

ELAINE: Sorry. Somebody... help us!

[EXT. BLEACHERS - DAY]

(Lots of people sit on the bleachers. A cop stands guard.)

MAN: !Mira! !Mira! Stacy Keach!

ELAINE: We're down here! Help!

MAN: There's people down there! Hold on!

(They lift the floorboards to reveal Elaine and her group.)

ELAINE: Let us out. There's an unmarried pregnant woman down here.

PREGNANT WOMAN: Don't judge me!

ELAINE: Help us up so we can cross the street?

POLICE OFFICER: Nah, nah, You can't cross here. There's a parade.

ELAINE: But we've come so far. We just want to unwind.

POLICE OFFICER: Hey, what can I tell ya?

(They close the floor boards.)

[INT. BLEACHERS - DAY]

BUSINESS MAN: Wanna make out some more?

ELAINE: Oh, god! Let us out!

[EXT. CITY SIDEWALK - DAY]

(George hides behind a van and sees a man with a pen. He sneaks up and grabs the pen and destroys it. He ends up with ink all over his hands.)

GEORGE: That wasn't a laser pen.

DELIVERY MAN: No. It's just a pen.

(The delivery man laughs.)

GEORGE: Oh, that's funny

DELIVERY MAN: No. You have, like, a dot on your face. Whoever's doing that is very clever.

[EXT. CITY STREET - DAY]

(Kramer lights a cigar with a sparkler.)

KRAMER: Come on, man. You need to lighten up. You know, a feeling like this only happens once a year.

(He tosses the sparkler into the back seat of Jerry's car where it lands on the Puerto Rican flag.)

KRAMER: Yeah, it's like this every day in Puerto Rico.

(Lamar starts to laugh as he sees the smoke rise.)

KRAMER: See, now you're getting the spirit of it, huh?

(Kramer smells the smoke, turns, and sees the fire.)

KRAMER: Ooh! !Dios mio!

(He grabs the flag, throws it onto the sidewalk and stops on it.)

MAN: Hey! There's a guy burning the Puerto Rican flag!

BOB: Who! Who is burning the flag?!

KRAMER: Oh, no.

BOB: Him?!

CEDRIC: That's not very nice.

KRAMER: It was an accident.

BOB: Do you know what day this is? Because I know what day this is, they know what day this is, so I was wondering if you know what day this is!

CEDRIC: Because it's Puerto Rican day.

BOB: Maybe we should stomp you like you stomp the flag! What do you think of that?

KRAMER: Now look, I just have one thing to say to you boys. Mama!

(Kramer runs off screaming. The others give chase.)

[INT. APARTMENT - DAY]

(The sales woman lets George in.)

SALES WOMAN: Right this way, Mr. Vandelay.

GEORGE: Well, this is a lovely apartment. Lovely! My kids are gonna go crazy. I, uh, I wonder if I could see the bathrooms. Preferably one with some paint thinner and, uh, some rags?

SALES WOMAN: It's down the hall.

(George walks to the bathroom, but stops when he sees Jerry.)

JERRY: Oh, hello...

GEORGE: Art.

JERRY: Mr. Vandelay, of course.

SALES WOMAN: You two know each other?

(Kramer bursts through the door.)

SALES WOMAN: Mr. Pennypacker!

KRAMER: Uh, yes, uh, I--I wanted to, uh, stop by and make sure that my shark tank fits -- uh, hello.

SALES WOMAN: Mr. Pennypacker, this is Mr. Vandelay, And you know Mr. Varnsen

KRAMER: Uh, Varnsen.

JERRY: Pennypacker.

KRAMER: Vandelay.

GEORGE: Pennypacker. Varnsen.

JERRY: Vandelay. Wait a second. Mr. Pennypacker, if you're here, and Mr. Vandelay is also here, then who's watching the factory?

KRAMER: The factory?

JERRY: The Saab factory?

KRAMER: Jerry, that's in Sweden.

(George turns on the TV as Jerry runs to the window just in time to see a mob attacking his car.)

JERRY: My car!

KRAMER: Well, you know, it's like this every day in Puerto Rico.

GEORGE: Jerry, the Mets lost.

JERRY: I love a parade!

[EXT. CITY SIDEWALK - NIGHT]

(Jerry's car is stuck in a stairwell outside a building. Kramer, George, and Jerry admire the mob's handiwork.)

GEORGE: How do you suppose they did that?

KRAMER: Well... there's no logical explanation. Alright. Well, shall we go home?

JERRY: Well, what about my car?

KRAMER: Well, Jerry, you can't deduct it now.

(Elaine approaches the group. She is totally disheveled with popcorn in her hair and her clothing stained.)

JERRY: Hey, there's Elaine.

ELAINE: Hey.

JERRY: Well, you look, uh... relaxed.

ELAINE: Well, it is Sunday night, and you know how I like to unwind.

(Lamar drives up in his maroon Golf.)

LAMAR: Hey, black Saab. Looks like that building cut you off! Ha ha ha! See ya around!

(He drives off.)

JERRY: Well, at least he didn't-

LAMAR: Jackass!

(Jerry sets his car alarm.)

JERRY: Somebody remember where we parked.

KRAMER: This was a fun day. It's nice to get out.

(The foursome walk off as a laser lights up George's rear end.)

The End
