---
layout: default
title: The Bottle Deposit
parent: Season 7
nav_order: 21
permalink: /the-bottle-deposit
cat: ep
series_ep: 131, 132
pc: 721, 722
season: 7
episode: 21, 22
aired: May 2, 1996
written: Gregg Kavet & Andy Robin
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697660/
wiki: https://en.wikipedia.org/wiki/The_Bottle_Deposit
---

# The Bottle Deposit

| Season 7 - Episode 21 & 22          | May 2, 1996               |
|:------------------------------------|:--------------------------|
| Written by Gregg Kavet & Andy Robin | Directed by Andy Ackerman |
| Series Episode 131 & 132            | Production Code 721 & 722 |

"The Bottle Deposit" is a two-part episode, and the 131st and 132nd episode and 21st and 22nd episode of the seventh season of the NBC sitcom Seinfeld. It aired on May 2, 1996. This was originally an hour-long episode, but it was split into two parts for syndication.

The episode was written by Gregg Kavet and Andy Robin. The director was Andy Ackerman.

## Plot

### Part 1

Since he will be out of town, Mr. Peterman wants Elaine to bid for him on a set of golf clubs owned by John F. Kennedy at an auction. He tells her he is willing to go as high as $10,000 for the clubs. Jerry thinks he hears a strange clunking noise in his car and asks Kramer and Newman, who had previously borrowed the car, about it, but they don't know anything. Newman learns that bottles and cans can be refunded for 10 cents in Michigan (as opposed to 5 cents in New York and many other states), but Kramer tells him it's impossible to gain a profit from depositing the bottles in Michigan due to the total gas, tollbooth and truck rental fees that would compile during the trip, noting his own failed attempts because he "couldn't crunch the numbers." Newman becomes obsessed with finding a way to make such a scheme work.

Meanwhile, Mr. Wilhelm is scolding George for needing to have orders repeated to him. Shortly after, Mr. Wilhelm begins talking of a big project for him to do just as he enters the bathroom. After waiting outside for a short while, George decides to go in, too. But when he enters, he finds that Wilhelm, who had thought George had followed him inside the entire time, had unknowingly been telling about the details of the big project out loud to nobody. Not wanting him to think he wasn't paying attention, George pretends he heard everything. He later asks Jerry what to do and Jerry tells him to ask Wilhelm a follow-up question.

Jerry then takes Elaine to the auction, where they bump into Sue Ellen Mischke, the bra-less "Oh Henry!" candy bar heiress, who taunts them: "Come to catch a glimpse of high society?". During the bidding, they start a bidding war over JFK's clubs, and Elaine ends up paying $20,000, twice what she was authorized by Peterman to spend. Jerry again hears a loud clunking noise while dropping Elaine off. Elaine decides to leave the clubs in Jerry's car and pick them up later. As he starts to leave, smoke begins billowing out from under the hood and Jerry finds out that Kramer and Newman had left some groceries in his car engine.

He visits Tony (Brad Garrett), a mechanic who is obsessed with car care. George tries to use Jerry's suggestion at work the next day, and Wilhelm unknowingly drops a hint: to get started, he first has to go to payroll. There, the clerk gives George a hard time because he's not being specific enough about "the project." The clerk calls Wilhelm to verify George's claims, but doesn't drop George any further hints.

Meanwhile, Newman, who has spent days trying to calculate a profit to the deposit scheme, realizes that there will be a surge of mail the week before Mother's Day (the "mother of all mail days") to be sorted in Saginaw, Michigan. He tells Kramer that he signed up for a mail truck that would carry spillover mail from the other four main trucks, leaving plenty of space left over in theirs for bottles and cans to refund in Michigan. Kramer realizes that by avoiding truck rental fees, Newman has found a loophole and they set off collecting cans and bottles around the city.

Wilhelm visits George to see how he is doing with the project. George informs him that he went down to payroll and Wilhelm asks if he is going downtown then. When asked if "going downtown is really necessary for the project", Wilhelm tells George that he has to go downtown, and mentions the Petula Clark song 'Downtown.' Thinking it's another clue, George and Jerry try to decipher it, but to no avail. George considers coming clean and admitting to Wilhelm that he has no idea what the project is. Jerry goes home and finds a message from Tony saying he needs to talk to him at the Auto Shop. Elaine calls shortly after and wants to pick up Peterman's golf clubs. Jerry tells her he left them in the car at the mechanic's, so they decide to meet up there.

Tony wants to make a lot of changes to the car, but Jerry doesn't want to spend so much money. He asks Tony if he could just have it back so he can take his business elsewhere. Tony is disappointed, but tells him he'll bring the car out front for him. Elaine arrives and meets Jerry to pick up the clubs just in time to see Tony driving away with Jerry's car.

### Part 2

Mr. Wilhelm is delighted with the job George did on the project; however, George has no idea what he did or how he did it. Unknowingly to George, Mr. Wilhelm had forgotten to take his medication and did it himself, which would explain his compliment (he did the same thing with the gardener at home).

While riding in the mail truck with Newman, a surprised Kramer suddenly spots Jerry's stolen car on an Ohio highway and alerts Jerry by using a mobile phone that he brought along. Newman and Kramer quickly argue whether to deliver their mail and empty bottles to Saginaw, Michigan as they had planned, or to pursue Jerry's stolen car as it exits the highway in Ohio, to which Kramer agrees.

George is sent to a mental hospital by Steinbrenner, due to George's "report". At the mental hospital, George bumps into Deena, (from "The Gum"), who believes George is finally getting the help he needs.

While still chasing Jerry's car, Kramer dumps their empty bottles to make the truck move faster and soon after, dumps Newman. Newman then finds a farmer's house, complete with his proverbial daughter. As Kramer continues his chase, Tony throws all of the JFK golfclubs at him, and Kramer is soon forced to give up the chase when the van gives out from the damage the clubs caused to it. Newman violates the farmer's only rule, to keep his hands off his daughter, and he and Kramer run away while being shot at. The daughter stops her father, but calls Newman "Norman" as she professes her love for him and bids him goodbye. Peterman's golf clubs (a valuable collectors' item) are returned (albeit in a bent and battered state), but not Jerry's car. Elaine gives the bent golf clubs to Mr. Peterman, who mistakes it being in a battered state by thinking Kennedy was an angry golfer.

## Production


*   This is Larry David's last voice over while still working as a writer for the show, in which he voiced George Steinbrenner, although he would continue to voice Steinbrenner for all of his future appearances until the end of the show.

*   This is the first Seinfeld episode to depict one of the main characters using a cell phone.

*   The farmer's daughter's cry of "Goodbye, Norman! Goodbye!" at the end of the episode was not originally scripted. Actress Karen Lynn Scott forgot that Wayne Knight's character was called Newman and accidentally called him "Norman", but the goof actually made the scene funnier, so it was kept in. Newman's first name was never revealed during the series. The actress's mistake and director Andy Ackerman's comments on why it was left in can be found on the Season 7 DVD's extra features.

## Cast

### Part 1

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Cosmo Kramer

#### Recurring

Wayne Knight ................ Newman  
Richard Herd ................. Mr. Wilhelm  
John O'Hurley ................ J. Peterman

#### Guests

Brad Garrett ........................ Tony  
Brenda Strong ..................... Sue Ellen Mischke  
Patrick Kerr ......................... Clerk  
Harvey Jason ...................... Auctioneer  
Larry Polson ........................ Homeless Guy

### Part 2

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Cosmo Kramer

#### Recurring

Wayne Knight ................. Newman  
Richard Herd .................. Mr. Wilhelm  
John O'Hurley ................. J. Peterman

#### Guests

Brad Garrett ........................ Tony  
Mary Jo Keenen ................... Deena  
Rance Howard ..................... Farmer  
Nicholas Mele ....................... Detective  
Karen Lynn Scott ................. Farmer's Daughter  
Sandy Ward ......................... Pop  
Dan O'Connor ...................... Young Cop  
Bonnie McNeil ...................... Woman

## Script

[Opening Monologue]

I love it when you get your car back from the car place, and it's got that paper mat on the floor. Like they're so obsessed with cleanliness, they don't even want their shoes to touch the carpet. Meanwhile, the mechanic comes out; he looks like Al Jolson. He's covered in goo, from head to toe. You can't even see him. Although, I prefer that to when they have the lab coat, The clipboard and the nice glasses. Now you know you're getting screwed. (as concerned car owner) 'Can I see it?' (as doctor-like mechanic) 'You better not. It's idling quietly right now. I think it should stay overnight. We wanna keep an eye on it, and we wanna keep the bill running up.'

[Yankee Stadium]

(Mr. Wilhelm is hurrying along a corridor, with George trailing in his wake. As Wilhelm speaks, George is making notes onto a pad and looking flustered.)

WILHELM: And you can tell the players that I reimburse the trainer for the cigarettes and the dive checks.

GEORGE: Sorry, the players will be reimbursed?

WILHELM: The trainer, George. Tell the players I'll reimburse the trainer. What's the matter with you? This is the third time I've had to repeat myself.

GEORGE: Sorry, Mr. Wilhelm.

WILHELM: Look, sorry doesn't cut it. We're running a ball club here George. You've got to pay attention.

GEORGE: I know, sir. It won't happen again.

WILHELM: Lemme see, I uh, I had an assignment for you... uh.

(Wilhelm wanders across the corridor, thinking to himself, he opens the door to the men's room and strolls through.)

WILHELM: Lemme think here.

(George starts to follow Wilhelm into the men's room, but thinks better of it. He wonders briefly what to do, then leans against the wall by the door, to await Wilhelm's return.)

[Elaine's Office]

(Elaine sits behind her desk working at her computer. Mr. Peterman enters. He's carrying an auction house catalogue.)

PETERMAN: Elaine.

ELAINE: Hi, Mr. Peterman.

PETERMAN: You know what a huge fan I am of John F Kennedy.

ELAINE: I do.

PETERMAN: It was the Peace Corps that gave me my start in this business. (nostalgic) Clothing the naked natives of Bantu Besh.

ELAINE: The pygmy pullover.

PETERMAN: Sotheby's is having an auction of JFK's memorabilia. One item in particular has caught my eye. The presidential golf clubs. To me, they capture that indefinable romance that was Camelot.

ELAINE: Whatever.

PETERMAN: But, unfortunately I will be out of town with my lady-friend and therefore unable to bid on the lot. I was hoping maybe you would go in my stead.

ELAINE: Oh. (pleasant surprise) Oh yeah, I'd be happy to. Uhm, how much d'you want this thing? (smilingly) I mean, you know, how high are you willing to go?

PETERMAN: I would see no trouble in spending up to, say, ten thousand dollars. Have my secretary give you a signed cheque.

(He drops the catalogue on the surprised Elaine's desk and exits.)

ELAINE: Wow.

[Yankee Stadium]

(George still waits outside the men's room. He's been waiting a while. He looks at his watch and decides to go in. As he enters, he finds Wilhelm emerging from a stall, and still talking.)

WILHELM: ...when you're done George, and bring it directly to me. Mr. Steinbrenner is very interested in this.

(Wilhelm washes his hands, while George looks panicky and opts to bluff it out.)

GEORGE: Yes, sir.

WILHELM: (drying his hands and heading for the door) Yes, George. I want you to make this project a top priority.

GEORGE: I will, sir. Top priority.

WILHELM: (exiting) Top priority.

GEORGE: Top priority.

(George throws up his arms in despair.)

[Jerry's Apartment]

(Jerry and George stand. George is explaining what happened.)

GEORGE: So he walks out of the stall, he's been talking the whole time.

JERRY: He pulled an LBJ on you.

GEORGE: LBJ?

JERRY: Lyndon Johnson, used to do that to his staffers.

GEORGE: No kidding?

JERRY: Oh yeah. He'd hold national security meetings in there. He planned the Hanoi bombing after a bad Thai meal.

GEORGE: Well, I still don't know what I'm supposed to do. I don't even know what my assignment is.

JERRY: Ask him to repeat it. Tell him there was an echo in there.

GEORGE: I can't. He's been on my case about not paying attention. Besides, it's too late, I already told him I heard him.

JERRY: You know what you do? Ask him a follow-up question. Tell him you're having trouble getting started, and you want his advice.

GEORGE: Yeah, follow-up question, that'll work.

(The door opens and Kramer enters, followed by Newman. Kramer is carrying a large can of some foodstuff, from which he is eating the odd morsel. Newman has a pack of soda (mellow yellow?), and is swigging from one of the bottles.)

KRAMER: Hey buddy.

JERRY: Hey.

GEORGE: Hey.

JERRY: Can I have my keys...

KRAMER: (tossing car keys to Jerry) Yeah.

JERRY: (catching keys) ...back, please?

KRAMER: You shoulda come, Jerry.

NEWMAN: We made quite a haul.

GEORGE: Where'd you go?

KRAMER: Price club.

GEORGE: Why didn't you take your car?

KRAMER: Ah, the steering wheel fell off. I don't know where it is.

(Newman finishes his soda and drops the bottle in Jerry's bin.)

KRAMER: What're you doing. (fetching the bottle from the trash) Don't throw that away.

NEWMAN: Well, I'm not paying the five cents for that stupid recycling thing.

KRAMER: You don't pay five cents, you get five cents back. Here, read the label here. (reads from bottle) Vermont, Connecticut, Massachusetts, New York. Refund, (brings bottle up close to Newman's eyes) vrrup, five cents.

NEWMAN: (taking bottle) Refund?

KRAMER: Yes.

JERRY: Well, what d'you think the hoboes are doing?

NEWMAN: I don't know, they're deranged.

(Kramer and Newman sit on Jerry's couch. Kramer has TV Guide, Newman still reading the bottle.)

GEORGE: Awright, listen, can you uh, gimme a lift back to my place?

JERRY: No I can't. I gotta pick up Elaine. I'm taking her to this Kennedy auction.

GEORGE: Awright, I'll see you later.

(George leaves. Jerry exits to the bedroom.)

NEWMAN: (peering at bottle label) What is this 'MI, ten cents'?

KRAMER: That's Michigan. In Michigan you get ten cents.

NEWMAN: Ten cents!?

KRAMER: Yeah.

NEWMAN: Wait a minute. You mean you get five cents here, and ten cents there. You could round up bottles here and run 'em out to Michigan for the difference.

KRAMER: No, it doesn't work.

NEWMAN: What d'you mean it doesn't work? You get enough bottles together...

KRAMER: Yeah, you overload your inventory and you blow your margins on gasoline. Trust me, it doesn't work.

JERRY: (re-entering) Hey, you're not talking that Michigan deposit bottle scam again, are you?

KRAMER: No, no, I'm off that.

NEWMAN: You tried it?

KRAMER: Oh yeah. Every which way. Couldn't crunch the numbers. It drove me crazy.

JERRY: (leaving) You two keep an eye on each other?

NEWMAN/KRAMER: (simultaneous) No problem. You bet.

(Jerry exits, shaking his head.)

[Sotheby's]

(An auction room, with several rows of seating facing a platform with a lectern for the auctioneer. Many people sit or stand around, with catalogues and numbered paddles for making bids. Jerry and Elaine enter.)

JERRY: Are you sure you didn't hear my car making a funny noise? I know those two idiots did something to it.

ELAINE: No, I didn't hear anything. (she spots a familiar face) Oh, my God, look who's here.

JERRY: Sue Ellen Mishke, the braless 'O Henry' candy bar heiress.

(Sue Ellen notices them, and comes over.)

SUE ELLEN: Well. Hello Elaine. Jerry.

ELAINE: Hi Sue Ellen.

JERRY: Hi Sue Ellen.

SUE ELLEN: I'm surprised to see you here. Come to catch a glimpse of high society?

ELAINE: (faked laughter) Oh, ho ha ha. No, no, I'm actually here to bid, Sue Ellen. I mean that is if anything is to my liking.

JERRY: I'm here to catch a glimpse... of high society.

SUE ELLEN: Well, I hope you find something that fits your budget.

(Sue Ellen walks away to her seat. Elaine and Jerry make their way to their seats,)

ELAINE: (half under her breath and half to Jerry) I... hate that woman.

[Jerry's Apartment]

(Kramer and Newman are still on Jerry's couch. Kramer is watching TV, While Newman has been working something out on a pad.)

NEWMAN: I don't understand. You fill an eighteen-wheeler?

KRAMER: No, an eighteen-wheeler's no good. Too much overhead. You got permits, weigh-stations, tolls... Look, you're way outta your league.

NEWMAN: I wanna learn. I want to know why.

[Sotheby's]

(A bag of golf clubs is brought onto the platform.)

ELAINE: (loudly, for the benefit of Sue-Ellen) Oh. Those are handsome. Look at that set. Yeah, think I might bid on those.

AUCTIONEER: Lot number seven forty-five. We have a full set of golf clubs, that were owned by President John F Kennedy, as seen in the famous photograph of the president chipping at Burning Tree on the morning of the Bay of Pigs invasion. The set in perfect condition, and we will start the bidding at four thousand dollars. Four thousand dollars? Do I have four thousand dollars?

(A man behind Elaine raises his paddle to bid.)

AUCTIONEER: I have four thousand dollars. Do I have five? (another person bids) Five thousand dollars. I have five thousand dollars. Do I have six? Six thousand dollars for this set of beautiful clubs. (another bid) Six. I have six thousand dollars. Can I have sixty-five hundred?

(Elaine raises her paddle to bid.)

AUCTIONEER: Sixty-five hundred to the dark-haired person on the right. We are at sixty-five hundred, do I hear sixty-six hundred?

(Sue-Ellen looks thoughtful.)

AUCTIONEER: The president's own golf clubs. Leisure life at Camelot. Sixty-five hundred going once...

(Jerry and Elaine think they've got them.)

SUE ELLEN: Eight thousand.

AUCTIONEER: Eight thousand. We have eight thousand. The bid is now eight thousand dollars.

ELAINE: (to Jerry) What is she doing? She's starting in on the bidding now? (to Auctioneer) Eighty-five hundred!

AUCTIONEER: We have eighty-five...

SUE ELLEN: Nine thousand.

AUCTIONEER: Nine thousand dollars.

JERRY: Think she wants those clubs.

AUCTIONEER: Do I hear ninety-five? Ninety-five hundred...

ELAINE: Ninety-five hundred.

SUE ELLEN: Ten thousand.

AUCTIONEER: Ten thousand, to the shapely woman on the left. Ten thousand going once...

JERRY: Well, that's your ceiling.

AUCTIONEER: Ten thousand going twice...

(Sue Ellen looks over at Elaine, with a smirk.)

ELAINE: (determined) Eleven thousand!

SUE ELLEN: Twelve thousand.

ELAINE: (angrier) Thirteen thousand!

SUE ELLEN: Fourteen thousand.

ELAINE: (vicious) Fifteen thousand!!

(Jerry buries his face in his hands.)

[Jerry's Car]

(Jerry is driving Elaine home. In the back seat, the golf clubs can be seen. There is a persistent clunking sound coming from the car.)

ELAINE: Peterman is gonna kill me.

JERRY: I really thought you had her there at seventeen thousand.

ELAINE: Why didn't you stop me?

JERRY: Do you hear this clunking?

ELAINE: (listening) A little.

[Jerry's Car, later]

(The Saab halts outside Elaine's apartment building.)

ELAINE: Oh. You know what? (indicates clubs) I'm gonna grab these from you later. You'll take care of 'em, okay? Okay. See you tomorrow.

JERRY: Okay.

ELAINE: Alrighty, bye.

JERRY: Bye.

(Elaine leaves the car and walks away. Jerry restarts the car, when he notices steam and smoke emerging from under the hood.)

JERRY: What's going on here?

(Jerry gets out of the car and runs to raise the hood.)

JERRY: Oh God!

(Jerry opens the hood of his car. As the smoke and steam clears, it becomes clear there are a variety of comestibles arranged on and around the engine compartment.)

JERRY: (angry) Oh, you idiots!

[Jerry's Apartment]

(Kramer and Newman still occupy the couch.)

NEWMAN: So we could put the bottles in a U-haul. You know, go lean and mean?

KRAMER: Newman, it's a dead-end, c'mon. (Jerry enters) Hey, there he is.

JERRY: Hey. You put your groceries under the hood of my car?

KRAMER: (to Newman) Aw, that's right, we forgot about those.

NEWMAN: (to Kramer) That's where my missing soda is.

JERRY: And your crab legs, and a thing of cheese. The Triple-A guy said I was this close to sucking a muffin down the carburetor. What were you thinking?

KRAMER: We ran outta space.

JERRY: Now I gotta take the car down to Tony and get it checked out.

KRAMER: Ah, Tony, he's good.

JERRY: Yeah, he's real good. But he's so obsessive about the car. He makes me feel guilty about every little thing that's wrong with it. I gotta get it washed before I bring it down to him, or I'm afraid he'll yell at me.

KRAMER: (offering the artichoke can) 'Choke?'

JERRY: No, thank you.

[Auto Shop]

(Jerry and Tony stand beside the Saab. Jerry looks worried and Tony is looking like he's in love with the car. He runs his hands over the roof and along the lines of the bodywork. Tony is a little intense.)

TONY: (lovingly) Oh, yeah. I remember this car. Beautiful car.

JERRY: Yeah. So, anyway, the engine's been idling a little rough. I thought it might be time for a check up...

(Tony isn't hearing Jerry. He climbs into the driver's seat and begins ferreting about.)

JERRY: There's really nothing wrong on the inside.

TONY: Well, the shift knob is loose. You know about that?

JERRY: No, I hadn't noticed.

TONY: (accusingly) Have you been picking at it?

JERRY: Have I been picking at it? No. You know. It's just wear and tear.

TONY: (disapprovingly) Wear and tear. I see.

JERRY: The engine is really the only thing that needs checking.

TONY: You been rotating the tires?

JERRY: Try to.

TONY: (sharp) You don't try to. You do it! Fifty-one percent of all turns are right turns. You know that? 'Try to.'

[Elaine's Office]

PETERMAN: Twenty thousand dollars!?! Elaine, that's twice the amount I authorized you to spend.

ELAINE: I know, Mr. Peterman, but but but but once I saw them, I just couldn't stand to let anyone else have them. (warming to her subject) You know, certainly not some stuck-up candy bar heiress who shamelessly flaunts herself in public without any regard...

PETERMAN: Well, where are they?

ELAINE: (ingratiatingly) They should be here today.

[George's Office]

(George stands, silently rehearsing his follow-up question. Wilhelm walks by the window, down the corridor. George takes the plunge.)

GEORGE: Uh, Mr. Wilhelm.

WILHELM: (entering the office) Yes George.

GEORGE: Hi, I was just uh... I just had one little question about uh, my assignment.

WILHELM: Yes, well I trust things are moving smoothly. Mr. Steinbrenner's counting on you, you know.

GEORGE: Yes, yes. Very smooth, super smooth. No, but I really wanna attack this thing, you know. Sink my teeth into it. So I was just wondering... what do you think would be the very best way to get started?

WILHELM: (confusion) Get started? I don't understand, George.

GEORGE: Well, I was wondering...

WILHELM: You mean you haven't been to payroll?

GEORGE: Payroll? No, no, I haven't done that.

WILHELM: Well, what's the problem? Now come on George. I told the big man you were moving on this. Now, don't let him down!

(Wilhelm leaves.)

GEORGE: Payroll!!

(George grabs his jacket, races to the door, checks Wilhelm has gone, and darts off down the corridor.)

[Yankee Stadium: Payroll Office]

(A meek looking clerk sits behind a counter. George enters.)

GEORGE: Hello there. I'm George Costanza.

CLERK: Yes?

GEORGE: Assistant to the travelling secretary. (fishing for a reaction) I'm uh, working on the project.

CLERK: What project?

GEORGE: Payroll project. Wilhelm? Big uh, big payroll project.

CLERK: You're gonna have to fill me in.

GEORGE: You know what, I'll just uh, I'll just look around for a little while. (moving to come round the counter) I'll just browse around.

CLERK: (blocking George) Hey, wait, hey. Excuse me, uh, you can't come back here.

GEORGE: Look, I am under direct orders from Mr. Wilhelm. So if you have a problem with that, maybe you should just take it up with him.

CLERK: Well, maybe I will.

GEORGE: (spotting possible salvation) You know what, I urge you to take it up with him. Go ahead, give him a call, he'll tell you what I'm doing here. (half to himself) Then you can tell me.

CLERK: (on phone) Mr. Wilhelm, uh, this is Lafarge in payroll. Uh, there's a Costanza here, says he's working on some project?

(George is leaning across the counter, trying to hear Wilhelm's side of the conversation.)

CLERK: (on phone) Oh. (he swaps the phone to his other ear) Oh, I see. (listens) Interesting. (listens) Well, that's quite a project. Alright, thank you.

(The clerk puts down the phone. George looks expectant.)

CLERK: (apologetically) Ah, I'm sorry uh, that I doubted you. Whatever you need, just uh, make yourself at home.

GEORGE: So he explained it all to you?

CLERK: Yes, he explained it all very clearly.

GEORGE: What'd he tell you?

CLERK: (upset) Look! You were right, I was wrong! You don't have to humiliate me about it, alright!

[Newman's Apartment}

(Newman sits on his couch. He's using an old mechanical adding machine and a pad to work on permutations for the 'Michigan deposit bottle scam'. There are spools of used paper from the adding machine all over the table and maps of the northeastern states of the US pinned up on the wall. He taps out a series of number, pulls the handle and reads the result, then looks at what he's written on his pad.)

NEWMAN: Damn!

(Frustrated, he sits back. He notices a framed photograph of his mother. A thought occurs.)

NEWMAN (V.O.): Oh, Mother's Day. (inspiration strikes) Wait a second. Mother's Day?!

(He starts typing figures into the adding machine rapidly. He mouths numbers to himself, shrugging as he makes estimates. When he finishes he tears the paper strip from the machine, compares it to figures on his pad.)

NEWMAN: (triumphant) Yessss!

(In celebration he swigs from a bottle of soda.)

NEWMAN: Ahaha!

[Hallway]

(Newman hurries up to Kramer's door and hammers on it with his fist. He waits a few seconds, then impatiently hammers again.)

NEWMAN: Come on Kramer!

(The door opens to reveal Kramer midway through a shave, holding a razor, with foam on his face.)

KRAMER: Wha...?

NEWMAN: It's the truck, Kramer. The truck!

KRAMER: Look, Newman, I told you to let this thing go.

NEWMAN: No, no, no, no no. Listen to me. Most days, the post office sends one truckload of mail to the second domestic regional sorting facility in Saginaw, Michigan.

KRAMER: (interested) Uh-huh.

NEWMAN: But, on the week before holidays, we see a surge. On Valentine's Day, we send two trucks. On Christmas, four, packed to the brim. And tomorrow, if history is any guide, will see some spillover into a fifth truck.

KRAMER: (realisation) Mother's Day.

NEWMAN: The mother of all mail days. And guess who signed up for the truck.

KRAMER: A free truck? Oh boy, that completely changes our cost structure. Our G and A goes down fifty percent.

NEWMAN: (excited) We carry a coupla bags of mail, and the rest is ours!

KRAMER: Newman, you magnificent bastard, you did it!

NEWMAN: (triumph) Let the collecting begin!

(They embrace joyfully.)

[Montage of scenes]

(A woman puts a soda can on top of a mail box while she reaches into her bag for something to post. A hand reaches from behind the box, picks up the can and disappears back behind the box. The woman is flummoxed by the can's disappearance. Newman stands nonchalantly beside a dumpster with a plastic sack containing some bottles and cans. In the dumpster, Kramer roots about beneath the plastic and cardboard, before coming up and handing a bottle to Newman, who drops it into his sack. In Monk's, a waitress puts a tray of empty bottle onto a shelf behind Kramer, who's eating a meal. As she leaves, he reaches behind him, takes the tray and tips the bottles into another plastic sack. Newman stands in a doorway on the street. A homeless guy pushes a shopping cart full of bottles and cans past. Newman drops a few coins onto the sidewalk, attracting the attention of the homeless guy, who leaves his cart to retrieve the change. Newman darts from the doorway, grabs hold of the cart and races away down the street, leaving the homeless guy shaking a fist and yelling after him. Newman finishes a bottle of soda and hands the empty to Kramer, who hands another bottle to Newman. There's a long line of empty bottles already on the table and Newman's looking close to capacity. As Newman reluctantly begins to drink this latest one, Kramer opens another and taps him on the stomach, causing Newman to splutter and spray soda across the table. A cigar-smoking Kramer and a gleeful Newman, regard the back of a mail truck, filled with plastic sacks of bottles and cans. They slap palms and shake hands as Kramer pulls down the door and flips the handle closed.)

[Yankee Stadium: George's Office]

(George has his head down on his desk. Wilhelm walks jauntily along the corridor and enters the office.)

WILHELM: So...

(George snaps awake.)

WILHELM: ...did you go down to payroll?

GEORGE: (standing) Yes, payroll. Yes I did. Very productive. Payroll... Paid off.

WILHELM: (pleased) Well then, I guess you'll be heading downtown then, huh?

GEORGE: Oh, yeah. Downtown. Definitely.

WILHELM: Well, I'm very interested to see how this thing turns out.

GEORGE: (to himself) Yeah, you said it. (to Wilhelm) Uh, excuse me, Mr. Wilhelm. Uh, do you really think... Well, is this downtown trip really necessary, you know, for the project?

WILHELM: Oh no, you've got to go downtown, George. It's all downtown. Just like the song says.

GEORGE: The song?

WILHELM: There's your answer. Downtown.

(Wilhelm leaves.)

GEORGE: (thoughtful) Downtown.

[Monk's]

(George and Jerry in a booth.)

JERRY: The song Downtown? You mean the Petula Clark song?

GEORGE: Yeah.

JERRY: You sure he didn't just mention it because you happened to be going downtown?

GEORGE: I think he was trying to tell me something, like it had some sort of a meaning.

JERRY: Okay, so how does it go?

GEORGE: 'When you're alone, and life is making you lonely, you can always go...'

JERRY: '...downtown.'

GEORGE: 'Maybe you know some little places to go, where they never close...'

JERRY: '...downtown.'

GEORGE: Wait a second. 'Little places to go, where they never close.' What's a little place that never closes?

JERRY: Seven-eleven?

GEORGE: 'Just listen to the music of the traffic, in the city. Linger on the sidewalk, where the neon lights are pretty.' Where the neon lights are pretty. The Broadway area?

JERRY: No, that's midtown.

GEORGE: 'The lights are much brighter there. You can forget all your troubles, forget all your cares, just go...'

JERRY: '...down town.'

GEORGE: 'Things'll be great, when you're...'

JERRY: '...downtown.'

GEORGE: I got nothing, Jerry. Nothing.

JERRY: Well, 'don't hang around and let your troubles surround you. There are movie shows...'

GEORGE: You think I should come clean? What d'you think, you think I Should confess?

JERRY: How can you lose?

[Jerry's Apartment/Elaine's Office]

(Jerry enters. He goes to his answering machine and plays the messages.)

TONY (O.S.): Yeah, Jerry, it's Tony Abato at the shop. Look, we gotta talk. You better come down, any time after four.

(Jerry looks less than happy at the prospect. The phone rings, and Jerry answers.)

JERRY: Hello.

ELAINE: Hi, it's me.

JERRY: Oh, hi.

ELAINE: Listen, I need to come over and pick up the clubs for Peterman.

JERRY: Oh, you know what?

ELAINE: (worry) Oh no. What?

JERRY: Oh, no. It's no big deal. I left the clubs in the car.

ELAINE: You left them in the car? How could you leave them in the car?

JERRY: I forgot.

ELAINE: Oh, go down and get them.

JERRY: I can't. The car's at the mechanics.

ELAINE: Ah, this is great. Alright, well, where is the mechanic? I'll just go and pick 'em up myself.

JERRY: No, no, you can't. He's working on the car right now. You can not disturb him while he's working. But I'm going down there in like an hour, if you wanna meet me down there. You know the place, it's on fifty-sixth street?

ELAINE: (resigned) Ugh, okay, alright, fine.

[Outside Auto Shop]

(Jerry looks a touch anxious as Tony approaches. Tony is still real intense.)

JERRY: Hey, Tony.

TONY: Thanks for coming in, Jerry.

JERRY: Sure.

TONY: I think I know what's goin' on here, and I just wanna hear it from you. But I want you to be straight with me. Don't lie to me, Jerry. You know that motor oil you're puttin' in there? (reproachful) From one of those quicky lube places, isn't it?

JERRY: Well, I change it so often, I mean to come all the way down here...

TONY: Jerry, motor oil is the lifeblood of a car. Okay, you put in a low-grade oil, you could damage vital engine parts. Okay. (holds up component) See this gasket? (throws it down) I have no confidence in that gasket.

JERRY: I really wanna...

TONY: Here's what I wanna do. I wanna overhaul the entire engine. But it's gonna take a major commitment from you. You're gonna have to keep it under sixty miles an hour for a while. You gotta come in, and you gotta get the oil changed every thousand miles.

JERRY: How much money is this gonna cost me?

TONY: (contempt) Huh. I don't understand you. It's your own car we're talking about. You know you wrote the wrong mileage down on the form? You barely know the car. You don't know the mileage, you don't know the tyre pressure. When was the last time you even checked the washer fluid?

JERRY: The washer fluid is fine.

TONY: (angry) The washer fluid is not fine!

JERRY: Alright, you know what, uhm... I just wanna take my car, and I'm gonna bring it someplace else.

TONY: What d'you mean?

JERRY: Just, can I have my car? I wanna pay my bill, I'm gonna be on my way.

TONY: Well, the car's on a lift.

JERRY: Well, just get it down.

TONY: (subdued) Alright. Okay. Well, uhm, wait here and I'll uh, I'll bring it around.

JERRY: Okay. Thank you, very much.

(Tony walks away into the auto shop. Elaine arrives.)

ELAINE: Hey. Where's the car?

JERRY: He's bringing it.

ELAINE: Good.

(There is the sound of a car starting up, then a squeal of tires and Jerry's Saab emerges from the auto shop at high speed. It passes Jerry and Elaine and Races away down the street. They stare open-mouthed after the car, and at each other in astonishment.)

[...to be continued]

[Montage of snippets]

JERRY (V.O.): Last week on Seinfeld.

A sequence of clips from The Bottle Deposit (1) establishes the story so far: Newman and Kramer are using a USPS mail truck to run deposit bottles and cans to Michigan, in order to collect 10 cents on each of them. George has been given an assignment by Mr. Wilhelm, but he hasn't a clue what it is. Elaine outbids Sue-Ellen Mishke at an auction, to buy John F Kennedy's golf clubs on behalf of Mr. Peterman, and leaves them in the back of Jerry's car. Kramer and Newman have left groceries under the hood of Jerry's car, meaning Jerry has to take it to Tony the mechanic, who loves the car more than Jerry does. When Jerry asks for his car back, Tony flees in it, taking JFK's clubs with him.

[Outside Auto Shop]

(Jerry and Elaine are outside the auto shop. Jerry is on the payphone.)

JERRY: Okay, thank you. (hangs up the phone)

ELAINE: So? What'd they say?

JERRY: They're sending a detective to my apartment tomorrow.

ELAINE: What the hell were you thinking leaving my clubs in that car?!

JERRY: Well, I didn't count on my mechanic pulling a Mary-Beth Whitehead, did I?

ELAINE: What kind of maniac is this guy?

JERRY: He's a very special maniac.

ELAINE: What am I supposed to tell Mr. Peterman?

JERRY: I don't know.

ELAINE: Why couldn't you take better care of that car?!

[Elaine's Office]

(Elaine at her desk. Peterman enters.)

PETERMAN: Well, are they here?

ELAINE: Mr. Peterman, uh... There seems to be a bit of a snag.

PETERMAN: Snag?

ELAINE: It seems that a psychotic mechanic has absconded with my friend's car.

PETERMAN: What does that have to do with my clubs?

ELAINE: They happened to be in the back seat at the time.

[Jerry's Apartment]

(Jerry is talking with a police detective at his door.)

DETECTIVE: What was the suspect wearing at the time of the incident?

JERRY: You know, like mechanic's pants, a shirt that said 'Tony'. Lemme ask you something, have you ever seen a case like this before?

DETECTIVE: All the time. A mechanic forms an emotional attachment, thinks he's gonna lose the car, he panics, he does something rash. I'm gonna ask you some personal questions. I'm sorry if I touch a nerve, but I think it'll help with the case. Had you been taking good care of the car?

JERRY: Had I been taking...?

DETECTIVE: Well, did you leave the A/C on? Do you zip over speed bumps? Do you ride the clutch? Things like that.

JERRY: W-well, what does it matter? It's my car, I can do whatever I want with it.

(The detective stares at Jerry.)

JERRY: Not that I would think of doing such things.

DETECTIVE: (making a note) Alright Mr Seinfeld, we'll let you know if we find anything. I gotta be honest with you, these cases never end up well.

JERRY: Well uh, whatever you can do. Thanks.

[Yankee Stadium: George's Office]

(George sits at his desk, his forehead resting on a folder he has clutched in his hands. Mr. Wilhelm enters, looking happy.)

GEORGE: (hesitant) Uh, Mr. Wilhelm. Uh, about the project...

WILHELM: That's what I came to talk to you about. Great job George. (shakes George's hand) You really nailed it.

GEORGE: I did?

WILHELM: Oh yes, I read through it this morning. I couldn't have done it better myself, and I turned it right over to Mr. Steinbrenner. Good work George.

(Wilhelm leaves. George looks stunned and confused.)

[Jerry's Apartment]

(By now, George is looking much more pleased.)

JERRY: I don't get it. He assigns it to you, you don't do it. Somehow it gets done, and now he's telling you what a great job you did.

GEORGE: Maybe somebody did it and didn't take credit for it. Maybe it was already done and didn't need doing in the first place. I have no idea who did it, what they did, or how they did it so well. And you know what? Jimmy crack corn and I don't care.

[Mr. Wilhelm's Home]

(Wilhelm sits on the couch. He has a newspaper and is talking to his wife, who's in another room.)

WILHELM: The gardener did a nice job planting the rose bushes, didn't he dear?

MRS WILHELM (O.C.): You planted the rose bushes, dear.

WILHELM: I did?

MRS WILHELM (O.C.): Yesterday. You remember.

WILHELM: (thinks for a moment) That's right. (pause) What's for dinner?

MRS WILHELM (O.C.): We just ate. Did you forget to take your medicine?

(Wilhelm can be seen struggling to recollect.)

[Jerry's Apartment]

(A still pleased looking George is fetching a drink from Jerry's fridge.)

GEORGE: The point is, however it got done, it's done. So, any luck with the car?

JERRY: No. The police have no leads (sitting on the couch arm) and I just found out today my insurance doesn't cover it.

GEORGE: Why not?

JERRY: They don't consider it stolen, if you wilfully give the guy the keys.

(The door opens and Elaine enters.)

ELAINE: (to George) Hey.

GEORGE: Hey.

ELAINE: (to Jerry) Hey. What did the detective say?

JERRY: They're looking.

GEORGE: I gotta go.

(George leaves. The phone rings and Jerry picks up.)

JERRY: Y'hello.

DETECTIVE (V.O.): Mr. Seinfeld?

JERRY: Yeah.

DETECTIVE (V.O.): It's Detective McMahon...

(Elaine looks quizzical. Jerry mouths, 'It's the police')

DETECTIVE (V.O.): ...I'm at the warehouse on Pier 38. Ah, I think you'd better get down here.

JERRY: Yeah, okay. (to Elaine) They may have found the car.

ELAINE: (makes surprise noise) Are the clubs in it? Ask him.

JERRY: Are there golf clubs in the back?

DETECTIVE (V.O.): We really can't tell. You better bring your service records.

[Pier 38 Warehouse]

(The interior of the warehouse is gloomy and dank. There are cars and parts of cars arranged round the area, together with tools, welding gear, etc. Detective McMahon stands beside a car-shaped object hidden under a white sheet. Jerry and Elaine are led in by a young patrolman who looks queasy.)

YOUNG COP: Watch where you step. There's quite a bit of... grease. Detective, Jerry Seinfeld is here.

DETECTIVE: How d'you do. Thanks for coming down.

JERRY: (indicating) This is Elaine Benes.

ELAINE: (explaining) We used to date, but now we're just friends.

DETECTIVE: I see.

JERRY: Yeah.

DETECTIVE: I'm sorry to make you go through this, but we need to make sure.

JERRY: Well, what's going on? What is this thing?

DETECTIVE: One of our patrolmen stumbled over this.

(He lifts the sheet, revealing what's beneath to Jerry and Elaine.)

ELAINE: (horrified) Huuh! (she turns away and covers her mouth)

JERRY: Oh my God!

(The young patrolman removes his cap out of respect for the victim.)

DETECTIVE: The block is nearly split apart. We found the overhead cams thirty feet away. We can only hope the body sold for scrap.

ELAINE: Oh, my God.

DETECTIVE: And we know it's a Saab. The angle on the V6 is definitely ninety-two. The model is hard to determine because the drive train is all burnt out.

JERRY: What is that smell?

DETECTIVE: Look at the clutch.

(They look. Jerry and Elaine don't like what they see.)

ELAINE: Uuh.

(The patrolman cracks and leaves hurriedly, looking nauseous.)

YOUNG COP: Excuse me.

DETECTIVE: Whoever did this didn't just dismantle it. I mean, they took their time, they had fun. They were very systematic. They went out of their way to gouge the sides of every piston, and the turbo was separated from the housing and shoved right up the exhaust pipe.

ELAINE: Uhh

JERRY: Wait a second. Turbo? I didn't have a turbo.

DETECTIVE: Your car's not a turbo?

JERRY: No, it's a nine-hundred S. (happy) It's a turbo, Elaine, a turbo!

ELAINE: (sobbing happiness) It's a tu-hur-bo.

(Elaine and Jerry hug in happiness. In the background, another woman arrives.)

WOMAN: Excuse me, did you say turbo? Saab turbo nine-thousand? Is it... (voice breaking) midnight blue?

DETECTIVE: (condolences) Yes ma'am.

[Mail Truck]

(Newman drives as he and Kramer give voice to their happiness.)

KRAMER/NEWMAN: (singing) Nine thousand, nine hundred and ninety-nine bottle and cans in the trunk, nine thousand, nine hundred and ninety-nine bottles and cans. At ten cents a bottle and ten cents a can, we're pulling in five hundred dollars a man. Nine thousand, nine hundred and ninety-eight bottle and cans in the trunk, nine thousand, nine hundred and ninety-eight bottles and cans. We fill up with gas, we count up our cash!!...

(Their singing ends shambolically as they lose track of the lyrics. But the pair still look gleeful.)

[Jerry's Apartment/Jerry's car]

(The phone rings in Jerry's apartment. He picks it up.)

JERRY: Hello.

TONY: Hey Jerry, it's Tony.

JERRY: Tony, where are you?

(The Saab is driving down a quiet country road at night.)

TONY: Aw look, I just want you to know that the car is fine. I got her all fixed up. We're in a nice area, no potholes, no traffic. So there's nothing to worry about. Okay? In fact, here, somebody wants to talk to you.

(Tony holds the phone toward the dash and revs the engine a little. Jerry can hear the engine noise over the phone.)

JERRY: Tony, y-you better bring that car back!

TONY: (angry) Nobody's giving anything back! You tried to take it from me, I don't forget that.

JERRY: Tony, it is my car, and I want it back!

TONY: Oh, your car. You want your car back!

JERRY: Tony.

TONY: Listen, that registration may have your name on it, Jerry. But this engine's running on my sweat and my blood.

(Tony hangs up the phone.)

JERRY: (exasperated) Where do I find these guys?

[Mail Truck]

(Kramer is driving the truck along a highway in daylight.)

NEWMAN: How much gas we got?

KRAMER: Three quarters of a tank.

(Newman punches the numbers into a calculator.)

KRAMER: That's better than we estimated.

NEWMAN: (smugly) That is seven dollars and twenty-two cents better.

(They give a smug little laugh.)

NEWMAN: Maybe we could uh, stop for a snack.

KRAMER: Ah, no, that's not in the budget.

NEWMAN: Yeah well, the budget changed, you know. I mean, it might be a good investment.

KRAMER: That's not a good investment, that's a loss.

(A convertible black Saab passes the mail truck.)

KRAMER: Hey, d'you see that car? Looks like Jerry's. I'm gonna check out that license plate.

(He accelerates the mail truck to close on the Saab, and leans forward, straining to make out the plate.)

KRAMER: Yeah, those are New York plates.

NEWMAN: Is that Jerry's number?

KRAMER: I don't know, but that's New York and we're in Ohio. Those are pretty good odds.

(Kramer reaches under his seat, rummaging for something.)

NEWMAN: What're you doing?

KRAMER: I'm calling Jerry.

NEWMAN: On what?

KRAMER: Brought my phone.

(He pulls out his mobile and hits the speed dial for Jerry.)

[Jerry's Apartment/Mail Truck]

JERRY: (answering phone) Y'hello.

KRAMER: Yeah, hey Jerry, what's your licence plate number?

JERRY: Why, what's up?

KRAMER: Yeah, well I think I spotted your car.

JERRY: Oh my god, you're kidding. (dives for his wallet) Hang on a second. (reading from his registration) It's JVN 728.

KRAMER: (checks the car ahead of him) Hey, that's it! That's it. Hey, uh look, we got him. We're driving right behind him in a truck.

JERRY: Oh my god. Yeah, yeah, he said he brought it to the country.

KRAMER: Well we're in the country and we're right on his tail.

JERRY: Good work Kramer, this is incredible.

KRAMER: Yeah, don't worry Jerry. We're right on this guy like stink on a monkey! I'll check back with you.

[Elaine's Office/Jerry's Apartment]

(The phone rings in Elaine's office. She answers it.)

ELAINE: Elaine Benes.

JERRY: Yeah, it's me. Kramer found the car!

ELAINE: Oh my god, where is it?

JERRY: It's somewhere in the country, they're following 'em.

ELAINE: Are the clubs there?

JERRY: I don't know. They're tailing him. I'm waiting for them to call me back.

ELAINE: Alright, I'm heading over right now.

[Jerry's Apartment]

(Elaine enters at a rush.)

ELAINE: What's the status?

JERRY: Last check-in, they were still on him.

ELAINE: Well, have they called the police yet?

JERRY: No, they won't call the police.

ELAINE: What? Why not?

JERRY: They're afraid they'll get in trouble for misusing a mail truck. Kramer doesn't want a record.

ELAINE: Kramer has a record.

JERRY: Not a Federal record.

(The phone rings. Jerry grabs the handset by the couch, Elaine picks up in the kitchen.)

[Mail Truck/Jerry's Apartment]

ELAINE/JERRY: Kramer?

JERRY: What's going on?

KRAMER: Yeah, nothing. We're still following him.

(Ahead of the truck, the black Saab indicates his intention to move onto the off-ramp.)

KRAMER: Wait a second, he's getting off. Yeah, he's gonna be going south on the one-thirty-five.

ELAINE: Keep following him.

KRAMER: Alright, alright, I'll follow him.

NEWMAN: Hey, we can't follow him, we're going north to Michigan.

KRAMER: Yeah, hey listen, I can't. It's gonna be taking us out of our way.

ELAINE: I need those clubs.

JERRY: Kramer, I want my car.

KRAMER: Well, I don't know what to do.

NEWMAN: Hey, we got ten thousand deposit bottles here. I mean, this guy could be going to Arkansas.

JERRY: Keep following him Kramer. don't let me down.

NEWMAN: Hey, don't listen to him. I mean, we can't afford a detour. Our budget won't hold it.

KRAMER: Well, I don't know what to do man!

NEWMAN: Kramer! Stay left. Left, left, left.

ELAINE/JERRY: Right. Go right!/South!

KRAMER: Alright! Alright. I'm getting off! I'm gonna go on the ramp.

(Kramer swerves onto the off-ramp at the last moment. Tyres squeal and the truck sways.)

[Mail Truck]

NEWMAN: I hope you realise what you've done. You've destroyed our whole venture.

KRAMER: This ramp is steep.

NEWMAN: All my work, my planning, my genius. All for nought.

KRAMER: Alright, look, we're pulling too much weight. He's getting away from us here. (indicating) Take the wheel.

(Newman reaches across and takes the steering wheel as Kramer gets out of the driving seat.)

NEWMAN: What're you doing?

KRAMER: (climbing though into the back of the truck) I'm gonna get something.

NEWMAN: Are you crazy?

(The truck swerves as Newman slides into the driving seat.)

KRAMER: Keep your foot on the gas.

(Kramer shoves his way through the sackloads of bottles and cans.)

NEWMAN: Hey! You're not dumping those bottles back there, are you?

(Kramer slides open the rear door of the truck.)

NEWMAN: Hey Kramer, those have wholesale value! We could cut our losses.

(Kramer grabs a sack and heaves it out the back of the truck.)

KRAMER: Look out below!!

(Car horns can be heard as the sack lands in the carriageway. Kramer grabs another sack and hurls that out, with another yelled warning.)

[Yankee Stadium: Steinbrenner's Office]

(Steinbrenner sits behind his desk. He's examining something on his desktop with a large powerful magnifying glass.)

STEINBRENNER: (to himself) With this magnifying glass, I feel like a scientist.

(There is a tap at the door, and George cautiously enters.)

GEORGE: You wanted to see me, sir?

STEINBRENNER: Ah, come in George, come in.

(George strolls up to Big Stein's desk, looking more confident.)

STEINBRENNER: Uh, Wilhelm gave me this project you worked on.

GEORGE: (smiling) Yes sir.

STEINBRENNER: Let me ask you something, George. You having any personal problems at home? Girl trouble, love trouble of any kind?

GEORGE: (wondering where this is leading) No sir.

STEINBRENNER: What about drugs? You doing some of that crack cocaine? You on the pipe?

GEORGE: (worried now) No sir.

STEINBRENNER: Are you seeing a psychiatrist? Because I got a flash for you young man, you're non compos mentis! You got some bats in the belfry!

GEORGE: What're.. What're you talking about?

STEINBRENNER: George, I've read this report. It's very troubling, very troubling indeed. It's a sick mind at work here.

(Two burly guys who are clearly medical orderlies come into the room behind George.)

STEINBRENNER: Okay, come on boys, come on in here. George, this is Herb and Dan.

(George regards the two guys, very nervously as they approach him and stand behind him, one on either shoulder.)

STEINBRENNER: They're gonna take you away to a nice place where you can get some help. They're very friendly people there. My brother-in-law was there for a couple of weeks. The man was obsessed with lactating women. They completely cured him, although he still eats a lot of cheese.

(Herb and Dan take hold of George's arms. George gets panicky)

GEORGE: Ah, see, Mister.. I didn't write that report. That, that's not mine.

(Herb and Dan begin to drag the struggling George across the office toward the door.)

STEINBRENNER: Of course you didn't George. Of course you didn't write it.

GEORGE: I didn't do it! It.. It just got done. I don't know how it got done, but it did.

(As Herb and Dan haul George through the door, George makes his last stand, trying to get a hold on the doorframe with his feet. Eventually he is dragged out into the corridor and vanishes from view.)

STEINBRENNER: Of course. Of course it got done. Things get done all the time, I understand. (as George disappears) Don't worry, your job'll be waiting for you when you get back. (banging his fist on his desk) Get better George. Get better!

[Mail Truck]

(The Saab travels down a quiet country road at night, followed by the mail truck. Kramer is driving, Newman looks furious in the passenger seat.)

KRAMER: (frustrated) Damn. I don't understand this. I've ditched every bottle and can, and we still can't gain. It's like we're...

(He looks across at the substantial bulk of Newman and a thought occurs.)

KRAMER: ...sluggish.

NEWMAN: I went through all those bottles and all those cans, for what? What a waste. And I'm really gonna catch hell for those missing mailbags.

KRAMER: Heyy, wasn't that a pie stand back there?

NEWMAN: (perks up) A pie stand? Where?

KRAMER: Oh yeah. Home-made pies, two hundred yards back.

NEWMAN: Aww, c'mon, pull over, pull over will ya.

(Kramer pulls the truck into the roadside. As it halts, Newman sticks his head out the window to peer back down the road.)

NEWMAN: Where? I..I..I don't see it.

KRAMER: Well open the door, you get a better look.

(Newman slides back the door and leans out.)

NEWMAN: I don't see any pie...

(Kramer plants his foot firmly in Newman's backside and heaves him out of the truck.)

NEWMAN: ...Aargh!

(As Newman lands heavily in the verge, Kramer slides the door shut and drives away.)

NEWMAN: Kramer!!

KRAMER: I'm sorry Newman, you were holding us back.

NEWMAN: (after speeding truck) Kramer!!

(In the mail truck, Kramer picks up his phone and redials.)

KRAMER: (shouting) Jerry! We've lost the fat man, and we're running lean. We're back on track, buddy!

[Country Roadside]

(Newman wanders forlornly along the roadside at night. He tries to thumb a ride from passing traffic, displaying his uniform insignia to drivers.)

NEWMAN: Federal employee. Federal employee.

(Aside from a few blaring car horns, he gets no response. He continues his trudge, a sour look on his face.)

[Countryside]

(Newman struggles up a steep slope. Newman pushes his way through a field of crops. He emerges from the vegetation and sees a farmhouse, its lights blazing. His face lights up. He stumbles towards the welcoming lights, tripping and falling, before picking himself up and running up to the building.)

[Farm]

(Newman reaches the steps to the porch and stumbles up them. As he reaches the door, a scent catches his attention. Looking to the window, he sees a pie left out on the window sill to cool. A craftier look comes to his face. He turns back to the door and knocks. After a few seconds, it opens.)

FARMER: Hello stranger.

NEWMAN: (a touch desperate) Ah, look, I.. I'm sorry to bother you, but I'm a US postal worker and my mail truck was just ambushed by a band of backwoods mail-hating survivalists.

FARMER: Calm down, now. Calm down. Don't worry, we'll take care of you. This farm ain't much, but uh, you're welcome to what we have. Hot bath, hearty meal, clean bed.

NEWMAN: Oh, thank you, sir.

FARMER: Just have one rule. Keep your hands off my daughter.

(Just then, the daughter in question slinks up behind the farmer. Blonde, twenty-ish, just one walking temptation.)

[Mail Truck/Jerry's Apartment]

(Kramer has the mail truck right behind Jerry's Saab as they race along a quiet country road.)

KRAMER: Jerry, we got 'im. I'm riding his tail. There's no escape. He's running scared, buddy.

(Jerry and Elaine are sitting on the couch, each with a phone handset.)

JERRY: How's the gas situation?

KRAMER: (checks dial) I got enough to get to Memphis.

(In front of him, Tony reaches into the back seat of the Saab.)

KRAMER: He's reaching in back. He's grabbing at something.

(Tony extracts a long, metallic object from behind himself.)

KRAMER: He's pulling out a gun! He's got a gun, Jerry!!

JERRY: Duck, Kramer! Duck!

(Kramer crouches as far as he can. Tony flings the object at Kramer's mail truck. It crashes against the windshield and bounces away.)

KRAMER: It's a golf club! It's no gun. He threw a golf club at me!

ELAINE: Those are JFK's golf clubs!

(Tony hurls another club at the mail truck. Again, it bounces off the windshield, leaving some cracks.)

KRAMER: Hey, I'm under fire here. (another club hits) I'm under heavy fire here, boy. (another hit) Jeez! That was a five-iron!

ELAINE: Stop the truck, Kramer. Pick up the clubs!

JERRY: No, don't stop, Kramer. Keep going, don't let him get away.

KRAMER: Wait a minute, I think he's done. (peers at the Saab) Oh no, he's taking out the woods!

(Tony flings a heavy wood at the truck.)

KRAMER: (noise)

(The Saab leads the truck down the road, with Tony hurling club after club over his shoulder and into the front of the truck.)

KRAMER: (yelling at Tony) You'll have to do a lot better than that!

(Tony hurls the golf bag at the truck. It slams solidly against the windshield, Kramer flinches, the truck swerves. The front wheel runs over a club on the tarmac and the tyre bursts.)

JERRY: (hearing the noises) What's happening!

(The truck is rattling and lurching as it struggles along the road.)

KRAMER: This truck is dying. We're losing him.

(The Saab easily outpaces the truck and accelerates away. The truck staggers to a halt, giving out a death rattle. A cloud of steam and smoke erupts from under the hood.)

KRAMER: I think we lost him.

JERRY: (disappointment) Dammit!

ELAINE: (quietly) Can you stop and pick up those clubs Kramer?

KRAMER: (subdued) Yeah, yeah, I'll get 'em.

(Jerry hangs up.)

[Country]

(Kramer climbs out of the truck and looks back down the road. He kicks the deflated tyre. Coming to the front of the truck, he picks a club off the front bumper and pulls the broken shaft of another out of the radiator grille. Kramer walks along the road with the bent and broken clubs. He comes upon the bag and transfers the clubs into it. Slinging it over his shoulder, he continues on his way, picking up more battered golf clubs as he goes.)

[Farm]

(Newman, the farmer and the farmer's daughter sit round the kitchen table. They are working their way through a generous meal.)

FARMER: Enjoy that mutton?

NEWMAN: (mouth full) It's delicious mutton. This is uh, this is outta sight. I would, I would love to get the recipe. It's very good.

(The farmer's daughter is staring at Newman and toying with her fork, touching it to her lips and teeth. (It's difficult to be arousing with cutlery, but she's giving it a pretty good shot.) Newman notices this and tries to take a nonchalant sip from a glass, but it goes down the wrong way and he splutters.)

FARMER: That cider too strong for you?

NEWMAN: No, no. I love strong cider. (for the farmer's daughter's benefit) I'm a big, strong, cider guy.

(The farmer's daughter licks her lips.)

FARMER: Gonna be milking Holsteins in the morning, if you'd like to lend a hand.

NEWMAN: (reluctant) You know, I don't really know that much about uh.. I don't have any.. I don't.. I don't think I know much about that.

FARMER: Ahh, Susie here'll teach you.

(The farmer's daughter goes wide eyed.)

FARMER: Just gotta pull on the teat a little.

(Susie and Newman half-laugh nervously.)

SUSIE: (suggestive) Nice having a big, strong, man around.

NEWMAN: You know, those mail bags, they get mighty heavy. I uh, I Nautilus, of course. (puffs out his chest)

(The farmer looks at him oddly.)

NEWMAN: (breaking from his pose) Can I have some gravy?

[Institution]

(George is using a payphone in the corner of the room. His free hand is holding the waistband of his trousers. In the background are a couple of inmates and visitors, and an orderly. Notable among them are Pop and Deena Lazzari, previously seen in 'The Gum'.)

GEORGE: (desperate) Steinbrenner had me committed! I'm in the nuthouse!

DEENA: I'll be back same time next week, Pop.

GEORGE: (quieter desperation) They took my belt, Jerry. I got nothing to hold my pants up. (listens) Well, you gotta come over here now! Just tell 'em what we talked about, how I, how I, I didn't do the project.

(Deena spots George as she makes her way out of the room.)

DEENA: George?

(George looks like his salvation has arrived. He hangs up the phone.)

DEENA: I see you're finally getting some help.

GEORGE: Aw, hoh, oh Deena, thank God. (he hugs Deena) Thank God you're here. Listen, you gotta help me. You gotta tell these people that I'm okay. You know that I don't belong in here.

DEENA: George, this is the best thing for you. (she walks away)

GEORGE: Yea... (sinks in) What? No, no!

(As he tries to follow Deena, the orderly grabs hold of him and restrains him.)

GEORGE: Deena! Deena, wait a... Deena, help!

(George is almost in tears and hops from foot to foot in frustration as the orderly holds him. Pop Lazzari wanders over.)

POP: Is that little Georgie C? How's the folks? You still got that nice little car?

[Countryside]

(Kramer approaches a familiar farmhouse. As he mounts the steps up to the porch, a commotion erupts inside the place. A gunshot rings out and the farmer's daughter screams. Kramer flinches. The farmer can be heard yelling angrily. The door is flung open and Newman runs out pulling up his trousers.)

NEWMAN: (screaming in panic) Aaah!! Aaah!

KRAMER: What you doing?!

NEWMAN: (pushing past Kramer) Kramer, help me! Help me!

(Newman sets off running. From the door of the farmhouse comes the farmer, armed with a shotgun, closely followed by his daughter, whose shirt is undone and hair is dishevelled.)

KRAMER: (takes one look and sets off after Newman) Jeez!

FARMER: (taking aim) I told you to keep away from my daughter!

(As Kramer and Newman reach the edge of the crops, the farmer fires a shot. Both Newman and Kramer leap in the air and run into the cover of the crop. Before the farmer can fire again, his daughter pushes the barrel of the shotgun downward, spoiling his aim.)

SUSIE: No daddy, you'll hurt him! I love him! (waving after Newman) Goodbye Norman, goodbye.

[Elaine's Office]

(Elaine is sitting with her head in her hands. Peterman enters at a rush.)

PETERMAN: (excited) Elaine! You found the clubs. That's wonderful news. Where are they?

ELAINE: (not the soul of happiness) Yep. Lemme get 'em for you, Mr. Peterman.

PETERMAN: Oh, I'll be inaugurating them this weekend, with none other than Ethel Kennedy. A woman whose triumph in the face of tragedy is exceeded only by her proclivity to procreate.

(Elaine puts the bag of clubs down beside Peterman. The clubs are, of course, wrecked. Elaine looks like she's expecting a poor reaction. Peterman picks up a club or two, staring in disbelief at the twisted metal.)

ELAINE: The uh, the letter of, authenticity's in the side pocket there.

PETERMAN: Elaine. I never knew Kennedy had such a temper.

ELAINE: (spotting a chance to keep her job) Oh. Oh yeah. The only thing worse was his slice. (she laughs nervously)

PETERMAN: See you on Monday.

(Peterman picks up the bag and heads for the door.)

ELAINE: Have a good game.

The End
