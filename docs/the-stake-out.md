---
layout: default
title: The Stake Out
parent: Season 1
nav_order: 2
permalink: /the-stake-out
cat: ep
series_ep: 2
pc: 103
season: 1
episode: 1
aired: May 31, 1990
written: Jerry Seinfeld & Larry David
directed: Tom Cherones
imdb: http://www.imdb.com/title/tt0697784/
wiki: https://en.wikipedia.org/wiki/The_Stake_Out_(Seinfeld)
---

# The Stake Out

| Season 1 - Episode 1                    | May 31, 1989             |
|:----------------------------------------|:-------------------------|
| Written by Larry David & Jerry Seinfeld | Directed by Tom Cherones |
| Series Episode 2                        | Production Code 103      |

"The Stake Out" is the second episode produced of the first season of the NBC comedy Seinfeld.

It aired as the second episode of the season on May 31, 1990. The episode was written by Jerry Seinfeld and Larry David and directed by Tom Cherones. The narrative features Jerry Seinfeld agreeing to attend a birthday party with his ex-girlfriend Elaine Benes. During the party, Jerry tries to flirt with another woman, but fails to learn anything about her except her place of work. Jerry is reluctant to ask Elaine for the woman's number because he does not talk to her about other women. Jerry's father, Morty, suggests that he "stake out" the woman's workplace and pretend to meet her accidentally, which Jerry does. "The Stake Out" is the first episode to feature Jerry's parents. The episode was nominated for a Writers Guild Award in 1991.

## Plot

Jerry and Elaine have just ended their relationship, but have chosen to remain friends. Elaine invites Jerry to a birthday party; he agrees to go on the condition that she accompanies him to a wedding that he and his parents have been invited to. At the party, Jerry meets an attractive woman named Vanessa. He wants to flirt with her, but is uncomfortable doing so in Elaine's presence. The woman leaves with another man before Jerry finds out her name; however, he learns that she works at a law firm called "Sagman, Bennett, Robbins, Oppenheim and Taft". During the party, Elaine tries to tell Jerry about a dream she had, which featured him. Jerry tries to end the conversation but this results in an argument after the party is over.

Back at his apartment, Jerry's parents, Morty and Helen, sleep over, with Jerry sleeping in Kramer's apartment. He talks about the party and claims that he cannot get the phone number of the woman from Elaine because he does not talk about other women with her; additionally, she is still angry with him. Morty suggests that Jerry "stake out" the woman by waiting outside her office, an idea which Jerry likes. The following day, Jerry and George Costanza perform the stake out, pretending that they are coming to see someone else in the building named "Art Corvelay", but under pressure, George insists that they make it "Art Vandelay". They meet the woman, who says the man she left the party with was her cousin. The two then decide to go out on a date.

Later that night, Jerry finds out from his mother that Elaine knows about the stake out. On the day of the wedding, Elaine tells Jerry that the reason that she was angry was because it was the first time she saw him flirt with another woman. They decide that they have to be able to talk more about their relationships if they wish to remain friends. Elaine then reveals that she has recently met a man using a stake out.

## Production

"The Stake Out" is based on a real life incident in which David was with a woman that he had dated previously named Monica Yates. They then went to a restaurant and David met another woman. However, he could not flirt as much as he wanted due to the presence of Yates. David did find out the name of the building where she worked at and staked her out. The names of the people in the title of the law firm are friends Larry David made at college.

This episode prompted running gags that were used in later episodes. These were the importer-exporter, George's ambitions of becoming an architect and Art Vandelay. The character of Vanessa (named after a woman David once went out with) also reappears in a later episode from the first season, "The Stock Tip".

"The Stake Out" is the first episode to mention the past relationship between Jerry and Elaine. Although it was the third episode to be filmed (after "The Seinfeld Chronicles" and "Male Unbonding"), it was the second episode to be broadcast. The episode order was changed because "The Stake Out" provided more background information about Elaine and her relationship with Jerry. Julia Louis-Dreyfus commented that she liked the script for the episode because it made the character seem human. She also commented on the fact that it was racy due to the mention of a pornographic film.

The opening scene caused some problems because it featured a woman walking off the set and taking one step down to get off it. Gleen Forbes, the set designer, thought that this made the show look cheap. The scene in which Jerry and Elaine are in a taxi was filmed in a studio using a black background and moving a fake taxi, due to budget restraints, in a method known as "Poor Man's Process."

This is the first episode to feature Jerry's parents. Only one casting session was performed to find the actors for the roles. Philip Sterling was originally cast to play the role of Morty Seinfeld, but was replaced with Phil Bruns. Bruns was then replaced as well because Seinfeld and David wanted the character to be harsher. As a result, the role was recast and given to Barney Martin — who had no idea that another actor had already established the part. In this episode Kramer greets Morty by name.

## Reception

When "The Stake Out" was first broadcast on May 31, 1990, the episode attracted a Nielsen rating of 16.2/24, meaning that 16.2% of American households watched the episode, and that 24% of all televisions in use at the time were tuned into it. When the episode was first repeated, on December 2, 1992, a special introductory film was made featuring Louis-Dreyfus and Seinfeld, in which they stated that this episode was the first one they did together. Strictly speaking, though, it was just the first episode broadcast — in terms of production order, "Male Unbonding" was the first episode in which the two characters both appeared.

The episode was nominated for a Writers Guild Award in 1991. Holly E. Ordway for DVD Talk Review commented positively on not just this episode, but the whole of the first season, saying, "What's not to like about an episode like 'The Stakeout,' in which (among other things) we are witness to the invention of George's alter ego, Art Vandelay (and his import/export business)?" Mary Kay Shilling and Mike Flaherty of Entertainment Weekly also liked the episode but had some doubts, saying it was, "A painfully realistic take on the lovers-to-friends transition that should have been more comically fruitful."

However, some reviews of the episode were critical, both now and at the time. When first broadcast, Matt Roush from USA Today wrote: "Lacking much in the way of attitude, the show seems obsolete and irrelevant. What it boils down to is that Seinfeld is a mayonnaise clown in the world that requires a little horseradish."

Colin Jacobson for DVD Movie Guide criticized the writing, saying, "the show's rather bland. It provides the occasional chuckle, but the characters aren't formed yet, and that makes the program ring false. The ending reconciliation between Jerry and Elaine causes particular problems; it doesn't turn sappy, but it comes too close for Seinfeld."

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Guests

Lynn Clark .......................... Vanessa  
Phil Bruns ............................ Morty Seinfeld  
Liz Sheridan ........................ Helen Seinfeld  
Maud Winchester ............... Pamela  
William Fair ........................ Roger  
Ron Steelman ..................... Artie Levine  
Joe George .......................... Uncle Mac  
Ellen Gerstein ..................... Carol  
Janet Rotblatt ..................... Woman

## Script

INT. COMEDY CLUB - NIGHT

(Jerry is on stage, performing.)

JERRY: So, I'm on line at the supermarket. Two women in front of me. One of them, her total was eight dollars, the other three dollars. They both of course choose to pay by the use of the...

(He pauses and gestures to audience for response.)

AUDIENCE: Cheque.

JERRY: Cheque. Now, the fact is, if it's a woman in front of you that's writing the cheque, you will not be waiting long. I have noticed that women are very fast with cheques, y'know, 'cause they write out so many cheques. The keys, they can never find in their purse, they don't know where that is, but the cheque book: they got that. They never fumble for the cheque book. The cheque book comes out of a holster: (Jerry "draws" imaginary book from a holster.) "Who do I make it out to? There's my ID." There's something about a cheque that, to a man, is not masculine. I don't know exactly what it is. I think to a man, a cheque is like a note from your mother that says, "I don't have any money, but if you'll contact these people, I'm sure they'll stick up for me... If you just trust me this one time- I don't have any money but I have these. I wrote on these. Is this of any value at all?"

INT. VIDEO STORE - NIGHT

(Jerry and Elaine are picking out a movie for the night.)

JERRY: What's that one?

ELAINE: "Cocoon II: The Return". I guess they didn't like it up there.

JERRY: Maybe they came back for Chinese food. Y'know Maureen Stapleton, if she gets a craving, she's probably screamin' at those aliens, "I gotta have a lo mein!"

ELAINE: Okay, what're we doing here? I have seen everything.

JERRY: Oh yeah? I don't believe you've seen... this.

(Jerry shows Elaine a movie from the Adult section.)

ELAINE: Oh, lovely.

JERRY: Yeah.

ELAINE: What do you think their parents think?

JERRY: "So, uh, what's your son doing now, Dr. Stevens?" "Oh, he's a public fornicator. Yes, he's a fine boy..."

ELAINE: Y'know what? This would be a really funny gift for Pamela's birthday.

JERRY: Pamela? Do I know her?

ELAINE: Yeah, you met her when we were going out.

JERRY: Oh yeah, right...

ELAINE: You have no idea who I'm talking about, do you?

JERRY: (quickly) No.

ELAINE: Blonde hair, remember? Glasses? (pause) Have you totally blocked out the entire time we were a couple?

JERRY: (a lightbulb goes on in his head) Riverside Drive.

ELAINE: Right! In fact... No, never mind...

JERRY: Well, what is it?

ELAINE: Well, a bunch of people are getting together tomorrow night at some bar for her birthday, but... (Jerry turns in disgust) you don't want to go to... that... no.

JERRY: Wait a second, wait a second. We could work out a little deal here.

ELAINE: What little deal?

JERRY: I will go to that, if you go with me to a little family wedding I have on Saturday.

ELAINE: A wedding? Have you lost it, man?

JERRY: Y'know, my parents are coming in for this...

ELAINE: They're coming in?

JERRY: Yeah, tomorrow.

ELAINE: Hey, did your father ever get that hair weave?

JERRY: No, no. Still doin' the big sweep across.

ELAINE: Why does he do that?

JERRY: Doesn't think anyone can tell. So c'mon, do we have a deal?

ELAINE: A wedding?

JERRY: There's a lot of people to mock...

ELAINE: Alright, what the hell.

JERRY: Great!

WOMAN: When you're dead, you're dead. That's it. You're not goin' anywhere...

INT. BAR - NIGHT

(Elaine and Jerry enter.)

ELAINE: C'mon lets go...

JERRY: Was I supposed to bring something?

ELAINE: You could have.

JERRY: I met her one time...

ELAINE: It is not necessary.

JERRY: What did you say then?

ELAINE: Sssshhhhhh!!!

PAMELA: Hi

ELAINE: Hi, Pamela, you remember Jerry.

PAMELA: (shakes Jerry's hand) Yes, we met.

JERRY: Hi, happy birthday.

PAMELA: Thanks, ah, everybody, this is Elaine and Jerry.

GUESTS, JERRY & ELAINE: Hi

JERRY: I didn't bring anything.

PAMELA: Uh, I put you two right here.

JERRY: Oh, Okay (turns to rest of table) I'm sorry, I didn't know what to bring, nobody told me.

(Jerry and Elaine sit next to each other at a table. Across from Jerry is a strikingly beautiful woman, Vanessa. The piano is playing awful dinner music.)

VANESSA: How big a tip do you think it'd take to get him to stop?

JERRY: I'm in for five...

VANESSA: I'll supply the hat.

JERRY: (thinking) Uh-oh... What do we have here?

VANESSA: Why don't you relax and take your jacket off?

JERRY: Oh, I can't. Uh, I have a tendency to get chilly.

VANESSA: How masculine.

JERRY: Plus I'm wearing short sleeves, I don't want to expose my tattoos. (Vanessa smiles; thinking) She's unbelievable!

(The guy beside Vanessa interrupts.)

ROGER: (to Vanessa) Hey, this guy says he knows Bricker.

VANESSA: Oh, you know Bricker! From where?

JERRY: (thinking) What's going on here? Gotta be her boyfriend, she's too good to be alone. What's the difference, I can't maneuver anyway with Elaine next to me.

VANESSA: (to Jerry) How do you know Pamela?

JERRY: Uh, friend of a friend. And you?

VANESSA: We went to law school together.

ELAINE: (interrupting Jerry's conversation) Oh, Jerry!

JERRY: (turning to Elaine; thinking) Oh no, not now.

ELAINE: I had this dream last night and you were in it.

JERRY: Oh really? (tries turning away in the hopes Elaine gets the hint; thinking) Oh God, I gotta get out of this.

ELAINE: You were you, but, you weren't you...

JERRY: No kidding. (thinking) Why is this happening? Please, make her stop!

ELAINE: I think, I think we were in my house where I grew up, and you were standing there, you were looking out the window...

JERRY: (thinking) This is brutal.

ELAINE: You turned around and you had these wooden teeth.

JERRY: How do you like that? (tries to turn away again; thinking)Can I turn now? Is this over? No, I can't, I can't. I'm stuck.

ELAINE: (noticing Jerry not wanting to listen; annoyed) Jerry? Are you listening to me?

JERRY: Yes, I heard you.

PAMELA: Elaine, what's the name of that jewelry store you took me to that time?

JERRY: (thinking) Thank you, Pamela! (turns to talk to Vanessa; to Vanessa) So, you're a lawyer...

VANESSA: Sagman, Bennet, Robbins, Oppenheim and Taft.

JERRY: (thinking) Sagman, Bennet, Robbins, Oppenheim and Taft. Sagman, Bennet, Robbins, Oppenheim and Taft... (to Vanessa) Of course, they handled my tattoo removal lawsuit.

VANESSA: Oh, that was you?

JERRY: Imagine, spelling "Mom" with two O's.

VANESSA: Very funny! What do you do?

JERRY: Comedian.

VANESSA: Really? That explains it.

JERRY: (thinking; quickly) Sagman, Bennet, Robbins, Oppenheim and Taft. Sagman, Bennet, Robbins, Oppenheim and Taft.

ROGER: Are you ready?

VANESSA: We gotta run. Happy birthday!

(Vanessa and Roger get up an leave. Jerry panics.)

JERRY: (thinking) I can't believe it. I got nothing! I don't even know her name! Sagman, Bennet, Robbins, Oppenheim and Taft. Sagman, Bennet, Robbins, Oppen... Sagman... Sag...

(A grim stare from Elaine knocks his concentration.)

INT. TAXICAB - NIGHT

(On the ride home from the party. Jerry and Elaine sit in the back seat.)

JERRY: That wasn't so bad, really.

ELAINE: Y'know, um, you could use a little work on your manners.

JERRY: Why? What did I do?

ELAINE: Wel-Well, I just don't appreciate these little courtesy responses, like I'm selling you aluminum siding.

JERRY: I was listening!

ELAINE: No! You couldn't wait to get back to your little... "conversation".

JERRY: No, you were talking about the, the um, the dream you had.

ELAINE: Uh-huh...

JERRY: Where you had, uh, wooden teeth.

ELAINE: No! No! You had wooden teeth! You had wooden teeth! I didn't have wooden teeth, you did!

JERRY: Alright, so I had wooden teeth, so what?

ELAINE: So nothing! Nothing. (annoyed sigh)

INT. COMEDY CLUB - NIGHT

(Jerry is on stage, performing.)

JERRY: Apparently Plato, who came up with the concept of the platonic relationship, was pretty excited about it. He named it after himself. He said, "Yeah, I got this new thing: Platonic. My idea, my name, callin' it after myself. What I do is, I go out with the girls, I talk with them- don't do anything, and go right home. What'd you think? I think it's going to be big!" I bet you there were other guys in history that tried to get relationships named after them, but it didn't work. Y'know, I bet you there were guys who tried to do it, just went, "Uh, Hi, uh my name's Rico. Would you like to go to bed immediately? Hey, it's a Riconic relationship."

INT. JERRY'S APARTMENT - NIGHT

(Jerry's parents, Helen and Morty, are sitting on a pullout couch. Jerry enters.)

JERRY: Hey!

MORTY: Ah, there he is!

JERRY: This is what I like, see? You come home and your parents are in your bed!

HELEN: Y'know, Jerry, we don't have to do this.

JERRY: What are you talkin' about? It's fine, I love having you here.

HELEN: Tomorrow we'll go to a hotel.

JERRY: Ma, will you stop?

HELEN: No, why should we take over your apartment?

JERRY: I don't care. I'm sleeping next door.

HELEN: Your friend Kramer doesn't mind?

JERRY: No, he's making a bouillabaisse.

JERRY: So, dad, lemme ask you a question. How many people work at these big law offices?

MORTY: Depends on the firm.

JERRY: Yeah, but if you called up and described someone, do you think they would know who it was?

MORTY: What's the matter? You need a lawyer?

JERRY: No, I met someone at this party, and I know where she works, but I don't know her name.

MORTY: So why don't you ask someone who was at the party?

JERRY: Nah, the only one I could ask is Elaine, and I can't ask her.

HELEN: Why not?

JERRY: Because it's complicated. There's some tension there.

HELEN: He used to go with her.

HELEN: Which one is she?

MORTY: From Maryland. The one who brought you the chocolate covered cherries you didn't like.

HELEN: Oh yeah, very alert. Warm person.

JERRY: Oh yeah, she's great.

HELEN: So, how come nothing materialized there?

JERRY: Well, it's a tough thing to talk about uh. I dunno...

HELEN: I know what it was.

JERRY: You don't know what it was.

HELEN: So, what was it?

JERRY: Well, we fight a lot for some reason.

HELEN & MORTY: Oh, well...

JERRY: And there was a little problem with the physical chemistry.

(His parents don't know how to respond.)

HELEN: Well, I think she's a very attractive girl.

JERRY: Oh, she is, she absolutely is.

HELEN: I can see if there was a weight problem...

JERRY: No, it's not that. It wasn't all one-sided.

HELEN: You know, you can't be so particular. Nobody's perfect.

JERRY: I know, I know...

MORTY: Y'know Jerry, it's a good thing I wasn't so particular.

HELEN: (hits Morty) Idiot. (to Jerry) So who're you looking for, Sophia Loren?

JERRY: That's got nothin' to do with it.

MORTY: How about Loni Anderson?

HELEN: Where do you get Loni Anderson?

MORTY: Why, what's wrong with Loni Anderson?

HELEN: I like Elaine more than Loni Anderson.

JERRY: What are you two talking about? Look, Elaine just wasn't the one.

HELEN: And this other one's the one?

JERRY: I dunno, maybe...

MORTY: So ask Elaine there for her number.

JERRY: I can't. She'll get upset. I never talk about other women with her, especially this one tonight.

HELEN: How could you still see her if your not interested?

JERRY: We're friends.

MORTY: Doesn't sound like you're friends to me. If you were friends you'd-you'd ask her for the number. Do you know where this other one works?

JERRY: Oh yeah.

MORTY: Well, go up to the office.

HELEN: Up to her office?

MORTY: Go to the building. She goes out to lunch, doesn't she?

JERRY: I guess.

MORTY: So, you stand in the lobby, by the elevator, and wait for her to come down for lunch.

JERRY: You mean stakeout the lobby?

HELEN: Morty, that's ridiculous. Just ask Elaine for the number!

MORTY: He doesn't want to ask Elaine for the number.

HELEN: So you've got him standing by the elevator like a dope! What happens when he sees her?

MORTY: He pretends he bumped into her!

JERRY: Y'know what? This is not that bad an idea.

INT. OFFICE BUILDING - DAY

(Jerry and George are staking out Vanessa.)

GEORGE: What does she look like?

JERRY: I dunno. Hard to say.

GEORGE: What actress does she remind you of?

JERRY: Loni Anderson.

GEORGE: Loni Anderson?!

JERRY: What, there's something wrong with Loni Anderson? (pause) Hey listen, thanks again for running over here. I appreciate it.

GEORGE: Yeah, sure. I was showing a condo on 48th street. Besides, you think I wanna miss this? (chuckles)

JERRY: I'm a little nervous.

GEORGE: Yeah, me too...

JERRY: If I see her, what do I say that I'm doing here in the building?

GEORGE: You came to see me. I work in the building.

JERRY: What do you do?

GEORGE: I'm an architect.

JERRY: You're an architect?

GEORGE: I'm not?

JERRY: I don't see architecture comin from you.

GEORGE: (somewhat annoyed) I suppose you could be an architect.

JERRY: I never said that I was the architect. Just somethin' else.

GEORGE: Alright, she's not even gonna ask, if we see her, which is remote.

JERRY: Well whaddaya want me to say, that I just wandered in here?

GEORGE: We're having lunch with a friend. He works in the building.

JERRY: What is his name?

GEORGE: Bert... Har... bin... son. Bert Har-bin-son.

JERRY: Bert Harbinson? It sounds made up.

GEORGE: No good? Alright, uh how about Art... Cor.....

JERRY: Art Cor...

GEORGE: ...velay.

JERRY: Corvelay?

GEORGE: Yeah, right.

JERRY: Well, what does he do?

GEORGE: He's an importer.

JERRY: Just imports, no exports?

GEORGE: (annoyed) He's an importer/exporter, okay? (beat) Elaine ever call you back?

JERRY: No, I guess she's still mad.

GEORGE: I don't understand, you never talk to her about other women?

JERRY: Never. (The elevator door opens.) Wait a second. That's her. On the right.

GEORGE: (anxious) I forgot who I am! Who am I?!

JERRY: You're you. We're having lunch with Art Corvelay.

GEORGE: Vandelay!

JERRY: Corvelay!

GEORGE: Let me be the architect! I can do it!

(Jerry ignores George and approaches Vanessa.)

JERRY: Hey, hey. uh Pamela's birthday party, didn't I see you there? Jerry.

VANESSA: Sure! Hi!

JERRY: Uh, this is George. (reaches for her name) I'm sorry...

VANESSA: Vanessa.

GEORGE: Nice to meet you.

JERRY: Ah, Sagman, Bennet, Robbins, Oppenheim and Taft.

VANESSA: That's right! Yea, what're you doing here?

JERRY: Oh, we're meeting a friend of ours for lunch. He works here in the building.

GEORGE: Yeah, Art Vandelay.

VANESSA: Really? Which company?

JERRY & George: (turning to each other) I don't know. He's an importer.

VANESSA: Importer?

GEORGE: ...And exporter.

JERRY: He's an importer/exporter.

GEORGE: (clears his throat) I'm, uh, I'm an architect.

VANESSA: Really. What do you design?

GEORGE: Uh, railroads, uh...

VANESSA: I thought engineers do that.

GEORGE: They can...

JERRY: Y'know I'm sorry you had to leave so early the other night.

VANESSA: Oh, me too. My cousin had to go back to Boston.

JERRY: Oh, that guy was your cousin! (walking in front of George so he gets the picture to leave)

VANESSA: Yeah, and that woman was your...

JERRY: Friend!

GEORGE: I'll just, uh, get a paper...

JERRY: So, um, do you date uh immature men?

VANESSA: Almost exclusively...

INT. JERRY'S APARTMENT

(Helen and Jerry are in the middle of a game of Scrabble. Morty sits on the couch. Helen sings to herself while she tries to form a word.)

HELEN: Bum bum bum bum... I have no letters... Bum bum bum bum...

JERRY: (annoyed) Ma, will you go already?

HELEN: Bum bum bum bum...

(She picks up a nearby dictionary.)

JERRY: What are you doing?!

HELEN: Wait, I just want to see something.

JERRY: You can't look in there, we're playing!

(Kramer enters.)

KRAMER: Hi.

JERRY: Hi.

MORTY: (cleaning his shoes) Good evening, Mr. Kramer!

KRAMER: Hey Morty! (to Jerry) Salad dressing?

JERRY: Look.

HELEN: "Quo". Is that a word?

JERRY: Maybe!

HELEN: Will you challenge it?

JERRY: Ma, you can't look up words in the dictionary! (to Morty) Dad, she's cheating!

KRAMER: "Quo"? That's not a word.

HELEN: (to Jerry) You're such a stickler...

JERRY: Well put something down, you're taking twenty minutes on this. So is Uncle Mac and Artie, they're all coming over here before the wedding?

HELEN: They'll be here at two o'clock. Oh, Elaine called. She said she'd be here at two-thirty. And she says "Hope your meeting went well with 'Art Vandelay'?"

JERRY: She said what?

HELEN: Just what I said, here.

(She hands Jerry the note. He reads it.)

JERRY: She knows! Oh, I am such a jackass.

HELEN: She knows what?

JERRY: She knows the whole stupid thing. Vanessa and the elevator...

(Kramer arranges Mrs. Seinfeld's tiles, offering her a word.)

HELEN: No, no, no, that won't do. He may have a "Z".

MORTY: So, how did she find out?

JERRY: Because, Vanessa probably told Pamela, and Pamela probably told Elaine.

(While they talk, Kramer has covertly scoped Jerry's tiles to confirm that Jerry doesn't have a "Z". This goes unnoticed by Jerry, who continues to talk with Morty.)

MORTY: So, what are you? Afraid of her?

JERRY: Yes. Yes I am! (to Helen) What else did she say on the phone?

HELEN: Whatever I wrote down.

JERRY: Yeah, but what was the tone in her voice? How did she sound?

HELEN: Who am I, Rich Little?

MORTY: Well, she can't be too mad. She's still coming to the wedding.

JERRY: Yeah, but now I'm nervous.

HELEN: Oh, stop it.

(She makes her move and tallies the points.)

JERRY: "Quone"?

HELEN: ...30... 31...

JERRY: "Quone"? No, I'm afraid that I'm going to have to challenge that.

(Jerry picks up the dictionary.)

HELEN: ...32...

KRAMER: No, you don't have to challenge that. That's a word. That's a definite word.

JERRY: I am challenging.

KRAMER: Quone. To quone something.

JERRY: Uh-huh.

(Jerry looks up the word.)

HELEN: I'm not playing with you anymore.

MORTY: Quone's not a word.

JERRY: No good. Sorry. There it is. Get it off.

HELEN: (to Kramer) Why did you make me put that down?

KRAMER: Nah, we need a medical dictionary! If a patient gets difficult, you quone him.

INT. JERRY'S APARTMENT-NEXT DAY

(A few people mingling about, waiting to go to the wedding. [SeinPurgatory] Jerry is talking to Carol.)

CAROL: You want some funny material, you oughta come down to where I work, now that's a sitcom!

JERRY: You must have quite a time down there.

(Jerry checks his watch.)

CAROL: We got plenty of time.

JERRY: Oh, I'm sorry. I'm just waiting for someone.

UNCLE MAC: Watch what you say to this guy. He'll put it in his next act!

(Carol and Uncle Mac laugh)

JERRY: Yeah, yeah...

UNCLE MAC: Jerry, did I tell you that I'm writing a book? An autobiography.

JERRY: Yeah, Uncle Mac, you mentioned it.

UNCLE MAC: It's based on all my experiences!

JERRY: That's perfect.

(Elaine enters.)

JERRY: Could you excuse me one second? I'm sorry.

(Jerry goes over and greets Elaine.)

JERRY: How do you do? (introducing himself) Uh, Jerry Seinfeld.

ELAINE: Oh, how do you do? Elaine Benes.

(Awkward sighs from Jerry and Elaine)

JERRY: Um, do you want to do this now, or do you want to wait until we get in the car?

ELAINE: Oh no, let's do it now.

JERRY: Alright, the whole elevator business, let me just explain-

ELAINE: Okay.

ARTIE: Jerry, were you goin' with us?

JERRY: No, I'm gonna take my car.

ARTIE: That's why I brought the wagon. Why the hell did I bring the wagon?

JERRY: Anyway, you know why I didn't ask you, I mean I felt so uncomfortable, and you were so annoyed in the cab.

ELAINE: Well, Jerry, I never saw you flirt with anyone before. It was quite the spectacle.

CAROL: Jerry, we'll see you there. Bye, Elaine.

ELAINE: Oh, bye. Good to see you.

ARTIE: Oh, we didn't meet.

JERRY: Oh, I'm sorry. Elaine, this is my cousin, Artie Levine.

(Jerry pronounces the name 'le-VEEN'.)

ARTIE: (correcting Jerry) Levine.

(Artie pronounces it with a long "I". Artie exits.)

JERRY: (sarcastically) Yeah, "Levine". And I'm Jerry Cougar Mellencamp. Anyway, I admit it was a fairly ridiculous thing to do, but I mean, I mean, obviously we have a little problem here.

ELAINE: Yeah, obviously.

JERRY: I mean, if we're gonna be friends, we gotta be able to talk about other people.

ELAINE: Couldn't agree more.

JERRY: Good.

ELAINE: Good.

JERRY: Good.

ELAINE: Great!

JERRY: Great? Where do you get 'great'?

ELAINE: It's great to... talk about... other people...

JERRY: ...Guys?

ELAINE: Yeah.

JERRY: Uh-huh. Yeah. So, anybody specific?

ELAINE: No. A general guy.

JERRY: Oh really? Elaine Marie Benes...

ELAINE: What? No, it's not a big deal.

JERRY: No, that's great! That's terrific!

ELAINE: No, we just met...

JERRY: Doesn't matter. What's the young man's name? I would like to meet him.

ELAINE: Hmmm, I don't think so.

JERRY: Well, what does he do? Is he an artisan, a craftsman, a labourer of some sort?

ELAINE: Wall street.

JERRY: Ah, high finance. Bulls, Bears, people from Connecticut.

ELAINE: And he happens to be pretty good lookin'.

JERRY: (pause) Alright, sir.

ELAINE: And... he's hilarious.

JERRY: Now that's not fair! So where did you meet this guy?

ELAINE: I staked out his health club.

JERRY: Uh huh. When you're on a stakeout, do you find it's better to stand up against the wall, or kinda crouch down behind a big plant?

(They leave his apartment)

INT. COMEDY CLUB - NIGHT

Jerry is on stage, performing.

JERRY: Y'know I think that even if you've had a relationship with someone, or let's say, especially if you've had a relationship with someone and you try to become friends afterwards, it's very difficult. Isn't this? It's hard. Because, you know each other so well, you know all of each others tricks. It's like two magicians, trying to entertain each other. The one goes, "Look, a rabbit." The other goes, "So? I believe this is your card." "Look, why don't we just saw each other in half and call it a night, okay?"

The End
