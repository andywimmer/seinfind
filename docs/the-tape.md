---
layout: default
title: The Tape
parent: Season 3
nav_order: 8
permalink: /the-tape
cat: ep
series_ep: 25
pc: 308
season: 3
episode: 8
aired: November 13, 1991
written: Larry David and Bob Shaw & Don McEnery
directed: David Steinberg
imdb: http://www.imdb.com/title/tt0697797/
wiki: https://en.wikipedia.org/wiki/The_Tape
---

# The Tape

| Season 3 - Episode 8                              | November 13, 1991        |
|:--------------------------------------------------|:-------------------------|
| Written by Larry David and Bob Shaw & Don McEnery | Directed by Tom Cherones |
| Series Episode 25                                 | Production Code 308      |

"The Tape" is the 25th episode of Seinfeld. It is the eighth episode of the show's third season. It first aired on November 13, 1991.

The episode was written by Larry David and Don McEnery and Bob Shaw and was directed by David Steinberg.

## Plot

Elaine anonymously leaves an erotic message on Jerry's tape recorder that he used to record his comedy act from the previous night. Upon hearing the message, he, George, and Kramer become obsessed with her. Elaine admits to George that she was the sexy voice in the tape. George is shocked to hear this and suddenly becomes attracted to her, but does not tell Elaine about it. Elaine makes George promise not to reveal her confession to Jerry or Kramer. Jerry, determined to get in touch with the woman who left the message, finds out who sat near the tape and gets her number. After his date with her, he tries to kiss her, but gets the "pull-back", and concludes that she is crazy.

George orders a cream from China after watching a commercial that claims that it can cure baldness. At Jerry's apartment, George makes a collect call to the product's company in Beijing while Jerry mocks George for being gullible enough to believe that the cream actually works. George tries desperately to communicate with the people on the other side of the line, but is unable to do so since they don't speak English; meanwhile, this is when Elaine decides to stop by and Kramer decides to start making his own home videos by recording whatever Jerry, George and Elaine are doing. This includes a fake interview with Elaine, portrayed as a porn star, and she says the sex is never simulated with George, arousing him. Finally, the Chinese delivery boy, Ping, delivers the take-out Kramer orders and George convinces Ping, who speaks Chinese, to talk on the phone and help him order the cream.

George finds it hard to control his obsession with Elaine and finally admits to Jerry that he is attracted to her. Jerry wants to know why, but George tries to keep Elaine's secret by not telling him. He finally cracks and tells Jerry that Elaine left the message. She comes in later and tells her secret to Jerry, but Jerry says George already told him. George then confesses that he is attracted to Elaine. She finds this news disturbing and then realizes that Jerry and Kramer have become attracted to her too. Freaked out, Elaine immediately leaves Jerry's apartment, despite Jerry, George and Kramer's pleas to stay. At the end, the three fight to hear the tape again.

## Production

This episode is the first appearance of Ping, the delivery boy who would return in the later episodes "The Visa", "The Virgin", "The Pilot" and "The Finale".

## Use in scientific research

The Simpsons "Bart the General" and Seinfeld's "The Tape" were used in a Dartmouth College experiment to study brain activity in relation to humorous moments in television shows. The results were published in a 2004 issue of the academic journal Neurolmage. The researchers noted, "During moments of humor detection, significant [brain] activation was noted in the left posterior middle temporal gyrus ... and left inferior frontal gyrus."

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Guests

John Apicella ...................... Repairman  
Ping Wu ............................ Delivery Boy (Ping)  
Norman Brenner ............... Beder

## Script

[Opening monologue]

JERRY: ....hair that was on your shower soap today could be in your head tomorrow. How did they do the first transplant? Did they have the guy take a shower , get his soap , rush it in there by helicopter, you know keep the soap alive on the soap support system ....looks it over. &quot;We got the hair but I think we lost the Zest.&quot; ....rejects the transplant with organs. Is it possible that a head could reject the hair transplant . Guy just standin' there and suddenly... ..Bink! ( motions hair flying out of his head)........lands in someone's frozen yogurt.

[Jerry's apartment]

(A repairman is working on his fridge , Jerry doing work with headphones on.)

REPAIRMAN: .....The gaskets that you have here are asymmetrical.

JERRY: Ah..ha!.. really. ( Jerry is barely listening to him )

REPAIRMAN: So I took off the motor relay on the compressor....'cos you..you (stutters) you've got some discoloration

JERRY: Oh! well whatever you have to do.

REPAIRMAN: I was working with one.....mount at a time 'cos you don't wanna disturb the position of the compressor.

JERRY: (sarcastically) No you don't..

(George enters the apartment.)

GEORGE: Hey! what are listening to?

JERRY: My show from last night.

GEORGE: Oh! you taped it?

JERRY: Yeah , I was doing new material.

GEORGE: Hey! Did 'ya ever do that thing on the toes that I said .

JERRY: Huh?

GEORGE: Yeah! like the big toe is like the captain of the toes, but sometimes the toe next to the big toe gets so big that there's like a power struggle and the second toe assumes control of the foot.

JERRY: The&quot; coup d&eacute;-toe&quot;

GEORGE: Yeah. Did you do it?

JERRY: Yeah!

GEORGE: So?

JERRY: Nothin'.....nothing at all.

GEORGE: Need to use the phone.

JERRY: Who you calling?

GEORGE: China.

JERRY: China really?

GEORGE: Yeah. I'll pay for it.

JERRY: What for?

GEORGE: What for? . I'll tell you what for.... for hair.

JERRY: Hair?

GEORGE: The Chinese have done it my friend. The Chinese have done it.

JERRY: Done what?

GEORGE: Discovered a cure for baldness.

REPAIRMAN: Did you see that last night?

GEORGE: It was on CNN ( Kramer comes in and he is taping from a Camcorder) This Chinese doctor Zeng Zau. has discovered a cure for baldness.

JERRY: (to Kramer) What's this?

KRAMER: Well I just got it. Spector gave it to me , he's giving everything away...becoming a minimalist.

GEORGE: Is that the guy who likes fat women?

JERRY: Doesn't the fat fetish conflict with the minimalism.

KRAMER: (to George) You , you know what you should've done is watching that report on CNN last night.

GEORGE: I did, I'm trying to call China.

KRAMER: You can't call China now its like, what, three 'O clock in the morning there

JERRY: Oh! my God!....

GEORGE: What?

JERRY: Oh! God.. Oh! man.....Oh! brother!!! I can't believe what I'm hearing. This woman his talking to me on my tape recorder while I was on stage. This is wild. I've never heard anything like this in my life. Listen to this.

GEORGE: (George puts on the headphones) Oh! my god...

KRAMER: Give me it..( tries to pull them off George's head)

GEORGE: Wa..Wait.Wait!......Who is this woman?

JERRY: I don't know . I have no idea . I was just listening and she came on.

GEORGE: This is like a Penthouse letter...Why can't I meet women like this?

KRAMER: All right Come on....again attempts to pull headphones off)

GEORGE: WAIT ,WAIT, WAIT, WAIT!!!!.... Where was the tape recorder?

JERRY: It was in the back of the room on the left, she must have been sitting right in front of it.

GEORGE: My god!!!

KRAMER: C'mon it's my turn.

GEORGE: All right, All right, all, right!!( gives the headphones to Kramer) How you gonna find out who this is?

JERRY: Good question.

KRAMER: Where's the volume..(finds it) A, YAI..YA...YA..YA!!!!

[Cut to Monks]

GEORGE: What do the Chinese have to gain by faking a cure for baldness?

JERRY: If it was real ,they would never let it out of the country. No baldness , it'd be like a nation of Supermen.

ELAINE: Hi boys.

BOTH: Hello...

ELAINE: What's happening?

JERRY: Tell her. I wanna hear her reaction.

GEORGE: This woman left this really sexy message on Jerry's tape recorder......

JERRY: (pushes George) NOT THAT YOU IDIOT!!

GEORGE: What??

JERRY: The Chinese , the Chinese bald cure.

GEORGE: I thought you meant the..

JERRY: No I meant the bald cure. We were talking about the bald cure.

ELAINE: What did she say?

PETER: Seinfeld.....( from way in the back of the restaurant. Cheesy plot device to have Jerry leave the table for a minute so George and Elaine can talk)

JERRY: Hey! Is that Peter? ...I can't believe it. Get me a cup of Decaf. (leaves table)

ELAINE: So did you hear this message?

GEORGE: Oh!, he he, It was unbelievable

ELAINE: Really!

GEORGE: Yeah. I can't get over it.

ELAINE: Huh! Sexy?

GEORGE: This woman drove us out of our minds

ELAINE: Like ...humm...How did she sound?

GEORGE: She had this throaty , sexy kind of whisper.

ELAINE: Really , like a... like a....(leans over to George and whispers) Jerry, I want to slide my tongue around you like a snake.....Ooooooooooha ,oooooohaaaa.....

GEORGE: Oh! my God!!......You?.....You?...That was you?....

ELAINE: Shhhhhh!!!

GEORGE: how did ya?...

ELAINE: I stopped at the club to see him and I was standing in the back while he was on, right?, and there was this tape recorder there and I.....got this impulse. Ha Ha Ha Ha....What?

GEORGE: Oh! no no nothing....

ELAINE: Now listen , promise me you won't tell him Okay.I want to have a little fun with this.

GEORGE: I had no Idea you were filled with such....sexuality..

ELAINE: Oh! That was nothing. So listen, what about this bald thing?

GEORGE: Ah! Some bald thing, a bald thing I dunno. It's nothing

(Jerry comes back)

JERRY: Remember Peter?

GEORGE: Peter?

JERRY: You remember Peter. remember I told you how he went to the track that one time and he was yelling at this jockey and the jockey got off the horse and started chasin' him.

ELAINE: So listen , what about this girl on the tape recorder?

JERRY: Oh Elaine....What do you think an enraptured female fan of mine might say?

ELAINE: I don't know.

JERRY: She went on in some detail about certain activities, illegal in some states, for consenting adults. Things you would know very little about.

ELAINE: Oh! really.

JERRY: Well this type of things is very common when you're in show business.

ELAINE: So what, Are you gonna ask her out.

JERRY: No I can't she didn't leave her name or number.

ELAINE: Bummer...Okay , good luck finding her . I'm taking off.

GEORGE: Wh.. Where you going?

ELAINE: Home.

GEORGE: Why you going home for?

ELAINE: Well , I just came from the gym , unless I can shower at your place.

JERRY: Sure.

[Back to Jerry's]

GEORGE: Oh! my god. Oh! man...

JERRY: I don't get it. Why would a woman do that and then leave no way to get in touch with her.

ELAINE: (coming out of the shower in a bathrobe) May be she realized she could never have you and she jumped off the George Washington Bridge.

GEORGE: (phone rings ,picks up) Operator? Beijing?

JERRY: Why are you doing this?

GEORGE: Why do I do anything? tsss...For women.

JERRY: Elaine have you ever gone out with a bald man?

ELAINE: No.

JERRY: You know what that makes you?...A baldist.

GEORGE: Oh. This I need. Hello!! Hello. i..i..is this the hair restoration clinic? ...Does anyone speak English?

ELAINE: ( to Kramer who just got in with his camcorder) Ooooh! You're taping.

KRAMER: Just be yourselves. ( Elaine plays with her hair flirtingly)

ELAINE: Aah! Okaaay.

KRAMER: Well we're talking with Elaine Benes; Adult film star on the set of her new picture &quot;Elaine does the Upper West side&quot;

ELAINE: ( to the camera) Hi. How 're you doin'?

KRAMER: I'm doin' fine.

GEORGE: Do you speak English?...English!!

KRAMER: Whooooa! here's the director Jerry Seinfeld . Jerry , you discovered Elaine Benes?

JERRY: Well yes I did that's true. A couple of a guys I knew in the coastguard told me about her.... and I sensed that she had the anger and intensity that I needed to make this film work.

GEORGE: English. Does anybody speak English .Nobody speaks English.

KRAMER: So What scene are you ready to shoot now , Elaine?

GEORGE: In this scene my co-star who's right over here ( goes over to George who is still on the phone) Follow meeeee... is George Costanza, he plays an airline pilot who's just returned from Rome and I'm about to show him how much I've missed him.

(Door buzzer rings)

KRAMER: That's my Chinese food...So George is this your first movie with Elaine?

GEORGE: (visibly disturbed) I...I..I dunno.

KRAMER: So Elaine in your movies is the sex real or is it simulated?

ELAINE: Oh. it's always simulated....except with George that's in my contract.

GEORGE: All right, Kramer that's it.....( pushes the camera) Hello . English. Does anyone speak English

KRAMER: (to the Chinese delivery boy) How much do I owe you?

PING: $15.90.

KRAMER: $15.90.?

GEORGE: Huh. Excuse me (to Ping) Hum... Do you speak chinese?

PING: Chinese....Yeah.

GEORGE: Look...humm..I'm on with Beijing with the hair restoration clinic. Could you talk to them for me and tell them I'd like to place an order.

PING: (sounds like) Gwen , Ayon. Wonche son thai gettin my chon fai yu.(looks at George and laughs)

GEORGE: They got a billion people over there and he found a relative.

PING: Ah Fuka suma. If you send money they send cream.

GEORGE: They send me? Aw right ..ask 'em Does it really work?

PING: Gym a gun sen tokomo. Chin che .They say you grow hair, Look a like Stalin

GEORGE: Ask' em Are there any side effects?

PING: Dowe o futo yum.... Impotence. ....( makes a just kidding gesture)

GEORGE: Aw! Funny he's a funny guy.

PING: Get a money order from the Bank of China , be here three days after they get check.

(George leaves all excited.)

PING: (continues his phone call) Ha Pachini fair pousher pousher mouist I fai chin fousher...

JERRY: (as Ping rambles on ) ...S'cuse me (Ping looks up) Kind of an expensive call.

&nbsp;

[On a street at night in George's car]

ELAINE: Thanks for driving me home. What did I do to deserve this?

GEORGE: Yoohoo ,Plenty......Wh..wh..what are doing hum...you're going in?

ELAINE: Well ya. I guess so Why? You wanna do something?

GEORGE: yeah....euh...I dunno What?

ELAINE: Pffft....there's really nothing to do.

GEORGE: ( becoming more and more awkward) Yeah.....

ELAINE: Do you think of anything?..

GEORGE: No, no....(mumbles)

ELAINE: I am up for anything.

GEORGE: Really...(he honks the car and is startled, Elaine laughs)......I have to say...You were really good doing that porno thing....you're talented.

ELAINE: I was just kiddin, around.......

GEORGE: I thought the thing you said about the sex not being simulated . That was really funny.

ELAINE: ( feeling awkward as well) Yeah! that was a...f...fun ..mmm?.

GEORGE: So all right I'll speak to you through Jerry and everything.

ELAINE: Okay...Thanks a lot for the ride.

(Elaine gets out of the car , George tries to catch his breath.)

[Back to Jerry's.]

JERRY: ..She was sitting at the table where I had my tape recorder...Okay great. Thanks again.. bye. HA Ha..Who do these women think they're dealing with? Did she think she was gonna leave this incredibly erotic message on my tape and I was just gonna let it go. Not Bloody likely...

KRAMER: What is that?

JERRY: That's my cockney accent.

KRAMER: Naw ,na , that's no good.

JERRY: Lets hear yours.

KRAMER: Not bloody likely..

JERRY: That's the worst cockney accent I've ever heard in my life.( George enters) Hey! Georgie boy, guess what I got.

GEORGE: Guess, what I got.

JERRY: Oh! Is that the bald stuff?

GEORGE: From China. All the way from China.

KRAMER: Wait, wait wait...Let me get the camera.

GEORGE: No Don't get the camera , we don't need the camera. Listen I know your skeptical , but I really believe in the Chinese.

JERRY: Yes I am skeptical.

GEORGE: Why do you have to be so suspicious of every one. This is a great man Zeng Zau, he wants to help bald people.

KRAMER: W..W..Wa...Wa..Wait..Wait wait.. Now lets videotape your head for the before picture, so we can watch how it grows and stuff. Sit down (George sits).....Lean back...A little bit to the right.

JERRY: Make sure you get this area here, where he needs the help....

GEORGE: All right, all right ( Goes to the bathroom)

KRAMER: He's a happy camper huh?

JERRY: Happy camper , I don't hear that expression enough.

KRAMER: Remember that guy who took my jacket. The one I found at my mother's house.

JERRY: Yeah.

KRAMER: My mother told me that he got arrested for mail fraud

JERRY: No kidding?

KRAMER: He's in jail.

JERRY: What happened to the jacket. Did he take it with him?

KRAMER: That's what I intend to find out. (George comes out of the bathroom and he's got white cream on his head)

JERRY: You can see it. You gonna walk around like that?

KRAMER: It stinks. Can you smell that?....You stink.

JERRY: How long are you suppose to leave it on for?

GEORGE: All day. ( phone rings , Jerry picks up)

JERRY: Hello.

ELAINE: It's Elaine Marie Benes.

JERRY: Well Hello..

ELAINE: Hello.. so did you ever find out who that woman was?

JERRY: Yes , I got her number.

GEORGE: Is that Elaine?

JERRY: Yeah.

GEORGE: HI ELAINE...

ELAINE: I guess you figure you're in for a pretty wild night?

JERRY: Well , as I said this type of thing is very common in show business

ELAINE: Well listen I'm going to (?) do you want me to stop by?

GEORGE: Did she say Hello?

JERRY: What? I dunno.

GEORGE: I mean , when I said Hello did she say Hello back?

JERRY: I don't know , Who keeps track of Hellos.

GEORGE: Isn't polite to say Hello when somebody says hello?

JERRY: She's coming up.

GEORGE: Elaine's coming up?

JERRY: Yeah. What's wrong, why? ( George runs back to the bathroom)

KRAMER: How often do you cut your toe nails?

JERRY: I would say every two and a half to eight weeks.

KRAMER: 'cos the other night , you know, I was sleeping with Marion I rolled over and I cut her ankle with my big toe.

JERRY: The big toe; The captain.

KRAMER: What?

JERRY: The captain of the toes. (phone rings) Hello.

ELAINE: Jerry...Jerry listen I got too much stuff this afternoon, I can't come over, forget it.

JERRY: Okay....too bad.

ELAINE: So humm....When you gonna call her?

JERRY: Soon as I get off the phone wih you.

ELAINE: Good luck.

JERRY: okay , bye (to George) What happened , did you take it off?

GEORGE: Yeah, that was enough.

JERRY: That's it, you gave up?

GEORGE: No No I'm working on a system...Who was that?

JERRY: That was Elaine , she changed her mind. She's not coming over.

(As Jerry dials the girls number, George races to the bathroom one more time.)

ALICIA: Hello.

JERRY: Hello is this Alicia? .This is Jerry Seinfeld.

ALICIA: Yeah.

JERRY: This is Jerry Seinfeld.

[Back to Jerry's after an implied date with Alicia]

JERRY: (to Kramer) ...( words missing)...laugh , everything's nice and at the end of the night I go for a little contact. I get the PULL BACK. This woman said the filthiest things I've ever heard in my life. I get the Pull Back.

(Door buzzer)

JERRY: Yeah..

GEORGE: It's George.

JERRY: Come on up . (looks at his watch) ...What's he doing here now?

KRAMER: So , you blew it?

JERRY: She must be psychotic or something.

KRAMER: Let me have her number.

JERRY: I'm not giving you her number.

KRAMER: I know how to handle these psychotics.

(George comes in wearing a huge cowboy hat.)

JERRY: Sheriff?........What's with the hat?

KRAMER: (George takes off the hat , he's got that cream on again) Pheeewwww! Boy! You stink.

JERRY: What are doing here now?

GEORGE: I have to talk to you about something .

KRAMER: All right lets take a look to see what we got ( examines George's head) Wait a second.. I think I see something here George. Lets go to the videotape.

GEORGE: Aahh..No..No..

JERRY: What's up?

GEORGE: I can't tell ya now , he's gonna be back in a ten seconds.

JERRY: So just start it.

GEORGE: I can't.

JERRY: Oh! Come om . He'll be over there for a half hour, he gets lost over there. C'mon so what is this about?

GEORGE: All right.........I've become attracted to Elaine..

Kramer burst in with is video setup.

KRAMER: All right....Sit down George.

GEORGE: Kramer, can we do this later..

KRAMER: No, I got the tape right here.

JERRY: Kramer, let's do this later.

KRAMER: (ignoring them) Now.. This is the tape that we made earlier and I think, that I see. a couple of buds right here.

GEORGE: Really? ..You think.

JERRY: Kramer. I would like to talk to George for a minute, please.

KRAMER: 'bout what?

JERRY: It's kinda private.

KRAMER: Like the big toe captain..

GEORGE: So now you're doing my bits?

JERRY: I'M NOT DOING YOUR BITS!!

KRAMER: Okay , all right. I'm gonna take a look at this huh!.( leaves)

JERRY: Does she know?

GEORGE: NO!!

JERRY: How did it happen?

GEORGE: I can't say.

JERRY: Well, why can't you say it?

GEORGE: Because I promised her.

JERRY: I thought you just said she doesn't know??

GEORGE: She doesn't.

JERRY: So how can you promise her?

GEORGE: Because she asked me to.

JERRY: What is this, an Abbott and Costello routine?

GEORGE: All right You really want to know?...It all started when she told met hat...she was the voice on your tape recorder.

JERRY: What, Elaine?

GEORGE: Yeah! She made me promise not to tell you .It's supposed to be a joke.

JERRY: (picks up the headphones) That was Elaine...

GEORGE: Well let me hear....( they struggle for the headphones)

JERRY: Wait a second. .Just give me a second

GEORGE: You heard it fifty times already.

JERRY: She's my ex-girlfriend I think I have precedence

(Door Buzzer)

JERRY: Yeaaaah!!!

ELAINE: Hi, It's Elaine is this a bad time?

(George grabs his hat and rushes to the bathroom again.)

GEORGE: (Yelling from the bathroom) Don't tell her anything, she'll kill me!!

JERRY: Okay, Okay, I promise. (puts on the headphones again) Wow!!! Oh Man...Oh God.. Oh Brother....Whoooaaaa!! Whoaaaa (Elaine enters he takes them off rapidly)

ELAINE: (Concerned) What's the matter?

JERRY: Oooh! I got a pain in my side.

ELAINE: (to George returning) Hi George. Something stinks in here.( George motions to Jerry, she nods)

JERRY: What are you doing here?

ELAINE: I was the one who talked into your tape recorder.

JERRY: I know, George told me.

ELAINE: You told him!!!!

GEORGE: He..He threatened me.

JERRY: Where did you come up with all that stuff?

ELAINE: That was nothing.

GEORGE: Elaine.. I have to tell you something...

JERRY: George NO!!

GEORGE: No no no no no no no..

JERRY: George I'm telling ya..

ELAINE: What is it?

GEORGE: I'm very attracted to you..

JERRY: Aye......

(Kramer comes in and yells)

KRAMER: I'VE FOUND A HAIR!!! Yes ( goes up to the video machine and inserts the tape) Hey, come here, come here ,take a look at this.

GEORGE: Ever since I found out that you let that message on Jerry's tape recorder I...

KRAMER: Whoa!!!....That was you?

ELAINE: It was a joke...

KRAMER: Wait..( picks up the walkman) Oh my god...Oh yeah....Elaine , I can't believe that that is you.

ELAINE: Aah.... ( she stares at the three of them all lined up like the Daltons, all looking at her with lust.) I think I'll get going...

GEORGE: Heuh. huh. Stick around a while.

JERRY: It's early.

KRAMER: We'll order Chinese.

(She leaves and they all scramble for the tape recorder.)

The End
