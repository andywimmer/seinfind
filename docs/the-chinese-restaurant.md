---
layout: default
title: The Chinese Restaurant
parent: Season 2
nav_order: 11
permalink: /the-chinese-restaurant
cat: ep
series_ep: 16
pc: 206
season: 2
episode: 11
aired: May 23, 1991
written: Larry David &amp; Jerry Seinfeld
directed: Tom Cherones
imdb: http://www.imdb.com/title/tt0697675/
wiki: https://en.wikipedia.org/wiki/The_Chinese_Restaurant
---

# The Chinese Restaurant

| Season 2 - Episode 11                   | May 23, 1991             |
|:----------------------------------------|:-------------------------|
| Written by Larry David & Jerry Seinfeld | Directed by Tom Cherones |
| Series Episode 16                       | Production Code 206      |

"The Chinese Restaurant" is the 11th episode of the sitcom Seinfeld's second season on NBC, and is the show's 16th episode overall. The episode revolves around protagonist Jerry (Jerry Seinfeld) and his friends Elaine Benes (Julia Louis-Dreyfus) and George Costanza (Jason Alexander) waiting for a table at a Chinese restaurant, on their way to see Plan 9 from Outer Space. Unable to get a table, they decide to wait and talk amongst each other, while George tries to use the phone that is constantly occupied and Jerry recognizes a woman, but he is unsure where he has seen her before.

Co-written by the series' creators Seinfeld and head writer Larry David, the episode is set in real time, without any scene-breaks. It was the first episode in which Jerry's neighbor Kramer (Michael Richards) did not appear, much to Richards' disappointment, as it turned out to become a highlight among the show's episodes. Considered a "bottle episode", NBC executives objected to its production and broadcast due to its lack of an involved storyline, thinking that audiences would be uninterested. It was not until David threatened to quit if the network forced any major changes upon the script that NBC allowed the episode to be produced, though the network postponed broadcast to the near end of season two.

First broadcast in the United States on May 23, 1991, the episode gained a Nielsen rating of 11.7/21. Television critics reacted positively to "The Chinese Restaurant", which is widely considered as one of the show's "classic episodes". In 1998, a South Florida Sun-Sentinel critic wrote that the episode, along with season four's "The Contest", "broke new sitcom ground".

## Plot

Jerry, George, and Elaine decide to eat dinner without a reservation at a Chinese restaurant before seeing a one-night showing of Plan 9 from Outer Space, which Jerry exultantly calls "the worst movie ever made!" The maître d' (James Hong) repeatedly tells the party they will receive a table in "5, 10 minutes", but with no result even after the time passes. Besides having only a short time until the movie begins, the characters have other worries:

    Jerry previously lied to his uncle, saying he could not join him for dinner; he prefers to see the film, yet feels guilty. He also notices a woman (Judy Kain) at the restaurant he has seen before, but cannot remember her name.
    George is anxious because, the night before, he left his girlfriend Tatiana during foreplay because he needed to use a bathroom and thought hers was too close to her bedroom to provide enough privacy. He wants to call Tatiana to invite her to join them, but is repeatedly prevented from using the restaurant's payphone, as it is first occupied by a man (Michael Mitz) who ignores George, and then by a woman who is rude to him.
    Elaine is frustrated by being extremely hungry; Jerry had offered her cookies, but she refused since they were health cookies and she disliked their taste.

Jerry dares Elaine to take an egg roll from someone's plate and eat it, offering her "fifty bucks" to do so. Elaine approaches a table with an elderly couple and tells them that her friend will give her $50 to eat one of their egg rolls, and she is willing to give them $25 of it. As she softly speaks the offer without moving her lips, they fail to comprehend her. She awkwardly walks away, then laughs off her attempt. George is finally able to call Tatiana, but he gets the answering machine, so he leaves a message. Tatiana calls the restaurant to reach George, but the maître d' calls "Cartwright" instead of "Costanza", and she "said curse word" when he told her that George is not there. When the mysterious woman encounters Jerry, he remembers belatedly that she is his uncle's receptionist and becomes upset knowing that she'll tell his uncle, who then will tell his parents and so on.

Plan after plan is thwarted: Elaine tries bribing the maître d', but is unsuccessful. They contemplate on getting food to go, but they can't eat Chinese food in a taxicab. Elaine is ravenous, but refuses to eat concession stand food at the movie theater. After missing Tatiana's call, George decides he's no longer in the mood for the film, Elaine wants to leave and get a hamburger, and Jerry decides that he might as well have dinner with his uncle. As soon as they leave, the maître d' calls their party.

## Production

"The Chinese Restaurant" was written by series co-creators Larry David and Jerry Seinfeld and directed by Tom Cherones, who directed all of season two's episodes. David came up with the idea of the real-time episode while he and Seinfeld were waiting for a table at a Chinese restaurant in Los Angeles. When David presented the episode to NBC executives, he received a negative reaction. The network felt that there was no real story and viewers would not be interested. Executive Warren Littlefield commented that he thought there were pages missing from the script he had received. David argued that each character had a storyline: Jerry's story was he recognized a woman but did not know from where; Elaine's story was that she was very hungry; and George's story was that he was unable to use the phone. NBC disagreed and objected to the broadcast of the episode. To satisfy the executives, staff writer Larry Charles suggested the group's storyline to be on their way to a one-night screening of Plan 9 from Outer Space, and thus introducing a "ticking clock" scenario to the story. When the NBC executives still objected, David threatened to quit the show if the network would force any major changes upon the script. Seinfeld supported David and NBC eventually allowed them to produce "The Chinese Restaurant" without any significant alterations, although they strongly advised them not go through with it, and postponed the broadcast until near the end of the season. In 2015, Seinfeld writer Spike Feresten revealed that the host's calling "Cartwright" instead of "Costanza" was a subtle allusion to the classic western show Bonanza. "Bonanza" rhymes with "Costanza" and the show's main characters are the "Cartwright" family.

"The Chinese Restaurant" was first read by its cast on December 5, 1990, and it was filmed on December 11. Filming took place at CBS Studio Center in Studio City, Los Angeles, California, where all filming for the second season took place. As only one location was used, it took roughly half of the time it usually took for an episode to be filmed. Cast members have remarked that the filming was shorter than on any other episode. A few changes were made; in the first draft of the script, George, Jerry, and Elaine entered the restaurant talking about their least favorite holiday. In the version that aired, they talk about combining the jobs of policemen and garbagemen into a single job. In the original draft, the three friends also discussed how to spend the long waiting period in the future, with George suggesting they bring a deck of cards and that Jerry bring a jigsaw puzzle with nothing but penguins. One scene was cut before broadcast, featuring George explaining to Jerry that he pulled his hamstring while trying to untuck the covers of a hotel bed during his recent stay in Boston. George can be seen grabbing his hamstring as he walks to the phone. The scene was later included on the Seinfeld seasons one and two DVD boxset.

At one point in the episode, Jerry mentions having a sister. She is never again mentioned in the series.

### Casting

"The Chinese Restaurant" was the first episode that did not feature regular character Kramer (Michael Richards), Jerry's neighbor. David explained that the reason for Kramer's absence was because—during Seinfeld's early seasons—the character never left his apartment and did not go out with the other three. Richards was still displeased with the absence of his character, as he felt the episode was a breakthrough and—as such—essential for the series' development. In an interview for the Seinfeld first and second season DVD box set, he commented, "The Chinese restaurant episode was so unique, and I just wanted to be a part of that because it was cutting edge. I knew that was a very important episode; it was so odd."

Michael Mitz—who portrayed one of the payphone occupants—would return in season five as a photographer in "The Puffy Shirt". The maître d' was portrayed by actor James Hong; it is one of the actor's most famous roles in the United States. (In fact, he had a small part in a season one episode of The Bob Newhart Show in 1972, portraying a man who is mistaken for a maître d'.) Judy Kain—known for a recurring role on Married... with Children—guest-starred as Lorraine Catalano, the receptionist of Jerry's uncle. David Tress guest-starred as Mr. Cohen, a guest who enters the restaurant and receives a table without reservation, as he is good friends with the maître d'. Larry David's voice can be heard among the group of elderly people Elaine offers money for one of their egg-rolls. Norman Brenner—who worked as Richards' stand-in on the show for all its nine seasons—appears as an extra; he is sitting by the door of the restaurant when George, Jerry, and Elaine enter, and is still at the same spot when they leave.

## Themes

The episode is widely considered to encapsulate Seinfeld's "show about nothing" concept, with The Tampa Tribune critic Walt Belcher calling it "the ultimate episode about nothing", and Lavery and Dunne describing it as "existential". Critics had a similar reaction to season three's "The Parking Garage", in which the four central characters spent the whole episode looking for their car. The structure of "The Chinese Restaurant"—described as "elongation"—drags a small event out over the course of an entire episode. Lavery and Dunne suggest that this structure critiques sitcoms with implied moral lessons (such as those found in so-called "very special episodes"). Vincent Brook—as part of his analysis regarding the influence of Jewish culture on Seinfeld—has said that the episode also conveys the theme of entrapment and confinement in a small space, a recurring theme on the show. The relationship between the characters and food is another recurring theme of the series. In Seinfeld, specific food items are associated with individual characters and food itself is a "signifier of social contracts".

Linda S. Ghent, Professor in the Department of Economics at Eastern Illinois University, discusses some economic issues in this episode. Just before Jerry's dare about the egg roll, Elaine says, "You know, it's not fair people are seated first come first served. It should be based on who's hungriest. I feel like just going over there and taking some food off somebody's plate." Ghent discusses the history and reasoning behind rationing mechanisms and economic efficiency, which are the bases behind how tables are seated at restaurants, rationales which are perhaps invisible to hungry or impatient customers. Elaine's attempt at bribery is an example of opportunity cost: the trio are willing to pay more than usual to get a table, if at least they can get a table. Ghent also gives Jerry's willingness to lie to his uncle as another example of opportunity cost: "Did I do a bad thing by lying to my uncle and saying I couldn't go to dinner? Plan Nine from Outer Space – one night only, the big screen! My hands are tied!"

## Reception

When the episode initially aired in the United States on NBC on May 23, 1991, it received a Nielsen rating of 11.7 and an audience share of 21—this meant that 11.7% of American households watched the episode, and that 21% of televisions in use at the time were tuned to it. Seinfeld was the eighteenth most-watched show of the week, and the sixth most-watched show on NBC. NBC executives held a meeting after the broadcast to determine the fate of the show, and decided it would receive a third season order if the writers would put more effort into episode storylines.

"The Chinese Restaurant" received very positive responses from critics and is considered one of Seinfeld's first "classic episodes". Kit Boss, a critic for the Ocala Star-Banner, wrote that the episode was "like real life, but with better dialogue". Various critics and news sources have praised how the episode defines the show's "show about nothing" concept. Critics have also noted that aside from being a turning point for the show, the episode also became a turning point for television sitcoms; one South Florida Sun-Sentinel critic commented that the episode, along with the season four episode "The Contest", "[...] broke new sitcom ground and expanded the lexicon of the '90s." Vance Durgin of The Orange County Register praised how the show "wrung" so much comedy "out of a simple premise". The episode was also included in a list compiled by The Star-Ledger called "50 events that shaped TV – and our lives" between 1900 and 1999. The Charlotte Observer has called "The Chinese Restaurant" the best Seinfeld episode, referring to it "the very epitome of the classic Seinfeld format".

Critics also praised Louis-Dreyfus' and Alexander's performances; The Age critic Kenneth Nguyen stated that they "characteristically, rock[ed] their line readings". Michael Flaherty and Mary Kaye Schilling of Entertainment Weekly, who graded the episode with an A−, commented, "George is at his pressure-cooker best, but it's Elaine—famished and in high dudgeon—who is the centerpiece." David Sims of The A.V. Club gave the episode an A+, saying "it's a deftly-plotted, extremely funny example of the 'show about nothing' label that Seinfeld assigned itself".

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus .............. Elaine Benes  

#### Guests

James Hong ........................ Bruce  
David Tress ........................ Mr. Cohen  
Judy Kain ........................... Lorraine  
Kate Benton ....................... Woman on Phone  
Michael Mitz ....................... Phone Guy  
Kendall McCarthy .............. Man

## Script

[opening monologue; first sentence kinda blurry]

JERRY: (A couple of days ago I used a public phone), go over time on the call, hang up the phone, walk away. You've had this happen? Phone rings. It's the phone company... they want more money. Don't you love this? And you got them right where you want them for the first time in your life. You're on the street, there's nothing they can do. I like to let it ring a few times, you know, let her sweat a little over there, then I just pick it up, &quot;Yeah, operator... oh, I got the money... I got the money right here... D'you hear that? (taps on microphone) That's a quarter. Yeah, you want that don't you?&quot;

[Jerry, Elaine &amp; George enter the Chinese restaurant]

ELAINE: No, they've just got to get more cops on the force, it's as simple as that.

GEORGE: Cops. I don't even care about cops. I wanna see more garbage men. It's much more important. All I wanna see are garbage trucks, garbage cans and garbage men. You're never gonna stop crime, we should at least be clean.

JERRY: I tell you what they should do, they should combine the two jobs, make it one job, 'cop\garbage man'. I always see cops walking around with nothing to do. Grab a broom! Start sweeping. You sweep sweep sweep... catch a criminal, get right back to sweeping.

ELAINE: You should run for mayor.

JERRY: Ehh, nobody listens.

ELAINE: Where is someone? I'm starving.

GEORGE: I think this is him right here.

ELAINE: Is there a table ready?

RESTAURANT MANAGER (BRUCE): How many?

ELAINE (to Jerry): How many?

JERRY (to George): Is Tatiana coming?

GEORGE: I don't know, I have to call her, tell her where we are. I'm very lucky she's even considering seeing me at all.

JERRY: Really? I thought things were going OK.

GEORGE: They were, it's kinda complicated.

JERRY: Well what is it?

ELAINE: How many?

JERRY: Ah, alright, four. Seinfeld.

BRUCE: Four. It'll be five, ten minutes.

GEORGE: What do you wanna do?

ELAINE: Let's go someplace else, I am too hungry.

JERRY: We might as well just stay here, we haven't got that much time if we wanna make it to the movie.

GEORGE: I gotta call Tatiana. where's the phone?

JERRY (to Elaine): Tatiana...

(George goes over to the public phone. There's a guy using it)

GEORGE: Excuse me, are you gonna be very long?

(guy on phone just turns the other way without answering)

BRUCE: Lashbrook, 4!

JERRY: So did I do a terrible thing?

ELAINE: You mean lying to your uncle?

JERRY: I couldn't have dinner with him. 'Plan 9 from Outer Space', one night only, the big screen. My hands are tied!

GEORGE (to Jerry): You know it's a public phone, you're not supposed to just chit-chat.

ELAINE: Jerry, get menus so when we sit down we can order right away.

JERRY: Can't look at a menu now, I gotta be at the table.

GEORGE: He knows I'm waiting. He sees me. He just doesn't wanna look.

ELAINE: Everything's gotta be just so with you, doesn't it?

JERRY: Hey, I offered you those cookies in my house.

ELAINE: Health cookies. I hate those little dustboard fructose things.

GEORGE: I just can't believe at the way people are. What is it with humanity? What kind of a world do we live in?

(Jerry stares at someone)

ELAINE: What?

JERRY: There's a woman over there that looks really familiar. Dark hair, striped shirt?

ELAINE: I've never seen her before.

JERRY: I know this woman. This is gonna drive me crazy.

(a group of people comes in, one of them bumps into Elaine)

MAN: Oh, Excuse me.

ELAINE: I'm sorry.

(the group just walks right into the dining room)

ELAINE: Didja see that? Those people, look, they're getting a table.

JERRY: Well maybe they were here from before.

ELAINE: No no no, they weren't here before.

GEORGE (to guy): Excuse me, are you going to be much longer? I have to make a very important call.

(guy on phone turns away again)

ELAINE: Find out what's going on!

JERRY: Excuse me, didn't those people just come in? I believe we were ahead of them.

ELAINE: Yeah.

BRUCE: What's your name?

JERRY: Seinfeld.

(Bruce starts talking to a Chinese woman in Chinese)

BRUCE: No, no, they were here before. Keckitch(sp?), 2!

ELAINE: Did you ever notice how happy people are when they finally get a table? They think they're so special because they've been chosen. It's enough to make you sick.

JERRY: Boy, you are really hungry.

GEORGE (whistles to guy on phone): Hey!

(guy on phone smiles at him and turns back. George, annoyed, goes over to Jerry)

GEORGE: If anything happens here, can I count on you?

JERRY: What?

GEORGE: If we decide to go at it.

JERRY: Yeah, I wanna get into a rumble...

GEORGE: I have to get in touch with Tatiana! And look at his little outfit. It's all so coordinated, the way his socks matching to his shirt. I really hate this guy.

ELAINE: I'm gonna faint...

JERRY: George, who is that woman in the stripes?

GEORGE: I don't know her.

JERRY: She looks so familiar.

ELAINE: Ya know, its not fair people are seated First Come First Served, It should be based on who's hungriest. I feel like just going over there and taking some food off somebody's plate.

JERRY: I'll tell you what, there's 50 bucks in it for you if you do it.

ELAINE: What do you mean?

JERRY: You walk over that table, you pick up an eggroll, you don't say anything, you eat it, say 'thank you very much', wipe your mouth, walk away- I give you 50 bucks.

GEORGE: What are they gonna do?

JERRY: They won't do anything; in fact, you'll be giving them a story to tell for the rest of their lives.

ELAINE: 50 bucks, you'll give me 50 bucks?

JERRY: 50 bucks. That table over there, the three couples.

ELAINE: OK, I don't wanna go over there and do it, and then come back here and find out there was some little loophole, like I didn't put mustard on it or something...

JERRY: No, no tricks.

ELAINE: Should I do it, George?

GEORGE: For 50 bucks? I'd put my face in the soup and blow.

ELAINE: Alright, alright. Here, hold this. I'm doin' it.

(Elaine goes over to the table, smiling)

ELAINE (through her teeth): I know this sounds crazy, but the two men who are standing behind me are going to give me 50 bucks if I stand here and eat one of your eggrolls.

(the people at the table are confused)

ELAINE (through teeth): I'll give you 25 if you let me do it.

PEOPLE AT TABLE: What? What is she talking about? What did she say?

(Elaine runs from the table, laughing)

JERRY: What happened?

ELAINE: Did you see that?

GEORGE: What were you doing?

ELAINE (laughing): I offered them 25, they had no idea...

JERRY: George, the phone's free.

GEORGE: Alleluia.

(as George reaches for the phone, a woman snatches it and starts dialing)

GEORGE: Excuse me, I was waiting here.

WOMAN AT PHONE: Where? I didn't see you.

GEORGE: I've been standing here for the last ten minutes!

WOMAN: Well I won't be long.

GEORGE: That's not the point. The point is I was here first.

WOMAN: Well if you were here first, you'd be holding the phone.

GEORGE (yelling at her): You know, we're living in a society! We're supposed to act in a civilized way.

(he goes over to Jerry and Elaine)

GEORGE: Does she care? No. Does anyone ever display the slightest sensitivity over the problems of a fellow individual? No. No. A resounding no!

(guy on phone approaches George)

GUY: Hey, sorry I took so long.

GEORGE: Oh that's OK, really, don't worry about it.

ELAINE: How do people fast? Did Ghandi get this crazy? I'm gonna walk around, see what dishes look good.

JERRY: I told my uncle I had a stomach ache tonight. You think he bought that?

GEORGE: Yeah, well, he probably bought it.

JERRY: So what happened with Tatiana?

GEORGE: I shouldn't even tell you this.

JERRY: Come on...

GEORGE: Well, after dinner last week, she invites me back to her apartment.

JERRY: I'm with you.

GEORGE: Well, it's this little place with this little bathroom. It's like right there, you know, it's not even down a little hall or off in an alcove. You understand? There's no... buffer zone. So, we start to fool around, and it's the first time, and it's early in the going. And I begin to perceive this impending... intestinal requirement, whose needs are going to surpass by great lengths anything in the sexual realm. So I know I'm gonna have to stop. And as this is happening I'm thinking, even if I can somehow manage to momentarily... extricate myself from the proceedings and relieve this unstoppable force, I know that that bathroom is not gonna provide me with the privacy that I know I'm going to need...

JERRY: This could only happen to you.

GEORGE: So I finally stop and say, &quot;Tatiana, I hope you don't take this the wrong way, but I think it would be best if I left&quot;.

JERRY: You said this to her after.

GEORGE: No. During.

JERRY: Oh, boy.

GEORGE: Yeah.

JERRY: Wow! So...?

GEORGE: So I'm dressing and she's staring up at me, struggling to compute this unprecedented turn of events. I don't know what to say to reassure this woman, and worst of all, I don't have the time to say it. The only excuse she might possibly have accepted is if I told her I am in reality Batman, and I'm very sorry, I just saw the Bat-Signal. It took me 3 days of phone calls to get her to agree to see me again. Now she's waiting for me to call her, and she's

(gestures towards woman on phone) still on the phone.

(Elaine comes over)

ELAINE: I hate this place. I don't know why we came here, I'm never coming back here again.

JERRY (still trying to remember): Who is that woman?!

ELAINE: Remember when you first went out to eat with your parents? Remember, it was such a treat to go and they serve you this different food that you never saw before, and they put it in front of you, and it is such a delicious and exciting adventure? And now I just feel like a big sweaty hog waiting for them to fill up the trough.

GEORGE: She's off. (goes over to the now available public phone)

ELAINE: Jerry, talk to that guy again.

JERRY: What am I gonna say?

ELAINE: Tell him we wanna catch a movie and that we're late.

(as Jerry approaches Bruce, a man walks in)

MR. COHEN: Hey, what stinks in here?

Bruce (laughing): Mr. Cohen! Haven't seen you for a couple of weeks.

MR. COHEN: Well, I've been looking for a better place.

(Bruce laughs)

BRUCE: Better place... Want a table?

MR. COHEN: No, just bring me a plate and I'll eat here.

BRUCE (laughing): Give him a plate and you eat here... Come on, I give you a table.

JERRY: Excuse me... we've been waiting here. Now, I KNOW we were ahead of that guy, he just came in.

BRUCE: Oh no, Mr. Cohen always here.

ELAINE: He's always here? What does that mean? What does that mean?

BRUCE: Oh, Mr. Cohen, very nice man. He live on Park Avenue.

ELAINE: Where am I? Is this a dream? What in God's name is going on here?!

GEORGE: She's not there. She left. She must've waited and left because those people wouldn't get off the phone.

JERRY: Didja leave a message?

GEORGE: Yeah, I told her to call me here and to tell anyone who answers the phone to ask for a balding, stocky man with glasses. I better tell him I'm expecting a call.

ELAINE: Oh, Jerry, here comes that woman...

JERRY: Where do I know her?

(woman in stripes approaches Jerry)

LORRAINE: Hello, Jerry!

JERRY: Heeeeyyyy... How you doin'?

LORRAINE: How is everything?

JERRY: Good, good, good... What's goin' on?

LORRAINE: Oh, working hard. And you?

JERRY: Oh, you know, working around, same stuff, doing... whatever.

LORRAINE: You haven't been around in a while.

JERRY: I know, I know... Well, you know.

LORRAINE: You should come by.

JERRY: Definitely. I plan to, I'm not just saying that.

ELAINE: Hi, I'm Elaine.

LORRAINE (shaking her hand): Lorraine. Catalano.

JERRY: I'm sorry, Lorraine, this is Elaine...

(They all laugh, then silent)

LORRAINE: Well it was nice seeing you, Jerry. And nice meeting you. (she leaves)

ELAINE (smug): Oh, nice to meet you too, Lorraine!

JERRY: Oh my god, Lorraine... that's Lorraine from my uncle's office. I'm in big, big trouble.

ELAINE: The one you broke the plans with tonight?

JERRY: Yeah, she works in his office. Now she's gonna see him tomorrow and tell him she saw me here tonight. He's gonna tell his wife, his wife's gonna call my mother. Oh, this is bad, you don't know, the chain reaction of calls this is gonna set off. New York, Long Island, Florida, it's like the Bermuda Triangle. Unfortunately, nobody ever disappears. My uncle to my aunt, my aunt to my mother, my mother to my uncle...

JERRY: ...My uncle to my cousin, my cousin to my sister, my sister to me.

ELAINE: You should've just had dinner with your uncle tonight and gotten in over with. It's just a movie.

JERRY: Just a movie?! You don't understand. This isn't 'Plans 1 through 8 from Outer Space', this is 'Plan 9', this is the one that worked. The worst movie ever made!

(Elaine nods)

JERRY: Hey, I got news for you, if we're making this movie, we gotta get a table immediately.

ELAINE: Alright, OK. Let's stop fooling around. Let's just slip him some money.

JERRY: In a Chinese restaurant? Do they take money?

ELAINE: Do they take money? Everyone takes money. I used to go out with a guy who did it all the time, you just slip him 20 bucks.

GEORGE: 20 bucks? Isn't that excessive?

ELAINE: Well what do you want to give him, change?

GEORGE: It's more than the meal!

JERRY: Oh, come on, We'll divide it up three ways.

GEORGE: Alright. 7,7, (points at himself) 6. I'm not gonna eat that much!

JERRY: I'm counting your shrimps. OK, Who's gonna do it?

GEORGE: Oh no, I can't do it. I-I'm not good at these things, I get flustered. Once I tried to bribe an usher at the roller derby, I almost got arrested.

ELAINE: I guess it's you, Jer.

JERRY: Me? What about you?

ELAINE: Oh, I can't do that, it's a guy thing.

JERRY: The woman's movement just can't seem to make any progress in the world of bribery, can they?

ELAINE: Give me the money.

(Elaine stands close to Bruce, trying to get his attention)

ELAINE: How's it going'?

BRUCE: Very busy.

(Elaine holds the money in front of Bruce)

ELAINE: Boy, we are REALLY anxious to sit down.

BRUCE: Very good specials tonight.

(Elaine puts the bill on the open reservations book)

ELAINE: If there's anything you can do to get us a table we'd really appreciate it.

BRUCE: What is your name? (he turns the page over the money)

ELAINE: No no, I want to eat now! (she gets the money from under the page)

BRUCE: Yes, we have sea-bass dinner tonight, very fresh.

ELAINE (Gives him the money): Here, take this. I'm starving. Take it! Take it!

(Bruce shrugs and takes it)

BRUCE: Dennison, 4! (goes over to 4 ladies) Your table is ready.

ELAINE: No no, no, I want that table. I want that table! Oh, come on, did you see that? What was that? He took the money, he didn't give us a table.

JERRY: You lost the 20.

ELAINE: Well, how could he do that?

GEORGE: You didn't make it clear.

ELAINE: Make it clear?

JERRY: What a sorry exhibition that was. Alright, let me get the money back.

(goes over to Bruce) Excuse me. I realize, this is extremely embarrassing, my friend here apparently made a mistake.

BRUCE: Your name?

JERRY: Seinfeld.

BRUCE: Yeah, Seinfeld 4!

JERRY: No no no, you see the girl there, with the long hair?

(Elaine waves at him, smiling)

BRUCE: Oh yes, yes. Very beautiful girl, very beautiful. Is your girlfriend?

JERRY: Well, actually, we did date for a while, but... it's really not relevant here.

BRUCE: Relationships are difficult. It's very hard to stay together-

JERRY: Alright, listen, alright. How much longer is it gonna be?

BRUCE: Oh. In about five, ten minutes.

(Jerry goes over to Elaine and George)

GEORGE: So?

JERRY: There seems to be a bit of a discrepancy.

ELAINE: So when are we gonna eat?

JERRY: Five, ten minutes.

GEORGE: We should have left earlier. I told you.

JERRY: I don't see any way we can eat and make this movie.

ELAINE: Oh, well I have to eat.

JERRY: Well let's just order to go, we'll eat it in the cab.

ELAINE: Eat it in the cab? Chinese food in a cab?

JERRY: We'll eat it in the movie.

ELAINE: Oh, who do you think you're going? Do you think that they have big picnic tables there?

JERRY: Well what do you suggest?

ELAINE: I say we leave now, we go to 'Skyburger' and we scarf 'em down.

JERRY: I'm not going to 'Skyburger'. Besides, it's in the opposite direction, let's just eat popcorn or something.

BRUCE (holding a phone): Cartwright?

ELAINE: I can't have popcorn for dinner!

BRUCE: Cartwright?

ELAINE (tries to snatch food off a waiter's tray): I have to eat!

JERRY: So they have hotdogs there.

ELAINE: Oh, movie hotdogs! I rather lick the food off the floor.

GEORGE: I can't go anywhere, I have to wait here for Tatiana's call. Let me just check.

(goes over to Bruce)

GEORGE: Excuse me, I'm expecting a call. Costanza?

BRUCE: Yeah, I just got a call. I yell 'Cartwright! Cartwright!', just like that. Nobody came up, I hang up.

GEORGE: Well, was it for Costanza or...

BRUCE: Yes, yes, that's it. Nobody answered.

GEORGE: Well was it a woman?

BRUCE: Yeah, yeah. I tell her you not here, she said curse word, I hang up.

(George comes over to Jerry and Elaine, stunned)

GEORGE: She called. He yelled Cartwright. I missed her.

JERRY: Who's Cartwright?

GEORGE: I'm Cartwright!

JERRY: You're not Cartwri-

GEORGE: Of course I'm not Cartwright! Look, why don't you two just go to the movies all by yourselves, I'm not in the mood.

ELAINE: Well me neither, I'm goin' to 'Skyburger'.

JERRY: So You're not going?

ELAINE: You don't need us.

JERRY: Well I can't go to a bad movie by myself. Who am I gonna make sarcastic remarks to, strangers? Eh, I guess I'll just go to my uncle's.

GEORGE: Should we tell him we're leaving?

ELAINE: What for? Let's just get out of here.

(they all leave)

BRUCE: Seinfeld, 4?

[closing monologue]

JERRY: Hunger will make people do amazing things. I mean, the proof of that is cannibalism. Cannibalism, what do they say, I mean, they're eating and, you know, &quot;This is good, who is this? I like this person&quot;. You know, I mean, I would think the hardest thing about being a cannibal is trying to get some very deep sleep, you know what I mean? I would think, you'd be like, (pretending to wake up) &quot;Who is that? Who's there? Who's there? Is somebody there? What do you want? What do you want? You look hungry, are you hungry? Get out of here!&quot;

The End
