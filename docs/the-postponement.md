---
layout: default
title: The Postponement
parent: Season 7
nav_order: 2
permalink: /the-postponement
cat: ep
series_ep: 112
pc: 702
season: 7
episode: 2
aired: September 28, 1995
written: Larry David
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697759/
wiki: https://en.wikipedia.org/wiki/The_Postponement
---

# The Postponement

| Season 7 - Episode 2   | September 28, 1995        |
|:-----------------------|:--------------------------|
| Written by Larry David | Directed by Andy Ackerman |
| Series Episode 112     | Production Code 702       |

"The Postponement" is the 112th episode of the NBC sitcom Seinfeld, and the second episode of the seventh season. It aired in the U.S. on September 28, 1995.

## Plot

Elaine's dog problem is solved by a rabbi in her apartment complex with a cable show. Elaine later confides in the rabbi about her insecurity about George getting engaged. The rabbi later tells several people, including Jerry, about Elaine's insecurity towards George's wedding.

Kramer's involvement in the dognapping worries him. George decides he wants to postpone the engagement until March 21 (Spring Equinox). His first attempt to postpone the wedding leads to Susan becoming hysterical and bursting into tears. Later George gets the idea to be nonchalant about the whole thing after watching a man break up with his girlfriend at Monk's. Intending to try a similar approach, he breaks down in tears and begs Susan to postpone the wedding; touched by his show of emotions, Susan agrees to postpone the wedding.

Kramer and Jerry go to see Plan 9 from Outer Space at the cinema. Kramer sneaks in gourmet coffee, spills it, and scalds himself; he says he has a case for a lawsuit (an obvious allusion to the well-known McDonald's coffee case).

The episode ends with Susan and George watching the rabbi's TV show and the rabbi recounting the story Elaine told him including a part about George (the rabbi references both Elaine and George by name) wanting to know if it was still cheating if he paid for a prostitute while engaged, resulting in the wedding's postponement being revoked.

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Cosmo Kramer

#### Recurring

Heidi Swedberg ............ Susan Biddle Ross

#### Guests

Bruce Mahler ...................... Rabbi (Glickman)  
Kelly Perine ........................ Usher  
John Rubano ...................... Man  
Evie Peck ........................... Woman

## Script

[Jerry and Elaine are walking down the street and Kramer is parking his car]

ELAINE: Hey, good news. My dog problem has been solved.

JERRY: Really? What happened?

ELAINE: Well, there's this rabbi in my building. You've met him. Very nice man.

JERRY: Isn't he the one with the show on cable?

ELAINE: Yea, yeah, yeah. So I spoke to him about the dog. He went down. Talked to the owner. She agreed to keep the dog inside from now on.

JERRY: That's great.

ELAINE: I know.

(Kramer crashing into parking spot)

JERRY: That looks pretty good.

ELAINE: He's in.

JERRY: Hey, say, you know, we haven't even discussed George's engagement yet.

ELAINE: What's to discuss?

JERRY: Come on! George is getting married!

ELAINE: Is he happy?

[At the restaurant! George is coming from the bathroom to sit with his bride-to-be.]

GEORGE: I will never understand the bathrooms in this country. Why is it that the doors on the stalls do not come all the way down to the floor?

SUSAN: Well, maybe it's so you can see if there's someone in there.

GEORGE: Isn't that why we have locks on the doors?

SUSAN: Well, as a backup system, in case the lock is broken, you can see if it's taken.

GEORGE: A backup system? We're designing bathroom doors with our legs exposed in anticipation of the locks not working? That's not a system. That's a complete breakdown of the system.

SUSAN: Can we change the subject, please?

GEORGE: Why? What's wrong with the subject? This is a bad subject?

SUSAN: No, fine. If you wanna keep talking about it, we'll talk about it.

GEORGE: It's not that I want to keep talking about it? I just think that the subject should resolve itself based on its own momentum.

SUSAN: Well, I didn't think that it had any momentum.

GEORGE: (To himself) How am I gonna do this? I'm engaged to this woman? She doesn't even like me. Change the subject? Toilets were the subject. We don't even share the same interests.

[Jerry's]

JERRY: Yeah, he seems pretty happy.

ELAINE: Well, that's all that counts, I guess.

JERRY: What's the matter?

ELAINE: Oh, nothin'.

JERRY: Well, you don't seem too enthused about the whole thing.

ELAINE: Well, what do you want me to do?

JERRY: Well, at least have some reaction to it.

ELAINE: [drinks beer] ...Well, I don't.

JERRY: Maybe you're a little jealous.

ELAINE: Oh, what? You think I wanna marry George?

JERRY: No! But maybe you wish it was you who was getting married, not him.

ELAINE: Oh, please! That is the last thing that I want.

JERRY: Oh, yeah. Right.

ELAINE: Yeah, right.

JERRY: Lainey!

ELAINE: Jerry!

JERRY: You don't wanna get married?

ELAINE: Yeah, that's right. I don't wanna get married.

JERRY: Oh, come on!

ELAINE: Oh, you come on.

JERRY: You're such...

(Kramer enters)

KRAMER: Oh, hey!

JERRY: Hey.

KRAMER: Elaine, listen, I was talking to a friend about this dog business. Do you realize this is gonna be on our permanent records? Are you aware of this?

ELAINE: Oh, dear.

KRAMER: It can never be erased. It'll follow us wherever we go for the rest of our lives. I'll never be able to get a job. I mean, doesn't that concern you? Everything I've worked for... down the drain because of one stupid mistake. I mean, aren't we entitled to make one mistake in our lives, Jerry?

JERRY: We gotta change the system.

KRAMER: Yes!

ELAINE: [crushing beer can] Well, I could care less. I hope it is on our record. I'm just sorry they didn't lock me up.

[Outside Elaine's place]

ELAINE: Oh, hello, Rabbi Krischma.

RABBI: Elaine! Always a pleasure to see you.

ELAINE: Thanks again for taking care of that dog for us.

RABBI: Elaine, often times in life there are problems, and just as often there are solutions.

ELAINE: Yeah, I suppose.

RABBI: Elaine, you don't seem yourself today. You seem, if I may say, troubled.

ELAINE: No, Rabbi, I'm not myself.

RABBI: Come upstairs. We'll have a talk.

[At Jerry's apartment. George trots in after his lunch with Susan and Jerry is kicking back with a paper.]

JERRY: Hey!

GEORGE: I want your honest opinion about something.

JERRY: Have I ever been less than forthright?

GEORGE: No, you haven't. Well, maybe you have. What do I know.

JERRY: Yeah, I probably have. Yeah, of course I have. What am I talking about?

GEORGE: All right. Okay, tell me what you think about this idea: Extend the doors on the toilet stalls at Yankee Stadium all the way to the floor.

JERRY: Extend the doors on the toilet stalls at Yankee Stadium to the floor ...door comes down. Hides your feet. Yes. I like it. I like it a lot.

GEORGE: It's good, right?

JERRY: I think it's fantastic. I think it's a fantastic idea.

GEORGE: You do?

JERRY: Yes, I do.

GEORGE: Well, I told it to Susan before, and she didn't like it.

JERRY: Hmm.

GEORGE: Yeah. Not only that, this is what she said to me, &quot;Can we change the subject?&quot;

JERRY: See, now that I don't care for.

GEORGE: Right. I mean, we're on a subject. Why does it have to be changed?

JERRY: It should resolve of its own volition.

GEORGE: That's exactly what I said, except I used the word &quot;momentum&quot;.

JERRY: Momentum - same thing.

GEORGE: Same thing. My god, I'm getting married in December, do you know that?

JERRY: Yeah, I know.

GEORGE: Well, I don't see how I'm gonna make December. I mean, I need a little more time. I mean, look at me I'm a nervous wreck. My stomach aches. My neck is killing me. I can't turn. Look. Look.

JERRY: You're turning.

GEORGE: Nah, it's not a good turn. December. December. Don't you think we should have a little more time just to get to know each other a little.

JERRY: If you need more time, you should have more time.

GEORGE: What, you think I could postpone it?

JERRY: Sure you can. Why not?

GEORGE: That's allowed? You're allowed to postpone it?

JERRY: I don't see why not.

GEORGE: So, I could do that?

JERRY: Sure, go ahead.

GEORGE: Alright! Alright. I'll tell you what. How about this? Got the date; March 21st, the first day of spring.

JERRY: Spring. Of course.

GEORGE: Huh? You know? Spring. Rejuvenation. Rebirth. Everything's blooming. All that crap.

JERRY: Beautiful.

GEORGE: She's not gonna like it.

JERRY: No, she's not.

GEORGE: You know, I think I'm a little bit scared of her. She's five-three, like a hundred pounds. I'm frightened to death of her.

JERRY: Well, she's a woman. They don't like to be disappointed.

GEORGE: Especially her. She does not like disappointment. Well, I have to do it. I can't make December. There's no way I can make December. Right? I mean, you can see that, right? I mean, look at me. Look. Look. Can I make December? I can't make December. Right? Look. Look.

JERRY: Yeah, you'd better shoot for March.

(Kramer enters)

KRAMER: Hey, hey.

GEORGE: March 21st. Hey! So, you're gonna back me on this, right?

JERRY: Oh, all the way.

GEORGE: You are a good friend. You know what? Even if you killed somebody I wouldn't turn you in.

JERRY: Is that so?

JERRY: Hey, Kramer if I killed somebody would you turn me in?

KRAMER: Definitely.

JERRY: You're kidding?

KRAMER: No, no, I would turn you in.

JERRY: You would turn me in?

KRAMER: Phwap, I wouldn't even think about it.

JERRY: I can't believe your a friend of mine.

KRAMER: What kind of person are you going around killing people?

JERRY: Well, I am sure I had a good reason.

KRAMER: Well, if you'll kill this person, who's to say I wouldn't be next?

JERRY: But you know me!

KRAMER: I *thought* I did!

[Rabbi's apartment]

ELAINE: I'm not a very religious person but I do feel as if I'm in need of some guidance here.

RABBI: Would you care for a snack of some kind? I have the Snackwells which are very popular but I think that sometimes with the so called fat free cookies people may overindulge, forgetting they may be high in calories

ELAINE: Thank you I am not very hungry. Anyway, um, this friend of mine, George, got engaged.

RABBI: How wonderful.

ELAINE: Yeah, yeah, well, for some reason, um, I just find myself just overcome with feelings of jealousy and resentment.

RABBI: Doesn't it give you any joy to see your friend enter into this holiest of unions?

ELAINE: No, no, no it doesn't. No joy, no joy whatsoever. Just the whole think makes me... sick.

RABBI: You know, Elaine, very often we cannot see the forest for the trees.

ELAINE: Yeah, I don't know what that means.

RABBI: Well, for example, say there's a forest...

ELAINE: You see the thing is it should have been me. You know, I'm smart. I'm attractive.

RABBI: You know my temple has many singles functions.

ELAINE: No, no, it's okay.

RABBI: My nephew Alex is someone who is also looking, perhaps...

ELAINE: I don't think so.

RABBI: He owns a flower store. Very successful.

[Outside Coffee shop]

JERRY: So you're nothing but a stoolie. Admit it.

KRAMER: Hey, don't do the crime if you can't do the time.

JERRY: Another Caf&eacute; Latte?

KRAMER: You better believe it.

KRAMER: Since when are you so trendy?

JERRY: Hey, baby, I set the trends. Who do you think started this whole Caf&eacute; Latte?

JERRY: I don't recall you drinking Caf&eacute; Lattes.

KRAMER: I've been drinking Caf&eacute; Lattes since the fifth grade and I haven't looked back.

JERRY: Hey, Planet 9 From Outer Space is playing tomorrow night. One show only.

KRAMER: I've always wanted to see this.

JERRY: You know I was supposed to see this five years ago. I was in a Chinese restaurant with George and Elaine and got all screwed up trying to get a table and I missed it.

KRAMER: Well, yeah, lets do it huh?

JERRY: Look at this Jerry, dropping paper on the ground. That's littering.

JERRY: Maybe you better call the cops and turn me in.

KRAMER: Maybe I will.

[George's]

GEORGE: Hi,

SUSAN: How was your day?

GEORGE: Good, good day. How was your day?

SUSAN: Mine was okay. So what's goin' on?

GEORGE: Oh, nothin' much. I went over to jerry's, uh, talked to Jerry.

SUSAN: Oh, the Lauers want to get together with us on Friday night.

GEORGE: The Lauers, really?

SUSAN: You don't want to go?

GEORGE: No, I want to go.

SUSAN: So what did Jerry have to say?

GEORGE: Oh, nothin' much ...talkin' ...Oh, oh, oh, did I have an unbelievable idea today!

SUSAN: Oh, yeah, the toilets. You told me.

GEORGE: Yeah, ha ha, it's not the toilets, it's not the toilets. It's something else. Are you ready for this?

SUSAN: Yeah.

GEORGE: Okay, how about this? All right, we get married March 21st, the first day of Spring.

SUSAN: What do you mean? You want to postpone the wedding?

GEORGE: No, no no it's not about postponing. I just think the first day of Spring is the perfect day to get married. You know, Spring! Rejuvenation! Rebirth! Everything is blooming all the...

SUSAN: If you don't want to marry me, George, just say so. (crying) Say so.

GEORGE: Still marry, still marry.

SUSAN: You don't love me.

GEORGE: Still love. Still love.

SUSAN: My parents told me you were too neurotic and that I was making a mistake.

GEORGE: No no no, no mistake, no mistake. No, no, listen, we're going to get married over Christmas, I... It doesn't make any difference to me. It's fine. Really.

SUSAN: Are you sure?

GEORGE: Yeah, yeah, sure, Christmas. Snow. Santa. All that stuff.

[Monk's]

JERRY: Let me take a guess. She cried and you caved.

GEORGE: How did you know that?

JERRY: I live and breathe my friend... I live and breathe.

GEORGE: I got to tell you I felt terrible. I really thought she was going to collapse and kill herself.

JERRY: Yes, it's very difficult. Few men have the constitution for it. That's why breakups take two or three tries. You gotta build up your immunity.

GEORGE: You see those tears streaming down you don't know what to do. It was like she was on fire and I was trying to put her out.

JERRY: Well, at least you probably had some, uh, pretty good make-up sex after.

GEORGE: I didn't have any sex.

JERRY: You didn't have make-up sex? How could you not have make-up sex? I mean that's the best feature of the heavy relationship.

GEORGE: I didn't have make-up sex.

JERRY: In your situation the only sex you're going to have better than make-up sex is if you're sent to prison and you have a conjugal visit.

GEORGE: Yeah, conjugal visit sex. That is happening!

WOMAN: (crying)

MAN: I can tell you're very upset but I'm sorry I'm not goin'.

GEORGE: Did you here that? I can't believe this he's eating his sandwich.

MAN: Are you going to eat those fries?

GEORGE: This is amazing. (George gets up to leave and shake's man's hand) Thank you. Thank you very much... I'm going back in! ...You'll feel better (to woman)

JERRY: ...Poor bastard.

[Outside Elaine's]

JERRY: Good evening, Rabbi.

RABBI: Good evening. And how does this evening find you?

JERRY: Well, Rabbi, well.

RABBI: I trust you are here to see your friend, Elaine.

JERRY: Yeah, that's right.

RABBI: I hope she's feeling better.

JERRY: What do you mean?

RABBI: She didn't tell you?

JERRY: No.

RABBI: Well it seems the engagement of her friend George has left her feeling bitter and hostile.

JERRY: Is that so?

RABBI: Yes, in fact she told me that she wishes she was the one getting married.

JERRY: Really?

RABBI: She came off as pretty desperate.

JERRY: I didn't know any of this.

RABBI: Apparently she doesn't think much of this George fellow either. I recall the word &quot;loser&quot; peppered throughout her conversation.

JERRY: Hmm, well it all comes as news to me.

[George's apartment]

GEORGE: (enters) Hi.

SUSAN: Hi, how was your day?

GEORGE: Good, good day. How was your day?

SUSAN: Ah, it was okay. What's going on?

GEORGE: Oh, nothing much. You know, I went over to Jerry's. Talked to Jerry. Um, could I talk to you for a minute?

SUSAN: Yeah, sure.

GEORGE: You see this is the thing... (crying) I just feel... (mumbling and crying) I'm scared. You and I together... (sobbing)

SUSAN: George, of course, of course it can wait until march if that is what you want.

GEORGE: Yeah?

SUSAN: Oh, don't worry your head. Of course.

GEORGE: All right. (smiles behind her back)

[Elaine's]

ELAINE: I've got that magazine article for you.

JERRY: You know I talked to the rabbi outside.

ELAINE: Are you.

JERRY: Understand you had a little talk with him too.

ELAINE: Yeah, talked earlier.

JERRY: Yes I know, I know.

ELAINE: ...What does that mean?

JERRY: Nothing, nothing.

ELAINE: He didn't mention...

JERRY: Yes he *did*.

ELAINE: He told you about our conversation?

JERRY: We had quite a little chat.

ELAINE: He told you about...

JERRY: Yes, about how you're very jealous of George. How you wished it was you who were getting married instead of him.

ELAINE: He told you all that? How could he?

JERRY: It didn't take much prodding either, I must say.

ELAINE: Can he do that?

JERRY: He did it.

ELAINE: But he's a Rabbi! How can a Rabbi have such a big mouth?

JERRY: That's what's so fascinating.

(Movie line)

JERRY: You better finish your little caf&eacute; latte there. They won't let you in with it.

KRAMER: Why not?

JERRY: Because they don't allow outside drinks into the movie.

KRAMER: Well that's stupid

JERRY: That's the rule.

KRAMER: Well, we'll just see if we can't get around that.

(Kramer puts coffee cup into his pants)

[Rabbi's apartment]

RABBI: Oh, Elaine. Come in. Come in. So nice to see you again.

ELAINE: Yeah...

RABBI: Can I offer you some Kasha Varnishkas?

ELAINE: No, no. Listen, Rabbi, I'd like to ask you a question. Why, why did you tell my friend Jerry what I talked to you about?

RABBI: Was that a problem for you?

ELAINE: Of course it was a problem for me... You didn't, you didn't tell anyone else about this, did you?

RABBI: Well, let's see? I seem to recall a conversation with Mrs. Winston in 1F.

ELAINE: Mrs. Winston?

RABBI: Yes, we were waiting for our mail to arrive and I happened to mention to her how you felt that it was never going to &quot;happen&quot; for you.

ELAINE: What about Don Ramsey? You didn't mention anything to him did you?

RABBI: Don Ramsey?

ELAINE: You know that tall really good looking guy, he lives on the fifth floor.

RABBI: Oh him! Well this morning I found myself in the elevator with him...

ELAINE: My god, you didn't.

(Movie theatre)

JERRY: Excuse me, pardon me, excuse me.

KRAMER: Oh, yow, oow Ah!

USHER: Hey, hey, what's going on? What just happened here?

KRAMER: Nothing Nothing.

USHER: Whatya got? One of those Caf&eacute; Lattes in your shirt?

KRAMER: I don't have anything. Ask him.

(Jerry makes a silent drink gesture)

USHER: All right, come on Coffee Boy, bring it out.

KRAMER: What?!

USHER: Here you go.

KRAMER: Ow

(Kramer leaves)

[Jerry's apartment]

ELAINE: But the whole thing is a mess. He told everyone in the building. I met that cute guy on the fifth floor. I mean he could barely bring himself to nod.

JERRY: Elaine, if I could say a word here about Jewish people. That man in no way represents our ability to take in a nice piece of juicy gossip and keep it to ourselves.

ELAINE: You didn't say this to George, did you?

JERRY: No... about how you wish it was *you* who was getting married instead of him? Feelings of resentment, hostility?

ELAINE: Yeah that! So...

(George enters)

GEORGE: Hey oh.

ELAINE: GEORGIE! CONGRATULATIONS! Oh, my god. I haven't seen you since it happened. I'm so happy for you.

GEORGE: Alright, thanks a lot.

ELAINE: Oh, come on. You really, really deserve it.

(Gives George a kiss)

GEORGE: Oh, deserve! I don't know if I deserve... I mean...

ELAINE: Are you kidding? I have seen the changes in you the past couple of years. Man, you have grown. You've matured.

GEORGE: Well, I guess I'm getting older.

ELAINE: Oh! Well, I just think it's wonderful. Honestly! I've gotta run, but um, please, please give my best to Susan.

GEORGE: Yeah.

ELAINE: My most, just heartfelt congratulations.

GEORGE: Yeah. Thanks. Hey, listen, if you ever get a date, maybe the four of us could go out together sometime.

ELAINE: Yes! Yes, yes. Sure.

GEORGE: Wait, as a matter of fact, wasn't there some guy in your building that you said you liked? He lived up on the fifth floor or something.

ELAINE: Yes. Yes, yes. Yes.

GEORGE: Yeah! Boy, she is something, isn't she?

JERRY: Yeah, she's something else. Hey, so what happened? Did you hold your ground or... uh...

GEORGE: Nope. I wept like a baby.

JERRY: What?

GEORGE: Well, I started to tell her and then all of the sudden, for some reason, I just burst into tears.

JERRY: You cried?

GEORGE: I bawled uncontrollably. I just poured my guts out. And I'll tell you, Jerry, it was incredible. I never realized how powerful these tears are. I could have postponed it another five years if I wanted to.

JERRY, GEORGE, &amp; KRAMER: Hey!

JERRY: Sorry about that movie-thing. I was joking around.

KRAMER: Sorry? Are you kidding? You did me the biggest favor of my life. I spoke to a lawyer, we're suing for millions.

JERRY: Suing? What for?

KRAMER: The coffee was too hot.

JERRY: It's supposed to be hot.

KRAMER: Not *that* hot.

[At George's home. He and Susan are in bed watching TV]

RABBI: (On TV) The prophet Isaah tells us without friends our lives are empty and meaningless.

GEORGE: Wait. Whoa! That's the Rabbi from Elaine's building. I just met this guy the other day.

RABBI: A young lady I know, let's call her Elaine, happened to find herself overwhelmed with feelings of resentment and hostility for her friend, let's call him George. She felt that George was somewhat of a loser and that she was the one who deserved to be married first. She also happened to mention to me that her friend had wondered if going to a prostitute while you're engaged is considered cheating. His feeling was they're never going to see each other again so what's the difference. But that is a subject for another sermon. Now, I'd like to close with a psalm.

The End
