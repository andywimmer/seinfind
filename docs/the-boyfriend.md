---
layout: default
title: The Boyfriend
parent: Season 3
nav_order: 17
permalink: /the-boyfriend
cat: ep
series_ep: 34
pc: 315
season: 3
episode: 17
aired: February 12, 1992
written: Larry David and Larry Levin
directed: Tom Cherones
imdb: http://www.imdb.com/title/tt0697738/
wiki: https://en.wikipedia.org/wiki/The_Boyfriend_(Seinfeld)
---

# The Boyfriend

| Season 3 - Episode 17                  | February 12, 1992        |
|:---------------------------------------|:-------------------------|
| Written by Larry David and Larry Levin | Directed by Tom Cherones |
| Series Episode 34                      | Production Code 315      |

"The Boyfriend" (also known as "The New Friend") is a two-part episode of the sitcom Seinfeld. It makes up the 34th and 35th episodes of the show, and 17th and 18th episodes of the show's third season. It first aired on February 12, 1992. In the "extras" section of the Season 3 DVD, Jerry Seinfeld says it is his favorite episode. Upon its first airing, it was initially titled "The New Friend".

## Plot

### Part 1

Jerry meets an idol of his—former New York Mets baseball player Keith Hernandez (appearing as himself)—and wants to make a good impression. Meanwhile, George is out of time on his unemployment and he works harder than ever on his scheme to get a 13-week extension. He tells the unemployment office that he was close to a job with Vandelay Industries, a company made up by George that makes latex products and whose main office's phone number is Jerry's apartment. Kramer and Newman accuse Keith of spitting on them after a past Mets game by the players parking lot at Shea Stadium; however, Jerry supports a "second-spitter theory" and that Keith was not involved. Keith asks Jerry about Elaine's relationship status, then makes a date with her, breaking a date he previously made with Jerry.

### Part 2

Having been busted by his unemployment officer after Kramer unknowingly answers Jerry's phone, George tries to curb losing his benefits by taking the officer's daughter (Carol Ann Susi) out for a date, which goes terribly wrong for him and causes him to express to Jerry his desire to date a tall woman. Meanwhile, Jerry becomes jealous that Keith is spending more of his time dating Elaine until Elaine ends the relationship because he smokes. When Keith asks Jerry to help him move his furniture, Jerry finally has enough and ends their friendship. Right then, Kramer and Newman confront Keith on the alleged "spitting incident". Keith provides them with the truth that, he in fact saw the real spitter, and names Phillies relief pitcher Roger McDowell. Kramer and Newman then remember they had taunted McDowell throughout the game and the pair apologize to Keith, also offering to help move his furniture. George rushes in with one last desperate attempt to win over his unemployment officer by getting Keith to meet her, but he is too late. As he mopes, a tall woman suddenly appears with George's wallet, which he had dropped on the sidewalk outside, causing George to give a happy smile.

## Pastiche of JFK film

The "spitting incident" depicted in the story is a pastiche of the 1991 film JFK. Jerry presents the "Magic Loogie Theory", a reference to the "Magic Bullet Theory" featured in the film. Also, the recount of the incident in the episode resembles the Zapruder film in JFK, as it uses the same color and photography effects. Additionally, the episode features Wayne Knight (as Newman), who also appeared in JFK in the same position as the scene it depicts.

In the episode "The Seinfeld Chronicles" (which aired July 1989), Jerry notes that Kramer "[hasn't] left the building in ten years," a point which Kramer does not dispute. This was clearly an exaggeration, since this episode reveals that Kramer, with Newman, attended a baseball game at Shea Stadium on June 14th, 1987.

## Reception and legacy

TV Guide ranked the episode fourth in their 1997 list of the 100 Greatest TV Episodes of All Time.

On June 23, 2010, Jerry Seinfeld called four innings of a Mets game at Citi Field against the Detroit Tigers on SportsNet New York, reuniting him with Hernandez, now an analyst for SNY. During that time, he revealed that if Hernandez had turned them down they would have asked Gary Carter to take his place.

## Cast

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Recurring

Wayne Knight ............... Newman

#### Guests

Keith Hernandez ................ Himself  
Rae Allen ............................. Mrs. (Lenore) Sokol  
Richard Assad ..................... Cabbie
Carol Ann Susi .................... Carrie  
Lisa Mende .......................... Carol  
Roger McDowell ................ Himself  
Stephen Prutting ................ Michael  
Richard Assad .................... Cabbie  
Melanie Good ..................... Woman

## Script

[Opening Monologue]

To me, going to the health club, you see all these people and they're working out, and they're training and they're getting in shape but the strange thing is nobody is really getting in shape for anything. The only reason that you're getting in shape is that so you can get through the workout. So we're working out, so that we'll be in shape, for when we have to do our exercise. This is the whole thing. The other thing I don't get about it, is why we're so careful about locking up our dirty towels and smelly jock-straps. What exactly is the black market on these disgusting gym clothes? I give my car to any valet guy in front of a restaraunt because he has a short red jacket, yeah he must be the valet guy, I don't even think about it but my stinking, putrified gym clothes, I got one of these locks you could put a bullet through it and it won't open. That stuff is safe.

[Gym Locker Room]

KRAMER: Wide open, I was wide open underneath! I had three inches on that guy. You two were hogging the ball.

GEORGE: Me? It wasn't me I never even saw the ball. All you do is dribble.

JERRY: I have to dribble, if I give it to you, you just shoot. You're a chucker.

GEORGE: Oh I'm a chucker.

JERRY: That's right, everytime you get the ball you shoot.

GEORGE: I can't believe you called me a chucker. No way I'm a chucker, I do not chuck, never chucked, never have chucked, never will chuck, no chuck!

JERRY: You chuck.

GEROGE: Kramer am I a chucker?

KRAMER: You're a chucker.

GEORGE: All these years I've been chuckin' and you've never told me?

JERRY: Well it's not an easy thing to bring up.

KRAMER: Hey you know this is the first time we've ever seen each other naked.

JERRY: Believe me I didn't see anything.

KRAMER: Oh, you didn't sneak a peak?

JERRY: No, did you?

KRAMER: Yeah, I snuck a peak.

JERRY: Why?

KRAMER: Why not? hey what about you George?

GEORGE: yeah, I ... I snuck a peak. ... But it was so fast I didn't see anything. It was just a blur.

JERRY: I made a conscious effort not to look. There's certain information I just don't want to have.

KRAMER: Uh, I gotta go meet Newman. All right. I'll see you later.

JERRY: All right

KRAMER: Have a good one.

JERRY: All right

GEORGE: See ya.

(Kramer exits)

GEORGE: look at this guy. Does he have to stretch in here?

JERRY: You know who that is? That's

GEORGE: Keith Hernandez? The baseball player?

JERRY: Yeah, that's him.

GEORGE: Are you sure?

JERRY: Positive.

GEORGE: Wow, Keith Hernandez. He's such a great player.

JERRY: Yeah, he's a real smart guy too. He's a Civil War buff.

GEORGE: I'd love to be a Civil War buff. ... What do you have to do to be a buff

JERRY: So Biff wants to be a buff? ... Well sleeping less than 18 hours a day would be a start.

GEORGE: ho ho ho ho. You know I only got two weeks left of unemployment. I got to prove I've been looking for a job to get an extension

JERRY: Hey, should we say something to him?

GEORGE: Oh, yeah I'm sure he loves to hear from fans in the locker room.

JERRY: well he could say hello to me. I wouldn't mind.

GEORGE: He's Keith Hernandez. You're Jerry Seinfeld.

JERRY: So?

GEORGE: What are you comparing yourself to Keith Hernandez. The guys a baseball player Jerry, Baseball!

JERRY: I know what he is. I recognized him. You didn't even notice him.

GEORGE: What, ... you are making some wisecracks in a night club... wo wo wo. The guy was in game SIX two runs down two outs facing elimination.

KEITH: Excuse me. I don't want to disturb you, I'm Keith Hernandez and I just want to tell you what a big fan I am. I love your comedy.

JERRY: Really?

KEITH: I've always wanted to do what you do.

JERRY: What I do? You are one of my favorite ball players of all time

GEORGE: Mine too.

KEITH: I love that bit about Jimmy Olson

JERRY: Thank you.

GEORGE: You know Keith, what I've always wondered, with all these ball clubs flying around all season don't you think there would be a plane crash? ...

KEITH: (to Jerry) Do you perform anywhere in new York right now?

JERRY: I'm performing in this club on the east Side. You should come in.

GEORGE: But if you think about it...26 teams, 162 games a season, you'd think eventually an entire team would get wiped out.

KEITH: You know, I live on the East Side.

JERRY: I'll tell you what, I'll give you my number and uh, just give me a call, tell me whenever you want to go.

KEITH: or maybe just to get together for a cup of coffee

JERRY: Oh. that would be great.

GEORGE: Uh, it's only a matter of time.

KEITH: Who's this chucker?

[Monks]

JERRY: It's been three days and he hasn't called.

ELAINE: Well maybe you should call him.

JERRY: I can't ... I can't

ELAINE: Why not?

JERRY: I don't know. I just feel he should call me.

ELAINE: What's the difference?

JERRY: You don't understand, Elaine. I don't want to be overanxious. If he wants to see me he has my number, he should call.

ELAINE: Yech, look at this ashtray. I hate cigarettes.

JERRY: I can't stand these guys. You give your number to them and then they don't call. Why do they do that?

ELAINE: I'm sorry honey.

JERRY: I mean, I thought he liked me. I really thought he liked me. we were getting along. He came over to me I didn't go over to him.

ELAINE: No,

JERRY: Why did he come over to me if he didn't want to see me?

ELAINE: I know.

JERRY: What did he come over to me if he didn't want to see me? I mean here I meet this guy this great guy, a baseball player, best guy I ever met in my life. .. Well that's it. I'm never giving my number out to another guy again.

ELAINE: Sometimes I've given my number out to guys and it takes them a month to call.

JERRY: Hu, good, good,... well if he's calling in a month he's got a prayer!

ELAINE: You know maybe he's been busy. Maybe he's been out of town?

JERRY: Oh, they don't have phones out of town? Why do(?) people say they're too busy. Too busy. Pick up a phone!! It takes two minutes. How can you be too busy?

ELAINE: Why don't you just go ahead and call him?

JERRY: I can't call here, it's a coffee shop. I mean what am I going to say to him?

ELAINE: Just ask him if he wants a to get together.

JERRY: For what dinner?

ELAINE: Dinner's good.

JERRY: Don't you think that's coming on a little too strong? .. Isn't that like a turn off?

ELAINE: Jerry, He's A GUY!

JERRY: ... this is all .. very confusing.

[Middle Monologue]

When you're in your thirties it's very hard to make a new friend. Whatever the group is that you've got now that's who you're going with. you're not interviewing, you're not looking at any new people, you're not interested in seeing any applications. They don't know the places. They don't know the food. They don't know the activities, If I meet a guy in a club on the gym or someplace I'm sure you're a very nice person you seem to have a lot of potential, but we're just not hiring right now. Of course when you're a kid, you can be friends with anybody. Remember when you were a little kid what were the qualifications? If someone's in front of my house NOW, That's my friend, they're my friend. That's it. Are you a grown up.? No. Great! Come on in. Jump up and down on my bed. And if you have anything in common at all, You like Cherry Soda? I like Cherry Soda! We'll be best friends!

[New York State Department of Labor]

MRS. SOKOL: You know you only have two more weeks before your benefits run out.

GEORGE: Yes and I was hoping ... to get a thirteen week extension.

MRS. SOKOL: So where have you been looking for work?

GEORGE: Well you know what I've discovered Mrs. Sokol. It's not so much the looking as the listening. I listen for work. And as I'm looking and listening I am also looking. You can't discount looking. It's sort of a combination. It's looking, and listening, listening and looking. But you must look.

MRS. SOKOL: Can you be specific about any of these companies?

GEORGE: Specific, Ah, lets see. I've walked in and out of so many buildings they all .. blend in together, I uh, ..

MRS. SOKOL: Well just give me one name.

GEORGE: Absolutely, uh, lets see there's, uh, Vandelay Industries, I just saw them. I got very close there. very close.

MRS. SOKOL: And what type of company is that?

GEORGE: Latex, latex manufacturing

MRS. SOKOL: And you interviewed there?

GEORGE: Yes, for a sales position. Latex salesman, the selling of latex, and latex related products. They just wouldn't give me a chance.

MRS. SOKOL: I'm going to need an address and a phone number for this uh, Vandelay company...

GOERGE: You like gum? 'Cause I have a friend in the gum business. I got a gum guy. I make one phone call. I got boxes of delivered right to your door.

MRS. SOKOL: The address!

GEORGE: YYYDDSSHE(?) ... Jose Jimenez. You recognize it?

MRS. SOKOL: No.

GEORGE: Jose Jimenez, ... verrry funny. ..very funny.

MRS. SOKOL: The ADDRESS!

GEORGE: uh, Uh, Vandelay Industries, is uh. 129 West 81st street. It's a very small industry Vandelay. It's one of the reasons I wanted to uh, work for them.

MRS. SOKOL: The PHONE number.

GEORGE: That's uh, KL5-8383. Are you calling them soon because, they keep very strange hours.

MRS. SOKOL: As soon as I'm done wit you!

GEORGE: Sure, well uh, you know I'll check in with you next week uh, I gotta run now because I got a full plate this afternoon. All right, really go to uh,.

(George runs down hall)

[Street phone booth- George pulls kid out of booth]

GEORGE: (Frantically, takes phone and screams...) He'll call you back.

[Jerry's Apartment]

KRAMER: (loungingly talking on phone) It's a par five. So you know I step up to the tee and I hit a beautiful drive right down the middle of the fairway. I mean you know my hook, right?

JERRY: Elaine, how about this shirt? Is this okay?

ELAINE: Jerry, ... He's a GUY!

KRAMER: well it's a dog leg left, so I play the hook right? .. hold on there's another call.

[Phone Booth]

GEORGE: (Frantically) Jerry, Jerry?

KRAMER: George?

GEORGE: Kramer put Jerry on the phone.

KRAMER: (Angrily) Yeah, look I'm in the middle of something. Call back.

GEORGE: Kramer!! Kramer no!!

KRAMER: ... so the ball takes of and I'm waiting for it to turn.

[Phone Booth]

GEORGE: hitting phone

[Cop bangs on booth with kid beside him]

(George looks at him)

[Jerry's Apartment]

KRAMER: Yeah, I'll talk to Jerry. Yeah, [Hangs up] . . . you know that was Michael and Carol. She's wondering when we're going to come over and see the baby.

JERRY: Oh, see the baby again with the baby..

ELAINE: Who are they?

JERRY: Uh, he's this guy who used to live in the building and they keep calling us to see the baby.

JERRY: (imitates) Ya' gotta see the babi - When are ya' gonna see the babi... Can't they just send us a tape?

ELAINE: You know if you waited a few more months it won't be a baby anymore then you wouldn't have to see it.

JERRY: uh uh because then it would be all grown up.

ELAINE: yeah ha ha ha

JERRY: Hey Kramer what do you think of this shirt?

KRAMER: (does a double take) It's too busy

ELAINE: It looks like you're trying too hard to make an impression on him. You're not being yourself.

KRAMER: What guy?

JERRY: I know he's just a guy but .. I LIKE him.

KRAMER: Who are you talkin about?

JERRY: Uh, Keith uh Hernandez.

KRAMER: KEITH HERNANDEZ?

NEWMAN: (enters) KEITH HERNANDEZ?

[In cab]

GEORGE: Do me a favor would you? Would you change lanes? Would you get outta this lane. You gotta get out of this lane. This lane stinks. They're all double parked here Please get outta this lane. I'm beggin you please please.

(changes lanes)

GEORGE: You know what, bad mistake my mistake do me a favor go back to the other lane - you'll never get there - forget this lane - y'a kn ow what this lane stinks - go back to the other lane - bad decision - go go go take this light - take this light -

CABBY: That's it GET OUT!!

GEORGE: Get out?

CABBY: Get out of my cab.

GEORGE: Wa, I'm not getting out of this cab

(Cabby gets out)

GEORGE: No, no! You can't throw me out

(Wrestling in cab)

[Jerry's Apartment]

JERRY: Hellooo Newman.

KRAMER: I hate KEITH HERNANDEZ - hate him.

NEWMAN: I despise him.

ELAINE: Why?

NEWMAN: Why? I'll tell you why...

KRAMER: Let me tell it ..

NEWMAN: No, you can't tell it ..

KRAMER: You always tell it ..

NEWMAN: All right, tell it.

KRAMER: Ja ja ja - just tell it

NEWMAN: June 14, 1987.... Mets Phillies. We're enjoying a beautiful afternoon in the right field stands when a crucial Hernandez error to a five run Phillies ninth. Cost the Mets the game.

KRAMER: Our day was ruined. There was a lot of people, you know, they were waiting by the player's parking lot. Now we're coming down the ramp ... [cut to film of the day - like the Zabruter film - with the Umbrella man and everything - Oh so brilliant parody!!!] ... Newman was in front of me. Keith was coming toward us, as he passes Newman turns and says, &quot; Nice game pretty boy.&quot;. Keith continued past us up the ramp.

NEWMAN: A second later, something happened that changed us in a deep and profound way front that day forward.

ELAINE: What was it?

KRAMER: He spit on us.... and I screamed out, &quot;I'm hit!&quot;

NEWMAN: Then I turned and the spit ricochet of him and it hit me.

ELAINE: Wow! What a story.

JERRY: Unfortunately the immutable laws of physics contradict the whole premise of your account. Allow me to reconstruct this if I may for Miss Benes as I've heard this story a number of times.

JERRY: Newman, Kramer, if you'll indulge me. According to your story Keith passes you and starts walking up the ramp then you say you were struck on the right temple. The spit then proceeds to ricochet off the temple striking Newman between the third and forth rib. The spit then cam off the rib turned and hit Newman in the right wrist causing him to drop his baseball cap. The spit then splashed off the wrist, Pauses In mid air mind you- makes a left turn and lands on Newman's left thigh. That is one magic luggie.

[THE BRILLIANCE OF THIS SCENE IS THAT IT IS AN EXACT PARODY OF KEVIN COSTNER'S COURTROOM; SCENE IN

THE FILM JFK - AND WAYNE KNIGHT PLAYED THE SAME POSITION IN BOTH!!!]

NEWMAN: Well that's the way it happened.

JERRY: What happened to your head when you got hit?

KRAMER: Well. uh, well my head went back and to the left

JERRY: Again

KRAMER: Back and to the left

JERRY: Back and to the left Back and to the left

ELAINE: So, what are you saying?

JERRY: I am saying that the spit could not have come from behind ... that there had to have been a second spitter behind the bushes on the gravelly road. If the spitter was behind you as you claimed that would have caused your head to pitch forward.

ELAINE: So the spit could have only come from the front and to the right.

JERRY: But that is not what they would have you believe.

NEWMAN: I'm leavin'. Jerry's a nut. (Exits)

KRAMER: Wait, wait, (Exits)

JERRY: The sad thing is we may never know the real truth.

(George runs in)

GEORGE: (Frantically) Did anybody call here asking for Vandelay industries?

JERRY: No. What happened to you?

GEORGE: Now, listen closely. I was at the unemployment office and I told them that I was very close to getting a job with Vandelay Industries and I gave them your phone number. So, when now when the phone rings you've got to answer &quot;Vandelay Industries&quot;.

JERRY: I'm Vandelay Industries?

GEORGE: Right.

JERRY: And what is that?

GEORGE: You're in latex

JERRY: Latex? And what do I do with latex?

GEORGE: Ya manufacture it.

ELAINE: Here in this little apartment?

JERRY: And what do I say about you?

GEORGE: You're considering hiring me for your latex salesman.

JERRY: I'm going to hire you as my latex salesman?

GEORGE: Right.

JERRY: I don't think so. Why would I do that?

GEORGE: Because I asked you to.

JERRY: If you think I'm looking for someone to just sit at a desk pushing papers around, you can forget it. I have enough headaches just trying to manufacture the stuff.

(Buzzer)

JERRY: Yeah.

KRAMER: It's Keith.

JERRY: All right we're coming down.

GEORGE: KEITH HERNANDEZ:?

JERRY: Yeah, come on Elaine, lets go.

GEORGE: Where are you goin?

ELAINE: He's giving me a ride You know there had to have been a second spitter. But who was it? Who had the motive?

JERRY: That's what I've been trying to figure out the past five years.

GEORGE: What the hell are you two talking about? (All exit)

[Keith's car]

JERRY: Well that was really fun, thanks.

KEITH: Yeah, it really was.

JERRY(MIND): Should I shake his hand?

JERRY: Well, ...

KEITH: UH, do you want to catch a movie this weekend? Have you seen JFK?

JERRY: No, I haven't.

JERRY(MIND): This weekend. WOW!

JERRY: Sure, that would be great.

JERRY(MIND): Damn, I was too overanxious, he must have noticed that.

JERRY: I mean, ... if you want to.

KEITH: Well, how about this Friday?

JERRY: Yeah, Friday's okay.

JERRY(MIND): Go ahead shake his hand. You're Jerry Seinfeld. You've been on the Tonight Show.

JERRY: Well, good night [holds hand out and shakes hand]

KEITH: Goodnight. Oh, Jer, by the way, the woman we gave a ride to earlier tonight,

JERRY: Elaine?

KEITH: Yeah. What's her story?

JERRY: Uh, I don't know, we used to go out.

KEITH: Would you mind if I gave her a call?

JERRY: For a date?

KEITH: Yeah.

JERRY: Oh, no, uh, go ahead. You got a pen?

KEITH: You sure you don't mind?

JERRY: .... (silence)

[Jerry's Apartment]

JERRY: So then we went to dinner.

GEORGE: Who paid?

JERRY: We split it.

GEORGE: Split it. Pretty good. Talk about game six?

JERRY: Naw, I gotta wait until its just the right time.

(Buzzer)

JERRY: Yeah

ELAINE: It's Elaine.

JERRY: Come on up.

GEORGE: So then what?

JERRY: Uh, nothin'. Then he took me home.

GEORGE: Shake his hand?

JERRY: (smiling) Yeah

GEORGE: What kind of a shake does he have?

JERRY: Good shake. Perfect shake. Single pump, not too hard, you know, doesn't have to prove anything, but, you know, firm enough to know he was there.

GEORGE: So, uh, you gonna see him again?

JERRY: He asked me if I was doing anything Friday night.

GEORGE: Wow! The weekend.

JERRY: So then as I was getting out of the car, ...

ELAINE: HI

JERRY: Hi Elaine.

ELAINE: Sooo, how was your date?

JERRY: What date? It's a GUY.

ELAINE: So you know , ... he called me.

JERRY: Already?

GEORGE: Keith called you?

(Elaine nods)

GEORGE: He he This guy really gets around.

ELAINE: Do you mind?

JERRY: I don't mind at all. Why should I mind? What did he say?

ELAINE: He asked me out for Saturday night.

JERRY: Oh, ya' going?

ELAINE: I told him I was busy.

JERRY: Ah, really.

ELAINE: So, we're going out Friday.

(long pause)

JERRY: Friday?

ELAINE: yeah.

JERRY: He's going' out with you on Friday?

ELAINE: Yeah.

JERRY: He's supposed to see ME on Friday.

ELAINE: Oh, uh, I didn't know.

JERRY: We made plans.

ELAINE: Well, uh, I'll cancel it.

JERRY: No, don't cancel it.

ELAINE: Huh. Well this is a little awkward, isn't it/

JERRY: Well, frankly it is.

ELAINE: I've never seen you jealous before.

JERRY: Well you're not even a fan. I was at game six - you didn't even watch it.

ELAINE: Wait a second wait a minute, You jealous of him or you jealous of me?

(long pause)

JERRY: Any Hennigans around here?

(Phone rings)

JERRY: Vandelay Industries, Kel Varnsen speaking. May we help you? ... Oh Hi Keith. Na, I was just jokin' around

JERRY: No. No. I don't mind at all.

ELAINE: (quietly) No, no, no, I can cancel.

JERRY: Sure, we can do something next week.

ELAINE: (quietly) I can cancel.

JERRY: No, its no problem at all.

ELAINE: (quietly) I,...

JERRY: Okay, take it easy. (hangs up) That was Keith. we're going to do something next week.

(Kramer enters)

KRAMER: Hey

JERRY: Hey what are you doing Friday night?

JERRY: Friday night? Nothin', ... now.

KRAMER: Okay, wanna come with me and see the baby?

JERRY: Fasten your seat belts. we're goin' to see the baby.

KRAMER: Come on, if you don't see the baby now you're never gonna see it

JERRY: All right, I'll go

KRAMER: All right

(Jerry exits with garbage)

(Kramer sits beside Elaine - awkward moment)

(phone rings)

KRAMER: Yallo. What delay industries?

ELAINE: no no , ..

GEORGE: (from bathroom) VANDELAY, SAY VANDELAY!

KRAMER: Na, you're way way way off.. Well, yeah that's the right number but this is an apartment

GEORGE: (from bathroom) VANDELAY, SAY VANDEL... (George falls) ... Vandelay Industries, ...

KRAMER: no problem, ... no problem. [Hangs up] ... How did YOU know who that was?

(Jerry enters - sees George on the floor)

JERRY: And you want to be my latex salesman.

(Notice magazine is on wrong side on the floor)

The End Part 1

[New York State department of Labor]

MRS. SOKOL: Just sign here please.

GEORGE: I know who it was too. It was the guy who interviewed me. He was very threatened by me. Why else wouldn't he hire me? I could sell latex like that (snaps fingers).

MRS. SOKOL: Sign that.

GEORGE: Who is this? (sees photo)

MRS. SOKOL: It's my daughta'

GEORGE: THIS is your daughter? My God! My God! I I hope you don't mind my saying. She is breathtaking.

MRS. SOKOL: Ya' think so?

GEORGE: Ah, would you take this picture away from me. Take it away and get it outta here. Let me just sign this and go.

MRS. SOKOL: You know she doesn't even have a boyfriend.

GEORGE: Okay, Okay. Who do you think you're talking to? What are ya'... you trying to make a joke, because it's not funny. I can tell you that.

MRS. SOKOL: I'm serious.

GEORGE: It's one think to not give me the extension But to tease and to torture me like this. There's no call for that.

MRS. SOKOL: Would you like her phone numba'?

MRS. SOKOL: Mrs. Sokol I, I don't know what to say. I, uh, where should I sign this thing?

MRS. SOKOL: No no no, Don't worry about it.

[Fitzpatrick's Bar]

ELAINE: So tell me more about this game SIX.

KEITH: Well, there was two outs, bottom of the tenth, we're one out away from losing the series.

ELAINE: ooooh ahhh

[Mike and Carol's Apartment]

KRAMER: (to baby) Koochie koochie koochie koo

JERRY: (to baby) Hello. How are you/

CAROL: So, wadda ya' think? Do you love her?

JERRY: Yes. I do love her. (to baby) You have a very nice place here.

CAROL: So how do you think she looks like?

KRAMER: Lyndon Johnson.

CAROL: What? Lyndon Johnson?

JERRY: He's joking.

KRAMER: I'm not joking. She looks like Lyndon Johnson.

CAROL: Jerry, I can't believe it took you so long to come see the baby. I kept saying to Michael, &quot;When is Jerry going to see the baby?&quot;

JERRY: I was saying the same thing.

CAROL: Let's take a picture. Michael, get the camera.

JERRY: Uh, you don't have to take a picture.

MIKE: I don't know where it is.

CAROL: It's in the bottom draw' of are dressa'. Hurry up! ... He's such an idiot.

JERRY: Jerry, You want to pick her up?

JERRY: I better not.

KRAMER: I'll pick her up.

(The Baby cries)

[George's car]

CARRIE:Thank you for a wonderful time George.

GEORGE: Glad you enjoyed it.

CARRIE:I haven't had a Big Mac in a long time.

GEORGE: ... millions and millions ...

CARRIE:Would you like to come up?

GEORGE: [pause] Would I like to come up? I would love to come up. I, I'm fighting not to. Fighting! Unfortunately I uh have to get an early start tomorrow. Gotta' get up and hit that pavement

CARRIE:But it's Saturday. all the offices are closed.

GEORGE: I got me an appointment with a hardware store. I'm not saying I want to do it for the rest of my life, but, uh, hardware fascinates me. Don't you love to make a key?

CARRIE:Will you call me as soon as you get home?

GEORGE: [pause] Tonight?

CARRIE:Yes.

GEORGE: Will I call you when I get home? ha ha What do you think? ee, you kill me kill me

CARRIE:Well. good night.[puckers up]

[Mike and Carol's Apartment]

KRAMER: Well it was an accident. Right Jerry it was an accident. Ah, she's going to be all right. .. baby, baby, ah, baby.

[Keith's car]

ELAINE: Well, thanks for a nice evening. It was really fun.

KEITH: Yeah, it was. [mind] Gosh, should I kiss her good night?

ELAINE: [mind] Is he going to try to kiss me?

ELAINE: I love Cajun cooking.

KEITH: Really, you know my mom's one quarter Cajun.

ELAINE: Uh, my father's half drunk. ha ha ha ha

KEITH: Maybe they should get together. [mind] Go ahead. Kiss her. I'm a baseball player dammit.

ELAINE: [mind] What's he waiting for? I thought he was a cool guy.

KEITH: [mind] Come on I won the MVP in 79. I can do whatever I want to.

ELAINE: [mind] This is getting awkward.

KEITH: Well, goodnight

ELAINE: Good night

(they kiss - REALLY KISS)

ELAINE: [mind] Who does this guy think he is?

KEITH: [mind] I'm Keith Hernandez.

[Jerry's Apartment]

ELAINE: Uh, who else? ... Mookie. Mookie was there. Do you know him?

JERRY: I don't know him. I know who he is.

ELAINE: Hum, he's such a great guy. You should meet him. You know he's the one who got that hit ...

JERRY: I know. He got the hit in game SIX. So, so then what happened?

ELAINE: Nuthin'. Then he took me home.

JERRY: So, did you two, uh, ... have uh, ...

ELAINE: What?!

JERRY: ... You know

ELAINE: Milk?

JERRY: No!

ELAINE: Cookies?

JERRY: Did he kiss you good night?

ELAINE: I dunno.

JERRY: What do you mean you don't know?

ELAINE: All right. He kissed me. Okay?

JERRY: Well, what kind of a kiss? Was it a peck? Was it a kiss? Was it a long make out thing?

ELAINE: Between a peck and a make out.

JERRY: So, you like him.

ELAINE: I don't understand. Before you were jealous of me. Now you're jealous of him?

JERRY: Ah, I'm jealous of everybody.

(phone rings)

JERRY: hello. Oh, hi. What's happening? ... what? oh um, sure, ... um, yeah, okay, uh. I'll see you then. Yeah, yeah, Bye.

ELAINE: Who was that?

JERRY: That was Keith.

ELAINE: What's going on?

JERRY: He wants me to help him move.

ELAINE: Help him move? Move what?

JERRY: You know, furniture.

ELAINE: So, what did you say?

JERRY: I said yes, but I don't feel right about it. I mean I hardly know the guy. That's a big step oin a relationship. The biggest. That's like going all the way.

ELAINE: And you feel you're not really ready for,...

JERRY: Well we went out one time. Don't you think that's coming on a little too strong?

[Kramer enters]

KRAMER: What's going on?

JERRY: Keith Hernandez just asked me to help him move.

KRAMER: What? Well, you hardly know the guy.... What a nerve. You see wasn't I right about this guy? Didn't I tell you? Now, you're not going to do it are you?

JERRY: ... I said yes.

KRAMER: YOU SAID YES!? Don't you have any pride or self respect? I mean, how can you prostitute yourself like this? I mean what are you going to do? You're going to start driving him to the airport?

JERRY: I'm NOT DRIVING HIM TO THE AIRPORT! ..

KRAMER: yeah yeah

JERRY: hey Kramer do me a favour .

KRAMER: What?

JERRY: Don't mention it to anybody.

KRAMER: I wish you never mentioned it to ME. [exits]

[George's car]

GEORGE: I had a great time tonight Carrie. And I am going to call you as soon as I get home.

CARRIE: Don't botha

GEORGE: Bother, wa', what kind of bother?

CARRIE: I would prefa' it if ya' didn'.

GEORGE: Why? Is there anything wrong?

CARRIE: It's over buddy. Done. Finished. So long. Good bye. Adios. Sayanara.

GEORGE: Why?

CARRIE: I bin thinkin about it. You got no job. You got no prospects. You're like Biff Loman.

GEORGE: I went to the hardware store interview.

CARRIE: You think I'm going to spend my life with somebody because he can get me a deal on a box of nails?

GEORGE: I thought were a team.

CARRIE: If I ever need a drill bit I'll call you. (exits car)

GEORGE: Carrie, could you do me a favour? Could you not mention this to your mother?

[Fitzpatrick's Bar]

KRAMER: Ya know I hate to brag but, uh, I did win eleven straight golden gloves.

ELAINE: (chuckles)

KRAMER: I wouldn't have brought it up but since you mentioned it.

ELAINE: Ha, I didn't mention it.

KRAMER: Well I won them anyway.

ELAINE: Well so what. I mean you played first base. I mean they always put the worst player on first base. That's were they put me and I stunk.

KRAMER: Elaine. you don't know the first thing about first base.

ELAINE: ha ha well I know something about getting to first base. And I know you'll never be there.

KRAMER: The way I figure it I've already been there and I plan on rounding second tonight at around eleven o'clock.

ELAINE: Well, uh, I'd watch the third base coach if I were you 'cause I don't think he's waving you in. You know I hate to say this but I think we're really hitting it off. Get it? Get it?

KRAMER: Funny.

[Keith takes out a cigarette]

ELAINE: What are you doing?

KRAMER: What's that?

ELAINE: You smoke?

KRAMER: Yeah.

ELAINE: I didn't know you SMOKED.

KRAMER: Is that a problem?

ELAINE: Uh, ...

[Monks]

JERRY: She likes him I mean she really likes him.

GEORGE: How do you kn ow?

JERRY: Who wouldn't like him? I like him. And I'm a guy.

GEORGE: I suppose he's an attractive man, I , ...

JERRY: Forget that. He's a ball player. MVP&lt; 1979. I'm making wise cracks in some night club. This guy was in game six. They're a perfect match. They like go together. They're like one of these brother and sister couples that look alike.

GEORGE: Hate those couples. I could never bee one of those couples. There are no bald woman around. You know?

JERRY: You know I know this sounds a little arrogant but I never thought she would find anyone she would like better than me. Ya know, I guess I had my chance and that's that.

GEORGE: You know what I would like to do? I would really like to have sex with a tall woman. I mean really tall. Like a like a giant Like six five.

JERRY: Really?

GEORGE: What was the tallest woman you ever slept with?

JERRY: I don't know ... six three.

GEORGE: Wow, ... god! You see this is all I think about. Sleeping with a giant. It's my life's ambition.

JERRY: So I guess it's fair to say you've set different goals for yourself than say, Thomas Edison, Magellan, these types of people.

GEORGE: Magellan? You like Magellan?

JERRY: Oh, yeah,. My favorite explorer. Around the world. Come on.

GEORGE: Who do you like?

GEORGE: I like DeSoto.

JERRY: DeSoto? What did he do?

GEORGE: Discovered the Mississippi.

JERRY: Oh. like they wouldn't have found that anyway.

GEORGE: All right, I've got to go down to the unemployment office. Wanna take a walk?

JERRY: No I can't I've got some stuff to do then I've got to meet Keith at my apartment at three. I'm helping him move.

GEORGE: What? The guy asked you to HELP HIM MOVE? Wow.

JERRY: I know isn't that something?

KRAMER: He's got money. Why doesn't he just pay a mover?

JERRY: I don't know ... he's got some valuable antiques, He's worried they'll break something.

GEORGE: The next thing you know, he'll have you driving him to the airport..

JERRY: I'M NOT DRIVING HIM TO THE AIRPORT!!

[New York State Department of lab or]

GEORGE: I gave. I gave everything I could Mrs. Sokol. but nothing was good enough for her.

MRS. SOKOL: Sign here please.

GEORGE: Ha, I don't know who she's looking for. I don't know. I'll tell you something. She's very particular, your daughter. Very particular. What is she looking for some big hot shot businessman? Well I've got my pride too. I'm not going to beg her.

MRS. SOKOL: All right just sign it. People are waiting.

GEORGE: You, uh, you like baseball? [picks up baseball from desk]

MRS. SOKOL: That was autographed by the '86 Mets. I saw every inning that year.

GEORGE: Funny, cause I happen to be very good friends with Keith Hernandez.

MRS. SOKOL: You know Keith Hernandez.

GEORGE: Know him? Would you, uh, like to meet him?

MRS. SOKOL: Oh, come on. Come on.

GEORGE: I can produce Keith Hernandez right here within the hour.

MRS. SOKOL: All right. You got ONE hour.

GEORGE: All right Mrs. S. I and my good pal Keith Hernandez will be right back.

(George slowly leave the office then takes off down the hallway)

[Same phone booth as part one]

(George sees large construction worker in booth and leaves. he hops into a cab)

GEORGE: 129 west 81st street and hurry.

(same cabby as part one; stares at him)

GEORGE: Goodbye (exits cab)

[Jerry's Apartment]

KEITH: Better bring your gloves, it's freezing out there. It shouldn't take too long. I'd say maybe, oh, four hours. Really though, Jerry, there's not that much. First we got the bedroom, we got two dressers and the bed.

JERRY: Is there a box spring?

KEITH: What's that?

JERRY: Is there a box spring?

KEITH: Yeah there's a box spring but it's attached to the headboard and we'll have to take that apart. Then we got the couch.

JERRY: Is that a sectional?

KEITH: Yeah. Twelve pieces. &lt;not clear&gt; ...coffee table.

JERRY: Is that a thick marble?

[... Jerry and Keith start to laugh and Jerry walks behind Keith so they don't make eye

contact and break up]

KEITH: Three inches thick. Got it in Italy. But the BIG problem is going to be the convertible sofa. You see when you move it it tends to open up so it's going to be real difficult getting it down the stairs.

JERRY: STAIRS??? There's no elevator?

KEITH: Nah, it's a brownstone. Three floors.

JERRY: I'm sorry I can't do this. I can't do it. I can't. It, it's too soon. I don't know you. I can't help you move. I'm sorry. I can't. I just can't.

(Kramer enters, sees Keith and does a double take)

KRAMER: Hello.

KEITH: Hello.

KRAMER: Oh, you don't remember me.

KEITH: No should I [continuity error: in fact he SHOULD from the basketball game]

KRAMER: Yeah, you should. I certainly remember you. Let me refresh your memory.

(Newman enters)

NEWMAN: June 14th, 1987. Mets Phillies. You made a big error. Cost the Mets the game. Then you're coming up the parking lot ramp.

KEITH: YOU said, &quot;Nice game, pretty boy.&quot;

KRAMER: Ah, you remember.

NEWMAN: And then you spit on us.

KEITH: Hey, I didn't spit at you.

NEWMAN: Oh, yeah, right.

KRAMER: No no no, well, then who was it?

KEITH: Well lookit, the way I remember it (back to the grainy 8mm film parody) I was walking up the ramp. I was upset about the game. That's when you called me pretty boy. It ticked me off. I started to turn around to say something and as I turned around I saw Roger McDowell behind the bushes over by that gravely road. ... Anyway he was talking to someone and they were talking to you. I tried to scream out but it was too late. It was already on its way.

JERRY: I told you!

NEWMAN: Wow, it was McDowell.

JERRY: But why? Why McDowell?

KRAMER: Well, maybe because we were sitting in the right field stands cursing at him in the bullpen all game.

NEWMAN: He must have caught a glimpse of us when I poured that beer on his head.

NEWMAN: It was McDowell.

KRAMER: Oh boy. Uh, look uh, Keith, uh, we're sorry.

NEWMAN: Yeah, I couldn't be sorrier. I uh.

KEITH: look guys, don't worry about it, I uh, Well I guess I better get going.

KRAMER: Wait, uh what are ya' doing?

KEITH: I gotta move.

KRAMER: Want any help?

KEITH: I'd love some.

KRAMER: I'd love to help you move.

NEWMAN: Me too.

KEITH: Ok guys, we gotta be careful of one thing. Some of the stuff's very fragile We're going to have to handle it like a baby.

KRAMER: No sweat.

(Kramer and Newman exit)

(phone rings)

JERRY: Hello, ... oh hi Elaine .. what's going on ... no he just left ... you broke up with him? ... ME TOO .. what happened? ... oh smoking you know you're like going out with C. Everet Coope ... me ... nah ... I couldn't go through with it ... I just didn't feel ready ... so what are you doing now? ... Oh, great idea, I'll meet you there in like thirty minutes. Okay bye.

(George frantically enters)

GEORGE: Keith, Keith Wa What happened? Where's Keith?

JERRY: You just missed him. he just left. What do you need him for?

GEORGE: (out the window) Keith, Keith, up here. Can you do me a favor? I need you to go to the unemployment office with me. I, I'm Jerry's friend ... the guy from the locker room, ... I'm the chucker. It'll take five minutes. Wait. Wait.

(sits down)

JERRY: Well Biff/ What's next?

GEORGE: I don't know.

(Tall girl enters)

TALL GIRL: Excuse me. I was walking behind you and you dropped your wallet.

[Ending Monologue]

When You're moving your whole world becomes boxes. That's all you think about is boxes. Boxes, where are there boxes? You just wander down the street going in and out of stores. Are there boxes here? Have you seen any boxes? I mean it's all you think about. You can't even talk to people because you can't concentrate. Shut up I'm looking for boxes. Just after a while you become like really into it you can smell them. You walk into a store. There's boxes here. Don't tell me you don't have boxes. Dammit, I can SMELL them. I'm like I'm obsessed. I love the smell of cardboard in the morning. You could be at a funeral. Everyone's mourning crying around, and your looking at the casket. That's a nice box Does anyone know where that guy got that box? When he's done with it do you think I could get that? it's got some nice handles on it. And that's what death is really. It's the last big move of your life. The hearse is like the van. The pale bearers are your close friends the only ones you could ask to help you with a big move like that. and the casket is that great perfect box you've been waiting for your whole life The only problem is, once you find it you're in it.

The End
