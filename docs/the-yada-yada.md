---
layout: default
title: The Yada Yada
parent: Season 8
nav_order: 19
permalink: /the-yada-yada
cat: ep
series_ep: 153
pc: 819
season: 8
episode: 19
aired: April 24, 1997
written: Peter Mehlman and Jill Franklyn
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697814/
wiki: https://en.wikipedia.org/wiki/The_Yada_Yada
---

# The Yada Yada

| Season 8 - Episode 19                      | April 24, 1997            |
|:-------------------------------------------|:--------------------------|
| Written by Peter Mehlman and Jill Franklyn | Directed by Andy Ackerman |
| Series Episode 153                         | Production Code 819       |

"The Yada Yada" is the 153rd episode of the American NBC sitcom Seinfeld. The 19th episode of the eighth season, it aired on April 24, 1997. Peter Mehlman and Jill Franklyn were nominated for an Emmy for Outstanding Writing in a Comedy Series in 1997.

## Plot

Jerry's dentist, Tim Whatley (Bryan Cranston), has just finished the process of converting to Judaism but is already making Jewish-themed jokes that make Jerry uncomfortable. Jerry goes so far as to say that he believes that Whatley only converted "for the jokes".

Kramer and Mickey Abbott double date, but can't decide which woman, Karen or Julie, is right for which one of them.

Elaine is a character reference for Beth and Arnie (Stephen Caffrey), a couple who are trying to adopt, but the story she tells during an interview destroys all hope of adoption: she mentions a time when she and the couple went to a movie, and the husband lost his temper because Elaine was talking during the movie.

George's new girlfriend Marcy likes to say "yada yada yada" to shorten her stories. He tries using this practice to avoid mentioning Susan's death but then becomes suspicious when Marcy tells him that her ex-boyfriend had visited her the night before "and yada yada yada, I'm really tired today." George later consults Jerry and Elaine, suspecting that Marcy used "blah blah" to cover up sex with her ex-boyfriend, and Elaine believes that this is possible.

> GEORGE: You don't think she'd yada yada sex?
>
> ELAINE: I've yada yada'd sex!

Later, George asks Marcy to tell him some of the things she was covering up with "yada yada," and discovers that she's a habitual shoplifter. George, however, is still reluctant to tell her how his engagement ended (since George mentioned: "Well, we were engaged to be married, uh, we bought the wedding invitations, and, uh, yada yada yada, I'm still single.")

Jerry confesses to a priest about what he thinks of Tim's conversion, saying that he's offended (not as a Jew, but as a comedian), only to get sidetracked when the priest laughs at a Catholic joke of Tim's that he repeats. However, the priest is less amused by a dentist/sadist joke that Jerry makes at the end of their conversation. George drops by Jerry's confession and tells him that they need to talk. Kramer decides on the right woman, and Mickey also decides to make his commitment; however, it becomes apparent they made a mistake when Kramer meets Karen's parents, who are revealed to be little people like Mickey. Tim hears about the dentist joke that Jerry told the priest. He takes extreme exception to it and, as a result, deliberately prolongs an uncomfortable procedure. After hearing Jerry's complaints about Tim, Kramer calls Jerry an "anti-dentite."

Elaine lobbies on behalf of Beth and Arnie, and propositions the adoption official as an inducement. Beth's marriage nonetheless fails and she accompanies Jerry to Mickey's wedding to Karen. Elaine, now dating the adoption agent, is dismayed. George shows up without Marcy, explaining that "She was getting shoes for the wedding, yada yada yada, I'll see her in 6 to 8 months." Julie runs out before the wedding begins, apparently in love with Mickey and unable to bear seeing him marry Karen. Mickey's dad, a dentist, stands up for Whatley and chastises Jerry for antagonizing him: "Tim Whatley was one of my students, and if this wasn't my son's wedding day, I'd knock your teeth out, you anti-dentite bastard." Jerry is initially comforted by Beth who is at first shown to harbor the same feelings towards dentists as Jerry, until she also reveals herself to be both racist and antisemitic, at which point Jerry dumps her and tells Elaine that she left "to get her head shaved." As Karen and Mickey walk out at the end of the ceremony, Karen says to Kramer, "I really wanted you," much to his surprise.

## Production

The episode was allowed by NBC to run longer than the usual thirty minutes, and its slightly above-average length was even boasted about in promos. An edited version airs in syndication, cutting out several small scenes and dialogues, but the full-length version is available on the Seinfeld Season 8 DVD collection and on Hulu.

### "Yada yada"

The episode is one of the most famous of the series, specifically for its focus on the phrase "yada yada". "Yadda yadda" was already a relatively common phrase before the episode aired, used notably by comedian Lenny Bruce, among others. The phrase may have originated with the 1950s "yackety-yack", 1940s vaudeville, or earlier. The Phrase can be heard in the background of After the Fire's[better source needed] 1982 version of the song, "Der Kommissar" at the 3:00 mark. The phrase was used by a secretary summing up the boring parts of a letter in the February 16, 1984 Magnum PI episode "The Return of Luther Gillis" 14 years before the Seinfeld episode, and in the May 13, 1993 Cheers episode "The Guy Can't Help It." On the May 7, 1992 The Simpsons episode Bart's Friend Falls in Love Bart says, "Well, Milhouse, tis better to have loved and lost, yada yada yada."

The phrase was used in the "She's Back" Episode of Wings, on 1/3/95, when character Sandy Cooper "yada, yadas" through the wedding ceremony to Joe Hackett.

The phrase was used in the fifth episode of Buffy the Vampire Slayer, "Never Kill a Boy on the First Date", which aired on March 31, 1997, appearing when Buffy Summers explains to Angel that she knows about a prophecy. The Seinfeld episode first aired a little less than a month later.

Before the episode aired, writer Peter Mehlman suspected that it would spawn a new Seinfeld catchphrase, but he thought it would be the phrase "anti-dentite" that would become popular. The Paley Center named "Yada Yada Yada" the No. 1 funniest phrase on "TV's 50 Funniest Phrases".

## Cultural references

Scott Adams, creator of Dilbert was influenced by the episode as broadcast on April 24, 1997, as the Dilbert comic strip 6 days later on April 30, 1997, had a direct reference to "yada yada".

Seinfeld's use of "yada yada" was referenced in the King of the Hill episode "Yankee Hanky" by Dale Gribble, who ridicules Hank Hill's discovery that he was born in New York City by using the phrase.

That Handsome Devil produced a song titled "Yada-Yada". Similarly, the episode was overtly referenced in the name of a popular Connecticut-based rock and roll outfit, The Anti-Dentites.

The episode was seemingly referenced by politician Sarah Palin in an email to The Daily Caller November 1, 2010:

> I suppose I could play their immature, unprofessional, waste-of-time game, too, by claiming these reporters and politicos are homophobe, child molesting, tax evading, anti-dentite, puppy-kicking, chain smoking porn producers…really, they are... I've seen it myself...but I'll only give you the information off-the-record, on deep, deep background; attribute these "facts" to an "anonymous source" and I'll give you more.

## Cast

#### Regulars

Jerry Seinfeld ...................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus .............. Elaine Benes  
Michael Richards .................. Cosmo Kramer

#### Recurring

Danny Woodburn .......... Mickey Abbott

#### Guests

Robert Wagner .................... Dr. Abbott  
Jill St. John ............................ Mrs. Abbott  
Debra Messing ..................... Beth  
Bryan Cranston ................... Dr. Tim Whatley  
Suzanne Cryer ..................... Marcy (The Yada Yada)  
Stephen Caffrey ................... Arnie  
Henry Woronicz .................. Father Curtis  
Monica Lacey ....................... Julie  
Ali Marsh ............................. Karen  
David Chandler ................... Brian  
Virginia Watson ................... Nun  
Walter Franks ..................... Waiter  
Jerry Maren ........................ Dad

## Script

[Jerry with George and his girlfriend Marcy at Monk's.]

GEORGE: So, Marcy, you should've seen me in the hot tub today.

MARCY: Why?

GEORGE: I was naked.

MARCY: Oh, George.

JERRY: I saw it.

MARCY: How'd he look?

JERRY: Okay. I wouldn't see it again.

MARCY: You know, a friend of mine thought she got Legionnaires' disease in the hot tub.

GEORGE: Really? What happened?

MARCY: Oh, yada yada yada, just some bad egg salad. I'll be right back. (She gets up)

JERRY: I noticed she's big on the phrase &quot;yada yada.&quot;

GEORGE: Is &quot;yada yada&quot; bad?

JERRY: No, &quot;yada yada&quot; is good. She's very succinct.

GEORGE: She is succinct.

JERRY: Yeah, it's like you're dating USA Today. (Tim the dentist enters Monk's)

GEORGE: Hey.

JERRY: Hey, George, you know Tim Whatley.

GEORGE: Yeah, dentist of the stars.

JERRY: What's up?

TIM: I'll tell you what's up. I'm a Jew.

JERRY: Excuse me?

TIM: I'm a Jew. I finished converting two days ago.

JERRY: Well... (Thinking of something to say) Welcome aboard.

TIM: Thanks.

GEORGE: Hey, where you just at the health club?

TIM: Oh, well, I didn't do much. I just sat in the sauna. You know, it was more like a Jewish workout. I'll see ya. (Jerry and George give confused looks)

[Elaine and Jerry at his place.]

JERRY: Elaine, the guy's Jewish two days, he's already making Jewish jokes.

ELAINE: So what? When someone turns twenty-one, they usually get drunk the first night.

JERRY: Booze is not a religion.

ELAINE: Tell that to my father. Anyway, guess what? Beth Lookner called me.

JERRY: Ooh. Beth Lookner, still waitin' out that marriage.

ELAINE: What are you talking about? That marriage ended six months ago. She's already remarried.

JERRY: I gotta get on that internet. I'm late on everything.

ELAINE: Anyway, Beth and her new husband Arnie have listed me a reference for an adoption agency. They're trying to get a baby. (Kramer and Mickey enter wearing the same shirt)

KRAMER: Elaine, alright, who looks better in this shirt? Me or Mickey?

MICKEY: We're double dating tonight, and if we wear the same shirt we'll look like idiots.

ELAINE: Hmm, turn around. (They turn around) Both so striking.

KRAMER: Tell me about it. We just picked up two women at the Gap.

ELAINE: How did you decide which one of you would date which girl? (They pause then look at each other)

[Marcy and George in his car.]

MARCY: So I'm on 3rd Avenue, mindin' my own business, and, yada yada yada, I get a free massage and a facial.

GEORGE: What a succinct story.

MARCY: I'm surprised you drive a Cadillac.

GEORGE: Oh, it's not mine. It's my mother's.

MARCY: Are you close with your parents?

GEORGE: Well, they gave birth to me, and, yada yada...

MARCY: Yada what?

GEORGE: Yada yada yada...

[At Mickey and Kramer's date.]

KAREN: (To Mickey, wearing the shirt Elaine looked at for them) I like your shirt.

MICKEY: Oh, thank you. It's 100% cotton, and some wool.

KRAMER: Well, you too seem to have the same taste.

JULIE: Well I like it, too.

KRAMER: Well I have the same shirt.

MICKEY: Yeah, well I'm wearin' it.

JULIE: I like your shirt too.

KAREN: Oh, so do I.

KRAMER: Oh. (Waiter approaches table)

WAITER: Anything to drink? Some wine, perhaps.

MICKEY: I like Merlot.

KAREN: I love Merlot.

JULIE: I'm crazy about Merlot.

KRAMER: I live for Merlot.

WAITER: We're out of Merlot.

[Cut to the adoption agency.]

AGENT: So you, uh, know Beth and Arnie pretty well?

ELAINE: Oh, yeah, yeah.

AGENT: Do you socialize with them often?

ELAINE: Yeah, we got out to dinner a lot. Usually Chinese, well sometimes Thai. And we go to the movies, Arnie's a real film buff.

AGENT: Oh.

ELAINE: Actually, I remember this one time, um, this is funny. Um, we went to see the movie Striptease. I don't know if you've seen... doesn't matter. Anyway, I was whispering something to Beth, and Arnie leans over to me, and he goes, &quot;Would you SHUT UP?!&quot; I mean, he barely even knew me. Where did he get ah-- But they're nice people. (Elaine tries to smile realizing her mistake)

[Jerry is in the dental chair when George enters.]

GEORGE: Oh, you're in here.

JERRY: What're you doing here?

GEORGE: I knew you had an appointment.

JERRY: Well this is very awkward.

GEORGE: I'll leave when the guy comes in. I gotta tell you, I am loving this yada yada thing. You know, I can gloss over my whole life story. (Picks up dental tool)

JERRY: Hey, you don't play with that. That's going in my mouth.

GEORGE: Hey, what this thing? Whew!

JERRY: Alright, that's enough. Now get going. Get outta here. (Tim and his staff enter)

GEORGE: Hey, Tim. Quick question. Is it normal for your teeth to make noises, like a hissing or a chirping?

JERRY: George...

TIM: Um...

GEORGE: Fine, I'll make an appointment. (He leaves)

TIM: Alright, it is cavity time. Ah, here we go. Which reminds me, did you here the one about the rabbi and the farmer's daughter? Huh?

JERRY: Hey.

TIM: Those aren't matzoh balls.

JERRY: Tim, do you think you should be making jokes like that?

TIM: Why not? I'm Jewish, remember?

JERRY: I know, but...

TIM: Jerry, it's our sense of humor that sustained us as a people for 3000 years.

JERRY: 5000.

TIM: 5000, even better. Okay, Chrissie. Give me a schtickle of fluoride.

[Jerry and Elaine at Jerry's apartment.]

JERRY: And then he asked the assistant for a schtickle of fluoride.

ELAINE: Why are you so concerned about this?

JERRY: I'll tell you why. Because I believe Whatley converted to Judaism just for the jokes. (Phone rings)

PHONE: Would you be interested in a subscription to the New York Times?

JERRY: Yes. (Slams down phone. Kramer and Mickey enter)

KRAMER: I don't believe that.

MICKEY: If you had gotten into the backseat of the car we could've figured this whole thing out.

KRAMER: Why were you holding the door open for?

MICKEY: Not for you! Who holds a door open for a man?

KRAMER: Well, I thought it was a nice gesture. But I guess I was wrong!

MICKEY: Let's just put they're names in a hat.

KRAMER: I don't even know their names! Look, why don't you just take the one that was on the left?

MICKEY: I'm not sure she was my type.

KRAMER: Oh, everybody's your type.

MICKEY: What the hell does that mean?

KRAMER: You've been married three times.

MICKEY: That's it, it's go time! (Charges toward Kramer, only to be held back by Jerry and Elaine)

JERRY: Alright, take it easy.

ELAINE: Hey, hey, hey!

KRAMER: Come on, let him go. You want throw? Let's throw!

ELAINE: Hey! Hold on a second. Alright, look, I got an idea. Why don't you just show up early for your next date, sit across from each other, and see who the girls sit next to.

MICKEY: That's not bad.

KRAMER: Alright, so we let the girls decide.

MICKEY: Yeah, why should we knock ourselves out?

KRAMER: Yeah, I wanna wear that shirt next time.

MICKEY: No, no one wears the shirt next time.

KRAMER: That's right, 'cause they already saw it.

MICKEY: We'll look like idiots. (They exit)

[George and Marcy at Monk's.]

GEORGE: Well, we were engaged to be married, uh, we bought the wedding invitations, and, uh, yada yada yada, I'm still single.

MARCY: So what's she doing now?

GEORGE: Yada.

MARCY: Speaking of ex's, my old boyfriend came over late last night, and, yada yada yada, anyway. I'm really tired today.

[Beth and Arnie see Elaine walking down the street.]

ELAINE: Beth, Arnie, hi. What's up?

ARNIE: Well our adoption application was denied.

ELAINE: Really.

BETH: The adoption agent seems to feel that Arnie has a violent temper.

ELAINE: Oh.

BETH: So we're just asking our friends what they may have said to the adoption agent.

ELAINE: Uh, you know, I just told them what kind people you are and, uh, yada yada yada, that is it.

[Cut to the dentist waiting room.]

JERRY: How you doing?

FATHER: I have a discomfort in my molar. (Enter Tim)

TIM: Well, the Curtis, why don't you come in? (To Jerry) Father Curtis, good guy. Oh, which reminds me, did you hear the one about the Pope and Raquel Welch on the lifeboat, huh? I'll tell you later. (Exits)

JERRY: Whatley. (Like &quot;Newman&quot;)

[At Kramer and Mickey's date, they enter to find the girls already at the table.]

KRAMER: What are they doing here?

MICKEY: I told you we should've gotten here a half hour early.

KRAMER: Alright, alright. Now what're we gonna do?

MICKEY: Alright, don't panic. Let's just decide now. Which one do you want?

KRAMER: Alright, I'll take Julie.

MICKEY: I knew you wanted her. That's who I wanted.

KRAMER: Alright, I'll take Karen.

MICKEY: No, no, you think I'm fallin' for that? I'll take Karen.

KRAMER: Alright, which one is Julie? (They walk over to the table and fight over who sits where) Hey, you ladies look lovely tonight. (Continue to struggle)

[Jerry's.]

JERRY: So Whatley said to me, &quot;Hey, I can make Catholic jokes, I used to be Catholic.&quot;

ELAINE: You see, I don't think it is a Catholic joke. I think it's more of a Raquel Welch joke. What was it? No, I said hand me the buoys. (Laughing) Buoys!

JERRY: Don't you see what Whatley is after? Total joke telling immunity. He's already got the two big religions covered, if he ever gets Polish citizenship there'll be no stopping him.

ELAINE: So what're you gonna do?

JERRY: I think this Father Curtis might be very interested to hear what Whatley has the Pope doing with Raquel Welch.

ELAINE: (Calling on phone) Hey, Beth, Arnie, it's Elaine. Um, thought you guys might wanna have lunch. Gimme a call. Bye.

JERRY: They're not getting a baby so you're taking them out to lunch?

ELAINE: Thought it would be nice.

JERRY: Poor Beth.

ELAINE: Hey, Arnie's just as upset.

JERRY: Oh screw him! (George enters)

GEORGE: Listen to this. Marcy comes up and she tells me her ex-boyfriend was over late last night, and &quot;yada yada yada, I'm really tired today.&quot; You don't think she yada yada'd sex.

ELAINE: (Raising hand) I've yada yada'd sex.

GEORGE: Really?

ELAINE: Yeah. I met this lawyer, we went out to dinner, I had the lobster bisque, we went back to my place, yada yada yada, I never heard from him again.

JERRY: But you yada yada'd over the best part.

ELAINE: No, I mentioned the bisque.

GEORGE: Well, I gotta do somethin'. (Walks over to bathroom. Kramer enters)

KRAMER: Well, I gotta do somethin'.

JERRY: George is already in there.

KRAMER: (Confused) No, Mickey and I, we can't work it out. You know, I'm thinking of asking that Karen out by myself.

JERRY: I thought you were leaning towards Julie.

KRAMER: I was, but the one I thought was Julie turned out to be Karen.

[George and Marcy at Monk's.]

GEORGE: Well it was a helluva yada yada.

MARCY: He's moving to Seattle. We wanted to say goodbye, I was just getting out of the shower, and yada yada yada--

GEORGE: Alright, enough! Enough! From now on, no more yada yada's. Just give me the full story.

MARCY: Okay.

GEORGE: Tell me about the free facial.

MARCY: Okay, well, like I said I was on 3rd Avenue, and I stopped by a large department store.

GEORGE: Which one?

MARCY: Bloomingdale's.

GEORGE: Very good. Go on.

MARCY: Oh, and I stole a Piaget watch.

GEORGE: What's that?

MARCY: And then, I was on such a... high, that I went upstairs to the salon on the fifth floor, and got a massage and facial, and skipped out on the bill.

GEORGE: Shoplifting.

MARCY: Well, what about you? You told me that you were engaged. What was the rest of that? (Pause)

[Jerry at the church approaches a nun.]

JERRY: Excuse me, Mother?

NUN: Sister.

JERRY: Sister, right. Do you know when Father Curtis has office hours?

NUN: Well not until tomorrow.

JERRY: Hmm, I really need to speak with him.

(Jerry enters confessional, sits down on kneeler. Father Curtis opens sliding door.)

FATHER: That's a kneeler.

JERRY: Oh. (Adjusts accordingly)

FATHER: Tell me your sins, my son.

JERRY: Well I should tell you that I'm Jewish.

FATHER: That's no sin.

JERRY: Oh good. Anyway, I wanted to talk to you about Dr. Whatley. I have a suspicion that he's converted to Judaism just for the jokes.

FATHER: And this offends you as a Jewish person.

JERRY: No, it offends me as a comedian. And it'll interest you that he's also telling Catholic jokes.

FATHER: Well.

JERRY: And they're old jokes. I mean, the Pope and Raquel Welch in a lifeboat.

FATHER: I haven't heard that one.

JERRY: Oh, I'm sure you have. They're out on the ocean and, yada yada yada, and she says, &quot;Those aren't buoys.&quot; (Father starts laughing) Father...

FATHER: One second... Well, if it would make you feel better I could speak to Dr. Whatley. I have to go back and have a wisdom teeth removed.

JERRY: You know the difference between a dentist and a sadist don't you?

FATHER: Um...

JERRY: Newer magazines.

FATHER: Now if you'll excuse me. (Closes door. George enters confessional.)

GEORGE: Jerry, I gotta talk to you.

[Kramer knocks on Karen's apartment door, she opens the door.]

KRAMER: Hi.

KAREN: Hi, Kramer.

KRAMER: Got a minute?

KAREN: Uh, actually my parents are over, but, would you like to meet them?

KRAMER: Sure.

KAREN: (Parents enter and are little people like Mickey) Mom, Dad.

KRAMER: Hi.

[At Monk's, Arnie is talking to Elaine.]

ARNIE: Elaine, I have to ask you something. What exactly happened down there?

ELAINE: Well, I don't know. I mean, I talked to him and, blah blah blah, he asked about you guys and, da da da da da, more questions, bleh bleh bleh...

ARNIE: Alright, shut up!

ELAINE: Again you are telling me to shut up?

ARNIE: What?

ELAINE: You yelled that time at the movies. That's why you're not getting the baby.

ARNIE: Oh my God. How am I gonna tell Beth?

ELAINE: Look, I'll go down and talk to this adoption guy and I'll make sure that it all gets worked out.

ARNIE: Alright, just don't screw it up this time! (He exits)

ELAINE: See, again with the yelling. Not a fan of the yelling.

[At the dentist, Tim is working on Jerry.]

JERRY: (In pain) Oh, are you about done?

TIM: I'm just getting warmed up. Because I'm just a sadist with newer magazines.

JERRY: Huh?

TIM: Father Curtis told me about your little joke.

JERRY: What about all your Jewish jokes?

TIM: I'm Jewish, you're not a dentist. You have no idea what my people have been through.

JERRY: The Jews?

TIM: No, the dentists. You know, we have the highest suicide rate of any profession?

JERRY: Is that why it's so hard to get an appointment?

[Jerry enters his apartment to see Kramer talking on his phone.]

KRAMER: So, I'll uh... alright. (Hangs up)

JERRY: Date with Karen?

KRAMER: No, Julie. She's the one.

JERRY: What happened to Karen?

KRAMER: Well, Mickey and her have a lot more in common. you know her parents are little people?

JERRY: Oh, small world. So little people can have not little people children?

KRAMER: Oh yeah, and vice versa. Mother Nature's a mad scientist, Jerry.

JERRY: So you won't believe what happened with Whatley today. It got back to him that I made this little dentist joke and he got all offended. Those people can be so touchy.

KRAMER: Those people, listen to yourself.

JERRY: What?

KRAMER: You think that dentists are so different from me and you? They came to this country just like everybody else, in search of a dream.

JERRY: Kramer, he's just a dentist.

KRAMER: Yeah, and you're an anti-dentite.

JERRY: I am not an anti-dentite!

KRAMER: You're a rabid anti-dentite! Oh, it starts with a few jokes and some slurs. &quot;Hey, denty!&quot; Next thing you know you're saying they should have their own schools.

JERRY: They do have their own schools!

KRAMER: Yeah!

[At the adoption agency]

ELAINE: One little baby, whatever you have in stock.

AENT: Miss Benes...

ELAINE: Look it, look it, Ryan. These people are gettin' a baby. Period. Now we can do this the easy way, or we can do this the fun way.

[Beth enters Jerry's place.]

BETH: Jerry, I'm sorry to bother you, but you always said you'd be there for me.

JERRY: Well, what's wrong?

BETH: I'm thinking of leaving Arnie.

JERRY: Talk to me.

BETH: He met with Elaine, and I asked him what happened, and he yada yada'd me. I mean, could he be having an affair?

JERRY: Well, I wouldn't put anything past anybody.

BETH: But we just got married.

JERRY: Well obviously that was a mistake. You need to forget about Arnie. The important thing is you're moving on.

BETH: Why would Elaine do that to me?

JERRY: Forget about Elaine. Let's just focus on us. Come on, big hug. (Mickey and Karen enter)

MICKEY: Hey Jerry. Where's Kramer? I've got exciting news.

JERRY: I'm kinda in the middle of something.

MICKEY: Karen and I are getting married.

JERRY: Oh, congratulations. Her marriage just fell apart.

MICKEY: (To Beth) How many is that for you?

BETH: Two.

MICKEY: You're a lightweight. Come on, honey.

[One week later at the wedding. Elaine is sitting with adoption agent. Jerry enters with Beth.]

ELAINE: Hey Jerry. What are you doing here with Beth?

JERRY: Beth and Arnie broke up.

ELAINE: So they don't want a baby?

JERRY: (Shaking his head no) Pff.

ELAINE: (Realizes her mistake) I think I'm gonna be sick. (George enters alone)

JERRY: Hey, where's Marcy?

GEORGE: She, uh, went shopping for some shoes for the wedding and, yada yada yada, I'll see her in six to eight months. (Kramer and Julie enter)

JERRY: Hey, Kramer, over here.

KRAMER: I just assume not sit next to you.

JERRY: Kramer... Oh look, there's Mickey and his parents.

ELAINE: Nice looking family.

JERRY: Very handsome.

KRAMER: (To Mr. Abbott) How ya doing?

MR. ABBOTT: Hey Kramer.

JULIE: Oh Mickey. Excuse me, I can't take this. (She exits quickly)

JERRY: Hi, Mr. Abbott.

MR. ABBOTT: That's Dr. Abbott, D.D.S. Tim Whatley was one of my students. And if this wasn't my son's wedding day, I'd knock your teeth out you anti-dentite bastard.

BETH: What was that all about?

JERRY: Oh, I said something about dentists and it got blown all out of proportion.

BETH: Hey, what do you call a doctor who fails out of med school?

JERRY: What?

BETH: A dentist. (They laugh)

JERRY: That's a good one. Dentists.

BETH: Yeah, who needs 'em? Not to mention the Blacks and the Jews. (Jerry fakes a smile)

(A little later)

ELAINE: Where's Beth?

JERRY: She went out to get her head shaved.

FATHER: We are gathered her today to unite this couple in holy... matrimony.

JERRY: Those wisdom teeth are tough to get out.

FATHER: Marriage is not an intervention to be entered lightly... Yada yada yada, I pronounce you man and wife. (They kiss, walk toward exit)

KAREN: (To Kramer) I really wanted you.

The End
