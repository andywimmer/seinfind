---
layout: default
title: The Strike
parent: Season 9
nav_order: 10
permalink: /the-strike
cat: ep
series_ep: 166
pc: 910
season: 9
episode: 10
aired: December 18, 1997
written: Dan O'Keefe and Alec Berg & Jeff Schaffer
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697790
wiki: https://en.wikipedia.org/wiki/The_Strike_(Seinfeld)
---

# The Strike

| Season 9 - Episode 10                                | December 18, 1997         |
|:-----------------------------------------------------|:--------------------------|
| Written by Dan O'Keefe and Alec Berg & Jeff Schaffer | Directed by Andy Ackerman |
| Series Episode 166                                   | Production Code 910       |

"The Strike" is the 166th episode of the NBC sitcom Seinfeld. This was the tenth episode of the ninth and final season. It aired on December 18, 1997. This episode featured and popularized the holiday of Festivus.

This episode also popularized the concept of a "two-face": someone who looks attractive sometimes and looks bad at other times, depending on exterior conditions, such as lighting. It also explained why Kramer never held a job throughout the show. The episode is also notable for featuring an appearance by actor/playwright Tracy Letts, who would win the 2008 Pulitzer Prize for Drama, for his play August: Osage County. TV Guide ranked this number three on its 'Top 10 Holiday Episodes' list.

## Plot

George, Elaine, and Jerry attend Tim Whatley's Hanukkah party, and Elaine meets a man whom she dubs "Denim Vest" (based on his wardrobe) and gives him a fake phone number after he asks her out. Later at the coffee shop, George opens his mail and is offended to receive a gift of a donation in his name from Whatley, and also gets upset when Elaine reads a card from George's father wishing him a "Happy Festivus", referring to a holiday that his father invented.

Kramer gets a call that a 12-year strike at his former job at H&H Bagels ended, so he decides to return to work. Meanwhile, Elaine realizes that she lost her card for a free submarine sandwich after giving it to "Denim Vest" with the fake phone number written on it. Determined to get her free sandwich, she goes to the off-track betting parlor, whose phone number is the fake number she gave to "Denim Vest", hoping he will call the betting parlor and she can get her sandwich card back. The two men at the betting parlor begin flirting with Elaine and ask for her phone number, prompting her to give the number for H&H Bagels. She goes to the bagel shop and waits to receive a call there.

After learning about Festivus from George and Jerry, Kramer becomes fascinated with the concept, and meets up with George's father, Frank, to learn about the holiday. Kramer requests to have off of work to celebrate Festivus and is denied, so he goes back on strike, picketing outside the store. While on strike, Kramer sabotages the bagel machine, causing a steam vent to burst inside the store, which causes Elaine to appear unattractive. She goes to meet "Denim Vest" for her sandwich card, and he does not have it on him, but says he can give it to her another time. Being turned off by Elaine's appearance, he gives her a fake phone number.

While giving out Christmas gifts at work, George hands out cards for donations made to "The Human Fund", a fake charity George created to get out of buying gifts for his co-workers. George's boss, Mr. Kruger, decides to give a large company donation to The Human Fund, only later to find out that the charity does not exist. When confronted by Mr. Kruger as to why George gave him a "fake Christmas gift", George hesitantly replies that he does not celebrate Christmas, but instead celebrates Festivus, and claims he gave out the fake cards to avoid being "persecuted for [his] beliefs". To prove to Mr. Kruger that Festivus is real, George invites him to a Festivus dinner at his parents' house.

Jerry goes on a date with Gwen (Karen Fineman), a girl whom he met at Whatley's party, and realizes she is much less attractive than when he first met her because of the different lighting. When Kramer first meets Gwen, he finds her rather unattractive, and later does not recognize her when picketing outside the bagel store, claiming that she is more attractive than Jerry's actual girlfriend, which leads Gwen to think Jerry is cheating on her.

Jerry, Elaine, George, and Mr. Kruger attend the Festivus dinner at George's parents' house. Kramer walks in with the two men from the betting parlor after they called H&H Bagels asking for Elaine. Gwen shows up at the dinner after Kramer tells her Jerry was there, and sees Elaine, whom she thinks is Jerry's "ugly" girlfriend that he's cheating on her with, and she storms out of the house. Kramer leaves to work a double shift at H&H Bagels after he ended his strike to use the bathroom. The Festivus dinner continues with the traditional "feats of strength" where Frank forces George to fight him as George refuses. Frank declares it "the best Festivus ever".

During the credits, Kramer is making a fresh batch of bagels, but he gets his chewing gum stuck in the dough. The manager sees this and, finally deciding he's had enough, fires Kramer, who cannot be happier.

## Continuity

At the Festivus dinner, Kruger refers to Kramer as "Dr. Van Nostrand", a callback to "The Slicer". In that episode, Kramer pretends to be a dermatologist and screens Kruger for cancer. Kramer also used the name Dr. Van Nostrand in the episode "The Package" while pretending to be Elaine's doctor "from the Clinic". Kramer used the stage name "Martin Van Nostrand" when attempting unsuccessfully to audition for the part of 'Kramer' in part I of the two-part episode "The Pilot" and pretended to be Peter Van Nostrand, a professor of English literature who contended that Shakespeare was an impostor, in "The Nose Job".

## The Human Fund

Due to his unconscionable cheapness, George Costanza found yet another way to save money after receiving a gift donation certificate from Tim Whatley. Instead of exchanging Christmas gifts (per normal custom) with his co-workers, George gave cards stating that a donation had been made in his co-workers' names to a charity called "The Human Fund" (with the slogan "Money For People"). This organization was strictly an invention of George's that did not in fact exist. The Human Fund was conceived by writer Jeff Schafer and based on Christmas cards the Seinfeld staff would receive from Castle Rock.

The Human Fund is also the name of a real organization based in Cleveland, Ohio established in 2005, eight years after the episode's airing. This organization reportedly drew its name from the episode.

In the Lost episode "Everybody Loves Hugo," after being presented with an award for his support of the Golden State Natural History Museum, Hugo Reyes (AKA Hurley) says to his mother that they have an event with the Human Fund next Saturday night.

## Cast

#### Regulars

Jerry Seinfeld .............................. Jerry Seinfeld  
Jason Alexander ......................... George Costanza  
Julia Louis-Dreyfus .................... Elaine Benes  
Michael Richards ........................ Cosmo Kramer

#### Recurring

Jerry Stiller .......................... Frank Costanza  
Estelle Harris ....................... Estelle Costanza  
Bryan Cranston .................. Dr. Tim Whatley

#### Guests

Daniel Von Bargen ................... Kruger  
Karen Fineman ......................... Gwen  
Dave Florek ............................. Harry  
Kevin Hamilton MacDonald ..... Denim Vest  
Tracy Letts .............................. Counterguy  
Amit Itelman ............................ Employee  
Stacey Herring ......................... Sandy  
Colin Malone ............................ Sleazy Guy  
Jerry Dixon .............................. Customer

## Script

[Setting: Tim Whatley's apartment]

ELAINE: So... Whatley's still Jewish, huh?

JERRY: Oh, sure. With out the parents, it's a breeze.

(Elaine laughs, Whatley enters)

TIM: Hey! Happy Chanukah!

JERRY: Hey, Tim. Great party.

(Tim holds up a mistletoe)

TIM: (Suggesting a kiss to Elaine) eh?

ELAINE: (Shrugging it off) eh.

TIM: (Accepting) Oh. (Turns to George) Hey, George, thanks again for getting me those Yankee tickets.

GEORGE: Oh, yeah. Still in good with the ground crew. (Laughs)

TIM: (Notices a woman walking by) Oh, hey, listen, I'd better circulate... (moving over to the woman) Happy Chanukah, Tiffany! (they both move off camera)

ELAINE: This place is like Studio 54 with a menorah.

GEORGE: I'm gonna get some more of these kosher cocktail franks... (leaves)

ELAINE: Oh... (sees a guy looking at her) I got denim vest checking me out. (laughs) Fake phone number's coming out tonight.

JERRY: You have a standard fake?

ELAINE: Mm-hmm.

JERRY: (Notices an attractive woman walking by, starts to follow her) That's neat.

ELAINE: (Holds onto Jerry's arm) No, please! Denim vest! He's smoothing it! Jerry! God! (Jerry escapes Elaine's grasp, moves over to the woman. The man wearing a denim vest moves over to Elaine.)

DENIM VEST: Hi!

(Scene cuts to Jerry talking to the woman)

JERRY: Hi, I'm Jerry.

WOMAN: Hi.

JERRY: You might not know it to look at me, but I can run really, really fast.

(Scene cuts to Elaine)

ELAINE: Nice vest. I like the... big metal buttons

DENIM VEST: They're snaps. Listen, maybe we should, uh, go out some time?

ELAINE: Why don't I give you my phone number?

[Setting: Coffee Shop]

(George enters)

GEORGE: Hey.

JERRY: Hey! How'd it go with the cocktail franks?

GEORGE: Great! I ate the entire platter! Had to call in sick today.

JERRY: Didn't you call in sick yesterday?

GEORGE: Hey, I work for Kruger Industrial Smoothing: &quot;We don't care, and it shows.&quot;

JERRY: (Notices George brought his mail) You're gonna open your mail here?

GEORGE: Hey, at least I'm bringing something to this. (Starts flipping through envelopes, reads one ) &quot;Have you seen me?&quot; (Flicks it aside) Nope. (looks at next envelope) Woah, something from Whatley.

JERRY: See? You give, and you get.

GEORGE: (Reading the card from Whatley) &quot;This holiday season a donation has been made in your name to the Children's Alliance.&quot;?

JERRY: Oh, that's nice.

GEORGE: I got him Yankee's tickets! He got me a piece of paper saying &quot;I've given your gift to someone else!&quot;

JERRY: To a children's charity!

GEORGE: Don't you see how wrong that is?! Where's your Christmas spirit? And eye for an eye!

(Elaine enters)

ELAINE: Hey!

JERRY: Hey.

(Waitress moves toward the table)

ELAINE: (To waitress) Oh, nothing for me. (Waitress leaves) I'm going to &quot;Atomic Sub&quot; later.

JERRY: &quot;Atomic Sub&quot;? Why are you eating there?

ELAINE: I got a card, and they stamp it every time I buy a sub. 24 stamps, and I become a submarine (makes a gesture) captain.

JERRY: What does that mean?

ELAINE: (Embarrassed) Free sub.

(George lets out a depressed sigh while reading a card)

ELAINE: What?

GEORGE: Nothing. It's a card from my dad.

ELAINE: What is it? (Grabs the card from George, he tries to stop her, but fails. She reads it out loud.) &quot;Dear son, Happy Festivus.&quot; What is Festivus?

GEORGE: It's nothing, stop it...

JERRY: When George was growing up...

GEORGE: (Interrupting) Jerry, No!

JERRY: His father...

GEORGE: No!

JERRY: Hated all the commercial and religious aspects of Christmas, so he made up his own holiday.

ELAINE: Ohhhh... and another piece of the puzzle falls into place.

GEORGE: (pleading) Alright...

JERRY: And instead of a tree, didn't your father put up an aluminum pole?

(Elaine starts laughing uncontrollably - and continues to do so)

GEORGE: Jerry! Stop it!

JERRY: And weren't there a feats of strength that always ended up with you crying?

(Jerry joins in with Elaine's laughter)

GEORGE: I can't take it anymore! I'm going to work! Are you happy now?! (Gathers his things, and runs out of the coffee shop. Elaine and Jerry laugh hysterically)

[Setting: Jerry's apartment]

(Elaine is digging into her purse)

ELAINE: Oh, I can't believe it! I've lost my &quot;Atomic Sub&quot; card! ...Oh no! I bet I wrote that fake number on the back of it when I gave it to denim vest!

JERRY: So?

ELAINE: I've eaten 23 bad subs, I just need 1 more! It's like a long, bad movie, but you want to see the end of it!

JERRY: No, you walk out.

ELAINE: Alright, then, it's like a boring book, but you gotta finish it.

JERRY: No, you wait for the movie!

ELAINE: (Irritated, and through clinched teeth) I want that free sub.

JERRY: You don't need the card. High-end hoagie outfit like that, it's all computerized! (Snaps) They're cloning sheep now.

KRAMER: (Correcting) No, they're not cloning sheep. It's the same sheep! I saw Harry Blackstone do that trick with two goats and a handkerchief on the old Dean Martin show!

JERRY: So, why don't you just try your blow-off number and see if he's called it?

ELAINE: That's a good idea.

(Kramer's cordless phone rings, startling him. He digs through his coat, and pulls it out of the pocket)

KRAMER: (Answering phone) Yeah, Go! Wha... really? Yeah, ok. Yeah! Bye. (Hangs up) Great news! Yeah, the strike has been settled. I'm going back to work.

JERRY: What strike?

KRAMER: Yeah, H&amp;H Bagels. That's where I worked.

JERRY: You?

ELAINE: Worked?

JERRY: Bagels?

KRAMER: Yeah. Look, see. I still have my business card. (Pulls it out, hands it to Elaine) Yeah, we've been on strike for 12 years.

ELAINE: Oh, I remember seeing those guys picketing out there, but I haven't seen them in a long time.

KRAMER: Yeah, well, H&amp;H wouldn't let us use their bath room while we were picketing. It put a cramp on our solidarity.

ELAINE: What were your... demands?

KRAMER: Yeah, 5.35 an hour. And that's what they're paying now.

ELAINE: I believe that's the new minimum wage.

KRAMER: Now you know who to thank for that! ...Alright, I've got to go. (Heads for the door)

JERRY: Why didn't you ever mention this?

KRAMER: Jerry, I didn't want you to know I was out of work. It's embarrassing! (Leaves) (Scene ends)

[Setting: H&amp;H Bagel Shop]

(Kramer walks through the door)

KRAMER: Alright, everybody! I'm back!

MANAGER: Who are you?

KRAMER: Cosmo Kramer... strike's over.

MANAGER: Oh yeah! Kramer.

KRAMER: Huh... wha- Didn't any of the guys come back?

MANAGER: NO, I&quot;m sure they all got jobs... like, ten years ago.

KRAMER: Oh, man. Makes you wonder what it was all for...

MANAGER: I could use someone for the holidays...

KRAMER: Alright! Toss me an apron, let's bagel! (Takes off his coat, puts it in the display case, then turns to see a plate full of bagels.) What are those?

MANAGER: Those are raisin bagels.

KRAMER: (Picks one up, he's mesmerized) I never thought I'd live to see that...

[Setting: Horse Track Betting]

(Elaine confronts two unattractive bookies)

ELAINE: So, anyway, I've been giving out your number as my standard fake.

BOOKIE: So. You're Elaine Benes. We've been getting calls fro you for 5 years.

ELAINE: So, listen, when this guy calls, if you could just give him my real number...

BOOKIE: (Interrupting) Hey, Charlie! Guess who's here. Elaine Benes.

(Co-Worker in the back speaks up)

CHARLIE: Elaine Benes?!

(Various other men in the line behind Elaine say the same thing)

BOOKIE: You make a lot of man friends. You know who's a man? Charlie here, he's a man. You know who else? Me. I'm a man.

CHARLIE: (faintly) I'm a man.

ELAINE: Ohh... my...

BOOKIE: I'll have this best guy call your real number. You just, uh, give it to me. And that way, I'll have it. (Slides a pad over to Elaine so she can write it down)

ELAINE: My number? Ohh.. (looks at Kramer's business card) Okay... Uh, well, there you go. (writes H&amp;H's number down) And, uh, tell you what... (looks at the board in the back) put a sawbuck on Captain Nemo in the third at Belmont.

[Setting: classy restaurant]

(Jerry and Tim Whatley meet)

TIM: Hey, Jerry.

JERRY: Hey, Tim.

TIM: What's up?

JERRY: Actually, I'm having dinner with a girl I met at your party.

TIM: Mazel Tov.

(Jerry's date, Gwen, arrives. She's completely unattractive)

GWEN: Jerry... hi.

JERRY: Gwen?

GWEN: Yeah.

JERRY: (Not willing to believe how much uglier she is) Really?

GWEN: Yeah! Come on, our table is ready.

(Tim gives Jerry a face - almost like he feels sorry for Jerry)

[Setting: Jerry's apartment]

GEORGE: So, attractive one day - not attractive the next?

JERRY: Have you come across this?

GEORGE: Yes, I am familiar with this syndrome -- she's a two-face.

JERRY: (Relating) Like the Batman villain?

GEORGE: (Annoyed) If that helps you..

JERRY: So, if I ask her out again - I don't know who's showing up: The good, the bad, or the ugly.

GEORGE: (Identifying what Jerry said) Clint Eastwood!

JERRY: Yeah.

GEORGE: Hey, check this out. I gotta give out Christmas presents to everyone down at Kruger, so I'm pulling a Whatley. (Give a Christmas card to Jerry)

JERRY: (Reading it) &quot;A donation has been made in your name to the Human Fund.&quot; - What is that?

GEORGE: (With pride) Made it up.

JERRY: (Continuing reading) &quot;The Human Fund. Money for people.&quot;

GEORGE: What do you think?

JERRY: It has a certain understated stupidity.

GEORGE: (Once again, Identifying) The Outlaw Josey Wales!

JERRY: ...Yeah.

(Enter Kramer)

(He is holding a sack full of bagels)

KRAMER: Ah, gentlemen... bagels on the house!

JERRY: How was your first day?

KRAMER: Oh, fantastic! (Jerry and George both pick out a bagel) It felt so good to get my hands back in that dough.

(Jerry and George stop before they take a bite from their bagels)

JERRY: Your hands were in the dough?

KRAMER: No, I didn't make these bagels. (Jerry and George both take a bite) Yeah, they're day-olds. The homeless won't even touch them. (Jerry and George stop eating) Oh, we try to fool them by putting a few fresh ones on top, but they dig... they, they test.

(George spits his bagel out)

GEORGE: Alright. Uh, well, I'm out of here. (Gets up to leave)

JERRY: Happy Festivus!

KRAMER: What's Festivus?

JERRY: When George was growing up...

GEORGE: (Interrupting) No!

JERRY: His father...

GEORGE: Stop it! It's nothing. It's a stupid holiday my father invented. It doesn't exist!

(Elaine enters while George is exiting)

ELAINE: Happy Festivus, Georgie.

(George leaves yelling out &quot;God!&quot;)

KRAMER: Frank invented a holiday? He's so prolific!

ELAINE: Kramer, listen, I got a little phone relay going, so, if a guy calls H&amp;H and he's looking for me, you take a message.

JERRY: You're still trying to get that free sub?

ELAINE: Hey! I have spent a lot of time, and I have eaten a lot of crap to get to where I am today. And I am NOT throwing it all away now.

JERRY: Is there a captain's hat involved in this?

ELAINE: Maybe.

[Setting: H&amp;H Bagel Shop]

FRANK: Kramer, I got your message. I haven't celebrated Festivus in years! What is your interest?

KRAMER: Well, just tell me everything, huh?

FRANK: Many Christmases ago, I went to buy a doll for my son. I reach for the last one they had - but so did another man. As I rained blows upon him, I realized there had to be another way!

KRAMER: What happened to the doll?

FRANK: It was destroyed. But out of that, a new holiday was born. &quot;A Festivus for the rest of us!&quot;

KRAMER: That must have been some kind of doll.

FRANK: She was.

[Setting: Kruger Office Building]

(George is in the hallway, dispensing his made-up gifts)

GEORGE: Merry Christmas, Merry Christmas! (Co-worker gives a gift to George) Oh, Sandy! Here is a little something for you... (hands her a card)

SANDY: (After reading the cheap gift, she's suddenly unimpressed) ...Oh ...thanks. (Walks off)

(George passes an open doorway)

GEORGE: Phil, I loved those cigars! Incoming! (Flicks his card toward Phil)

PHIL: Ow!

(George meets up with Kruger)

GEORGE: Aw, Mr. Kruger, Sir. Merry Christmas! (Hands him a card)

KRUGER: Not if you could see our books... what's this?

GEORGE: The Human Fund.

KRUGER: Whatever. (Walks off)

GEORGE: Exactly. (Sees an off-camera co-worker) Erica!

[Setting: H&amp;H Bagel Shop]

(Frank is still telling Kramer about Festivus)

FRANK: And at the Festivus dinner, you gather your family around, and you tell them all the ways they have disappointed you over the past year.

KRAMER: Is there a tree?

FRANK: No. Instead, there's a pole. It requires not decoration. I find tinsel distracting.

KRAMER: Frank, this new holiday of yours is scratching me right where I itch.

FRANK: Let's do it then! Festivus is back! I'll get the pole out of the crawl space. (Turns to leave, meets up with Elaine)

ELAINE: Hello, Frank.

FRANK: Hello, woman. (leaves)

ELAINE: Kramer! Kramer... any word from the vest?

KRAMER: No. (To manager of H&amp;H) Ah, listen, Harry, I need the 23rd off.

MANAGER: Hey! I hired you to work during the holidays. This is the holidays.

KRAMER: But it's Festivus.

MANAGER: What?

KRAMER: You know you're infringing on my right to celebrate new holidays...

MANAGER: That's not a right.

KRAMER: Well, it's going to be! Because I'm going back on strike. Come on Elaine. (Takes of his apron, and goes for his coat) It's a walk out!

ELAINE: No, I got to stay here and wait for the call.

KRAMER: What? You're siding with management?!

ELAINE: No, I just...

KRAMER: (Interrupting) Scab! Scab! (pointing at Elaine) Scab!

[Setting: Taxi Cab]

(Gwen joins Jerry in the cab. She's in her attractive state)

GWEN: Hey.

JERRY: Boy, am I glad to see you.

GWEN: You were expecting someone else?

JERRY: You never know.

GWEN: (To driver) You know, you might want to take the tunnel.

JERRY: So, uh, what do you feel like eating? Chinese or Italian?

(All the sudden, Gwen is extremely ugly)

GWEN: I can go either way.

JERRY: (Shocked) You're telling me.

[Setting: the Coffee Shop]

GEORGE: So, she was switching? Back and forth?

JERRY: Actually, the only place she always looked good was in that back booth over there.

GEORGE: So, just bring her here. This is all you really need.

JERRY: I can't just keep bringing her to the coffee shop. I mean, what if things, you know, progress?

GEORGE: Lights out.

JERRY: Alright, I'll give it a shot! I do really like this coffee shop. Nice cuff links, by the way.

GEORGE: (Pointing to them) Office Christmas gift. I tell you, this Human Fund is a gold mine!

JERRY: That's not a french cuff shirt, you know.

GEORGE: I know. I cut the button off and poked a hole with a letter opener.

JERRY: Oh, that's classy.

(Frank and Kramer enter. Frank is dragging an aluminum pole)

KRAMER: Well, Happy Festivus.

GEORGE: What is that? Is that the pole?!

FRANK: George, Festivus is your heritage - it's part of who you are.

GEORGE: (Sulking) That's why I hate it.

KRAMER: There's a big dinner Tuesday night at Frank's house - everyone's invited.

FRANK: George, you're forgetting how much Festivus has meant to us all. I brought one of the casette tapes. (Franks pushes play, George as a child celebrating Festivus is heard)

FRANK: Read that poem.

GEORGE: (Complaining) I can't read it. I need my glasses!

FRANK: You don't need glasses, you're just weak! You're weak!

ESTELLE: Leave him alone!

FRANK: Alright, George. It's time for the feats of strength.

(George has a break down)

GEORGE: No! No! Turn it off! No feats of strength! (Gets up and starts running out of the coffee shop) I hate Festivus!

FRANK: We had some good times.

(Gwen walks in, and greets Jerry. She's in her unattractive state)

GWEN: Hey.

JERRY: Hi there. This is Kramer, and Frank.

GWEN: Hi.

KRAMER: (Shocked at her ugliness, he stammers) Hello.

GWEN: So, you ready to go?

JERRY: Uh, why don't we stay here? The back booth just opened up. (They both walk to the booth and sit down. Suddenly, Gwen is attractive) Now this is a good looking booth.

[Setting: H&amp;H Bagel Shop]

(Kramer is picketing out side.)

KRAMER: Protect Festivus! Hey, no bagels, no bagels, no bagels, (Continues to chant)

(Cut to inside the store)

MANAGER: (To a waiting Elaine) Lady, if you want a sandwich, I'll make you a sandwich.

ELAINE: (Whining) I want the one that I earned. (Phone rings) I'll get it. I'll get it! (Into phone) H&amp;H, and Elaine.

KRAMER: (From a phone booth right outside the store) Elaine, you should get out of there. I sabotaged the bagel machine last night. It's going down.

ELAINE: What did you do?

KRAMER: You've been warned.

(Elaine looks out the window, and sees Kramer at the pay phone)

ELAINE: Oh, hi! (Waves at him)

(Steam starts coming from a pipe on the machine. Elaine hangs up)

WORKER: Hey, the steam valve's broke.

MANAGER: Can we still make bagels?

WORKER: Sure. It's just a little steamy.

(Kramer knocks on the shop door)

KRAMER: Hey! How do you like your bagels now?!

(No one inside seems to care. Kramer waits by the door to see if anyone was affected)

[Setting: Kruger Building]

KRUGER: George, I got something for you. (Pulls a check from his pocket) I'm suppose to find a charity and throw some of the company's money at it. They all seem the same to me, so, what's the difference? (Hands the check to George)

GEORGE: 20 thousand dollars?

KRUGER: Made out to the Human Fund. (Tries to enter his office, but it's locked) Oh, damn. I've locked myself out of my office again. Oh well. I'm going home.

[Setting: Coffee Shop]

GWEN: Jerry, how many times do we have to come to this... place?

JERRY: Why? It's our place.

GWEN: I just found a rubber band in my soup.

JERRY: Oh... I know who's cooking today!

(Enter George)

GEORGE: Hey! Surprise, surprise!

JERRY: Hey, Georgie!

GWEN: I think I'm just gonna go.

JERRY: I'll be here.

(Gwen leaves)

GEORGE: (Sees Gwen's meal) Hey, soup.

JERRY: She didn't touch it.

(George spoons through his soup, and finds a rubber band)

GEORGE: Ohh... Paco! (Flicks rubber band toward the kitchen) Hey, take a look at this. (Hands Jerry Kruger's check)

JERRY: 20 thousand dollars from Kruger? You're not keeping this.

GEORGE: I don't know.

JERRY: Excuse me?

GEORGE: I've been doing a lot of thinking. This might be my chance to start giving something back.

JERRY: You want to give something back? Start with the 20 thousand dollars.

GEORGE: I'm serious.

JERRY: You're going to start your own charity?

GEORGE: I think I could be a philanthropist. A kick ass philanthropist! I would have all this money, and people would love me. Then they would come to me... and beg! And if I felt like it, I would help them out. And then they would owe me big time! (Thinking to himself) ...First thing I'm gonna need is a driver...

[Setting: Outside H&amp;H Bagels]

(Kramer is chanting 'no bagels, no bagels...' Elaine walks out, her make-up is distorted, and her face is pale because of the steam)

ELAINE: Kramer, the vest just called.

KRAMER: (Shocked by the way Elaine looks) Yama - Hama! It's fright night!

ELAINE: Oh, yeah, I got a little steam bath. Listen, in 10 minutes, I'm gonna have my hands on that &quot;Atomic Sub&quot; card.

KRAMER: And?

ELAINE: (Embarrassed) Free sub. (Starts to leave) I'll see ya.

KRAMER: Yeah.

(Gwen walks by, she's in her ugly state)

GWEN: Kramer, Hi!

KRAMER: Oh, hello.

GWEN: It's Gwen... We met ...at the coffee shop.

KRAMER: Ah-huh.

GWEN: I'm dating your friend, Jerry...

KRAMER: Ahh... I don't know who you really are, but I've seen Jerry's girlfriend, and she's not you. You're much better looking - and like, a foot taller.

GWEN: That's why we're always hiding in that coffee shop! He's afraid of getting caught.

KRAMER: Oh, he's a tomcat.

(Cut to Elaine)

(She meets up with Denim Vest on the street corner)

ELAINE: Steve.

DENIM VEST: Hmm?

ELAINE: It's Elaine.

DENIM VEST: From Tim Whatley's party?

ELAINE: Yeah.

DENIM VEST: You look.. different.

ELAINE: I see you're still sticking with the denim. (He's wearing a denim coat) Do you have that card that I gave you?

DENIM VEST: Well, I had it back at my place, but I can't go there now... I'll give it to you later, or something.

ELAINE: No, no, no. You give me your number.

DENIM VEST: Okay. Sure. (Pulls out a pad, and starts writing a number down) Do you have the mumps?

ELAINE: No.

DENIM VEST: Typhoid?

ELAINE: No.

DENIM VEST: (Hands her the paper, and runs off) Yama - Hama!

(Elaine looks at the number, and sees it's the same as the number on a nearby truck)

ELAINE: A fake number! Blimey!

[Setting: Kruger's office]

KRUGER: George, we have a problem. There's a memo, here, from accounting telling me there's no such thing as the Human Fund.

GEORGE: Well, there could be.

KRUGER: But there isn't.

GEORGE: Well, I - I could, Uh, I could give the money back. Here. (Holds it out)

KRUGER: George, I don't get it. If there's no Human Fund, those donation cards were fake. You better have a damn good reason why you gave me a fake Christmas gift.

GEORGE: Well, sir, I - I gave out the fake card, because, um, I don't really celebrate Christmas. I, um, I celebrate Festivus.

KRUGER: Feminist?

GEORGE: Festivus, Sir. And, uh, I was afraid that I would be persecuted for my beliefs. They drove my family out of Bayside, Sir!

KRUGER: Are you making all this up, too?

GEORGE: Oh, no, Sir. Festivus is all too real. And... I could prove it - if I had to.

KRUGER: Yeah, you probably should.

[Setting: The Costanza's house]

GEORGE: Happy Festivus!

FRANK: George? This is a surprise. (Looking at Kruger) Who's the suit?

GEORGE: Yo, dad. This is my boss, Mr. Kruger.

FRANK: Have you seen the pole, Kruger?

GEORGE: Dad, he doesn't need to see the pole.

FRANK: He's gonna see it.

(Enter Jerry and Elaine. Elaine is still ugly from the steam)

GEORGE: Happy Festivus! (Sees Elaine) Yama - Hama!

ELAINE: I didn't have time to go home. What are you doing here?

GEORGE: Embracing my roots.

JERRY: They nailed you on the 20 G's?

GEORGE: Busted cold.

(Cut to Kruger and Frank)

(They're looking at the Festivus pole)

FRANK: It's made from aluminum. Very high strength-to-weight ratio.

KRUGER: I find your belief system fascinating.

(Enter Kramer)

(Kramer's with the two bookies from Horse Track Betting)

KRAMER: Hey! Happy Festivus, everyone! (Hugs George, and jumps up and down) Hee, hee, hee!

BOOKIE: Hello again, Miss Benes.

ELAINE: What are you doing here?

BOOKIE: Damnedest thing... me and Charlie were calling to ask you out, and, uh, we got this bagel place..

KRAMER: (Finishing the story) I told them I was just about to see you... It's a Festivus miracle!

(Estelle comes through the kitchen door, hitting Kramer as she opens it)

ESTELLE: Dinner's ready!

FRANK: Let's begin.

(Everyone sits around the table. Kruger recognized Kramer from &quot;The Meat Slicer&quot; episode..) KRUGER: Dr... Van Nostrand?

KRAMER: Uh... that's right.

(Cut to Frank)

FRANK: Welcome, new comers. The tradition of Festivus begins with the airing of grievances. I got a lot of problems with you people! And now you're gonna hear about it! You, Kruger. My son tells me your company stinks!

GEORGE: Oh, God.

FRANK: (To George) Quiet, you'll get yours in a minute. Kruger, you couldn't smooth a silk sheet if you had a hot date with a babe... I lost my train of thought.

(Frank sits down, Jerry gives a face that says &quot;That's a shame&quot;. Gwen walks in)

GWEN: Jerry!

JERRY: Gwen! How'd you know I was here?

GWEN: Kramer told me.

KRAMER: Another Festivus miracle!

(Jerry gives Kramer a death stare. He shuts up. Gwen notices Elaine)

GWEN: I guess this is the ugly girl I've been hearing about.

ELAINE: Hey, I was in a shvitz for 6 hours. Give me a break.

(Gwen leaves, Jerry follows)

JERRY: Gwen. Gwen, wait! Ah! (runs back to his seat) Bad lighting on the porch.

ELAINE: (To bookie) Hey, how'd my horse do?

BOOKIE: He had to be shot.

FRANK: And now as Festivus rolls on, we come to the feats of strength.

GEORGE: Not the feats of strength...

FRANK: This year, the honor goes to Mr. Kramer.

KRAMER: Uh-oh. Oh, gee, Frank, I'm sorry. I gotta go. I have to work a double shift at H&amp;H.

JERRY: I thought you were on strike?

KRAMER: Well, I caved. I mean, I really had to use their bathroom. Frank, no offence, but this holiday is a little (makes a series of noises) out there.

GEORGE: Kramer! You can't go! Who's gonna do the feats of strength?

(Exit Kramer)

KRUGER: (Sipping liquor from a flask) How about George?

FRANK: Good thinking, Kruger. Until you pin me, George, Festivus is not over!

GEORGE: Oh, please, somebody, stop this!

FRANK: (Taking off his sweater) Let's rumble!

(Cuts to an outside view of the Costanza's house)

ESTELLE: I think you can take him, Georgie!

GEORGE: Oh, come on! Be sensible.

FRANK: Stop crying, and fight your father!

GEORGE: Ow! ...Ow! I give, I give! Uncle!

FRANK: This is the best Festivus ever!

[Setting: H&amp;H Bagel Shop]

(Kramer is shaping some dough and chewing gum - his gum falls into the dough. He starts looking for the gum, and starts extracting it from the dough. The manger is watching)

MANAGER: Alright. That's enough. You're fired.

KRAMER: Thank - you! (Gets his coat, and leaves)

The End
