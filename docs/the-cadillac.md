---
layout: default
title: The Cadillac
parent: Season 7
nav_order: 14
permalink: /the-cadillac
cat: ep
series_ep: 124, 125
pc: 714, 717
season: 7
episode: 14, 15
aired: February 8, 1996
written: Larry David & Jerry Seinfeld
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697667/
wiki: https://en.wikipedia.org/wiki/The_Cadillac
---

# The Cadillac

| Season 7 - Episode 14 & 15              | February 8, 1996          |
|:----------------------------------------|:--------------------------|
| Written by Larry David & Jerry Seinfeld | Directed by Andy Ackerman |
| Series Episode 124 & 125                | Production Code 714 & 717 |

"The Cadillac" is an hour-long, two-part episode of NBC sitcom Seinfeld. It was the 124th and 125th episode and 14th and 15th episode for the seventh season. It aired on February 8, 1996. This was the last episode to be co-written by Jerry Seinfeld.

Academy Award-winning actress Marisa Tomei plays herself in the episode.

## Plot

### Part 1

Jerry comes back from a high-paying gig and awes Kramer with his earnings. Jerry surprises his parents by buying them a new fully loaded Cadillac Fleetwood; learning about his financial situation, Elaine becomes infatuated with Jerry once again (The Fleetwood base model cost US$36,995 at the time[citation needed]).

The Plaza Cable company wants to meet with Kramer, who makes sure he is not at home when they arrive, to retaliate for their lateness when his cable was installed.

Meanwhile, as The A.V. Club describes it, "Back in New York, we're treated to a whole ethical dilemma where George wrestles with the news that he's Marisa Tomei's type and could have been introduced to her if he was still single." George reconsiders his engagement to Susan when Katy, Elaine's friend, reveals that she is friends with actress Marisa Tomei:

> KATY: I actually would've set you up with a friend of mine.
>
> GEORGE: Oh-ho, yeah?
>
> KATY: You'd be perfect for her. She loves quirky, funny guys.
>
> GEORGE: Bald... uh?
>
> KATY: Loves bald.
>
> GEORGE: Loves bald? (laughs) Wow. Who is she?
>
> KATY: Marisa Tomei.
>
> GEORGE: (taken aback) The actress?
>
> KATY: Yeah.
>
> GEORGE: You're friends with Marisa Tomei?
>
> KATY: That's right.
>
> GEORGE: That's, that's incredible. My Cousin Vinny, I loved her, she was fantastic!
>
> KATY: Yeah, I know.
>
> GEORGE: You were gonna fix me up with her?
>
> KATY: Yeah, she's just been sitting home.
>
> GEORGE: (fever pitch) Marisa Tomei's sitting home, Elaine!

Hearing all this, George develops an obsession for Marisa.

Jack Klompus accuses Morty of embezzling funds from the office of condo president to pay for his new Cadillac. Jack does not believe Jerry has enough talent to earn so much money, and convinces the rest of the Condo Board of this. Board member Herb decides to call for an investigation as Ralph seconds the motion.

George wants to meet Marisa for a cup of coffee, even when Elaine tells him "it's cheating!" on his fiancée, and he insists on getting Marisa's phone number, to the point of harassing Katy when she is in the hospital with a heart condition.

During the credits, Morty informs Jerry and Helen that he is being threatened for impeachment. Nobody has ever been impeached as condo president before as Morty states that if he gets impeached, they will have to move to Del Boca Vista. Their neighbor Evelyn tells them that three of the building representatives are voting for impeachment, three other building representatives are voting against impeachment, and board member Mrs. Choate of Building D is left with the deciding vote.

### Part 2

Elaine calls Jerry in Florida and tells him she wants to come and join him, but Jerry demurs. Kramer continues to avoid the Plaza Cable worker.

Morty and Helen meet with Mabel Choate who is coincidentally the same woman from whom Jerry stole the marble rye back in "The Rye". While Jerry seems to remember her, Mabel doesn't as they officially meet. Mabel made a reference to how she was mugged of her marble rye while she was visiting her daughter and plans not to return. Jerry takes his leave as Morty explains his side of the story in order to get her vote.

George's obsession with Marisa Tomei makes Susan suspicious when she comes home and finds him on the couch watching My Cousin Vinny and again, later, when she finds him watching Only You (specifically, scenes in which Tomei is on screen). George obtains Marisa's phone number and works with Elaine to create a cover story involving Elaine and her fictitious "boyfriend", Art Vandelay. In a scene that is a nearly exact inversion of the walking date in The Soup, George meets Marisa Tomei and they have a similar date in the park. Marisa is initially enchanted by George, but when he tells her, "Well, Marisa. See, the thing is, I'm sort of engaged," she is furious with him, decking him and storming off. Susan suspects George is having an affair with Elaine and questions her regarding his whereabouts. Controlling herself (having initially burst out into spontaneous laughter), Elaine answers the questions. The answers follow the cover story they agreed on earlier, but Susan trips her up on one of the questions and is still suspicious and asks another question they hadn't anticipated. After Susan leaves, Elaine frantically tries to contact George so he will be able to give a matching answer to the question. George returns to the apartment and is met by Susan, who asks the same tricky question. George gives a wrong answer and receives his second punching that day.

Morty is relying on the vote of Mabel Choate to save him from impeachment. The vote is reversed when Mabel hears Jack Klompus refer to her under his breath as an "old bag," triggering her memory of the incident when Jerry had used that same epithet on her. Mabel then rants to Morty that it was his son who stole the rye bread from her and votes for Morty's impeachment. With some of the other board members changing their vote, Morty is removed from office as he was the only one who voted against it. In accordance with the second constitution of The Pines of Mar Gables Phase II, Herb has condo vice-president Jack Klompus sworn in as the new condo president. Jack is victorious while Morty remains silent.

The cable guy repeatedly tries to confront Kramer, but he always gets away, in scenes which reference Alfred Hitchcock's Vertigo. The cable guy finally concedes defeat and apologizes on behalf of cable guys everywhere, promising better service across the board. Kramer appears and has an emotional reconciliation with the cable guy and the two embrace.

During the credits, Morty and his wife leave the condo in a scene referencing Oliver Stone's Nixon. Some of The Pines of Mar Gables Phase II's inhabitants see them off where two of them are sad to see the Seinfelds go.

## Critical reception

The A.V. Club community gave the two-part episode an A- grade. David Sims discusses Del Boca Vista politics at length:

> The Cadillac is a cautionary tale—Jerry means well to surprise his parents with a new Cadillac bought with all the money he's making as a comedian, but it only raises suspicion in the condo community. Later, he stops them from going to the early-bird special because he does not want a steak dinner at 4:30, but that just ostracizes the poor Seinfelds from their snippy friends.

[From the Script by David and Seinfeld:]

> JERRY: (bewildered) Four-thirty? Who eats dinner at four-thirty?
>
> MORTY: By the time we sit down, it'll be quarter to five.
>
> JERRY: I don't understand why we have to eat now.
>
> HELEN: We gotta catch the early-bird. It's only between four-thirty and six.
>
> MORTY: Yeah. They give you a tenderloin, a salad and a baked potato, for four-ninety-five. You know what that cost you after six?
>
> JERRY: Can't we eat at a decent hour? I'll treat, okay?
>
> HELEN: You're not buying us dinner.
>
> JERRY: (emphatic) I'm not force-feeding myself a steak at four-thirty to save a couple of bucks, I'll tell you that!
>
> HELEN: All right (sitting on the couch), we'll wait. (pointedly) But it's unheard of.

Sims continues,

> Much like The Pen, this Seinfeld-and-David-scripted episode's message is simple: don't fuck with these crazy old people. Sure, much of what Jerry says is rational. And sure, even the old people have a certain logic to them from time to time. But there's an element to the retirement community that Jerry just simply can't understand, nor should he want to, and his efforts to fix the problems he's caused only make things worse. "You could put a fence around these condos and call it an insane asylum and nobody would know the difference!" he cries. Even though, in reality, Jerry could easily produce documentation showing he bought his dad the car and Morty wasn't stealing anything, the audience accepts that that's not how things work around here. Klompus' accusation snowballs quickly, and there's nothing Morty and Helen can do about it once that Cadillac is parked in their driveway, a monument to arrogance in their neighbors' envious eyes... Of course, Jerry isn't helped by the fact that he actually assaulted and robbed one of the condo board members on a New York City street in The Rye. Sure, all he took from her was a marble rye, but that was not his finest hour.

Sims was less impressed by the George/Marisa subplot, saying, among further comments, "This is George at his worst—he's not just being a bad person, he's also not very good at it. Sometimes, his deviousness is pretty impressive, but this is not one of those times, and it's amazing it takes Susan so long to figure out he's lying about something. Ultimately, Tomei's appearance and everything around it ends up disappointing—it's a glorified B-plot when it shouldn't be, and it wraps up too quickly with George just getting his comeuppance from both ladies with none of his usual deviousness."

## Cast

### Part 1

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Cosmo Kramer

#### Recurring

Liz Sheridan ................... Helen Seinfeld  
Barney Martin ................ Morty Seinfeld  
Heidi Swedberg ............. Susan Biddle Ross  
Sandy Baron .................. Jack Klompus

#### Guests

Walter Olkewicz .................. Nick (Cable Guy)  
Annabelle Gurwitch ............ Katy  
Bill Macy .............................. Herb  
Jesse White .......................... Ralph

### Part 2

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Cosmo Kramer

#### Regulars

Liz Sheridan .................... Helen Seinfeld  
Barney Martin ................. Morty Seinfeld  
Heidi Swedberg .............. Susan Biddle Ross  
Sandy Baron ................... Jack Klompus

#### Guests

Marisa Tomei ....................... Herself  
Walter Olkewicz .................. Nick (Cable Guy)  
Ann Guilbert ....................... Evelyn  
Frances Bay ......................... Mabel Choate  
Bill Macy .............................. Herb  
Jesse White .......................... Ralph  
Annie Korzen ...................... Doris Klompus  
Daniel Zacapa ...................... Power Guy (John)  
Golde Starger ...................... Bldg &quot;A&quot;  
Janice Davis ......................... Bldg &quot;B&quot;  
Art Frankle .......................... Bldg &quot;C&quot;

## Script

[Jerry's Apartment]

(Jerry enters, carrying a couple of bags and his mail. No sooner has he put his bags down than Kramer enters.)

KRAMER: Hey! You're back!

JERRY: Oh, I'm back, baby.

KRAMER: Yeah, how was it?

JERRY: Great. One of the best jobs I ever had.

KRAMER: Good for you.

JERRY: I killed.

KRAMER: You killed.

JERRY: (taking off his jacket) Slaughtered. Wiped the floor with 'em and (hanging up jacket) not only that, the money was unbelievable.

KRAMER: Unbelievable.

JERRY: Yeah. Highest paying job I ever had.

KRAMER: How much?

JERRY: (reluctant) Nah, nah. I'd rather...

KRAMER: Aw, c'mon.

JERRY: Nah. It's not good for friends to talk about money, it can affect the friendship.

KRAMER: I tell you how much I make.

JERRY: And I'm always impressed.

KRAMER: Just show me the cheque, c'mon.

JERRY: Awright, fine. You wanna see it?

KRAMER: I wanna see it.

JERRY: Okay.

(Jerry gets out his wallet, extracts a check from within it and hands it to Kramer.)

JERRY: Here, check that out.

(Kramer reads the cheque, and a look of astonishment comes to his face.)

KRAMER: (stunned) Whuf! This is unbelievable.

JERRY: I told you.

(Kramer has to sit on the couch, he's so shocked.)

KRAMER: My god, you're rich.

JERRY: (taking back the check) Oh yeah.

KRAMER: I didn't know you made that kinda money. (subdued) Jeez.

JERRY: What?

KRAMER: I don't think I can talk to you any more. I feel inferior.

JERRY: I never shoulda told you.

KRAMER: You know, Jerry, I think this changes the relationship. I mean, I feel it. Do you feel it?

JERRY: No, I can't feel anything.

KRAMER: Well, what're you gonna do with all that money?

JERRY: Actually, I was thinking of donating a large portion of it to charity.

KRAMER: (pleased) Really?

JERRY: (deadpan) No.

KRAMER: Well, you should, Jerry.

JERRY: No, to tell you the truth, I was thinking of buying my father a new car.

KRAMER: Now, you see, that's nice.

JERRY: Maybe a Cadillac.

KRAMER: (smiles) Cadillac. Ooh-la-la.

JERRY: Yeah. (thinking) That would really blow his mind. He's always wanted one, his whole life, he's never been able to afford it. (decisive) I'm gonna do it.

KRAMER: You're gonna score some big points with the man upstairs on this one.

JERRY: Oh, isn't that what it's all about?

(Kramer and Jerry smile in agreement.)

[Monk's]

(Elaine sits in a booth, across from her friend Katy.)

ELAINE: Who was Pippi Longstocking?

KATY: Pippi Longstocking? (thoughtful) Hmm, I don't don't know.

ELAINE: Did she have anything to do with Hitler?

KATY: Hitler? (thinks) Maybe.

(George enters and walks over to the booth. He's looking impatient.)

GEORGE: Hey.

ELAINE: Hey George.

GEORGE: Have you seen Jerry? (sitting beside Elaine) I told him two o'clock.

(George twists round, so he can watch the door.)

ELAINE: (indicating) You remember Katy?

GEORGE: (distracted) Mm? (acknowledges Katy) Oh, hi. Oh, yeah, yeah, yeah. We met uh, skiing that time. You're uh, married to the eye guy.

KATY: Also ear, nose and throat.

GEORGE: Nose. (chuckles) What's the worst that can happen to a nose? What does it get? Stuffed? (chuckles)

KATY: (amused) He's funny.

ELAINE: Ah, you don't have to tell me.

GEORGE: Ahh, ladies, please, please. (chuckles)

ELAINE: Oh, you know what? He got engaged.

KATY: Oh, you did? (disappointed) Oh.

GEORGE: What? Is that bad?

KATY: I actually would've set you up with a friend of mine.

GEORGE: Oh-ho, yeah?

KATY: You'd be perfect for her. She loves quirky, funny guys.

GEORGE: (curious) Bald? Uh?

KATY: Loves bald.

GEORGE: Loves bald? (laughs) Wow. Who uh, who is she?

KATY: Marisa Tomei.

GEORGE: (taken aback) The actress?

KATY: Yeah.

GEORGE: You're friends with Marisa Tomei?

KATY: That's right.

GEORGE: (getting excited) That's, that's incredible. My Cousin Vinny, I love her, she was fantastic!

KATY: Yeah, I know.

GEORGE: (more excitement) You were gonna fix me up with her?

KATY: Yeah, she's just been sitting home.

GEORGE: (fever pitch) Marisa Tomei's sitting home, Elaine! Wh.. why didn't you tell me that Katy was friends with Marisa Tomei?!

ELAINE: (deadpan) Oh, I don't know what I was thinking.

[Jerry's Apartment]

(Jerry is talking on the phone as George enters and hangs up his coat.)

JERRY: I want this baby fully loaded. (listens) Well, how soon can you get it there? (listens) Oh, that's terrific. (listens) Okay, thanks, bye.

(Jerry hangs up and turns to George.)

JERRY: Hey, guess what? I just bought my father a Cadillac.

GEORGE: Really? Wow, you're quite the good son.

JERRY: Yeah, I'm a very good boy. I'm flying down there and surprising him.

(Jerry walks to the kitchen.)

GEORGE: You gonna sleep on the fold-out?

JERRY: (joyless) Yeah, yes.

(Jerry opens the fridge door and gets out a piece of pie on a plate.)

GEORGE: Uh, let me ask you something. You uh, you ever hear of Marisa Tomei?

JERRY: The actress?

GEORGE: Yeah. She's uh, she's something, isn't she?

JERRY: Oh yeah.

GEORGE: Well, you know Katy, Elaine's friend?

JERRY: Yeah?

GEORGE: She happens to be very good friends with her.

JERRY: Marisa Tomei?

GEORGE: Uh-hmm.

JERRY: How does she know Marisa Tomei?

GEORGE: I don't know, I didn't ask.

JERRY: You didn't ask how she knows Marisa Tomei?

GEORGE: Not the point!

JERRY: Alright.

GEORGE: Can I finish?

JERRY: Go ahead. Seems like a reasonable question, is all I'm saying. I would've asked her.

(Jerry opens the freezer compartment and gets out a tub of ice cream.)

GEORGE: Alright! So, she said, that she could've fixed me up with her.

JERRY: What d'you mean, could've?

GEORGE: Well, you know, if I uh, wasn't engaged.

JERRY: Ohh!

GEORGE: Coulda fixed me up with Marisa Tomei. She said I was just her type!

JERRY: Really?

(Jerry puts a scoop of ice cream on the pie.)

GEORGE: (getting animated) Yeah. Yeah. You know the odds of me being anyone's type?! I have never been anyone's type, but apparently, this Marisa Tomei loves funny, quirky, bald men.

JERRY: You know, she won an academy award?

GEORGE: (sarcasm) Hu-hu, like I don't know that? (excitement) My Cousin Vinny, I love that! I, George Costanza, could be on a date with an Oscar winner! An Oscar winner, Jerry! You know what that's like? It's like if fifty years ago, someone fixed me up with Katherine Hepburn? Same thing!

JERRY: Now there's a match. You and Katherine Hepburn.

GEORGE: I mean, you've seen her, right?

JERRY: Katherine Hepburn? Oh yeah.

GEORGE: (shout) Marisa Tomei!

JERRY: Yeah, yeah, yeah.

GEORGE: I mean, she's beautiful, right? She's just my type. The dark hair, the full lips.

(George wanders to the couch, Jerry following with his plate of pie and ice cream.)

JERRY: You like full lips.

GEORGE: Oh, I love full lips. Something you can really put the lipstick on.

(The two sit on the couch.)

JERRY: Mmm-mmm. Too bad you're engaged.

(Jerry takes a mouthful of pie.)

GEORGE: (downcast) Yeah, too bad. Too bad.

(There is a knock at the door.)

JERRY: (about the pie) This is no good.

(Jerry puts down the plate, gets up and opens the door, to reveal a guy in a Plaza Cable cap.)

JERRY: Hi.

NICK: Hi, uh, excuse me. I'm uh, I'm with Plaza Cable. I'm sorry to bother you. I... I'm looking for the guy who lives over here. Been waiting about uh, heh, two hours. You uh, happen to know where he is?

JERRY: No.

NICK: If you see him, Could you just tell him the cable company was here?

JERRY: Sure.

NICK: Thanks.

(The guy leaves. Jerry closes the door and returns to the couch.)

JERRY: (about the cable guy) Nice people.

(Jerry sits beside George again.)

GEORGE: Listen, lemme ask you a question. What if I got a cup of coffee with her?

JERRY: Well, what about Susan?

GEORGE: (worked up) I can't have a cup of coffee with a person?! I'm not allowed to have coffee?!

JERRY: Would you tell Susan about it?

GEORGE: (frustrated) Not necessarily.

JERRY: Well, if you can't tell Susan about it, then there's something wrong.

GEORGE: (shouts) Of course there's something wrong! (points at Jerry) We had a pact!

(Jerry heads for the fridge, just as Kramer enters.)

JERRY: Hey, cable guy's looking for you.

KRAMER: Oh, yeah?

JERRY: Yeah.

KRAMER: Oh. Yeah, I been getting HBO and Showtime for free. See, they just found out about it, so now they wanna come and take it out.

JERRY: Well, said he was waiting about two hours. Seemed a little put out.

KRAMER: Oh, was he? Was he? I guess the cable man doesn't like to be kept waiting.

JERRY: You don't seem too bothered by it.

KRAMER: You remember what they did to me ten years ago? &quot;Oh, we'll be there in the morning between nine and one&quot;, or &quot;We'll be there between two and six&quot;! (quiet anger) And I sat there, hour after hour, without so much as a phone call. Finally, they show up, no apology, tracking mud all over my nice clean floors. (malice) Now, they want me to accommodate them. Well, looks like the shoe's on the other foot, doesn't it?

JERRY: Boy, I've never seen you like this.

KRAMER: Oh, you don't wanna get on my bad side.

[George's Apartment]

(George sits on his couch, watching My Cousin Vinny on video. Onscreen, Marisa Tomei, is in court, explaining transmission systems.)

MARISA: ...positrack, which was not available on the sixty-four Buick Skylark.

(Susan enters)

SUSAN: Hey. (glances at TV) Hey what're you watching? What is that? My Cousin Vinny?

GEORGE: Yeah.

(Susan stands behind George as she speaks.)

SUSAN: I thought you saw that before. How come you're watching that again?

GEORGE: I.. I.. I dunno.

SUSAN: Did you know Marisa Tomei won an Oscar for that? Boy, she's beautiful, don't you think? I wish I looked like that.

(George daren't say anything, but from his expression it's clear he wishes Susan looked like that.)

SUSAN: Turn it off. You're making me jealous. I'm gonna think you like her more than you like me.

(Susan leaves to the bedroom.)

GEORGE: (forcing a laugh) Hu, ha.

(When Susan has gone, George picks up the phone and dials, keeping an eye open in case Susan returns.)

[George's Apartment/Elaine's Apartment]

ELAINE (O.C.): Hello.

GEORGE: (whispering) Elaine, it's me, George.

(Elaine is sitting up in bed, reading a newspaper as she speaks to George.)

ELAINE: George. How come you're whispering?

GEORGE: (still quiet) Never mind, never mind. I need you to do me a favour. Uhm, remember what we were talking about at the coffee shop earlier?

ELAINE: No.

GEORGE: (still quiet) Think, a second. You know, your friend was talking about me and, you know...

ELAINE: Ech, George, I have no idea what you're talking about.

GEORGE: (still quiet) The actress.

ELAINE: What?

GEORGE: (shouts) Marisa Tomei!

(Susan hears from the bedroom.)

SUSAN (O.C.): What?

GEORGE: (calling to Susan) Uh, ah, nothing. Nothing.

ELAINE: Oh, yeah. Yeah, what about her?

GEORGE: Well, uhm, I think I'd like to do it.

ELAINE: What? (shocked) What?! George, no way. You're engaged!

GEORGE: A cup of coffee. That, that doesn't mean anything.

ELAINE: Uhh. No George!

GEORGE: Elaine.

ELAINE: Forget it!

(Elaine hangs up her phone in disgust.)

GEORGE: Elaine! (shouts) ELAINE!!

(Susan walks back in on George at this point. George quickly slips the phone under a cushion to conceal it, and goes back to watching My Cousin Vinny, to provide an excuse for his raised voice.)

GEORGE: D'oh... Oh, oh, the judge! I hate this guy!

(Susan looks at him oddly.)

[Jerry's Apartment]

(Jerry is packing some gear into a bag, while Elaine watches and Kramer peers out into the hall through Jerry's peephole.)

ELAINE: I can't believe you're buying your father a car.

JERRY: And, best of all, it's a Cadillac.

ELAINE: (sharp intake of breath) Hoh. A Cadillac! (impressed) Wow.

(Elaine seems to become a lot more interested in Jerry. She moves over to the table where Jerry's packing, and turns the 'flirt' knob to the 'on' position.)

ELAINE: I had no idea you had this kind of money.

JERRY: Ahh, I uh, I don't like to talk about it.

ELAINE: Yeah, I thought you were doing okay. I just didn't think you were in this kind of, you know, uh... position.

JERRY: Ah, it's just money.

ELAINE: (smiling) So, when're you getting back from Florida?

JERRY: Oh, I don't know, play it by ear. Why?

ELAINE: I don't know. (big smile) Just, things seem a little more exciting when you're around. That's all.

JERRY: Are you okay?

ELAINE: (squeaky) Sure, sure.

JERRY: What was that?

(Kramer sees something through the peephole. He motions Jerry and Elaine to quieten down.)

KRAMER: Quiet, quiet.

[Jerry's Apartment/Hall Outside Jerry's Apartment]

(Nick, the cable guy is standing by Kramer's door. George wanders up to Jerry's door and, as he gets there, is addressed.)

NICK: Hey, uh, you know the guy who lives here?

GEORGE: Yeah.

(George moves to open Jerry's door, but the cable guy is still talking.)

NICK: (quietly angry) All morning I been waiting here. All morning.

(Peering through the peephole, Kramer makes a noise of excited satisfaction.)

NICK: (still angry) The guy says he's gonna be home. I show up and all I do is wait. I'm getting pretty sick and tired of it. I'm not gonna put up with it much longer.

(The cable guy stalks away. Inside Jerry's, Kramer turns to speak to Jerry and Elaine.)

KRAMER: I'm loving this.

(Kramer lowers his eye to the peephole again, just as George opens the door. The door cracks Kramer on the forehead with a thump. George enters. Kramer peers round George to check the cable guy's gone.)

GEORGE: Kramer, there's a guy out here that...

KRAMER: Yeah, yeah. I know, I know.

(Kramer exits and crosses the hall to his apartment. George closes the door behind himself, and takes off his coat. He notices Elaine.)

GEORGE: (pointedly to Elaine) Oh. Thanks very much for yesterday, by the way.

ELAINE: Jerry, he want me to fix him up with Marisa Tomei. I am not gonna be a part of this!

GEORGE: (animated) Fixed up? A cup of coffee! A cup of coffee is not a fix up!

ELAINE: You wanna meet her. You wanna see if she likes you?

GEORGE: (defensive) So what? So what if I do?

ELAINE: (shrill) You're engaged!

GEORGE: (worked up) I'm aware! I'm aware!! But this is Marisa Tomei, Elaine. An Oscar winner! How can I live the rest of my life, knowing I coulda been with Marisa Tomei? She said I was just her type! She loves short, stocky, balding funny men!

JERRY: I notice you threw 'stocky' in there.

GEORGE: Yeah, what the hell?!

ELAINE: George! It's cheating.

GEORGE: (adamant) It's not cheating if there's no sex!

ELAINE: Yes it is!

GEORGE: (frustrated) Ahh!

ELAINE: (looking for support) Jerry!

(Jerry is standing by the kitchen counter. He has a stack of bank notes and is counting them. Elaine is mesmerized by the sight of all that currency. She stands by the counter, her eyes locked onto the money.)

JERRY: Uhm, hold, hold on a second. (pauses counting) I'm, I'm sorry, I didn't hear what you said.

(Elaine looks almost dizzy at the prospect of all that cash.)

GEORGE: (pleading) Elaine, c'mon. Would you just make the call? (indicates the phone) Please, make the call.

ELAINE: (distractedly) Yeah, yeah, fine, I'll make...

(Elaine picks up the phone and dials a number.)

GEORGE: (triumph) Alright.

(Jerry resumes his counting, but has lost track of where he was up to. He stands, holding one bill, trying to recall. Elaine has obviously been paying more attention, even as she dialled the phone.)

ELAINE: Eight-fifty.

JERRY: Oh, right.

(Elaine waits for the phone to be picked up.)

ELAINE: So uh, how are you getting to the airport? D'you need a ride, or...

JERRY: No, don't be silly, I've arranged a car.

(As Jerry goes to leave the kitchen, Elaine is sure to keep in his path, smiling flirtatiously.)

ELAINE: You sure? You sure? 'Cos, you know...

JERRY: Maybe I'll let you pick me up.

ELAINE: (big smile) Okay.

(Jerry is finally allowed out of the kitchen by Elaine, and heads for the bedroom. The phone is picked up at the other end of Elaine's line. As Elaine speaks, George stands beside her, doing a little selection of dances.)

ELAINE: Oh, hi. Uhm, is Katy there? (listens) Wh...? Uhm, what? (listens) Oh, really?

(Something about the tone of her voice causes George to cease dancing.

ELAINE: Well, could you tell her Elaine called? Yeah, thanks.

(Elaine turns to an expectant George.)

ELAINE: Huh, she's in the hospital. (concerned) She has an arrhythmia.

GEORGE: (impatient) What about Marisa Tomei?!

[Morty and Helen's, Florida]

(Morty is eating a cookie at the kitchen table. Helen comes over and grabs the box he's eating from.)

HELEN: Morty, what d'you have to open this box for? (waving at another box) There's already a box of cookies open.

MORTY: I wanted a Chip Ahoy.

HELEN: I don't like all these open boxes.

(Helen puts both boxes of cookies away in a cupboard.)

MORTY: Look, I got a few good years left. If I want a Chip Ahoy, I'm having it.

(The door opens and Jerry enters, carrying his bags.)

JERRY: Surprise!

MORTY: (shout) Jerry!

HELEN: (shock) Oh, my god!

MORTY: What the hell are you doing here?

JERRY: I'm a good son.

HELEN: Just like that? No calls?

JERRY: Just like that.

MORTY: Boy, you are really something.

HELEN: To what do we owe this great honour?

JERRY: You wanna know? Come on outside.

MORTY: Outside? What's going on?

HELEN: Whenever Jerry comes, something exciting happens.

(Helen darts out of the door eagerly.)

MORTY: (laughs) Heh hah!

JERRY: C'mon, c'mon.

(Morty follows Helen outside.)

[Outside Morty and Helen's, Florida]

(Standing at the kerb is a brand new, silver, Cadillac, gleaming in the Floridian sunshine. Jerry leads Morty and Helen over to it.)

JERRY: Well, what d'you think?

MORTY: Look at this! Look at this! (excited) You bought a Cadillac?!

JERRY: I bought it for you. It's yours.

(Morty and Helen both look astonished.)

MORTY: You what? You bought me a Cadillac?

JERRY: I bought you a Cadillac. (hands over the keys) Here you go.

HELEN: (sharply, to Jerry) Are you out of your mind?

JERRY: What?

MORTY: (to Helen) You don't want it? Are you kidding?

HELEN: He's not buying us a Cadillac.

MORTY: What are you, nuts?

HELEN: It's a very nice gesture Jerry, but take it back.

(Helen wrests the keys from Morty's grasp and hands them back to Jerry.)

MORTY: (to Jerry) Can you believe this?!

HELEN: I'm not letting him buy us a Cadillac. He hasn't got that kind of money.

JERRY: How d'you know?

HELEN: Oh, get out of here Mister Big Shot.

JERRY: Why can't I buy my father a car?

HELEN: Your father doesn't need a car.

MORTY: Yes, I do!

(Morty grabs the keys back from Jerry.)

HELEN: Oh, Morty.

MORTY: We're keeping it.

HELEN: Over my dead body.

(Helen makes another grab for the keys, and she and Morty struggle for possession, watched by Jerry.)

JERRY: (under his breath) Well, this worked out just as I had hoped.

[Kramer's Apartment]

(Kramer sits on his couch reading a newspaper. Beside him, on a table, are his phone and an answering machine. The phone rings, but Kramer doesn't answer, instead allowing the machine to pick up.)

MACHINE (KRAMER): Giddyup.

NICK (O.C.): Yeah, this is Nick Stevens, from Plaza Cable.

(Kramer smiles on hearing the voice.)

NICK (O.C.): Well, I waited all morning again. You said you were gonna be there from nine to one.

(Kramer puts down his paper and stretches out on the couch to listen.)

NICK (O.C.): I was there. Where were you? You think I got nothing better to do than stand outside all morning, waiting for you to show up?!

(Kramer waves his fist, mocking the cable guy's anger. He laughs to himself.)

NICK (O.C.): You're not gonna get away with this!

[Outside Morty and Helen's, Florida]

(Morty is sitting in the Cadillac, with Jerry leaning on the door. They're going through the features of the car.)

MORTY: Hey Jerry, look at this. My seat's got a memory, in case somebody moves it. I could be in prison for five years. I come out, my seat goes right back to where I like it.

JERRY: That's what I was thinking.

(Jack Klompus approaches the car.)

JACK: (pointing) Hello, Jerry.

JERRY: Hiya, Jack.

(Morty gets out of the car, shuts the door and leans against it.)

MORTY: (indicating) So, how d'you like this?

JACK: Who's car?

MORTY: It's mine.

JACK: Yours?

MORTY: That's right. My son bought it for me.

JACK: He what?

MORTY: My son bought me the car. It's a present.

JACK: (disbelief) You bought it?

JERRY: That's right. I bought it.

MORTY: You ever see one so nice?

JACK: Some car.

MORTY: You wanna take a ride?

JACK: No, thank you.

MORTY: C'mon, take a ride.

JACK: I don't wanna take a ride.

MORTY: Why not?

JACK: I don't feel like taking a ride. Do I have to take a ride?!

JERRY: He doesn't wanna take a ride.

MORTY: Uh huh.

JACK: (worked up) What d'you think? I've never ridden in a Cadillac before? Believe me, I've ridden in a Cadillac hundreds of times. Thousands.

MORTY: (skeptical) Thousands?!

JACK: What? D'you think you're such a big shot now, because you got a Cadillac?

MORTY: (dismissive) Ahh!

JACK: (dismissive) Yaah!

(Jack angrily stalks away.)

MORTY: Could you believe that guy?

JERRY: Aahh!

[Hospital Room]

(George sits, talking enthusiastically to someone.)

GEORGE: Anyway, I was thinking about what you said. About uh, me and Marisa.

(The person he's talking to is Katy, who's unconscious, wearing an oxygen mask and has an intravenous drip in her.)

GEORGE: You know, about the uh, two of us getting together. And I know that I said I was engaged, but (laughs) uh, you know, it's really just something you say. It's like, going steady. You know uh, going steady, engaged, it's, it's all just stuff you say. (chuckles) Anyway, I was watching uh, My Cousin Vinny, on the uh, on the tape the other day, and I was thinking that uh, you know, the two of us might take a meeting, as they say. (chuckles) So, what d'you, what d'you think?

(Katy is still unconscious.)

GEORGE: (urgent) Move a pinkie, if it's yes. Can you move a pinkie?

[Kramer's Apartment]

(Kramer is reading a magazine on the couch. The phone rings and he answers it.)

KRAMER: Y'hello.

NICK (O.C.): (inexpertly disguising his voice) Hi, uh ah, Mister Kramer, ah, this is McNab down at the phone company. We've got a report about some trouble on your line.

(Kramer smiles, as he recognizes the voice.)

KRAMER: Hmm. Well, I haven't had any trouble.

NICK (O.C.): Uh, the thing is, we happen to have a man right in your neighborhood.

(Kramer walks over to his window, and uses his fingers to open a couple of blades on his blinds. Peering through, he can see a public phone box across the street. Parked beside the box is a truck emblazoned with the Plaza Cable insignia, and in the box is the familiar figure of Nick, the cable guy.)

KRAMER: (playing along) Ohh, is that right? Well, I have been having a little trouble with my call-waiting.

NICK: We can fix that. We can fix that.

KRAMER: (sitting back on the couch) Oh, can you? That's funny, because, as it happens, I don't have call-waiting. Seems to me that the phone company would know that.

NICK: Oh, right. (laughing it off) I'm sorry, I was looking at the wrong work order.

KRAMER: Yeah. Could you hold on for a second? I've got something on the stove.

(Kramer puts the phone down beside a radio and switches it on. It plays cheesy music into the phone. Kramer gets up and heads out the door.)

[Street]

(Nick, the cable guy, stands in the phone box. He's listening to the cheesy music on the phone, and getting impatient. He glances at his watch. Suddenly, a car horn blares and a shout is heard.)

KRAMER: Hey, McNab!

(The cable guy turns, to see Kramer leaning out of the window of a cab as it passes.)

KRAMER: &quot;Chunnel's&quot; on HBO tonight. Why don't you stop by?

(Kramer waves and smiles happily at the dumbstruck cable guy, as the cab accelerates away.)

[Tenant's Committee, Florida]

(Around a table sit a number of residents, including an unhappy looking Jack Klompus. Morty is standing behind a lectern addressing the gathering. He bangs a gavel to get their attention.)

MORTY: Alright. Next thing on the agenda, the restoration of the fence at the Briarwood gate. At the present time, we are still accepting bids.

HERB: C'mon Morty, it's been broken for six months already. What the hell are you doing?

MORTY: Well, I, as your president, have to find the best price, and right now, I for one, do not think it's cost-feasible.

JACK: (half to himself) I'll bet you don't.

MORTY: What was that?

JACK: C'mon Morty, the jig is up!

MORTY: What're you talking about?

JACK: I'm sorry. I'm sitting here, the whole meeting, holding my tongue. I've known you a long time, Morty, but I cannot hold it in any longer.

HERB: What's going on, Jack?

JACK: I'll tell you what's going on. Morty Seinfeld has been stealing funds from the treasury.

MORTY: Stealing?!

RALPH: What proof do you have?

JACK: Proof? You want proof? He's driving around in a brand new Cadillac. Now what more proof do you want?

MORTY: My son bought me that car!

JACK: Your son?

MORTY: Yeah.

JACK: Your son could never afford that car. We all saw his act, last year, at the playhouse. He's lucky he can pay his rent!

HERB: Jack's right! He stinks!

RALPH: It's his material.

MORTY: I tell you, he bought it for me!

HERB: (bangs table with his fist) I move for a full investigation!

RALPH: I second.

(Morty looks dumbstruck.)

[George's Apartment]

(George sits on the couch, watching Only You on the video. Onscreen, the ever-lovely Marisa is delivering her lines.)

MARISA: ...go introduce myself.

(Susan enters, with a bag of groceries.)

SUSAN: Hey! What're you watching? (looks at TV) Only You? That's another Marisa Tomei movie, and you've seen that one too. (jokingly) What, d'you have a thing for her?

GEORGE: (laughing it off and trying too hard) Yeah, yeah. I have a thing for Marisa Tomei. Like she would ever go out with a short, stocky, bald man. (forced laughter) Hu hu, ha ha. Like that's her type. Huh. She's an Oscar winner. (nervous laughter) He heh. Besides, I don't even know her. It's not like anyone's trying to fix us up. Who, who would try and fix me up with Marisa Tomei?

SUSAN: What are you talking about?

(Susan wanders away into the kitchen with the groceries, looking bemused by George. George watches her go, staring at the kitchen door as he slips off into a daydream.)

[George Dream Sequence]

(George stares at the kitchen door, which glows with golden light. Then Marisa Tomei slinks out of the kitchen in a long black dress, and her hair up. She comes and sits beside George on the couch, looking at him lovingly.)

MARISA: Have I told you how much I love you today?

GEORGE: Not in the last fifteen minutes.

MARISA: Well, I do love you very much.

GEORGE: And I love you, Marisa.

MARISA: Well then, c'mon, get dressed. We're going to be late for the premiere.

(George leans across and takes Marisa in his arms. They kiss and slowly recline onto the couch.)

[George's Apartment]

(Susan returns from the kitchen, to find George kissing, and slowly reclining onto the couch with, a cushion. George runs a hand across his head, in the fashion of a loving Marisa's caress. Then he raises his head from the cushion to see Susan observing him. She turns and goes back into the kitchen, wordlessly. George puts the cushion on the arm of the couch and lies down.)

[Morty and Helen's, Florida]

(Jerry sits on the couch and Helen is in the kitchen, as an anxious Morty delivers his news.)

HELEN: Impeachment?

MORTY: That's right. Have you ever heard of anything like that?

HELEN: Can they do that?

MORTY: If they get the votes.

HELEN: I told you we shouldn't have let him give us the car.

JERRY: Didn't you tell 'em I got the bill of sale? That proves I paid for it.

MORTY: It doesn't make a difference. They think we're in cahoots.

JERRY: You know, you could put a fence around these condos, and call it an insane asylum. Nobody would know the difference!

MORTY: No-one's ever been impeached before. I couldn't live here. We'd have to move to Boca.

(There's a knock at the door, and then it is opened and Evelyn walks in.)

EVELYN: Hello?

JERRY: Oh, hi Evelyn.

(Morty sits by the table.)

EVELYN: Hello, Jerry. (to the senior Seinfelds) I just got off the phone with Saul Brandus.

HELEN: What'd he say?

EVELYN: He's voting to impeach.

(Morty thumps the table with his fist.)

EVELYN: Not because he think you stole the money, but mainly because you never thanked him for giving you his aisle seat at Freddy Roman's show.

MORTY: I did so thank him!

HELEN: (admonishing) No, he never heard you. (prods Morty's back) I told you he didn't hear you.

MORTY: Ah, he's deaf anyway.

EVELYN: Now, my sources tell me, there's three votes for impeachment, three votes against impeachment, and one undecided.

MORTY: Who's that?

EVELYN: Mrs. Choate.

JERRY: Who's she?

MORTY: Oh, that one. She's been a member of the board longer than anybody. She's very tough to deal with.

HELEN: Maybe we should have her over for coffee, and explain our side of it?

MORTY: That's a good idea.

EVELYN: Okay. I'll see you at the Lichtenberg's, tonight.

HELEN: (surprise) The Lichtenberg's?

EVELYN: Yes, they're having a party.

HELEN: We weren't invited.

EVELYN: Oh. Probably they think you're too good for them. You know, because of the car.

(Jerry puts his face in his hands.)

The End part 1

[Montage]

JERRY (V.O.): Last week, on Seinfeld.

(A selection of scenes from The Cadillac (1) establish the story so far.)

[Morty and Helen's, Florida]

(Jerry is trying, unsuccessfully, to sleep on the infamous fold-out. He twists and turns, but can't find a comfortable position.)

JERRY (V.O.): Ow. Stupid fold-out! Why'd they put the bar in the middle of the bed?

(Jerry continues to thrash about in discomfort. The phone rings and Jerry picks it up.)

JERRY: Hello?

(On the other end of the line is Elaine, in bed back in New York.)

ELAINE: (flirtatious) Hi Jerry.

JERRY: Elaine? Wh..what's going on?

(Elaine rests on one elbow, playing with her hair as she chats playfully with Jerry.)

ELAINE: (clears throat) I was thinking, I mean, I'm not really doing that much this weekend, and I thought, well, huh, what the hell, maybe I'll come down there and hang out a little.

JERRY: (a little confused) You wanna hang out here, at phase two of the Pines of Mar Gables?

(Elaine gives a slightly overdone girly giggle-laugh at Jerry's comment.)

ELAINE: Well, it's just two hours by plane...

JERRY: Gee, I dunno what to tell you.

(There is a beep from Elaine's phone.)

ELAINE: Dammit! I got another call. Uh, hang on, don't hang up Jerry.

(Elaine hits a button on her phone to take the other call.)

ELAINE: Yeah?

(The other call is George. He sits on his couch in his dressing gown.)

GEORGE: (urgent whisper) Elaine! You have got to get me Marisa Tomei's phone number!

ELAINE: (impatient) Okay, George, I am on the other line. I promise you, I'll get you her number.

(George punches the air in triumph, and sits back on the couch.)

GEORGE: Yeahhh.

ELAINE: Goodbye.

(Elaine hits the button on her phone again, to reconnect with Jerry.)

ELAINE: (smiling and flirty) Hi.

JERRY: Hi.

ELAINE: So, what d'you think?

JERRY: Uh, I don't think so.

ELAINE: (disappointed) Oh.

JERRY: I'll be back on Monday.

ELAINE: Well, if you need that ride, just uhm, gimme a call. (little laugh) Heh. I can meet you at the gate, Jer.

JERRY: Yeah, yeah. Whatever. Alright, I'll see you.

(Jerry is moving to put down the phone when Elaine speaks.)

ELAINE: (urgent) Jerry? Jerry?

JERRY: (putting the phone back to his ear) Yeah.

ELAINE: (breathy) Bye.

JERRY: Bye.

(Jerry puts down the phone, still a little bemused by Elaine.)

[Kramer's Apartment]

(The phone rings and Kramer picks up.)

KRAMER: Hello.

(Somewhere outside, a guy in overalls and a safety helmet is on the phone. He's writing on a clipboard as he speaks to Kramer.)

JOHN: Hello, Mister Kramer? This is John Hanaran, from Con Ed.

(Kramer gets a smile on his face, thinking it's another of the cable guy's tricks.)

KRAMER: Ohh, it's Hanaran now, is it?

JOHN (V.O.): Yeah. We've had some reports of power surges in your building. It seems some jokers were up on the roof, and they must've damaged some of the wires.

KRAMER: Oh, you don't say.

(Kramer walks to his window as he listens.)

JOHN: (a little confused by Kramer) Yeah. Either way, we need to get into your apartment and do a safety check.

(Kramer opens his blinds slightly, as he did before. But there is no cable guy in the phone box across the street, only a woman talking on the phone.)

KRAMER: (impressed) Ohh, you're good. You are really good.

JOHN: (confused) What're you talking about?

(Kramer hits a button to end the call, and puts the phone back on the table. John, the power guy, stares at his phone in disbelief.)

[Monk's]

(Elaine sits in a booth. George enters, looking unnaturally happy, and comes over.)

GEORGE: Marisa Tomei! I, just spoke to Marisa Tomei! (sitting opposite Elaine) And I wasn't even that nervous.

(Elaine watches George, as he rambles on.)

GEORGE: You know, I can't remember the last time I called a woman without being nervous. I, usually, I'm pacing all over the room, I'm...

ELAINE: (looking at her watch) Okay, well that's all the time we have for today. Why don't we pick up with this next week?

(Elaine rises.)

GEORGE: Hey, where you going?

ELAINE: I got stuff to do.

GEORGE: What? You can't leave yet.

ELAINE: Why not?

GEORGE: We have to discuss my alibi.

ELAINE: Alibi? What does that have to do with me?

GEORGE: I usually spend Saturday afternoons with Susan. She's gonna want to know what I'm doing. I can't use Jerry, he's in Florida.

ELAINE: Oh, so you wanna say you were with me?

GEORGE: Yes.

ELAINE: (pulls a face) Puh. Okay, fine. You were with me.

(Elaine sets off to leave, but is stopped by George.)

GEORGE: Wait a second, wait. Why are we together?

ELAINE: What is the difference?

GEORGE: Because, if you ever see her and it comes up, we have to be in sync. Hmm?

(Reluctantly, Elaine sits back down with a sigh, as George furrows his brow in concentration.)

GEORGE: Okay. Now, why do I have to see (points) you?

(George and Elaine both think hard for a moment. Then Elaine gets a flash of inspiration.)

ELAINE: Ah! Because, I'm going to the dentist, and I'm afraid, and I want you to go with me.

(Elaine smiles and makes a 'voila' gesture.)

GEORGE: (dismissive) It's no good.

(Elaine looks offended.)

ELAINE: (pointedly) Okay. Fine.

GEORGE: What?

ELAINE: I don't like the way you just rejected my suggestion.

GEORGE: Hey, hey, let's not get so defensive here. This is a give and take process.

ELAINE: I thought that my suggestion was good. And, I think you could've been a little more tactful.

GEORGE: (explaining, with lots of hand gestures) Okay, look. We've never worked together on a lie. Now, you don't understand how I work. I have a certain way of working. Jerry and I have worked together a few times. He knows how I work. It's not a personal thing, y'know? We're just trying to come up with the best possible lie. That's what this is all about.

ELAINE: (weary) Okay.

GEORGE: Alright?

ELAINE: Okay.

GEORGE: Okay.

ELAINE: Fine.

GEORGE: Good.

ELAINE: Fine, fine, fine.

(The twosome go back to thinking for a while. This time George gets the inspiration.)

GEORGE: Okay. How about this?

ELAINE: What?

GEORGE: (pleased with himself) You are having problems with your boyfriend and I am meeting you to discuss the situation.

ELAINE: I don't have a boyfriend.

GEORGE: (still pleased) She doesn't know that. We say that you do.

ELAINE: (not convinced) Ech.

GEORGE: It's good. Believe me.

ELAINE: I thought my idea was just as good.

GEORGE: The dentist thing?

ELAINE: (pointed) Yeah, right. The dentist thing.

GEORGE: Alright, the dentist thing was not good.

ELAINE: Okay, alright. What's his name? Who is he?

GEORGE: (after a moment's thought) Art Vandelay.

ELAINE: (incredulity) Art Vandelay? This is my boyfriend?

GEORGE: That's your boyfriend.

ELAINE: What does he do?

GEORGE: He's an importer.

ELAINE: Just imports? No exports?

GEORGE: (getting irritated) He's an importer-exporter. Okay?

ELAINE: Okay. So, I'm dating Art Vandelay. What is the problem we're discussing?

GEORGE: (thoughtful) Yes. Yes.

ELAINE: (sighs) Yi-yi-yi.

(Elaine and George go into another bout of deep thought.)

ELAINE: Ah! (explaining, with hand gestures) How 'bout this? How about, he's thinking of quitting the exporting, and just focussing in on the importing. And this is causing a problem, because, why not do both?

(Elaine finishes with an expectant smile. George looks skeptical, and Elaine seems offended again.)

ELAINE: (irked) Oh, what? You don't like that suggestion either?

GEORGE: It's very complicated.

ELAINE: (definitely irritated) You know, it seems to me that it's all you, and none of my ideas are getting in. You know, I mean, you just know it all and I am Miss Stupid. Right?

(George avoids Elaine's gaze, looking down.)

[Morty and Helen's, Florida]

(Helen is in the kitchen, putting cookies on a plate, as a restless Morty fusses around.)

MORTY: What're you doing? Are you making coffee?

HELEN: Yeah.

MORTY: Well, maybe you better make a pot of tea, too.

HELEN: Morty, you're driving me crazy.

MORTY: Look, I don't want anything to go wrong. If this woman votes to impeach me, I'll be a laughing stock.

HELEN: You wanna drive a Cadillac? Expect to pay the consequences.

(The doorbell chimes.)

MORTY: There she is.

(Morty and Helen both go to the door. Morty opens it and Mrs Choate enters.)

MORTY: Hello. Hello, Mrs Choate.

HELEN: Oh, come in, come in. May I take your coat.

MRS. CHOATE: No, no. I prefer to wear it. Nobody's taking my coat.

HELEN: Ah, how 'bout a cup of coffee or something?

MRS. CHOATE: Coffee? Ach. I'll take hot water with lemon, if you have it.

HELEN: I'll see. (indicating the couch) Have a seat. That's a lovely scarf you're wearing. Where did you get it?

(Helen goes to the kitchen as Mrs Choate sits on the couch.)

MRS. CHOATE: Ahh, they're a dime a dozen.

(Jerry enters.)

HELEN: Oh, hi Jerry. Mrs Choate, (indicating) this is my son, Jerry.

(Mrs Choate turns to look at Jerry. Jerry looks astonished as he recognizes Mrs Choate. He remembers an incident from &quot;The Rye&quot;.)

[New York Street, &quot;The Rye&quot;]

(Jerry grapples with Mrs Choate, for possession of a bag.

JERRY: Gimme that rye!)

MRS. CHOATE: Stop it. Let go. Help! Someone, help!

(Jerry wrests the bag from the old lady's grip.)

JERRY: Shut up, you old bag!

(Jerry races away down the street with the rye.)

MRS. CHOATE: (shouting after him) Thief! Stop him! Stop him, he's got my rye!

[Morty and Helen's, Florida]

(Jerry manages to regain a normal expression.)

JERRY: Nice to meet you.

MRS. CHOATE: Hello.

MORTY: (holding Jerry's shoulders) Jerry lives in New York. You just came from New York, didn't you?

(Jerry looks desperate. The last thing he needs is Mrs Choate connecting him with New York.)

MRS. CHOATE: Yeah. I was visiting my daughter, and I'll never go back. The crime there is just terrible. Do you know I got mugged for a marble rye, right on the street?

(Jerry gives a look of sympathy. Helen brings a couple of saucers and cups from the kitchen, placing them on the table before Mrs Choate.)

HELEN: (sympathy) Oh, that's terrible. They stole a rye? Why would they steal a rye?

MORTY: That's what the city's turning into. They'll steal anything.

MRS. CHOATE: They're like savages.

JERRY: Yeah, there's some sickos out there.

MRS. CHOATE: (peering at Jerry) You look very familiar. Have we ever met?

JERRY: You ever go to Camp Tiyoga?

HELEN: Maybe you've seen him on television.

MORTY: Jerry's a comedian.

MRS. CHOATE: Naw, I don't watch TV.

(Jerry has moved rapidly to the door.)

JERRY: (opening the door) Well, it was nice meeting you.

MORTY: Jerry, don't go.

JERRY: Ah, I think I'll go.

(Jerry darts out and shuts the door behind him.)

MRS. CHOATE: So, Morty, what's this all about? What d'you want?

[George's Apartment]

(Susan sits on the couch, reading. She spots George, in his coat, heading for the door.)

SUSAN: Hey, where're you going?

GEORGE: Wha..? I didn't tell you? I gotta go meet Elaine.

SUSAN: Elaine? What for?

GEORGE: I don't know. She..She's having some problems with this guy she's seeing.

SUSAN: I didn't even know she was dating anyone.

GEORGE: Yeah, yeah. She's seeing this guy, Art Vandelay.

SUSAN: So what does he do?

GEORGE: He's an importer-exporter.

SUSAN: What kind of problems are they having?

GEORGE: (not happy delivering Elaine's lie) Well, he uh, he wants to uh, quit the exporting and uh, focus just on the importing. And it's a problem, because she thinks the exporting is as important as the importing.

(Susan looks unconvinced.)

SUSAN: Are you having an affair with Elaine?

GEORGE: (trying to laugh it off) Right. C'mon! I'm having an affair with Elaine?! If I was having an affair with Elaine, I wouldn't tell you I'm going to see Elaine. I would make up some other person to tell you I was gonna go see, and then I would go see Ela..Elaine.

SUSAN: Huh?

(George gives a smile and slides out the door.)

[Street]

(Kramer strolls along the street, carrying a big bag of groceries. He becomes aware that a vehicle is slowly driving along the road just behind him. Kramer shifts his load of groceries, so that a shiny baking tray is in a position he can use it as a mirror. Behind him, he can see the blurry image of a Plaza Cable truck. Kramer begins to walk faster, then breaks into a run.)

[Another Street]

(Kramer slows from his run, and looks behind himself. A smile comes to his face, as he sees the truck is no longer there. He allows himself a little shrug of satisfaction at having beaten the cable guy. Kramer turns and begins to walk on, with his groceries, when there is a screech of tyres and the cable truck comes to a halt in front of him - having taken another route to head him off.)

(The cable guy gives a 'come here' gesture with his forefinger through the truck window. Kramer turns and sets off running. He runs into a litter bin, drops his groceries and falls to the floor. The cable guy climbs from his truck and begins to advance on Kramer. Kramer picks himself up and, abandoning his groceries, sets off running again, with the cable guy in pursuit.)

[Variety of New York locations]

(Kramer runs across a busy thoroughfare, followed, seconds later, by the cable guy.)

(Kramer hauls himself over a wall in a park, looks around, and then sets off running. Moments later, the cable guy struggles over the same wall and sets off after Kramer.)

(Kramer runs across an expanse of grass in the park, with the cable guy just a few yards behind.)

(Kramer runs across a New York rooftop. He comes to the edge of the roof and looks for a way across to the next building. Kramer looks worriedly behind him, for the pursuing cable guy, then down into the gap between buildings. Despite his worry at the height, he steps back a few paces, takes a run-up, and leaps across to land on the roof of the next building. Kramer picks himself up and sets off running again. Behind him, the cable guy comes to the same gap, looks down at how far he has to fall, and thinks better of attempting the leap.)

NICK: (yelling after the fleeing Kramer) I'll get you! I'll get you Kramer! You won't get away with this!

[Morty and Helen's, Florida]

(Morty is fastening a jacket as he enters.)

MORTY: Alright, are you ready to eat?

HELEN: (glancing at her watch) Oh, right, let's go. Jerry, let's go, it's time to eat. We're going to dinner.

(Jerry wanders into the room. He's in a t-shirt and sweatpants, and holding a comic book he's been reading.)

JERRY: (confused) Dinner? W..What time is it?

HELEN: (pulling on a coat) It's four-thirty.

JERRY: (bewildered) Four-thirty? Who eats dinner at four-thirty?

MORTY: By the time we sit down, it'll be quarter to five.

JERRY: I don't understand why we have to eat now.

HELEN: We gotta catch the early-bird. It's only between four-thirty and six.

MORTY: Yeah. They give you a tenderloin, a salad and a baked potato, for four-ninety-five. You know what that cost you after six?

JERRY: Can't we eat at a decent hour? I'll treat, okay?

HELEN: You're not buying us dinner.

JERRY: (emphatic) I'm not force-feeding myself a steak at four-thirty to save a coupla bucks, I'll tell you that!

HELEN: Alright, (sitting on the couch) we'll wait. (pointedly) But it's unheard of.

(Jerry shakes his head, incredulous, and wanders away with his comic book.)

[Park]

(George sits on a bench with Marisa Tomei. He's mid-speech, and Marisa's looking interested, smiling and laughing.)

GEORGE: ...So, anyway, if you think about it, manure is not really that bad a word. I mean, it's 'newer', which is good, and a 'ma' in front of it, which is also good. Ma-newer , right?

MARISA: (laughing) You're so right. I never thought of it like that. Manure. 'Ma' and the 'newer'.

(Marisa laughs and George is smiling happily.)

MARISA: Did you just make that up?

GEORGE: What, you think I'm doing material here?

MARISA: (laughs) No, no. It's hard to believe anyone could be so spontaneously funny.

GEORGE: (modest) And I'm a little tired.

(Marisa laughs again, then speaks, still smiling, but more seriously.)

MARISA: So, tell me, how is it that a man like you, so bald, and so quirky and funny, how is it you're not taken?

GEORGE: Well, Marisa. See, the thing is... I'm sort of engaged.

(Marisa's face falls in disappointment.)

MARISA: What?

GEORGE: I'm, you know, engaged.

(Marisa's expression turns to anger. She swings a fist and punches George in the face, then grabs her bag and storms away. George feels the painful spot where he received the blow.)

[Outside Scott's, Florida]

(A few people stand around outside. The Seinfeld's Cadillac drives up to the front of the restaurant. There's a gap in the parked cars, right by the entrance.)

JERRY (V.O): Hey look, there's a spot right in front.

MORTY(V.O.): Always, Jerry. Always.

(The Cadillac pulls into the space. From the entrance of Scott's emerge Jack and Doris Klompus, Jack using a toothpick)

DORIS: Was that delicious or what?

JACK: Where you gonna get a better meal than that?

DORIS: Better than Danny's.

JACK: Danny's? (scoffs) C'mon!

(The Seinfelds walk toward the entrance, where they meet the Klompus duo.)

MORTY: Hello Jack.

HELEN: Doris.

DORIS: Hello, hello.

JERRY: Jack.

DORIS: Hello, Jerry.

JERRY: Hi Doris.

JACK: Hello Morty. (looking at watch) Well, missed the early bird.

MORTY: Yeah, so?

JACK: (pointed) Must be nice to have that kind of money.

(Jack calls over to one of the people outside the restaurant.)

JACK: Bernie, look who's eating at six o'clock. (pointed) Your suddenly well-to-do president. But, you enjoy your last meal in office. Tomorrow, they kick you out, you'll have plenty of time to drive around in your Cadillac.

(Morty is looking confident, even smug.)

MORTY: They're not kicking me out. You don't have the votes.

JACK: That's what you think.

MORTY: We'll see.

JACK: Yes, we will.

JERRY: Awright, let's eat already.

(The Seinfelds enter Scott's, and Jack and Doris walk past the Cadillac.)

[Elaine's Apartment]

(Elaine answers a knock at the door, to find a slightly anxious looking Susan.)

ELAINE: Hi, Susan.

SUSAN: Hi.

ELAINE: Hi. Come in, come in. Have a seat.

(Susan walks in, and crosses to the couch. Elaine closes the door.)

SUSAN: (standing) Uh, Elaine, I have to ask you a question.

ELAINE: Oh, sure.

SUSAN: (dead serious) Are you having an affair with George?

ELAINE: (disbelief) Wha...?! (uncontrolled laughter) Ha ha ha. Ha, no.

(Elaine has to sit on the arm of a chair, she's laughing so hard.)

ELAINE: (laughter) Don't be ridiculous! (laughter) I mean, why would anyone wanna sleep...

(Elaine realizes who she's speaking to, and the laughter dies away.)

ELAINE: ...Well, obviously... You know... (disbelief) Ap.. ap, Chu... Why would you think I was having an affair with George?

SUSAN: Ohh, because he said that he had to talk with you earlier about some problem that you were having.

ELAINE: (recalling the arranged lie) Yeah, yeah, I did have to talk with him. I definitely had to talk with him. Having a problem with my boyfriend.

SUSAN: Art Vandelay?

ELAINE: Yeah, Art Vandelay.

SUSAN: I'm sorry. (laughing it off) I feel like an idiot.

ELAINE: (smiling) No, no. (laughing) Huh-hu-hu. It's okay.

SUSAN: Oh, just forget that we ever talked. Okay?

ELAINE: It is so forgotten.

SUSAN: Alright.

(Susan moves toward the door.)

ELAINE: Okay. (relieved) Okay, no problem.

(Susan has the door open, when a thought occurs to her.)

SUSAN: So, was George helpful at all?

ELAINE: (unconvincing) Yeah. Oh, yes, yes, he was very helpful. (hesitant) Uhm, because, you know, Art and I were getting into this whole thing about his business. Uhm, you know he's an importer-exporter.

SUSAN: (not convinced) Yeah.

ELAINE: (explaining, with gestures) Uhm, George felt that I was too adamant in my stand that Art should focus on the exporting and forget about the importing.

SUSAN: (spotting an inconsistency) Wait a minute. I thought that Art wanted to give up the exporting.

ELAINE: What'd I say?

SUSAN: The importing.

ELAINE: (caught out) I did. Uh...

SUSAN: So, what does he uh, import?

ELAINE: (extemporising) Uh... chips.

SUSAN: Oh. What kinda chips?

ELAINE: Potato.

SUSAN: Ah.

ELAINE: (embroidering) Some corn.

SUSAN: And what does he export?

ELAINE: Diapers.

SUSAN: (fake smile) I'm sorry for bothering you.

ELAINE: Oh, no, it's okay.

(Susan and Elaine both use fake laughs, as Susan leaves and closes the door.)

ELAINE: Okay.

(The moment the door closes behind Susan, Elaine races to her phone and dials a number hurriedly. She stands, listening to the ring, desperate for a response.)

ELAINE: (to herself) C'mon, George, pick up. Oh, pick up. Oh, pick up.

[George's Apartment]

(Susan stands sifting through some magazines, when the door opens and George enters. He's still feeling his face where Marisa punched him. )

GEORGE: Hi.

SUSAN: Hi. (pointedly) So, George, what does Art Vandelay import?

(George looks surprised by the question, and thinks for a moment before replying.)

GEORGE: Matches? Long matches.

(Susan raises her arm, and delivers the second punch that George has received today. George is thrown backwards out of the door by the blow, and Susan slams the door behind him.)

[Tenant's Committee, Florida]

(Around the table are the board of phase two. Morty and Jack sit beside each other. Herb is chairing the meeting, and several other representatives are present, some chatting with each other. Herb spots Mrs Choate coming in through the door.)

HERB: ...There she is. Okay, if I can have your attention for a minute here. We're calling this emergency meeting of the board of phase two, to consider a motion of impeachment of our president, (indicating) Morty Seinfeld.

(Mrs Choate sits beside Morty. )

MORTY: Nervous, Jack?

JACK: What for?

MORTY: Because I have the votes.

Morty turns to Mrs Choate.

MORTY: Nice to see you, Mrs Choate.

MRS. CHOATE: Hello, Morty.

(Jack looks surprised at the friendliness between Morty and Mrs Choate.)

HERB: Building A. Are you for, or against, the motion to impeach?

RALPH: What does that mean?

HERB: It means, if you're for the motion, you're against Morty.

RALPH: So why don't you say that?

HERB: Hey, I'm running the meeting.

RALPH: If you think so.

HERB: Building A?

BUILDING A: For impeachment.

HERB: Building B?

BUILDING B: Against impeachment.

HERB: Building C?

BUILDING C: For impeachment.

HERB: Building D?

MRS. CHOATE: Against impeachment.

JACK: (a whisper to Morty) I can't believe you got that old bag.

(Mrs Choate overhears Jack's comment, and it stirs a memory. She thinks back.)

[New York Street, &quot;The Rye&quot;]

(Jerry and Mrs. Choate struggle for possession of a bag containing a marble rye. )

MRS. CHOATE: Help! Someone help!

JERRY: Shut up, you old bag!

(Jerry wrests the bag from Mrs. Choate and takes off down the street. )

MRS. CHOATE: (after Jerry) Oh, thief! Thief!

[Tenant's Committee, Florida]

(Mrs. Choate wears a look of shocked realization. She stands and addresses Morty. )

MRS. CHOATE: It's him. (pointing at Morty) It's your son. Now I know where I saw him. He stole my marble rye.

MORTY: My son never stole anything. He's a good boy.

RALPH: They should lock him up.

MRS. CHOATE: (pointing at Morty) Like father, like son. (thumps table) I change my vote. I vote to impeach!

BUILDING B: Me too. I change my vote.

HERB: All those in favour, say aye.

ALL (EXCEPT MORTY): Aye.

HERB: All opposed.

(Morty forlornly raises his hand. )

HERB: The ayes have it. The motion passes.

(Jack smiles a big smug smile of triumph, aimed at Morty.)

HERB: Morty Seinfeld, you are officially dismissed as condo president. As vice-president, Jack Klompus, in accordance with the phase two constitution, is hereby installed as president.

(Jack stands to acknowledge the honour. )

HERB: Hear, hear, Jack.

(Jack raises a hand in salute, as the rest of the board applaud him. A disgruntled Morty remains silent. )

[Hall Outside Jerry's Apartment]

(A weary looking Nick, the cable guy, leans against Kramer's door. He knocks and there's no response. )

NICK: (weary) Alright, I know you're in there. I know you can hear me. You win, okay? You win. I can't do it any more. What d'you want from me? Apology? Alright, I'm sorry. There, I said it, I'm sorry, I'm sorry. I see now how we made you feel when we made you sit home waiting. I dunno why we do it. (upset) I guess maybe we just kind of enjoy taking advantage of people. (reasonable) Well, that's gonna change. From now on, no more 'nine to twelve', no more 'one to five'. We're gonna have appointments. Eleven o'clock is gonna mean eleven o'clock. And, if we can't make it, we're gonna call you, tell you why. (worked up) For god's sakes, if a doctor can do it, why can't we? (almost sobbing) Anyway, that's it.

(The cable guy sighs, and begins to walk away. Kramer's door opens, and Kramer emerges. He and Nick look at each other apologetically for a few seconds. Then Kramer steps forward and they hug. )

[Morty and Helen's, Florida]

(Morty comes in from the bedroom, looking around at his home. Helen holds his coat as he slips into it. She adjusts his collar and hands him a blue hat. Jerry enters through the front door. )

JERRY: The bags are in the car, I guess we better go.

(Jerry leads the way, followed by Helen, who folds a coat over her arm. Morty waits a second or two, having a final look round the room. He stands by the door, then steels himself. Giving a determined nod, he exits, closing the door behind him.)

[Outside Morty and Helen's, Florida]

(The Seinfelds walk, three abreast, toward the Cadillac. As they walk, they pass the other residents, who have come to see them off. They nod acknowledgement. As they get to the car, Helen and Jerry get in, but Morty stands in the open door of the Cadillac, turns to the assembled throng and gives a big smile, and arms-wide gesture (like Nixon leaving the White House), before climbing into the car. The residents wave back, one or two of them in tears, and Morty slowly drives the Cadillac away.)

The End
