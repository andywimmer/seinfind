---
layout: default
title: The Trip
parent: Season 4
nav_order: 1
permalink: /the-trip
cat: ep
series_ep: 41
pc: 401
season: 4
episode: 1
aired: August 12, 1992
written: Larry Charles
directed: Tom Cherones
imdb: http://www.imdb.com/title/tt0697799/
wiki: https://en.wikipedia.org/wiki/The_Trip_(Seinfeld)
---

# The Trip

| Season 4 - Episode 1     | August 12, 1992          |
|:-------------------------|:-------------------------|
| Written by Larry Charles | Directed by Tom Cherones |
| Series Episode 41        | Production Code 401      |

"The Trip" is a two-part story (the 41st and 42nd episodes to air) of NBC's popular sitcom, Seinfeld. Airing on August 12, 1992 and August 19, 1992, it kicked off the fourth season of the show.

## Plot

### Part 1

Jerry is offered two free tickets from New York City to Hollywood to appear on The Tonight Show with Jay Leno. He offers one to George and they decide that while they are in Los Angeles they will track down Kramer, who headed to Los Angeles in the previous episode, "The Keys", to become an actor. A dead woman turns up in another part of LA and Kramer's script he had given to her is found on her body. George thinks he has insightful conversations with the talk show guests (Corbin Bernsen and George Wendt) but they both call him "some nut" when they appear publicly. Jerry can't remember the wording for a joke and blames the hotel maid, Lupe (Dyana Ortelli), who threw it away while cleaning the room. As Jerry and George leave The Tonight Show, they see Kramer's picture on the news. He is the main suspect for the "Smog Strangler".

### Part 2

Kramer is arrested when he is mistaken for "The Smog Strangler," a serial killer roaming the streets of Los Angeles. While also in L.A., Jerry and George try to help resolve the accusation. They use a pay phone to call the police and they say they have some important information regarding the stranglings. Two policemen in a police cruiser come to pick them up and take them back to the station. On their way, the officers see a man (Clint Howard) trying to break into a car. They arrest him and put him in the back with Jerry and George; Jerry and the man get into an argument about tipping, with the latter insisting that Jerry's tipping habits are too cheap. They have to stop again when they get a police call regarding to the "Smog Strangler" and happen to be close to the scene. Jerry and George want to make sure Kramer is not imprisoned, so they open the door of the car, and, in their hurry, leave the door open. The man who was breaking into the car escapes.

Kramer is taken to the police station and is brutally interrogated by the lieutenant (Peter Murnik), causing Kramer to have a nervous breakdown, which leads to him hysterical sobbing. While he is being questioned, the lieutenant receives a phone call stating that the Smog Strangler has killed another victim while Kramer was in custody, and so he is allowed to leave. After Kramer is exonerated, Jerry and George decide to return to New York, but Kramer opts to remain in Los Angeles. However, by the end of the episode, Kramer has returned to New York and is once again living across the hall from Jerry. He offers no explanation of his return, other than he lives there.

At the end of the episode, it is revealed that the Smog Strangler is suspected to be the man that was in the back seat with George and Jerry, the one they accidentally let escape. It is broadcast on a news program that his whereabouts are unknown, but that he is a generous tipper.

## Production

The scene in which the man breaks into the car was shot near the Bicycle Shack on Ventura Place in Studio City, California, a short distance from CBS Studio Center, the main studio for Seinfeld. When Kramer is confronted by the police at his apartment (about 12 minutes into the episode), Larry David and episode writer Larry Charles can be seen standing in the crowd behind the officers, at the far right of the scene. The cop riding shotgun is the same actor that would later portray Jake Jarmel. The Hotel/Apartment that Kramer is staying in while in Hollywood is in the same building that was used in Pretty Woman, in which Jason Alexander co-starred.

This was the only two-part episode of Seinfeld in which both parts had the same name but were aired on two separate dates instead of a one-hour special. However, "The Wallet"/"The Watch" is a continuation episode pairing which also aired on separate dates with a "To Be Continued" at the end of "The Wallet".

Elaine does not appear in either part of "The Trip", and appears only minimally in "The Pitch" and "The Ticket". This was due to the fact that Julia Louis-Dreyfus was on maternity leave.

Kramer's first name is missing from the script found on the dead woman's body, a reference to how—at this point in time—no one knows his full name.

The episodes were broadcast much earlier than normal, as NBC wanted to cash in on ratings from the 1992
imdb:
Barcelona Olympics (which NBC also covered) and as such these two episodes got some of the highest Nielsen ratings thus far.

## Cast

### Part 1

#### Regulars

Jerry Seinfeld ............................... Jerry Seinfeld  
Jason Alexander .......................... George Costanza  
Julia Louis-Dreyfus ...................... Elaine Benes  
Michael Richards .......................... Kramer

#### Guests

Peter Murnik ................................ Lt. Martel  
Elmarie Wendel ............................. Helene  
Debi A. Monahan .......................... Chelsea  
Ricky Dean Logan ......................... The Freak  
Vaughn Armstrong ....................... Lt. Coleman  
Keith Morrison ............................... Newscaster (Himself)  
Manfred Melcher ............................ Officer  
Christopher Michael Moore ............ Studio Guard  
Dyana Ortelli .................................. Chambermaid (Lupe)  
Michael Gerard ............................... Receptionist  
Fred Savage .................................... (Himself)  
George Wendt ................................ (Himself)  
Corbin Bernsen&nbsp; .............................. (Himself)

### Part 2

#### Regulars

Jerry Seinfeld ....................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus ............. Elaine Benes  
Michael Richards ................. Kramer

#### Guests

Peter Murnik ...................... Lt. Martel  
Elmarie Wendel .................. Helene  
Marty Rackham .................. Officer #1  
Peter Parros ....................... Officer #2  
Vaughn Armstrong ............ Lt. Coleman  
Clint Howard ..................... Tobias Lehigh Nagy  
Steve Greenstein ................ Man  
Kerry Leigh Michaels ......... Woman  
Keith Morrison ................... Newscaster (Himself)  
Peggy Lane O'Rourke ........ Reporter #1  
Deck McKenzie .................. Reporter #2  
Steve Dougherty ............... Prison Guard

## Script

Opening monologue.

It's amazing to me that people will move thousands of miles away to another city, they think nothing of it. They get on a plane, boom. They're there, they live there now. Just, uh, I'm living over there. You know, pioneers, it took years to cross the country. Now, people will move thousands of miles just for one season. I don't think any pioneers did that, you know. Yeah, it took us a decade to get there, and, uh, we stayed for the summer, it was nice, we had a pool, the kids loved it, and then we left about ten years ago and we just got back. We had a great summer, it took us 20 years and now our lives are over.

[George and Jerry enter Monk's Coffee Shop.]

GEORGE: Kramer was on Murphy Brown?

JERRY: Yeah.

GEORGE: Are you sure?

JERRY: Yeah.

GEORGE: Murphy Brown, the TV show.

JERRY: C'mon, will ya?

GEORGE: Kramer was on Murphy Brown? That son of a gun!

JERRY: Something, isn't it?

GEORGE: With Candace Bergen!

JERRY: I know!

GEORGE: I've always liked her. Remember her in 'Carnal Knowledge'?

JERRY: Sure.

GEORGE: Did she show her breasts in that?

JERRY: She's not really the naked type.

GEORGE: I can't believe I missed Kramer. You know he asked me to go with him to California.

JERRY: He did?

GEORGE: Yeah, I turned him down.

JERRY: How come you didn't tell me?

GEORGE: He asked me to keep it a secret.

JERRY: But you can never keep a secret.

GEORGE: I know. This was like a record. My previous record was when Joni Hirsch asked me not to tell anybody that we slept together. Kept a lid on that for about 28 seconds.

JERRY: Well, you've come a long way.

GEORGE: I've matured.

JERRY: Hey listen, the Tonight Show called me, they want me to come out and do the show on the 28th and they're giving me two free tickets to LA. You wanna go?

GEORGE: A free ticket?

JERRY: Yeah, in fact we could track down Kramer. I always felt bad about the way he left, you know? That was a mess. I never should have taken back those keys.

GEORGE: What about accommodations?

JERRY: All taken care of.

GEORGE: Is there a meal allowance? What about seat assignments? Could I have the Kosher meal? I hear the Kosher meal is good. And I need clothes. Gotta get a haircut. Gonna have to, I have to refill my allergy medication. Oh, do I need a hat? I need a hat, don't I? Could we do the Universal tour? They have that Backdraft exhibit now, that looks very cool to me...

[Kramer, backstage, talking to some aspiring young actors.]

KRAMER: So my acting technique, my personal acting technique is working with color, imagining color, then finding the emotional vibrational mood connected to the color. See, if you look through my scripts, you'll see that all my lines have a special color, so I don't memorize language, I memorize color. This way I can go through red, yellow, green, blue. And I have a full palette of emotions.

STUDIO GUARD: Hey, didn't I tell you to get out of here?

KRAMER: Uh, did you?

STUDIO GUARD: C'mon, let's go.

KRAMER: Well, I was just--

STUDIO GUARD: Yeah yeah, you were just nothing. C'mon, let's go.

KRAMER: Alright, we'll talk about this a little later. Are you an actor?

[Kramer's apartment building in LA. Singers and actors are heard practicing in the background. Kramer leaves his apartment and makes a call on the pay phone in the hall.]

VOICE: Murphy Brown.

KRAMER: Uh, yeah, uh, Candace Bergen please.

VOICE: Who's calling please?

KRAMER: Ah, well, just tell her that it's Kramer.

(Dial tone.)

KRAMER: Alright I'll uh, I'll call her at home. (To man waiting behind him) Go ahead, it's all yours.

(Helena, a neighbor, steps into the hallway.)

HELENA: Hello Kramer.

KRAMER: Oh, uh, Helena, how are you?

HELENA: I haven't worked since 1934, how do you think I am?

KRAMER: Well, that's only uh, 58 years.

HELENA: It was a Three Stooges short, &quot;Sappy Pappy.&quot; I played Mr. Sugarman's secretary, remember?

KRAMER: Yeah, right, right, yeah, yeah, that was a Shemp, right?

HELENA: No, a Curly. The boys played three sailors who find a baby, the baby's been kidnapped and the police think that they did it.

KRAMER: Uh huh, right.

HELENA: But, but of course they didn't do it, the police had made an awful mistake.

KRAMER: Right.

HELENA: Moe hits Curly with an axe,

KRAMER: Uh huh.

HELENA: The Stooges catch the kidnappers,

KRAMER: Right.

HELENA: But it's too late.

KRAMER: Really.

HELENA: The baby's dead.

KRAMER: Really?

HELENA: The boys are sent to Death Row and are executed.

KRAMER: Well I don't remember that part.

HELENA: I play Mr. Sugarman's secretary.

KRAMER: Oh, yeah, yeah, you were, you were very good.

HELENA: Yeah, it was sad for a Three Stooges, what with the dead baby and the Stooges being executed and all.

KRAMER: Well, that was an unusual choice for the stooges.

HELENA: Would you like to buy me a fat-free frozen yogurt at the store, Kramer?

KRAMER: Uh, well, uh, you know I can't right now, you know, uh, I got a very big meeting, I got these people interested in my movie treatment. So, uh, I guess we'll have to make it another time, alright?

HELENA: Well No! No, don't go out there, Kramer, they'll hurt you, they'll destroy you. You'll never make it in this town, you're too sensitive like me,

KRAMER: Helena, you're wrong, you know I'm not that sensitive at all.

HELENA: I was engaged to Mickey Rooney! He left me at the altar. Kramer! Kramer!

[Jerry's apartment. George walks in with several stuffed suitcases and backpacks.]

JERRY: What is this?

GEORGE: What?

JERRY: We're going on a two day trip, what are you, Diana Ross?

GEORGE: I happen to dress based on mood.

JERRY: Oh. But you essentially wear the same thing all the time.

GEORGE: Seemingly. Seemingly. But within that basic framework there are many subtle variations, only discernable to an acute observer, that reflect the many moods, the many shades, the many sides of George Costanza.

JERRY: (referring to George's outfit): And what mood is this.

GEORGE: This is Morning Mist.

[A murder scene. There's a body under a blanket and two Lieutenants are talking.]

LT. COLEMAN:: What do you figure, 20? 21?

LT. MARTEL:: Close enough.

LT. COLEMAN:: Forensics ought to be able to nail it down.

LT. MARTEL:: No ID?

LT. COLEMAN:: No ID.

LT. MARTEL:: No witnesses?

LT. COLEMAN:: Just the trees, Johnny. Pretty young thing.

LT. MARTEL:: She was. Not any more. Somebody saw to that.

LT. COLEMAN:: Sure did, Johnny. Damn shame too. What do you make of it?

LT. MARTEL:: I don't know, but I don't like it.

[Jerry and George are at the airport, in line for the metal detector.]

JERRY: Look at this guy, he's like a cat burglar. He thinks if he goes through real slow the machine won't detect him.

GEORGE: Personally I'm a little nervous about going through these things. I'm afraid I'm gonna step through into another dimension.

JERRY: Just go.

(George braces himself and walks through the detector.)

GEORGE: Heh he, I made it.

(Jerry walks through and the machine beeps.)

SECURITY GUARD: Empty your pockets please.

(Jerry empties his pockets and walks through again, the machine beeps again.)

SECURITY GUARD: Walk through again please.

(Jerry walks through, the machine beeps again.)

SECURITY GUARD: Are you sure you don't have any metal on you? Bracelets? Rings? Anklets?

JERRY: Anklets?

SECURITY GUARD: A lot of men wear anklets.

JERRY: Really?

SECURITY GUARD: Yeah.

OTHER SECURITY GUARD (to George): What do you have in your bag, sir?

GEORGE: My bag?

SECURITY GUARD: Step over here please.

JERRY: Over here?

OTHER SECURITY GUARD: Do you have a knife in the bag?

GEORGE: A knife?

OTHER SECURITY GUARD: Open the bag, please.

(George opens his bag, the other security guard begins rummaging.)

OTHER SECURITY GUARD: What's this?

GEORGE: Moisturizer?

OTHER SECURITY GUARD: For your wife?

GEORGE: No, I uh... I use it.

SECURITY GUARD: Spread your arms and legs please.

(The security guard begins waving a small beeping detector up and down Jerry's body.)

JERRY: (facing the lengthening line behind him) Ladies and gentlemen, I implore you.

OTHER SECURITY GUARD: Have a good trip.

SECURITY GUARD: Alright, go ahead.

JERRY: That's it?

SECURITY GUARD: That's it.

JERRY: Alright.

GEORGE: C'mon Jerry, let's go. What was that all about?

JERRY: I must have iron rich blood.

GEORGE: Here we go, LA.

JERRY: The Coast,

GEORGE: La-la Land. I got the window seat, right?

JERRY: Who said that?

GEORGE: I called it.

JERRY: Oh no.

GEORGE: What do you mean, oh no.

[Monologue, Jerry on stage.]

Seems to me that the closest thing we have to Royalty in America are the people that get to ride in those little carts through the airport. Don't you hate these things? They come out of nowhere; Beep Beep, cart people, look out, cart people! Look out! We all scurry out of the way like worthless peasants. Oooh! It's cart people! I hope we didn't slow you down. Wave to the cart people, Timmy, they're the best people in the world. Ya know, if you're too fat, slow and disoriented to get to your gate on time, you're not ready for air travel. The other people I hate are the people that get on to the moving walkway and then just stand there. Like it's a ride? Excuse me, there's no animated pirates or bears along the way here. Do your legs work at all?

[Stock photo of the HOLLYWOOD sign. Cut to a casting office. Kramer enters.]

KRAMER: Oh ah, yeah, I'm here for the audition.

RECEPTIONIST: Which audition, the music video, the horror movie, the exercise tape or the infomercial?

KRAMER: Uh, let's see... well.

[Cut to a montage of Kramer in group auditions for the productions the receptionist mentioned.]

[Kramer and Chelsea, a woman he met in the horror movie audition, exit the

casting office.]

KRAMER: You scream good.

CHELSEA: You too.

[Kramer and Chelsea are seated at a restaurant table.]

CHELSEA: So, can I keep this treatment?

KRAMER: Oh yeah, yeah, I got 20 copies.

CHELSEA: 'Cause I can, uh, show it to my manager. He has connections with West German television money.

KRAMER: Really.

CHELSEA: Yeah, they're trying to put together a miniseries for me on Eva Braun. I mean think about it, is that a great idea? We know nothing about Eva Braun, only that she was Hitler's girlfriend.

KRAMER: Um-hm.

CHELSEA: What was it like having sex with Adolf Hitler? What do you wear in a bunker? What did her parents think of Hitler as a potential son-in-law? I mean it could just go on and on...

KRAMER: Wait wait, hold it, hold it. Look who's over there. Don't look, don't look! It's Fred Savage.

CHELSEA: Big deal.

KRAMER: He'd be perfect for my movie. This is a once-in-a-lifetime opportunity. (takes a deep breath) I gotta go over there, I gotta give him a copy of my treatment.

CHELSEA: Why are you breathing so hard?

KRAMER: Well, I'm just a little nervous. OK, I gotta relax. Phew. Wish me luck, huh?

(Kramer gets up and approaches Fred Savage who's sitting at a table nearby.)

KRAMER: Hey. Oh, did I frighten you? I'm not crazy. I mean, I may look weird, but I'm just like you, I'm just a regular guy just trying to make it in this business. You know I really like your work, the, uh...

FRED SAVAGE: Thank you.

KRAMER: Yeah, I can't remember the name of it.

FRED: Thanks.

KRAMER: Yeah, my mind's a blank, I'm sorta nervous, you know, uh...

FRED: That's ok. Relax, relax.

KRAMER: Ok, but I got this...

(Kramer lifts his leg and places his foot on a low table and the table collapses.)

KRAMER: Stupid table. You know, I'm not normally like this, usually I'm very cool and charming, I don't mean to bother you or anything but I think it's fate that you happened to be here at the same time as me.

FRED (a little frightened and backing away towards the door): Yeah, its fate, you know, can't avoid your fate.

KRAMER: I got this treatment I think you'll be great in.

FRED: Yeah.

KRAMER: So I'd like to give it to you.

FRED: Yeah, thank you, thanks a lot. Bye!

KRAMER: (bumping into a lamp) Alright, excuse me. Uh wait, wait.

(Fred takes the treatment and bolts for the door. After he leaves, Kramer gives the rest of the patrons a thumbs-up.)

[George and Jerry are in their hotel room. Jerry's on the telephone.]

JERRY: Yeah, Kramer. K-R-A-M-E-R. Uh, I don't know, wavy? George, how would you describe Kramer's hair?

GEORGE: Curly.

JERRY: Wavy.

GEORGE: What'd you ask me for?

JERRY: Yeah, I'll hold on. Hey George, did you see a piece of paper I had on the nightstand here, like crumpled up, like a napkin?

GEORGE: Nope.

JERRY: 'Cause I had like three jokes on it, they were all perfectly worded just the way I wanted to have it. Can't find it. Hello?

GEORGE (from the bathroom): Hey, a shoe buffing machine!

JERRY: I don't know, 6-3, George, how tall is Kramer?

GEORGE: You got your own shampoo, conditioner, body lotion! Jerry, body

lotion!

JERRY: About 6-3.

GEORGE: Ooh, a shower cap!

KNOCK KNOCK KNOCK KNOCK

JERRY: Coming.

(George walks to the door wearing a shower cap.)

LUPE: Hello. I have more towels.

GEORGE: Oh good, good, come in. Come in, welcome. I'm George. And this is Jerry, over there, on the phone, that's Jerry. And you are, um?

LUPE: Lupe.

GEORGE: Lupe. That's very nice, very nice. Listen, are you going to be making up the bed in the morning?

LUPE: Oh Yes.

GEORGE: Fine. Excellent. Could you do me a favor? Could you not tuck the blankets in? 'Cause I can't sleep all tucked in.

LUPE: Oh, yes, yes.

GEORGE: Yes, I like to just be able to take the blankets and swish them and swirl them, you know what I mean? You know, I don't like being all tucked in. I like to have a lot of room, you know I like to have my toes pointed up in the air. Just like to scrunch up the blankets.

LUPE: Yes, yes. It's too tight to sleep.

GEORGE: Exactly, you know what I'm talking about, right?

LUPE: Oh yes, It's too tight. (Gesturing towards Jerry) Him too?

GEORGE: Uh, Jerry, you want your blankets tucked in?

JERRY: Excuse me, what?

GEORGE: You want your blankets tucked in?

JERRY: What blankets?

GEORGE: When Lupe makes up the beds in the morning.

JERRY: I don't know, whatever they do.

LUPE: I tuck in? Yes?

JERRY: Tuck in, tuck in.

GEORGE: Alright, so that's one tuck and one no-tuck.

LUPE: Okay.

GEORGE: Yeah. One second sweetheart. Jerry, I really think it'd be easier if you didn't tuck.

JERRY: Excuse me, fine, you don't want me to tuck, put me down for a no-tuck.

GEORGE (to Lupe): Two no-tucks.

JERRY: Uh, hang on a second, You know what? Changed my mind, make it a tuck.

GEORGE: You just said you weren't tucking.

JERRY: I'm tucking! Hello? Hello? They hung up on me. They don't know where Kramer is anyway.

GEORGE: Alrighty, so. That's one tuck and one no-tuck. Got that?

JERRY: Excuse me, um, did you see a piece of paper on the nightstand here earlier today crumpled up like a napkin?

LUPE: Oh, yes, yes. I throw away when we clean the room.

JERRY: Oh, okay, thanks.

LUPE: Thank you.

GEORGE: Thank you.

LUPE: Bye-bye.

GEORGE: Alright, Lupe, bye-bye now.

LUPE: Bye.

GEORGE: Bye-bye.

(Lupe exits.)

JERRY: I can't believe she threw that out. I had like the perfect wording of a whole joke I was gonna do about the X-ray counter at the airport, I was gonna do it on the Tonight Show, now I can't remember it.

GEORGE: Well what did you want her to do, you left it on the night table.

JERRY: They're not supposed to just take everything and throw it out!

GEORGE: Hey, hey, hey! It's not Lupe's fault, you shouldn't have left it out.

JERRY: Alright, just get your thing together and let's get out of here.

GEORGE: Alright, now. What mood am I in, what mood am I in?

[Cut to Jerry and George in the car, continuing their discussion.]

GEORGE: You shouldn't have tucked.

JERRY: I like it tucked.

GEORGE: Nobody tucks anymore.

(As Jerry drives past, Kramer exits a printing shop, gets into his car and drives off, spilling dozens of 8&quot;x10&quot; publicity photos. A police office notices this and picks up one of the photos.)

[Another murder scene. A police officer pulls a blanket over the body's head, but we catch a glimpse of the dead girl; it's Chelsea, Kramer's friend from the auditions. The same two Lieutenants as before are just arriving.]

OFFICER: Hey, Lieutenant.

LT. MARTEL:: Yeah.

OFFICER: This was found on her person.

LT. MARTEL:: On her person? What kind of expression is that?

OFFICER: I don't know, sir. Police lingo.

LT. MARTEL:: Oh yeah? What's your name, son.

OFFICER: Ross.

LT. MARTEL:: Ross. Do you see that person there, Ross?

OFFICER: Yes sir.

LT. MARTEL:: She's dead. Have you got that?

OFFICER: Yes sir.

LT. MARTEL:: Good. Now get out of here before you find yourself on transit patrol writing tickets to senior citizens with fake bus passes.

OFFICER: Yes sir.

(The Lt. examines the evidence. It's a title page that says &quot;The Keys&quot; A movie treatment by Kramer. The paper is torn so that the space where Kramer's first name was is missing.)

LT. MARTEL:: I think we just caught a break.

[Jerry and George are at the NBC studio.]

GEORGE: This is very exciting! You're on the Tonight Show, NBC, who else is on the show?

JERRY: I don't know.

GEORGE: Might meet a celebrity.

JERRY: I can't believe she threw out my napkin.

GEORGE: What are you worried about, you know it.

JERRY: You gonna be alright here?

GEORGE: Yeah yeah yeah yeah, go. Go about your business, I'll just wander around.

JERRY: Alright, don't wander too far, I'll meet you back here in fifteen minutes.

GEORGE: Go, go, go, don't worry about it.

(Jerry leaves and Corbin Bernsen enters through a stage door. he stands near George, obviously waiting for someone.)

GEORGE: Hey. (pointing at him) Corbin Bernsen.

CORBIN BERNSEN: How ya doing?

GEORGE: Big fan! Big fan.

CORBIN BERNSEN: Yeah.

GEORGE: Hey, you grew a beard, huh?

CORBIN BERNSEN: Yeah, yeah. I'm doing a movie during my hiatus.

GEORGE: Hey. You know, do I have a case for you guys to do on L.A. Law.

CORBIN BERNSEN: Really.

(Flash forward to the middle of George's 'pitch'.)

GEORGE: ...so mind you, at this point I'm only going out with her two or three weeks. So she goes out of town and she asks me to feed her cat. So at this time, there's a lot of stuff going on in my life and, uh, it slips my mind for a few days. Maybe a week. Not a week, five, six days.

CORBIN BERNSEN: Yeah yeah yeah. So what happened?

GEORGE: Well, it's the damnedest thing. The cat dies. So she comes back into town, she finds the cat lying on the carpet stiff as a board.

CORBIN BERNSEN: So you killed the cat.

GEORGE: That's what she says. I say, listen. It was an old cat. It died of natural causes. So get this, now she tells me that I gotta buy her a brand new cat. I say listen, honey. First of all, it was a pretty old cat. I'm not gonna buy you a brand new cat to replace an old dying cat. And second of all, I go out to the garbage, I find you a new cat in fifteen seconds. I say, you show me an autopsy report that says this cat died of starvation, I spring for a new cat. So she says something to me, like, uh, I dunno, get the hell out of here, and she breaks up with me. Now don't you think that would be a great case on L.A. Law?

(Corbin Bernsen just stares at George.)

(Flash forward, George is now talking with George Wendt in the same hallway.)

GEORGE: I don't wanna tell you how to run your show.

GEORGE WENDT: Oh, of course not.

GEORGE: But really, it's enough with the bar already.

GEORGE WENDT: Yeah, well.

GEORGE: Seriously, have they though about changing the setting?

GEORGE WENDT: Doubt it, I doubt it. Yeah.

GEORGE: Really? Because people do meet in places besides a bar, huh?

GEORGE WENDT: Well yeah, they do, heh heh.

GEORGE: What about a rec room? Huh? Or a community center.

GEORGE WENDT (checking his watch, obviously uncomfortable): Yeah, you oughta write one of those.

GEORGE: Yeah?

GEORGE WENDT: Yeah, I'll bring it up with the producers, I gotta... uh...

GEORGE: Fabulous, I'll think about that George, thank you!

(Jerry walks up as George Wendt leaves hastily.)

JERRY: How's it going?

GEORGE: Great! Great! I actually just had two meaningful intelligent conversations with Corbin Bernsen and George Wendt.

JERRY: Really?

GEORGE: Yeah, yeah, not fan talk, not gushing, you know? Actual conversation, I was incredibly articulate!

JERRY: You got toilet paper on your heel there.

(George looks down, Jerry walks away.)

[The Tonight Show, George is in the audience applauding.]

Announcer: It's the Tonight Show with Jay Leno. Tonight Jay welcomes Corbin Bernsen, George Wendt and comedian Jerry Seinfeld.

(Quick cut to Corbin Bernsen, mid-interview.)

CORBIN BERNSEN: Oh yeah, yeah, people are always coming up to me trying to give me a great case for L.A. Law, just a few seconds ago, right here, right outside in the hallway this nut, some sick nut comes up to me and says he's supposed to watch this girl's cat while she's away out of town. Anyway he forgets to feed the cat, the cat dies, starves to death, he kills the cat, refuses to get her a new one, won't give her any money, won't pay her, and he wants Arnie Becker to represent him. Nice guy. Yeah, that'd make a *great* case for L.A. Law. Thanks a lot.

(Quick shot of the audience, everyone is laughing besides George.)

[Cut to a police station. Helena is being questioned by Lt. Martel.]

HELENA: He's a very handsome man. Passionate, intense, but troubled, strange. I think he may be in love with me. Of course there's nothing abnormal about that, I have many suitors.

[Cut back to the Tonight Show, George Wendt, mid-interview.]

GEORGE WENDT: It's funny, 'cause even after all these years, we still get people giving us advice, how to improve the show. Actually, a few moments ago I ran into a nut back there, he said, you know, that maybe we should think about, you know, not doing the show in a bar.

(Another quick shot of the audience, again everyone is laughing besides George.)

[Cut back to the police station. One of the kids from the van Kramer hitched a ride to LA in (see &quot;The Keys&quot;) known only as 'The Freak' is being questioned by the Lt.]

THE FREAK: So that's when I said, &quot;Hey, Kramer, dude. You ever killed a man before?&quot; And he said, &quot;What do you think, Junior? These hands have been soaking in Ivory liquid?

[Cut back to the Tonight Show, Corbin Bernsen and George Wendt are talking between takes.]

GEORGE WENDT: The guy you talked to, what did he look like?

CORBIN BERNSEN: Short little bald guy with glasses.

GEORGE WENDT: Yeah, yeah, that's the same guy I talked to.

CORBIN BERNSEN: It never ends, does it?

(Cut to Jerry, on stage, doing his routine.)

JERRY: So I'm going through the airport and I have to put my bag on that little uh, uh, the uh, that uh, the conveyor belt.

(Quick shot of the audience, nobody is laughing.)

(Quick shot of an extremely uncomfortable Jerry.)

[Cut back to the police station. The Lt. is on the phone.]

LT. MARTEL: Issue an arrest warrant, put out an APB. Let's pick up this, uh, Kramer.

[George and Jerry are leaving the Tonight Show set]

JERRY: I was terrible.

GEORGE: What are you, crazy? You were fine.

JERRY: Nah, did you hear the end? I couldn't remember what I was trying to say, that whole thing about the, uh...

GEORGE: Conveyor belt.

JERRY: Yeah. Because she threw out my napkin.

GEORGE: I can't believe, you're blaming Lupe?

JERRY: Yes, Lupe. I'm blaming Lupe.

(George and Jerry walk past a TV and stop as a special report is being broadcast.)

TV NEWSCASTER: Our top story tonight, there has been a break in the so called 'Smog Stranglings'. Police have just released a photo of the suspect being sought in connection with the slayings. He is known only as &quot;Kramer&quot;.

(George and Jerry stare at each other in disbelief.)

Closing monologue.

Talk show hosts never seem to have any idea how much time is left in the show. You know, they're always looking off camera, &quot;Do we have time? Are we out of time? How we doin' on time? Anybody know what the time is? What's the time? Check the time?&quot; You never see Magnum P.I. go, &quot;Should I strangle this guy or are we gonna take a break here? Can you stay for another beating? I'll tell you what, I'll bop him in the head, we'll do a commercial, we'll come back, I'll drive in the car real fast, stay with us.&quot;

To Be Continued...

[OPENING MONOLOGUE]

There are many different job in the police. It seems to me, that the chalk outline guy is one of the better jobs that you can get. You know it's not dangerous, the criminals are long gone, that seems like a good one. I don't know who they are, I guess they're people who wanted to be a sketch artists, but they couldn't draw too well...&quot;listen Johnson, forget the sketches. Do you think if we left a dead body right there on the sidewalk, you could manage to trace around it? Could you do that?&quot;. I don't even know how it helps to solve the crime? You know, they look at the thing on the ground...&quot;aah his arm was like that when he hit the pavement. That means the killer must've been Jim.&quot;

[OPENING SCENE]

(GEORGE AND JERRY ARE IN A CAR. GEORGE IS GRABBING JERRY'S ARM)

GEORGE: He's on the lamb(?), he's on the loose!

JERRY: Would you let go of my arm?! I'm trying to drive, you're getting us both killed!

GEORGE: What are we supposed to do? What do you do on a situation like this? Should we call a lawyer, should we call the police?

JERRY: Obviously we're gonna call the police and tell that he's not the guy.

GEORGE: Hope he's not the guy.

JERRY: Couldn't be the guy...nah.

GEORGE: God, I'm starved, I'm weak from hunger.

JERRY: How can you think of food at the time like this?

GEORGE: Time like what? I'm hungry. My stomach doesn't know that Kramer's wanted.

JERRY: I told you to have breakfast, you should've had breakfast!

GEORGE: I couldn't have breakfast, it was lunchtime! The three hour time difference threw me. I wanted a tuna fish sandwich, they wouldn't serve me tuna fish sandwich, because they were only serving breakfast.

JERRY: You should've had some eggs.

GEORGE: For lunch? Who eats eggs for lunch?

JERRY: Have you ever heard of egg salad?

GEORGE: Why didn't you say something then?

JERRY: I've gotta to tell you about existence of egg salad?

GEORGE: I need food, Jerry. I feel faint, I'm getting light headed.

JERRY: I've gotta call the police, there's a pay phone over there.

GEORGE: Pay phone in L.A., look it's a miracle.

[CUT TO KRAMER SINGING IN A SHOWER FOLLOWED BY A FALLING SOUND]

[BACK TO GEORGE AND JERRY ON THE STREET]

JERRY: I don't have any change. You've got any change?

GEORGE: No, I don't have any change. I never carry change.

JERRY: Well, we need change and all I have is twenties.

GEORGE: I have a ten.

JERRY: So, break it.

GEORGE: I hate asking for change. They always make a face. Like I'm asking them to donate a kidney.

JERRY: So, buy something.

GEORGE: What?

JERRY: I don't know, some mints or TicTacs.

GEORGE: Breath problem?

JERRY: No, I just want some change.

GEORGE: Tell me.

JERRY: Your breath is fine. It's delightful, it's delicious.

GEORGE: You know, I haven't eaten anything.

JERRY: I just wanna call the police!

GEORGE: Why don't you just call 911?

JERRY: But is this an emergency?

GEORGE: Of course it is.

JERRY: How is this an emergency?

GEORGE: Your friend is been accused of being a serial killer. I think that qualifies.

JERRY: All right, I'll call 911. Think he did it? Could've he done it? Couldn't done it? How could've he done it? Couldn't be? Could it? Hello 911? How are you? I'm sorry it was just a reflex...I know it's an emergency number...it is an emergency...my friend is being accused of being a smog strangler and I know he didn't do it...they're putting me trough to the detective in charge of the investigation...what is my name? Who am I? I'm eh...George Costanza...

GEORGE: What's the matter with you? Are you crazy? Why are you using my name?!

JERRY: Oh, don't be a baby! What are you scared of?

GEORGE: What am I scared of? I'm scared of the same thing that you are, everything! Why don't you just use your own name?

JERRY: Your name is a good name, Costanza. Sounds like it's stands for something, they'll believe us.

GEORGE: Really?

JERRY: Sure.

GEORGE: You think so?

JERRY: Oh yeah. Yes I have some very important information regarding the smog strangler. (George leans close) would you suck a mint or something. Can I come right now? I suppose, where are you located? Where is that? I don't know where we are. Where are we?

GEORGE: I don't know.

JERRY: We don't know. He says ask somebody, ask that guy.

GEORGE: Excuse me, where are we?

MAN: Earth.

JERRY: Hey, you know I'm on the phone with the police! Some guy just gave me a wise answer. Ask that woman.

GEORGE: Excuse me Ms. which street are we on?

WOMAN: I don't know.

GEORGE: You don't know?

WOMAN: I don't know.

GEORGE: How come you don't know what street are you on?

WOMAN: You don't know.

JERRY: George, it says it here on the phone. It's 12145 Ventura Boulevard. Aha, ok...do we know where the 101 is? (George shakes his head) No...do we know where 170 is? (George shakes his head) No...do we know where 134 is? (George just looks at Jerry) No. Aha, ok. (Jerry hangs up) He's gonna send a black and white to pick us up.

(Police car rolls by the sidewalk and stops. The police listens their conversation.)

GEORGE: Black and white?

JERRY: A cop car.

GEORGE: Why didn't you just say that?

JERRY: I thought it sounded kind of cool.

GEORGE: Oh yeah, real cool. You're a cool guy.

JERRY: Oh, you are? I guaranty you, Lupe is going to tuck your covers in.

GEORGE: I'll bet you, how much?

JERRY: Her tip.

GEORGE: You've got a bet.

JERRY: Ok.

GEORGE: How much do you tip a chamber maid?

POLICE: Which one of you is Costanza?

(Jerry and George point at each other.)

POLICE: Get in.

GEORGE: Hi, how are you guys? Listen, does either one of you have like a mint or piece of gum or...

[KRAMER IS SHAVING. HE SNEEZES AND GETS SHAVING CREAM ON THE MIRROR]

[BACK TO THE POLICE CAR WITH JERRY, GEORGE AND TWO COPS]

GEORGE: Jerry, would you do me a favor, close the window.

(JERRY SEARCHES FOR THE HANDLE, BUT CAN'T FIND ONE)

JERRY: Hey, get out of here...hey officer, he's fooling around back here.

COP: Cut it up back there.

GEORGE: He started it.

JERRY: I did not. You guys gonna go through some red lights?

COP: I don't think so.

JERRY: But you could?

COP: Oh yeah, of course we could. We can do anything we want.

COP 2: We could drive on the wrong side of the road.

COP: Yeah, we do that all the time. You should see the looks on people's faces.

COP 2: Shoot people...

GEORGE: You guys ever shot anybody?

COPS: No...

GEORGE: Hey, can I flip on the siren?

JERRY: Why are you bothering them for?

GEORGE: I'm just asking, all they have to do is say no.

COP: Yeah, go ahead.

(GEORGE TRIES THE SIREN)

GEORGE: Wohoo, check it out.

JERRY: Can I try it?

COP: Yeah, go ahead, hurry up.

(JERRY TRIES THE SIREN)

JERRY: Scared the hell out of that guy.

GEORGE: You know what I never understood? Why did they change the siren noise? When I was a kid it was always &quot;waaaa, waaaa&quot;, you know now it &quot;woo-woo-woo-woo-woo&quot;. Why did they do that, did they do some research? Did they find that woo-woo was more effective than waa?

JERRY: Yeah, what about those English sirens, you know...eee-aaa-eee-aaa-eee-aaa...

JERRY and GEORGE: Eee-aaa-eee-aaa-eee-aaa...

COP: Hey!

[KRAMER IS COMBING HIS HAIR. TRYING TO GET THE COMB THROUGH.]

KRAMER: I'm dizzy.

[BACK TO THE POLICE CAR]

JERRY: Nice shotgun.

COP: Thanks.

JERRY: Clean as a whistle.

GEORGE: You could eat of that shotgun.

JERRY: What is that, a 12 gauge?

COP: Yeah.

JERRY: 12 gauge. Seems to be the most popular gauge.

GEORGE: My favorite.

JERRY: Mine too, love the 12 gauge.

GEORGE: Makes the 11 gauge look like a cap pistol.

COP: What do got over there?

COP 2: I don't know.

COP: Looks like a possible 5-19.

JERRY: 5-19? What's a 5-19?

GEORGE: Where?

COP 2: Think so?

COP: Looks like it.

JERRY: I can't believe this. A 5-19?

GEORGE: Where, where? I can't see.

COP: This is car 23, we have a possible 5-19 in progress, over.

COP 2: All right, let's pull over and check it out.

JERRY: Pull over? You can't pull over.

GEORGE: What are you doing? Where do you think you're going?

JERRY: Pull over? The lieutenant is waiting to see us.

GEORGE: Hey hey hey, we're in a rush here.

JERRY: We have an appointment!

GEORGE: What are you doing?!

JERRY: Great.

(COPS GO TO ARREST A GUY FOR TRYING TO STEAL A CAR)

GEORGE: There's a bag of Pepperidge Farm cookies up there.

JERRY: Which flavor?

GEORGE: Milano.

JERRY: Cops eating Milanos. What crazy town is this?

GEORGE: Should I take some?

JERRY: I think that's a 5-19.

GEORGE: I'm starving...

(GEORGE REACHES FOR THE COOKIES AND THE COPS SLAM A GUY ON THE HOOD AND GEORGE BACKS AWAY)

JERRY: They're busting this guy.

GEORGE: They're cuffing him.

JERRY: I can't believe this.

(GEORGE GET CLOSER TO JERRY AND THEY THROW THE GUY NEXT TO GEORGE)

[MIDDLE MONOLOGUE]

I can't believe that cops still have to read that whole &quot;you have the right to remain silence&quot;-speech to every criminal they arrest. I mean is there anybody who doesn't know that by now? Can't they just go &quot;Freeze, you're under arrest. You've ever seen Beretta? Yeah, good, get in the car.&quot;

[BACK TO POLICE CAR]

GEORGE: Hi.

JERRY: Hi, I'm Jerry.

GEORGE: George, how you doing?

(GEORGE OFFERS HIS HAND, BUT THE GUY IS IN CUFFS)

GUY: What did you do?

GEORGE: Nothing.

JERRY: Nothing.

GUY: Oh yeah right, me neither. Hey I didn't do nothing!

COP: Shut up.

JERRY: Hot out.

GUY: Brutal.

GEORGE: What do you tip a chamber maid.

GUY: I don't know, five bucks a night.

JERRY: No, a dollar, two tops.

GUY: Hey, you guys aren't cuffed. What are you, narks?

GEORGE: Narks?

JERRY: Imagine, us narks?

GEORGE: No no no, you know actually we are friends of a serial killer.

GUY: Really? Well, that's very nice.

GEORGE: Oh, thank you.

JERRY: Suspected serial killer, he didn't actually do it.

GEORGE: Yeah well, we don't think.

JERRY: We're pretty sure.

GUY: A dollar a night?

JERRY: Yeah, that's a good tip!

GUY: That stinks!

JERRY: I read it in Ann Landers.

GUY: Oh, Ann Landers sucks!

COP 2: Hey, shut it up back there.

POLICE RADIO: Attention all units, attention all units, all units code 3. All units in the area, code 3 in progress, 1648 North Bartholis, units required for assistance in apprehension of 702.

COP: That's smog strangler.

JERRY: Kramer.

COP 2: Got him. Let's go.

(POLICE CAR COMES TO THE SCENE. JERRY OPENS THE BACK DOOR THROUGH THE WINDOW AND THEY GO AFTER THE COPS)

JERRY: I wanna see what's happening.

GEORGE: I don't know why I'm doing this.

LT MARTEL KNOCKS ON A DOOR AND KRAMER OPENS

KRAMER: Jerry, George!

LT. MARTEL: You are under arrest in first degree murder and death of Ms Chelsea Lang.

(THE CAR THIEVE GUY RUNS AWAY FROM THE DOOR THAT JERRY AND GEORGE LEFT OPEN)

[KRAMER IS RUSHED THROUGH A GROUP OF PRESS]

REPORTERS: Why did you do it? What possessed you?

KRAMER: I don't know...

[COUNTY OF LOS ANGELES CENTRAL JAIL]

(JERRY AND GEORGE ARE VISITING KRAMER)

KRAMER: Hey, how are you doing? Jerry! George!

JERRY: We're doing fine. How are you?

KRAMER: What me? Fabulous, just fabulous. I've got a lot of auditions, a lot of call backs and I've got a lot of interest for my movie treatment. I'm in development, I'm in developed vehicles. And there's a lot of energy here, man. You know, the vibe, it's powerful. I'm just swept up at it. Yeah, I'm a player.

GEORGE: A player?

KRAMER: Yeah, a player...

JERRY: Kramer, do you realize what's going on here? Do you know why you're here?

KRAMER: What? What this? I'll be out of here in couple of hours. Hey, guess who I met today? Rick Savage, oh nice kid, really good kid. You know, we're talking about doing a project together.

JERRY: Kramer, you've been arrested as a serial killer!

KRAMER: So? I'm innocent! I mean you guys believe that I'm innocent, don't you? Jerry? George?

JERRY: Well, yeah...sure.

POLICE: Kramer, let's go. The Lieutenant wants to see you.

KRAMER: Ok, yeah. All right look, I'll be out of here by noon. Maybe we'll have lunch together, huh?

(KRAMER GOES WITH THE POLICE OFFICER, BUT ASKS IF HE COULD SAY JUST ONE MORE THING)

KRAMER: Help me!! Help me!

(OFFICER DRAGS KRAMER AWAY)

[KRAMER IS BEEN INTERROGATED BY LT. MARTEL]

KRAMER: I didn't kill anyone, I swear! I swear to God!

LT. MARTEL: Don't you ever swear to my God, Kramer. My God is the god who protects the innocent and punishes the evil scum like you, have you got that?

KRAMER: You're making a big mistake.

LT. MARTEL: No! You have made the mistake, Kramer. Sickies like you always do. The only difference is that this time you're gonna pay.

KRAMER: What?

LT. MARTEL: Now you might beat the gas chamber Kramer, but as long as I have got a breath in my body you will never ever see the light of day again.

KRAMER: Wow wow wow wow, you've got the wrong man!! It wasn't me!

LT. MARTEL: Oh yeah, right. Maybe it was one of your other personalities huh, the wise guy, the little kid, the bellhop, the ball player, maybe the door to door vacuum cleaner salesman, but not you right? No, you wouldn't hurt a fly. You just couldn't help yourself, could you Kramer? You saw life brimming brightly with optimism and verve and you just had to snuff it out.

KRAMER: Ok, can I just talk to somebody? Can I just explain...

LT. MARTEL: I'm not interested in your explanations, Kramer! Sure, I bet you've got a million of 'em. Maybe your mother didn't love you enough, maybe the teacher didn't call on you in school when you had your little hand raised, maybe the pervert in the park had a present in his pants, huh? Well, I've got another theory you're a weed.

KRAMER: No...

LT. MARTEL: Society is filled with them. They're choking the life out of the all pretty flowers.

(KRAMER SOBS)

LT. MARTEL: You see something even remotely pretty and you have to choke the life out if it, don't you Kramer?

(KRAMER CRIES)

LT. MARTEL: You killed all the pretty flowers, didn't you Kramer? You killed the pretty little flowers, didn't you? You dirty, filthy, stinky weed! Didn't you?

(PHONE RINGS)

OFFICER: Lieutenant, it's for you.

LT. MARTEL: Martel...yeah...yeah...yeah...yeah.

(KRAMER KEEPS CRYING)

OFFICER: What it is, Lieutenant?

LT. MARTEL: Let him go.

OFFICER: What, but Lieutenant?

LT. MARTEL: You heard me, let him go. They just found another body at the Laurel Canyon. Go on Kramer, get out of my sight.

KRAMER: Hey, how did you know about the guy in the park?

LT. MARTEL: I said beat it!

[JERRY AND GEORGE ARE WAITING OUTSIDE OF THE JAIL]

(KRAMER WALKS OUT)

KRAMER: Hahaa!

GEORGE: What?

JERRY: What happened?

KRAMER: Somebody got killed while they had me in custody.

JERRY: Really? Did you hear that? Somebody else was killed!

GEORGE: You're kidding? Somebody else got killed?

JERRY: While you were in jail. So you're free.

KRAMER: Yes, I'm free. (singing) 'cause the murderer struck again!

(THEY ALL DANCE A FEW STEPS AND THEN AS A POLICE GOES BY THEY LEAVE QUIETLY)

[KRAMER, JERRY AND GEORGE AT THE HILLS OF L.A.]

JERRY: So Kramer, what are you going to do?

KRAMER: Do? Do? Hey, I'm doing what I do. You know, I've always done what I do. I'm doing what I do, way I've always done and the way I'll always do it.

GEORGE: Kramer, what the hell are you talking about?

KRAMER: What do you want me to say? That the things haven't worked out the way that I planned? That I'm struggling, barely able to keep my head above water? That L.A. is a cold place even in the middle of the summer? That it's a lonely place even when your stuck in traffic at the Hollywood Freeway? That I'm no better than a screenwriter driving a cab, a starlet turning tricks, a producer in a house he can't afford? Is that what you want me to say?

GEORGE: I'd like to hear that.

JERRY: Yeah...

KRAMER: Well, I'm not saying that! You know, things are going pretty well for me here. I met a girl...

JERRY: Kramer, she was murdered!

KRAMER: Yeah, well I wasn't looking for a long term relationship. I was on TV.

GEORGE: As a suspect in a serial killing.

KRAMER: Ok, yeah, you guys got to put a negative spin on everything.

GEORGE: What did they put on this tuna? Tastes like a dill, I think it's a dill.

JERRY: So you're not gonna come back to New York with us?

KRAMER: No no I'm not ready, things are starting to happen.

GEORGE: Taste this, is this a dill?

JERRY: No, it's tarragon. Hey Kramer, I'm sorry about that whole fight we had about you having my apartment keys and everything.

KRAMER: Ok, it's forgotten.

GEORGE: Tarragon? Oh, you're crazy.

JERRY: Well, take it easy.

KRAMER: Yeah, ok.

GEORGE: Yeah, take care. Stay in touch.

KRAMER: Hey hey, whoa come on give me a hug...

JERRY: Oh, no...

GEORGE: No, you're crushing my sandwich.

[JERRY AND GEORGE AT THE HOTEL. GEORGE IS KICKING THE TUCKED COVERS.]

JERRY: Yeah, it's so nice when it happens to you.

[BACK TO JERRY'S APARTMENT. GEORGE AND JERRY ARE WATCHING TV. ]

GEORGE: Mint?

JERRY: No thanks.

GEORGE: I've got to tell you, I'm really disappointed in Lupe.

JERRY: It's been three days already, forget about Lupe.

GEORGE: Do you think she gets to take any of those little bars of soap home?

JERRY: No, I don't.

GEORGE: You would think that at the end of the week when they hand out the checks, throw in a few soaps.

JERRY: Yeah, maybe they should throw in a couple of lamps too.

GEORGE: I'll tell you something, if I'd own a company, my employees would love me. They'd have huge pictures of me up on the walls and in their home, like Lenin.

JERRY: How much did you wound up tipping her?

GEORGE: Oh my God, I forgot!

JERRY: Well, communism didn't work.

(KRAMER WALKS IN)

KRAMER: Hey!

(KRAMER GOES TO THE FRIDGE)

KRAMER: Any mustard? This is empty.

JERRY: Yeah, there's a new one in there.

KRAMER: No no, I don't like this one. It's too yellow. Any pickles?

JERRY: Help yourself.

KRAMER: Yeah, all right.

GEORGE: Kramer, what are you doing here?

KRAMER: Getting something to eat.

JERRY: Kramer, here!

(JERRY THROWS THE APARTMENT KEYS TO KRAMER. KRAMER WALKS OUT AND COMES BACK WITH HIS KEYS. HE THROWS THEM TO THE TABLE KNOCKING JERRY'S SODA.)

[ACTION NEWS. KEITH MORRISON.]

Newscaster: Authorities exposed today, that the latest suspect in the smog strangling was apprehended this week on an unrelated charge, but somehow managed to escape from the police car, in which he was being held. Tobias Lehigh Nagy, who is also wanted in connection with a series of unrelated slayings in the North West is still at large, his whereabouts unknown. He's described as 5'5&quot; bald and reputedly a very generous tipper.

[CLOSING MONOLOGUE]

The thing about L.A. to me, that kind of threw me, was when they have these smog alerts out there and they actually recommend that people stay indoors during the smog alert. Now, maybe I'm way off, but don't you think, wouldn't you assume, that the air in the house pretty much comes from the air in the city where the house is? I mean what do they think, that we live in a jar with couple of holes punched in the top? What the hell is going on out there? It's very strange, do you realize that it's now possible for parents to say to their children &quot;All right kids, I want you in the house and get some fresh air! Summer vacation, everybody indoors.&quot;

The End
