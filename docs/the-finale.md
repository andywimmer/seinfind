---
layout: default
title: The Finale
parent: Season 9
nav_order: 23
permalink: /the-finale
cat: ep
series_ep: 179, 180
pc: 923, 924
season: 9
episode: 23, 24
aired: May 14, 1998
written: Larry David
directed: Andy Ackerman
imdb: http://www.imdb.com/title/tt0697695
wiki: https://en.wikipedia.org/wiki/The_Finale_(Seinfeld)
---

# The Finale

| Season 9 - Episode 23 & 24 | May 14, 1998              |
|:---------------------------|:--------------------------|
| Written by Larry David     | Directed by Andy Ackerman |
| Series Episode 179 & 180   | Production Code 923 & 924 |

"The Finale" is the two-part series finale of the American sitcom Seinfeld. They are the 179th and 180th episodes of the show and the 23rd and 24th episodes of the ninth season. It aired on NBC May 14, 1998 to an audience of 76 million viewers. Its initial running time was 1 hour and 15 minutes.

The fake working title for this show was "A Tough Nut to Crack" to throw off outsiders about the contents of the episode. For the first and final time since season seven, Jerry performs a stand-up comedy routine. Larry David, co-creator of the series, returned to write the script for this episode.

The episode garnered strongly divided responses upon airing, and continues to polarize critics and audiences.

## Plot

### Part 1

Jerry and George have finally struck a deal with NBC to produce their pilot Jerry as a series, upon receiving a call from Elizabeth Clark calling from the office of NBC President James Kimbrough (Peter Riegert). Jerry and George will be leaving New York City for California to begin work. Jerry is given use of NBC's private jet by NBC executives Jay Crespi and Stu Chermak as a courtesy and he, George, Elaine, and Kramer decide to go to Paris for "one last hurrah". Elaine tries to get hold of her friend Jill. First, she can't get any reception with her cell phone on the street. Then, Jerry interrupts her with news of the pilot pickup and Elaine hangs up on Jill to take the call. Jerry then scolds her for trying to rush the call before they all leave for Paris, and for thinking about calling from the plane. On the plane that is piloted by Captain Mattox and his co-pilot Kurt Adams, George and Elaine argue over the quality of the plane and what Elaine considers an "effeminate" way in which George sits in the jet, while Kramer is still trying to get water out of his ears from a trip to the beach he made earlier in the day.

Kramer's desperation to get the water out of his ears causes him to jump up and down on the plane and, as a result, he stumbles and falls into the cockpit, which causes the pilots to lose control. While the plane is nosediving, the four prepare for death. George, momentarily feeling the need to confess, reveals he cheated in "The Contest," and Elaine begins to tell Jerry that she always loved him; but the plane steadies itself and they make a safe emergency landing in the fictional small town of Latham, Massachusetts.

While waiting for the airplane to be repaired, they witness an overweight man named Howie (John Pinette) getting carjacked at gunpoint by a criminal (Jerry Thomas Johnson). Instead of helping him, they crack jokes about his size while Kramer films it all on his camcorder, then proceed to walk away. The victim notices this and tells the reporting officer Matt Vogel (Scott Jaeck), who arrests them on a duty to rescue violation that requires bystanders to help out in such a situation.

Because this is the first case implementing this law, they are advised by the deputy to call a lawyer to represent them. Jerry and his friends do not have any choice but to call on Jackie Chiles to represent them for the upcoming trial. District Attorney Hoyt (James Rebhorn) hears that Jackie Chiles will be representing Jerry and his friends and tells the prosecutor that he will find out everything about them.

### Part 2

Jerry, Elaine, George, and Kramer are in their cell having their meal while awaiting the trial when Jerry's beeper goes off stating that their airplane is ready. Geraldo Rivera and Jane Wells cover the news about the trial of Jerry, Elaine, George, and Kramer. The people associated with the main characters are packing for the trial and heading to Latham, Massachusetts. Jerry's parents Morty and Helen, George's parents Frank and Estelle, Newman, Jerry's Uncle Leo, Jacopo "J." Peterman, David Puddy, Mickey Abbot, Kenny Bania, Susan Ross' parents, Rabbi Glickman, Keith Hernandez, and George Steinbrenner are among those shown making their way to Latham, Massachusetts. In addition to these people, many others from New York like Kramer's mother Babs Kramer and Matt Wilhelm have made the trip to watch the trial in the courtroom. A lengthy trial ensues presided over by Judge Arthur Vandelay (Stanley Anderson). George considers this to be a good sign as Arthur Vandelay was one of the many fake names he used for himself and phony companies he claimed to have worked for.

District Attorney Hoyt starts his opening statement that the defendants have ignored their Good Samaritan Law and mocked the victim of a mugging. He also states that the defendants must pay for this crime. Jackie Chiles starts his opening statement that this trial is a waste of the taxpayer's money, the defendants are innocent from bystanding, and that the real criminal is still out there.

District Attorney Hoyt starts to ask a lot of witnesses in hopes to make Jerry, Elaine, George, and Kramer pay for breaking the duty to rescue law.

* Officer Matt Vogel testifies that Jerry, George, Kramer, and Elaine were simply standing there while the victim was being robbed. During his testimony, Kramer's video is played upon Matt Vogel getting permission from the court to play it.

* Howie claims that Jerry, Elaine, Kramer, and George just did nothing while he was getting robbed. District Attorney Hoyt has no further questions towards Howie. Following the testimony, Jay Crespi and Stu Chermak take their seats as George shouts at them for the jet nearly killing them causing Judge Vandelay to bang his gavel.

* Mabel Choate (Frances Bay), the elderly woman Jerry mugged for a loaf of marble rye bread for George to give to his soon-to-be in-laws in "The Rye," recounts this incident that took place on January 4, 1996. Her use as a witness upon her taking the stand leads to an objection by Jackie Chiles because she was not present at the crime. District Attorney Hoyt states that they plan to use a series of character witnesses and the judge overrules the objection so that he can hear what she has to say.

* Marla Penny (Jane Leeves), the virgin whom Jerry dates in "The Virgin" and "The Contest," reluctantly explains that she broke up with Jerry on October 28, 1992 after learning of the contest that the four had to see who could go the longest without "gratifying themselves." This caused everyone to groan as J. Peterman even quotes "For the love of God."

* Donald Sanger (voiced by Jon Hayman), the Bubble Boy whom Jerry tried to visit, was brought in the courtroom by his father Mel Sanger (Brian Doyle-Murray). Donald describes the argument he had with George on October 7, 1992 while playing a game of Trivial Pursuit where Moors was misprinted as "Moops" when it came to the "Who invaded Spain in the 8th Century" question. Their argument ultimately led to Susan Ross accidentally destroying the protective bubble in which he lives (due to an unspecified medical condition). The bubble actually popped while Donald was choking George who was framed for trying to kill him by those who knew Donald. George and Donald then start their "Moors or Moops" argument again.

* Lola (Donna Evans) describes events from "The Handicap Spot". George parked in a handicapped space, which caused her to travel a greater distance to get to where she was going. Her electric wheelchair was destroyed in an accident when it lost power and Kramer bought her a used wheelchair with faulty brakes which caused her to go careening down a hill.

* Dr. Wexler (Victor Raider-Wexler) recaps the events of "The Invitations" where he treated George's ex-fiancee Susan Ross after she passed out from licking envelopes covered with toxic glue on May 16, 1996. Dr. Wexler described a look of "restrained jubilation" on George's face when he was informed she died. His testimony causes Susan Ross' parents to angrily react (which confirmed Mr. and Mrs. Ross' suspicion in "The Wizard" that George was behind their daughter's death) where Henry Ross calls George a murderer and Mrs. Ross stating that George knew those envelopes were toxic as Judge Vandelay had to do an "order in the court" to break up the commotion. George is last seen looking quite upset at himself for treating Susan rather negatively.

* Sidra Holland (Teri Hatcher), the woman Jerry dated from the health club in "The Implant," recounts an incident in 1993 when Elaine tripped in the sauna and landed on her breasts. She claimed that Jerry had sent Elaine into the sauna to find out if her breasts were real or implants. When Sidra entered the courtroom, Jackie Chiles developed a crush on her.

* Lt. Joe Bookman (Philip Baker Hall), the library cop from "The Library", talks about what a library cop does and mentions about how Jerry had a 25-year-overdue library book. He calls Jerry a "criminal" which is what a "delinquent" for 25 years have been called.

* Robin (Melanie Chartoff), a comedy club waitress and George's girlfriend from "The Fire," recounts her child's birthday party where a flashback shows George cowardly and selfishly pushing children and an elderly woman out of the way to escape a small kitchen fire is shown. A deleted scene had Robin also answering yes to the question if her mother was also knocked down during the incident.

* The Garden Valley Shopping Mall Security Guard from "The Parking Garage" (David Dunard) testifies about catching Jerry urinating in public and his reasoning for doing so. The flashback showed Jerry telling the security guard that "I could get uromysitisis poisoning and die, that's why." District Attorney Hoyt quotes "Uromycitisis! I wonder if they're having any trouble controlling themselves during this trial? Perhaps these two hooligans would like to have a pee party right here in the courtroom!" When Jackie Chiles objects stating that his clients' bathroom problems are not an issue of the trial, Judge Vandalay tells him to sit down when Jackie tries to refer to the Disability Act of 1990.

* Detective Hudson (James Pickens, Jr.) refers to an incident that took place at the end of "The Wig Master" due to an odd set of circumstances that led to Kramer being mistaken for a pimp and getting arrested by the police.

* Kramer's ex-girlfriend Leslie the Low-Talker (Wendel Meldrum) from "The Puffy Shirt" is apparently there to testify against Jerry for bad-mouthing the puffy shirt she had him promote on The Today Show. Jackie Chiles objects because Leslie is a low-talker and that nobody can hear her. He tells Judge Vandalay to either get Leslie a microphone or they should move on with the trial. A deleted scene had District Attorney Hoyt getting close enough to hear that Jerry's negative comment ruined her business prior to Jackie Chiles' objection.

* George's former boss with the New York Yankees George Steinbrenner (played by Lee Bear and voiced by Larry David) recalls how he was rumored to be a Communist in "The Race", but did not state how he was traded for Tyler Chicken in "The Muffin Tops". Frank Costanza stands up and shouts to Steinbrenner "How could you give $12,000,000.00 to Hideki Irabu?" Judge Vandelay had to do an "order in the court" to break up the argument.

* Marcelino (Miguel Sandoval), the sleazy grocer and cockfighting ringleader from "The Little Jerry", has his testimony simply consisting of him saying "cockfighting" in response to a question from District Attorney Hoyt.

* Roger Hoffman (David Byrd), the Pharmacist from "The Sponge," testifies about the event on the night of December 7, 1995 where Elaine came into Pasteau Pharmacy and "said she needed a whole case" of Today sponges shortly after they were pulled from the market. Roger also added that the sponges in question are not "the kind you clean your tub with. They're for sex." This caused everyone to groan upon hearing this testimony. A deleted scene had him also stating that Elaine was agitated and desperate at the time.

* Elaine's ex-boyfriend and co-worker Fred (Tony Carlin) from "The Pick" testifies about Elaine's accidental nipple exposure in a Christmas card on December 1992. A deleted scene had Elaine stating that it was inadvertent and that Kramer took the picture for the Christmas card as Kramer claimed that the lighting called for it. Judge Vandelay banged his gavel.

* Elaine's former boss Justin Pitt (Ian Abercrombie) mentioned that he fired her over a misunderstanding in "The Diplomat's Club". Pitt even believed Elaine was trying to kill him and testifies she tried to smother him with a pillow. A deleted scene prior to this accusation had him mentioning how he hired Elaine where she worked for him for September 1994 to May 1995. When Elaine shouted to Pitt that her trying to smother him with the pillow wasn't true, Judge Vandelay banged his gavel. When asked by District Attorney Hoyt on why she tried to kill him, Pitt stated that she and Jerry had somehow found out that she was in his will.

* Yev Kassem The Soup Nazi (Larry Thomas) is first asked to spell his name, which he refuses to do. Then he testifies that the four used to come into his soup shop, mentioning that George didn't know how to order right. He even banned Elaine from the soup shop for a year. Elaine later found soup recipes in an old armoire that once belonged to the Soup Nazi and in an act of revenge reveals his recipes to the public. This caused the Soup Nazi to close his soup shop and move to Argentina while mentioning that Elaine ruined his business. When Elaine whispers to Jerry, Kramer, George, and Jackie that his soup wasn't good anyway, Yev stands up and shouts "WHAT DID YOU SAY?!"

* Babu Bhatt (Brian George), a former Pakistani restaurateur who appeared in "The Cafe" and "The Visa", was brought back into the United States where he retells the story of how Jerry's advice to change the menu of his restaurant "The Dream Cafe" from varied to Pakistani caused his customer base to dry up. Then he charges that Elaine and Jerry purposely mixed up his mail so he did not get his visa renewal papers and was deported back to Pakistan. Babu ended his testimony by quoting "All they do is mock me, just like they did the fat fellow. All the time. Mocking, mocking, mocking, mocking, mocking. All the time! Now it is Babu's turn to mock. Finally I will have some justice. Send them away! Send them all away! Lock them up forever! They are not human. Very bad! Very, very, very bad!" Babu then waves his index finger at them. Babu's story is only partially true where his restaurant did not get any business except for Jerry's patronage before he changed the menu, and his renewal papers were accidentally delivered to Jerry's house while Jerry was out of town. When back in Pakistan, Bhatt had stated to a friend that he made a promise that he would go back to the US one day to have his revenge on Seinfeld.

As the jury goes over the evidence, Geraldo Rivera and Jane Wells recap to those watching their show about what was heard during the trial. Jane Wells even stated that the testimonies went into the night until Judge Vandelay has decided that he has heard enough. It was also mentioned that the closing arguments have occurred and that the jury has been deliberating for four and a half hours. Jerry, Elaine, George, and Kramer hope that Jackie Chiles would get them acquitted. Estelle enters Judge Vandelay's office in order to get him to reduce the punishment for her son if he is found guilty by doing something for him. Judge Vandelay asks "What do you mean"' Estelle says "You know."

Everyone else is seen killing time awaiting for the jury to be done:

* Mabel Choate, Justin Pitt, Marla Penny, Marcelino, Joe Bookman, Dr. Wexler, Jay Crespi, and Stu Chermak are standing in the halls of the courthouse.

* Rabbi Glickman is reading something from his book to Jerry's parents and Frank Costanza.

* J. Peterman, Keith Hernandez, Kenny Bania, and Mickey Abbot are playing pool for money at the bar.

* David Puddy is lying under a tree using a tanning mirror on his face.

* Matt Wilhelm is trying to break up an argument between George Steinbrenner and a waiter.

* Newman is eating food in his car while lying down in his back seat.

* Yev Kassem is seen outside a building serving some of his soup to Babu Bhatt, Robin, Mr. Lippman, and Poppie. When Poppie is seen asking for some salt for his soup, Yev Kassem does his "No Soup for You" gesture and takes away his soup and spoon.

* Henry Ross is seen buying a gun.

* Jackie Chiles is in bed with Sidra Holland until he gets a call that the jury has reached a verdict.

The jury re-enters the courtroom. When Kramer claims that a woman on the jury is smiling at them, Jerry tells him that she's smiling at them because they might go to prison. Everyone rises when Judge Vandelay enters the courtroom. When it comes to the verdict, the forewoman of the jury (Myra Turley) states that the jury finds Jerry, George, Elaine, and Kramer guilty of criminal indifference. Mr. and Mrs. Ross and the testifiers are pleased with the verdict, Estelle faints, and Newman has a brief choking moment from laughing while eating food. Judge Vandelay breaks up the commotion by threatening to clear the courtroom if they didn't stop.

He then says to the four "I do not know how, or under what circumstances the four of you found each other, but your callous indifference and utter disregard for everything that is good and decent has rocked the very foundation upon which our society is built. I can think of nothing more fitting than for the four of you to spend a year removed from society so that you can contemplate the manner in which you have conducted yourselves. I know I will." Judge Vandalay adjourns the court and takes his leave from the courtroom as George angrily quotes to Kramer "You had to hop! You had to hop on the plane!" As everyone starts to leave, Elaine tells David not to wait for her to which he indifferently says "Alright." Frank tries to wake up Estelle from her fainting so that they can beat the traffic as Uncle Leo comforts Babs in the background. Before leaving with Sidra, Jackie Chiles tells the four that he may have lost the case, but he did get satisfied with Sidra while commenting "And by the way: they're real, and they're spectacular!"

In the final scene before the credits, the four main characters sit in their holding cell awaiting their prison transport. Kramer is finally able to get the water out of his ears after days of trying. Elaine decides that she's going to use her one phone call from prison to call Jill, saying that the prison call is the "king of calls". Jerry begins a conversation about George's shirt buttons, using lines from the first episode. George then wonders if they've had that conversation before, which Jerry acknowledges.

During the credits at the Latham County Prison, Jerry is wearing a Latham County orange jumpsuit and performing a stand-up routine of prison-related jokes to an audience of fellow prisoners (including Kramer and George; Elaine is not seen as she is in the women's section of the prison). No one is laughing except for the studio audience and Kramer. As Jerry is then escorted off the stage by a prison guard (Jon Hayman) for talking back at a heckler, he says to his mostly hateful audience, "Hey, you've been great! See you in the cafeteria!" as the audience jeers, and Kramer gives him a standing ovation.

## Broadcast and reception

The top price for a 30-second commercial during the U.S. broadcast was approximately $1 million, marking the first time ever on American television history that a regular primetime television series (as well as a non-sport broadcast) had commanded at least $1-million advertising rate (previously attained only by Super Bowl general telecasts).

In its original American broadcast, 76.3 million U.S. television viewers tuned into "The Finale", becoming the fifth most watched overall series finale in the U.S. after M*A*S*H, Roots, Cheers and The Fugitive. When this episode originally aired on NBC, TV Land paid tribute by not programming any shows opposite it, instead just showing a still shot of a closed office door with a pair of handwritten notes that said "We're TV Fans so... we're watching the last episode of Seinfeld. Will return at 10pm et, 7pm pt."

Although the finale of Seinfeld enjoyed a huge audience during the May 1998 telecast, it received polarized reviews and was criticized by many for portraying the main characters as people with no respect for society, and for mocking the audience who tuned in to watch them every week. Entertainment Weekly's Ken Tucker seemed to echo this sentiment in declaring the episode "off-key and bloated...Ultimately, Seinfeld and David's kiss-off to their fans was a loud, hearty, 'So long, suckers!'" Others valued it for the large number of cameo appearances from past episodes, as well as the perceived in-joke of the four characters being convicted and imprisoned on the charge that they did nothing, a play on the "show about nothing" mantra.

The night before "The Finale" aired, competing ABC television show Dharma & Greg aired the episode "Much Ado During Nothing". Their story centered around their title characters trying to win back a duck lawn ornament from Dharma's friend Jane by doing the most daring sexual act in public. After getting caught by the police once, they devise a scheme sure to succeed. Their plan centers on them "doing the deed" while the final episode is airing, saying that "...everybody in the country is going to be watching the last episode of Seinfeld."

Although Larry David has stated he has no regrets about how the show ended, a 2010 Time article noted that the Seinfeld reunion during the seventh season of Curb Your Enthusiasm "was viewed by many as his attempt at a do-over." This was also referenced by Jerry in the seventh-season finale of Curb Your Enthusiasm, saying "We already screwed up one finale" with David responding "we didn't screw up a finale, that was a good finale!" Having said that, during a Seinfeld roundtable reunion discussion, Larry admitted to understanding the disappointment and said if he were to redo it he would have kept the plot of the finale less of a secret, which only heightened expectations.

In 2011, the finale was ranked No. 7 on the TV Guide Network special, TV's Most Unforgettable Finales.

## Syndication version

This version had cut several parts from the original episode (US) or rearranged some parts:

* In the plane:
  * When the plane was falling, Elaine said to Jerry "Jerry, I've always loved...", but in the syndication version, "loved..." is cut.

* In the trial:
  * The testimonies of Donald Sanger, George Steinbrenner, and Detective Hudson.

  * The scene between Jerry and Elaine before the jury re-enters the courtroom.

* The scene with Jerry, Elaine, George and Kramer having a meal in their cell was used for the credits of the first part.

* Jerry's opening stand-up comedy act, which was the first since the finale of Season 7.

## Deleted scenes

The scenes that were cut are now available on DVD.

* First half

  * The scene with Kramer in the car is much longer than originally aired.

  * Dialogue between Jerry and George in a taxi.

  * The coffee shop scene with Jerry, Elaine, Kramer and George is much longer, examining more locations than before.

* Second half

  * The exchange between the minor cast members before the jury is cut down. It consists of Keith Hernandez telling Newman that he has been doing some announcing, Rabbi Glickman talking to Frank and Estelle Costanza and Morty and Helen Seinfeld about enlightenment, Uncle Leo meeting Babs Kramer, J. Peterman meeting David Puddy, and Mickey Abbot telling Kenny Bania that he took his seat.

  * Jackie Chiles' opening dialogue is much longer than aired. It had him mentioning that the criminal is also lying and laughing at this while the defendants are lucky to be alive from the airplane's emergency landing. He also claimed that he can get the pilot of the airplane to the court to testify on their behalf.

  * Justin Pitt being called to the stand was featured as well as an extended version of his testimony.

  * The extended testimonies of Robin, Leslie, Roger Hoffman, and Fred.

  * Testimonies from Arnold Deensfrei, Detective Blake, Mr. Lippman, Ramon, and Poppie.

  * Jerry's final standup is much longer than before.

* An alternate ending was also filmed. The jury re-enters the courtroom. When Kramer claims that a woman on the jury is smiling at them, Jerry tells him that she's smiling at them because they might go to prison. When it comes to the verdict, the forewoman of the jury states that the jury finds Jerry, George, Elaine, and Kramer not guilty of criminal indifference which disappoints the testifiers and leaves Judge Vandelay speechless. This ending can be found as an Easter egg on the "Seinfeld" season 9 DVD set.

## Cast

### Part 1

#### Regulars

Jerry Seinfeld ...................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus .............. Elaine Benes  
Michael Richards .................. Cosmo Kramer

#### Recurring

Wayne Knight ................ Newman  
Jerry Stiller ..................... Frank Costanza  
Estelle Harris .................. Estelle Costanza  
Liz Sheridan .................... Helen Seinfeld  
Barney Martin ................. Morty Seinfeld  
Steve Hytner ................. Kenny Bania  
Len Lesser ...................... Uncle Leo  
Phil Morris ....................... Jackie Chiles  
John O'Hurley ................. J. Peterman  
Patrick Warburton ......... David Puddy  
Danny Woodburn .......... Mickey Abbott

#### Guests

Peter Blood .......................... Jay Crespi  
David Byrd .......................... Pharmacist  
Steve Carlson ....................... Captain Maddox  
David Dunard ...................... Guard  
Donna Evans ........................ Woman  
Geoffrey C. Ewing ............... Bailiff  
Warren Frost ........................ Mr. (Henry) Ross  
Keith Hernandez .................. Himself  
Scott Jaeck ............................ Officer Vogel  
Wendle Josepher .................. Susie  
Robert Katims ...................... Mr. Deensfrei  
Scott Klace ........................... Guard  
Bruce Mahler ....................... Rabbi Glickman  
Ed O'Ross ............................ Det. Blake  
Kevin Page ........................... Stu Chermak  
James Pickens Jr. .................. Det. Hudson  
John Pinette ......................... Howie  
Victor Raider-Wexler ........... Dr. Wexler  
James Rebhorn .................... D.A. Hoyt  
Peter Riegert ....................... Kimbrough  
Geraldo Rivera .................... Himself  
McNally Sagal ...................... Carol  
Gay Thomas ........................ O'Neal  
Myra Turley ........................ Foreman  
Jane Wells ............................ Herself  
Grace Zabriskie ................... Mrs. Ross  
Van Epperson ...................... Passerby  
Jeff Johnson ......................... Criminal  
Sean Moran ......................... Man  
Dianne Turley Travis ........... Receptionist  
Jim Zulevic ........................... Bernie

### Part 2

#### Regulars

Jerry Seinfeld ...................... Jerry Seinfeld  
Jason Alexander .................. George Costanza  
Julia Louis-Dreyfus .............. Elaine Benes  
Michael Richards .................. Cosmo Kramer

#### Recurring

Wayne Knight ................ Newman  
Jerry Stiller ..................... Frank Costanza  
Estelle Harris .................. Estelle Costanza  
Liz Sheridan ................... Helen Seinfeld  
Barney Martin ................ Morty Seinfeld  
Ian Abercrombie ............. Mr. Pitt  
Richard Fancy ................. Lippman  
Richard Herd .................. Wilhelm  
Steve Hytner ................. Kenny Bania  
Len Lesser ...................... Uncle Leo  
Phil Morris ....................... Jackie Chiles  
John O'Hurley ................. J. Peterman  
Patrick Warburton ......... David Puddy  
Danny Woodburn ......... Mickey Abbott

#### Guests

Stanley Anderson ................ Judge Vandelay  
Frances Bay ......................... Mrs. Choate  
Peter Blood .......................... Jay Crespi  
David Byrd .......................... Pharmacist  
Tony Carlin .......................... Co-Worker  
Maggie Egan ........................ Ticket Clerk  
Steve Carlson ....................... Captain Maddox  
Melanie Chartoff .................. Robin  
Brian Doyle-Murray ............. Mel Sanger  
David Dunard ...................... Guard  
Geoffrey C. Ewing ............... Bailiff  
Warren Frost ....................... Mr. (Henry) Ross  
Brian George ....................... Babu Bhatt  
Philip Baker Hall .................. Mr. Bookman  
Teri Hatcher ........................ Sidra  
John Hayman ...................... Bubble Boy  
Keith Hernandez ................. Himself  
Carlos Jacott ........................ Pool Guy  
Scott Jaeck ........................... Officer Vogel  
Wendle Josepher ................. Susie  
Robert Katims ...................... Mr. Deensfrei  
Scott Klace ........................... Guard  
Jane Leeves ......................... Marla  
Bruce Mahler ....................... Rabbi Glickman  
Wendel Meldrum ................. Low-Talker  
Sheree North ....................... Babs  
Ed O'Ross ............................ Det. Blake  
Kevin Page ........................... Stu Chermak  
James Pickens Jr. .................. Det. Hudson  
John Pinette ......................... Howie  
Victor Raider-Wexler ........... Dr. Wexler  
James Rebhorn ..................... D.A. Hoyt  
Geraldo Rivera ..................... Himself  
McNally Sagal ....................... Carol  
Miguel Sandoval ................... Marcelino  
Reni Santoni .......................... Poppie  
Gay Thomas ......................... O'Neal  
Larry Thomas ....................... Soup Nazi  
Myra Turley ......................... Foreman  
Jane Wells ............................. Herself  
Grace Zabriskie .................... Mrs. Ross  
Van Epperson ...................... Passerby  
Dianne Turley Travis ........... Receptionist  
Jim Zulevic ........................... Bernie

## Script

[Opening Monologue]

It seems like whenever these office people call you in for a meeting, the whole thing is about the sitting down. I would really like to sit down with you. I think we need to sit down and talk. Why don'tyou come in, and we'll sit down. Well, sometimes the sitting down doesn't work. People get mad at the sitting.You know, we've been sitting here for I don't know how long. How much longer are we just going to sit here? I'll tell you what I think we should do. I think we should all sleep on it. Maybe we're not getting down low enough. Maybe if we all lie down, then our brains will work.

[Jerry and George at Monks]

GEORGE: I can't eat this without catsup. Would it kill her to check up on us? Would that be a terrible thing? &quot;How's everything? Do you need anything? What can I do for you?&quot;

JERRY: I know what you mean.

GEORGE: Do ya?

JERRY: It's like going out with someone and you never hear from them again.

GEORGE: Same thing!

JERRY: Not really, but it's something. Ask the people behind you.

GEORGE: Excuse me. Are you using your catsup?

WOMAN: What do you think? You want to give him the catsup?

MAN: It's up to you.

WOMAN: You know what? I don't think so. I'm going to need it from time to time.

JERRY: So what are you doing later? You want to go to the movies?

GEORGE: Nah - what for?

JERRY: To see a movie.

GEORGE: I've been to the movies.

JERRY: Not this movie.

GEORGE: They're all the same. You go, you sit, you eat popcorn, you watch. I'm sick of it.

JERRY: Did you shower today?

GEORGE: Yeah.

JERRY: That's usually the kind of mood I'm in when I haven't showered.

GEORGE: When is it going to be my turn, Jerry? When do I get my 15 minutes? I want my 15 minutes!

JERRY: Oh, quit complaining. At least you have your health.

GEORGE: Ah! Health's not good enough. I want more than health. Health's not doing it for me anymore. I'm sick of health.

WOMAN: Alright, we're done. You can have it now.

GEORGE: Oh, very gracious.

[Jerry and George are walking down the sidewalk. Kramer and Elaine drive up.]

MAN: Nice day

GEORGE: Yeah.

JERRY: What is that?

KRAMER: Hey! JoJo!

JERRY: Ey, ey!

ELAINE: Alright, thanks for the ride, Kramer.

KRAMER: No, thank you. So what are you doing?

JERRY: Nothing.

KRAMER: Come on, let's go to the beach.

GEORGE: What are you crazy?

KRAMER: What? It's a beautiful day.

JERRY: Have a good time.

KRAMER: Yeah, there's something in the air today. You feel it? There's something in the air.

JERRY: You know you're turning into Burt Lancaster?

KRAMER: Yeah, there's something in the air.

(Kramer drives off)

ELAINE: Oh, I forgot to call Jill. Jill. Hi, it's Elaine. How is your father? Is everything okay? What? I can't hear you so good. There's a lot of static. Wha? I'm going to call you back.

JERRY: Jill's father is in the hospital and you call to ask about him on a cell phone?

ELAINE: What? No good?

JERRY: Faux pas.

ELAINE: Faux pas?

GEORGE: Big hefty stinking faux pas.

ELAINE: Why?

JERRY: You can't make a health inquiry on a cell phone. It's like saying &quot;I don't want to take up any of my important time in my home so I'll just get it out of the way on the street.&quot;

GEORGE: On-the-street cell-phone call is the lowest phone call you can make.

JERRY: It's an act of total disregard. It's selfish.

GEORGE: It's dismissive.

JERRY: It's pompous.

GEORGE: Why don't you think before you do something?

ELAINE: Here's a thought - Bye bye.

(Elaine leaves)

GEORGE: Too much?

[Jerry and George enter Jerry's apartment]

GEORGE: Boy - I'm really surprised at Elaine - that whole phone business - she should know better than that.

JERRY: Hey - hey - hey!

GEORGE: What?

JERRY: Where do you think this relationship is? If you are thinking of instituting an open-door urination policy, let me disabuse you of that notion right now, my friend.

GEORGE: You're so uptight.

JERRY: Uptight? Let's all just have a big pee party. Hey everybody, grab a bucket. We're going up to Jerry's. It's a pee party.

[Jerry listens to his answering machine]

PHONE TAPE: Jerry, this is Elizabeth Clark calling from James Kimbrough's office at NBC. Could you please give us a call?Thanks.

JERRY: Hello. Yeah, hi, this is Jerry Seinfeld calling for James Kimbrough. Hello? Hi? Uh huh, really, uh, no problem, definitely, ok, buhbye. That was James Kimbrough.

GEORGE: Who's he?

JERRY: He is the new president of NBC. He wants to sit down with us and talk about &quot;Jerry.&quot;

GEORGE: Our show, &quot;Jerry&quot;?

JERRY: Right.

GEORGE: &quot;Jerry&quot;, oh my God. He wants to talk about &quot;Jerry&quot;?

JERRY: Yeah!

GEORGE: When?

JERRY: Today, like right now.

GEORGE: Right now? &quot;Jerry&quot;?

JERRY: &quot;Jerry&quot;!

GEORGE: He wants to talk about &quot;Jerry&quot;?

JERRY: He wants to talk about &quot;Jerry&quot;!

GEORGE: &quot;Jerry&quot;!

JERRY: &quot;Jerry&quot;!

GEORGE: Can I go like this?

JERRY: Sure!

GEORGE: No sports jacket? I don't need a sports jacket? Writers wear sports jackets.

JERRY: Forget the sports jacket.

GEORGE: I won't feel like a writer.

JERRY: You're not a writer.

GEORGE: Right!

[Jerry and George are sitting in a waiting room at NBC]

GEORGE: Water. Need some water! Water here!

JERRY: Ok, now listen, I don't want any scenes in here like the last time.

GEORGE: Don't worry, don't worry, no scenes.

JERRY: Don't blow this.

GEORGE: I will not blow this.

JERRY: If he says he doesn't want it to be a show about nothing, don't go nuts.

GEORGE: It's fine, it doesn't have to be about nothing.

JERRY: He might not want nothing.

GEORGE: Something, nothing, I could care less.

JERRY: He might want a show about anything and everything.

GEORGE: Anything, everything, something, nothing - Who the hell cares? Put me down. I'm down!

JERRY: Alright.

RECEPTIONIST: Mr. Kimbrough is ready to see you

GEORGE: Magic time.

JERRY: What?

GEORGE: Nothing

[Jerry and George are escorted into Mr. Kimbrough's office]

RECEPTIONIST: Mr. Kimbrough.

STU: Hey, Jerry, good to see you.

GEORGE: Hey, hey, hey!

STU: How you been?

JERRY: Good, good. You remember George.

STU: George, good to see you.

GEORGE: Hello Stu.

STU: You remember Jay Crespi.

GEORGE: Jay Crespi, how am I gonna forget Jay Crespi?

STU: This is James Kimbrough.

KIMBROUGH: Nice to meet you, pleasure, thanks for coming in.

GEORGE: Kimbrough.

JERRY: Don't spell.

GEORGE: K-I-M-B-R-O-U-G-H

KIMBROUGH: That's right.

GEORGE: It's a talent I have.

KIMBROUGH: Why don't we sit down, glad you're here.

GEORGE: Woo! Some day out there - You ever see weather like that? Woo! It's crisp - it's crispy crisp.

JERRY: Shut up, George.

KIMBROUGH: Can I get you anything?

GEORGE: What do we have in the fruit department?

JERRY: Oy.

STU: Pineapple.

GEORGE: Oh, that's a dangerous fruit. It's like a weapon that thing, got spikes on the end. You can get killed from one of those things.

KIMBROUGH: Anyway, let me tell you why I called. When I took over here last month, I reviewed what was in development,and it was pretty much same old, same old.

GEORGE: Been there, done that.

KIMBROUGH: Right. I was looking for something different. Something that would have people talking at the water coolers.

GEORGE: Water coolers?

CRESPI: We call it a water-cooler show.

JERRY: Because the next day in the offices, people gather around the water coolers to talk about it, right?

GEORGE: See, I think people would talk about it at the coffee machines.

JERRY: Well it's probably just easier to say &quot;water cooler show&quot; than &quot;coffee machine show.&quot;

GEORGE: It's really not accurate. Nobody drinks from a water cooler any more - they use bottles.

JERRY: But I think Mr. Kimbrough makes a good point.

KIMBROUGH: Anyway, Stu here started telling me about a show, &quot;Jerry&quot;, that he developed five years ago.

STU: I have always loved it.

KIMBROUGH: He said it was a show about nothing. So, I saw the pilot and I've got to tell you - I flipped out.

CRESPI: He totally flipped out.

KIMBROUGH: What I want to do is put it on the air. 13-episode commitment. Start it off on Wednesday night, build up an audience. This show needs time to grow. I love that Kramer guy.

JERRY: He's a little off the wall.

CRESPI: Oh yeah.

STU: Kramer.

KIMBROUGH: And Elaine - I wouldn't mind seeing something happening between you two.

JERRY: Definitely.

GEORGE: I tell you, I really don't think so-called relationship humor is what this show is all about.

KIMBROUGH: Or we could not do the show altogether, how about that?

GEORGE: Or we could get them together. Woo!

[George and Jerry attempt a hug outside Mr. Kimbrough's office]

GEORGE: Yeah!

JERRY: Yeah!

[Elaine is at home using the phone when Jerry calls to tell her the news]

ELAINE: Jill, hi, it's Elaine. Well, I'm calling from my home. Indoors. Well, I was just calling to see how your fa.. I'm sorry, I'm getting another call. Hang on just a second. Hello?

JERRY: Hi. Elaine, it's me.

ELAINE: Jerry, I'm on the other line.

JERRY: No no - this is an emergency - get off the phone.

ELAINE: I'm sorry, Jill. I'm going to have to take this call. Jerry, what's the emergency?

JERRY: The &quot;Jerry&quot;'s back on - the TV show! George and I are moving to California!

ELAINE: That's the emergency?

JERRY: Did you hear what I said?

ELAINE: I was on the other line talking to Jill.

JERRY: Jill? Well, why didn't you say so?

ELAINE: You said it was an emergency.

JERRY: So now she's lost a phone face-off? That's even worse than your cell phone walk-and-talk.

[Jerry is telling his parents the good news about Jerry]

HELEN: Congratulations, they're doing the show.

MORTY: They should have put that show on 5 years ago. Bunch of idiots at that network. Can I tell you something, Jerry? It's all crap on TV. The only thing I watch is Xena the Warrior Princess. She must be about six-six.

HELEN: She's not six-six.

MORTY: Jerry, you ever watch that?

JERRY: Yeah, it's pretty good.

[George is telling his parents the news.]

ESTELLE: They picked up the show?

GEORGE: I'm moving to California.

FRANK: Oh baby-doll, this kid's going places, I told you.

ESTELLE: The NBC guy liked it?

GEORGE: Of course he liked it.

ESTELLE: He told you he liked it?

GEORGE: He wouldn't put it on if he didn't like it.

ESTELLE: Well, what are you doing?

GEORGE: I'm writing.

ESTELLE: You know how to write?

FRANK: Without the writing, you have nothing. You're the ones that make them look good.

ESTELLE: Since when do you know how to write? I never saw you write anything.

GEORGE: Ma?!

ESTELLE: I don't know how you're going to write all those shows. And where are you get all the ideas?

FRANK: Would you leave him alone? You'll shatter his confidence!

GEORGE: I don't need any ideas. It's a show about nothing.

ESTELLE: Nothing. Please. I'll tell you the truth - the whole thing sounds pretty stupid to me.

[Jerry's Apartment]

(Jerry is on the phone with his agent, Kramer walks in.)

JERRY: NBC is letting me use their private jet? And I can go anywhere I want? That's fantastic! Thanks. Great. Okay, bye.

KRAMER: Oh hey!

JERRY: Hey - how was the beach?

KRAMER: Oh, you missed it, buddy - lot of femininas - some major femininas

JERRY: I had a little meeting today at NBC. What are you doing?

KRAMER: You know, I went swimming and I can't get this water out of my ear.

JERRY: So do you remember five years ago, we did that pilot, &quot;Jerry&quot;? Well, the new guy at NBC wants to do it. They're putting it on the air! They're giving us a 13-episode commitment. George and I are moving to California!

KRAMER: You're moving to California?

JERRY: Yeah, only for a while.

KRAMER: Yeah, but Jerry, what happens if the show's a hit? You could be out there for years! You might never come back.

JERRY: No, I'll be back.

KRAMER: Jerry. It's L.A. Nobody leaves. She's a seductress, she's a siren, she's a virgin, she's a whore.

JERRY: And my agent said as a bonus, I can use their private jet, so we'll all go somewhere - the four of us, one big fling before George and I go to California.

KRAMER: Fling!

[The group is sitting at their table at Monks]

ELAINE: So we can go anywhere we want?

JERRY: Anywhere.

ELAINE: Why are they doing this?

JERRY: I think they want to make it up to us cause they let this thing sit on their shelf for five years.

ELAINE: This is all very exciting.

GEORGE: So? Where are we going?

KRAMER: I say Japan.

ELAINE: Why Japan?

KRAMER: Oh - geishas - they cater to your every whim. They're shy at first, but they're quite skilled at conversation. They can discuss anything from world affairs to the fine art of fishing - or baking.

ELAINE: Oh - I got it - how about Russia?

JERRY: Russia, it's so bleak.

ELAINE: It's not bleak - it's springtime.

JERRY: It's still bleak.

ELAINE: You can't be bleak in spring.

JERRY: You can be bleak in spring.

GEORGE: If you're bleak, you're bleak.

ELAINE: What about Switzerland?

KRAMER: Oh - Switzerland - the Von Trapp family, huh?

GEORGE: It's a bit hilly - no?

ELAINE: You're not going to do any walking.

GEORGE: What if I want to walk around a little?

ELAINE: So then you'll walk down the hill and we'll pick you up.

GEORGE: What if I'm at the bottom?

ELAINE: Alright! You know what, just forget it!

JERRY: Alright - come on - come on now, people. Let's face it, we're not all going to agree on anything. Why don't we all just go to Paris?

ELAINE: I'll go to Paris.

GEORGE: Me too.

KRAMER: Oh yeah - oui oui.

JERRY: So that's it - it's settled, we're going to Paris.

(they put theirs hands in the middle)

GROUP: Yeah!

[Jerry's Apartment]

(Elaine enters)

ELAINE: Hey. NBC limo is downstairs - beep beep beep. {NBC tune} I'm just going to call Jill one more time before we go.

JERRY: Wait, you can't make a call like that on your way out. You can't rush that conversation.

ELAINE: Well, I can't call from the limo. Can I call from the plane?

JERRY: First you make a cell-phone walk-and-talk, then she loses a call-waiting face-off, now you're talking about a plane call?

ELAINE: Alright, I'll just have to call her from Paris.

(Knock at the door. Jerry answers.)

NEWMAN: Hello, Jerry.

JERRY: Hello, Newman. What gives?

NEWMAN: I was speaking earlier with Kramer and he mentioned something about a private jet to Paris?

JERRY: Yeah, that's right.

NEWMAN: Well, I hear it's quite beautiful there this time of year, and of course you know I'm one-quarter French.

JERRY: Really.

NEWMAN: Oh yes, in fact I still have family there. This probably won't interest you, but I have a cousin there who's suffering very badly. She's lost all use of her muscles. She can only communicate by blinking. I would so love to see her - bring a ray of sunshine into her tragic life. But alas, I can't afford it, for I am, as you know, but a simple postal worker.

JERRY: That's a shame.

NEWMAN: Take me! Take me!

JERRY: Oh, forget it. Pull yourself together. You're making me sick. Be a man!

NEWMAN: Alright! But hear me and hear me well - The day will come. Oh yes, mark my words, Seinfeld - your day of reckoning is coming. When an evil wind will blow through your little playworld, and wipe that smug smile off your face. And I'll be there, in all my glory, watching - watching as it all comes crumbling down.

[The group arrives by limo to the airport]

CAPTAIN: Ah, Jerry?

JERRY: Yeah.

CAPTAIN: I'm Captain Maddox this is my co-pilot, Kurt Adams. Ready to go to Paris?

JERRY: All set. We'll just grab the bags.

CAPTAIN: Don't worry about that. We'll take care of them for you.

JERRY: Just keeps on getting better and better.

(Our heroes enter the plane)

JERRY: Not bad.

ELAINE: Wow!

KRAMER: The only way to fly.

GEORGE: This is it?

(After take-off, the group chit-chats)

GEORGE: I'm sorry - I have to say, I'm a little disappointed, I thought it would be a lot nicer.

JERRY: You're complaining about a private jet?

GEORGE: You think this is the plane that Ted Danson gets?

JERRY: Ted Danson is not even on the network anymore.

GEORGE: Still, I bet when they gave him a plane, it was a lot nicer than this one.

ELAINE: Will you shut up? You are ruining the whole trip.

GEORGE: This is a real piece of junk. I don't even feel safe on this thing. I have a good mind to write a letter to Mr. Kimbrough.

JERRY: You're not writing any letters!

ELAINE: Will you turn around?

GEORGE: Why?

ELAINE: You are annoying me sitting like that. It's effeminate.

GEORGE: It's effeminate to sit like this?

ELAINE: Yes, I think it's a little effeminate.

GEORGE: How is this effeminate?

ELAINE: I don't know - it just is.

GEORGE: Kramer, what are you doing?

JERRY: Still got water in your ear?

KRAMER: Can't get rid of it. Maybe it leaked inside my brain.

GEORGE: Would you stop that? It's not safe to be jumping up and down on a plane.

KRAMER: I got to get it out, I can't take this anymore.

GEORGE: Kramer, don't be fooling around up here.

GEORGE: Kramer!

CAPTAIN: Hey, get the hell out of here!

ELAINE: What is that?

GEORGE: Oh my God!

ELAINE: What is that noise? What is that noise?

JERRY: Kramer, what the hell did you do?

KRAMER: I lost my balance.

ELAINE: Oh my God!

ELAINE: What's going on?

JERRY: Kramer!

KRAMER: It was an accident.

GEORGE: I told you to stop with the hopping.

ELAINE: Oh my God, we're going down. We're going to die!

GEORGE: Just when I was doing great. I told you God wouldn't let me be successful.

JERRY: Is this it? Is this how it ends? It can't- it can't end like this.

KRAMER: I'm ready! I'm ready! Glory hallelujah!

GEORGE: Jerry? Jerry can you hear me?

JERRY: Yeah.

GEORGE: There's something I have to tell you.

JERRY: What? What is it?

GEORGE: I cheated in the contest.

ELAINE: What?

JERRY: What?

GEORGE: The contest - I cheated.

JERRY: Why?

GEORGE: Because I'm a cheater! I had to tell you.

JERRY: Great - I won.

ELAINE: Jerry, I gotta tell you something too.

JERRY: Yeah, Elaine I got something I want to say to you.

ELAINE: No no - me first.

JERRY: Alright.

ELAINE: Jerry, I've always loved...

GEORGE: Hey - What's going on?

KRAMER: We're straightening out!

ELAINE: We're straightening out?

JERRY: We're straightening out!

GEORGE: We're straightening out!

GROUP: Yeah!

[Outside the plane]

CAPTAIN: Well, again, sorry about that little mishap. But once you get everything checked out there shouldn't be anymore problems.

JERRY: Where are we?

CAPTAIN: Latham, Massachusetts. Why don't you take a cab into town, get yourself something to eat. I got your beeper number - I'll beep you as soon as we're ready.

JERRY: Okay.

ELAINE: Okay.

JERRY: We'll see you later.

[In front of a store, in Latham, Massachusetts]

ELAINE: Well, what are we going to do about Paris? I mean are we actually going to get back on this plane?

JERRY: I say we go back to New York, and take a regular flight.

GEORGE: I'm not getting on a regular plane now - I'm all psyched up to go on a private jet. No way I'm getting on a regular plane.

ELAINE: Well, I'm sure that they would fly us first class.

GEORGE: First class doesn't make it anymore. Now you get on the phone with Kimbrough, tell him what happened and tell him to get another plane down here, but this time, the good one - the Ted Danson plane.

JERRY: Alright, I'll feel him out.

GEORGE: Yeah, just tell him to hurry it up.

STRANGER: Nice day.

JERRY: Another one?

(A carjacking takes place in front of the group)

ROBBER: Alright fatso, out of the car.

KRAMER: I want to capture this.

ROBBER: Come on! Gimme your wallet.

VOGEL: Don't shoot.

JERRY: Well, there goes the money for the lipo.

ELAINE: See, the great thing about robbing a fat guy is it's an easy getaway. You know? They can't really chase ya!

GEORGE: He's actually doing him a favor. It's less money for him to buy food.

ROBBER: I want your wallet. Come on. Come on, come on.

JERRY: That's a shame. Alright, I'm gonna call NBC.

VOGEL: Officer, he's stealing my car! Officer, I was carjacked. I was held up at gunpoint! He took my wallet, everything!

JERRY: Okay, thanks anyway. They can't get another plane.

KRAMER: Alright, what's wrong with the plane we got? They're just checking it out.

ELAINE: Forget it.

JERRY: No, no, no. We're not getting on there. Come on, let's go get something to eat in Sticksville.

OFFICER: Alright, hold it right there.

KRAMER: What?

OFFICER: You're under arrest.

JERRY: Under arrest? What for?

OFFICER: Article 223-7 of the Latham County Penal Code.

ELAINE: What? No, no - we didn't do anything.

OFFICER: That's exactly right. The law requires you to help or assist anyone in danger as long as it's reasonable todo so.

GEORGE: I never heard of that.

OFFICER: It's new. It's called the Good Samaritan Law. Let's go.

[In a cell, at the Latham County Jail]

ELAINE: The Good Samaritan Law? Are they crazy?

GEORGE: Why would we want to help somebody?

ELAINE: I know.

GEORGE: That's what nuns and Red Cross workers are for.

KRAMER: The Samaritans were an ancient tribe - very helpful to people.

ELAINE: Alright - um, excuse me, hi, could you tell me what kind of law this is.

DEPUTY: Well, they just passed it last year. It's modeled after the French law. I heard about it after Princess Diana was killed and all those photographers were just standing around.

JERRY: Oh, yeah.

ELAINE: Oh, yeah.

DEPUTY: You're the first ones to be arrested on it, probably in the whole country.

GEORGE: Alright, so what's the penalty here? Let's just pay the fine or something and get the hell out of here.

DEPUTY: Well, it's not that easy. Now see, the law calls for a maximum fine of $85,000 and as much as five years in prison.

ELAINE: What?

GEORGE: Oh no no no no - we have to be in California next week. We're starting a TV show.

DEPUTY: California? Oh gosh, I don't think so. Yeah, my guess is you're gonna be prosecuted. Better get yourselves a good lawyer.

[Lawyer Jackie Chiles' office]

CHILES: Who told you to put the cheese on? Did I tell you to put the cheese on? I didn't tell you to put the cheese on.

(The phone rings)

SECRETARY: Jerry Seinfeld on the phone.

CHILES: You people with the cheese. It never ends. Hello? Uh huh. Uh huh. Uh huh. Good Samaritan Law? I never heard of it. You don't have to help anybody. That's what this country's all about. That's deplorable, unfathomable, improbable. Hold on. Suzie, cancel my appointment with Dr. Bison. And pack a bag for me. I want to get to Latham, Massachusetts, right away.

[Hoyt's Office]

HOYT: So they got Jackie Chiles, huh? (Sets down the newspaper) Uh hmm. You know what that means. This whole place is going to be swarming with media by the time this thing is over. You're not going to be able to find a hotel room in this town. The whole country is going to be watching us. Now we got to do whatever it takes to win it, no matter what the cost. The big issue in this trial is going to be character. I want you to find out everything you can about these people - and I mean everything.

[The four are in prison, having a meal]

KRAMER: Mmmm, this is pretty good chow, huh?

GEORGE: Would it kill him to check up on us? No - drops off the meals and that's it. I realize we're prisoners, but we're still entitled to catsup.

ELAINE: I guess we could've called for help.

JERRY: But then we would have missed the whole thing.

KRAMER: I still had it on video. We could have watched it later.

GEORGE: Yeah, he's right.

JERRY: I forgot about the video.

ELAINE: Sure - the video.

(Jerry's beeper starts beeping)

ELAINE: What is that?

JERRY: Plane's ready.

[Rivera Live news show]

RIVERA: Hi everybody, I'm Geraldo Rivera. Tonight we'll be talking about what most of you have probably been discussing in your homes, and around the water coolers in your offices. I am speaking of course of the controversial Good Samaritan trial that gets underway Thursday in Latham, Massachusetts. Now before we meet our distinguished panel, let's go to Latham live, where Jane Wells is standing by. Jane-

WELLS: Yes. Good evening, Geraldo.

RIVERA: What's the mood? What's going on tonight?

WELLS: Well, Latham is fairly quite tonight, considering the media circus that has descended upon this quaint little town.

RIVERA: And what about the defendants - the so-called New York Four. How are they holding up?

WELLS: Well, I did speak with one of the deputies who had some contact with them, and he told me quote &quot;There's no love lost with that group.&quot;

RIVERA: Anything else, Jane?

WELLS: There also seems to be some friction between Mr. Seinfeld, and Ms. Benes. The rumor is that they once dated, and it's possible that ended badly.

RIVERA: Well, ladies and gentlemen, who know, maybe this trial will bring them closer together. Maybe they'll even end up getting married.

[Jerry's parents are packing]

HELEN: I hope you packed enough - this trial could last for weeks.

MORTY: What's all that?

HELEN: Cereal.

MORTY: You're packing cereal?

HELEN: I'm bringing it for Jerry.

MORTY: You got enough here for a life sentence.

HELEN: He likes it. He says he misses that more that anything.

MORTY: So bring a snack-pack.

[George's parents are packing]

ESTELLE: Poor Georgie, was it our fault this happened to him? Did we do something wrong? Maybe it was our fault.

FRANK: Maybe it was your fault. It wasn't my fault. I can tell you that.

ESTELLE: Oh, so it was my fault, but not yours.

FRANK: You were the one who smothered him.

ESTELLE: I did not smother him.

FRANK: You smothered! He couldn't get any air! He couldn't breathe! He was suffocating!

ESTELLE: Sure, and you were always in Korea with your religious chachkis.

FRANK: I had to make a living!

[Newman, laughing, leaves his building with a suitcase. Uncle Leo leaves with his bags, followed by J. Peterman, David Puddy in his 8-ball jacket, Mickey, Kenny Bania, Mr. and Mrs. Ross, Mr. Bookman, Keith Hernandez, and George Steinbrenner.]

[The four are at a table. Jerry and Kramer are enjoying some cereal, while they wait for Jackie Chiles.]

KRAMER: This is excellent huh? Don't worry I didn't use too much milk, cause I know we gotta make it last.

JERRY: You know I've had to reduce my milk level. My whole life I've always filled to at least three quarters - sometimes, to the top of the cereal. Now, to conserve, I can't even see the milk anymore. It's a big adjustment.

KRAMER: I bet.

JERRY: It's one of the hardest things I've ever had to do.

(Enter Jackie Chiles)

CHILES: Good morning.

ELAINE: Good morning, Jackie.

JERRY: Good morning.

CHILES: Is everybody ready? Didn't I tell you I wanted you to wear the cardigan?

GEORGE: It makes me look older.

CHILES: Look older? Do you think this is a game? Is that what you think this is? I'm trying to give you amoral compass. You have no moral compass. You're going to walk into that courtroom, and the jury's going to see a mean, nasty, evil George Costanza. I want them to see Perry Como. No one's going to convict Perry Como. Perry Como helps out a fat tub who's getting robbed.

(Jerry laughs)

CHILES: Do you think it's funny?

JERRY: No.

CHILES: You damn right it isn't. You better not be carrying on laughing in that courtroom, funny man. Cause if you start getting all smart-alecky, making wisecracks, acting a fool, you gonna find yourself in here for a long, long time. I don't like that tie. Suzie, get one of my ties from my briefcase.

ELAINE: How do I look, Jackie?

CHILES: Oh, you looking good. You look strong. You one fine-looking sexy lady.

ELAINE: Thank you, Jackie.

KRAMER: How bout me, Jackie?

CHILES: Kramer, you always look good. You got respect for yourself. You're genuine. Jury's going to pick up on that.

(Jackie hands Jerry a tie)

CHILES: Here.

JERRY: This one?

CHILES: That's right.

JERRY: Do I have to?

ELAINE: Jackie says put it on, Jerry.

[Court is starting]

BAILIFF: All rise. Fourth District County Court, Latham, Massachusetts is now in session. The Honorable Judge Arthur Vandelay presiding.

GEORGE: Vandelay? The judge's name is Vandelay?

CHILES: Vanda who?

GEORGE: Jerry, did you hear that?

JERRY: Yeah.

GEORGE: I think that's a good sign.

VANDELAY: Is the District Attorney ready to proceed?

HOYT: We are, Your Honor.

VANDELAY: Mr. Hoyt.

HOYT: Ladies and gentlemen, last year, our City Council by a vote of twelve to two, passed a Good Samaritan Law. Now, essentially, we made it a crime to ignore a fellow human being in trouble. Now this group from New York not only ignored, but, as we will prove, they actually mocked the victim as he was being robbed at gunpoint. I can guarantee you one other thing, ladies and gentlemen, this is not the first time they have behaved in this manner. On the contrary, they have quite a record of mocking and maligning. This is a history of selfishness, self-absorption, immaturity, and greed. And you will see how everyone who has come into contact with these four individuals has been abused, wronged, deceived and betrayed. This time, they have gone too far. This time they are going to be held accountable. This time, they are the ones who will pay.

(Newman shown eating popcorn)

VANDELAY: Mr. Chiles.

CHILES: I am shocked and chagrined, mortified and stupefied. This trial is outrageous! It is a waste of the taxpayers' time and money. It is a travesty of justice that these four people have been incarcerated while the real perpetrator is walking around laughing - lying and laughing, laughing and lying. You know what these four people were? They were innocentbystanders. Now, you just think about that term. Innocent. Bystanders. Because that's exactly what they were. We know theywere bystanders, nobody's disputing that. So how can a bystander be guilty? No such thing. Have you ever heard of a guilty bystander? No, because you cannot be a bystander and be guilty. Bystanders are by definition, innocent. That is the nature of bystanding. But no, they want to change nature here. They want to create a whole new animal - the guilty bystander. Don't you let them do it. Only you can stop them.

VANDELAY: Is the prosecution ready to present its first witness?

HOYT: We are, Your Honor. Call Officer Matt Vogel to the stand.

BAILIFF: Call Matt Vogel.

HOYT: So they were just standing there?

VOGEL: Yes.

HOYT: Did one of them have a video camera?

VOGEL: Yes.

HOYT: Your Honor, with the court's permission, we would like to play back that video and enter it into evidence as Exhibit A.

VANDELAY: Proceed.

[The tape plays]

VOGEL: Don't shoot.

JERRY: Well, there goes the money for the lipo.

ELAINE: See, the great thing about robbing a fat guy is it's an easy getaway. They can't really chase ya!

GEORGE: He's actually doing him a favor. It's less money for him to buy food.

[New Witness: the victim of the robbery]

HOYT: So they just stood there and did nothing?

VOGEL: Yeah, nothing. Nothing!

HOYT: No further questions.

(Enter NBC executives Stu Chermak and Jay Crespi)

GEORGE: Hey! Great plane! Thanks a lot. Piece of junk. You know you almost got us killed!

HOYT: Call Mabel Choate to the stand.

BAILIFF: Call Mabel Choate.

CHILES: Your Honor. I most strenuously and vigorously object to this witness. She was not present at the time of the incident. Her testimony is irrelevant, irrational, and inconsequential.

HOYT: Your Honor, the prosecution has gone to great lengths and considerable cost to find these character witnesses.It is imperative that we establish this is not merely an isolated incident. It's part of a pattern of anti-social behavior that's been going on for years.

VANDELAY: Objection overruled. I'll hear the witness.

HOYT: Now, Mrs. Choate, would you please tell the court what happen the evening of January 4th.

CHOATE: Well, I was in Snitzer's Bakery when I got accosted by that man.

HOYT: Let the record show that she is pointing at Mr. Seinfeld.

HOYT: What did he want?

CHOATE: My marble rye.

HOYT: Your marble rye?

CHOATE: I got the last one. He kept persisting, and I said no.

HOYT: And then you left the bakery.

CHOATE: That's right.

HOYT: But it didn't end there, did it, Mrs. Choate?

CHOATE: Oh no.

[Flash-back from past episode, &quot;The Rye&quot;]

JERRY: Gimme that rye.

CHOATE: Stop it.

JERRY: I want that rye lady.

CHOATE: Help - someone help.

JERRY: Shut up, you old bag!

[Back to the courtroom]

HOYT: No further questions.

HOYT: I call Marla Penny to the stand.

BAILIFF: Call Marla Penny.

JERRY: The virgin!

HOYT: And what was your connection to the defendants?

PENNY: I dated Mr. Seinfeld for several weeks in the autumn of 1992.

HOYT: Then on the evening of October 28, there was an abrupt end to that relationship. Tell us what happened.

PENNY: It's rather difficult to talk about.

HOYT: It's alright. Take your time.

PENNY: Well, I became aware of a -

HOYT: A what?

PENNY: A, uh -

HOYT: Yes?

PENNY: A contest.

HOYT: Contest?

PENNY: Yes.

HOYT: What was the nature of the contest?

PENNY: Oh please, I can't.

HOYT: It's okay.

PENNY: The four of them made a wager to see if they could -

HOYT: Yes?

PENNY: To see who could go the longest without gratifying themselves.

PETERMAN: For the love of God!

PENNY: It was horrible, horrible!

HOYT: Call Donald Sanger to the stand.

JERRY: Who the hell is that?

MR. SANGER: Come on Donald, you're doing fine.

GEORGE: The Bubble Boy!

CHILES: Bubble Boy?

JERRY: That's right, the Bubble Boy.

CHILES: What's a Bubble Boy?

JERRY: He's a boy who lives in a bubble.

BUBBLE BOY: What the hell are all you looking at?

HOYT: So Donald, would you please tell the court about the incident that occurred in your house, October 7th, 1992.

BUBBLE BOY: Well, Jerry Seinfeld was supposed to come to my house, but his friend Costanza showed up instead, so I challenged him to a game of Trivial Pursuit.

[Flash-back from past episode, &quot;The Bubble Boy&quot;]

GEORGE: Who invaded Spain in the Eighth Century?

BUBBLE BOY: That's a joke - the Moors.

GEORGE: Oh no - I'm so sorry, it's the Moops. The correct answer is the Moops.

BUBBLE BOY: Moops? Let me see that. That's not Moops, you jerk. It's Moors. It's a misprint.

GEORGE: Sorry, the card says Moops.

BUBBLE BOY: It doesn't matter. It's Moors - there's no Moops.

GEORGE: It's Moops.

BUBBLE BOY: Moors!

GEORGE: Moops!

GEORGE: Help! Someone!

BUBBLE BOY: There's no Moops, you idiot.

SUSAN: Stop it! Let go of him!

MRS. SANGER: Donald, stop it. No. Donald, stop it.

[Back to the court]

GEORGE: It was Moops.

BUBBLE BOY: Moors.

[New Witness: the lady Kramer gave a defective wheelchair to in &quot;The Handicapped Spot&quot;]

HOYT: So Mr. Costanza parked in a handicapped spot, and as a result you got in an accident, and your wheelchair was destroyed?

LADY: That's right.

HOYT: And then Mr. Kramer gave you a used wheelchair?

LADY: That's right.

[Flash-back showing lady screaming going out of control down hill in her wheelchair.]

[New Witness: Dr. Wilcox, the doctor on duty when Susan died]

HOYT: So you were the doctor on duty the night Susan Ross died?

WILCOX: Yes, that's right. It was May 16, 1996. I'll never forget it.

HOYT: So you broke the news to Mr. Costanza? Could you tell the court, please, what his reaction was?

WILCOX: I would describe it as restrained jubilation.

MR. ROSS: Murderer!

MRS. ROSS: He killed our daughter! He knew those envelopes were toxic!

VANDELAY: Order in this court!

HOYT: Call Sidra Holland to the stand.

CHILES: Whew! Look at this one, she fine. You dated her?

HOYT: So you met Jerry Seinfeld in a health club sometime in 1993?

SIDRA: Yes.

HOYT: And you also met Miss Benes in that same health club?

SIDRA: Yes, that's true.

HOYT: Would you describe the circumstances of that meeting.

SIDRA: We were in the sauna, making chit-chat.

[Flash-Back from past episode, &quot;The Implant&quot;]

SIDRA: You know, I've seen you around the club. My name's Sidra. This is Marcie.

ELAINE: Oh, hi, I'm Elaine.

[Back in the courtroom]

HOYT: So, she pretended to trip, and she fell into your breasts?

SIDRA: Yes.

HOYT: Why would she do something like that?

SIDRA: Because he sent her in there to find out if they were real.

[New Witness: Joe Bookman, library cop]

HOYT: State your name.

BOOKMAN: Bookman, Joe Bookman.

HOYT: And what's your occupation?

BOOKMAN: I'm a library cop.

HOYT: What does a library cop do?

BOOKMAN: We chase down library delinquents.

HOYT: Anyone in this room ever delinquent?

BOOKMAN: Yeah, he was. Right over there - Seinfeld.

HOYT: How long was his book overdue?

BOOKMAN: 25 years. We don't call them delinquent after that long.

HOYT: What do you call them?

BOOKMAN: Criminals.

[New Witness: George's old girlfriend]

HOYT: So you and Mr. Costanza were dating.

WOMAN: Yes.

HOYT: And then what happened?

WOMAN: Well, I invited him to attend my son's birthday party and -

[Flash-Back from past episode, &quot;The Fire&quot;]

GEORGE: Fire! Get out of the way!

[New Witness: parking lot security guard]

GUARD: At the time, I was employed as a security guard in the parking lot at the Garden Valley Shopping Mall.

[Flash-back from past episode, &quot;The Parking Garage&quot;]

JERRY: Why would I do it unless I was in mortal danger? I know it's against the law.

GUARD: I don't know.

JERRY: Because I could get uromycitisis poisoning and die - that's why.

[Back to the courtroom]

HOYT: Uromycitisis! I wonder if they're having any trouble controlling themselves during this trial? Perhaps these two hooligans would like to have a pee party right here in the courtroom!

CHILES: Objection, Your Honor! This is completely inappropriate! My clients' medical condition is not on trial here! I refer you to the Disability Act of 1990.

VANDELAY: Sit down, Mr. Chiles.

[New Witness: Police Detective]

HOYT: Alright, Detective, then what happened?

DETECTIVE: We got a tip that a lot of prostitutes had been turning tricks in the parking lot.

[Flash-Back from past episode, &quot;The Wig Master&quot;]

PROSTITUTE: You just cost me some money.

KRAMER: Cool it, lady. Cool it. Cool it, lady. Cool it.

POLICE: Police officers - freeze right there!

[Back to the courtroom]

HOYT: So Cosmo Kramer was, in fact, a pimp.

(Detective shacks his head &quot;yes&quot;)

[Witness: the low-talker from &quot;The Puffy Shirt&quot;]

HOYT: So you asked Mr. Seinfeld if he would wear your puffy shirt on the Today Show?

LOW-TALKER: (Mumbles)

HOYT: Excuse me?

CHILES: Uh, excuse me, Your Honor, but what is the point of this testimony? This woman's a low-talker. I can't hear a word she's saying. So either get some other kind of microphone up there, or let's move on.

[New Witness: George Steinbrenner]

HOYT: Call George Steinbrenner to the stand.

BAILIFF: Call George Steinbrenner.

HOYT: So George Costanza came to work for you in May of 1994?

STEINBRENNER: Yes, that's right, he was good kid - a lovely boy. Shared his calzone with me - that was a heck of a sandwich, wasn't it, Georgie?

GEORGE: Yes, sir, that was a good sandwich, sir.

STEINBRENNER: He had one little problem though.

HOYT: What was that?

STEINBRENNER: He was a communist. Thick as they come. Like a big juicy steak.

FRANK: How could you give twelve million dollars to Hideki Irabu?!

VANDELAY: Order!

[New Witness: Marcellino from &quot;The Little Jerry&quot;]

HOYT: Cock fighting?

MARCELLINO: Cock fighting.

[New Witness: Pharmacist from &quot;The Sponge&quot;]

PHARMACIST: Sponges. I don't mean the kind you clean your tub with. They're for sex. Said she needed a whole case of them.

[New Witness: Elaine's old boyfriend from work]

MAN: She exposed her nipple.

[New Witness: Mr. Pitt]

HOYT: How did she try to kill you?

PITT: She tried to smother me with a pillow.

HOYT: Call Yev Kassem to the stand.

BAILIFF: Call Yev Kassem.

JERRY: Who?

ELAINE: The Soup Nazi!

CHILES: Soup Nazi? You people have a little pet name for everybody.

HOYT: State your name.

SOUP NAZI: Yev Kassem.

HOYT: Could you spell that?

SOUP NAZI: No! Next question.

HOYT: How do you know the defendants?

SOUP NAZI: They used to come to my restaurant.

[Flash-Back to &quot;The Soup Nazi&quot; Episode]

GEORGE: Medium turkey chili.

JERRY: Medium crab bisque.

GEORGE: I didn't get any bread.

JERRY: Just forget it. Let it go.

GEORGE: Um, excuse me, I think you forgot my bread.

SOUP NAZI: You want bread?

GEORGE: Yes, please.

SOUP NAZI: Three dollars!

GEORGE: What?

SOUP NAZI: No soup for you!

[Back in the courtroom]

SOUP NAZI: But the idiot clowns did not know how to order. I banned that one - the woman - for a year. Then one day, she came back.

[Flash-Back to the &quot;Soup Nazi&quot; episode]

ELAINE: Five cups chopped porcini mushrooms. Half a cup of olive oil. Three pounds celery.

SOUP NAZI: That's my recipe for wild mushroom.

ELAINE: You're through, Soup Nazi. Pack it up. No more soup for you. Next!

[Back in the courtroom]

SOUP NAZI: She published my recipes. I had to close the store, move to Argentina. She ruined my business!

ELAINE: Soup's not all that good anyway.

SOUP NAZI: What did you say?!

HOYT: The state calls Mr. Babu Bhatt to the stand.

JERRY: How did they find Babu?

ELAINE: I thought he was deported.

HOYT: You came a long way to be here today, haven't you?

BABU Yes, all the way from Pakistan.

HOYT: And what's your connection to the defendant?

BABU I owned a restaurant. Seinfeld told me to change the menu to Pakistani. But nobody came! There were no people.

HOYT: And then what happened?

BABU Then, he got me an apartment in his building. But they mixed up the mail. And I never got my immigration renewal papers. So they deported me. It's all his fault. Him. And the woman. But they did not care. They're totally indifferent. All they do is mock me, just like they did the fat fellow. All the time. Mocking, mocking, mocking, mocking, mocking. All the time! Now it is Babu's turn to mock. Finally I will have some justice. Send them away! Send them all away! Lock them up forever! They are not human. Very bad! Very, very, very bad!

[Rivera Live]

RIVERA: Hi everybody, I'm Geraldo Rivera and welcome to this special edition of Rivera Live. Well, arguments in the Good Samaritan trial ended today. The jury has been in deliberation for four and a half hours now. Let's go live to Jane Wells who is in Latham, Massachusetts, covering this trial for us. Jane -

WELLS: Geraldo, just a few minutes ago, the jury asked to see the video tape.

RIVERA: That's the one where they are overheard making sarcastic remarks during the robbery.

WELLS: Yes, it's a very incriminating piece of evidence. But I must tell you, Geraldo, this courtroom and everyone who has attended this trial is still reeling from the endless parade of witness who have come forth so enthusiastically to testify against these four seemingly ordinary people. One even had the feeling that if Judge Vandelay didn't finally put a stop to it, it could've gone on for months.

RIVERA: Jane, whose testimony do you think resonated most strongly with this jury?

WELLS: That is so hard to say. Certainly there's the doctor with the poison invitations. The Bubble Boy was an extremely sympathetic and tragic figure. And that bizarre contest certainly didn't sit well with this small town jury.There's the woman they sold the defective wheelchair to, the deported Pakistani restaurateur. Geraldo, it just went on, and on, and on, into the night.

RIVERA: And so we wait.

[The group, waiting for the jury to decide]

JERRY: Do they make you wear uniforms in prison?

ELAINE: I think so.

JERRY: It's not that bright orange one is it?

ELAINE: I hope it's not that one, because I cannot wear orange.

KRAMER: Will you stop worrying? Jackie's going to get us off. He never loses. How about when he asked that cop if a black man had ever been to his house. Did you see the look on his face?

[George's mother, Estelle, tries to butter up Judge Vandelay]

ESTELLE: Sorry to bother you, Judge.

VANDELAY: How did you get in here?

ESTELLE: Please, if he's found guilty, please be kind to him. He's a good boy.

VANDELAY: This is highly irregular.

ESTELLE: Well, maybe there's something I can do for you.

VANDELAY: What do you mean?

ESTELLE: You know

[Witnesses waiting in pool hall, at restaurant, etc. Mr. Ross buys a gun]

[Sidra Holland and Jackie Chiles are in bed]

SIDRA: Oh, Jackie, you're so articulate.

CHILES: We have plenty of time, too. This jury could be out for days.

(Phone rings)

CHILES: Hello? Damn. They're ready.

[Back in the courtroom]

JERRY: Hey Elaine, what was it you were about to say to me on the plane when it was going down?

ELAINE: I've always loved ...United Airlines.

(Jury reenters the courtroom)

KRAMER: I think it's going to be okay - that girl just smiled at me.

JERRY: Maybe because she knows you're going to jail.

BAILIFF: All rise.

VANDELAY: Ladies and gentlemen of the jury, have you reached a verdict?

FOREMAN: We have, Your Honor.

VANDELAY: Will the defendants please rise. And how do you find, with respect to the charge of criminal indifference?

FOREMAN: We find the defendants - guilty.

VANDELAY: Order! Order in this court, I will clear this room! I do not know how, or under what circumstances the four of you found each other, but your callous indifference and utter disregard for everything that is good and decent has rocked the very foundation upon which our society is built. I can think of nothing more fitting than for the four of youto spend a year removed from society so that you can contemplate the manner in which you have conducted yourselves. I know I will. This court is adjourned.

(Exit Judge Vandelay)

GEORGE: You had to hop! You had to hop on the plane.

ELAINE: Puddy, don't wait for me.

PUDDY: Alright.

FRANK: We gotta get out of here. We want to beat the traffic.

SIDRA: Come on, Jackie. Let's go.

JERRY: What?

CHILES: Oh, and by the way, they're real, and they're spectacular.

[The group, walking back to the holding cell]

JERRY: Well, it's only a year. That's not so bad. We'll be out in a year, and then we'll be back

KRAMER: Could be fun. Don't have to worry about your meals, or what you're going to do Saturday night. And they do shows. Yeah, we could put on a show - maybe &quot;Bye Bye Birdie&quot; or &quot;My Fair Lady&quot;. Elaine, you could be Liza Doolittle.

ELAINE: Why don't you just blow it out your a...

(They enter the cell)

ELAINE: If I call Jill from prison, do you think that would make up for the other ones?

JERRY: Sure.

ELAINE: Cause you only get one call. The prison call is like the king of calls.

JERRY: I think that would be a very nice gesture.

KRAMER: I got it - it's out! How about that, huh? Oh, boy, what a relief.

JERRY: See now, to me, that button is in the worst possible spot.

GEORGE: Really?

JERRY: Oh yeah. The second button is the key button. It literally makes or breaks the shirt. Look at it, it's too high, it's in no-man's land.

GEORGE: Haven't we had this conversation before?

JERRY: You think?

GEORGE: I think we have.

JERRY: Yeah, maybe we have.

[Epilogue, on stage in the prison]

JERRY: So what is the deal with the yard? I mean when I was a kid my mother wanted me to play in the yard. But of course she didn't have to worry about my next door neighbor Tommy sticking a shiv in my thigh. And what's with the lockdown? Why do we have to be locked in our cells? Are we that bad that we have to be sent to prison, in prison? You would think the weightlifting and the sodomy is enough. So, anyone from Cellblock D?

PRISONER 1: I am.

JERRY: I'll talk slower. I'm kidding - I love Cellblock D. My friend George is in Cellblock D. What are you in for,sir?

PRISONER 2: Murder one.

JERRY: Murder one? Oooooo, watch out everybody. Better be nice to you. I'm only kidding sir - lighten up. How about you, what are you in for?

PRISONER 3: Grand theft auto.

JERRY: Grand theft auto - don't steal any of my jokes.

PRISONER 3: You suck - I'm gonna cut you.

JERRY: Hey, I don't come down to where you work, and knock the license plate out of your hand.

GUARD: Alright, Seinfeld, that's it. Let's go. Come on.

JERRY: Alright, hey, you've been great! See you in the cafeteria.

The End
